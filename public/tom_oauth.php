<?php

/*
 * tom微信借授权
 */
$url = get_url();

$oauth_back_url = urldecode($_GET['oauth_back_url']);

preg_match("#((http|https)://[^?]*/)tom_oauth.php#", $url, $urlmatches);
if (is_array($urlmatches) && !empty($urlmatches['0'])) {
	$url = str_replace($urlmatches['0'], $oauth_back_url, $url);
}

header('Location: ' . $url);

function get_url() {
	$protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
	$url = "$protocol$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
	return $url;
}

//如何借用其他微信公众号网页授权是个让人烦躁的事情，js代码在部分机型效果不明显，然而某插件商提供的方法让我佩服的五体投地。
//如果在你的方法要使用该方法，你只需要在节点中加入以下方法即可
//    if ($wxasnum == 1 || $wxasnum == 10000) {
//        $redirecturl = 'xxx.php?oauth_back_url=' . urlencode($redirecturl);
//    }