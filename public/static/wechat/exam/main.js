/**
 * 考试调查报告
 */
function sssss() {
	var total = ($('input[type=radio]').length) / 4;
	var answerd = 0;
	var jsonstr = '';
	$("input[type='radio']").each(function() {
		if ($(this).is(":checked")) {
			var itemid = $(this).attr("data-id");
			var value = $(this).val();
			answerd += 1;
			jsonstr += ',{"itemid":' + itemid + ',"selected":"' + value + '"}';
		}
	});

	jsonstr = '[' + jsonstr.substr(1, jsonstr.length) + ']';

	if (total == answerd) {
		$(".submit").show();
		$("#datas").val(jsonstr);
	}
}

function sendanswer(url) {
	$.post(url, {
		data : $("#datas").val(),
		examid : $("#examid").val(),
		r : Math.random()
	}, function(res) {
		alert(res.msg);		
		if(res.code==1){
			location.href=res.data;
		}
	});
}
function alertmsg(msg, url, overtime=5000) {
	var style = "display: block; width: 92%;padding:4%; height: 100%; z-index: 10; position: fixed; text-align: center; top: 0px; background: rgba(1,1,1,0.8); color: #fff; padding-top: 200px; font-size: 1em;line-height:1.5em;left:0;";
	var str = '<div id="alertc" style="' + style + '">' + msg + '</div>';
	$("body").append(str);
	setTimeout('$("#alertc").remove()', overtime);

	if (url != null && url != "") {
		location.href = url;
	}
}

function checkForm(o){    
	var re= /^(((13[0-9]{1})|(15[0-9]{1})|(17[0-9]{1})|(18[0-9]{1}))+\d{8})$/; 
	
	if(!re.test(o.examphone.value)){      
		alert('请输入正确的手机号码');
		$('#examphone').val('');
		return false;   
	}
	if($('#examname').val()==''){  
		alert('请输您的姓名或者其他称呼');
		return false;
	}
}


$(document).ready(function() {
	var ua = window.navigator.userAgent.toLowerCase();
	if (ua.match(/MicroMessenger/i) != 'micromessenger') {
		alertmsg('本活动仅可以在微信中打开答题 <br/><br/><img src="http://www.5atl.com/weixin.jpg" /><br/><br/>微信扫描二维码回复“答题'+$("#examid").val()+'”参与答题', '',10000);
		return false;
	}
	else{
		if($('#authurl').val()!=undefined){
			var html='你准备好答题了吗？<br/><br/>';
			html+='<form action="'+$("#authurl").val()+'" method="post" class="info" onsubmit="return checkForm(this)" >';
			html+='<input type="text" id="examname" name="examname" placeholder="您的名字" />';
			html+='<input type="text" id="examphone" name="examphone" placeholder="您的电话" />';
			html+='<input type="submit" value="开始答题" />';
			html+='<br/><br/>完善资料答题后有机会获得奖励哟';
			html+='</form>';
			alertmsg(html, '',100000);
		}
	}
});