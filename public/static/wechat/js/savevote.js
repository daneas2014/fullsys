var voteid = $("#vote_id").val();

$(".bottom_name").change(function() {
	var parent = $(this).parent().parent();
	var url = $(this).find("option:selected").attr("data-url");
	parent.find(".bottom_link").val(url);
	parent.find(".bottom_icon").val(url);
});
// 奖品保存
$(".psave").click(function() {
	var parent = $(this).parent().parent();
	var dataline = "prizegrade=" + parent.find(".prizegrade").val();
	dataline += "&prizename=" + parent.find(".prizename").val();
	dataline += "&prizesponsor=" + parent.find(".prizesponsor").val();
	dataline += "&sponsorurl=" + parent.find(".sponsorurl").val();
	dataline += "&prizepic=" + parent.find(".prizepic").val();
	dataline += "&prizenum=" + parent.find(".prizenum").val();
	dataline += "&id=" + parent.find(".valueid").val();
	dataline += "&vote_id=" + voteid;
	dataline += "&stype=3";

	saveValues(dataline, parent);
});

$(".pdel").click(function() {
	var parent = $(this).parent().parent();
	var id = parent.find(".valueid").val();
	if (parseInt(id) > 0) {
		var dataline = "dtype=3&id=" + id;
		var res = delValues(dateline);
		layer.msg(res.msg, {
			icon : 0,
			time : 1500,
			shade : 0.1
		});
	}

	parent.remove();
});

// 广告保存
$(".bsave").click(function() {

	var parent = $(this).parent().parent();
	var dataline = "img_url=" + parent.find(".img_url").val();
	dataline += "&external_links=" + parent.find(".external_links").val();
	dataline += "&banner_rank=" + parent.find(".banner_rank").val();
	dataline += "&title=" + parent.find(".title").val();
	dataline += "&id=" + parent.find(".valueid").val();
	dataline += "&vote_id=" + voteid;
	dataline += "&stype=5";

	saveValues(dataline, parent);
});

$(".bdel").click(function() {
	var parent = $(this).parent().parent();
	var id = parent.find(".valueid").val();
	if (parseInt(id) > 0) {
		var dataline = "dtype=4&id=" + id;
		var res = delValues(dateline);
		layer.msg(res.msg, {
			icon : 0,
			time : 1500,
			shade : 0.1
		});
	}

	parent.remove();
});

// 赞助商保存
$(".ssave").click(function() {
	var parent = $(this).parent().parent();
	var dataline = "sponsorname=" + parent.find(".sponsorname").val();
	dataline += "&sponsorurl=" + parent.find(".sponsorurl").val();
	dataline += "&sponsorpic=" + parent.find(".sponsorpic").val();
	dataline += "&sponsorurl=" + parent.find(".sponsorurl").val();
	dataline += "&id=" + parent.find(".valueid").val();
	dataline += "&vote_id=" + voteid;
	dataline += "&stype=4";

	saveValues(dataline, parent);
});

$(".sdel").click(function() {
	var parent = $(this).parent().parent();
	var id = parent.find(".valueid").val();
	if (parseInt(id) > 0) {
		var dataline = "dtype=5&id=" + id;
		var res = delValues(dateline);
		layer.msg(res.msg, {
			icon : 5
		}, function(index) {
			layer.close(index);
		});
		return true;
	}

	parent.remove();
});

// 菜单保存
$(".msave").click(function() {

	var parent = $(this).parent().parent();
	var dataline = "bottom_name=" + parent.find(".bottom_name").val();
	dataline += "&bottom_link=" + parent.find(".bottom_link").val();
	dataline += "&bottom_icon=" + parent.find(".bottom_icon").val();
	dataline += "&bottom_rank=" + parent.find(".bottom_rank").val();
	dataline += "&otherlink=" + parent.find(".otherlink").val();
	dataline += "&hide=" + parent.find(".bottom_hide").val();
	dataline += "&id=" + parent.find(".valueid").val();
	dataline += "&vote_id=" + voteid;
	dataline += "&stype=6";

	saveValues(dataline, parent);
});

function saveValues(dataline, parent) {
	$.ajax({
		type : "post",
		url : "/wechat/votemanage/save.html",
		data : dataline,
		dataType : 'json',
		success : function(res) {
			layer.msg(res.msg, {
				icon : 5
			}, function(index) {
				layer.close(index);
			});
			if (res.code != 1) {
				return false;
			} else {
				parent.find(".valueid").val(res.data);
			}
		}
	});
}

function delValues(dateline) {
	$.ajax({
		type : "post",
		url : "/wechat/votemanage/delete.html",
		data : dataline,
		dataType : 'json',
		success : function(res) {
			layer.msg(res.msg, {
				icon : 5
			}, function(index) {
				layer.close(index);
			});
			if (res.code != 1) {
				return false;
			} else {
				locatioin.href = location.href;
			}
		}
	});
}

function newrow(tableid) {
	var tr = '';
	switch (tableid) {
	case "prizetable":
		tr = '<tr><td><input type="text" class="prizegrade"/></td><td><input type="text" class="prizename" /></td><td><input type="text" class="prizesponsor"/></td><td><input type="text" class="sponsorurl"  value="http://"/></td><td><input type="text" class="prizepic"  value="http://"/></td><td><input type="text" class="prizenum"/></td><td><input type="button" class="psave" value="保存"> || <input type="button" class="pdel" value="删除"><input type="hidden" class="valueid"/></td></tr>';
		break;
	case "sponsor":
		tr = '<tr><td><input type="text" class="sponsorname"/></td><td><input type="text" class="sponsorurl"/></td><td><input type="text" class="sponsorpic" /></td><td><input type="button" class="ssave" value="保存"> || <input type="button" class="sdel" value="删除"><input type="hidden" class="valueid"/></td>				</tr>';
		break;
	case "bannars":
		tr = '<tr><td><input type="text" class="img_url"/></td><td><input type="text" class="external_links"/></td><td><input type="text" class="title"/></td><td><input type="text" class="banner_rank"/></td><td><input type="button" class="bsave" value="保存"> || <input type="button" class="bdel" value="删除"><input type="hidden" class="valueid"/></td>				</tr>';
		break;
	}

	$("#" + tableid).append(tr);
}