/**
 * 本js先加载所有的js和css资源，也包含投票功能所有的js方法
 */
var rightdomain='www.5atl.com';

function getCountDown(timestamp,str) {
	var nowTime = new Date();
	var endTime = new Date(timestamp * 1000);

	if (endTime < nowTime) {
		return true;
	}

	setInterval(function() {
		var nowTime = new Date();
		var endTime = new Date(timestamp * 1000);

		if (endTime < nowTime) {
			$("#timer").html("活动已开始");
		}

		var t = endTime.getTime() - nowTime.getTime();
		var day = Math.floor(t / 1000 / 60 / 60 / 24);
		var hour = Math.floor(t / 1000 / 60 / 60 % 24);
		var min = Math.floor(t / 1000 / 60 % 60);
		var sec = Math.floor(t / 1000 % 60);

		if (hour < 10) {
			hour = "0" + hour;
		}
		if (min < 10) {
			min = "0" + min;
		}
		if (sec < 10) {
			sec = "0" + sec;
		}
		var countDownTime = str + day + "天" + hour + "小时" + min + "分钟" + sec + "秒";
		$("#timer").html(countDownTime);

	}, 1000);
}

function valilen(obj, l) {
	var t = $(obj);
	var v = t.val();
	if (v.length > l) {
		t.val(v.substr(0, l));
	}
}

function voteprocess(tourl, fromurl, voteid, voteitemid) {
	var thisdomain=location.host;
	if(thisdomain!=rightdomain){
		var turnto='http://'+rightdomain+'/wechat-vote-item.html?voteitemid='+voteitemid+'&voteid='+voteid;
		alertmsg('即将跳转至选手页面投票',turnto,1000);
		return false;
	}
	
	var ua = navigator.userAgent.toLowerCase();
	if (ua.match(/MicroMessenger/i) != 'micromessenger') {
		alertmsg('本活动仅可以在微信中打开投票|点赞 <br/><br/><img src="http://www.5atl.com/weixin.jpg"> <br/><br/>微信扫描二维码回复“投票+'+voteid+'”即可为ta投票', '',15000);
		return false;
	}
	$.post(tourl, {
		"voteid" : voteid,
		"voteitemid" : voteitemid
	}, function(res) {
		if (res.code == 1) {
			alertmsg(res.msg);
			var c = ".votecount" + voteitemid;
			var ct = parseInt($(c).text()) + 1;
			$(c).text(ct);
		} else {
			alertmsg(res.msg, res.data);
		}
	},"json");
}

function alertmsg(msg, url, overtime=5000) {
	var style = "display: block; width: 92%;padding:4%; height: 100%; z-index: 10; position: fixed; text-align: center; top: 0px; background: rgba(1,1,1,0.8); color: #fff; padding-top: 200px; font-size: 1em;line-height:1.5em;left:0;";
	var str = '<div id="alertc" style="' + style + '">' + msg + '</div>';
	$("body").append(str);
	setTimeout('$("#alertc").remove()', overtime);

	if (url != null && url != "") {
		if(isiOS()){
			location = url;
		}
		else{
			location.href = url;
		}
	}
}

function comment(tourl, msg, voteid, voteitemid) {
	var ua = navigator.userAgent.toLowerCase();
	if (ua.match(/MicroMessenger/i) != 'micromessenger') {
		alertmsg('本活动仅可以在微信中打开 <br/><img src="http://www.5atl.com/weixin.jpg"> <br/><br/>微信扫描二维码回复“投票+'+voteid+'即可为ta留言', '',15000);
		return false;
	}

	$.post(tourl, {
		"msg" : msg,
		"voteid" : voteid,
		"voteitemid" : voteitemid
	}, function(res) {
		if (res.code == 1) {
			alertmsg(res.msg, location.href);
		} else {
			alertmsg(res.msg, res.data);
		}
	});
}

// 判断是否是苹果手机
function isiOS() {
	var u = navigator.userAgent;
	var isiOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/); // ios终端
	return isiOS;
}
