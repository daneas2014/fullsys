/**
 * 随手拍要用到的所有js放这里
 */

function alertmsg(msg, url, overtime) {
	if (overtime == undefined || overtime == null || overtime < 1000) {
		overtime = 5000;
	}
	var style = "display: block; width: 92%;padding:4%; height: 100%; z-index: 10; position: fixed; text-align: center; top: 0px; background: rgba(1,1,1,0.8); color: #fff; padding-top: 200px; font-size: 1em;line-height:1.5em;left:0;";
	var str = '<div id="alertc" style="' + style + '">' + msg + '</div>';
	$("body").append(str);
	setTimeout('$("#alertc").remove()', overtime);

	if (url != null && url != "") {
		location.href = url;
	}
}

function valilen(obj, l) {
	var t = $(obj);
	var v = t.val();
	if (v.length > l) {
		t.val(v.substr(0, l));
	}
}

function apply(openid,tid) {
	if (openid == '' || openid == undefined || openid == null) {
		alertmsg('本活动仅针对铜梁视窗会员 <br/><br/><img src="http://www.5atl.com/weixin.jpg"> <br/><br/>微信扫描二维码参与','', 15000);
		return false;
	}
	
	if(tid<1){
		tid=1;
	}

	location.href = "http://www.5atl.com/wechat-pai-apply.html?openid="+ openid+"&tid="+tid;
}