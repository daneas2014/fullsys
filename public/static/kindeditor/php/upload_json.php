<?php

//define('QINIU_AUTOLOAD', '/data/myprograms/vendor/qiniu/php-sdk/autoload.php');
define('QINIU_AUTOLOAD', 'D:/centos/myprograms/vendor/qiniu/php-sdk/autoload.php');

require_once 'JSON.php';

$php_path = dirname(__FILE__) . '/';
$php_url = dirname($_SERVER['PHP_SELF']) . '/';

// 文件保存目录路径
$save_path = $_SERVER['DOCUMENT_ROOT'] . '/attachment/keditor/';
// 文件保存目录URL
$save_url = '/attachment/keditor/';
// 定义允许上传的文件扩展名
$ext_arr = array(
    'image' => array(
        'gif',
        'jpg',
        'jpeg',
        'png',
        'bmp',
    ),
    'flash' => array(
        'swf',
        'flv',
    ),
    'media' => array(
        'swf',
        'flv',
        'mp3',
        'wav',
        'wma',
        'wmv',
        'mid',
        'avi',
        'mpg',
        'asf',
        'rm',
        'rmvb',
    ),
    'file' => array(
        'doc',
        'docx',
        'xls',
        'xlsx',
        'ppt',
        'htm',
        'html',
        'txt',
        'zip',
        'rar',
        'gz',
        'bz2',
    ),
);

// 最大文件大小
$max_size = 1000000;

$save_path = realpath($save_path) . '/';

// PHP上传失败
if (!empty($_FILES['imgFile']['error'])) {
    switch ($_FILES['imgFile']['error']) {
        case '1':
            $error = '超过php.ini允许的大小。';
            break;
        case '2':
            $error = '超过表单允许的大小。';
            break;
        case '3':
            $error = '图片只有部分被上传。';
            break;
        case '4':
            $error = '请选择图片。';
            break;
        case '6':
            $error = '找不到临时目录。';
            break;
        case '7':
            $error = '写文件到硬盘出错。';
            break;
        case '8':
            $error = 'File upload stopped by extension。';
            break;
        case '999':
        default:
            $error = '未知错误。';
    }
    alert($error);
}

// 有上传文件时
if (empty($_FILES) === false) {
    // 原文件名
    $file_name = $_FILES['imgFile']['name'];
    // 服务器上临时文件名
    $tmp_name = $_FILES['imgFile']['tmp_name'];
    // 文件大小
    $file_size = $_FILES['imgFile']['size'];
    // 检查文件名
    if (!$file_name) {
        alert("请选择文件。");
    }
    // 检查目录
    if (@is_dir($save_path) === false) {
        alert("上传目录不存在。");
    }
    // 检查目录写权限
    if (@is_writable($save_path) === false) {
        alert("上传目录没有写权限。");
    }
    // 检查是否已上传
    if (@is_uploaded_file($tmp_name) === false) {
        alert("上传失败。");
    }
    // 检查文件大小
    if ($file_size > $max_size) {
        alert("上传文件大小超过限制。");
    }
    // 检查目录名
    $dir_name = empty($_GET['dir']) ? 'image' : trim($_GET['dir']);
    if (empty($ext_arr[$dir_name])) {
        alert("目录名不正确。");
    }
    // 获得文件扩展名
    $temp_arr = explode(".", $file_name);
    $file_ext = array_pop($temp_arr);
    $file_ext = trim($file_ext);
    $file_ext = strtolower($file_ext);
    // 检查扩展名
    if (in_array($file_ext, $ext_arr[$dir_name]) === false) {
        alert("上传文件扩展名是不允许的扩展名。\n只允许" . implode(",", $ext_arr[$dir_name]) . "格式。");
    }
    // 创建文件夹
    if ($dir_name !== '') {
        $save_path .= $dir_name . "/";
        $save_url .= $dir_name . "/";
        if (!file_exists($save_path)) {
            mkdir($save_path);
        }
    }
    $ymd = date("Ymd");
    $save_path .= $ymd . "/";
    $save_url .= $ymd . "/";
    if (!file_exists($save_path)) {
        mkdir($save_path);
    }
    // 新文件名
    $new_file_name = date("YmdHis") . '_' . rand(10000, 99999) . '.' . $file_ext;
    // 移动文件
    $file_path = $save_path . $new_file_name;
    if (move_uploaded_file($tmp_name, $file_path) === false) {
        alert("上传文件失败。");
    }
    @chmod($file_path, 0644);
    $file_url = $save_url . $new_file_name;

    header('Content-type: text/html; charset=UTF-8');
    $json = new Services_JSON();

    $new_file_url = uploadtoqiniu($_SERVER['DOCUMENT_ROOT'] . $file_url);

    echo $json->encode(array(
        'error' => 0,
        'url' => $new_file_url,
    ));
    exit();
}

/**
 *
 * 上传图片到七牛云，是否删除本地图片,上传成功返回路径，上传失败返回本地路径
 *
 * @param unknown $filePath
 *            真实物理路径
 * @param unknown $delold
 *
 */
function uploadtoqiniu($filePath, $delold = true)
{
    require_once QINIU_AUTOLOAD;
    $fileName = str_replace($_SERVER['DOCUMENT_ROOT'] . '/', '', $filePath);

    // 配置参数【七牛云没有文件夹概念，只有将文件命名为本地文件存储路径,$filename就是路径】 
    $domain = 'http://cdn.dmake.cn/';
    $accessKey = 'TJffc8CgU98SIq_VQB89gkcWOYalbYKJgH5CYmRi';
    $secretKey = 'oknKrL-M1cUXQ8HrmJDc7HqzKnOzhP2a0-lnZ2Ud';
    $bucket = 'dmake';

    // 初始化上传类
    $auth = new \Qiniu\Auth($accessKey, $secretKey);
    $upToken = $auth->uploadToken($bucket);

    // 上传
    $uploadMgr = new \Qiniu\Storage\UploadManager();
    list($ret, $err) = $uploadMgr->putFile($upToken, $fileName, $filePath);

    // 返回全路径
    if ($err !== null) {
        $handle = fopen('debugger.txt', "a+");
        $str = fwrite($handle, date('Y-m-d H:i:s') . "Start======\n" . json_encode($err) . "\n");
        fclose($handle);
        return $fileName;
    } else {
        if ($delold) {
            unlink($filePath);
        }
        return $domain . $fileName;
    }
}

function alert($msg)
{
    header('Content-type: text/html; charset=UTF-8');
    $json = new Services_JSON();
    echo $json->encode(array(
        'error' => 1,
        'message' => $msg,
    ));
    exit();
}
