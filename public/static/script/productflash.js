﻿function ShowImage(ev, object) {
    var event = mouseCoords(ev);
    var top = document.documentElement.scrollTop - $("#" + object).height() / 2 + 100;
    var leftx = document.body.offsetWidth;
    setTimeout(function() {
        $("#imgImg").attr("src", $("#" + object).text());
        $("#imgShow").show();

        if (event.x + 50 + 510 > leftx) {
            $("#imgShow").css("left", event.x - 510 - 50 + "px");
            $("#imgShow").css("top", top + "px");
        }
        else {
            $("#imgShow").css("left", event.x + 50 + "px");
            $("#imgShow").css("top", top + "px");
        }
    }, 0)

}

function HideImage() {
    setTimeout(function() {
        $("#imgShow").hide();
        $("#imgImg").attr("src", "/zh-CN/img/picloading.gif");
    }, 0)

}

function Move(ev, object) {
    var event = mouseCoords(ev);
    var top = document.documentElement.scrollTop - $("#" + object).height() / 2 + 100;
    var leftx = document.body.offsetWidth;
    if (event.x + 50 + 510 > leftx) {
        $("#imgShow").css("left", event.x - 510 - 50 + "px");
        $("#imgShow").css("top", top + "px");
    }
    else {
        $("#imgShow").css("left", event.x + 50 + "px");
        $("#imgShow").css("top", top + "px");
    }
}

function mouseCoords(ev) {
    if (ev.pageX || ev.pageY) {
        return { x: ev.pageX, y: ev.pageY };
    }
    return {
        x: ev.clientX,
        y: ev.clientY + 100
    };
}

function onMove(left, number, frame) {
    var total = parseInt($("#total"+frame).text());
    var max = parseInt($("#max" + frame).text());
    if (left == "left") {
        total = total - parseInt(number);
        if (total < 632 - max) total = 632 - max;
        if (total > 0) total = 0;
        moveElement('piclists' + frame, total, 0, 5);
    }
    else {
        total = total + parseInt(number);
        if (total > 0) total = 0;
        moveElement('piclists' + frame, total, 0, 5);
    }

    $("#total" + frame).text(total);
}



function moveElement(elementID, final_x, final_y, interval) {
    if (!document.getElementById) return false;
    if (!document.getElementById(elementID)) return false;
    var elem = document.getElementById(elementID);
    if (elem.movement) {
        clearTimeout(elem.movement);
    }
    if (!elem.style.left) {
        elem.style.left = "0px";
    }
    if (!elem.style.top) {
        elem.style.top = "0px";
    }
    var xpos = parseInt(elem.style.left);
    var ypos = parseInt(elem.style.top);
    if (xpos == final_x && ypos == final_y) {
        return true;
    }
    if (xpos < final_x) {
        var dist = Math.ceil((final_x - xpos) / 10);
        xpos = xpos + dist;
    }
    if (xpos > final_x) {
        var dist = Math.ceil((xpos - final_x) / 10);
        xpos = xpos - dist;
    }
    if (ypos < final_y) {
        var dist = Math.ceil((final_y - ypos) / 10);
        ypos = ypos + dist;
    }
    if (ypos > final_y) {
        var dist = Math.ceil((ypos - final_y) / 10);
        ypos = ypos - dist;
    }
    elem.style.left = xpos + "px";
    elem.style.top = ypos + "px";
    var repeat = "moveElement('" + elementID + "'," + final_x + "," + final_y + "," + interval + ")";
    elem.movement = setTimeout(repeat, interval);
}