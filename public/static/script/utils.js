﻿function arrayContains(array, item) {
    if (!array || array.length == 0) {
        return false;
    }

    for (var index = 0; index < array.length; index++) {
        if (item == array[index]) {
            return true;
        }
    }

    return false;
}

function is_email(message){
    if(isNullOrEmpty(message)){
        return false;
    }
			
    var regex = /^\s*[a-zA-Z0-9_\+-]+(\.[a-zA-Z0-9_\+-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*\.([a-zA-Z]{2,4})\s*$/;
    if(!regex.test(message)){
        return false;
    }
			
    return true;
}

function is_mobile(message) {
    if (isNullOrEmpty(message)) {
        return false;
    }

    var regex = /^1\d{10}$/;
    if (!regex.test(message)) {
        return false;
    }

    return true;
}

function tryGetCookieValue(name, defaultValue) {
    var arr = document.cookie.match(new RegExp("(^| )" + name + "=([^;]*)(;|$)"));
    if (arr != null) {
        return decodeURIComponent(arr[2]);
    }
    return defaultValue;
}

function HTMLEncode(text) {
    if (!text)
        return '';

    text = text.replace(/&/g, '&amp;');
    text = text.replace(/</g, '&lt;');
    text = text.replace(/>/g, '&gt;');

    return text;
}

function arrayContains(array, item) {
    if (!array || array.length == 0) {
        return false;
    }

    for (var index = 0; index < array.length; index++) {
        if (item == array[index]) {
            return true;
        }
    }

    return false;
}

function isNullOrEmpty(str) {
    return str == null || str.length == 0;
}

function isNullOrWhitespace(str) {
    if(isNullOrEmpty(str)) {
        return true;
    }

    for (var index = 0; index < str.length; index++) {
        if(!isNullOrEmpty(str[index])) {
            return false;
        }
    }

    return true;
}

function setCommentCookie(commentId,key) {
    var commentIds = tryGetCookieValue(key);
    if (commentIds == '') {
        setCookie(key, commentId, 24 * 1000 * 60 * 60 * 7);
    } else {
        setCookie(key, (commentIds + "," + commentId), 24 * 1000 * 60 * 60 * 7);
    }
}

function arrayKeyContains(array, key) {
    if (!array || array.length == 0) {
        return false;
    }
    for (var index = 0; index < array.length; index++) {
        if (key == array[index].key) {
            return true;
        }
    }

    return false;
}

function arrayRemoveAt(array, item) {

    if (!array || array.lenth == 0) {
        return array;
    }

    for (var index = 0; index < array.length; index++) {
        if (array[index] == item) {
            array.splice(index, 1);
        }
    }

    return array;
}

function arrayRemoveAtkey(array, key) {

    if (!array || array.lenth == 0) {
        return array;
    }

    for (var index = 0; index < array.length; index++) {
        if (array[index].key == key) {
            array.splice(index, 1);
        }
    }

    return array;
}

function getQueryParam(name)
{
       var query = window.location.search.substring(1);
       var vars = query.split("&");
       for (var i=0;i<vars.length;i++) {
               var pair = vars[i].split("=");
               if(pair[0] == name){return pair[1];}
       }
       return(false);
}

String.prototype.StringFormat = function () {
    if (arguments.length == 0) {
        return this;
    }
    for (var StringFormat_s = this, StringFormat_i = 0; StringFormat_i < arguments.length; StringFormat_i++) {
        StringFormat_s = StringFormat_s.replace(new RegExp("\\{" + StringFormat_i + "\\}", "g"), arguments[StringFormat_i]);
    }
    return StringFormat_s;
};


/**
*
* 页面增强
*
*/

function isQuotaExceeded(e) {
    var quotaExceeded = false;
    if(e) {
        if(e.code) {
            switch(e.code) {
                case 22:
                    quotaExceeded = true;
                    break;
                case 1014: // Firefox 
                    if(e.name === 'NS_ERROR_DOM_QUOTA_REACHED') {
                        quotaExceeded = true;
                    }
                    break;
            }
        } else if(e.number === -2147024882) { // IE8 
            quotaExceeded = true;
        }
    }
    return quotaExceeded;
}

function setLocalStorage(key, value) {
    var curtime = new Date().getTime(); // 获取当前时间 ，转换成JSON字符串序列 
    var valueDate = JSON.stringify({
        val: value,
        timer: curtime
    });
    try {
        localStorage.setItem(key, valueDate);
    } catch(e) {
        // 兼容性写法
        if(isQuotaExceeded(e)) {
            console.log("Error: 本地存储超过限制");
            localStorage.clear();
        } else {
            console.log("Error: 保存到本地存储失败");
        }
    }
}


function getLocalStorage(key) {
	var exp = 60 * 10 * 1000; // 10分钟的毫秒数
    if(localStorage.getItem(key)) {
        var vals = localStorage.getItem(key); // 获取本地存储的值 
        var dataObj = JSON.parse(vals); // 将字符串转换成JSON对象
        var nowTime=new Date().getTime();
        // 如果(当前时间 - 存储的元素在创建时候设置的时间) > 过期时间 
        var isTimed = ( nowTime - dataObj.timer) > exp;
        if(isTimed) {
            console.log("存储已过期");
            localStorage.removeItem(key);
            return null;
        } else {
            var newValue = dataObj.val;
        }
        return newValue;
    } else {
        return null;
    }
}

function SetCookie(name, value, days) {
    days = days || 1;
    var times = days * 24 * 60 * 60 * 1000;
    var exp = new Date();
    exp.setTime(exp.getTime() + times);
    document.cookie = name + "=" + escape(value) + ";expires=" + exp.toGMTString();
}

function getCookie(name) {
    var arr = document.cookie.match(new RegExp("(^|)" + name + "=([^;]*)(;|$)"));
    if (arr != null) arr = unescape(arr[2]);
    return arr;
}


function popalert(txt){	
	var style='style="position:fixed;left:calc(100% / 2 - 75px);top:200px;text-align:center;display:inline-block;width:150px;height:64px;padding-top:20px; border-radius:10px ;color: #fff;background:#fb6227;"';
	var pop='<div class="popalert" '+style+'>'+txt+'</div>';
	$('body').append(pop);
	$('.popalert').fadeOut(2000,function(){$(this).remove();});
}

/*
逻辑：
1. 第一次登陆记录外部来源，并登记来源的所有信息
2. 登记下游贡献次数是多少，需要排除重复阅览的页面
3. 新注册用户需要绑定用户来源渠道
*/

var rptokenstr;

/**
 * 调用api通用方法
 * @param array
 * @param callback
 */
function JsonAPI(array,callback,apiurl,type){
	var seedurl=apiurl==undefined?'/api/index/index':apiurl;
	var method=type==undefined?'post':'get';
    $.ajax({type: method,async: true,timeout: 10000,url: seedurl,data: array,traditional: true,dataType: "json",cache: true,success: function (data) {callback(data);}});
}


/**
 * 第三方登录
 * @param type
 */
function GetLoginToken(type){
	JsonAPI('c=Login&a='+type+'&t='+rptokenstr+'&returnUrl='+location.href,function(result){
        if (!result.success) {
            return;
        }
        location.href = result.authenticationUrl;        		
	});
}

/**
 * 监听输入框案件
 * @param 值
 * @param 类型
 * @param 事件
 */
function onKeyDown(value, type, event) {
    var e = event || window.event || arguments.callee.caller.arguments[0];
    if (e && e.keyCode == 27) { // 按 Esc 
        //要做的事情
    }
    if (e && e.keyCode == 113) { // 按 F2 
        //要做的事情
    }
    if (e && e.keyCode == 13) { // enter 键
        switch (type) {
            case 'search':
                window.open("/articles/" + value , "_blank");
                break;
        }
    }
}

function callService(n){
    window.open('http://wpa.qq.com/msgrd?v=3&uin=554305954&site=qq&menu=yes','_blank');
}



// 统一代码
function valiUniCode() {
    this.firstarray = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
    this.firstkeys = [3, 7, 9, 10, 5, 8, 4, 2];
    this.secondarray = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'J', 'K', 'L', 'M', 'N', 'P', 'Q', 'R', 'T', 'U', 'W', 'X', 'Y'];
    this.secondkeys = [1, 3, 9, 27, 19, 26, 16, 17, 20, 29, 25, 13, 8, 24, 10, 30, 28];
    this.verify = function (str) {
        var code = str.toUpperCase();

        /*
        统一代码由十八位的阿拉伯数字或大写英文字母（不使用I、O、Z、S、V）组成。
            第1位：登记管理部门代码（共一位字符）
            第2位：机构类别代码（共一位字符）
            第3位~第8位：登记管理机关行政区划码（共六位阿拉伯数字）
            第9位~第17位：主体标识码（组织机构代码）（共九位字符）
            第18位：校验码​（共一位字符）
        */
        if (code.length != 18) {
            return false;
        }
        var reg = /^\w\w\d{6}\w{9}\w$/;
        if (!reg.test(code)) {
            return false;
        }
        /*
            登记管理部门代码：使用阿拉伯数字或大写英文字母表示。​
            机构编制：1​
            民政：5​
            工商：9​
            其他：Y
            */
        reg = /^[1,5,9,Y]\w\d{6}\w{9}\w$/;
        if (!reg.test(code)) {
            return false;
        }
        /*
            机构类别代码：使用阿拉伯数字或大写英文字母表示。​	 
            机构编制机关：11打头​​
            机构编制事业单位：12打头​
            机构编制中央编办直接管理机构编制的群众团体：13打头​​
            机构编制其他：19打头​
            民政社会团体：51打头​
            民政民办非企业单位：52打头​
            民政基金会：53打头​
            民政其他：59打头​	 
            工商企业：91打头​
            工商个体工商户：92打头​
            工商农民专业合作社：93打头​	 
            其他：Y1打头​
            */
        reg = /^(11|12|13|19|51|52|53|59|91|92|93|Y1)\d{6}\w{9}\w$/;
        if (!reg.test(code)) {
            return false;
        }
        /*
            登记管理机关行政区划码：只能使用阿拉伯数字表示。按照GB/T 2260编码。​
            例如：四川省成都市本级就是510100；四川省自贡市自流井区就是510302。​
        */
        reg = /^(11|12|13|19|51|52|53|59|91|92|93|Y1)\d{6}\w{9}\w$/;
        if (!reg.test(code)) {
            return false;
        }
        /*
                主体标识码（组织机构代码）：使用阿拉伯数字或英文大写字母表示。按照GB 11714编码。
            */
        var firstkey = this.calc(code.substr(8), this.firstarray, this.firstkeys, 11);
        /*
            第六步：用阿拉伯数字11减去余数，得求校验码的数值。当校验码的数值为10时，校验码用英文大写字母X来表示；当校验码的数值为11时，校验码用0来表示；其余求出的校验码数值就用其本身的阿拉伯数字来表示。​
            11-2＝9，因此此公司完整的组织机构代码为 59467239-9。​​
        */
        var firstword;
        if (firstkey < 10) {
            firstword = firstkey;
        }
        if (firstkey == 10) {
            firstword = 'X';
        } else if (firstkey == 11) {
            firstword = '0';
        }
        if (firstword != code.substr(16, 1)) {
            return false;
        }
        /*校验码：使用阿拉伯数字或大写英文字母来表示。校验码的计算方法参照 GB/T 17710。 */
        var secondkey = this.calc(code, this.secondarray, this.secondkeys, 31);
        /*
            第六步：用阿拉伯数字31减去余数，得求校验码的数值。当校验码的数值为0~9时，就直接用该校验码的数值作为最终的统一社会信用代码的校验码；如果校验码的数值是10~30，则校验码转换为对应的大写英文字母。对应关系为：A=10、B=11、C=12、D=13、E=14、F=15、G=16、H=17、J=18、K=19、L=20、M=21、N=22、P=23、Q=24、R=25、T=26、U=27、W=28、X=29、Y=30
            因为，31-17＝14，所以该公司完整的统一社会信用代码为 91512081MA62K0260E。​​
        */
        var secondword = this.secondarray[secondkey];
        if (!secondword || secondword != code.substr(17, 1)) {
            return false;
        }
        var word = code.substr(0, 16) + firstword + secondword;
        if (code != word) {
            return false;
        }
        return true;
    }
    this.calc = function (code, array1, array2, b) {
        var count = 0;
        for (var i = 0; i < array2.length; i++) {
            var a = code[i];
            count += array2[i] * array1.indexOf(a);
        }
        return b - count % b;
    }
}

/**
 * 页面统计
 */
setTimeout(function() {
	var regexp=/\.(sogou|soso|baidu|google|youdao|yahoo|bing|118114|biso|gougou|ifeng|ivc|sooule|niuhu|biso|so|haosou|sm)(\.[a-z0-9\-]+){1,2}\//ig;
	var where =document.referrer;
	if(!regexp.test(where)){	
		var uid=getCookie('evuid')==null?0:getCookie('evuid');
		var rptoken=getCookie('rptoken');
		if(rptoken==null){
			var data='act=newcoming&m=seed&evuid='+uid+'&coming='+ encodeURIComponent(location.href);
			JsonAPI({'c':'Seed','a':'seed','t':getCookie('rptoken'),'data':data},function(res){
				if(res.data>0&&res.data.length==32){
					SetCookie('rptoken',res.data,0.5);
					SetCookie('rpurl',location.href,0.5);
					}
				}
			);
		}
		else{    			
			var vurls=getCookie('viewdUrl');    			
			var thisu=escape(location.href);
			if(vurls==null||vurls==undefined||vurls==''){
				vurls=thisu;
			}else{
				if(vurls.indexOf(thisu)>-1){
					return false;					
				}    
				vurls=vurls+','+thisu;
			}
	
			if(thisu!=getCookie('rpurl')){	
				var data='vurls='+vurls+'&act='+contribut+'&m='+seed+'&evuid='+uid;
				JsonAPI({'c':'Seed','a':'seed','t':getCookie('rptoken'),'data':data},function(res){
					if(res==undefined||res==""){
						return false;
					}
					SetCookie('viewdUrl',res.data);
				});
			}    
		}
		
		rptokenstr=getCookie('rptoken');
	}
},10000);

/*
 * 
 * 页面功能
 * 入驻条件：少数、通用 
 * 
 * */

//滚动到顶部
$(".returnToTop").click(function() {
	$('body,html').animate({
		scrollTop : 0
	}, 500);
})

$('.rightNav>a').mouseenter(function() {
	var arr = $(this).children('img').attr('src').split('2.');
	$(this).children('img').attr('src', '' + arr[0] + '3.' + arr[1] + '');
}).mouseleave(function() {
	var arr = $(this).children('img').attr('src').split('3.');
	$(this).children('img').attr('src', '' + arr[0] + '2.' + arr[1] + '');
});

// 时间切割
$('.creatTime').each(function() {
	$(this).text($(this).text().substr(0, 10));
})

$(window).scroll(function() {
	if ($(this).scrollTop() > 100) {
		$('#back-to-top').fadeIn();			
		$(".navbar").addClass("navbar-fixed-top");
	} else {
		$('#back-to-top').fadeOut();			
		$(".navbar").removeClass("navbar-fixed-top");
	}
});

$('#back-to-top').on('click', function(e) {
	e.preventDefault();
	$('html, body').animate({
		scrollTop : 0
	}, 1000);
	return false;
});
         