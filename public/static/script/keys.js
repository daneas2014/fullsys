﻿
function isEmail(str) {
    if (str.length > 100)
        return false;
    var regu = "^(([0-9a-zA-Z]+)|([0-9a-zA-Z]+[_.0-9a-zA-Z-]*[_.0-9a-zA-Z]+))@([a-zA-Z0-9-]+[.])+([a-zA-Z]{2}|net|NET|com|COM|gov|GOV|mil|MIL|org|ORG|edu|EDU|int|INT)$";
    var re = new RegExp(regu);
    if (str.search(re) != -1)
        return true;
    else
        return false;
}

function isMobile(str) {
    var s = new String(str);

    var regexp = "^1[3,5]{1}[0-9]{1}[0-9]{8}$";
    var re = new RegExp(regexp);
    if (s.search(re) != -1)
        return true;
    else
        return false;

}

function isTelphone(str) {
    var s = new String(str);

    var regexp ="(^[0-9]{3,4}\-[0-9]{3,8}$)|(^[0-9]{3,8}$)|(^\([0-9]{3,4}\)[0-9]{3,8}$)|(^0{0,1}13[0-9]{9}$)";
    var re = new RegExp(regexp);
    if (s.search(re) != -1)
        return true;
    else
        return false;
}

function isNumber(str) {
    var s = new String(str);

    var regexp = "^[0-9]*[1-9][0-9]*$";
    var re = new RegExp(regexp);
    if (s.search(re) != -1)
        return true;
    else
        return false;
}

function isYear(str) {
    var s = new String(str);

    var regexp = regexp = "^([1-2]\d{3})$";
    var re = new RegExp(regexp);
    if (s.search(re) != -1)
        return true;
    else
        return false;
}

function ltrim(s) {
    return s.replace(/^\s*/, "");
}


function rtrim(s) {
    return s.replace(/\s*$/, "");
}

//去掉空格
function trim(s) {
    return rtrim(ltrim(s));
}

function checkpassword(str) {
    var s = new String(str);

    var regexp =  "^[A-Za-z0-9]+$";
    var re = new RegExp(regexp);
    if (s.search(re) != -1)
        return true;
    else
        return false;
}
function isContainSensitiveWords(str) {
    var s = new String(str);
    //<script>
    var scriptRegexp = /.*<script.*?>.*?<\/script>/i;
    var re = new RegExp(scriptRegexp);
    if (s.search(re) != -1)
        return true;
    var phpRegexp = /.*<\?php.*/i;
    re = new RegExp(phpRegexp);
    if (s.search(re) != -1)
        return true;
    var iFrameRegexp = /.*<iframe.*/i;
    re = new RegExp(iFrameRegexp);
    if (s.search(re) != -1)
        return true;
    var otherRegexp = /.*<\%.*/i;
    re = new RegExp(otherRegexp);
    if (s.search(re) != -1)
        return true;
    return false;
}