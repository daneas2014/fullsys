var lunhui = {

	//成功弹出层
	success: function(message,url){
		layer.msg(message, {icon: 6,time:2000}, function(index){
            layer.close(index);
            window.location.href=url;
        });
	},

	// 错误弹出层
	error: function(message) {
        layer.msg(message, {icon: 5,time:2000}, function(index){
            layer.close(index);
        });       
    },

	// 确认弹出层
    confirm : function(id,url) {
        layer.confirm('确认删除此条记录吗?', {icon: 3, title:'提示'}, function(index){
	        $.getJSON(url, {'id' : id}, function(res){
	            if(res.code == 1){
	                layer.msg(res.msg,{icon:1,time:1500,shade: 0.1});
	                Ajaxpage()
	            }else{
	                layer.msg(res.msg,{icon:0,time:1500,shade: 0.1});
	            }
	        });
	        layer.close(index);
	    })
    },

    //状态
    status : function(id,url){
	    $.post(url,{id:id},function(data){	         
	        if(data.code==1){
	            var a='<span class="label label-danger">禁用</span>'
	            $('#zt'+id).html(a);
	            layer.msg(data.msg,{icon:2,time:1500,shade: 0.1,});
	            return false;
	        }else{
	            var b='<span class="label label-info">开启</span>'
	            $('#zt'+id).html(b);
	            layer.msg(data.msg,{icon:1,time:1500,shade: 0.1,});
	            return false;
	        }         	        
	    });
	    return false;
	}


}



function getQueryParam(name) {
	var query = window.location.search.substring(1);
	var vars = query.split("&");
	for (var i = 0; i < vars.length; i++) {
		var pair = vars[i].split("=");
		if (pair[0] == name) {
			return pair[1];
		}
	}
	return (false);
}

// start 外国时间格式化到中国时间
function padLeftZero(str) {
	return ('00' + str).substr(str.length);
}

function formatDate(date, fmt) {
	if (date == null || date == '') {
		return '';
	}
	if(typeof(date)=='string'&&date.length<12){
		return date;
	}
	if (/(y+)/.test(fmt)) {
		fmt = fmt.replace(RegExp.$1, (date.getFullYear() + '')
				.substr(4 - RegExp.$1.length));
	}
	let o = {
		'M+' : date.getMonth() + 1,
		'd+' : date.getDate(),
		'h+' : date.getHours(),
		'm+' : date.getMinutes(),
		's+' : date.getSeconds()
	};
	for ( let k in o) {
		if (new RegExp(`(${k})`).test(fmt)) {
			let str = o[k] + '';
			fmt = fmt.replace(RegExp.$1, (RegExp.$1.length === 1) ? str
					: padLeftZero(str));
		}
	}
	return fmt;
}
// end 外国时间格式化到中国时间

function getEarlyMonth(x){
	let d=new Date();
	return d.getFullYear()+"-"+(x?(d.getMonth()+x):d.getMonth())+"-1";
}
