// map.clearMap();
maps.forEach(m => {
    var icon = new AMap.Icon({
        size: new AMap.Size(64, 48),
        image: m.thumbnail,
        imageOffset: new AMap.Pixel(0, -60),
        imageSize: new AMap.Size(64, 48)
    });

    var mx = m.location.split(',');
    var marker = new AMap.Marker({
        map: map,
        title: m.description,
        //icon: icon,
        label: {
            content: m.name,
            offset: [0, 5],
            direction: 'left'
        },
        position: [mx[0], mx[1]],
        extData: m,
        clickable: true
    });
    //鼠标点击marker弹出自定义的信息窗体
    marker.on('click', function (e) {
        var t = e.target._originOpts.extData;
        var title = t.name+'<span style="font-size:11px;color:#F00;">特色:'+t.points+'</span>';
        var content = [];
        content.push("<img src='"+t.thumbnail+"' width='240'>地址："+t.address);
        content.push("电话："+t.mobel);
        content.push("<a href='{:addon_url('mapx://index/show')}?id="+t.id+"'>详细信息</a>");

        var infoWindow = new AMap.InfoWindow({
            isCustom: true,  //使用自定义窗体
            content: createInfoWindow(title, content.join("<br/>")),
            offset: new AMap.Pixel(16, -45)
        });

        infoWindow.open(map, marker.getPosition());
    });
});

//缩略，把所有点都包含
map.setFitView(null, false, [100, 150, 10, 10]); 

// 缩放工具
AMap.plugin(['AMap.ToolBar'], function(){ 
    map.addControl(new AMap.ToolBar()); 
}); 

//构建自定义信息窗体
function createInfoWindow(title, content) {
    var info = document.createElement("div");
    info.className = "custom-info input-card content-window-card";

    //可以通过下面的方式修改自定义窗体的宽高
    info.style.width = "400px";
    // 定义顶部标题
    var top = document.createElement("div");
    var titleD = document.createElement("div");
    var closeX = document.createElement("img");
    top.className = "info-top";
    titleD.innerHTML = title;
    closeX.src = "https://webapi.amap.com/images/close2.gif";
    closeX.onclick = closeInfoWindow;

    top.appendChild(titleD);
    top.appendChild(closeX);
    info.appendChild(top);

    // 定义中部内容
    var middle = document.createElement("div");
    middle.className = "info-middle";
    middle.style.backgroundColor = 'white';
    middle.innerHTML = content;
    info.appendChild(middle);

    // 定义底部内容
    var bottom = document.createElement("div");
    bottom.className = "info-bottom";
    bottom.style.position = 'relative';
    bottom.style.top = '0px';
    bottom.style.margin = '0 auto';
    var sharp = document.createElement("img");
    sharp.src = "https://webapi.amap.com/images/sharp.png";
    bottom.appendChild(sharp);
    info.appendChild(bottom);
    return info;
}

//关闭信息窗体
function closeInfoWindow() {
    map.clearInfoWindow();
}


/* 区域描点 见下方community、district文件
<script src="https://a.amap.com/jsapi_demos/static/data/community.js"></script>
<script src="https://a.amap.com/jsapi_demos/static/data/district.js"></script>
*/
        
var clusterIndexSet = {
    city: {
        minZoom: 2,
        maxZoom: 10,
    },
    district: {
        minZoom: 10,
        maxZoom: 12,
    },
    area: {
        minZoom: 12,
        maxZoom: 15,
    },
    community: {
        minZoom: 15,
        maxZoom: 22,
    },
};
function getStyle(context) {
    var clusterData = context.clusterData; // 聚合中包含数据
    var index = context.index; // 聚合的条件
    var count = context.count; // 聚合中点的总数
    var marker = context.marker; // 聚合绘制点 Marker 对象
    var color = [
        '8,60,156',
        '66,130,198',
        '107,174,214',
        '78,200,211',
    ];
    var indexs = ['city','district','area','community'];
    var i = indexs.indexOf(index['mainKey']);
    var text = clusterData[0][index['mainKey']];
    var size = Math.round(30 + Math.pow(count / points.length, 1 / 5) * 70);
    if(i <= 2){
        var extra = '<span class="showCount">'+ context.count +'处</span>';
        text = '<span class="showName">'+ text +'</span>';
        text += extra;
    } else {
        size = 12 * text.length + 20;
    }
    var style = {
        bgColor: 'rgba(' + color[i] + ',.8)',
        borderColor: 'rgba(' + color[i] + ',1)',
        text: text,
        size: size,
        index: i,
        color: '#ffffff',
        textAlign: 'center',
        boxShadow: '0px 0px 5px rgba(0,0,0,0.8)'
    }
    return style;
}
function getPosition(context) {
    var key = context.index.mainKey;
    var dataItem = context.clusterData && context.clusterData[0];
    var districtName = dataItem[key];
    if(!district[districtName]) {
        return null;
    }
    var center = district[districtName].center.split(',');
    var centerLnglat = new AMap.LngLat(center[0], center[1]);
    return centerLnglat;
}
function _customRender (data) {
    const keys = Object.keys(data.clusterData);
    let markers = [];
    for (var i = 0; i < keys.length; i++) {
        var key = keys[i];
        var cluster = data.clusterData[key];
        var position = cluster.data[0].lnglat;
        var marker = new AMap.LabelMarker({
            position: position,
            text: {
                content: cluster.data.length.toString(),
                style: {
                    fillColor: '#ffffff'
                }
            }
        });
        markers.push(marker)
    }
    return {
        type: 'type',
        layer: null,
        markers: markers,
    };
}
// 自定义聚合点样式
function _renderClusterMarker (context) {
    var clusterData = context.clusterData; // 聚合中包含数据
    var index = context.index; // 聚合的条件
    var count = context.count; // 聚合中点的总数
    var marker = context.marker; // 聚合点标记对象
    var styleObj = getStyle(context);
    // 自定义点标记样式
    var div = document.createElement('div');
    div.className = 'amap-cluster';
    div.style.backgroundColor = styleObj.bgColor;
    div.style.width = styleObj.size + 'px';
    if(styleObj.index <= 2) {
        div.style.height = styleObj.size + 'px';
        // 自定义点击事件
        context.marker.on('click', function(e) {
            console.log(e)
            var curZoom = map.getZoom();
            if(curZoom < 20){
                curZoom += 1;
            }
            map.setZoomAndCenter(curZoom, e.lnglat);
        });
    }
    div.style.border = 'solid 1px ' + styleObj.borderColor;
    div.style.borderRadius = styleObj.size + 'px';
    div.innerHTML = styleObj.text;
    div.style.color = styleObj.color;
    div.style.textAlign = styleObj.textAlign;
    div.style.boxShadow = styleObj.boxShadow;
    context.marker.setContent(div)
    // 自定义聚合点标记显示位置
    var position = getPosition(context);
    if(position){
        context.marker.setPosition(position);
    }
    context.marker.setAnchor('center');

};
 
//根据行政区划生成数据统计点
function initPoints(){
    var cluster = new AMap.IndexCluster(map, points, {
        renderClusterMarker: _renderClusterMarker,
        clusterIndexSet: clusterIndexSet,
    });
}