<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

// [ 应用入口文件 ]
// 定义应用目录
define('APP_PATH', __DIR__ . '/../application/');
define('CONF_PATH', __DIR__ . '/../config/');
define('EXTEND_PATH', __DIR__ . '/../extend/');
define('VENDOR_PATH', __DIR__ . '/../vendor/');
define('BASEINIT_PATH', __DIR__ . '/../BaseFormInit');

// 缓存键值，避免冲突
define('CACHE_AD', 'CACHE_AD_');
define('CACHE_ARTICLE', 'CACHE_ARTICLE_');
define('CACHE_COMMODITY', 'CACHE_COMMODITY_');
define('CACHE_FORUM', 'CACHE_FORUM_');
define('CACHE_VIDEO', 'CACHE_VIDEO_');
define('CACHE_LICENCE', 'CACHE_LICENCE_');
define('CACHE_PACKAGE', 'CACHE_PACKAGE_');
define('CACHE_SUPLERS', 'CACHE_SUPLERS_');
define('CACHE_MALL', 'CACHE_MALL_');
define('CACHE_SEARCH', 'CACHE_SEARCH_');
define('CACHE_SITES', 'CACHE_SITES_');
define('CACHE_SITES_A', 'CACHE_SITES_A_');
define('CACHE_ADMIN', 'CACHE_ADMIN_');
define('CACHE_API', 'CACHE_API_');

// 本系统所有的自定义cache key定义，屁股后面要把appid加上
define('CKEY_WX_TOKEN', 'openflat_wx_token');
define('CKEY_WX_REFRESHTOKEN', 'openflat_wx_refreshtoken');
define('CKEY_WX_CONFIG', 'openflat_wx_config');

// 网站设置
define('SITE_NAME', '板砖博客');
define('SITE_DOMAIN', 'https://www.dmake.cn');
define('AUTHURL', '/ucenter/uauth/login');

//借用别人的微信授权
//define('WX_AUTH_DOMAIN','https://www.dmake.cn/');
define('WX_AUTH_DOMAIN', 'http://wwww.dmake.cn/');
define('WX_AUTHSPLIT', '_');

ini_set("display_errors", "On");//打开错误提示
ini_set("error_reporting",E_ALL);//显示所有错误

if (!defined("TOP_SDK_WORK_DIR")) {
	define("TOP_SDK_WORK_DIR", "/tmp/");
}

define('WXCERT_PATH', __DIR__ . '/../addons/wechat/cert/');

// 屏蔽IP
$ban = include "banip.php";
if ($_SERVER["REMOTE_ADDR"] && $ban != []) {
	foreach ($ban as $b) {
		if ($b == $_SERVER["REMOTE_ADDR"]) {
			die("您的IP已被禁止，如需访问请咨询管理员");
		}
	}
}

// 加载框架引导文件
require __DIR__ . '/../thinkphp/start.php';