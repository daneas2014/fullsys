<?php
namespace org;

use PSCWS4;
use PhpAnalysis;

/*
 * 找到当前目录绝对路径
 */
define('KROOT', str_replace('\\', '/', dirname(__FILE__)));

/*
 * 文章关键词分析
 */
class Kwass
{

    public function get_tags_arr($title)
    {
        require ('scwskw/pscws4.class.php');
        $pscws = new PSCWS4();
        $pscws->set_dict(KROOT . '/scwskw/scws/dict.utf8.xdb');
        $pscws->set_rule(KROOT . '/scwskw/scws/rules.utf8.ini');
        $pscws->set_ignore(true);
        $pscws->send_text($title);
        $words = $pscws->get_tops(5);
        $tags = array();
        foreach ($words as $val) {
            $tags[] = $val['word'];
        }
        $pscws->close();
        return $tags;
    }

    public function get_keywords_str($content)
    {
        require ('scwskw/phpanalysis.class.php');
        PhpAnalysis::$loadInit = true;
        $pa = new PhpAnalysis('utf-8', 'utf-8', true);
        $pa->LoadDict();
        $pa->SetSource($content);
        $pa->StartAnalysis(true);
        $tags = $pa->GetFinallyResult();
        return $tags;
    }
}