<?php

namespace com;

use \think\Db;

/**
 * 这个类主要用于快速管理网站后台，利用vue做清单页、单表保存
 * @author Daneas
 *
 */
class Fastadmin extends \think\Controller {

	/**
	 * 适用于单表查询，有id,title,keywords,description的表，前端用VUE展示
	 *
	 * @param unknown $db
	 * @param array $map
	 * @param array $orderby
	 * @param $assign 传入要渲染的参数
	 * @param string $fields 要展示的字段
	 * @return \think\response\Json|unknown
	 */
	public function vueQuerySingle($db, $map = [], $orderby = '', $assign = [], $fields = null) {

		if ($orderby == '' || $orderby == null) {
			$orderby = 'id desc';
		}

		$parentid = input('get.parentid') ? input('get.parentid') : 0;
		$this->assign('parentid', $parentid);

		if ($map == []) {
			$key = input('key');
			if ($key && $key !== "") {
				$map['title|keywords|description'] = ['like', "%" . $key . "%"];
			}
		}

		$Nowpage = input('get.page') ? input('get.page') : 1;
		$limits = input('get.rows') ? input('get.rows') : config('list_rows'); // 获取总条数

		if ($fields !== null) {
			$list = $db->where($map)
				->page($Nowpage, $limits)
				->order($orderby)
				->field($fields)
				->paginate($limits);
		} else {
			$list = $db->where($map)
				->page($Nowpage, $limits)
				->order($orderby)
				->paginate($limits);
		}

		$data['list'] = $list->items();
		$data['count'] = $list->total();
		$data['page'] = $Nowpage;

		if (input('get.page')) {
			return json($data);
		}

		return $this->view->assign($assign)->fetch();
	}

	/**
	 * 双表left join查询
	 *
	 * @param [type] 表1名称 alias a
	 * @param [type] 表2名称 alias b
	 * @param array $map
	 * @param string left join条件
	 * @param string $orderby
	 * @param array $assign
	 * @param [type] $fields
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2024-02-27
	 */
	public function vueQueryDouble($table1, $table2, $map = [], $joincondition, $orderby = '', $assign = [], $fields = null) {

		if ($orderby == '' || $orderby == null) {
			$orderby = 'id desc';
		}

		$parentid = input('get.parentid') ? input('get.parentid') : 0;
		$this->assign('parentid', $parentid);

		if ($map == []) {
			$key = input('key');
			if ($key && $key !== "") {
				$map['title|keywords|description'] = ['like', "%" . $key . "%"];
			}
		}

		$Nowpage = input('get.page') ? input('get.page') : 1;
		$limits = input('get.rows') ? input('get.rows') : config('list_rows'); // 获取总条数

		if ($fields !== null) {
			$list = Db::name($table1)
				->join($table2, $joincondition, 'left')
				->where($map)
				->page($Nowpage, $limits)
				->order($orderby)
				->field($fields)
				->paginate($limits);
		} else {
			$list = Db::name($table1)->alias('a')
				->join($table2 . 'as b', 'left')
				->where($map)
				->page($Nowpage, $limits)
				->order($orderby)
				->paginate($limits);
		}

		$data['list'] = $list->items();
		$data['count'] = $list->total();
		$data['page'] = $Nowpage;

		if (input('get.page')) {
			return json($data);
		}

		return $this->view->assign($assign)->fetch();
	}

	/**
	 * 适用于单表保存，主键为id的表,有update_time,create_time
	 *
	 * @param [type] $db
	 * @param [type] $data
	 * @param string $dbname
	 * @param array $assign
	 * @param array $ignorefileds 需要排除的字段
	 * @param array $ign_iu 是否需要排除uid、update_time
	 * @param [type] $pkfield 默认是id，非默认就要自己来写了
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2021-01-21
	 */
	public function singleDataSave($db, $data, $dbname = '', $assign = [], $ignorefileds = [], $ign_iu = [false, false], $pkfield = 'id') {

		if (request()->isPost()) {
			// 因为引入了很多子系统，主要是admin和iot，session要加前缀区别一下
			$uid = session('uid', '', 'admin') ? session('uid', '', 'admin') : session('uid', '', 'iot');
			$username = session('username', '', 'admin') ? session('username', '', 'admin') : session('username', '', 'iot');

			$act = $data['act'];
			$id = $data[$pkfield];

			if (array_key_exists('act', $data)) {
				unset($data['act']);
			}
			if (array_key_exists('id', $data)) {
				unset($data['id']);
			}
			if (array_key_exists('file', $data)) {
				unset($data['file']);
			}

			if ($ign_iu != [] && $ign_iu[0] === false) {
				$data['adminid'] = $uid;
			}

			if ($ignorefileds) {
				foreach ($ignorefileds as $i) {
					unset($data[$i]);
				}
			}

			if ($act == 'del') {
				$result = $db->where($pkfield, $id)->delete();
			}

			if ($act == 'edit' && $id && $id > 0 && !isset($data['update_time'])) {
				if ($ign_iu != [] && $ign_iu[1] === false) {
					$data['update_time'] = time();
				}
				$result = $db->where($pkfield, $id)->update($data);
			}

			if (($act == 'edit' || $act == 'new') && ($id == 0 || $id == null)) {
				if (!isset($data['create_time'])) {
					$data['create_time'] = time();
				}
				$result = $db->insertGetId($data);
			}

			writelog($uid, $username, "用户【 $username  |  $uid 】$act 了表【 $dbname 】记录: $result ", 1);

			if ($result) {
				return json(['code' => 1, 'data' => $result, 'msg' => '操作成功']);
			}

			return json(['code' => -1, 'data' => $result, 'msg' => '操作失败']);
		}

		$id = input('param.id');

		$assign['data'] = $id > 0 ? $db->find([$pkfield => $id]) : null;

		// 转义html代码，不然后端显示不出来
		if ($assign['data'] && isset($assign['data']['detail'])) {
			$assign['data']['detail'] = $this->foramp($assign['data']['detail']);
		} else if ($assign['data'] && isset($assign['data']['content'])) {
			$assign['data']['content'] = $this->foramp($assign['data']['content']);
		} else if ($assign['data'] && isset($assign['data']['summary'])) {
			$assign['data']['summary'] = $this->foramp($assign['data']['summary']);
		}

		return $this->view->assign($assign)->fetch();
	}

	/**
	 * html转义
	 *
	 * @param string $html
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2021-01-21
	 */
	private function foramp($html = '') {

		if ($html == null || $html == "") {
			return "";
		}

		$html = str_replace('&nbsp;', '&amp;nbsp;', $html);
		$html = str_replace('&gt;', '&amp;gt;', $html);
		$html = str_replace('&lt;', '&amp;lt;', $html);

		return $html;
	}
}
