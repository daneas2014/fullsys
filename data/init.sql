/*
 Navicat Premium Data Transfer

 Source Server         : local'host
 Source Server Type    : MySQL
 Source Server Version : 50726
 Source Host           : localhost:3306
 Source Schema         : fullsys

 Target Server Type    : MySQL
 Target Server Version : 50726
 File Encoding         : 65001

 Date: 17/05/2022 15:17:39
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for addons
-- ----------------------------
DROP TABLE IF EXISTS `addons`;
CREATE TABLE `addons`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '插件名或标识',
  `title` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '中文名',
  `description` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '插件描述',
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '状态',
  `config` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '配置',
  `author` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '作者',
  `version` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '版本号',
  `create_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '安装时间',
  `has_adminlist` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否有后台列表',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 58 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '插件表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of addons
-- ----------------------------

-- ----------------------------
-- Table structure for advertisment_bits
-- ----------------------------
DROP TABLE IF EXISTS `advertisment_bits`;
CREATE TABLE `advertisment_bits`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `adid` int(11) NOT NULL,
  `ip` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `customerid` int(11) NULL DEFAULT NULL,
  `create_time` int(11) NOT NULL,
  `jumpurl` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 80 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of advertisment_bits
-- ----------------------------

-- ----------------------------
-- Table structure for advertisment_position
-- ----------------------------
DROP TABLE IF EXISTS `advertisment_position`;
CREATE TABLE `advertisment_position`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pkey` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `changetype` int(11) NOT NULL COMMENT '0：创建时间最新 1 创建时间最晚  2随机 3 轮换',
  `description` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `type` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `size` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_time` int(11) NULL DEFAULT NULL,
  `update_time` int(11) NULL DEFAULT NULL,
  `adminid` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 119 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of advertisment_position
-- ----------------------------
INSERT INTO `advertisment_position` VALUES (42, 'search-foot-ad', 'search-foot-ad', 0, '搜索结果页底部', '图片', '850x100', NULL, NULL, 0);
INSERT INTO `advertisment_position` VALUES (44, 'index-a3', 'Index-A3', 0, '首页中部横幅', '图片', '1200*100', NULL, NULL, 0);
INSERT INTO `advertisment_position` VALUES (45, 'index-a4', 'Index-A4', 0, '首页中部横幅下左侧第一个', '图片', '218*257', NULL, NULL, 0);
INSERT INTO `advertisment_position` VALUES (46, 'index-a5', 'Index-A5', 0, '首页中部横幅下左侧第二个', '图片', '218*257', NULL, NULL, 0);
INSERT INTO `advertisment_position` VALUES (47, 'product-a1', 'Product-A1', 0, '产品页顶部轮播', '轮播', '795*380', NULL, NULL, 0);
INSERT INTO `advertisment_position` VALUES (48, 'product-a2', 'Product-A2', 0, '产品页产品推荐左侧广告', '图片', '210*420', NULL, NULL, 0);
INSERT INTO `advertisment_position` VALUES (49, 'product-a3', 'Product-A3', 0, '产品页热门产品左侧图片广告', '图片', '278*209', NULL, NULL, 0);
INSERT INTO `advertisment_position` VALUES (50, 'product-a4', '产品首页新厂商/产品推荐', 0, '产品页新商家推荐,375X368,188X183', '图片', '375X368', NULL, NULL, 0);
INSERT INTO `advertisment_position` VALUES (51, 'product-a5', 'Product-A5', 0, '产品页中部横幅', '图片', '1200*100', NULL, NULL, 0);
INSERT INTO `advertisment_position` VALUES (53, 'product-a6', 'Product-A6', 0, '产品页轮播右侧', '图片', '160*160', NULL, NULL, 0);
INSERT INTO `advertisment_position` VALUES (59, 'productdetail-ad', '产品页详情广告', 0, '', '图片广告', '850X100', NULL, NULL, 0);
INSERT INTO `advertisment_position` VALUES (60, 'tagstoadd', '产品匹配活动', 0, '本广告位用于具有同一个属性的广告和产品', '图片', '875*100px', NULL, NULL, 0);
INSERT INTO `advertisment_position` VALUES (71, 'campaigns-index', '活动聚合页广告', 0, '这个拿来放轮播图，品牌宣传类的', '轮播', '1200*360', NULL, NULL, 0);
INSERT INTO `advertisment_position` VALUES (73, 'common-top', '页面导航通用广告', 0, '全站通用页面导航通栏广告', '图片', '1200*100', NULL, NULL, 0);
INSERT INTO `advertisment_position` VALUES (74, 'topmenu-bbs1', '主导航社区广告1', 0, '网站社区预留广告位', '图片', '500*200', NULL, NULL, 0);
INSERT INTO `advertisment_position` VALUES (75, 'topmenu-bbs2', '主导航社区广告2', 0, '网站社区预留广告位', '图片', '500*200', NULL, NULL, 0);
INSERT INTO `advertisment_position` VALUES (76, 'product-index-note', '产品首页顶部右侧公告', 0, '', '文字', '', NULL, NULL, 0);
INSERT INTO `advertisment_position` VALUES (77, 'mall-a1', '商城首页顶部广告', 0, '', '轮播', '1920*500', NULL, NULL, 0);
INSERT INTO `advertisment_position` VALUES (78, 'common-390', '通用右侧广告', 0, '营销活动或重点推广适用', '图片', '390宽', NULL, NULL, 0);
INSERT INTO `advertisment_position` VALUES (79, 'menu-a1', '主菜单高端定制', 0, '', '图片', '', NULL, NULL, 0);
INSERT INTO `advertisment_position` VALUES (80, 'mall-top', '在线订购顶部广告', 0, '在线订购版块顶部全部展示', '图片', '1200*100', NULL, NULL, 0);
INSERT INTO `advertisment_position` VALUES (93, 'mall-1', '商城分类1', 0, '', '图片', '200*375', NULL, NULL, 0);
INSERT INTO `advertisment_position` VALUES (94, 'mall-2', '商城分类2', 0, '', '图片', '200*375', NULL, NULL, 0);
INSERT INTO `advertisment_position` VALUES (95, 'mall-3', '商城分类3', 0, '', '图片', '200*375', NULL, NULL, 0);
INSERT INTO `advertisment_position` VALUES (96, 'mall-4', '商城分类4', 0, '', '图片', '200*375', NULL, NULL, 0);
INSERT INTO `advertisment_position` VALUES (111, 'poprbp', '产品页右下角弹窗', 0, '', '弹窗', '250px', NULL, NULL, 0);
INSERT INTO `advertisment_position` VALUES (112, 'poprba', '文章资讯右下角弹窗', 0, '', '弹窗', '250px', NULL, 1568966784, 0);
INSERT INTO `advertisment_position` VALUES (117, 'forum-right', '论坛右侧单图广告', 0, '', '图片', '', 1574326851, NULL, 1);
INSERT INTO `advertisment_position` VALUES (118, 'minindex', '小程序首页', 0, '小程序首页轮播', '轮播', '414*187', 1575292943, NULL, 1);

-- ----------------------------
-- Table structure for advertisments
-- ----------------------------
DROP TABLE IF EXISTS `advertisments`;
CREATE TABLE `advertisments`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `summary` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `title` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `begintime` datetime(0) NOT NULL,
  `endtime` datetime(0) NOT NULL,
  `createdtime` datetime(0) NOT NULL,
  `imagepath` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `isdisabled` tinyint(4) NOT NULL,
  `type` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `url` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `imgalt` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `positionid` int(11) NOT NULL,
  `bits` int(11) NOT NULL,
  `sort` int(11) NULL DEFAULT NULL,
  `create_time` int(11) NULL DEFAULT NULL,
  `update_time` int(11) NULL DEFAULT NULL,
  `adminid` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of advertisments
-- ----------------------------
INSERT INTO `advertisments` VALUES (1, ' ', '又拍云', '2019-10-29 00:00:00', '2020-11-10 00:00:00', '0000-00-00 00:00:00', 'https://o4jiepyc4.qnssl.com/static/promotion_cdn_sdk-60a28fdf4b56abc70709498f21424aa3.png', 0, NULL, 'https://portal.qiniu.com/signup?code=1hm7kzbx0yr0y', NULL, 117, 21, 0, 1574326924, NULL, 1);
INSERT INTO `advertisments` VALUES (2, '', '免费的萤火小程序上可以怎么样和企业官网和电商网站融合？ ', '2019-12-02 00:00:00', '2023-12-05 00:00:00', '0000-00-00 00:00:00', 'http://cdn.dmake.cn/attachment/images/20191112/15735586998.png', 0, NULL, '/pages/article/detail?id=41', NULL, 118, 0, 0, 1575293061, NULL, 1);
INSERT INTO `advertisments` VALUES (3, ' ', '阿里云服务器', '2019-12-12 00:00:00', '2020-11-01 00:00:00', '0000-00-00 00:00:00', 'http://cdn.dmake.cn/attachment/images/20191212/157611908610.jpg', 0, NULL, 'https://s.click.taobao.com/0cjnHtv', NULL, 78, 58, 0, 1576119019, 1576119088, 1);

-- ----------------------------
-- Table structure for article
-- ----------------------------
DROP TABLE IF EXISTS `article`;
CREATE TABLE `article`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `categoryid` int(11) NOT NULL,
  `creativetype` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `imagepath` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `title` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `subtitle` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `detail` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `summary` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `keywords` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `groupid` int(11) NULL DEFAULT NULL,
  `ispublish` tinyint(4) NOT NULL,
  `isheadline` tinyint(4) NOT NULL,
  `headlinetime` datetime(0) NULL DEFAULT NULL,
  `original` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `originalpath` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `adminid` int(11) NULL DEFAULT NULL,
  `create_time` int(11) NULL DEFAULT NULL,
  `update_time` int(11) NULL DEFAULT NULL,
  `scroll` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `IX_Article_is_push`(`ispublish`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 87 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of article
-- ----------------------------
INSERT INTO `article` VALUES (2, 3, '原创', '//cdn.dmake.cn/attachment/images/20191024/15718956682.jpg', '文章模块分类导航自定义', NULL, '<p>博客建站第一步，完善网站资讯分类和产品分类，在此模块中注意，名称和SEO标题并不是一个维度的内容，SEO标题、简介与关键词会极大的影响分类页的收录排名。</p><p><img src=\"http://cdn.dmake.cn/attachment/upload/201910/24/15718957131527.png\" style=\"\"/></p><p><img src=\"http://cdn.dmake.cn/attachment/upload/201910/24/15718957179353.png\" style=\"\"/></p><p><br/></p>', '博客建站第一步，完善网站资讯分类和产品分类，在此模块中注意，名称和SEO标题并不是一个维度的内容，SEO标题、简介与关键词会极大的影响分类页的收录排名。', '文章模块，分类导航，自定义导航', 0, 1, 0, NULL, '', '', 1, 1570719660, 1571902269, 0);
INSERT INTO `article` VALUES (3, 3, '原创', '//cdn.dmake.cn/attachment/images/20191024/15719186849.jpg', '按模块划分独立频道，频道既子站点', NULL, '<p>板砖博客多模块建站，除了建设平台型网站，更能根据模块配置形成独立子站点，运营可大可小。</p><p>板砖博客平台系统是基于前后端分离的理念开发而来，经过<strong>门户网站、微信公众平台、资讯网站、资源网站和企业站群</strong>等多种运营模式和建站模式产品化，方便更多的个人站长和企业能够快速建立互联网品牌。</p><p>在config.php文件中注入下方代码，在服务器中再次配置域名到系统根目录，即可完成模块变子站、二级站点的功能，同理可以达到站群效果。</p><p>&nbsp; &nbsp; // +----------------------------------------------------------------------</p><p>&nbsp; &nbsp; // | 多模块绑定域名</p><p>&nbsp; &nbsp; // +----------------------------------------------------------------------</p><p>&nbsp; &nbsp; \\think\\Route::domain([</p><p>&nbsp; &nbsp; &nbsp; &nbsp; &#39;域名&#39; =&gt; &#39;模块&#39;,</p><p>&nbsp; &nbsp; ]),</p><p><br/></p>', '板砖博客多模块建站，除了建设平台型网站，更能根据模块配置形成独立子站点，运营可大可小。', '二级域名，子站点', 0, 1, 0, NULL, '', '', 1, 1570719674, 1571920385, 0);
INSERT INTO `article` VALUES (4, 3, '原创', '//cdn.dmake.cn/attachment/images/20191024/15719034068.jpg', '针对搜索引擎友好的SEO体验', NULL, '<p>板砖博客内置百度链接提交工具，可手动、自动提交链接，并生成sitemap文件。</p><p>使用该功能需要系统管理员自行申请百度站长账号，并获得配置链接，再集成至系统即可。</p><p>一、 账号申请并获得提交权限</p><p>登录百度站长（ziyuan.baidu.com） &gt; 数据引入 &gt;链接提交 &gt; 找到例如“<span style=\"color: rgb(102, 102, 102); font-family: tahoma, PingFangSC, &quot;microsoft yahei ui&quot;, &quot;microsoft yahei&quot;, sans-serif; font-size: 14px; white-space: normal;\">http://data.zz.baidu.com/</span><span class=\"ping-path\" style=\"color: rgb(102, 102, 102); font-family: tahoma, PingFangSC, &quot;microsoft yahei ui&quot;, &quot;microsoft yahei&quot;, sans-serif; font-size: 14px; white-space: normal;\">urls</span><span style=\"color: rgb(102, 102, 102); font-family: tahoma, PingFangSC, &quot;microsoft yahei ui&quot;, &quot;microsoft yahei&quot;, sans-serif; font-size: 14px; white-space: normal;\">?</span><span class=\"emphasis\" style=\"color: rgb(249, 93, 93); font-family: tahoma, PingFangSC, &quot;microsoft yahei ui&quot;, &quot;microsoft yahei&quot;, sans-serif; font-size: 14px; white-space: normal;\">site</span><span style=\"color: rgb(102, 102, 102); font-family: tahoma, PingFangSC, &quot;microsoft yahei ui&quot;, &quot;microsoft yahei&quot;, sans-serif; font-size: 14px; white-space: normal;\">=</span><span class=\"post-site\" style=\"color: rgb(102, 102, 102); font-family: tahoma, PingFangSC, &quot;microsoft yahei ui&quot;, &quot;microsoft yahei&quot;, sans-serif; font-size: 14px; white-space: normal;\">https://www.</span><span style=\"color: rgb(102, 102, 102); font-family: tahoma, PingFangSC, &quot;microsoft yahei ui&quot;, &quot;microsoft yahei&quot;, sans-serif; font-size: 14px; white-space: normal;\">&amp;</span><span class=\"emphasis\" style=\"color: rgb(249, 93, 93); font-family: tahoma, PingFangSC, &quot;microsoft yahei ui&quot;, &quot;microsoft yahei&quot;, sans-serif; font-size: 14px; white-space: normal;\">token</span><span style=\"color: rgb(102, 102, 102); font-family: tahoma, PingFangSC, &quot;microsoft yahei ui&quot;, &quot;microsoft yahei&quot;, sans-serif; font-size: 14px; white-space: normal;\">=</span><span class=\"post-token\" style=\"color: rgb(102, 102, 102); font-family: tahoma, PingFangSC, &quot;microsoft yahei ui&quot;, &quot;microsoft yahei&quot;, sans-serif; font-size: 14px; white-space: normal;\">xxxx</span>”的路径，将token记录并配置到config·php文件。</p><p>二、生成sitemap文件和提交链接</p><p>登录博客后端，在“系统管理&gt;站长地图”中选择需要生成地图的月份，点击“生成地图”即可，在此过程中本月产生的链接也会同步推送到百度中建立索引。</p><p>另外如果需要单独对某一个链接进行提交，在“系统管理&gt;百度推送”中复制粘贴链接，点击提交即可，如果有多个链接，用西文“，”分割。</p>', '板砖博客内置百度链接提交工具，可手动、自动提交链接，并生成sitemap文件。', 'SEO体验', 0, 1, 0, NULL, '', '', 1, 1570719685, 1571903811, 0);
INSERT INTO `article` VALUES (5, 3, '原创', '//cdn.dmake.cn/attachment/images/20191024/15719193641.jpg', 'Thinkphp路由方式伪静态以及URL方法动态域名自由切换', NULL, '<p>因本系统是基于Thinkphp5拓展而成，因此在规范内，平台建设者可以根据建设情况自由调整URL规则。</p><p>路由定义方法：在router.php中加入路由规则，以下为实际路由情况。</p><p>请注意下方写法：当路由中存在相同开头的地址时，参数数量多的需要放在前面，因为按照后读取后执行的原则，如果顺序放反了那么程序将读取不到参数。</p><p><span style=\"white-space: nowrap;\"><br/></span></p><p><span style=\"white-space: nowrap;\">&nbsp; &nbsp; &#39;commodity/:id/:act/:uid&#39;=&gt;[&#39;detail/index&#39;,[&#39;method&#39;=&gt;&#39;get&#39;],[&#39;id&#39;=&gt;&#39;\\d+&#39;,&#39;act&#39;=&gt;&#39;\\w+&#39;,&#39;uid&#39;=&gt;&#39;\\d+&#39;]],</span></p><p><span style=\"white-space: nowrap;\">&nbsp; &nbsp; &#39;commodity/:id/:act&#39;=&gt;[&#39;detail/index&#39;,[&#39;method&#39;=&gt;&#39;get&#39;],[&#39;id&#39;=&gt;&#39;\\d+&#39;,&#39;act&#39;=&gt;&#39;\\w+&#39;]],</span></p><p><span style=\"white-space: nowrap;\">&nbsp; &nbsp; &#39;commodity/:id&#39;=&gt;[&#39;detail/index&#39;,[&#39;method&#39;=&gt;&#39;get&#39;],[&#39;id&#39;=&gt;&#39;\\d+&#39;]],</span></p>', '因本系统是基于Thinkphp5拓展而成，因此在规范内，平台建设者可以根据建设情况自由调整URL规则。', 'Thinkphp路由', 0, 1, 1, NULL, '', '', 1, 1570719700, 1571919826, 0);
INSERT INTO `article` VALUES (6, 3, '原创', '//cdn.dmake.cn/attachment/images/20191024/15719200301.jpg', 'PC&微信公众号&小程序一站式运营平台建设思路', NULL, '<p>根据模块分离和统一API接口的方式，无缝开发网站模块和微信公众号、小程序体系前后端数据融合以及调度问题。</p><p>在浅层次的系统磨合中，PC端后台主要管理公众号、小程序的基本配置和数据等，如菜单、会员信息等，在基础的配置成型的情况下，可以利用Easywechat SDK完成基于网页的互动，基于PC端的统一API，以及小程序用户openid完成小程序与PC端的数据交互和统一。</p>', '根据模块分离和统一API接口的方式，无缝开发网站模块和微信公众号、小程序体系前后端数据融合以及调度问题。', '运营平台建设', 0, 1, 0, NULL, '', '', 1, 1570719717, 1571920224, 1);
INSERT INTO `article` VALUES (7, 3, '原创', '//cdn.dmake.cn/attachment/images/20191024/15719208431.jpg', '5重会员等级提升会员交互率', NULL, '<p>板砖博客预设5重会员等级，约束会员行为：第三方登录、手机号注册登录、完善个人信息、行为积分等，提升会员等级。</p><p>会员等级是网站会员体系中最基础的环节，用于为用户提供不同权益下的服务等级和内容。平台方可以通过不同行为增设等级、自动升级，完善自己的会员营销体系。</p><p><br/></p><p><br/></p>', '5重会员等级，约束会员行为：第三方登录、手机号注册登录、完善个人信息、行为积分等，提升会员等级。', '会员等级', 0, 1, 0, NULL, '', '', 1, 1570719728, 1571920992, 0);
INSERT INTO `article` VALUES (8, 3, '原创', '//cdn.dmake.cn/attachment/images/20191024/15719211888.jpg', '记录会员行为轨迹，为会员提供定制化内容推送', NULL, '<p>板砖博客预设行为轨迹接口，完整记录会员行为轨迹，用数据分析会员关注点，精准定位会员画像，为会员提供精准服务。</p>', '板砖博客预设行为轨迹接口，完整记录会员行为轨迹，用数据分析会员关注点，精准定位会员画像，为会员提供精准服务。', '会员行为，定制化推送', 0, 1, 0, NULL, '', '', 1, 1570719738, 1571921282, 0);
INSERT INTO `article` VALUES (9, 3, '原创', '//cdn.dmake.cn/attachment/images/20191024/15719213576.jpg', '会员安全认证，保证会员账号安全', NULL, '<p>为保证用户账号账号安全，板砖博客开启3重数据加密，极大程度的防治黑客盗取用户账号。</p>', '为保证用户账号账号安全，板砖博客开启3重数据加密，极大程度的防治黑客盗取用户账号。', '安全认证', 0, 1, 0, NULL, '', '', 1, 1570719749, 1571921461, 0);
INSERT INTO `article` VALUES (10, 3, '原创', '//cdn.dmake.cn/attachment/images/20191024/15719239603.jpg', '平台厂家和产品子模块简介', NULL, '<p style=\"text-align: center;\"><img src=\"http://cdn.dmake.cn/attachment/images/20191024/15719239603.jpg\"/></p><p>为什么目前互联网上充斥着很多资源下载网站和板砖博客厂家和产品子模块如此相近呢？这完全不是巧合，这就是技术层面上行业默认的框架结构。</p><p>平台厂家和产品子模块以树形方式从平台（根）拓展到厂家（主干）到产品（分支）再到产品相关的下载（叶）、介绍（花）、购买（果实），层层递进环环相扣，因此在板砖博客中数据统计和分析模块能够在关联各层级数据后提供精准数据。</p><p>在板砖博客中关于厂家和产品等模块中只是提供了基础通用的属性和功能，如果需要拓展请运营方自行研发或邀约板砖参与。</p><p><br/></p>', '目前互联网上充斥着很多资源下载网站，例如太平洋下载和多特下载站等，这些下载网站结构极大程度上类似于板砖博客厂家和产品子模块。', '产品模块', 0, 1, 0, NULL, '', '', 1, 1570719761, 1572329865, 0);
INSERT INTO `article` VALUES (11, 3, '原创', '//cdn.dmake.cn/attachment/images/20191024/15719216070.jpg', 'B2C商城模块，支持微信支付、支付宝支付', NULL, '<p>板砖博客B2C商城模块，标准化购物流程，支持微信支付，支付宝支付2种支付通道，带邮件、短信提醒功能。平台运营可根据订单状态需要开启提醒功能。</p><p>商城模块可与供应商模块搭配使用，建立完整运营数据链。</p><p><img src=\"http://cdn.dmake.cn/attachment/upload/201910/30/15724441464705.jpg\"/></p>', '板砖博客B2C商城模块，标准化购物流程，支持微信支付，支付宝支付2种支付通道，带邮件、短信提醒功能。', 'B2C商城模块，支持微信支付，支付宝支付', 0, 1, 0, NULL, '', '', 1, 1570719774, 1572444300, 0);
INSERT INTO `article` VALUES (12, 3, '原创', '//cdn.dmake.cn/attachment/images/20191024/157192380910.jpg', '资源下载模块，建立起基于会员的资源下载站', NULL, '<p>板砖博客为什么会拓展资源下载这个模块呢，其实是有个小插曲的。</p><p>板砖博客的女博主岸芷蝶舞是网络剧编剧和声优，在运营的需求下，需要满足会员机制剧本观看、音频下载，板砖博客继续满足了这一需求。</p><p>在此之后，我们还会继续推动资源下载模块的应用场景，例如游戏下载、软件下载和付费下载等等。</p><p><img src=\"http://cdn.dmake.cn/attachment/upload/201910/30/157244496728.jpg\"/></p>', '板砖博客的女博主岸芷蝶舞是网络剧编剧和声优，在运营的需求下，需要满足会员机制剧本观看、音频下载，板砖博客继续满足了这一需求。', '资源下载站', 0, 1, 0, NULL, '', '', 1, 1570719791, 1572445044, 0);
INSERT INTO `article` VALUES (13, 3, '原创', '//cdn.dmake.cn/attachment/images/20191024/15719237287.jpg', '视频课程，可录播独立视频，也可建立系列教程', NULL, '<p>板砖博客在研究过保险产品、电子产品和教育产品后，认为这一系列都是基于厂商模式形成的拓展服务生态链，另外有朋友也咨询过如何建立自己的补习、教育类平台，通过对专业的教育视频网站进行研究，板砖博客视频模块实现了单课可上线，观看可购买，建立完整的教育拓展服务。</p><p><img src=\"http://cdn.dmake.cn/attachment/upload/201910/30/15724444701643.jpg\"/></p><p><img src=\"http://cdn.dmake.cn/attachment/upload/201910/30/15724446637730.jpg\"/></p>', '板砖博客视频课程模块，1:1复刻慕课网模式，轻松搭建vip视频通道，UI可定制。', '视频课程，可录播独立视频，也可建立系列教程', 0, 1, 0, NULL, '', '', 1, 1570719801, 1572444699, 0);
INSERT INTO `article` VALUES (14, 3, '原创', '//cdn.dmake.cn/attachment/images/20191024/157192362210.jpg', '轻论坛模式，简约简介不简单', NULL, '<p>\n	更多的轻论坛的体验，请移步到板砖博客“交流中心”体验。\n</p>\n<p>\n	<img src=\"http://cdn.dmake.cn/attachment/keditor/image/20191030/20191030080206_11901.png\" alt=\"\" /> \n</p>', '在轻论坛模式下，我们更专注内容本身，尽可能的抛弃了用户属性，突出了主题的要点、便捷的回帖机制。', '轻论坛模式', 0, 1, 0, NULL, '', '', 1, 1570719812, 1572503276, 1);
INSERT INTO `article` VALUES (15, 3, '原创', '//cdn.dmake.cn/attachment/images/20191024/15719234501.jpg', '积分悬赏模式，推动用户互动动力', NULL, '<p style=\"text-align:left;\">\n	积分悬赏模式是传统论坛、社区模块中被保留下来的一个，积分悬赏模式是社区中积分模式的平衡点，发帖赚积分，悬赏消耗积分，能力越强积分越多，积分越多权益越多，下面是积分模式下的表现形式。\n</p>\n<p style=\"text-align:center;\">\n	<img src=\"http://cdn.dmake.cn/attachment/keditor/image/20191030/20191030073327_38721.png\" alt=\"\" />\n</p>', '积分悬赏模式是社区中积分模式的平衡点，发帖赚积分，悬赏消耗积分，能力越强积分越多，积分越多权益越多。', '', 0, 1, 0, NULL, '', '', 1, 1570719821, 1572420997, 1);
INSERT INTO `article` VALUES (16, 3, '原创', '//cdn.dmake.cn/attachment/images/20191024/15719233250.jpg', '根据访问量、关联分类访问量、评论和下有贡献率等指标排序', NULL, '<p>在音乐界，根据分类热度、用户推荐指数进行排行已经区以常态化。</p><p>在新闻界，根据关键词热度、用户阅读习惯进行数据分类和阅读、评价、收藏等综合指数推荐内容也已经常态化了。</p><p>那么作为小平台，只是在评论和阅读的基础上进行排名已经落后了，然而通过大数据精准推送还做不到，那该怎么办呀？</p><p><br/></p><p>板砖博客，内置3种热门排行模式，引用模板即可调用：</p><p>1、根据分类阅读和评价综合排名</p><p>2、根据单一指标数值排名</p><p>3、根据用户访问分类痕迹进行精准排名</p><p style=\"text-align: center;\"><img src=\"http://cdn.dmake.cn/attachment/upload/201910/30/15724457129231.jpg\" style=\"width: 269px; \"/></p>', '在内容为王的时代里，热门推荐和排名不再是简单的通过访问量排序即可。在大型平台中根据用户访问习惯推送热门内容已成为常态化。', '根据访问量，关联分类访问量，评论和下有贡献率等指标排序', 0, 1, 0, NULL, '', '', 1, 1570719831, 1572445733, 0);
INSERT INTO `article` VALUES (17, 3, '原创', '//cdn.dmake.cn/attachment/images/20191024/15719231731.jpg', '社区论坛纯净模式，只介于主题帖与回答、悬赏，不附带更多的自定义模式', NULL, '<p style=\"text-align:center;\">\n	<img src=\"http://cdn.dmake.cn/attachment/keditor/image/20191030/20191030071817_43295.jpg\" alt=\"\" />\n</p>\n<p style=\"text-align:left;\">\n	Discuz和phpwind是国内2大社区建站系统，其功能性之强大，以及独有的产品中心都是他们生长壮大的原因之一，同理越强大越复杂，很多平台运营者认为其都超出运营系统要求。\n</p>\n<p style=\"text-align:left;\">\n	<br />\n</p>\n<p style=\"text-align:left;\">\n	因此板砖博客论坛模块，秉承简洁-简介-简介的理念，在保留了基础的主题帖、回帖、悬赏积分、板块内容管理、分等级置顶等，其余功能都被舍去，毕竟板砖社区模块只为板砖博客整体运营体系而生，同理也适合任何以大型平台运营为基础的环境。\n</p>\n<p style=\"text-align:left;\">\n	<br />\n</p>\n<p style=\"text-align:center;\">\n	<img src=\"http://cdn.dmake.cn/attachment/keditor/image/20191030/20191030072313_63044.png\" alt=\"\" />\n</p>\n<p style=\"text-align:center;\">\n	<br />\n</p>\n<p style=\"text-align:center;\">\n	<br />\n</p>', '在中国，最全面的社区系统就是discuz和phpwind，在他们的基础上，您可以通过学习后快速新增模块和模式，但是操作很是复杂。然而目前更多的综合平台的社区业务趋于简单化，因此板砖博客论坛社区模块便是好的选择。', '社区模式，论坛模式', 0, 1, 0, NULL, '', '', 1, 1570719844, 1572420200, 0);
INSERT INTO `article` VALUES (18, 3, '原创', '//cdn.dmake.cn/attachment/images/20191024/15719228164.jpg', '全系统数据调用，js注入获得内容推荐更好体验', NULL, '<p style=\"text-align: center;\"><img src=\"http://cdn.dmake.cn/attachment/upload/201910/30/15724459313635.jpg\"/></p><p style=\"text-align: center;\">网页广告注入案例</p><p style=\"text-align: left;\">相信大家都遇到过这么几种情况：</p><p style=\"text-align: left;\">看网页的时候在内容中突然跳出一个大分段，中间是推荐的相关的其他内容；</p><p style=\"text-align: left;\">在哪个网站上都能看到淘宝等购物广告，而且长得惊人的一致。</p><p style=\"text-align: left;\">那么这些内容是如何做到的呢？</p><p style=\"text-align: left;\">——JS注入</p><p style=\"text-align: left;\">网站站长仅需将这种带节奏的js写入网页模板中即可完成内容的注入，当然注入何种内容完全看JS中业务需要。</p><p style=\"text-align: left;\">板砖博客通过系统数据池接口智能推送关联度最高的内容到网页前端，实现内容智能加载，完成您自己的内容注入！</p>', '板砖博客通过前端技术在节点获得相关内容智能推送，不影响现有结果，一句JS即可完成。', '全系统数据调用，js注入获得内容推荐更好体验', 0, 1, 0, NULL, '', '', 1, 1570719854, 1572446162, 0);
INSERT INTO `article` VALUES (19, 3, '原创', '//cdn.dmake.cn/attachment/images/20191024/15719229900.jpg', '行为数据采集，悄然无声填充数据池', NULL, '<p><br/></p><p style=\"overflow-wrap: break-word; color: rgb(0, 0, 0); font-family: &quot;sans serif&quot;, tahoma, verdana, helvetica; font-size: 13.3333px; white-space: normal;\">越来越多的企业已开始挖掘用户行为数据的商业价值，利用行为数据进行精准有效的数字营销。以互联网行业淘宝为例，我们经常可以发现，在我们浏览淘宝之后，在淘宝APP中可以获得相关搜索词或者是浏览同类商品，在其他的平台中，可以获得同样的推荐，那么这是怎么做到的呢？</p><p style=\"overflow-wrap: break-word; color: rgb(0, 0, 0); font-family: &quot;sans serif&quot;, tahoma, verdana, helvetica; font-size: 13.3333px; white-space: normal;\">这里不得不说2种数据采集方式了：</p><p style=\"overflow-wrap: break-word; color: rgb(0, 0, 0); font-family: &quot;sans serif&quot;, tahoma, verdana, helvetica; font-size: 13.3333px; white-space: normal;\">1、 内置数据采集方式</p><p style=\"overflow-wrap: break-word; color: rgb(0, 0, 0); font-family: &quot;sans serif&quot;, tahoma, verdana, helvetica; font-size: 13.3333px; white-space: normal;\">2、 无节点数据采集方式</p><p style=\"overflow-wrap: break-word; color: rgb(0, 0, 0); font-family: &quot;sans serif&quot;, tahoma, verdana, helvetica; font-size: 13.3333px; white-space: normal; text-align: center;\"><img src=\"http://cdn.dmake.cn/attachment/images/20191024/15719229900.jpg\"/></p><p style=\"overflow-wrap: break-word; color: rgb(0, 0, 0); font-family: &quot;sans serif&quot;, tahoma, verdana, helvetica; font-size: 13.3333px; white-space: normal;\">首先来说内置数据采集这种方式，这类采集方式是在业务系统中固定的业务节点进行数据采集或统计，例如浏览某个网页的时候关联用户浏览记录，付费的时候关联付费统计，这类采集是精准业务下的统计，可以用于固定业务的统计分析。</p><p style=\"overflow-wrap: break-word; color: rgb(0, 0, 0); font-family: &quot;sans serif&quot;, tahoma, verdana, helvetica; font-size: 13.3333px; white-space: normal;\">然后来说一下无节点数据采集方式，这类方式是大而全的数据采集，在web系统中往往是以js注入方式执行，在c#或者java等非开源程序中以base基类为注入点进行采集。</p><p style=\"overflow-wrap: break-word; color: rgb(0, 0, 0); font-family: &quot;sans serif&quot;, tahoma, verdana, helvetica; font-size: 13.3333px; white-space: normal;\">那么无节点数据采集方式的好处是什么呢？</p><p style=\"overflow-wrap: break-word; color: rgb(0, 0, 0); font-family: &quot;sans serif&quot;, tahoma, verdana, helvetica; font-size: 13.3333px; white-space: normal;\">1、 在没有数据之前，我们不可能预测平台系统的访问和交互重点所在，没有数据的运营只是自我感觉良好，无节点数据采集可以随时调整采集范围，从用户动作、停留时间、下游贡献、热力点击等多个维度去记录用户行为，当行为数据有一定累计的情况下，那么可以通过分析工具或者大数据方式转换成可读图表、报表；</p><p style=\"overflow-wrap: break-word; color: rgb(0, 0, 0); font-family: &quot;sans serif&quot;, tahoma, verdana, helvetica; font-size: 13.3333px; white-space: normal;\">2、 在数据拓展方面，js采集数据的好处是可以在不接触平台系统的时候，通过修改js的代码就能随时更新采集规则。</p><p style=\"overflow-wrap: break-word; color: rgb(0, 0, 0); font-family: &quot;sans serif&quot;, tahoma, verdana, helvetica; font-size: 13.3333px; white-space: normal;\">在这两个优点的加持下，数据采集填充数据池不再是一个让人头疼的大动作了，都是小意思啦！</p>', '板砖博客内置行为数据采集双通道模式，丰富的采集用户行为数据。', '数据采集，数据池', 0, 1, 0, NULL, '', '', 1, 1570719864, 1572418742, 0);
INSERT INTO `article` VALUES (20, 3, '原创', '//cdn.dmake.cn/attachment/images/20191024/15719083639.jpg', '基于Composer快速完成功能开发成为现实', NULL, '<p>不会百度的程序猿不是号程序猿，不会git的程序员永远只能埋头苦干。</p><p>composer是 PHP 用来管理依赖（dependency）关系的工具。你可以在自己的项目中声明所依赖的外部工具库（libraries），Composer 会帮你安装这些依赖的库文件。</p><p style=\"text-align: center;\"><img src=\"http://cdn.dmake.cn/attachment/images/20191024/15719083639.jpg\"/></p><p>板砖博客基于composer快速搭建支付系统、微信公众号接口等功能，将开发速度提升至极致，单一功能从设计到上线不超过2天。</p><p>因不同的开发者使用的工具不同，板砖推荐以下3中编辑器：Sublime Text&nbsp; &nbsp; |&nbsp; &nbsp;ZendStudio&nbsp; &nbsp; |&nbsp; &nbsp;Visual studio code</p><p>板砖使用的是ZendStudio，因为其composer功能更为方便一点，另外错误代码提醒功能也能很大程度减少编码错误！</p>', '板砖博客基于composer快速搭建支付系统、微信公众号接口等功能，将开发速度提升至极致，单一功能从设计到上线不超过2天。', 'composer', 0, 1, 0, NULL, '', '', 1, 1570719875, 1572329398, 0);
INSERT INTO `article` VALUES (21, 3, '原创', '//cdn.dmake.cn/attachment/upload/201910/30/15723979214016.png', '基于百度echart可视化图形报表，反应数据能力更清晰', NULL, '<p style=\"text-align:center;\">\n	<br />\n</p>\n<p style=\"text-align:left;\">\n	借用淘宝上的一句广告词：你不要用XXX，除非你看到了我！\n</p>\n<p style=\"text-align:left;\">\n	在很多大型企业中数据报表的多样性往往除了excel以外还有额外的数据反馈系统，你知道如何从报表流水似的数据中看到运营结果？板砖在言几又书店看到了很多教大家做excel报表的教程，这些的确是提升个人能力的方法，但是企业真的需要如此复杂的报表方式吗？\n</p>\n<p style=\"text-align:left;\">\n	基于这个疑问（板砖不喜欢使用excel<img src=\"http://img.baidu.com/hi/jx2/j_0028.gif\" />），板砖为平台运营建立了多种图表反馈模型，用以解决以上问题：\n</p>\n<p style=\"text-align:left;\">\n	<img src=\"http://cdn.dmake.cn/attachment/upload/201910/30/15723979214016.png\" />\n</p>\n<p style=\"text-align:left;\">\n	<br />\n</p>\n<p style=\"text-align:left;\">\n	1、 厂商综合实力模型，用于看近期哪些厂商具有提升或者被排斥的必要\n</p>\n<p style=\"text-align:left;\">\n	2、 产品实力分布模型，用于看产品流量构成、下游贡献率等，编辑和运营可以根据图表适度调整自己工作内容\n</p>\n<p style=\"text-align:left;\">\n	3、 编辑内容能力模型，用于了解编辑的综合实力，及时发现团队中的人员短板，利于组织优化\n</p>\n<p style=\"text-align:left;\">\n	4、 员工内容创作模型，用于抓团队中开小差的员工\n</p>\n<p style=\"text-align:left;\">\n	5、 平台综合运营走势，通过对访问量、贡献率等问题的综合评分，查看连续12个月的综合情况以及当月预示，管理团队可以看到团队的长时间工作价值。\n</p>\n<p style=\"text-align:left;\">\n	<img src=\"http://cdn.dmake.cn/attachment/upload/201910/29/15723388374894.png\" />\n</p>\n<p style=\"text-align:left;\">\n	基于以上模型，在运营的大局观上基本满足了一个运营经理的日常关注要点，当然基于其他平台的数据暂时无法展现。\n</p>', '板砖体验过小型团队在运营中不能及时获取数据的痛苦，也看到过一些公司管理层每天花大量时间编写数据报表等反应工作结果，然而这些事情真的很耗时耗力并且没有必要。', '百度echart,可视化图形报表', 0, 1, 0, NULL, '', '', 1, 1570719894, 1611885638, 1);
INSERT INTO `article` VALUES (22, 3, '原创', '//cdn.dmake.cn/attachment/upload/201910/29/15723412571433.png', '原生业务数据统计，导入导出excel表格', NULL, '<p style=\"text-align:left;\">\n	在运营中通常会有数据支持的需求，例如短信群发对象、订单导出、某个用户行为达成的对象等，板砖博客除了在平台后端通过数据表格等方式展现业务数据以外，也提供一键导出功能，为运营自行制作excel相关图表做基础的数据支撑。\n</p>\n<p style=\"text-align:left;\">\n	以下图片为导入导出的一般应用场景示例图\n</p>\n<p style=\"text-align:left;\">\n	<img src=\"http://cdn.dmake.cn/attachment/upload/201910/29/15723412571433.png\" />\n</p>', '通过板砖博客将数据池数据经过业务思维组合以后，形成可以做为决策参考的数据报表，并提供导出功能。', '数据统计，导出到excel', 0, 1, 0, NULL, '', '', 1, 1570719904, 1611885861, 0);
INSERT INTO `article` VALUES (23, 3, '原创', '//cdn.dmake.cn/attachment/upload/201910/29/15723395902760.png', '可拓展数据池，给您留下更多的数据利用空间', NULL, '<p style=\"text-align:left;\">\n	很多公司的推广人员（SEM|SEO|社区推广等）每天有很多对外推广的工作，比如说付费广告、发帖、外链等形式占据了很大一部分时间，这就形成了运营基础信息的发布。\n</p>\n<p style=\"text-align:left;\">\n	然而，我们的付费广告、外链和推广真的是有用吗？哪些部分是可以精简甚至舍弃的？哪些部分是需要追加投入的？这些经营性决策就需要数据支持。\n</p>\n<p style=\"text-align:left;\">\n	板砖博客内置数据采集模块，推广人员在推广渠道中按照步骤在原始链接中添加标识?channel=xxx即可，每当网友访问即可形成数据，最后通过板砖博客的数据图表可视化就形成了可读的决策支持！\n</p>\n<p style=\"text-align:left;\">\n	同时，板砖博客的数据池是通用数据池，可以根据不同的运营需要建立不同的运营模型，详情请参考》<a href=\"https://www.dmake.cn/article/21\" target=\"_blank\">数据图表可视化建模</a> \n</p>\n<p style=\"text-align:left;\">\n	<img src=\"http://cdn.dmake.cn/attachment/upload/201910/29/15723395902760.png\" /> \n</p>', '在管理信息系统中所有信息经过筛选后形成数据，数据通过建模以后形成决策支持，这就是板砖博客数据池的建设目的——为最终决策支持做好可扩展准备。', '数据池，数据利用', 0, 1, 0, NULL, '', '', 1, 1570719914, 1611885681, 0);
INSERT INTO `article` VALUES (24, 3, '原创', '//cdn.dmake.cn/attachment/images/20191024/15719221490.jpg', '微信小程序营销：微信投票、阅读返利、发帖有奖、微信考试、口令红包、语音红包', NULL, '<p style=\"text-align: left;\">板砖博客微信功能是基于easywechat4.0开发的，在此也感谢easywechat产品团队的无私奉献。</p><p style=\"text-align: left;\">板砖在制作微信模块的时候有3个特别需要注意的地方，其他的都是功能性逻辑问题：</p><p style=\"text-align: left;\">1、<span style=\"white-space: normal;\">公众号独立的服务器配置</span></p><p style=\"text-align: left;\">2、跨域名借其他公众号获取openid</p><p style=\"text-align: left;\">3、<span style=\"white-space: normal;\">公众开放平台</span></p><p style=\"text-align: left;\"><span style=\"white-space: normal;\"><br/></span></p><p style=\"text-align: left;\">如果您是运营人员而非技术人员，欢迎关注公众号“暂保密”体验功能；如果您是技术人员想看更多相关思路，请方位我的thinkphp博客：http://www.thinkphp.cn/member/article/p/1.html</p><p style=\"text-align: left;\"><span style=\"white-space: normal;\"><br/></span></p><p style=\"text-align: center\"><img src=\"http://cdn.dmake.cn/attachment/images/20191024/15719221490.jpg\" style=\"text-align: center; white-space: normal;\"/></p><p style=\"text-align: left;\">首先来看看公众号独立的服务器配置，使用easywechat的部分网友在配置验证时会遇到经常不能够认证通过的情况，这种情况下板砖是这样处理的：</p><p style=\"text-align: left;\">在确保功能稳定无误的情况下，直接返回echostr，不要尝试纠结一些小问题，否则会浪费很多时间。</p><p style=\"text-align: center;\"><img src=\"http://cdn.dmake.cn/attachment/upload/201910/29/15723374936431.png\"/></p><p style=\"text-align: left;\"><br/></p><p style=\"text-align: left;\">然后是借用openid，很多时候运营方没有自己的服务号，这个时候通过订阅号回复获取openid的方式体验又很不好，因此板砖在tom插件中获得并改善了借用openid的方法：</p><p style=\"text-align: left;\"></p><pre>&lt;?php\n\n/*\n * tom微信借授权\n */\n$url = get_url();\n\n$oauth_back_url = urldecode($_GET[&#39;oauth_back_url&#39;]);\n\npreg_match(&quot;#((http|https)://[^?]*/)tom_oauth.php#&quot;, $url, $urlmatches);\nif (is_array($urlmatches) &amp;&amp; ! empty($urlmatches[&#39;0&#39;])) {\n    $url = str_replace($urlmatches[&#39;0&#39;], $oauth_back_url, $url);\n}\n\nheader(&#39;Location: &#39; . $url);\n\nfunction get_url()\n{\n    $protocol = (! empty($_SERVER[&#39;HTTPS&#39;]) &amp;&amp; $_SERVER[&#39;HTTPS&#39;] !== &#39;off&#39; || $_SERVER[&#39;SERVER_PORT&#39;] == 443) ? &quot;https://&quot; : &quot;http://&quot;;\n    $url = &quot;$protocol$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]&quot;;\n    return $url;}//////////////////////////////////////////如果你想在自己的代码中实现这一方法，那么只需要判断是否需要即可，当然在板砖博客中已经集成if ($wxasnum == 1 || $wxasnum == 10000) { $redirecturl = &#39;xxx.php?oauth_back_url=&#39; . urlencode($redirecturl); }</pre><p></p><p style=\"text-align: left;\">最后来说一下公众号开放平台的问题，其实公众平台核心问题就出在管理员授权和取消授权两个问题上，其余的操作部分和普通公众号服务器授权一致，因此开发者可以注意easywechat的授权与取消授权功能上。</p><p style=\"text-align: left;\"></p><pre>    /*\n     * 第三方app的refreshtoken建名\n     */\n    private $key_refreshkey = CKEY_WX_REFRESHTOKEN;\n\n    public function callback()\n    {\n        $openPlatform = Factory::openPlatform(config(&#39;openflat&#39;));\n        \n        if (input(&#39;get.auth_code&#39;)) {\n            $result = $openPlatform-&gt;handleAuthorize(input(&#39;get.auth_code&#39;));\n            // file_put_contents(&#39;/www/web/tlsc/web/public_html/inf.txt&#39;, json_encode($result));\n            $info = $result[&#39;authorization_info&#39;];\n            $data = [\n                &#39;access_token&#39; =&gt; $info[&#39;authorizer_access_token&#39;],\n                &#39;refresh_token&#39; =&gt; $info[&#39;authorizer_refresh_token&#39;],\n                &#39;authorization_info&#39; =&gt; json_encode($result)\n            ];\n            \n            cache($this-&gt;key_refreshkey . $info[&#39;authorizer_appid&#39;], $info[&#39;authorizer_refresh_token&#39;]);\n            \n            Db::name(&#39;op_app&#39;)-&gt;where(&#39;appid&#39;, $info[&#39;authorizer_appid&#39;])-&gt;update($data);\n            \n            return redirect(url(&#39;admin/openflat/index&#39;));\n        }\n        \n        $server = $openPlatform-&gt;server;\n        $message = $server-&gt;getMessage();\n        \n        switch ($message[&#39;InfoType&#39;]) {\n            \n            case Guard::EVENT_AUTHORIZED:\n                // 处理授权成功事件\n                $server-&gt;push(function ($message) {\n                    // $message 为微信推送的通知内容，不同事件不同内容，详看微信官方文档\n                    // 获取授权公众号 AppId： $message[&#39;AuthorizerAppid&#39;]\n                    // 获取 AuthCode：$message[&#39;AuthorizationCode&#39;]\n                    // 然后进行业务处理，如存数据库等...\n                    \n                    $appid = $message[&#39;AuthorizerAppid&#39;];\n                    $code = $message[&#39;AuthorizationCode&#39;];\n                    \n                    Db::name(&#39;op_app&#39;)-&gt;where(&#39;appid&#39;, $appid)\n                        -&gt;update([\n                        &#39;authcode&#39; =&gt; $code,\n                        &#39;status&#39; =&gt; &#39;active&#39;\n                    ]);\n                }, Guard::EVENT_AUTHORIZED);\n                break;\n            \n            case Guard::EVENT_UPDATE_AUTHORIZED:\n                // 处理授权更新事件\n                $server-&gt;push(function ($message) {\n                    \n                    $appid = $message[&#39;AuthorizerAppid&#39;];\n                    $code = $message[&#39;AuthorizationCode&#39;];\n                    \n                    Db::name(&#39;op_app&#39;)-&gt;where(&#39;appid&#39;, $appid)\n                        -&gt;update([\n                        &#39;authcode&#39; =&gt; $code\n                    ]);\n                }, Guard::EVENT_UPDATE_AUTHORIZED);\n                break;\n            \n            case Guard::EVENT_UNAUTHORIZED:\n                // 处理授权取消事件\n                $server-&gt;push(function ($message) {\n                    \n                    $appid = $message[&#39;AuthorizerAppid&#39;];\n                    \n                    Db::name(&#39;op_app&#39;)-&gt;where(&#39;appid&#39;, $appid)\n                        -&gt;update([\n                        &#39;status&#39; =&gt; &#39;cancel&#39;\n                    ]);\n                }, Guard::EVENT_UNAUTHORIZED);\n                break;\n        }\n        \n        $server-&gt;serve()-&gt;send();\n    }</pre><p></p><p style=\"text-align: left;\"><br/></p>', '板砖曾经做过一个地方门户网站，现在罗列一些曾经吸粉引流、提升活跃度的模块罗列出来，因为没有经理制作更多的模块，所以运营者可以根据自身情况拓展。', '微信小程序营销，微信投票、阅读返利、发帖有奖、微信考试、口令红包、语音红包', 0, 1, 0, NULL, '', '', 1, 1570719925, 1572338096, 1);
INSERT INTO `article` VALUES (25, 3, '原创', '//cdn.dmake.cn/attachment/images/20191024/157192227110.jpg', '微信小程序商城原生版本', NULL, '<p>因为板砖博客的小程序商城以“萤火”为基础改进和拓展了其他功能，基础功能体验请前往：传送门https://demo.yiovo.com/index.php?s=/store/index/index</p><p>板砖认为此版小程序系统是最接地气的基础版本，营销功能和拓展很方便，如果没有技术实力的团队可能还是得花不少时间去实现。</p><p>此版本小程序包含了基础的商品管理、订单、促销、优惠券和门店的功能，甚至包含了小票打印机功能，特别适合小团队直接使用，大团队拓展开发。</p><p><img src=\"http://cdn.dmake.cn/attachment/upload/201910/29/15723290728437.png\"/></p><p>微信小程序商城有多种表现形式，板砖博客自带商城有参考萤火小程序，建议只需要电商功能的客户直接用“萤火小程序”体验，如果您只想有个小程序不想去管程序和服务器的话，用板砖代理的“手边”小程序，可享零售价5折起。</p><p><br/></p>', '微信小程序商城有多种表现形式，板砖博客自带商城有参考萤火小程序，建议只需要电商功能的客户直接用“萤火小程序”体验，如果您只想有个小程序不想去管程序和服务器的话，用板砖代理的“手边”小程序，可享零售价5折起。', '微信小程序商城', 0, 1, 0, NULL, '', '', 1, 1570719936, 1572329211, 0);
INSERT INTO `article` VALUES (26, 3, '原创', '//cdn.dmake.cn/attachment/upload/201910/29/15723282936295.png', '微信公众号、小程序消息主动推送', NULL, '<p>\n	板砖博客推荐使用微信公众号服务号消息主动推送功能，推送稳定效果好。如果您是通过“公众号开放平台”注册的微信公众号和小程序数据链接，那么服务号和小程序同时具备了消息主动推送的能力。因为<span>小程序推送消息会要求formid，这个formid需要每7天一更新，所以对小程序的运营有一定的技术难度，因此运营者可以用公众号消息推送来互补这个遗憾。</span>\n</p>\n<p>\n	<strong>推送原理</strong>\n</p>\n<p>\n	公众号的推送是根据openid的推送的。\n</p>\n<p>\n	但公众号id获取被微信卡死了，规则为：必须要先获取openid，然后根据openid获取unionid.\n</p>\n<p>\n	当用户在小程序中操作的时候，可以根据小程序的openid找到小程序和公众号公共的unionid，然后再去找公众号的openid进行推送。\n</p>\n<p style=\"text-align:center;\">\n	<img src=\"http://cdn.dmake.cn/attachment/upload/201910/29/15723282936295.png\" />\n</p>\n<p style=\"text-align:center;\">\n	<br />\n</p>\n<p style=\"text-align:left;\">\n	技术说完说步骤：\n</p>\n<p style=\"text-align:left;\">\n	1、 注册微信公众号--服务号，记得是服务号哦，可以获取信息推送的权限，认证需要300元，过程不再复述\n</p>\n<p style=\"text-align:left;\">\n	2、 服务号审核通过后可以快捷注册一个小程序，小程序的绑定之后再说吧\n</p>\n<p style=\"text-align:left;\">\n	3、 公众号设置配置板砖博客地址，并在板砖博客wxauth·php中配置相关信息\n</p>\n<p style=\"text-align:left;\">\n	4、 公众号有粉丝后即可开始推送，建议结合平台统计功能使用可精准推送，消息推送功能正在和数据统计进行匹配升级，未来可通过数据标签等特征自动匹配。\n</p>\n<p style=\"text-align:left;\">\n	<br />\n</p>\n<p style=\"text-align:left;\">\n	<img src=\"http://cdn.dmake.cn/attachment/upload/201910/29/15723286583208.png\" />\n</p>\n<p style=\"text-align:left;\">\n	<br />\n</p>\n<p style=\"text-align:left;\">\n	<br />\n</p>', '板砖博客推荐使用微信公众号服务号消息主动推送功能，推送稳定效果好。', '微信公众号,小程序消息主动推送', 0, 1, 0, NULL, '', '', 1, 1570719945, 1611885912, 0);
INSERT INTO `article` VALUES (72, 1, '原创', '//cdn.dmake.cn/attachment/canvs/202102/01/1612160422.jpg', 'PayPal SDK在与Thinkphp中部署的几次报错解析', NULL, '<p style=\"text-align:center;\">\n	<img src=\"https://cdn.dmake.cn/attachment/canvs/202102/01/1612160422.jpg\" alt=\"Dmake收银方式新增PayPal渠道，适应全球销售通道\" /> \n</p>\n<p style=\"text-align:center;\">\n	<br />\n</p>\n<p style=\"text-align:left;\">\n	如果你的系统之前集成过了其他支付，那么PayPal接入也将非常简单。\n</p>\n<p style=\"text-align:left;\">\n	板砖主要参考了这篇文章：https://blog.csdn.net/weixin_43932088/article/details/87879755\n</p>\n<p style=\"text-align:left;\">\n	因为该博主已经写的非常详细了，所以我这里只对3个主要步骤说明：\n</p>\n<p style=\"text-align:left;\">\n	1.&nbsp;<span style=\"color:#4D4D4D;font-family:-apple-system, \" font-size:16px;background-color:#ffffff;\"=\"\">composer require paypal/rest-api-sdk-php:*&nbsp;&nbsp;</span> \n</p>\n<p style=\"text-align:left;\">\n	<span style=\"color:#4D4D4D;font-family:-apple-system, \" font-size:16px;background-color:#ffffff;\"=\"\">2. 回调接口不用像作者那样加时间戳，毕竟逻辑不一样 </span> \n</p>\n<pre class=\"prettyprint lang-php\">            $redirectUrls-&gt;setReturnUrl($config[\'returnurl\'])\n                -&gt;setCancelUrl($config[\'cancelurl\']);</pre>\n<p>\n	<br />\n</p>\n<p>\n	3.&nbsp; 如果遇到以下错误码，请记得检查结算货币是否是CNY，这样是会报错的\n</p>\n<p>\n	<br />\n</p>\n<pre class=\"prettyprint lang-php\">Got Http response code 400 when accessing https://api.sandbox.paypal.com/v1/payments/payment.</pre>\n4. 记得开启debug，找异常方便点\n<p>\n	<br />\n</p>\n<p>\n	<br />\n</p>\n<pre class=\"prettyprint lang-php\">        // 实例初始化\n        $apiContext = new \\PayPal\\Rest\\ApiContext(\n            new \\PayPal\\Auth\\OAuthTokenCredential($config[\'ClientID\'], $config[\'ClientSecret\'])\n        );\n\n        /*sandbox 模式*/\n        $apiContext-&gt;setConfig([\'mode\' =&gt; \'sandbox\',\n            \'log.LogEnabled\' =&gt; true,\n            \'log.FileName\' =&gt; \'./paypal.log\',\n            \'log.LogLevel\' =&gt; \'DEBUG\', // PLEASE USE `INFO` LEVEL FOR LOGGING IN LIVE ENVIRONMENTS\n            \'cache.enabled\' =&gt; true,\n\n            // \'http.CURLOPT_CONNECTTIMEOUT\' =&gt; 30\n            // \'http.headers.PayPal-Partner-Attribution-Id\' =&gt; \'123123123\'\n            //\'log.AdapterFactory\' =&gt; \'\\PayPal\\Log\\DefaultLogFactory\' // Factory class implementing \\PayPal\\Log\\PayPalLogFactory\n        ]);</pre>\n总体来说，PayPal的集成并不是特别难，集成方法和支付宝的大致相同，如果集成有难度，请看文初的博客教程。\n<p>\n	<br />\n</p>\n<p>\n	另外在组装post实例中，是否需要将消费者联系方式和消费明细集成属于选填项，没有填写也不会报错。\n</p>', 'PayPal在集成进Thinkphp系统遇到的400问题和注意事项分享。', '', 0, 1, 0, NULL, '', '', 1, 1612162251, 1612185722, 0);
INSERT INTO `article` VALUES (27, 3, '原创', '//cdn.dmake.cn/attachment/images/20191024/15719226287.jpg', '内置七牛云存储，成本低存储效果好，可靠易扩展', NULL, '<p>七牛云存储作为国内性价比较高的云存储平台，在以下多个方面具有优势，并且板砖在对比了七牛云、又拍云等多个云存储方式后，认为七牛云是php项目中最容易使用和拓展的方式。</p><p style=\"text-align: center;\"><img src=\"http://cdn.dmake.cn/attachment/images/20191024/15719226287.jpg\"/></p><p style=\"text-align: left;\">板砖接下来分享以下使用体验：</p><p style=\"text-align: left;\">1、 成本低，注册用户每月都可享受标准存储免费空间0-10 GB，每月免费上传流量无上限，标准存储每月写请求 Put / Delete0-10 万次，标准存储每月读请求 Get0-100 万次，用人话来将，就是作为一般官网和个人站点来说可以说是完全无付费可能。</p><p style=\"text-align: left;\">2、 效率高，七牛云为各类开发语言都出了标准版的sdk，系统集成块，另外还自带水印、剪裁、压缩和扫黄的功能，运营者可以在此节省不少力气。</p><p style=\"text-align: left;\">3、 稳定，七牛云目前已经脱离了普通新创公司的枷锁，在诸多大型互联网公司的背后加持下，服务稳定、价格也在逐步下调，性价比可谓不俗。<span style=\"color: inherit;\">另外，企业用户可以在付费以后开出专票。</span></p><p style=\"text-align: left;\"><span style=\"color: inherit;\"><strong>4、板砖博客后端富文本编辑控件中，已经集成了UEditor/UMeditor/KindEditor三种控件无缝保存资源到七牛云中</strong></span></p><p style=\"text-align: left;\"><span style=\"color: inherit;\">注意：板砖博客自带七牛SDK，功能已经集成，如果您想试用本系统，请自行开通七牛账号并配置到index·php相应位置，注册传送门：https://portal.qiniu.com/signup?code=1hm7kzbx0yr0y</span></p>', '七牛云海量存储系统（KODO）是自主研发的非结构化数据存储管理平台，支持中心和边缘存储。平台经过多年大规模用户验证已跻身先进技术行列，并广泛应用于海量数据管理的各类场景。', '七牛云存储', 0, 1, 0, NULL, '', '', 1, 1570719955, 1572416296, 0);
INSERT INTO `article` VALUES (28, 3, '原创', '//cdn.dmake.cn/attachment/images/20191024/15719064705.jpg', '上传即上云，再大的网站平台一台虚拟主机足矣', NULL, '<p style=\"text-align: center;\"><img src=\"http://cdn.dmake.cn/attachment/upload/201910/24/1571906686510.jpg\"/></p><p>为了极大的减少对服务器的压力，板砖博客在内容存储中开启2种模式：本地存储和云存储，极大的减少了对服务器硬件配置的压力。</p><p>一般情况下，企业搭建一个服务器最低配置应是2核2G内存5M宽带100G数据盘，预估经费一年最低1500元。</p><p>在经过内容存储的压力分配下，除了少数的css和js资源被存储在本地，图片、附件、视频等都存储在第三方云平台，下载不限速、容量可扩张，在这种模式下即便是虚拟主机2M宽带，也能完美运行日IP量达5000的平台，但是成本将降低到500/年。</p>', '为了极大的减少对服务器的压力，板砖博客在内容存储中开启2种模式：本地存储和云存储，极大的减少了对服务器硬件配置的压力。', '虚拟主机，云端存储', 0, 1, 0, NULL, '', '', 1, 1570719964, 1571906937, 1);
INSERT INTO `article` VALUES (29, 3, '原创', '//cdn.dmake.cn/attachment/images/20191024/15719227440.jpg', '站点资源可管理，丰富的分组和管理方式精简云存储内容', NULL, '<p>板砖博客结合云存储特征，内置了七牛云资源管理工具，便于编辑反复利用云资源、管理云资源。</p><p>通过对云资源的分组、重命名、打标签，编辑可以快速的找到常用内容，缩短编辑时间。</p><p style=\"text-align: center;\"><img src=\"http://cdn.dmake.cn/attachment/upload/201910/29/15723270275852.png\"/></p><p>众所周知云存储进入平台发展的重要历程以内，如何管理云平台资源成为一个必须的模块，因为其不在本地服务器，如果内容遭受篡改而运营方不知情的情况下必然是一个重要的隐患！</p>', '众所周知云存储进入平台发展的重要历程以内，如何管理云平台资源成为一个必须的模块，因为其不在本地服务器，如果内容遭受篡改而运营方不知情的情况下必然是一个重要的隐患！', '资源管理，云存储管理', 0, 1, 0, NULL, '', '', 1, 1570719975, 1572327108, 0);
INSERT INTO `article` VALUES (30, 1, '原创', '//cdn.dmake.cn/attachment/images/20191025/15719702187.jpg', 'Fontawesome5.11升级了，给web开发者更多的icon选择性！', NULL, '<p>Fontawesome4.7图标依然在不断新增中，但是Fontawesome可以预见性的遇到了拓展不足的地方。例如ICON边缘粗细、自定义颜色等多个能够让WEB开发者自由裁量的选择都受到了限制，而5.11版本起解决了这些问题。</p><p>Fontawesome5.11图标可以根据您的需要使用任何颜色来显示。而Font Awesome专业版更支持全新的双色图标。这些漂亮的图标可以通过继承项目的基色来实现开箱即用，或者根据您的需要进行自定义。</p><p>作为想尝鲜的你肯定有了以下几个疑问:</p><p>1. 哪儿去体验<span style=\"white-space: normal;\">Fontawesome5.11？</span></p><p><span style=\"white-space: normal;\">2.&nbsp;<span style=\"white-space: normal;\">Fontawesome5.11与4.*版本ICON内容有什么却别？</span></span></p><p><span style=\"white-space: normal;\"><span style=\"white-space: normal;\">3.&nbsp;<span style=\"white-space: normal;\">Fontawesome5.11的是怎么引入的？</span></span></span></p><p><span style=\"white-space: normal;\"><span style=\"white-space: normal;\"><span style=\"white-space: normal;\">4.&nbsp;<span style=\"white-space: normal;\">Fontawesome5.11如何能够从4.*版本无缝切入？</span></span></span></span></p><p><span style=\"white-space: normal;\"><span style=\"white-space: normal;\"><span style=\"white-space: normal;\"><span style=\"white-space: normal;\"><br/></span></span></span></span></p><p><span style=\"white-space: normal;\"><span style=\"white-space: normal;\"><span style=\"white-space: normal;\"><span style=\"white-space: normal;\">今天板砖将来为大家逐一解答！</span></span></span></span></p><p style=\"text-align: center;\"><img src=\"http://cdn.dmake.cn/attachment/upload/201910/25/15719705296662.png\"/></p><p style=\"text-align: left;\">目前大家可以通过fontawesome各中文网跳转进入5.*版本中文网，或者直接访问网址：https://www.dmake.cn/fontawesomev</p><p style=\"text-align: left;\">从ICON获取体验来讲，fontawesome5.*版本的中文网体验极差，交互性也没有4.*版本的强，但是毕竟是免费使用他的产品，大家就忍忍吧。</p><p style=\"text-align: left;\">上图5.*版本的资源截图上，大家可以看到多了svgs文件夹，难道这是5.*的突破？</p><p style=\"text-align: left;\">果然，出于完美渲染和强化变形（放大缩小）的目的，5.*版本对svg图标进行了大范围的升级！</p><p style=\"text-align: center;\"><img src=\"http://cdn.dmake.cn/attachment/upload/201910/25/1571970788497.png\" style=\"\"/></p><p style=\"text-align: center;\"><img src=\"http://cdn.dmake.cn/attachment/upload/201910/25/15719707927745.png\" style=\"\"/></p><p style=\"text-align: left;\">好了，接下来板砖将为大家讲解一下5.*版本和4.*的主要区别和引入、升级的问题！</p><p style=\"text-align: left;\"><br/></p><p style=\"text-align: left;\">Fontawesome5.* 与4.*相比主要多了以下特征：</p><p style=\"text-align: left;\">1. 新增了实心(fas)、常规(far)和品牌(fab)三种模式，也就是说有一部分icon的引入要从以前的“fa fa-x”变更至“fas fa-x”，例如引入微信图标，4.*版本引入方式为“fa fa-weixin”，到了5.*版本就应该写为&quot;fab fa-weixin&quot;，这个原生的写法，如果想获得兼容，接下来会详细讲解。</p><p style=\"text-align: left;\">2. 5.*版本后支持对icon的粗细自定义，以及放大倍数有了新的选择：</p><p><br/></p><p style=\"text-align: center;\"><img src=\"http://cdn.dmake.cn/attachment/upload/201910/25/1571971461594.png\"/></p><p>3. 在专业版（付费版750RMB）中可以获得更多的自定义权限</p><p style=\"text-align: center;\"><img src=\"http://cdn.dmake.cn/attachment/upload/201910/25/15719716782584.png\"/></p><p style=\"text-align: left;\">4. 5.*版本新增了emoj图标和很多的品牌图标，对于UI有更多素材要求的用户的确是个好事儿。</p><p style=\"text-align: center;\"><br/></p><p style=\"text-align: left;\">接下来是万众期待的4.*升级5.*的时刻了，我们需要注意以下几个方面：</p><p style=\"text-align: left;\">1. 引入以下两个css文件</p><p><span style=\"white-space: nowrap;\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 《link rel=&quot;stylesheet&quot; href=&quot;https://use.fontawesome.com/releases/v5.11.2/css/all.css&quot; 如果是直接使用5.*版本，引入这个表即可》</span></p><p><span style=\"white-space: nowrap;\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 《link rel=&quot;stylesheet&quot; href=&quot;https://use.fontawesome.com/releases/v5.11.2/css/v4-shims.css&quot; 如果是4.*升级，需要再引用这个表》</span></p><p><span style=\"white-space: nowrap;\">总体来说小编不建议兼容的方式升级Fontawesome，建议直接删除4.*使用5.*，因为改变并不会花费太多的精力，但是如果为了兼容就需要不断的试错。</span></p><p><span style=\"white-space: nowrap;\">更多的缘由和细节建议访问：http://fa5.dashgame.com/#/文档/从4.x版本升级&nbsp; 学习</span></p><p><span style=\"white-space: nowrap;\">2. 升级后需要注意的问题：</span></p><p><span style=\"white-space: nowrap;\"></span></p><p><span style=\"white-space: nowrap;\">图标名称变更：</span><span style=\"white-space: nowrap; color: inherit;\">在5.x版本中删除了别名。每个图标都只有一个正式名称，其中一些名称已经根据新标准和约定进行了调整。</span></p><p><span style=\"white-space: nowrap; color: inherit;\">线框图标：</span><span style=\"white-space: nowrap; color: inherit;\">同样地，所有具有线框样式（通常以-o结尾）的图标现在都有一个far前缀，并且删除了它们的-o后缀。</span></p><p>以上就是小编体验过4.7版本升级5.11后，以及删除4.7直接使用5.11的感受，如果能够帮助到您，您可以将本文分享给其他web开发者。</p><p><span style=\"white-space: nowrap;\"><br/></span><br/></p><p><span style=\"white-space: nowrap;\"><br/></span></p><p><br/></p>', 'Fontawesome5.11图标可以根据您的需要使用任何颜色来显示。而Font Awesome专业版更支持全新的双色图标。这些漂亮的图标可以通过继承项目的基色来实现开箱即用，或者根据您的需要进行自定义。', 'Fontawesome5.11', 0, 1, 1, NULL, '', '', 1, 1571970451, 1571990479, 1);
INSERT INTO `article` VALUES (31, 1, '原创', '//cdn.dmake.cn/attachment/images/20191101/157258527610.jpg', '.NET代码生成器解析，10年前是这样解决数据底层和模型的自动生成的', NULL, '<p>\n	作为微软的亲儿子，.NET开发自然是微软系重要的开发语言，经过一代一代的框架迭代之后，.NET程序撑起了Windows桌面程序的大半边天。\n</p>\n<p>\n	作为新手开发者来说，对.NET基础其实并不好，甚至有软件相关专业的大学生毕业了也没有找到个门道，所以在这里还是要规劝专业开发者们一定要补足开发原理和开发语言理论，否则到最后你就不得不在百度上搜索“代码生成器”了。\n</p>\n<p>\n	10年前板砖才自学.NET开发，在面对对象、映射、数据库CRUD操作时的恐慌，为此采取了底层代码生成器这个方法。\n</p>\n<p>\n	回顾当时的困境，大致如此：\n</p>\n<p>\n	1. 常用代码要复用啊，每次写方法头都大了呀！\n</p>\n<p>\n	2. model类不喜欢写啊，为什么要做这么枯燥的工作，如果model类没有跟着数据及时更新，程序还要报错怎么办？\n</p>\n<p>\n	3. CRUD（增删改查）原理都一样，为何每次都要写一模一样的方法？\n</p>\n<p>\n	4. 面对不断增加的数据表和字段，怎么样才能及时更新底层代码？\n</p>\n<p>\n	<br />\n</p>\n<p>\n	因此，板砖在观察代码的构造以后，制作了一个通用工具，用以处理重复工作和底层代码的生成。\n</p>\n<p>\n	那么这个工具包具有哪些功能，又该如何使用呢？板砖为大家讲解一下：\n</p>\n<p>\n	<br />\n</p>\n<p>\n	1. 自动生成代码，如下方代码，在.NET项目中新增触发时间，自动生成MODEL层和DAL层，自定义DAL和BLL层可自行新增项目，代码生成效率只需一瞬间，100张数据表生成时间不到5秒\n</p>\n<p>\n	<br />\n</p>\n<p>\n	<img src=\"http://cdn.dmake.cn/attachment/keditor/image/20191101/20191101052313_13001.png\" alt=\"\" /> \n</p>\n<pre class=\"prettyprint lang-cs\">using System;\nusing System.Collections.Generic;\nusing System.Linq;\nusing System.Web;\nusing System.Web.UI;\nusing System.Web.UI.WebControls;\n\nusing Daneas.Utility.Universals;\n\nnamespace Newtec.Web\n{\n    public partial class init : System.Web.UI.Page\n    {\n        protected void Page_Load(object sender, EventArgs e)\n        {\n\n        }\n\n        protected void Button1_Click(object sender, EventArgs e)\n        {\n            new CodeGenerator().CreateModels();\n\n            Response.Write(\"model is finished\");\n        }\n\n        protected void Button2_Click(object sender, EventArgs e)\n        {\n            new CodeGenerator().CreateBaseDAL();\n\n            Response.Write(\"dal  is finished\");\n        }\n    }\n}</pre>\n<pre class=\"prettyprint lang-cs\">添加项目文件之后，需要定位和定义底层命名控件和文件地址</pre>\n<p>\n	2. 常用工具集，这个工具集就太多啦，包含了数据转换、映射等等功能，时间太久远了，就不再做介绍\n</p>\n<p>\n	<br />\n</p>\n<p>\n	感兴趣的朋友可以自行去我的CSDN账户下载：https://download.csdn.net/download/daneas/6288453\n</p>', '10年前板砖才初学.NET开发，在面对对象、映射、数据库CRUD操作时的恐慌，为此采取了底层代码生成器这个方法。', '.NET代码生成器解析，数据底层和模型的自动生成', 0, 1, 0, NULL, '', '', 1, 1572586186, 1572586198, 1);
INSERT INTO `article` VALUES (32, 1, '原创', '//cdn.dmake.cn/attachment/images/20191111/15734416911.png', 'Devexpress汉化工具机翻网络版，老板再也不用花钱买汉化啦', NULL, '<p>\n	板砖曾经写过3年的.NET程序，对Devexpress有点好感，目前因为板砖转移阵地到PHP上面啦，所以今天板砖就用PHP语言教大家如何汉化Devexpress插件<img src=\"https://www.dmake.cn/static/kindeditor/plugins/emoticons/images/16.gif\" border=\"0\" alt=\"\" /> \n</p>\n<p>\n	本次devexpress汉化仅针对Devextreme，不过汉化思路贯穿全局，有需要自己做其他插件汉化的同学可以加板砖QQ好友。\n</p>\n<p>\n	Devextreme的汉化文件是json文件，那就是太方便啦，板砖的汉化思路如下：\n</p>\n<p>\n	1. 采集需要汉化的词条\n</p>\n<p>\n	2. 存数据库\n</p>\n<p>\n	3. 翻译\n</p>\n<p>\n	4. 下载到本地\n</p>\n<p>\n	这个思路是不是很清晰，虽然如此，有很多同学要说了，无图无真相，于是乎不如直接上代码吧<img src=\"https://www.dmake.cn/static/kindeditor/plugins/emoticons/images/44.gif\" border=\"0\" alt=\"\" /> \n</p>\n<p>\n	第一步：采集词条保存到数据库，这个是思路啊，有关键步骤板砖阉割过了，没有体验怎么会记得住呢，汉化机翻工作千万不要边翻译边保存啊，几千条一次性的保存、翻译工作必然卡死服务器，不管是本地还是远程体验都不是很好，所以朋友们自行脑补如何分解保存。\n</p>\n<p>\n	<br />\n</p>\n<pre class=\"prettyprint lang-php\">        $file = \'./devexpress/jsonresources/dx-analytics-core.zh-Hans.json\';\n\n        $json_string = file_get_contents($file);\n        \n        $data = json_decode($json_string, true);\n                \n        $rows = Db::name(\'op_devexpress\')-&gt;insertAll($data);\n        \n        echo \'翻译完毕\';\n        exit();</pre>\n第二步就是翻译啦，翻译就很简单，我用的百度的翻译API，免费的，但是翻译速度很慢，这时候板砖又要出一个小题：几千页的翻译如何无人值守，要么自动任务，要么···看你的想法啦\n<p>\n	<br />\n</p>\n<p>\n	<br />\n</p>\n<pre class=\"prettyprint lang-php\">        $db = Db::name(\'op_devexpress\');\n        \n        $list = $db-&gt;where(\'mwords\', \'\')\n            -&gt;where(\'dvwords\', \'&lt;&gt;\', \'\')\n            -&gt;where(\'LENGTH(dvwords)&gt;10\')\n            -&gt;limit(10)\n            -&gt;select();\n        \n        if ($list == null) {\n            echo \'翻译完了\';\n            exit();\n        }\n        \n        foreach ($list as $d) {\n            $trans = translate($d[\'dvwords\'], \'en\', \'zh\');\n            if ($trans &amp;&amp; array_key_exists(\'trans_result\', $trans)) {\n                $vtrans = $trans[\'trans_result\'][0][\'dst\'];\n                $db-&gt;where(\'id\', $d[\'id\'])-&gt;update([\n                    \'mwords\' =&gt; $vtrans,\n                    \'translateid\' =&gt; 0,\n                    \'translate_time\' =&gt; time()\n                ]);\n            }\n        }\n        echo \'这一页翻译完了\'；</pre>\n关键的步骤都完了，剩下的就是保存汉化文件到本地了，这一步我认为没有什么难度，不过还是贴出我的方法吧，老规矩：板砖不会给你完整的代码，这就是一个找茬游戏！\n<p>\n	<br />\n</p>\n<p>\n	如果你直接运行下方代码，恭喜你中招了，你不会得到你想要的结果，为什么呢？因为板砖阉割了最严格的程序基础原理部分，如果你能补充那一行代码，恭喜你，devextreme的汉化工作你可以在30分内完成了，而且正确率高达75%呢。\n</p>\n<p>\n	<br />\n</p>\n<pre class=\"prettyprint lang-php\">        $file = \'./devexpress/jsonresources/dx-analytics-core.zh-Hans.json\';\n      \n        $list = Db::name(\'op_devexpress\')-&gt;where(\'dirpath\', $file)\n            -&gt;select();\n                    \n        $txt = \'\';\n        \n        $jsons=[];\n        foreach ($list as $l) {\n            $json[$l[\'dvid\']] = $l[\'translate_time\'] &lt;=0 ? $l[\'dvwords\'] : $l[\'mwords\']; array_push($jsons, $json); } $txt = json_encode($jsons,JSON_UNESCAPED_UNICODE); file_put_contents(str_replace(\'jsonresources\', \'json resources\', $file), $txt); echo \'生成完了\'.$file;</pre>\n当然，很多朋友看完这一篇都是一脸懵逼的，板砖在讲啥，怎么devextreme就被汉化啦？\n<p>\n	<br />\n</p>\n<p>\n	如果你想知道更多，或者就像简单的拿到devextreme汉化的文件，请加QQ554035954. 关于Devexpress Winforms汉化和Devexpress VCL汉化，请看下图···\n</p>\n<p style=\"text-align:center;\">\n	<img src=\"http://cdn.dmake.cn/attachment/images/20191111/15734416911.png\" alt=\"Devexpress汉化工具网络版\" /> \n</p>', '板砖跨专业推荐，Devexpress汉化工具你值得拥有！Devexpress是.NET开发中全球顶级UI插件，用于美化项目，它不仅效果出众而且价格昂贵，购买一套或者自己汉化一套都要花费不少时间金钱，板砖于是乎自己做了一个在线版本的汉化工具！', 'Devexpress汉化工具', 0, 1, 1, NULL, '', '', 1, 1573441735, 1574405630, 0);
INSERT INTO `article` VALUES (33, 2, '原创', '//cdn.dmake.cn/attachment/images/20191111/15734554316.png', '互联网平台运营=好的主题+漂亮的配图+优秀的创意+不厌其烦的渠道分发', NULL, '<p>\n	<br />\n</p>\n<p>\n	<img src=\"http://cdn.dmake.cn/attachment/images/20191111/15734554316.png\" /> \n</p>\n<p>\n	非金融属性的互联网平台，已经把生活完整的带入了手机和电脑的世界里。互联网的内容运营实在是太丰富了，而今天板砖主要讲的是平台运营的基础，真的是太基础了：标题、配图、关键词、描述和内容。\n</p>\n<p>\n	优秀的平台大多不同，优秀的内容千篇一律，那就是构成内容的5要素。\n</p>\n<p>\n	在刚才所述的5要素里面，其实生产的顺序是倒叙的：内容=》描述=》关键词=》配图=》标题。\n</p>\n<p>\n	板砖曾经做过两年的内容编辑，目前也带了一年的编辑和运营团队，经验里更加认同上述优秀内容产生的标准公式，接下来让我为您揭开大道至简的内容产生原理。\n</p>\n<p>\n	<br />\n</p>\n<p>\n	1. 内容=主题\n</p>\n<p>\n	板砖在17年的时候主导过一个京东众筹的项目——卖水果。这水果有什么好说的呢？甜一点？大一点？我还要卖你高价钱！\n</p>\n<p>\n	京东众筹的审核部门为我们提供了一个标准公式：20%的产品简介+60%的故事说明+20%的展望未来，妥妥的没问题！\n</p>\n<p>\n	从今天的双十一活动倒推线下活动召集，这个公式依然成立，比如说我们可以试试用100各字招募和邀约陌生男女参与一个酒吧的活动吧。\n</p>\n<p>\n	<br />\n</p>\n<p>\n<pre class=\"prettyprint lang-js\">活动简介：今天光棍节你准备好脱单了吗？你是选择形单影只的像条单身狗在家吃泡面还是来到荷尔蒙弥漫的得意坝坝展现雄姿引美女尖叫连连？\n\n活动故事（图文）：引人想入非非的异性/酒会/荷尔蒙碰撞/牵手的遐想/灯光的蔓延都是一个好故事。\n\n展望未来：今天你难道要错过这1/365的机会吗？ 牵手的喜悦/礼品与奖品的欢愉/可以想象的不夜夜。</pre>\n</p>\n<p>\n	<br />\n</p>\n<p>\n	2. 描述=精华流露\n</p>\n<p>\n	现在的网文一篇就不会低于1000字，但是优秀的文案只会给你一句话的抽象时间，这里板砖就要推荐一本书了《爆款文案》，虽然我是不相信作者的实力的，但是我认为写作思路如此。\n</p>\n<p>\n	<br />\n</p>\n<p>\n	3. 关键词=给自己的初心\n</p>\n<p>\n	当编辑们写作已经有一段时间了，那么文笔一定可以飞跃畅想了，如果陷入了无边无际的联想中，那么内容是经不起延长的。关键词，及时把作者思绪围在一个范围内，同时也告知搜索引擎内容重点。\n</p>\n<p>\n	<br />\n</p>\n<p>\n	4. 配图=给读者视觉上的联想启发\n</p>\n<p>\n	如何让人能够认同喝了牛奶就会健康？如何让人能够错觉的认为涂了护肤品皮肤就能变得更好？不用一个字，当一张图片同时表达出这两种状态的时候，读者自然就相信了，这就是配图的力量！\n</p>\n<p>\n	<br />\n</p>\n<p>\n	5. 标题=有力的呐喊\n</p>\n<p>\n	我们积蓄已久的故事感慨和内心澎湃，光是用文字和图片不足以点燃读者的热血，那就唯有用一句口号，彻底的打破它。\n</p>\n<p>\n	<br />\n</p>', '板砖在互联网种经营了约10年的光景，看过了不少优秀和稍弱的文案与内容处理方式，大大的赞同大道至简的互联网运营方式，而在内容运营种，更是如此。', '互联网平台，平台运营，渠道分发', 0, 1, 1, NULL, '', '', 1, 1573456735, 1573457203, 0);
INSERT INTO `article` VALUES (34, 2, '原创', '//cdn.dmake.cn/attachment/images/20191111/15734595603.png', '编辑思维进阶篇——用创新帮助编辑快速高效的完成日常的编辑工作', NULL, '<h3>\n	创新的起源\n</h3>\n<p>\n	大多创新的起源都是为了偷懒\n</p>\n<div>\n	蒸汽机的发明让人不用守着劳力和牲畜工作\n</div>\n<p>\n	<br />\n</p>\n为了不听噪音，也不用自己打扇子，空调被研发出来了<br />\n为了让美女们花更少的时间P出更漂亮的照片，从而有了美图秀秀<br />\n互联网爬虫的产生让更少的编辑去抄袭更多的内容<br />\n近10年来，为了减少程序猿的工作量而发明了不少自动编码软件、开发框架<br />\n大数据的产生让人不用花更多时间盯着表格数据花时间做分析<br />\n一些厉害的互联网公司的员工不想去公司上班，于是公司改变了员工考核方式、让员工在自己喜欢的地方工作<br />\n·········<br />\n各行各业，大家为了偷个懒，花了不少时间和精力在创新上，改变了使用的工具，改变了思考的方式，改变了工作习惯，但是——只有勤奋的人才有资格偷懒。<br />\n<h3>\n	创新和编辑思维的关系\n</h3>\n<p>\n	思维是创新的催化剂，编辑的工作其实也是创作的工作<br />\n<br />\n编辑的第一件事是分析编辑目的<br />\n——我为什么要写这个文章，我想发出这个文章后要获得什么样的结果，我应该从哪个角度去写，我应该搜集什么样的素材<br />\n<br />\n编辑的第二件事是形成团队的写作风格<br />\n——图片如何处理、段落如何划分、标题的构成是什么样的公式、文章详情必须存在的要素是什么<br />\n——团队写作风格就是标准，标准是需要根据团队的工作经验的提升而修订优化的\n</p>\n<p>\n	<br />\n编辑的第三件事是观察、分析和总结<br />\n——观察是指回顾个人写长时间写作的结果，流量是否有变化，变化程度<br />\n大不大<br />\n——分析是指选出具有代表性的作品，例如成绩最差的和成绩最好的，通过对比封面图、标题组成、关键词、描述、段落划分、要素等部分，了解好与不好的区别。 <br />\n如果在分析阶段发现以上内容都是一样的啊，没有多大区别的时候，就需要往上层继续推敲和对比了：我写这个作品的目的是什么，想得到什么结果，素材是否符合主题，人群选择是否正确，应用环境是否正确\n</p>\n<h3>\n	创新如何开始\n</h3>\n<p>\n	如何做好工作计划：月计划、周计划、单日工作规划<br />\n做好工作计划是顺利完成工作的前提条件，按照工作计划稳步推进可以减少个人惆怅的时间，可以让工作有序，可以让人在工作中不会因为发蒙而感到烦躁。<br />\n如何做好学习计划：学习计划、实践计划<br />\n做好学习计划是为了弥补个人工作能力欠缺、择业方向少等不利于个人职业规划而制定的短、中期业务能力升级的计划；<br />\n实践计划是围绕个人学习结果验证、推断验证等直接方式，没有经过实践的一切工作、学习都是没有价值的。<br />\n从实践结果中总结和创新<br />\n在实践结果中总结可以优化的步骤、可以复用的方法、可以抛弃的陈规、可以共享的创意、可以利用的工具、可以表达的公式。\n</p>\n<p>\n	<br />\n</p>\n<h3>\n	如何做好工作计划\n</h3>\n<div>\n	月计划\n</div>\n<p>\n	<br />\n</p>\n<p>\n	月计划主要是针对接下来的一个月，在未收到主管工作任务之前就要做的计划，主要分为：<br />\n工作总结：总结上一个月和本月的工作业绩，对比季度数据，提炼个人/团队的优秀案例与失败案例，做好分析与总结。<br />\n核心工作：通过工作总结，制定出重点的运营计划或者是内容发布计划，包含数量与发布节点等，参考《平台部营销事件执行流程.png》<br />\n普通工作：除核心工作以外，给普通的厂商/社群运营/百度推广做好维护时间节点排班<br />\n计划外时间预留：预计可能发生的事件预留事件，一个月通常会存在一次<br />\n周计划<br />\n为了达成月计划，按天给月计划排班<br />\n单日工作规划<br />\n为了完成周计划，将每天分为几个时间段，分别完成当日的工作计划，注意每天最大限度的预留一个小时左右应对当日突发状况。<br />\n</p>\n<h3>\n	技能提升计划\n</h3>\n学习目的——想解决在工作中的具体问题<br />\n学习的具体项目——找到合适的学习教程或方向<br />\n边学习遍实践——快速的在具体问题中成长起来<br />\n新学科学习计划<br />\n职业规划是什么——避免学一些无用内容<br />\n职业特征是什么——找到学习的范围<br />\n自我训练——自己给自己设置“命题作文”式的任务，尽可能多的模拟训练<br />\n实践与总结<br />\n我在技能提升和新学科的学习中学到了什么，掌握到了什么程度<br />\n我学到的东西可以在那些时候反复训练（列一个实践计划清单）<br />\n在学习中因为哪种方法让我学习和训练效果最好（记本本上）\n<p>\n	<br />\n</p>', '各行各业，大家为了偷个懒，花了不少时间和精力在创新上，改变了使用的工具，改变了思考的方式，改变了工作习惯，但是——只有勤奋的人才有资格偷懒。', '编辑', 0, 1, 0, NULL, '', '', 1, 1573460047, 1573460149, 0);
INSERT INTO `article` VALUES (39, 1, '原创', '//cdn.dmake.cn/attachment/images/20191124/15746000393.jpg', '微信公众号吸粉之口令篇，用Easywechat做文字口令和语言口令', NULL, '<p>\n	我们首先来确认一遍为什么网友会喜欢口令红包的游戏思路：\n</p>\n<p>\n	1. 娱乐性质\n</p>\n<p>\n	2. 利益诱惑\n</p>\n<p>\n	关于娱乐的性质，把公众号吸粉如投票、转发这种体力活变成找茬、寻宝等娱乐活动，自然会降低一点网友的排斥心理；而利益诱惑，简单来说就是尝试30秒获得一个现金红包，不用邀约1000好友帮忙拼夕夕，也不用建群盖楼，回复口令即有机会立即获得一个大于1块钱的现金红包，这也是为什么吸粉很快。\n</p>\n<p>\n	当然，这个功能吸纳的关注可能不是你的精准用户，这里推荐一篇卢松松博客《你这一辈子，有没有为五毛钱拼过命？》，会打开你对利益营销的思想大门！\n</p>\n<p>\n	接下来切入正题，口令红包需要在服务号的基础上体验最完善，订阅号只能做“文字口令”。原理直接上代码！\n</p>\n<p>\n	<br />\n</p>\n<pre class=\"prettyprint lang-php\">$server = $this-&gt;wxapp()-&gt;server;\n        \n        $server-&gt;push(function ($message) {\n            \n            $wxid = $message[\'ToUserName\'];\n            $openid = $message[\'FromUserName\'];\n            \n            switch ($message[\'MsgType\']) {\n                case \'text\':\n                    $text = new ServiceProcess();\n                    return $text-&gt;processPutTxt($wxid, $openid, $message[\'Content\']);\n                    break;\n                case \'voice\':\n                    \n                    if ($message[\'Recognition\']) {\n                        $regstr = \'/[[:punct:]]|[[:lower:]]|。|，|,/i\';\n                        $word = preg_replace($regstr, \'\', $message[\'Recognition\']);\n                        $one = Db::name(\'wx_luckcode\')-&gt;where(\'code\', $word)\n                            -&gt;where(\'gh_id\', $wxid)\n                            -&gt;find(); // 还剩余多少现金红包\n                        \n                        if ($one != null &amp;&amp; $one[\'leftnum\'] &gt; 0) {\n                            $group = new ServiceLuckmoney();\n                            return $group-&gt;ProcessCode($wxid, $word, $openid);\n                        }\n                        \n                        if (strstr($word, \'天王盖地虎\') || strstr($word, \'口令\')) {\n                            $word = str_replace(\'天王盖地虎\', \'\', $word);\n                            $word = str_replace(\'口令\', \'\', $word);\n                            $word = str_replace(\' \', \'\', $word);\n                            $group = new ServiceLuckmoney();\n                            return $group-&gt;ProcessCode($wxid, $word, $openid);\n                        } else {\n                            $text = new ServiceProcess();\n                            return $text-&gt;processPutTxt($wxid, $openid, $word);\n                        }\n                    } else {\n                        return \'亲，我只是个机器人啊，听不懂你说什么啊！\';\n                    }\n                   break;</pre>\n以上代码示例讲述了一个利用语音做口令红包的基础配置：语音转化成为文字，文字去触发获得红包的机会。\n<p>\n	<br />\n</p>\n<p>\n	当然，如果你希望每一个参与者都获得红包的机会，那么你的成本可能会很高，所以在触发获得红包的机会的时候，希望你能够用随机概率或者稳定的红包兑现规律。\n</p>\n<p>\n	以下为我如何利用随机数拒绝口令和利用规律发放红包的实现，该方法为可行的直接实现方式，你可以通过各项判断条件揣摩出具体的模块设计思路：\n</p>\n<p>\n	<br />\n</p>\n<pre class=\"prettyprint lang-php\">        if (rand(1, 10) == 8) {\n            return new Text(\'现在抽奖人数太多，请稍后再试！\');\n        }\n        \n        $tao = \'/太阳/太阳/太阳  铜梁视窗合伙人计划招募中&lt;a href=\"https://mp.weixin.qq.com/s/seuptyomIZYaHZ35j6dlsA\"&gt;#点击了解详情#&lt;/a&gt;\';\n        \n        $one = $this-&gt;where(\'code\', $code)\n            -&gt;where(\'gh_id\', $gh_id)\n            -&gt;find(); // 还剩余多少现金红包\n        \n        if ($one == null) {\n            return new Text(\'您的口令错误【\' . $code . \'】！请在本公众号文章中找寻正确口令\');\n        }\n        \n        if ($one[\'starttime\'] &gt; time() || $one[\'endtime\'] &lt; time()) {\n            return new Text(\'口令使用时间为\' . date(\'Y-m-d H:i:s\', $one[\'starttime\']) . \'至\' . date(\'Y-m-d H:i:s\', $one[\'endtime\']));\n        }\n        \n        if ($one[\'totalnum\'] == 0 &amp;&amp; $one[\'leftnum\'] == 0) {\n            return new Text(\'现在的口令是:\' . $one[\'othercode\']);\n        }\n        \n        if ($one[\'leftnum\'] &lt;= 0) {\n            \n            $othercode = $tao;\n            if (strlen($one[\'othercode\']) &gt; 1) {\n                $othercode = \'\n今天的红包早早被领完，要不咱们再加一个？\n试试 \' . $one[\'othercode\'];\n            }\n            \n            return new Text(\'今日口令\"\' . $code . \'\"红包\' . $one[\'totalnum\'] . \'个已领完。\n请关注明日微信公众号推文，寻找当天口令！\n您也可以点击=&gt;&lt;a href=\"\' . $one[\'jumpurl\'] . \'\"&gt;#这里#&lt;/a&gt;了解他们的活动详情\n加客服daneas2014详询活动日程和诸多好物推荐！\' . $othercode);\n        }\n        \n        $db = Db::name(\'wx_luckcodelog\');\n        \n        $logs = $db-&gt;where(\'code\', $code)\n            -&gt;where(\'openid\', $openid)\n            -&gt;where(\'gh_id\', $gh_id)\n            -&gt;count();\n        \n        if ($logs &gt;= $one[\'maxnum\']) {\n            return new Text(\'您今天的口令次数已耗尽（共\' . $one[\'maxnum\'] . \'次），还剩余\' . $one[\'leftnum\'] . \'个现金红包哟，赶紧邀约你的好友参与吧\' . $tao);\n        }\n        \n        $log = $db-&gt;where(\'code\', $code)\n            -&gt;where(\'openid\', $openid)\n            -&gt;where(\'gh_id\', $gh_id)\n            -&gt;where(\'money\', 1)\n            -&gt;find();\n        \n        // 生成现金红包\n        if ($log == null) {\n            \n            if ($one != null &amp;&amp; $one[\'leftnum\'] &gt; 0 &amp;&amp; ($db-&gt;where(\'code\', $code)-&gt;count()) % $one[\'rate\'] == 0) {\n                \n                $type = $one[\'leftnum\'] &gt; 3 ? rand(1, 2) : 1;\n                \n                if ($type == 2 &amp;&amp; rand(1, 4) == 4) {\n                    $type = 2;\n                } else {\n                    $type = 1;\n                }\n                \n                if ($type == 1) {\n                    \n                    $this-&gt;where(\'code\', $code)-&gt;setDec(\'leftnum\', 1);\n                } else {\n                    \n                    $this-&gt;where(\'code\', $code)-&gt;setDec(\'leftnum\', 3);\n                }\n                \n                $db-&gt;insert([\n                    \'openid\' =&gt; $openid,\n                    \'code\' =&gt; $code,\n                    \'gh_id\' =&gt; $gh_id,\n                    \'money\' =&gt; 1,\n                    \'createtime\' =&gt; time()\n                ]);\n                \n                return new Text(self::CreateLuckM($gh_id, $openid, $type));\n            }\n        }\n        \n        // 生成券包\n        $db-&gt;insert([\n            \'openid\' =&gt; $openid,\n            \'code\' =&gt; $code,\n            \'gh_id\' =&gt; $gh_id,\n            \'money\' =&gt; 0,\n            \'createtime\' =&gt; time()\n        ]);\n        \n        $random = rand(0, 1);\n        \n        if ($random == 0) {\n            return new Text(\'(⊙o⊙)… 这次你啥也没中，继续关注本活动总会有搞着的\' . $one[\'jumpurl\']);\n        }\n        \n        return new Text(self::CreateLuckC($gh_id, $openid, $one[\'jumpurl\'], $one[\'jumpword\']));</pre>\n最后关于“<span>CreateLuckC</span><span id=\"__kindeditor_bookmark_start_17__\"></span>”，这个方法是具体的如何发放红包功能，可以通过配置微信支付链接，用户点击即可获得，例如下方效果，感兴趣的朋友们可以复制代码体验一下，备注Easywechat请使用最新版本哟。\n<p>\n	<br />\n</p>\n<p>\n	<img src=\"http://cdn.dmake.cn/attachment/images/20191124/15746000393.jpg\" />\n</p>', '现在微信公众号吸粉是越来越难，主要在于网友已经玩坏了各种花样，而且企业能拿出来的花样也越来越少，今天我将分享一下我是怎么样用30元获得3000粉丝的实战经验。', '微信公众号，口令', 0, 1, 0, NULL, '', '', 1, 1574601340, NULL, 0);
INSERT INTO `article` VALUES (35, 2, '原创', '//cdn.dmake.cn/attachment/images/20191112/15735586998.png', '优秀文案不能流水线产出，只有走心和投入才有可能', NULL, '<p>\n	到底什么是文案？\n</p>\n<p>\n	板砖在职场中至少摩挲了3年，终于想起问问自己这个问题！\n</p>\n<p style=\"text-align:center;\">\n	<img src=\"http://cdn.dmake.cn/attachment/images/20191112/15735586998.png\" alt=\"优秀文案写作\" /> \n</p>\n<p>\n	板砖在一家IT平台担任运营经理一职的时候，往往因为编辑的写作能力而感到焦虑：为什么编辑们一直很努力，一直也在找规律，但是就是写不出好的文案来？\n</p>\n<p>\n	文案难写吗？当然难写，文案不是字面意义上的“COPY”，也不像其他文本有固定模板可以套用，也不是机械式的流水线就能加工出来，也没有一个放之四海而皆准的标准审美。\n</p>\n<p>\n	但是文案真的男鞋吗？好像又不是那么难写！\n</p>\n<p>\n	写好文案需要几步呢？板砖认为在没有时间压力的环境下和具备一定知识储备的情况下写好文案需要这几步：\n</p>\n<p>\n	1. 构思文案主题\n</p>\n<p>\n	2. 分解文案构成\n</p>\n<p>\n	3. 针对属性罗列关键词\n</p>\n<p>\n	4. 尝试多角度还原主题\n</p>\n<p>\n	国内知名广告公司<span style=\"color:#444444;font-family:\" font-size:15px;background-color:#ffffff;\"=\"\">奥美广告副董事长叶明桂在他的著作里提到过“文案就是马屁精”，这句话不是让你去拍甲方的马屁，而是去拍受众的马屁，去费劲心思的琢磨受众在想什么，然后说他们爱听的话和想知道的话。</span> \n</p>\n<p>\n	在广告圈儿里，很多人对杜蕾斯的广告文案深感愉悦，我们接下来解析杜蕾斯是如何把国人难以开口的物件写的清新脱俗的。\n</p>\n<p>\n	众所周知，杜蕾斯是做情趣用品的，下方文案和配图简洁意骇，引人联想，我们来拆分文案的构成：\n</p>\n<p>\n	1. 主题：情趣跳蛋\n</p>\n<p>\n	2. 文案构成：用人联想的能力去替代一般广告中需要事务画面，毕竟谁会把使用跳蛋的画面公开呢\n</p>\n<p>\n	3. 产品属性和关键词：对情愫稍有介入的朋友们肯定知道，此类产品是辅助类产品，用于刺激女性的感官，通常在high top time时从声音上和物理上达到一定的效果，但是对男性刺激最为大的是声音，物理上的可能是惊吓或者喜悦吧\n</p>\n<p>\n	4. 通过上述3个参考要素来讲，这类产品需要一句话文案肯定是不能用画面示人的，那如何体现出产品属性和关键词呢，我个人对杜蕾斯的文案人员的文学造诣还是很佩服的。从我个人粗俗的想象画面可以得出“娇喘连连”“此起彼伏”等毫无文学造诣的词汇，但是“从未如此悦耳”，寥寥六字，将使用体验和产品对比一下子给人一个画面感。\n</p>\n<p>\n	总结，运营人时常说，文案不是写给自己看的，所以你的思维站点就很重要。文案人要削尖了脑袋去思考不同受众所思所想，才能找到角度去引发共鸣。\n</p>\n<p>\n	沃顿商学院营销学教授乔纳伯杰曾说过：“只要把一些有唤醒作用的情绪元素，加入到故事或者广告中，就能激发人们的共享意愿。”\n</p>\n<p style=\"text-align:center;\">\n	<img src=\"http://cdn.dmake.cn/attachment/keditor/image/20191112/20191112195759_70983.jpg\" alt=\"\" width=\"350\" height=\"560\" title=\"\" align=\"\" /> \n</p>', '中国越来越快的互联网成败节奏，催动着整个行业大多数老板把文案编辑流水化作业当做常态，然而文案却是日薄西山！', '文案', 0, 1, 1, NULL, '', '', 1, 1573559494, 1573560673, 0);
INSERT INTO `article` VALUES (36, 1, '原创', '//cdn.dmake.cn/attachment/images/20191115/15737952212.png', '做系统功能性和模式的延展，是另辟蹊径还是原地飞升', NULL, '<p>\n	在写本篇文章的时候，板砖已经用过.NET和PHP做过多个、多种系统了，也是为什么板砖博客期望将自己的项目经历产品化的原因。\n</p>\n<p>\n	我们首先来确认一件事儿，那就是大版本升级“必遭雷劈”！\n</p>\n<p>\n	在大版本升级、去冗余化、以及跨语言平台升级（产品重构）中，我们技术人员将遇到令人发指般的500错误一大框，究其原因可以总结为以下几点：\n</p>\n<p>\n	1. 路由规则适配的缺失，原系统或许有10个路由规则，另外隐藏了5条路由规则，当你升级重构的时候就会发生诸多404或500错误，这个时候不仅要快速去找问题，更有可能快速还原一个功能模块出来，\n</p>\n<p>\n	2. 资源的缺失，不过这类还算比较人道，资源缺失不外乎路径不对，找出来，覆盖上去就完结了，\n</p>\n<p>\n	3. 产品重构，相当于重新审视一遍项目可能性，从并发到数据关联程度到数据库的冗余，在一次短暂而快速的重构中设计到<strong>数据库表的拓展、数据转移、删减</strong>，记住这3个步骤，在项目平移之前请练习20遍，工作量之大，令人发指，在数据库的更新完成后，请不要参考老代码，请不要参考老代码，请不要参考老代码！重要的事情说3遍，不少老代码不够好，而是新的实现思路可以提升各种效率。\n</p>\n<p>\n	那么做系统功能性和模式的延展，是另辟蹊径还是原地飞升？\n</p>\n<p style=\"text-align:center;\">\n	<img src=\"http://cdn.dmake.cn/attachment/images/20191115/15737952212.png\" alt=\"系统升级\" /> \n</p>\n<p>\n	板砖建议：看清楚形势再上菜！\n</p>\n<p>\n	我们可以从以下条件中去评估是重构系统还是原系统升级：\n</p>\n<p>\n	1. 在面对复杂的运营环境中，原系统核心模块是否可以抵挡高流量、高调整，是否能够对未来可以预见的5年做好技术支撑，如果不行，请重构！\n</p>\n<p>\n	2. 虽然目前的IT市场人满为患，但是一个小众的语言或者高精尖的语言并不适合财务压力较大的公司，如果我们的系统如此，公司财务压力如此，请重构！\n</p>\n<p>\n	3. 原系统通过各种补丁升级，目前已经遇到了严重的维护压力，并且不能保证系统稳定的情况下，请重构！\n</p>\n<p>\n	4. 通过新技术，可以在财务和运营效率上解决很大问题的时候，并且有时间和团队支持重构的情况，请重构！\n</p>\n<p>\n	<br />\n</p>\n<p>\n	板砖博客在运营、技术的解释中会一直延续言简意赅的风格，能少说一句绝对不会再加一句修饰词，感兴趣的朋友可以一直关注板砖动态，如果想知道板砖博客改版进度的朋友们可以加我QQ获取源码和讨论：QQ 554305954\n</p>', '板砖在2019年经历了一场规模巨大的系统升级，经过考察了甲方爸爸的系统构成和冗余之后决心一次性铲除，并在原有业务逻辑不变的基础上做巨大升级，工作好比把歼7直接升级到歼16，对于这件事儿我有话说！', '系统升级', 0, 1, 1, NULL, '', '', 1, 1573796000, 1573796168, 1);
INSERT INTO `article` VALUES (37, 2, '原创', '//cdn.dmake.cn/attachment/images/20191117/15739766833.jpg', '电商平台真的还是停留在卖货的阶段吗？不，电商早已开启带货模式！', NULL, '<p>\n	2019年，在实体行业几经磨砺的一年里， 除了TJP三大电商平台大放光辉，诸多实体店铺也开始敞开心扉接收电商模式，不仅仅是接受电商卖货，也开始了电商带货的节奏。\n</p>\n<p style=\"text-align:center;\">\n	<img src=\"http://cdn.dmake.cn/attachment/images/20191117/15739766833.jpg\" width=\"480px\" /> \n</p>\n<p style=\"text-align:center;\">\n	<br />\n</p>\n<p style=\"text-align:left;\">\n	刚刚过去的双11，天猫、京东、苏宁等传统电商平台纷纷亮出了自己的成绩单，阿里以2684亿再次刷新了这个购物狂欢节的新纪录。在这个时候诸多实体门店也只是跟着眼红了一把。如果你是经常逛街的群体，其实你也能看到大街小巷的双十一促销广告，但是为什么线上电商就是要比门店折扣热闹呢，即便是线下门店折扣或许比线上还大，为什么就没有人愿意来了呢？\n</p>\n<p style=\"text-align:left;\">\n	早期，电商通过价格优势完胜实体店，并以这个优势领先了多年，给实体店造成一种“电商打压实体店生成空间”的假象。\n</p>\n<p style=\"text-align:left;\">\n	进入正题，从微信大规模普及和快递大规模提速之后，电商已经从价格优势“卖货”往“熟人带货”方向转移了！\n</p>\n<p style=\"text-align:left;\">\n	让我们从小众圈子，到企业“熟人带货”，再到平台“带货模式”3个方面讲解，一定能够找到自己的体验感。\n</p>\n<p style=\"text-align:center;\">\n	<img src=\"http://cdn.dmake.cn/attachment/keditor/image/20191117/20191117155316_55237.png\" alt=\"\" /> \n</p>\n<p style=\"text-align:left;\">\n	上图是真实社区、同城打造的微信带货群，有些个人群体通过渠道找到价优质廉的产品，通过微信社群网罗了很多购物人群，基本上是女性（大街上扫码求关注就是这类社群的起点）。社群分工明确，拉人头进群的拉人，找货源的找货源，群内直播带货的直播，嫣然一条完整的销售链条，据板砖身边的一个团长介绍，平时带货一个月可以做到个人收入超过1万元，而整个团队超过20人，可见月销售额之惊人！\n</p>\n<p style=\"text-align:center;\">\n	<img src=\"http://cdn.dmake.cn/attachment/keditor/image/20191117/20191117160427_39086.png\" alt=\"\" /> \n</p>\n<p style=\"text-align:left;\">\n	上图是实体店铺的公众号，在前年该公众号还是一个“卖货平台”，主要是以促销、商城销售为主，月销售额约15万左右，然而在今年5月份起，公司改变了网销侧重点和经营模式，在传统的“卖货”手段之外，大胆的启用了销售明星、会员互动等模式。销售明星建立起个人影响力辐射门店地区周边，建立起与顾客之间的信任感；而会员活动也跟着拼多多的步伐在“邪路上越走越远”，2019年日常公众号月销售额约为30万。\n</p>\n<p style=\"text-align:center;\">\n	<br />\n</p>\n<p style=\"text-align:center;\">\n	<img src=\"http://cdn.dmake.cn/attachment/keditor/image/20191117/20191117155845_97272.png\" alt=\"\" /> \n</p>\n<p style=\"text-align:left;\">\n	上图依然是同城社群中褥羊毛党的聚集地，看到这些微信求砍价、盖楼、领券、体现的词汇相信大家都很熟悉，2019年包含了淘宝、京东、拼多多等巨头电商在这一轮的社群营销中占据了绝大多数席位，社群中除了职业团体外，以家庭和朋友圈为核心力量将“带货”和促销关注、传播和转化做到了极致，逐渐“社群电商”取得了极大的成功。\n</p>\n<p style=\"text-align:left;\">\n	云集和拼多多仅仅是2017年至2018年间社交驱动电商爆发增长的典型代表。据不完全统计，2018年内相对规模较大的以社交驱动的电商平台就有拼多多、云集、有赞、未来集市、微盟、礼物说、万色城等超过10家，2018年社交电商融资总金额超过200亿元。《2018年中国移动社交电商发展报告》数据显示，社交电商自2013年出现后连续五年高速发展。具体来看，2017年社交电商行业市场规模达到6835.8亿元，同比增长88.84%。\n</p>\n<p style=\"text-align:left;\">\n	相比之下，天猫、京东、苏宁易购等综合电商平台，在2018年12月的月独立设备数是77.4千万台，同比上涨2.38%，月人均使用时长是137.4分钟，同比上涨2.08%，月人均使用次数是35.1次，同比下跌了2.77%。这一涨一跌的数据，或许可以佐证随着消费者上网模式的变化，传统的综合类电商平台常用的“搜索”驱动模式逐渐衰弱。而以社交分享模式为主的分享时代已经到来。以拼多多为例，其最初的增长都是依赖于微信、QQ群等用户之间的互相分享，通过拼团的模式达到用户扩张的目标。在这一片未被主流电商平台开垦的市场内，拓荒出一片肥沃的土壤，滋养拼多多的上市计划。而拼多多最大的贡献实际在于改变了传统意义上电商平台以搜索、广告为核心的曝光模式。拼团的核心是社交分享，基于消费者的社交圈，无形中为促销商家带去了一批潜在客户，这种互动感和裂变式传播性在传统电商打法中严重缺乏，而这与移动端的电商环境必不可分。本质上来说，拼多多的全新打法，实现了电商去中心化。也让市场看到了电商发展的多种可能性。随后，云集以“0元开店”体验会员计划，单日新增体验会员突破700万。最终也成功到纳斯达克敲钟。\n</p>\n<p style=\"text-align:left;\">\n	<br />\n</p>\n<p style=\"text-align:left;\">\n	那么，社群运营中“带货”这种模式还能走多远？\n</p>\n<p style=\"text-align:left;\">\n	虽然各平台、企业和社群甚至政府机构对社群电商抱有支持和深度开拓的计划，但是排除掉支持的部分，平台保护机制也在逐步的将小平台和品牌瓦解掉！\n</p>\n<p style=\"text-align:left;\">\n	无论是微信限制使用小程序或服务号在社群中“拉新”的数据标志以外，还是各自媒体平台也逐渐将品牌营销展示的力度逐步压制，都标志着“社群电商”需要抱大腿的潜规则变明规则，特别是在微信上，除了拼多多等腾讯系营销号和应用能够肆无忌惮的疯狂生长，其他品牌或小平台根本无招架之力。\n</p>\n<p style=\"text-align:left;\">\n	基于阿里平台、腾讯平台等大型流量平台的店主和应用，或将成为这场博弈中的最终幸存者！\n</p>\n<p style=\"text-align:left;\">\n	<br />\n</p>\n<p style=\"text-align:left;\">\n	目前，短视频平台还是带货生力军！\n</p>\n<p style=\"text-align:left;\">\n	在早期的淘宝直播中，以销售为目的的纯带货模式，以及到以内容营销如微博、博客模式的知识分享带货，再到现在诸如抖音、小红书等等一系列短视频新兴平台中故事情节带货，都挖掘出了生活中带货可能性。\n</p>\n<p style=\"text-align:left;\">\n	<br />\n</p>\n<p style=\"text-align:left;\">\n	然而，我们还有多少可以挖掘的带货方法呢？板砖认为，在互联网市场和实体销售链中，根据销售群体、市场区域、技术支持等多种“硬软环境”下，通过自身的审视和努力，在新的销售和带货模式出现和成熟之前，<strong>电商之路会越来越专业化、数据可视化</strong>，掌握了知识和数据灵活应用的个人、团体和企业，必将在电商变革之路中存活和壮大。\n</p>\n<p style=\"text-align:left;\">\n	<br />\n</p>\n<p style=\"text-align:left;\">\n	本文部分摘抄于“运营人”博客。\n</p>\n<p style=\"text-align:left;\">\n	<br />\n</p>\n<p style=\"text-align:left;\">\n	<br />\n</p>\n<p style=\"text-align:left;\">\n	<br />\n</p>', '板砖入电商时间稍晚，曾经也长期停留在不愿改变现状的态度下，错失了电商从卖货阶段到带货阶段的实操爆炸的阶段。', '电商平台，带货模式', 0, 1, 1, NULL, '', '', 1, 1573978655, 1573979440, 1);
INSERT INTO `article` VALUES (38, 2, '转帖', '//cdn.dmake.cn/attachment/images/20191122/15743914580.jpeg', '技术为王？业务为王？运营为王？中台告诉你快速应对业务变化才是王', NULL, '<p>\n	本篇文章引用了“ITPUB”近期发布的一篇文章《要不要赶个时髦，去建设一个「 中台 」？》，这篇文章很好的解答了板砖心中的这个疑问，并且具备一定的专业性质，也开启解决了板砖的部分疑惑！\n</p>\n<p>\n	在和企业打交道中，一旦面临新的业务变更时，大多数企业有以下3中感慨：\n</p>\n<pre class=\"prettyprint\">有的企业反馈说：我们的小组总是在重复同样的工作，但是如果有新项目的情况下，项目经理、技术小组、销售小组和运营小组等会重新分配人手，这样做我们的项目可以快速推进，而且成本开销不大，甚至能够在新小组成立之时没有额外成本开销。\n\n有的企业反馈：我们公司一旦成立新的项目，那么就要招聘新的项目经理，配置新的技术等等，成本开销实在是太大了。</pre>\n<p style=\"text-align:left;\">\n	其实这里就出现了企业中台的模型，让我们来分解一下中台到底是个什么鬼，中台需要做什么，中台到底是怎么运行的，那些企业适合、不适合建立企业中台。\n</p>\n<p style=\"text-align:left;\">\n	一、中台到底是什么\n</p>\n<p style=\"text-align:left;\">\n	中台在板砖和一系列的程序员、架构师的概括中可以理解为“代码复用”，在销售的概括中可以理解为“销售流程复制”，在运营的概括中可以理解为“运营模式复用”。然而这个中台模式，就是将以上（还有更多岗位）各岗位的模式重新组合，打造新的产品、项目。\n</p>\n<pre class=\"prettyprint\">中台 就是一个架构理念，它是一个企业的指挥体系，它是希望将一些可复用的“能力”统一起来，采用共享的方式去建设，用来解决各个业务团队重复开发、数据分散、试错成本高等问题，中台的核心就是**“对能力的共享”、“对能力的复用”**，它应该是公司内部的统一协同平台。</pre>\n很多人一看到平台就认为是个网站或者是一个应用，但是“中台平台”是一个看不见的企业指挥体系，想象一下“董事会”、“评价委员会”等，这一些平时看不见人，当出现特别事情的适合会组建的临时组织。\n<p style=\"text-align:left;\">\n	二、中台需要做什么\n</p>\n<p style=\"text-align:left;\">\n	根据“中台到底是什么”的铺设，板砖简单来介绍中台到底需要做什么这个概念。\n</p>\n<pre class=\"prettyprint\">中台就是独立职能部门通过“业务组合”的方式，新建立起一个包括各项职能的临时或中短期的小组，该小组听命于中台指挥。\n其特点为：职能全部涵括，短期、有指挥层和执行层。\n那么中层的指挥层就应该是职能部门中具有管理权限的人员组成，其余的是业务执行人员。</pre>\n中台搭建，具体的是协调和组织项目启动、落地和达成项目预期。\n<p style=\"text-align:left;\">\n	从下图看阿里云的企业中台框架可以看到，写代码的写代码，做营销的做营销，做财务的做财务，唯独“中台”被划分为了多种“业务中台”，其实这个中台就是在所有实际执行链条中起到协调和推波助澜作用的。\n</p>\n<p style=\"text-align:center;\">\n	<br />\n</p>\n<p style=\"text-align:center;\">\n	<img src=\"http://cdn.dmake.cn/attachment/images/20191122/15743914580.jpeg\" alt=\"阿里云中台架构示例\" /> \n</p>\n<p>\n	三、中台到底是怎么运营的\n</p>\n<p>\n	板砖认为：组织、协调、精炼、和共享是中台运营的关键。\n</p>\n<p>\n	组织即建立起职能部门的中台成员、业务逻辑和架构等，协调即多个业务部门的直接人员业务协调和相互帮助，精炼即对中台运营结果及时性的调整，共享即为各职能部门的优劣信息和可能性信息能做到通达的状态。\n</p>\n<p>\n	例如在实际的中台组织中还能拆分出例如技术中台、研发中台、运营中台、组织中台 等职能中台组织，举例：\n</p>\n<pre class=\"prettyprint\">研发中台：研发中台是关注开发效率的平台，将公司的开发流程、最佳实践沉淀为可重用的能力，为应用的开发提供了流程、质量管控和持续交付的能力。</pre>\n<p>\n	四、企业都可以建立中台吗？\n</p>\n<p>\n	很多企业喜欢跟阿里腾讯等大厂的运营模式、管理模式等，中台也不例外。那么企业真的都可以建立中台吗？ITPUB作者“猿天地”做了一个自己的解答：\n</p>\n<pre class=\"prettyprint\">1. 公司得核心业务不成熟 或 公司业务线很少如果企业属于创业公司，主业务模式都不明朗，这种情况就真的不建议搞什么中台的。中台是讲究多业务服务用的，咱就一个业务，这个业务还在探索，搞啥子中台嘛，把技术平台搞好点就可以了。\n2. 公司里没有相类似的业务即使不是创业公司，是一个中型甚至是大型公司了，但如果公司里虽然业务多，但是每个业务线做的领域都区别很大，比如业务线1做面向C端的电商，业务线2做游戏，业务线3做面向B端企业级产品。这种情况，很难沉淀共性的业务服务，也做不了中台，还是拉一个团队继续做基础平台给各个业务服务吧。\n3. 公司没有足够的人力人力是硬指标，即使上面说的问题都没有，完全符合做中台，那也得考虑考虑人员安排。毕竟中台的建设是需要由独立的团队去完成，并且还应该是一个高效率的团队（不然前台业务会抱怨中台响应不及时），公司是否有这部分的人力预算去建设中台，人员从哪儿来，这个硬性条件必须提前考虑。</pre>\n<p>\n	以上三点板砖是认同的，除此之外板砖也要补充一点：\n</p>\n<p>\n	中台强调的是业务能力，除了沟通协调等本职工作中的专业能力同样会影响中台的运转，所以中台到底适不适合企业，你怎么看？\n</p>\n<p>\n	本文部分章节转载自 ITPUB&nbsp;http://www.itpub.net/2019/11/22/4372/\n</p>', '板砖今年也遇到了“中台危机”，中台模式是一种快速应对市场变化的快节奏打造新产品、新业务链的模式，然而这种模式真的是万能的吗？', '中台建设', 0, 1, 0, NULL, '', '', 1, 1574391469, 1574393659, 1);
INSERT INTO `article` VALUES (40, 1, '原创', '//cdn.dmake.cn/attachment/images/20191126/15747501255.png', '公众号智能客服和聊天机器人怎么做？用图灵机器人！', NULL, '<p>\n	图灵机器人在早先除了文字对话以外，还有一些基于文字的自动处理功能，而且可以根据关键词返回指定问题。\n</p>\n<p style=\"text-align:center;\">\n	<img src=\"http://cdn.dmake.cn/attachment/images/20191126/15747501255.png\" />\n</p>\n<p style=\"text-align:left;\">\n	在微信公众号和智能语音设备普及之后，图灵机器人真正的走向了全面商用的阶段，今天板砖就来讲解一下图灵机器人是如何做到公众号文字回复和语音回复的！\n</p>\n<p style=\"text-align:left;\">\n	首先我们来做一个公用函数，用于和图灵的大脑链接并获得自动回答，请注意下方的uid，uid可以是数字、字符串等可以代表唯一身份确认的内容，不然就会出现鸡同鸭讲的现象。\n</p>\n<p style=\"text-align:left;\">\n<pre class=\"prettyprint lang-php\">function tulingchat($words, $uid = 10000)\n{\n    $api = \'http://www.tuling123.com/openapi/api\';\n    $appid = \'e16355116e0da27aeb6e6cc9\';\n    $secret = \'453a1daf5b7\';\n    \n    $array = array(\n        \'key\' =&gt; $appid,\n        \'info\' =&gt; $words,\n        \'secret\' =&gt; $secret,\n        \'loc\' =&gt; \'重庆市\',\n        \'userid\' =&gt; $uid\n    );\n    \n    $curl = curl_init();\n    curl_setopt($curl, CURLOPT_HTTPHEADER, array(\n        \"Content-Type:application/json; charset=utf-8\"\n    ));\n    curl_setopt($curl, CURLOPT_URL, $api);\n    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);\n    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);\n    curl_setopt($curl, CURLOPT_POST, 1);\n    curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($array));\n    curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);\n    $result = curl_exec($curl);\n    curl_close($curl);\n    \n    return $result;\n}</pre>\n接下来是使用easywechat SDK对接图灵机器人，注意：机器人对话最好的位置是微信文本最后无法通过关键词等返回任一内容的情况下，才调用，具体示例如下:\n</p>\n<p style=\"text-align:left;\">\n<pre class=\"prettyprint lang-php\">function robot($word, $uid)\n    {\n        $result = tulingchat($word, $uid);\n        $json = json_decode($result, true);\n        $txt = \'\';\n        \n        if ($json[\'code\'] == 40001 || $json[\'code\'] == 40002 || $json[\'code\'] == 40004 || $json[\'code\'] == 40007) {\n            $txt = $json[\'text\'];\n        }\n        if ($json[\'code\'] == 100000) {\n            $txt = $json[\'text\'];\n        }\n        if ($json[\'code\'] == 200000) {\n            $txt = $json[\'text\'] . \'-&gt;&lt;a href=\"\' . $json[\'url\'] . \'\"&gt;点击阅读全文&lt;/a&gt;\';\n        }\n        if ($json[\'code\'] == 302000) {\n            $txt = $json[\'text\'] . \':\\n\';\n            foreach ($json[\'list\'] as $item) {\n                $txt .= \'&lt;a href=\"\' . $item[\'detailurl\'] . \'\"&gt;\' . $item[\'article\'] . \'&lt;/a&gt;\\n\';\n            }\n        }\n        if ($json[\'code\'] == 308000) {\n            $txt = $json[\'text\'] . \':\\n\';\n            foreach ($json[\'list\'] as $item) {\n                $txt .= \'&lt;a href=\"\' . $item[\'detailurl\'] . \'\"&gt;\' . $item[\'name\'] . \'&lt;/a&gt;\\n\';\n            }\n        }\n        \n        return new Text($txt);\n    }</pre>\n图灵机器人官方同样页给出了第三方授权方式接入机器人，板砖的建议是复杂的业务环境下最好还是使用自己的SDK进行自动对话，这样可以插入自定义业务，而且不会出现多次回复的情况。\n</p>\n<p style=\"text-align:left;\">\n	当然，公众号和其他智能硬件怎么返回语音内容的呢，其实我们可以借用百度的语音转化API，可以将多种语气的语音转换和保存到本地当中，方法可以看下方函数\n</p>\n<p style=\"text-align:left;\">\n<pre class=\"prettyprint lang-php\">function makevoice()\n    {\n        $client = new AipSpeech(\'107310\', \'RHUzrY7GArqySqUW1Ey\', \'jvKmOZ49T7rb6yQtO852hE\');\n        \n        // tex String 合成的文本，使用UTF-8编码，请注意文本长度必须小于1024字节 是\n        // lang String 语言选择,填写zh 是\n        // ctp String 客户端类型选择，web端填写1 是\n        // cuid String 用户唯一标识，用来区分用户，填写机器 MAC 地址或 IMEI 码，长度为60以内 否\n        // spd String 语速，取值0-9，默认为5中语速 否\n        // pit String 音调，取值0-9，默认为5中语调 否\n        // vol String 音量，取值0-15，默认为5中音量 否\n        // per String 发音人选择, 0为女声，1为男声，3为情感合成-度逍遥，4为情感合成-度丫丫，默认为普通女 否\n        \n        $txt = input(\'get.code\');\n        \n        $result = $client-&gt;synthesis($txt, \'zh\', 1, array(\n            \'vol\' =&gt; 10,\n            \'per\' =&gt; 4,\n            \'spd\' =&gt; 4,\n            \'pit\' =&gt; 6\n        ));\n        \n        $path = \'/audio/\' . time() . \'.mp3\';\n        \n        // 识别正确返回语音二进制 错误则返回json 参照下面错误码\n        if (! is_array($result)) {\n            file_put_contents(\'.\' . $path, $result);\n        }\n        \n        return redirect($path);\n    }</pre>\n以上就是我怎样10分钟打造自己的聊天机器人的，如果你对这个小窍门感兴趣，赶快体验一下。\n</p>', '板砖是6年前开始使用图灵机器人的，随着微信公众号的普及，我是这样10分钟打造自己的聊天机器人的（附代码）。', '公众号客服，聊天机器人，图灵机器人', 0, 1, 0, NULL, '', '', 1, 1574750980, NULL, 0);
INSERT INTO `article` VALUES (41, 1, '原创', '//cdn.dmake.cn/attachment/images/20191128/15749121360.png', '免费的萤火小程序上可以怎么样和企业官网和电商网站融合？', NULL, '颜值即正义，免费即王道，萤火小程序商城，是一款开源的电商系统，为中小企业提供最佳的新零售解决方案。采用稳定的MVC框架开发，执行效率、扩展性、稳定性值得信赖。永久更新维护。<br />\n后台演示：https://demo.yiovo.com/<br />\n<p>\n	项目地址：https://gitee.com/xany/bestshop-php\n</p>\n<p style=\"text-align:center;\">\n	<img src=\"http://cdn.dmake.cn/attachment/images/20191128/15749121360.png\" /> \n</p>\n<p style=\"text-align:center;\">\n	<br />\n</p>\n<p style=\"text-align:left;\">\n	不可否认的是，萤火小程序商城的扩展性和模块化做的是真心不错，对于电商千奇百怪的规格要求、订单要求、供应商和平台运营的素材管理等模块来说，是真的不错。\n</p>\n<p style=\"text-align:left;\">\n	然而客户的需求是千奇百怪的，即便是目前互联网推崇移动端，但是谁也不知道企业是否想在此基础上拓展企业品牌网站甚至官网出来。因此板砖在此基础上做了一些代码小小的增加和改变，帮助有这样需求的开发者能够快速进入业务。\n</p>\n<p style=\"text-align:left;\">\n	1. 首先我们确认我们到底需要扩展什么模块，在application中新建模块，建立起数据读取和view展示的规则，这一点不多说！\n</p>\n<p style=\"text-align:left;\">\n	<img src=\"http://cdn.dmake.cn/attachment/keditor/image/20191128/20191128114106_49357.png\" alt=\"\" /> \n</p>\n<p style=\"text-align:left;\">\n	2. 我们需要在后端加入新增的模块，例如cms。这个步骤中第一步就是将模块的后端地址加入menu中，在application/store/extra/menu.php中加入节点，至于如何添加文章并管理需要你自己去定义啦！\n</p>\n<p style=\"text-align:left;\">\n	至此，如何利用萤火小程序的框架去拓展自己的官网系统和官网PC商城的思路就到此结束了，最后强调一下：在PC端的购买方式如果有需要额外附加表的情况下，一定不要改变萤火的结构，因为下一次升级的时候或许会导致系统崩溃。\n</p>', '萤火小程序是板砖特别推荐的免费小程序系统，因为除了高颜值和标准化模块设计以外，有THINKPHP基础的人都能够去拓展出企业品牌官网和电商网站。', '萤火小程序', 0, 1, 0, NULL, '', '', 1, 1574912777, 1575351394, 0);
INSERT INTO `article` VALUES (42, 2, '原创', '//cdn.dmake.cn/attachment/keditor/image/20191209/20191209065414_84582.png', '浅谈互联网产品设计，应用模块在运营中的使用和案例', NULL, '<p>\n	回望2012年，板砖正式进入了互联网行业中从事网站开发的工作，很清楚的记得我的直属上级每周布置开发任务的适合在一张A4纸上面给我们讲解下一周开发要点，这张A4纸就是本篇文末附上的产品设计思路导图的原型了吧。\n</p>\n<p>\n	话不多说，让我们直接进入互联网应用产品设计和运营的千丝万缕。\n</p>\n<p>\n	在板砖的经验总结和观察总结来说，一个互联网应用是基于一个运营主方向的扩展，整套体系可以分为：基础业务平台、应用扩展两个部分。\n</p>\n<p>\n	那么什么是基础业务平台呢？就是承载你业务的主干部分。例如说论坛的基础业务平台就是Discuz安装完毕后的状态，可以做一些基础的信息填充和交互，那么应用扩展是什么呢，就是来自平台研发或第三方基于这个业务平台做出的很多应用，就像汽车买回来之后车主还花钱做的各种改装和装饰。\n</p>\n<p>\n	那么应用模块的设计和运营中的一些思想是如何结合的呢？板砖要从信息<span>粘连</span>、商品销售、活跃度提升、忠诚度和社交<span>以上几个点来详细解答，这几个点可以单独拓展也能独立运作。</span> \n</p>\n<p>\n	<span><img src=\"http://cdn.dmake.cn/attachment/keditor/image/20191209/20191209065414_84582.png\" alt=\"互联网产品设计、应用模块和运营\" width=\"100%\" /><br />\n</span> \n</p>\n<p>\n	<span>1. 信息粘连，指的是在基础业务平台中具有独家或专业性质的信息运营。虽然这类信息可能在基础业务平台中也是一般展示的，但是在运营指向性明确的情况下，我们可以作为单独的频道设计用以突出运营，形成具有优势的互联网产品应用。</span> \n</p>\n<p>\n	<span>例如搜狐IT虽然也是搜狐信息体系下的一环，但是在频道化运营后具备了一定的市场地位。</span> \n</p>\n<p>\n	<span>2. 商品销售，指的是基于简单的下单-&gt;支付-&gt;结束的简单流程外，额外拓展的一些销售手段，例如双十一把全国人民“算计”的头大的淘宝活动，把简单的促销做成了具有社交属性、分销属性和计算的模式，其实这不是运营组胡乱折腾消费者，而是用“趣味”“社交”“流量锁定”“流量分发”等等多种思想打造的利器，当然这类大招只适合短期使用，感兴趣可以深入了解本篇最后一章。当然除了这类大招，日常花式打折是有必要的，单一的折扣方式不足以引起消费者参与更多的消费热情，而且持续的单一会让营销趋于疲倦，就像每天吃蛋炒饭一样会厌倦。</span> \n</p>\n<p>\n	<span>3. 活跃度提升，指的是利用运营手段将持续累计的用户群体形成更强烈的使用依赖。活跃度提升一般都是在工具类和生活娱乐方向使用频繁。例如程序员喜欢使用的chinaz、tool.lu等网站就是属于高频使用工具网站，普通网友喜欢用的券妈妈、丰富的抖音变脸滤镜等就是属于具有娱乐和便捷的粘连应用。</span> \n</p>\n<p>\n	<span>4. 忠诚度，是每个网站和应用都头大的话题，怎么样让用户虔诚的每天使用我们的互联网应用呢？在互联网界利用的更多的是权力和权益两把大刀，权力和权益分级，让普通网友对更高的等级权力充满期待和向往，例如普通用户和VIP在阿里妈妈的佣金差距达到了20%，而和SVIP永久差距达到了80%，这些差距是金钱可以度量的差距，因此用权力和权益未基础打造的忠诚度是较为可靠的。</span> \n</p>\n<p>\n	<span>5. 社交场景，把更多的社交属性融入互联网应用中，使原本枯燥的使用过程变得有趣起来，而且一定是消耗碎片化时间、打通社交的较为理想的方法。例如淘宝盖楼、拼多多砍价、淘宝花式扫五福、街边扫码送礼品等，其实都是社交场景的体现。</span> \n</p>\n<p>\n	<span><br />\n</span> \n</p>\n<p>\n	当然，互联网产品设计的思路不止于此，今天从常见的设计思路和运营目的去解释我们的产品设计的时候应该去考虑的几个要点。普通运营可以从左至右的考虑运营细节，产品设计可以从右至左的考虑产品的原型。如果你对本文感兴趣，可以加QQ554305954和板砖花式探讨。\n</p>', '板砖写的应用没有20也应该也有超过10种了，然而在很长一段时间内都是从自己的开发经验来做的产品设计，在近期有些运营上的思维碰撞让我重新理了一下应用设计的整体思路。', '产品设计，应用模块，运营案例', 0, 1, 0, NULL, '', '', 1, 1575872368, 1575874504, 0);
INSERT INTO `article` VALUES (43, 1, '原创', '//cdn.dmake.cn/attachment/images/20191210/15759564261.png', 'Visual Studio Code 开发PHP项目和Composer的配置', NULL, '<p>\n	PHP开发者使用VSCODE可以使用的最简单扩展配置如下：\n</p>\n<p>\n	1.&nbsp;composer，没有composer的IDE开发PHP是没有灵魂的，注意安装Composer的时候请勾选开发模式，否则VSCODE无法执行命令等，安装之后注意在setting.json中配置以下节点：\n</p>\n<p>\n	<br />\n</p>\n<pre class=\"prettyprint lang-js\">{\n    \"composer.executablePath\":\"c:\\\\composer\\\\composer.bat\",\n    \"composer.workingPath\": \"D:\\\\centos\\\\myprograms\",\n}</pre>\n<p>\n	<br />\n</p>\n<p>\n	<img src=\"http://cdn.dmake.cn/attachment/images/20191210/15759564261.png\" /> \n</p>\n<p>\n	2. php的代码格式化和自适应插件phpfmt - PHP formatter ，PHP IntelliSense ，在这两个插件的配合下，PHP开发者体验会同时拥有Zend Studio和Sublime的良好体验，当然也是需要配置PHP可执行路径的，否则没有相关API的支持安装了也是白搭。\n</p>\n<p>\n	<br />\n</p>\n<pre class=\"prettyprint lang-js\">{\n    \"php.validate.executablePath\": \"D:\\\\phpstudy_pro\\\\Extensions\\\\php\\\\php7.3.4nts_p\\\\php.exe\",\n    \"phpfmt.php_bin\": \"D:\\\\phpstudy_pro\\\\Extensions\\\\php\\\\php7.3.4nts_p\\\\php.exe\",\n    \"php.executablePath\": \"D:\\\\phpstudy_pro\\\\Extensions\\\\php\\\\php7.3.4nts_p\\\\php.exe\", \n}</pre>\n<p>\n	3. 以上环节安装完毕后，给方法打注释是很不方便的，不能快速生成方法名、参数名等，需要再安装\n</p>\n<p>\n	安装完成后可以自定义一些注释参数\n</p>\n<p>\n	<br />\n</p>\n<pre class=\"prettyprint lang-js\">\"php-docblocker.extra\": [\n        \"@description\",\n        \"@example\",\n        \"@author daneas\",\n        \"@since $CURRENT_YEAR-$CURRENT_MONTH-$CURRENT_DATE\"\n    ]</pre>\n<p>\n	<br />\n</p>\n<p>\n	板砖实践认为，以上3个重要插件即可满足PHP的日常主要开发了，当然项目中需要引用的扩展还需要开发者自行补充。\n</p>\n<p>\n	在composer的使用中大家可能会遇到的几个问题有：\n</p>\n<p>\n	1. 镜像问题，当然composer的中文镜像有些时候是有作用的，但是不稳定，所以推荐使用阿里云镜像，可以解决例如“Expected one of: \'STRING\', \'NUMBER\', \'NULL\', \'TRUE\', \'FALSE\', \'{\', \'[\'”这类问题\n</p>\n<p>\n	<br />\n</p>\n<pre class=\"prettyprint lang-js\">	\"repositories\": {\n		\"packagist\": {\n			\"type\": \"composer\",\n			\"url\": \"https://mirrors.aliyun.com/composer/\"\n		}\n	},</pre>\n2. composer.phar文件不存在的问题，一般来说你需要检查问题目录，删除composer.bat文件，使用安装默认位置的文件。\n<p>\n	<br />\n</p>\n<p>\n	希望以上两点能够帮助到相关问题的朋友。\n</p>\n<p>\n<pre class=\"prettyprint lang-js\">完整的setting.json配置\n{\n    \"php.validate.executablePath\": \"D:\\\\phpstudy_pro\\\\Extensions\\\\php\\\\php7.3.4nts_p\\\\php.exe\",\n    \"phpfmt.php_bin\": \"D:\\\\phpstudy_pro\\\\Extensions\\\\php\\\\php7.3.4nts_p\\\\php.exe\",\n    \"php.executablePath\": \"D:\\\\phpstudy_pro\\\\Extensions\\\\php\\\\php7.3.4nts_p\\\\php.exe\", \n    \"editor.wordWrap\": \"on\",\n    \"breadcrumbs.enabled\": true,\n    \"window.zoomLevel\": 0,\n    \"composer.executablePath\":\"c:\\\\composer\\\\composer.bat\",\n    \"composer.workingPath\": \"D:\\\\centos\\\\myprograms\",\n    \"composer.enabled\": true,\n    \"editor.fontSize\": 15,\n    \"workbench.activityBar.visible\": true,\n    \"php-docblocker.extra\": [\n        \"@author dmakecn@126.com\",\n        \"@at $CURRENT_YEAR-$CURRENT_MONTH-$CURRENT_DATE\"\n    ]\n}</pre>\n完整的PHP开发环境，包含了github，代码自动补全，代码格式化，代码注释和图标优化。<img src=\"http://cdn.dmake.cn/attachment/keditor/image/20191213/20191213103617_52946.png\" alt=\"\" />\n</p>\n<p>\n	<br />\n</p>', 'Visual Studio Code（vscode）已经成为了一个成熟的跨平台跨语言开发IDE，PHP开发者也可以尝试使用vscode管理和开发自己的项目。', 'vscode，composer', 0, 1, 0, NULL, '', '', 1, 1575956867, 1576204633, 0);
INSERT INTO `article` VALUES (44, 1, '原创', '//cdn.dmake.cn/attachment/images/20191226/15773264370.png', 'CDN那么多，怎样批量管理七牛云、易速云、又拍云等多个CDN资源解决方案', NULL, '<p>\n	板砖在平台开发的经历中，特别推荐七牛云存储，便宜稳定。然而很多开发者甚至我曾经开发的应用中在未来还会使用更多品牌的CDN，因此板砖决定不使用云存储提供的SDK，而自己来打造一个较为规范的资源管理工具。\n</p>\n<p>\n	首先来整理一下思路哈，便于需要这个解决方案的同学自己去改造系统，在文末将附上前端代码：\n</p>\n<p>\n	1. 建立记录所有资源的系统表，这个表将你存在所有的CDN的资源路径、名称等记录下来，如何记录资源的cdn信息，板砖不用再多说，看官自己看着办吧\n</p>\n<p style=\"text-align:center;\">\n	<img src=\"http://cdn.dmake.cn/attachment/keditor/image/20191226/20191226022119_75555.png\" alt=\"\" />\n</p>\n<p style=\"text-align:center;\">\n	该表是板砖建的基础表，你可以根据需求去改\n</p>\n<p>\n	<br />\n</p>\n<p>\n	2. 建立资源的分组表，可以是功能分组，也可以是CDN品牌的分组，这个看自己了，这个分组表就不再赘述了，上图中groupid就是本表的主键。\n</p>\n<p>\n	3. 使用板砖的资源管理工具即可，根据自己的需求改装。\n</p>\n<p>\n	<br />\n</p>\n<p>\n	注意：在本解决方案中使用了webuploader控件，保存方法等需要各位自己解决，另外分页插件是用js写的原生插件，后期还会继续更新。\n</p>\n<p>\n	如果有对本篇感兴趣且不是特别明白的，欢迎加QQ554305954和板砖一起探讨，如果您觉得本篇还行，感谢扫描下图中的小程序码支持一下。\n</p>\n<p>\n	<br />\n</p>\n<p>\n	<br />\n</p>\n<p style=\"text-align:center;\">\n	<img src=\"http://cdn.dmake.cn/attachment/images/20191226/15773264370.png\" />\n</p>\n<p style=\"text-align:center;\">\n	板砖的资源管理器一览\n</p>\n<p style=\"text-align:center;\">\n	<img src=\"http://cdn.dmake.cn/attachment/keditor/image/20191226/20191226022354_99006.png\" alt=\"\" />\n</p>\n<p style=\"text-align:center;\">\n	新增资源分组，板砖是以应用场景来分组的\n</p>\n<p style=\"text-align:center;\">\n	<img src=\"http://cdn.dmake.cn/attachment/keditor/image/20191226/20191226022408_38918.png\" alt=\"\" />\n</p>\n<p style=\"text-align:center;\">\n	修改资源属性，重新分组和alt重命名\n</p>\n<p style=\"text-align:left;\">\n<pre class=\"prettyprint lang-html\">&lt;link rel=\"stylesheet\" type=\"text/css\" href=\"/static/webupload/webuploader.css\"&gt;\n&lt;link rel=\"stylesheet\" type=\"text/css\" href=\"/static/webupload/style.css\"&gt;\n&lt;style&gt;\n    .file-item{float:left;position:relative;width:110px;height:110px;margin:0 20px 20px 0;padding:4px}\n    .file-item .info{overflow:hidden}\n    .uploader-list{width:100%;overflow:hidden}\n    #img_data{margin:8px 0}\n    .piclib{float:left;margin-left:10px;background:#02b7ee;border:0;border-radius:3px;color:#fff;padding:10px 16px}\n&lt;/style&gt;\n\n&lt;div id=\"fileList\" class=\"uploader-list\"&gt;&lt;/div&gt;\n&lt;div id=\"imgPicker\" style=\"float:left;\"&gt;上传图片&lt;/div&gt;    \n&lt;input type=\"button\" class=\"piclib\" onclick=\"loadGroups();loadImages(1);$(\'.yunsource\').show();\" value=\"图库选图\"/&gt;\n\n&lt;script type=\"text/javascript\" src=\"/static/webupload/webuploader.min.js\"&gt;&lt;/script&gt;\n&lt;script type=\"text/javascript\"&gt;\n    var uploader = WebUploader.create({     \n        auto: true,\n        swf: \'/static/js/webupload/Uploader.swf\',\n        server: \"{:url(\'upload\')}\",\n        duplicate :true,\n        pick: \'#imgPicker\',\n\n        accept: {\n            title: \'Images\',\n            extensions: \'gif,jpg,jpeg,bmp,png\',\n            mimeTypes: \'image/jpg,image/jpeg,image/png\'\n        },\n\n        \'onUploadSuccess\': function(file, data, response) {\n            $(\"#data_photo\").val(data._raw);\n            $(\"#img_data\").attr(\'src\', data._raw).show();\n        }\n    });\n\n    uploader.on( \'fileQueued\', function( file ) {\n        $(\'#fileList\').html( \'&lt;div id=\"\' + file.id + \'\" class=\"item\"&gt;\' +\n            \'&lt;h4 class=\"info\"&gt;\' + file.name + \'&lt;/h4&gt;\' +\n            \'&lt;p class=\"state\"&gt;正在上传...&lt;/p&gt;\' +\n        \'&lt;/div&gt;\' );\n    });\n\n    uploader.on( \'uploadSuccess\', function( file ) {\n        $( \'#\'+file.id ).find(\'p.state\').text(\'上传成功！\');\n    });\n\n    uploader.on( \'uploadError\', function( file ) {\n        $( \'#\'+file.id ).find(\'p.state\').text(\'上传出错!\');\n    });\n&lt;/script&gt;  \n&lt;!-----------------------------------------------资源框框里面的--------------------------------------------------------&gt;\n&lt;style&gt;\n    .yunsource{width:800px;height:560px;display:none;position:fixed;z-index:1000;top:150px;left:30%;border:1px solid #efefef;box-shadow:1px 1px 1px #eee;background:#fff}\n    .yunsource .yun_left{width:120px;float:left}\n    .yunsource .yun_left ul{list-style:none;margin: 0;padding: 15px;}\n    .yunsource .yun_left ul li{line-height:3}\n    .yunsource .yun_left ul li a{color:#666;font-size: 1.2em;}\n    .yunsource .yun_left ul li a .active{color:#666}\n    .yunsource .yun_right,.save_group,.save_source{width:678px;float:left;border-left:2px solid #eee;height:100%}\n    .yunsource .yun_right #items img{display:block;width:150px;height:120px;margin:5px;object-fit:cover;border-radius:4px;overflow:hidden;border:1px solid #eee;transition:all .6s;}\n    .yunsource #items{width:100%;display:inline-block}\n    .yunsource #items .item{width:160px;height:150px;float: left;}\n    .yunsource #items .item div{width: 100%; text-align:center;}\n    .yunsource #items .item a:first-child{color: #3f51b5;margin-right: 2px;}\n    .yunsource #items .item a:last-child{color: #f44336;margin-left: 2px;}\n    .yunsource .barcon{width:100%;line-height:24px;    text-align: CENTER;}\n    .yunsource .barcon a{padding: 3px 6px;;}\n    .yunsource .save_group,.save_source{display:none;line-height: 2;padding: 50px;}\n&lt;/style&gt;\n&lt;div class=\"yunsource\"&gt;\n    &lt;div class=\"yun_left\"&gt;\n    &lt;ul&gt;\n    &lt;/ul&gt;\n    &lt;/div&gt;\n    &lt;div class=\"yun_right\"&gt;\n    &lt;span onclick=\"$(\'.yunsource\').hide()\" style=\"float: right;padding: 10px;color: red;\"&gt;&lt;i class=\"fas fa-window-close\"&gt;&lt;/i&gt;&lt;/span&gt;\n    &lt;div id=\"items\"&gt;&lt;/div&gt;\n    &lt;div id=\"barcon\" class=\"barcon\"&gt;&lt;/div&gt;\n    &lt;/div&gt;\n    &lt;div class=\"save_group\"&gt;\n        &lt;h4&gt;图片分组&lt;/h4&gt;\n        &lt;input type=\"text\" id=\"yun_groupname\" value=\"\" class=\"form-control\" placeholder=\"请输入分组名\" style=\"margin-bottom: 20px;\"/&gt;\n        &lt;button class=\"btn btn-danger\" type=\"button\" onclick=\"saveGroup()\"&gt;提交&lt;/button&gt;\n        &lt;button class=\"btn btn-success\" type=\"button\" onclick=\'$(\".yun_right\").fadeIn(600);$(\".save_group\").fadeOut(600);\'&gt;取消&lt;/button&gt;\n    &lt;/div&gt;\n    \n    &lt;div class=\"save_source\"&gt;\n    &lt;h4&gt;资源编辑&lt;/h4&gt;\n    &lt;img src=\"\" id=\"editurl\" style=\"max-height: 250px;\"/&gt;\n    &lt;input type=\"hidden\" id=\"yun_id\"/&gt;\n    &lt;select id=\"yun_groupid\" class=\"form-control\"  style=\"margin-bottom: 20px;\"&gt;&lt;/select&gt;\n    &lt;input type=\"text\" id=\"yun_title\" value=\"\" class=\"form-control\" placeholder=\"请输入图片名称\" style=\"margin-bottom: 20px;\"/&gt;\n    &lt;div style=\"display: inline-block;width: 100%;\"&gt;\n        &lt;button class=\"btn btn-danger\" type=\"button\" onclick=\"saveSource()\"&gt;提交&lt;/button&gt;\n        &lt;button class=\"btn btn-success\" type=\"button\" onclick=\'$(\".yun_right\").fadeIn(600);$(\".save_source\").fadeOut(600);\'&gt;取消&lt;/button&gt;\n    &lt;/div&gt;\n&lt;/div&gt;\n&lt;/div&gt;\n&lt;script&gt;  \n    var nowGroupId = 0;\n    var pageRows=16;\n\n    function groupClick (groupid) {\n        if (groupid == nowGroupId) {\n            return true;\n        }\n        nowGroupId = groupid;\n        loadImages(1);\n    }\n\n    //加载分组\n    function loadGroups() {\n        $(\".yun_left&gt;ul&gt;li\").remove();\n        $(\"#yun_groupid&gt;option\").remove();\n        $(\".yun_left&gt;ul\").append(\'&lt;li&gt;&lt;a href=\"javascript:groupClick(0);\"&gt;全部&lt;/a&gt;&lt;/li&gt;\');\n        $.post(\"{:url(\'yun/groups\')}\", { groupid: 0 }, function (res) {\n            if (res.code == 1) {\n                for (let i in res.data) {\n                    var r=res.data[i];\n                    $(\".yun_left&gt;ul\").append(\'&lt;li&gt;&lt;a href=\"javascript:groupClick(\' + r.id + \');\"&gt;\' + r.name + \'&lt;/a&gt;&lt;/li&gt;\');\n                    $(\"#yun_groupid\").append(\'&lt;option value=\"\' + r.id + \'\"&gt;\' + r.name + \'&lt;/option&gt;\');\n                }\n            }\n            $(\".yun_left&gt;ul\").append(\'&lt;li&gt;&lt;a href=\"javascript:newGroup();\"&gt;新增分组&lt;/a&gt;&lt;/li&gt;\');\n        });\n    }\n\n    //加载图片，刷新分页插件\n    function loadImages(nowPage) {\n        $.post(\"{:url(\'yun/index\')}\", { groupid: nowGroupId, page: nowPage, rows: pageRows }, function (res) {\n            $(\"#items\").html(\'\');\n            iniPage(nowPage, pageRows, res.count);\n            for (let i in res.list) {\n                var img = res.list[i];\n                $(\"#items\").append(\'&lt;div class=\"item\"&gt;&lt;img src=\"\' + img.url + \'\" onerror=\"/static/admin/images/no_img.jpg\" class=\"yunsourceimg\"/&gt;\'\n                    + \'&lt;div&gt;&lt;a href=\"javascript:editsource(\' + img.id + \');\"&gt;编辑&lt;/a&gt;\' \n                    + \'&lt;a href=\"javascript:delsource(\' + img.id + \');\"&gt;删除&lt;/a&gt;&lt;/div&gt;&lt;/div&gt;\');\n            }\n        });\n    }\n\n    //新开一个分组，跳出分组页面\n    function newGroup() {\n        $(\".yun_right\").fadeOut(600);\n        $(\".save_group\").fadeIn(600);\n    }\n\n    function editsource(id){            \n        $(\"#yun_id\").val(id);\n        $.getJSON(\"{:url(\'yun/getsource\')}\",{id:id,r:Math.random()},function(res){                \n            $(\"#editurl\").attr(\"src\",res.data.url);\n            $(\"#yun_title\").val(res.data.title);\n            $(\".yun_right\").fadeOut(600);\n            $(\".save_source\").fadeIn(600);\n        });\n    }\n&lt;/script&gt;\n\n&lt;script type=\"text/javascript\"&gt;\n    //所有提交都在这里解决\n    function saveGroup(){\n        if($(\"#yun_groupname\").val().length&lt;1){\n            layer.msg(\'请输入分组名\', { icon: 5, time: 1500, shade: 0.1 });\n            return false;\n        }\n\n        $.post(\"{:url(\'yun/savegroup\')}\",{act:\'new\',id:0,name:$(\"#yun_groupname\").val()},function(data){\n            if (data.code == 1) {\n                layer.msg(data.msg, { icon: 6, time: 1500, shade: 0.1 }, function (index) {\n                    loadGroups();                        \n                    $(\".yun_right\").fadeIn(600);\n                    $(\".save_group\").fadeOut(600);\n                    return true;\n                });\n            } else {\n                layer.msg(data.msg, { icon: 5, time: 1500, shade: 0.1 });\n                return false;\n            }\n        })\n    }\n    \n    //所有提交都在这里解决\n    function saveSource(){\n        if($(\"#yun_title\").val().length&lt;1){\n            layer.msg(\'请输入资源名\', { icon: 5, time: 1500, shade: 0.1 });\n            return false;\n        }\n\n        $.post(\"{:url(\'yun/savesource\')}\",{act:\'edit\',id:$(\'#yun_id\').val(),title:$(\"#yun_title\").val(),groupid:$(\"#yun_groupid\").val()},    function(data){\n            if (data.code == 1) {\n                layer.msg(data.msg, { icon: 6, time: 1500, shade: 0.1 }, function (index) {\n                    loadGroups();                        \n                    $(\".yun_right\").fadeIn(600);\n                    $(\".save_group\").fadeOut(600);\n                    return true;\n                });\n            } else {\n                layer.msg(data.msg, { icon: 5, time: 1500, shade: 0.1 });\n                return false;\n            }\n        })\n    }\n\n    function delsource(id) {\n        if (confirm(\'本删除仅删除数据库记录，不删除CDN资源\')) {\n            $.post(\"{:url(\'yun/savesource\')}\", { act: \'del\', id: id }, function (data) {\n                if (data.code == 1) {\n                    layer.msg(data.msg, { icon: 6, time: 1500, shade: 0.1 }, function (index) {\n                        loadImages(1);\n                        return true;\n                    });\n                } else {\n                    layer.msg(data.msg, { icon: 5, time: 1500, shade: 0.1 });\n                    return false;\n                }\n            });\n        }\n    }\n&lt;/script&gt;\n&lt;script&gt;\n    // 生成自定义分页\n    function iniPage(currentPage, pageSize, totalRows) {\n\n        var totalPage = 0;\n        if ((totalRows % pageSize) != 0) {\n            totalPage = parseInt(totalRows / pageSize) + 1;\n        } else {\n            totalPage = parseInt(totalRows / pageSize);\n        }\n\n        var startRow = (currentPage - 1) * pageSize + 1;\n        var endRow = currentPage * pageSize;\n        endRow = (endRow &gt; totalRows) ? totalRows : endRow;\n\n        var tempStr = \"&lt;span&gt;共\" + totalPage + \"页&lt;/span&gt;\";\n\n        if (currentPage &gt; 1) {\n            tempStr += \"&lt;span class=\'btn\' href=\\\"#\\\" onClick=\\\"loadImages(\" + (1) + \")\\\"&gt;首页&lt;/span&gt;\";\n            tempStr += \"&lt;span class=\'btn\' href=\\\"#\\\" onClick=\\\"loadImages(\" + (currentPage - 1) + \")\\\"&gt;上一页&lt;/span&gt;\"\n        }\n\n        for (var pageIndex = 1; pageIndex &lt; totalPage + 1; pageIndex++) {\n            if(currentPage==pageIndex){\n                tempStr += \"&lt;a&gt;&lt;span style=\'color:#333;\'&gt;\" + pageIndex + \"&lt;/span&gt;&lt;/a&gt;\";\n            }\n            else{\n                tempStr += \"&lt;a onclick=\\\"loadImages(\" + pageIndex + \")\\\"&gt;&lt;span&gt;\" + pageIndex + \"&lt;/span&gt;&lt;/a&gt;\";\n            }\n        }\n\n        if (currentPage &lt; totalPage) {\n            tempStr += \"&lt;span class=\'btn\' href=\\\"#\\\" onClick=\\\"loadImages(\" + (currentPage + 1) + \")\\\"&gt;下一页&lt;/span&gt;\";\n            tempStr += \"&lt;span class=\'btn\' href=\\\"#\\\" onClick=\\\"loadImages(\" + (totalPage) + \")\\\"&gt;尾页&lt;/span&gt;\";\n        }\n\n        document.getElementById(\"barcon\").innerHTML = tempStr;\n    }\n&lt;/script&gt;\n\n&lt;script&gt;\n    var copyto = \'[copyto]\';\n    $(\'#items\').on(\"click\",\'.yunsourceimg\', function () {\n        var source = $(this).attr(\'src\');\n        switch (copyto) {\n            case \"kindeditor\":\n                break;\n            case \"ueditor\":\n                break;\n            default:\n                $(\"#data_photo\").val(source);\n                $(\"#img_data\").attr(\'src\', source).show();\n                $(\".yunsource\").hide();\n                break;\n        }\n    });\n&lt;/script&gt;</pre>\n</p>\n<p style=\"text-align:center;\">\n	<br />\n</p>\n<p style=\"text-align:center;\">\n	<br />\n</p>', '板砖发现，不少的平台在整个运行周期内可能同时或者分时段使用了不同的CDN云存储，而管理这些资源的时候使用者脑壳是很大的，而且如何深度使用这些CDN资源也是一个问题。那么板砖今天来讲一下我的管理方式。', 'CDN管理，云管理', 0, 1, 0, NULL, '', '', 1, 1577327363, NULL, 0);
INSERT INTO `article` VALUES (70, 5, '原创', '//cdn.dmake.cn/attachment/images/20210126/16116400125.jpg', '当你的员工做兼职老板该怎么办？当然是支持TA呀！', NULL, '<img src=\"http://cdn.dmake.cn/attachment/images/20210126/16116400125.jpg\" alt=\"\" /> \n<p>\n	<br />\n</p>\n<h4>\n	观察\n</h4>\n<p>\n	板砖的职业生涯截止当前经历过技术小工、码农、市场销售、公司运管、产品经理等多个岗位，从事行业在IT、零售和广告范围都走过一圈。\n</p>\n<p>\n	因此，对于在市场中对兼职这个概念还是稍有体会。总的来说员工的兼职工作在大部分时间里都能给企业带来积极的价值影响。\n</p>\n<h4>\n	积极地一面\n</h4>\n<p>\n	一个企业无论大小，都面临过业务的局限性；一个管理者无论高低，都面临知识的天花板。那么在遇到这些受限制的情景下大多公司需要怎么去应对？\n</p>\n<p>\n	传统途径不外乎“招聘高手”、“求助专业咨询公司”、“摸索尝试”、“读书自学”、“求助他人”这几个方式方法。但是稍作理解就是<strong>花钱、求人</strong>。\n</p>\n<p>\n	那么为什么说员工做兼职能够带来积极的一面呢？理由如下：\n</p>\n<p>\n	<br />\n</p>\n<ol>\n	<li>\n		兼职能够拓展员工视野、反复锤炼熟悉技能、不断学习新的业务\n	</li>\n	<li>\n		做兼职的员工收获了其他经验，这些经验能够为现公司所用\n	</li>\n	<li>\n		培训、练手成本都由其他公司承担，自己不用承担额外费用，也不需要招人\n	</li>\n</ol>\n<h4>\n	负面的预防\n</h4>\n<p>\n	很多企业老板对员工做兼职的芥蒂大致在于“自己花钱付工资，你却在帮别人干活”这一点，诚然此事却为事实。\n</p>\n<p>\n	那么预防员工做兼职荒废主业对企业产生不良影响，以下几点企业管理者可以考虑：\n</p>\n<p>\n	<br />\n</p>\n<ol>\n	<li>\n		&nbsp;日常工作的考核，需要检查员工的工作推进情况，如果完成保质保量可以无视影响\n	</li>\n	<li>\n		&nbsp;注意员工的精神状态，如果做兼职影响了其休息效果，哪怕能够保质保量完成工作但是却累垮了身体，也是不允许的\n	</li>\n	<li>\n		&nbsp;注意保密，一些IT行业要注意数据不能泄露、核心库不能留出，避免自家保密内容被他人所知\n	</li>\n	<li>\n		&nbsp;不要占用8小时工作时间，如果只是偶发占用时间不长参考第一条\n	</li>\n</ol>\n<h4>\n	总结\n</h4>\n<p>\n	兼职是一块磨刀石，员工是一把刀，管理者是刀具使用人，刀磨好了为自己所用那么是双赢，刀磨坏了、伤到自己了就应该思考管理是否缺席。\n</p>\n<p>\n	<br />\n</p>\n<p>\n	<br />\n</p>', '老板的胸怀决定了团队的大小，员工的智慧决定了业务的范围。今天板砖主要想和大家介绍下员工做兼职对于公司本身的价值影响。', '', 0, 1, 0, NULL, '', '', 1, 1611641141, 1611641717, 0);
INSERT INTO `article` VALUES (46, 1, '原创', '//cdn.dmake.cn/attachment/images/20191227/15774228158.png', '如何在系统中集成淘宝客/京东客/拼多多客接口？基于阿里妈妈SDK案例分享', NULL, '<p>\n	最早做淘宝客的人肯定是挣了钱的，再后来想挣钱的同学就需要在技巧、技术、营销等方面做足功夫了。本篇板砖主要是分享几个常用的淘宝客系统使用的SDK，同时也会分享源码、效果出来。\n</p>\n<p style=\"text-align:center;\">\n	<img src=\"http://cdn.dmake.cn/attachment/keditor/image/20191226/20191226014009_13760.jpg\" width=\"150\" /> \n</p>\n<p style=\"text-align:center;\">\n	扫码关注随风来小程序，即可体验\n</p>\n<p style=\"text-align:left;\">\n	板砖因为很久没有玩儿淘宝客了，于是只有个初级权限共13个淘宝客SDK权限，这些权限刚好可以完成基础的用户体验，例如大额券、选品库、淘逛街、淘抢购、商品搜索和展示等，板砖在此基础上打造了上述的小程序淘宝客。\n</p>\n<p style=\"text-align:center;\">\n	<img src=\"http://cdn.dmake.cn/attachment/keditor/image/20191227/20191227130904_71310.png\" alt=\"\" />\n</p>\n<p style=\"text-align:left;\">\n	淘宝客如何集成到自己的独立系统中其实是个小问题，最大的问题是每一个接口返回的字段都是不一样的，难道要针对每一个接口新建一个数据表么？\n</p>\n<p style=\"text-align:left;\">\n	当然不是！\n</p>\n<p style=\"text-align:left;\">\n	淘宝客基础SDK每天的调用次数为1000000次，算下来完全支持一个日活1K人以上的数据获取体验了，因此我建议：咱现取现用，核心数据存库！\n</p>\n<p style=\"text-align:left;\">\n	取数据咱就不说了，直接使用官方SDK示例代码，做好自己的配置即可！\n</p>\n<p style=\"text-align:left;\">\n	为了满足未来面向多平台的淘客业务，板砖做了工厂方法，保证未来的可扩展性能。\n</p>\n<p style=\"text-align:left;\">\n	目前呢板砖提供的小程序只用了1张数据表，然而效果我认为还行吧，如果想做淘宝客或者购买淘宝客系统的同学，可以加QQ554305954，让板砖告诉你怎么样低成本的打造自己的淘客平台！\n</p>\n<p style=\"text-align:left;\">\n<pre class=\"prettyprint lang-php\">        $api = null;        \n        switch ($model) {\n            case \"taobao\":\n                $api = new \\app\\api\\model\\TModel();\n                break;\n            case \"jd\":\n                $api = new \\app\\api\\model\\JModel();\n                break;\n            case \"pdd\":\n                $api = new \\app\\api\\model\\PModel();\n                break;\n        }        \n        return $api-&gt;initialize();</pre>\n如何解决多SDK间数据保存的问题。\n</p>\n<p style=\"text-align:left;\">\n<pre class=\"prettyprint lang-php\">\n    /**\n     * 根据接口解析进入的数据并删除重复和保存\n     * @param unknown 方法名\n     * @param unknown 接口返回数据\n     * @return \\think\\response\\Json|boolean\n     */\n    private function CheckSave($action, $resp)\n    {\n        $jsonStr = json_encode($resp);\n        $jsonArray = json_decode($jsonStr, true);\n        if (array_key_exists(\'code\', $jsonArray)) {\n            return $resp;\n        }\n        $catename = \'category_name\';\n        switch ($action) {\n            case \'JuTqg\': // 聚划算和淘抢购\n                $goodtype = 1;\n                if (!array_key_exists(\'results\', $jsonArray)) {\n                    return $resp;\n                }                \n                $items = $jsonArray[\'results\'][\'results\'];\n                break;\n            case \"DG\":\n                $goodtype = 0;\n                if (!array_key_exists(\'result_list\', $jsonArray)) {\n                    return $resp;\n                }\n                $items = $jsonArray[\'result_list\'][\'map_data\'];\n                break;\n        }\n        if ($items == [] || count($items) &lt; 1) {\n            return false;\n        }        \n        $ids = \'\';\n        foreach ($items as $item) {\n            $ids .= \',\' . $item[\'num_iid\'];\n        }\n        $recodes = $this\n            -&gt;where(\'num_iid\', \'in\', $ids)\n            -&gt;delete();\n        $data = [];\n        foreach ($items as $item) {            \n            $item[\'category_name\'] = $item[$catename];\n            if ($item[\'category_name\'] == \'其他\') {\n                continue;\n            }\n            $item[\'goodstype\'] = $goodtype;\n            $item[\'create_time\'] = time();            \n            //https://open.taobao.com/api.htm?docId=35896&amp;docType=2\n            if ($goodtype == 0) {\n                if (array_key_exists(\'small_images\', $item) &amp;&amp; array_key_exists(\'string\', $item[\'small_images\'])) {\n                    $small = $item[\'small_images\'][\'string\'];\n                    $item[\'small_images\'] = json_encode($small);\n                }                \n                $this-&gt;insertGetId($item);\n                array_push($data, $item);\n            }\n            //https://open.taobao.com/api.htm?docId=27543&amp;docType=2\n            if ($goodtype == 1) {\n                $this-&gt;insertGetId($item);\n                array_push($data, $item);\n            }\n            // 正常的搜索\n            if ($goodtype == 2) {\n                array_push($data, $item);\n            }\n            unset($item);\n        }\n        return $data;\n    }</pre>\n</p>', '板砖从2013年才开始接触淘宝客，错过了2012年的平台和流量的红利期，这个话题在100个故事中会将。这次主要还是讲如何做TP5+PHPSDK的带货接口。', '淘宝客，京东客，拼多多客，淘宝客系统', 0, 1, 0, NULL, '', '', 1, 1577423636, NULL, 0);
INSERT INTO `article` VALUES (47, 1, '原创', '//cdn.dmake.cn/attachment/images/20200108/15784598656.png', '微信小程序开发文字弹幕实例，无限轮播的规范化代码', NULL, '<p style=\"text-align:center;\">\n	<img src=\"http://cdn.dmake.cn/attachment/keditor/image/20200108/20200108130446_94872.png\" alt=\"微信轮播\" /> \n</p>\n<p style=\"text-align:left;\">\n	如上图所示，板砖在地图上方实现了弹幕效果。通过下方代码基本上你可以直接复制粘贴实现该功能。但是请注意：\n</p>\n<p style=\"text-align:left;\">\n	1. util 是我个人封装的方法库，这里的minrequest你可以用wx.request代替\n</p>\n<p style=\"text-align:left;\">\n	2. util.randomarround方法是获得min-max的随机数，需要你自己配置\n</p>\n<p style=\"text-align:left;\">\n	前端代码\n</p>\n<p style=\"text-align:left;\">\n	<br />\n</p>\n<pre class=\"prettyprint lang-html\">{{item.barrage_shootText}}</pre>\n<p>\n	<br />\n</p>\n<p style=\"text-align:left;\">\n	后端代码\n</p>\n<pre class=\"prettyprint lang-js\">const app = getApp();\nvar util = require(\'../../utils/util.js\');\n\n/**\n * 弹幕设置\n */\nvar barrage_list_arr = []; // 记录预设的条\nvar barrage_list_moving = []; // 记录正在参加弹幕滚动的条\nvar phoneWidth = 0;\nvar timers = [];\nvar timer;\n\n/**\n * 组装一条弹幕\n */\nconst iniBarrage = (img, text) =&gt; {\n  return {\n    barrage_shootText: text,\n    barrageText_height: (Math.random()) * 266,\n    barrage_shoottextColor: \"rgb(\" + parseInt(Math.random() * 256) + \",\" + parseInt(Math.random() * 256) + \",\" + parseInt(Math.random() * 256) + \")\",\n    barrage_phoneWidth: util.randomaround(0, phoneWidth),\n    display: \"display:block\"\n  };\n}\n\n/**\n * 加载弹幕\n */\nconst loadPops = (that) =&gt; {\n  let pops = that.data.pops;\n  let list = [];\n\n  for (let i in pops) {\n    let one = pops[i];\n    list.push(iniBarrage(one[\'openid\'], one[\'detail\']));\n    barrage_list_arr = list;\n    that.timer = setInterval(that.barrageMove, 500);\n  }\n\n  that.setData({\n    barrage_list: list,\n    barragefly_display: \'block\'\n  });\n}\n\n\nPage({\n  data: {\n    barrageTextColor: \"#D3D3D3\",\n    barrage_inputText: \"none\",\n    barrage_shoottextColor: \"black\",\n    barragefly_display: \"none\",\n    bind_shootValue: \"\",\n    barrage_phoneWidth: 0,\n    barrage_list: [],\n    pops: [],\n  },\n\n\n  /**\n   * 初始化\n   */\n  onLoad: function(options) {\n    var that = this;\n\n    //获取屏幕的宽度\n    wx.getSystemInfo({\n      success: function(res) {\n        phoneWidth = res.windowWidth - 100;\n        that.setData({\n          barrage_phoneWidth: res.windowWidth - 100,\n        })\n\n          loadPops(that);\n      }\n    })\n  },\n\n  /**\n   * 加载弹幕信息\n   */\n  onShow: function() {\n    let that = this;\n\n    let data = {\n      \"ctl\": \'apps\',\n      \"act\": \"pops\",\n      \'hash\': app.globalData.hash\n    };\n    util.minRequest(data, function(res) {\n      that.setData({\n        pops: res.data\n      });\n    });\n  },\n\n  /**\n   * 打开/关闭弹幕\n   */\n  barrageSwitch: function(e) {\n    if (!e.detail.value) {\n      //清空弹幕\n      barrage_list_arr = [];\n      //设置data的值\n      this.setData({\n        barrageTextColor: \"#D3D3D3\",\n        barrage_inputText: \"none\",\n        barragefly_display: \"none\",\n        barrage_list: barrage_list_arr,\n      });\n\n      //清除定时器\n      clearInterval(timer);\n    } else {\n      this.setData({\n        barrageTextColor: \"#04BE02\",\n        barrage_inputText: \"flex\",\n        barragefly_display: \"block\",\n      });\n      //打开定时器\n      timer = setInterval(this.barrageText_move, 100);\n    }\n  },\n\n\n  /**\n   * 弹幕轮播\n   */\n  barrageMove: function() {\n\n    if (barrage_list_moving.length == 0) {\n      //todo:弹幕跑完了，重置\n      for (var i in barrage_list_arr) {\n        let temp = barrage_list_arr[i];\n        temp.barrage_phoneWidth = util.randomaround(0, phoneWidth);\n        barrage_list_moving.push(temp);\n      }\n    }\n\n    //todo:跑弹幕\n    for (var i in barrage_list_moving) {\n\n      let now = barrage_list_moving[i];\n\n      var textMove = barrage_list_moving[i].barrage_phoneWidth - 2; // 左移5\n\n      barrage_list_moving[i].barrage_phoneWidth = textMove;\n\n      // todo：优化跑完了不移除这条，把他归0就是了\n      if (textMove &lt;= -100 &amp;&amp; barrage_list_moving.length &gt; 0) {\n        barrage_list_moving.splice(0, 1);\n        now.barrage_phoneWidth = phoneWidth + 80; //隐藏到屏幕里等待发送\n        barrage_list_moving.push(now);\n      }\n    }\n    this.setData({\n      barrage_list: barrage_list_moving\n    });\n  }\n\n})</pre>\n<p>\n	如果你对此不是很明白，欢迎加QQ：554305954和我深入浅出的交流一番。\n</p>', '最近板砖在测试一项基于地理位置的弹幕模块，在搜索了很多相关资料之后总是觉得太过冗余，于是亲自来精简和提炼了一下微信文字弹幕的功能。', '微信小程序，文字弹幕', 0, 1, 0, NULL, '', '', 1, 1578460287, 1578460345, 0);
INSERT INTO `article` VALUES (48, 1, '原创', '//cdn.dmake.cn/attachment/images/20200109/157853716310.png', 'TP5缓存你用好了吗？至新手开发者应用缓存经验', NULL, '<p style=\"text-align:center;\">\n	<img src=\"http://cdn.dmake.cn/attachment/images/20200109/157853716310.png\" />\n</p>\n<p style=\"text-align:left;\">\n	在开始深入Tp5缓存用法之前，我先来回顾一下常见的简便用法：\n</p>\n<p style=\"text-align:left;\">\n<pre class=\"prettyprint lang-php\">Cache::set(\'name\',$value,3600); //缓存的原装用法\n\n// 缓存的助手方法用法\n$options = [\n    \'type\'   =&gt; \'File\', \n    \'expire\' =&gt; 0,\n    \'path\'   =&gt; APP_PATH . \'runtime/cache/\', \n];\ncache($options);\ncache(\'name\', $value, 3600);\nvar_dump(cache(\'name\'));\ncache(\'name\', NULL);\ncache(\'test\', $value, $options); \n\n// 缓存的进阶用法\nCache::tag(\'tag\')-&gt;set(\'name1\',\'value1\');\nCache::set(\'name1\',\'value1\');\nCache::tag(\'tag\',[\'name1\',\'name2\']);\nCache::clear(\'tag\');</pre>\n在上述3种TP5缓存的用法里，更多新人会使用到前两种，读取都很方便。\n</p>\n<p style=\"text-align:left;\">\n	<strong>但是如果你的缓存量有1G怎么办？</strong>\n</p>\n<p style=\"text-align:left;\">\n	很多同学可能想着不可能吧，我的网站一天流量不过100IP，内容不到十万级别怎么可能有这么多缓存呢，而且还有2小时自动过期的机制呢！\n</p>\n<p style=\"text-align:left;\">\n	板砖提出这个问题是因为曾经面领着整个系统缓存还真的超过了1G，然而没有特别的缓存清除机制，导致为了临时修正一个缓存错误不得不清除所有缓存，这个对服务器来说好像也不是什么大事儿，但是从严谨的态度来说这个机制很不好。\n</p>\n<p style=\"text-align:left;\">\n	因此，板砖小小调整了一下缓存助手，助手函数放在common.php中，大家可以围观：\n</p>\n<p style=\"text-align:left;\">\n<pre class=\"prettyprint lang-php\">/**\n * 为了预防cache出错，做的判断\n * @param unknown tag前缀\n * @param unknown 名称\n * @param string $value 当不写value的时候是获取，写的时候是赋值\n * @return mixed|\\think\\cache\\Driver|boolean|NULL\n */\nfunction dcache($tag, $name, $value = \'\', $outtime = 3600)\n{\n\n    if (config(\'cache_status\') == 0) {\n        return null;\n    }\n\n    try {\n        if ($value == \'\') {\n            return cache($name);\n        }\n\n        if ($tag) {\n            return Cache::tag($tag)-&gt;set($name, $value, $outtime);\n        }\n\n        return cache($name, $value, $outtime);\n\n    } catch (Exception $e) {\n        return null;\n    }\n}\n\n// 清除分类缓存方法\nCache::clear(CACHE_ARTICLE);</pre>\n本方法中可以根据配置确认是否缓存，在缓存中强制设置tag，在清除缓存的时候就可以根据tag做分类缓存清除，相对来说逻辑严密一点点。\n</p>', '在板砖才开始上手TP5的时候，这个小东西完全经验着我了，各种助手方法太方便了，cache方法也不例外，但是随着开发经验的丰富才发现，很多官方的原生方法并不是我想象的这样玩儿的。', 'TP5缓存', 0, 1, 0, NULL, '', '', 1, 1578537788, NULL, 0);
INSERT INTO `article` VALUES (49, 1, '原创', '//cdn.dmake.cn/attachment/images/20200113/157892306610.jpg', 'wdcp升级mysql5.5至mysql5.6的8大步骤，亲测有效', NULL, '<p>\n	虽然现在很多网络应用数据库都用上了MySql8.0.19了，但是wdcp的很多用户默认还在使用着老旧的Mysql5.5，一步从Mysql5.5升级到Mysql8.0或许让很多用户感到不安，万一挂了怎么办？那么你可以先试试Mysql5.5升级到Mysql5.6，当然这个教程也支持你升级到5.7。\n</p>\n<p style=\"text-align:center;\">\n	<img src=\"http://cdn.dmake.cn/attachment/images/20200113/157892306610.jpg\" />\n</p>\n<p>\n	接下来让我们8步，在Linux上实现这个过程（以下步骤在CentOs中实验有效），\n</p>\n<p>\n	1. 备份数据库文件中的data文件夹\n</p>\n<pre class=\"prettyprint lang-bsh\">service mysqld stop\nmkdir -p /www/wdlinux/mysql_bk\ncp -pR /www/wdlinux/mysql/data/* /www/wdlinux/mysql_bk\nmv /www/wdlinux/etc/my.cnf /www/wdlinux/etc/my_old.cnf</pre>\n2. 编译高版本数据库的环境\n<pre class=\"prettyprint lang-bsh\">yum install cmake gcc gcc-c++ make zlib-devel ncurses-devel bison -y\n</pre>\n3.&nbsp;下载和编译安装5.6/5.7，用时约20分钟\n<p>\n	<br />\n</p>\n<pre class=\"prettyprint lang-bsh\">wget https://dev.mysql.com/get/Downloads/MySQL-5.6/mysql-5.6.44.tar.gz\ntar -zxvf mysql-5.6.44.tar.gz\ncd mysql-5.6.44\ncmake -DCMAKE_INSTALL_PREFIX=/www/wdlinux/mysql-5.6.44 -DMYSQL_DATADIR=/www/wdlinux/mysql-5.6.44/data -DDOWNLOAD_BOOST=1 -DWITH_BOOST=boost/boost_1_59_0/ -DSYSCONFDIR=/www/wdlinux/etc -DWITH_INNOBASE_STORAGE_ENGINE=1 -DWITH_PARTITION_STORAGE_ENGINE=1 -DWITH_FEDERATED_STORAGE_ENGINE=1 -DWITH_BLACKHOLE_STORAGE_ENGINE=1 -DWITH_MYISAM_STORAGE_ENGINE=1 -DWITH_EMBEDDED_SERVER=1 -DENABLE_DTRACE=0 -DENABLED_LOCAL_INFILE=1 -DDEFAULT_CHARSET=utf8mb4 -DDEFAULT_COLLATION=utf8mb4_general_ci -DEXTRA_CHARSETS=all\nmake &amp;&amp; make install</pre>\n4.&nbsp;修改快捷方式目录，从原目录指向新目录\n<p>\n	<br />\n</p>\n<p>\n	<br />\n</p>\n<pre class=\"prettyprint lang-bsh\">rm -f /www/wdlinux/mysql\nln -sf /www/wdlinux/mysql-5.6.44 /www/wdlinux/mysql</pre>\n5. 初始化mysql在wdcp中的配置\n<p>\n	<br />\n</p>\n<p>\n	<br />\n</p>\n<pre class=\"prettyprint lang-bsh\">sh scripts/mysql_install_db.sh --user=mysql --basedir=/www/wdlinux/mysql --datadir=/www/wdlinux/mysql/data\nchown -R mysql.mysql /www/wdlinux/mysql/data</pre>\n6. 还原数据库数据，这里注意了，还备份了mysql数据库的\n<p>\n	<br />\n</p>\n<p>\n	<br />\n</p>\n<pre class=\"prettyprint lang-bsh\">mv /www/wdlinux/mysql/data/mysql /www/wdlinux/mysql/data/mysql1\ncp -pR /www/wdlinux/mysql_bk/* /www/wdlinux/mysql/data/\ncp support-files/mysql.server /www/wdlinux/init.d/mysqld\ncp support-files/mysql.server /etc/init.d/mysqld\nchmod 755 /www/wdlinux/init.d/mysqld\nchmod 755 /etc/init.d/mysqld</pre>\n7. 启动数据库，验证是否成功，如果成功可以重启服务器进入稳定运行\n<p>\n	<br />\n</p>\n<p>\n	<br />\n</p>\n<pre class=\"prettyprint lang-bsh\">service mysqld start\nmysql_upgrade -uroot -ppwd\nreboot</pre>\n8. 有的朋友在重启之后发现数据库中InnoDb数据库表无法打开，则执行这一步\n<p>\n	<br />\n</p>\n<p>\n	<br />\n</p>\n<pre class=\"prettyprint lang-bsh\">cp /www/wdlinux/mysql_bk/ibdata1 /www/wdlinux/mysql/data\ncd /www/wdlinux/mysql/data\nrm *.err\nrm *.pid\nservice mysqld restart</pre>\n最后一步是重建InnoDb的数据表，希望本篇对遇到问题的朋友有所帮助。\n<p>\n	<br />\n</p>\n<p>\n	如果你在转移过程中有任何问题，请加QQ 554305954联系我。\n</p>', 'mysql5.5和mysql5.6是个性能分水岭，mysql5.6较5.5其中有一个很大的好处，比如给表加字段的时候，5.5或以前的版本会锁表，5.6就不会锁表，而且速度很快，在负载均衡上提升了。', 'wdcp升级mysql', 0, 1, 0, NULL, '', '', 1, 1578923087, 1578923125, 0);
INSERT INTO `article` VALUES (50, 1, '原创', '//cdn.dmake.cn/attachment/images/20200113/157892306610.jpg', 'wdcp升级mysql5.6至mysql8.0.19的升级脚本', NULL, '<p>\n	以下脚本请复制另存为sh文件并上传服务器www文件夹，然后ssh登录服务器，运行命令 sh xxx.sh\n</p>\n<p>\n	升级的时候请注意以下变量，如IN_DIR 、备份文件夹，不过板砖建议你单独运行以下备份数据文件夹的命令：\n</p>\n<p>\n<pre class=\"prettyprint lang-bsh\">service mysqld stop\nmkdir -p /www/wdlinux/mysql_bk\ncp -pR /www/wdlinux/mysql/data/* /www/wdlinux/mysql_bk\nmv /www/wdlinux/etc/my.cnf /www/wdlinux/etc/my_old.cnf</pre>\n上述备份文件的命令在上一篇Mysql5.5升级Mysql5.6已经用过了，安全，建议先行备份，在执行下面的文件\n</p>\n<p>\n<pre class=\"prettyprint lang-bsh\">#!/bin/bash\n# mysql 8.0 update scripts\n# Author: wdlinux\n# Url: http://www.wdlinux.cn\n# Modify: windsage\n\nIN_DIR=\"/www/wdlinux\"\n\nif [ ! $1 ];then\n\n        MYS_VER=\"8.0.19\"\nelse\n        MYS_VER=$1\nfi\n\necho \"升级有风险 操作需谨慎\"\necho \"欢迎学习交流互助提高\"\necho \"qq：242013800\"\necho\nread -p \"DO YOU REALLY WANT TO UPDATE? (Y/N): \" yn\nif [ \"$yn\" == \"Y\" ] || [ \"$yn\" == \"y\" ]; then\n        echo \"MYSQL IS NOW UPDATING!\"\nelse\n        exit\nfi\necho\necho \"-------------------------------------------------------------\"\necho\n\n\nif [ ! -f mysql-boost-${MYS_VER}.tar.gz ];then\n# 因mysql5.7需要boost，所以这个直接是包含boost的源码包\n        wget -c https://cdn.mysql.com//Downloads/MySQL-8.0/mysql-boost-${MYS_VER}.tar.gz\nfi\nyum install -y cmake bison libmcrypt-devel libjpeg-devel libpng-devel freetype-devel curl-devel openssl-devel libxml2-devel zip unzip\nif [ ! -d $IN_DIR/mysql-${MYS_VER} ];then\nmkdir -p $IN_DIR/mysql-${MYS_VER}\n# 数据库文件夹默认要求是data\nmkdir -p $IN_DIR/mysql-${MYS_VER}/data\nfi\n\nif [ ! -d mysql-${MYS_VER} ];then\ntar zxvf mysql-${MYS_VER}.tar.gz\nfi\ncd mysql-${MYS_VER}\necho \"START CONFIGURING MYSQL\"\nsleep 3\nmkdir -p build\ncd  build/\nmake clean\ncmake. \\\n-DCMAKE_INSTALL_PREFIX=$IN_DIR/mysql-$MYS_VER \\\n-DSYSCONFDIR=$IN_DIR/etc \\\n-DDEFAULT_CHARSET=utf8 \\\n-DDEFAULT_COLLATION=utf8_general_ci \\\n-DWITH_SSL=bundled \\\n-DWITH_DEBUG=OFF \\\n-DWITH_EXTRA_CHARSETS=complex \\\n-DENABLED_PROFILING=ON \\\n-DWITH_INNOBASE_STORAGE_ENGINE=1 \\\n-DWITH_MYISAM_STORAGE_ENGINE=1 \\\n-DWITH_MEMORY_STORAGE_ENGINE=1 \\\n-DWITH_ARCHIVE_STORAGE_ENGINE=1 \\\n-DWITH_BLACKHOLE_STORAGE_ENGINE=1 \\\n-DENABLE_DOWNLOADS=1 \\\n-DWITH_BOOST=../boost/boost_1_68_0/\n[ $? != 0 ] &amp;&amp; echo \"NO! CONFIGURE ERROR!  :(\" &amp;&amp; exit\necho \"START MAKE\"\nsleep 3\nmake\n[ $? != 0 ] &amp;&amp; echo \"NO! MAKE ERROR!  :(\" &amp;&amp; exit\necho \"START MAKE INSTALL\"\nsleep 3\nmake install\n[ $? != 0 ] &amp;&amp; echo \"NO! MAKE INSTALL ERROR!  :(\" &amp;&amp; exit\n\nservice mysqld stop\n# 建立备份文件夹，这个可以自定义设置,建议一开始就先备份，不要问我为什么，这样可视化安全更放心\nif [ ! -d /www/wdlinux/mysql_bk ];then\nmkdir -p /www/wdlinux/mysql_bk\ncp -pR /www/wdlinux/mysql/data/* /www/wdlinux/mysql_bk\nfi\nrm -f /www/wdlinux/mysql\nln -sf $IN_DIR/mysql-$MYS_VER /www/wdlinux/mysql\nsh scripts/mysql_install_db.sh --user=mysql --basedir=/www/wdlinux/mysql --datadir=/www/wdlinux/mysql/data\nchown -R mysql.mysql /www/wdlinux/mysql\nchown -R mysql.mysql /www/wdlinux/mysql/data\nmv /www/wdlinux/mysql/data/mysql /www/wdlinux/mysql/data/mysqlo\ncp support-files/mysql.server /www/wdlinux/init.d/mysqld\nchmod 755 /www/wdlinux/init.d/mysqld\nservice mysqld restart\nln -sf $IN_DIR/mysql/lib/libmysqlclient.so.20. /usr/lib/libmysqlclient.so.20\ncd ..\nrm -rf mysql-${Ver}/\n# rm -rf mysql-${Ver}.tar.gz\nsleep 2\nservice mysqld restart\nsleep 2\nmysqld --initialize --user=mysql --console\n# 或者把备份的数据库复制回来，注意不要覆盖performance_schema\n# cp -pR /mnt/mysql_bk/* /www/wdlinux/mysql/data/  \necho\necho \"-------------------------------------------------------------\"\necho \"数据库需要初始化，自动生成的密码会存入mysql的日志，默认在/var/log/mysql/error.log\"\necho\necho \"此时查看mysql日志，应该一切正常了，但新手朋友注意了，此时你的httpd无法启动了，需要编译安装php。\"\necho\n</pre>\n</p>', 'mysql8.0已经上线了4个年头了，但是在生产环境中为了稳定安定评定的运营，DBA们暂时不对环境中的Mysql5.6或者5.7下手，今天板砖参考wdcp论坛中的基友的帖子来试验了一番Mysql5.6升级Mysql8.0.19。', 'wdcp升级mysql,mysql升级8.0', 0, 1, 0, NULL, 'wdcp论坛', 'https://www.wdlinux.cn/bbs/viewthread.php?tid=63626&highlight=mysql', 1, 1578924214, NULL, 0);
INSERT INTO `article` VALUES (51, 5, '原创', '//cdn.dmake.cn/attachment/images/20200119/15794117754.jpg', '那年创业，三进三出给我的商业启蒙', NULL, '<p>\n	2014年让板砖记忆尤新的是：我开始创业了，并且一塌糊涂。2014年的创业其实是一出闹剧，今天简单的用故事脚本的路子回顾一下。\n</p>\n<p>\n	背景：\n</p>\n<p>\n	1. 男，管理专业，毕业2年，会编程无存款，家庭从事装修行业\n</p>\n<p>\n	2. 选择创业，方向不定，到处探索\n</p>\n<p>\n	3. 背负想迅速稳定并开启一段独立事业\n</p>\n<p style=\"text-align:center;\">\n	<img src=\"http://cdn.dmake.cn/attachment/images/20200119/15794117754.jpg\" alt=\"创业\" /> \n</p>\n<p>\n	<strong>创业跳坑一：进入陌生行业，尝试不擅长岗位</strong> \n</p>\n<p>\n	板砖创业的第一站，是选择了装修行业。因为整个家族的人都会，从此入手我应该有强大的技术支持。创业初始必然没有师傅带领，于是乎通过招聘进入重庆一别墅装修公司，客户起步高、公司规模大，学习范围广。\n</p>\n<p>\n	板砖在装修公司呆了1个月，浅要的学习了家装行业的各种模式，于是乎就犯了第一个错（当然因为这个错让我提早认识到自己不适合这个行业）：拉大旗、找团队、找业务。\n</p>\n<p>\n	板砖回家迅速的集结了周边几个会CAD、会销售、有闲暇时间的同学、朋友一起筹措着成立正式的团队，开始跨行的新篇章，然而接下来的实践经历让我们一次性扑街：\n</p>\n<p>\n	1. 没有资金形成稳定团队、客户可视化的经营场地；\n</p>\n<p>\n	2. 没有成熟案例，不能让目标客户放心接洽；\n</p>\n<p>\n	3. 没有成熟经验，在客户现场频繁出错，不能准确及时的给予报价。\n</p>\n<p>\n	创业第一坑——完结。\n</p>\n<p>\n	总结：\n</p>\n<p>\n	1. 商业中，好的点子不代表好的方案，有人支持并不代表一定能够获得支持\n</p>\n<p>\n	2. 跨行业等于完全重来，学习不应该浅唱则止，长期磨练和积累是门槛\n</p>\n<p>\n	<br />\n</p>\n<p>\n	<strong>创业跳坑二：进入熟悉的行业，选择了小众的产品</strong> \n</p>\n<p>\n	创业第一坑让我背负了一点小债，并且失败经验完全不足以让我开启第二段创业。但是因为第一段折腾完毕之后需要挣钱还债，于是选择了从事互联网行业运营作为再次创业的起点，毕竟互联网创业基本上完全靠一张脸（<img src=\"https://www.dmake.cn/static/kindeditor/plugins/emoticons/images/28.gif\" border=\"0\" alt=\"\" />玩笑，但是2014年的互联网创业起点的确很低）。\n</p>\n<p>\n	<strong>因为没有资金要么找合伙人，要么找成熟平台入伙</strong>！在决定之后在本地寻找合适的机会、调研和调查了周边城市的发展情况后，决定入伙本地一门户网站，进入之后惊奇发现这个门户网站居然只有2个编辑<img src=\"https://www.dmake.cn/static/kindeditor/plugins/emoticons/images/3.gif\" border=\"0\" alt=\"\" />，不管三七二十一，只要有钱就挣！团队虽小，但是我准备了以下几个可以快速切入业务的方法，并取得了好的结果：\n</p>\n<p>\n	1. 拜访目标商户和客户（告知市场又多了一个搅局者）\n</p>\n<p>\n	2. 给友好的目标客户提供定制化的方案（告诉市场我比别人靠谱）\n</p>\n<p>\n	3. 到处展示自己、挑战竞争对手（告诉市场，我真的比别人强）\n</p>\n<p>\n	4. 抱大腿，找到可以频繁合作的企业并开展合作（告诉市场，我也是有后台的）\n</p>\n<p>\n	如果市场不变化，我可能还在做门户网站吧，但是事实证明，你的计划永远比不上市场变化。\n</p>\n<p>\n	2015年开始，微信和公众号发力，让越来越多的人涌入自媒体行业，小型门户网站逐步垮台，板砖和合伙大佬提出扩大产品销售范围被拒绝之后，毅然离开，结束了第二段伪创业。\n</p>\n<p>\n	总结：\n</p>\n<p>\n	1. 商业中不会存在“一招鲜吃遍天”的业务，至少我们能接触到的技术和市场不存在；\n</p>\n<p>\n	2. 积累人脉，有1次以上美好的合作经历能够帮助持续业务的机会\n</p>\n<p>\n	3. 合作人很重要，合作团队的眼光同样重要\n</p>\n<p>\n	<br />\n</p>\n<p>\n	<strong>创业跳坑三：我有的是产品，有的是能力，还有口碑合作企业，为什么要再战市场？</strong> \n</p>\n<p>\n	在第二段创业结束后，板砖选择了自己一个人干，咱有技术有能力，还有之前积累的合作伙伴，我怕啥？\n</p>\n<p>\n	于是乎这一次板砖选择了成立自己的公司：找办公室、注册公司、前期缺人找朋友来顶岗、开发自己的平台···\n</p>\n<p>\n	在自己注册公司之后一个月惊奇的发现自己总成交订单才——5000块，刨除成本后一人分了几百块钱<img src=\"https://www.dmake.cn/static/kindeditor/plugins/emoticons/images/7.gif\" border=\"0\" alt=\"\" />此后如何能够挣稳定的钱成了一个问题，在几经思考后于是开拓了本地互联网市场没有的业务：\n</p>\n<p>\n	1. 为企业定制网站功能\n</p>\n<p>\n	2. 去威客平台接业务\n</p>\n<p>\n	3. 继续本地客户的宣传业务\n</p>\n<p>\n	4. 代理IT产品销售和电商业务运营\n</p>\n<p>\n	经过短暂调整，本地的几个重要客户意识到他们也需要定制化的技术服务，这不仅拉平了接下来2年内的日常开销，也为板砖进入正式的商业合作打下了基础。\n</p>\n<p>\n	在为其他企业代理业务的时候出了一个插曲：2016年的时候农村淘宝、全民创业和电商普及扰乱了整个公司的视野，板砖和合伙人为了一起去配合某趋于政府的一些行为，把盈利项目放到了身后，导致当年的所有商务进账非常紧张，到最后不得不靠比赛奖金和政府扶持过日子。之后对此类业务一刀切，重新回到了正常经营的路上，但是此时发现：绝大多数曾经的竞争对手已经稳步到达同一条业务起跑线上。\n</p>\n<p>\n	因为之前的业务关系和市场动作，板砖的公司也有一定的影响力，几个行业领导企业在互联网行业市场的推动下意识到互联网行业在未来一段时间必有大用，于是拉拢板砖入伙，至此，创业第三坑再次进入焦灼状态。\n</p>\n<p>\n	总结：\n</p>\n<p>\n	1. 企业宗旨、商务定位一定要明确，并且不能为其他因素干预\n</p>\n<p>\n	2. 与政府的合作关系不一定会是稳定的关系，因此做生意应该保持一定的距离\n</p>\n<p>\n	3. 只要时刻努力，为明确的人创造了明确的收益，那么潜在的商业机会也是有重要价值的\n</p>\n<p>\n	4. 业务应该跟着市场变，特别是互联网行业，变化有多快机会才有多少\n</p>\n<p>\n	<br />\n</p>\n<p>\n	关于创业，在最后和合伙蜜月期中，板砖选择了退出，因为可以计算的市场份额和商业大环境已经注定，要么安于此要么走出去。\n</p>\n<p>\n	<br />\n</p>', '最该学习的时候选择了下海，终于被浪花冲上了岸···', '创业，欠债', 0, 1, 0, NULL, '', '', 1, 1579413087, 1579414257, 0);
INSERT INTO `article` VALUES (52, 4, '原创', '//cdn.dmake.cn/attachment/images/20200120/15795058056.jpg', 'Python当道，Java流行，PHP建设业务系统的前景在哪里？', NULL, '<p style=\"text-align:center;\">\n	<img src=\"http://cdn.dmake.cn/attachment/images/20200120/15795058056.jpg\" alt=\"世界开发语言2019年排行，php位列第8\" /> \n</p>\n<p style=\"text-align:left;\">\n	随着区块链、互联网泡沫的双重清洗下，Python激流勇进，C和C++因为其技术特性继续保持前列。在很多业务系统加持下，Java和C#的地位依然没有撼动。\n</p>\n<p style=\"text-align:left;\">\n	而PHP的开发者们，在2019年度过得并不好！\n</p>\n<p style=\"text-align:left;\">\n	互联网创业入门最低成本最低的开发语言是谁——PHP！\n</p>\n<p style=\"text-align:left;\">\n	国内IT行业入手最快、人力最多的是开发语言是谁——PHP！\n</p>\n<p style=\"text-align:left;\">\n	应用场景最少、但是使用人群最多的是谁——PHP！\n</p>\n<p style=\"text-align:left;\">\n	全年裁员最多的IT人员标签是谁——PHP！\n</p>\n<p style=\"text-align:left;\">\n	曾经世界上最好的语言到底惹谁了？为什么在2019年过的是如此难堪？\n</p>\n<p style=\"text-align:left;\">\n	作为PHP的浅要开发者，板砖有话说：\n</p>\n<p style=\"text-align:left;\">\n	目前的PHP开发者绝大多数都是垃圾！好不吝惜的用这个词来形容绝大多数PHP开发者的时候我是痛心疾首的，为什么？\n</p>\n<p style=\"text-align:left;\">\n	学历低，没有任何计算机相关基础经验；经验无，没有任何互联网开发或运营思维；技术差，培训机构只会教你Yii2、Laravel、Yaf、Thinkphp这几种入门超级低的框架，只要不是文盲，看着文档都能做功能；学习不深入，什么设计模式，模块等都不懂，只要完成功能其他都不管。\n</p>\n<p style=\"text-align:left;\">\n	在2019年互联网泡沫的纷纷破灭之下，很多PHP从业者都是第一批被裁人员，继而无措、转语言、转行，搞得是一地鸡毛！\n</p>\n<p style=\"text-align:left;\">\n	那么PHP建设业务系统的前景将会是怎么样的呢？\n</p>\n<p style=\"text-align:left;\">\n	针对于互联网端，PHP依然将是占比最大的开发语言，无论全球流行的WordPress和国内流行的Discuz系统，都是基于PHP开发的，而且占据比例惊人。\n</p>\n<p style=\"text-align:left;\">\n	因此板砖认为PHP从业者依然不必担忧未来如何，事业如何，只需要从以下几个关键节点中不断提升自我，PHP依然是世界上最好的开发语言！\n</p>\n<p style=\"text-align:left;\">\n	<br />\n</p>\n<ul>\n	<li>\n		深入学习PHP的原生功能、了解软件开发的一些常用模式、了解业务架构中的的基本模式\n	</li>\n	<li>\n		不要只沉迷于一种开发语言，能够随时根据业务需求切换自己的能力\n	</li>\n	<li>\n		多深入运营业务，能够知道为什么要做这个产品，怎么去做好，而不是编程\n	</li>\n	<li>\n		多学习经典案例，多和行业人士交流，保持对技术方面的敏锐程度\n	</li>\n</ul>\n<p>\n	<br />\n</p>', 'PHP占据着“世界上最好的开发语言”多年，在2018年起Python出乎意料的超越了PHP，Java也稳中有升占据鳌头，那么PHP建站的未来将是如何呢？', 'PHP', 0, 1, 1, NULL, '', '', 1, 1579506099, 1579507007, 1);
INSERT INTO `article` VALUES (53, 1, '原创', '//cdn.dmake.cn/attachment/images/20200113/157892306610.jpg', 'PHP系统配套数据库不要在用Mysql5.7了，Mysql8.0负载翻了近4倍', NULL, '<p>\n	MySQL 8.0 正式版 8.0.12 已发布，官方表示 MySQL 8 要比 MySQL 5.7 快 2 倍，还带来了大量的改进和更快的性能！\n</p>\n<p>\n	而阿里云技术团队对比2020年最新版Mysql8.19版本和Mysql5.7版本后发现，在某些服务器硬件环境下最大性能提升了3-4倍！\n</p>\n<p>\n	<img src=\"http://cdn.dmake.cn/attachment/keditor/image/20200123/20200123102458_19560.png\" alt=\"mysql8.19与Mysq5.7的read负载对比\" /> \n</p>\n<p>\n	<img src=\"http://cdn.dmake.cn/attachment/keditor/image/20200123/20200123102510_19250.png\" alt=\"mysql8.19与Mysq5.7的insert负载对比\" /> \n</p>\n<p>\n	<img src=\"http://cdn.dmake.cn/attachment/keditor/image/20200123/20200123102518_52401.png\" alt=\"mysql8.19与Mysq5.7的delete负载对比\" /> \n</p>\n<p>\n	<img src=\"http://cdn.dmake.cn/attachment/keditor/image/20200123/20200123102555_62037.jpg\" alt=\"mysql8.19与Mysq5.7的update负载对比\" /> \n</p>\n<p>\n	基本上在这里，我只提取了InnoDB行操作，它执行查找（读取），删除，插入和更新。当线程数量增加时，MySQL 8.0明显优于MySQL 5.7！在这两个版本中都没有针对配置项进行任何个性化变更，只有我统一配置的参数项。所以这两个版本中的配置几乎都使用默认值。<br />\n有趣的是，MySQL团队关于新版本中读写性能的声明，这些图表指出了性能的显著提高，特别是在高负载服务器上。想一下MySQL 5.7和MySQL 8.0在InnoDB行操作上的区别，确实存在有很大的不同，特别是当线程数增加的时候。MySQL 8.0表明，无论工作负载如何，它都能高效地运行。\n</p>\n<p>\n	PHP系统标配Mysql8.19的好处不言而喻了：\n</p>\n<ul>\n	<li>\n		提升系统访问和业务数据交换的效率\n	</li>\n	<li>\n		减少PHP程序员对系统优化的难度，特别是为了解决性能瓶颈做的垂死挣扎\n	</li>\n</ul>\n<p>\n	以上两个场景就是你为什么需要从Mysql5.7升级到Mysql8.19的重要原因。当然，从 MySQL 5.7 升级到 MySQL 8.0 仅支持通过使用 in-place 方式进行升级，并且不支持从 MySQL 8.0 降级到 MySQL 5.7（或从某个 MySQL 8.0 版本降级到任意一个更早的 MySQL 8.0 版本）。唯一受支持的替代方案是在升级之前对数据进行备份。\n</p>', '步入2020年了，很多业务系统居然还在使用Mysql5.7甚至更低版本，这里板砖有话要说了，一边在代码上不断去有话提升一点点性能，一边又无视原地升级性能的绝佳方式是什么心态？', 'PHP Mysql', 0, 1, 0, NULL, '', '', 1, 1579746607, 1579746635, 0);
INSERT INTO `article` VALUES (54, 2, '原创', '//cdn.dmake.cn/attachment/images/20200127/158009443310.png', '如何利用金字塔原理推导公司业务和团队架构', NULL, '<p style=\"text-align:left;\">\n	<strong>金字塔原理是什么？简单来说就是利用像</strong><strong>金字塔一样的层次显著的结构去清晰的思考、设计、表达和反应我们想去表达的、设计的内容。</strong> \n</p>\n<p style=\"text-align:left;\">\n	首先，您需要一个可以还原你思维的软件工具，以下推荐Xmind，ProcessON，Thebrain，MindMaster这4种导入软件，都能够实现上图效果。\n</p>\n<p style=\"text-align:left;\">\n	然后，您需要通过金字塔原理相关课程了解SCQA、“归纳推理”、“演绎推理”三个关于结果性和结果向导性知识要点，这3个要点属于金字塔原理中战略指导的实践要点。\n</p>\n<p style=\"text-align:left;\">\n	最后，开始用思维导图+金字塔原理推导我们的业务和团队规模吧。\n</p>\n<p style=\"text-align:left;\">\n	<img src=\"http://cdn.dmake.cn/attachment/images/20200127/158009443310.png\" alt=\"思维导图利用金字塔原理推导公司业务和团队架构\" /> \n</p>\n<p style=\"text-align:left;\">\n	1. 利用S（场景）C（冲突）Q（问题）A（答案）提炼我们的简单业务场景：\n</p>\n<p style=\"text-align:left;\">\n	首先利用场景判断我们的选题的正确性，排除因为运营目的和业务对象不匹配\n</p>\n<ul>\n	<li>\n		我们的产品/业务适用于什么对象：企业？个人？\n	</li>\n	<li>\n		我们的产品/业务适用于什么场景？它的受众群是企业？个人？\n	</li>\n</ul>\n<p>\n	然后通过运营目的和业务对象的冲突去提炼我们的业务/产品和应用场景的冲突\n</p>\n<ul>\n	<li>\n		我们的产品/业务中心和业务对象距离太远\n	</li>\n	<li>\n		我们的产品/业务过于复杂/简单\n	</li>\n	<li>\n		我们的产品/业务包装设计等不适合目标客户\n	</li>\n	<li>\n		我们的定价策略和目标客户不匹配\n	</li>\n	<li>\n		···\n	</li>\n</ul>\n<p>\n	最后通过场景的冲突提出可以分离的问题，同时也可以回复答案（建议最后给问题写答案，这样可以避免自问自答中断思维逻辑）：\n</p>\n<ul>\n	<li>\n		我们怎么样让产品/业务和业务对象拉近业务距离\n	</li>\n	<li>\n		我们怎么样优化产品/业务设计\n	</li>\n	<li>\n		我们怎么样调整价格策略\n	</li>\n</ul>\n<p>\n	当问题一一列举之后就是为这些问题提供答案，你会发现利用SCQA原理可以以金字塔结构从上至下的解决目标问题。\n</p>\n<p>\n	2. 利用归纳推理、演绎推理和SCQA解决复杂的经营业务问题：\n</p>\n<p>\n	第一步：利用归纳推理排除对经营无关的问题，归纳推理是由点及面的推理方法，为了满足我们的业务主题，可以列举多个可能对业务产生结果的行为可能性，例如\n</p>\n<p>\n	<br />\n</p>\n<ul>\n	<li>\n		在上海建立业务中心\n	</li>\n	<li>\n		招聘100个具备销售基础的市场人员\n	</li>\n	<li>\n		搭建一个网络销售平台\n	</li>\n	<li>\n		购买100辆汽车\n	</li>\n	<li>\n		···\n	</li>\n</ul>\n<p>\n	<strong>在归纳推理中，尽可能的把有用的“点”罗列出来，避免把时间放到扯淡上了，每个推理中可能会派生出子结构，但是切记，任何结构不要超过三层，超过三层的结构可能会掉入逻辑黑洞。这里使用SCQA方法简要判断和筛查。</strong> \n</p>\n<p>\n	第二步：利用演绎推理，为归纳的主线提供可行性判断，这里会使用SCQA去判断合理性，例如：\n</p>\n<p>\n	<br />\n</p>\n<ul>\n	<li>\n		想扩大业务就招聘100个业务员\n	</li>\n	<li>\n		100个业务员投入成本太大，且投入过度\n	</li>\n	<li>\n		那最多需要多少个业务员\n	</li>\n	<li>\n		在100万人口中需要2个业务员，上海市4000万人口，所以需要40个\n	</li>\n</ul>\n<p>\n	<br />\n</p>\n<p>\n	你看，这里就利用SCQA去判断了合理性，而通过演绎推理法算出了业务团队规模。\n</p>\n<p>\n	当然，今天板砖写的是2个简单的利用金字塔原理推导公司业务和团队架构案例，如果你想更深入做公司、平台的运营，推荐您把下文也一并看了，选择您最适合的学习方法。\n</p>\n<p>\n	<strong> </strong>\n</p>\n<p>\n	<strong>目前在互联网上关于金字塔原理的书籍和有生课程有很多，哪些课程是有用的必要的目前还是一个争议议题，但是可以肯定的是——一百个人心目中有一百个哈姆雷特，找到你认为行之有效的就是最好的。</strong>\n</p>\n<p>\n	<strong>很多教学派推荐芭芭拉·明托于2002年出版的《金字塔原理》一书，板砖认为本书属于教学派，对于企业和创业阶段的小伙伴来说不是很合适。板砖认为下方喜马拉雅免费课程是接地气的解读了金字塔原理，并且可以直接投入实践。下面，板砖将用该课程的知识演示公司业务和团队架构的推理过程。</strong>\n</p>\n<p style=\"text-align:center;\">\n	<strong><img src=\"http://cdn.dmake.cn/attachment/keditor/image/20200127/20200127111309_49462.jpg\" alt=\"\" width=\"520\" height=\"723\" title=\"\" align=\"\" /> </strong>\n</p>\n<p>\n	<br />\n</p>\n<p>\n	<br />\n</p>', '板砖在工作的一长段时间里推导公司业务和团队架构的时候还停留在纸笔上，在2018年起发现——思维导图软件真香！', '金字塔原理', 0, 1, 0, NULL, '', '', 1, 1580095862, 1580096795, 1);
INSERT INTO `article` VALUES (55, 2, '原创', '//cdn.dmake.cn/attachment/images/20200128/15801785017.png', '从PV/UV到点击率跳出率解答百度统计中的平台运营哲学', NULL, '<p>\n	新晋网站平台管理人员，特别是非网站运营相关空降到该岗位的人必看！\n</p>\n<p style=\"text-align:center;\">\n	<img src=\"http://cdn.dmake.cn/attachment/images/20200128/15801785017.png\" alt=\"百度统计中的平台运营哲学\" /> \n</p>\n<p>\n	首先我们来理一下本篇主要内容：\n</p>\n<ul>\n	<li>\n		什么是PV/UV/点击率/跳出率/平均访问时常\n	</li>\n	<li>\n		如何通过来源分析优化你的内容分发平台\n	</li>\n	<li>\n		如何通过访客分析优化你的网站页面\n	</li>\n	<li>\n		页面点击图有什么作用\n	</li>\n</ul>\n<p>\n	1.&nbsp;什么是PV/UV/点击率/跳出率/平均访问时常\n</p>\n<p>\n	<span id=\"__kindeditor_bookmark_start_61__\">PV是指百度中统计到的该页面被访问了多少次（只是统计到该页面被访问或被刷新多少次，数据比较笼统）</span> \n</p>\n<p>\n	<span>UV是指百度中统计到的有多少个不同的浏览器访问到该页面多少次（该数据比较真实可靠）</span> \n</p>\n<p>\n	<span>点击率是指再百度搜索结果中每100次平均被点击几次，这对付费推广数据有指导意义，确认你的推广文案是好是差</span> \n</p>\n<p>\n	<span>跳出率是指每100个点击进入该页面的直接关闭该页面的概率，这对运营和内容创作者来说是最重要的，跳出率过高说明该页面的内容价值很低</span> \n</p>\n<p>\n	2.&nbsp;如何通过来源分析优化你的内容分发平台\n</p>\n<p>\n	<img src=\"http://cdn.dmake.cn/attachment/keditor/image/20200128/20200128104936_64286.png\" alt=\"\" />\n</p>\n<p>\n	通过来源类型，你可以推断出主要的流量分布：\n</p>\n<p>\n	<ul>\n		<li>\n			直接访问说明是精准的访问者通过快捷方式、书签、直接输入网址而来；\n		</li>\n		<li>\n			搜索引擎和其他搜索引擎说明是通过关键词搜索而来，这里会产生点击率这个数值；\n		</li>\n		<li>\n			外部链接指你通过分享链接到QQ群、朋友圈以及在微博等发布的链接带来的流量；\n		</li>\n	</ul>\n</p>\n<p>\n	如果你的外部链接占比非常高，说明你的平台具有很好的社群基础；如果是直接访问占比高，说明你的网址已经具备一定的品牌效应；如果你的搜索引擎占比高，说明你的网站价值和SEO做的很好。\n</p>\n<p>\n	<img src=\"http://cdn.dmake.cn/attachment/keditor/image/20200128/20200128104948_36816.png\" alt=\"\" />\n</p>\n<p>\n	而通过来源网站可以很清晰的把对你业务影响巨大的网站找出来，你可以通过投放付费广告或者相关的合作产生更多的流量。\n</p>\n<p>\n	3.如何通过访客分析优化你的网站页面\n</p>\n<p>\n	方可属性包含性别、地区、行业、年龄、职业、兴趣爱好、系统环境、忠诚度方面的数据，这些数据可以帮助运营者优化内容风格，比如说女性群体占比特别巨大的应感性化协作，行业占比比较统一的情况下可以写作行业专业化内容等等。\n</p>\n<p>\n	4.页面点击图有什么作用\n</p>\n<p>\n	页面点击图可以统计单个页面和模板页面，单个页面例如网站首页，模板页面例如文章详情页面。\n</p>\n<p>\n	如果页面点击图中的热力区仅集中在几个点上，单个页面说明这个页面上有价值的内容很少，仅有几个点吸引人，而在模板页面上的表现形式说明有些固定位置的推荐或者按钮具备价值。\n</p>\n<p>\n	那么是否说点击图满屏都是重点才是好的呢？答案是否定的！\n</p>\n<p>\n	最好的热力图体现为，你期望展现的热点区域数值很高，说明了你的价值输出是成功的，在其他区域热力值很高，说明网友对这部分也是感兴趣的，你可以作为拓展。\n</p>\n<p>\n	<strong>因篇幅有限，关于百度统计中免费的数据分析详解请点击下方按钮下载学习！</strong>\n</p>\n<p>\n	<a href=\"/attachment/file/bdv3.pdf\" target=\"_blank\">网站分析白皮书V3.0.pdf下载</a> \n</p>', '百度统计不仅仅是一个流量统计工具，如果你没有自己的平台数据分析模块，那么百度统计将是你改变网站结构、优化网站内容的重要工具。', '运营', 0, 1, 0, NULL, '', '', 1, 1580179636, 1580180617, 0);
INSERT INTO `article` VALUES (56, 2, '原创', '//cdn.dmake.cn/attachment/images/20200226/15826961676.png', '在自媒体中巧妙使用话题营销3大原理和3大步骤打造营销爆款', NULL, '<p style=\"text-align:left;\">\n	板砖作为一个互联网人内心其实很挣扎的：我既厌烦网络营销各环节中的阴暗面，也深知更要带领我的团队从善做好业务的运营。\n</p>\n<p style=\"text-align:left;\">\n	在结合了《品牌营销100讲》的一两个观点和我的实操经验以后，总结出来了下述《话题营销打造热门事件》方法模板。\n</p>\n<p style=\"text-align:left;\">\n	这个模板从理论到实操都有深刻的挖掘，希望能够帮到互联网行业中正在摸索的你。\n</p>\n<p style=\"text-align:center;\">\n	<img src=\"http://cdn.dmake.cn/attachment/images/20200226/15826961676.png\" alt=\"在自媒体中巧妙使用话题营销3大原理和3大步骤打造营销爆款\" /> \n</p>\n<p style=\"text-align:left;\">\n	要想理解话题的热门事件的打造原理和方法，需要和一些实际的案例相结合，那么在上述思维理论之后我来做一一详解。\n</p>\n<p style=\"text-align:left;\">\n	<strong>话题营销为什么会有起点呢，难道话题不应该是突然产生的吗？</strong> \n</p>\n<p style=\"text-align:left;\">\n	请问你有没有想过，我么和别人搭话、推销自己、满足别人等等是不是扯了很多话题，短句、小故事、听别人说或者朋友圈自爆，其实这些行为都是由目的的——为了满足自己的目的，或者是为了引诱他人行为为目的。\n</p>\n<p style=\"text-align:left;\">\n	那么这样大家就理解了“话题的起点这个问题”，从我们很多的个人经验和通过网络热点的分析得出“话题起点三要素：争议、好奇、送福利”是怎么表现得呢？\n</p>\n<p style=\"text-align:left;\">\n	<strong>那么满足了三要素后，是否就合适做话题呢？</strong> \n</p>\n<p style=\"text-align:left;\">\n	答案是否定的，如果是两个人聊天抖机灵，满足三要素可以保证你的胜率比较大。但是为了做一场社会性质的话题营销至少还需要以下4个要素：\n</p>\n<ul>\n	<li>\n		在同质化严重的时候，那么有1个差异化特别明显的地方，都可以拿来做话题\n	</li>\n	<li>\n		在有很大群众基础的时候，发布主题的新鲜动态也能做主题，类似明星发动态\n	</li>\n	<li>\n		在有对这个社会有重大的理论、技术或三观起到震动作用的，例如特斯拉要上火星了\n	</li>\n	<li>\n		迎合网络热门，切入主题很自然的，例如肺炎期间某硬件设备急速援建雷神山\n	</li>\n</ul>\n<p>\n	我们用3个微博热点来举例。\n</p>\n<p style=\"text-align:center;\">\n	<img src=\"http://cdn.dmake.cn/attachment/keditor/image/20200226/20200226143708_86301.jpg\" alt=\"话题营销举例\" /> \n</p>\n<p style=\"text-align:left;\">\n	我们拿今天的微博热榜来举例：\n</p>\n<p style=\"text-align:left;\">\n	上述四条满足了“好奇”这个要素，第三条满足了“迎合网络热度”这个要素，第四条、第五条满足了“巨大震动”这个要素。\n</p>\n<p style=\"text-align:left;\">\n	<strong>原理说完说心理：话题营销其实是在利用网友的心理做文章的</strong> \n</p>\n<p style=\"text-align:left;\">\n	<br />\n</p>\n<ul>\n	<li>\n		有个B站UP主说得好“坏名声好过没有名声”，首先确认了自己的话题目的之后，就注定要收获“某种名声”；\n	</li>\n	<li>\n		做营销的人经常把“痛点”作为营销的出发点，那么试想哪儿来那么多痛点让你抒发呢？科技革新频率也不过如此，话题营销利用的更多的是“痒点、槽点、爽点”。\n	</li>\n</ul>\n<p>\n	还是以上述案例为例，没有一个热点是靠“痛点”来营销的，也并不是所有热点都收获了好名声。\n</p>\n<p>\n	第一条，触痛的是整个网民的吐槽——原来后勤是这样的，最后一条触痛的是网民对官僚行为的吐槽；第三条是全国人民的爽点——就快要解放了；股市触动的是金融人员的痒点——欲罢不能，离之可惜。\n</p>\n<p>\n	因篇幅有限，板砖在这里不对话题性小的具体方式方法做解读，新手朋友们只需要跟着<span>《话题营销打造热门事件》方法模板实操，一定会得到超越期待的答案。</span>\n</p>', '在这个信息爆炸的时代，谁抓住了眼球就是抓住了第一桶金。', '话题营销', 0, 1, 1, NULL, '', '', 1, 1582696424, 1582700637, 0);
INSERT INTO `article` VALUES (57, 1, '原创', '//cdn.dmake.cn/attachment/images/20200119/15794117754.jpg', 'PHP利用SMTP发送群发单显邮件并显示自定义发件名的代码', NULL, '<p>\n	为了减少更多的开发者在这个上面浪费时间，今天板砖分享一下我的实施代码，虽然不是新发明发现，不过也是一种实操思路！\n</p>\n<p>\n	我的思路是：\n</p>\n<p>\n	<ol>\n		<li>\n			通过传入的发送地址列表，判断是否是直接发送还是群发单显\n		</li>\n		<li>\n			通过修改sendmail方法中代码<span id=\"__kindeditor_bookmark_start_4__\">&nbsp;$header .= \"From: $from&lt;\".$from.\"&gt;\\r\\n\"; 实现自定义发件名</span>\n		</li>\n	</ol>\n</p>\n<pre class=\"prettyprint lang-php\">    $sendto = \"\"; //主要发送\n    $cc = \"\"; //抄送\n    $bcc = \"\"; //秘密抄送\n\n    //***************判断发送的是1个人还是一群人,大于2人的时候就群发单显\n    $list = explode(\",\", $to);\n    if (count($list) &gt; 1) {\n        $sendto = $list[0];\n        $bcc = str_replace($sendto . \",\", \"\", $to);\n    } else {\n        $sendto = $to;\n    }\n\n    //******************** 配置信息 ********************************\n    $smtpserver = $host; //SMTP服务器,ignore ssl is normal send type\n    $smtpserverport = $port; //SMTP服务器端口 25,994 is ssl send\n    $smtpusermail = $sendfrom; //SMTP服务器的用户邮箱\n    $smtpemailto = $sendto; //发送给谁\n    $smtpuser = $nick; //SMTP服务器的用户帐号，注：部分邮箱只需@前面的用户名\n    $smtppass = $password; //SMTP服务器的授权码\n    $mailtitle = $subject; //邮件主题\n    $mailcontent = $msg; //邮件内容\n    $mailtype = \"HTML\"; //邮件格式（HTML/TXT）,TXT为文本邮件\n\n    //************************ 配置信息 ****************************\n    $smtp = new Smtp($smtpserver, $smtpserverport, true, $smtpuser, $smtppass); //这里面的一个true是表示使用身份验证,否则不使用身份验证.\n    $smtp-&gt;debug = false; //是否显示发送的调试信息\n    $state = $smtp-&gt;sendmail($smtpemailto, $smtpusermail, $mailtitle, $mailcontent, $mailtype, $cc, $bcc);\n\n    if ($state == \"\") {\n        return false;\n    }\n\n    return true;</pre>\n<p>\n	<br />\n</p>', '关于这个主题网络上的答案是千篇一律，并没有真正的解决PHP通过设置SMTP来发送群发邮件只显示个人邮箱的功能。', 'PHP邮件，SMTP设置', 0, 1, 0, NULL, '', '', 1, 1584409943, NULL, 0);
INSERT INTO `article` VALUES (69, 5, '原创', '//cdn.dmake.cn/attachment/images/20210125/16115488735.jpg', '板砖博客和DMAKE建站的来由', NULL, '<p style=\"text-align:center;\">\n	<img src=\"http://cdn.dmake.cn/attachment/images/20210125/16115488735.jpg\" /> \n</p>\n<h4>\n	曾经\n</h4>\n<p>\n	板砖曾经在重庆工商大学任职数据中心技术员、校论坛站长，后来人送外号板砖。\n</p>\n<p>\n	板砖直且刚，在工作和生活中都在努力的充当别人的后援支持，也在努力的变得更强。\n</p>\n<p>\n	后来创业，做了一个地方门户网站，当时移动互联网还未兴起之时，做B2B2C、做资源站、做论坛、做淘宝客、做小说付费站，在微信体系面世起又攻会员体系运营，逐渐的将这些互联网运营思路汇总到1个系统当中，在2017年这个系统成功申请了软件著作权证书。\n</p>\n<p>\n	虽然创业之路已中断，但是创意思维不能断，因此开了这个板砖博客记录以往经验和产品打磨。\n</p>\n<p>\n	DMAKE只是因为正好薅到这个域名，思来想去也正和我意，被重整的系统就命名为DMAKE建站系统。\n</p>\n<h4>\n	现在\n</h4>\n<p>\n	目前系统的功能性推进速度会比较缓慢，因为工作时会引用该系统的各种功能，为了避免纠纷，因此需要做代码脱敏工作。\n</p>\n<p>\n	而运营相关经验的总结会比代码多，因为目前周末也在为企业做运营管理咨询顾问，用通俗易懂的话语去翻译品牌营销、新媒体运营方法、电商在这个阶段的落地，我认为这是很多中小企业更需要关注的。\n</p>\n<p>\n	板砖的分享只是互联网历史足迹中轻微的一点，但是如果能够帮助到他人，我还是很高兴的。\n</p>\n<h4>\n	未来\n</h4>\n<p>\n	IT和运营其实一直都是一回事儿。IT是运营思路的具象化体现，运营是IT的中枢与指挥，双方密不可分。\n</p>\n<p>\n	因为第一学历就是信息管理与信息系统，所以在管理理论和IT方面都具有一些经验。特别是在12年以后接触了大量企业需求和实操后，管理运营等理论已经化为思想中的单元。\n</p>\n<p>\n	如何把IT和运营有机的结合、动态的拓展思路和落地，是板砖博客的下一步计划。\n</p>\n<p>\n	当然，每一步进行过程我都会在此分享。\n</p>\n<h4>\n	顾虑\n</h4>\n<p>\n	DMAKE建站系统其实是和整个行业生态圈绑定的，用到了微信、支付宝、云存储、阿里妈妈等等业务SDK，因为是个人爱好站点维护周期不能得到保证，所以有些功能模块的体现会偶发故障，希望试用者可以自行排查，也欢迎在发现问题的时候告知我。\n</p>', '冥冥之中自有天意，踽踽独行享一波三折，一己之力填新手坑，人到中年议职场是。', '', 0, 1, 0, NULL, '', '', 1, 1611549763, 1611639799, 0);
INSERT INTO `article` VALUES (59, 2, '原创', '//cdn.dmake.cn/attachment/images/20200427/15879648918.png', '做自己的淘小铺！免费版淘宝客小程序源码下载', NULL, '<p style=\"text-align:center;\">\n	<img src=\"http://cdn.dmake.cn/attachment/images/20200427/15879648918.png\" /> \n</p>\n<p style=\"text-align:center;\">\n	淘宝客小程序页面一览\n</p>\n<p style=\"text-align:center;\">\n	<br />\n</p>\n<p style=\"text-align:left;\">\n	今年淘小铺是一个很热门的话题，但是我知道很多人连100块的门票都不相交。所以板砖博客特供免费版淘宝客小程序给有一定开发经验的同学，跟随大势撸一波淘宝客高佣金！\n</p>\n<p style=\"text-align:left;\">\n	当然，淘小铺是一个技术实力雄厚同时也是一个收费入门的平台。如果你觉得自己也想做一个可以免费使用的平台，板砖博客的免费版小程序可能是你的热门之选！\n</p>\n<p style=\"text-align:left;\">\n	小程序简介：\n</p>\n<p style=\"text-align:left;\">\n	<br />\n</p>\n<ul>\n	<li>\n		本小程序通过阿里妈妈接口实时采集最新数据，相关接口都已更新！\n	</li>\n	<li>\n		本小程序可以免费供各位同学自行学习、修改和商用，接口永久免费！\n	</li>\n	<li>\n		本小程序的公开版本持续更新，永久免费！\n	</li>\n</ul>\n<p>\n	<br />\n</p>\n<p style=\"text-align:left;\">\n	小程序版本：BETA2020.4.27\n</p>\n<p style=\"text-align:left;\">\n	使用方法：\n</p>\n<p style=\"text-align:left;\">\n	<br />\n</p>\n<ol>\n	<li>\n		注册任意性质的小程序并通过微信审核；\n	</li>\n	<li>\n		获得阿里妈妈的SDK权限、淘宝联盟PID（不懂得自己百度一下）\n	</li>\n	<li>\n		将源代码中得参数替换成你自己得\n	</li>\n	<li>\n		微信开发者工具中提交代码，审核通过后即可使用\n	</li>\n</ol>\n<p>\n	<br />\n</p>\n<p style=\"text-align:left;\">\n	<br />\n</p>\n<pre class=\"prettyprint lang-js\">// 通用的post接口\nconst _minRequest = (data, callback, api) =&gt; {\n  api = api == undefined ? apiurl : api;\n  if (typeof (data) == \'object\') {\n    data.appid = wx.getAccountInfoSync().miniProgram.appId;\n  }\n\n  /** 如果你自己也有备案的网站和淘宝联盟账号，下面的3个参数填了之后所有生产的数据都是基于你的账号，包含所有收益\n  data.appkey = \'\';\n  data.secret = \'\';\n  data.pid = \'\';\n  */\n\n  wx.request({\n    url: api,\n    data: data,\n    header: {\n      \"content-type\": \"application/x-www-form-urlencoded\"\n    },\n    method: \"POST\",\n    dataType: \"json\",\n    success: function (res) {\n      (callback &amp;&amp; typeof (callback) === \"function\") &amp;&amp; callback(res.data);\n    },\n  });\n}</pre>\n<p>\n	如有使用疑问联系QQ554305954，备注小程序\n</p>\n<p style=\"text-align:left;\">\n	做自己的淘小铺！<a href=\"/attachment/file/tcg.zip\" target=\"_blank\"><span style=\"background-color:#003399;color:#FFFFFF;\">免费版淘宝客小程序源码下载</span></a> \n</p>', '今年淘小铺是一个很热门的话题，但是我知道很多人连100块的门票都不相交。所以板砖博客特供免费版淘宝客小程序给有一定开发经验的同学，跟随大势撸一波淘宝客高佣金！', '淘小铺，淘宝客小程序', 0, 1, 0, NULL, '', '', 1, 1587965441, 1587972987, 0);
INSERT INTO `article` VALUES (60, 2, '原创', '//cdn.dmake.cn/attachment/images/20200827/15985176082.jpg', '运营人的职能范围不是只做策划，落地和促成目的达成很重要', NULL, '<p>\n	故事一：我已经很忙了，这个事儿我干不了！\n</p>\n<p>\n	故事二：领导许诺我的奖励黄了，下一次我还能相信他吗？\n</p>\n<p>\n	故事三：TA才是项目主导，我有资格干预项目吗？\n</p>\n<p>\n	故事四：现阶段我们比对手领先了一大截，我们是这个市场最专业的\n</p>\n<p>\n	故事五：这些工作我做的够好了，卖不出去关我什么事儿？\n</p>\n<p>\n	故事六：团队还没有启动就在找我谈条件，我该怎么办？\n</p>', '近期板砖的同事和客户在积极立项主动运营，但是进度和结果并不明晰，经过一一交流后找到了这几个来自个人和企业的硬伤，下面几个故事您可能似曾相识。', '运营职能', 0, 0, 0, NULL, '', '', 1, 1598517826, NULL, 0);
INSERT INTO `article` VALUES (61, 1, '原创', '//cdn.dmake.cn/attachment/images/20200924/16009402762.jpg', '二三线程序员，如何才能一年赚到20万外包', NULL, '<p>\n	板砖在国内某排名第一的IT控件网站任职平台经理，在这个岗位上不仅释放了我在运营领域的一些灵感，同时也反馈了我不少关于商业的小确幸。\n</p>\n<p>\n	在国内，绝大多数的程序员是接不到外包订单的，因为自己资源窄没名气，或者在某一个领域有名气但是无法承担起复杂的业务环境，诸多原由导致最终结果——心有余而力不足。\n</p>\n<p>\n	<strong>那么</strong><strong>二三线程序员，如何才能一年赚到20万外包？</strong> \n</p>\n<p>\n	从地理上来看，二三线城市大致包括了重庆、成都、长沙、武汉等城市，普遍程序员工资不超过1万基准，注意，程序员和工程师是两码事，程序员更接地气的称呼——码农。\n</p>\n<p>\n	那么在这些城市里接的业务肯定大多数属于1-3万以内的小业务了，那么如果你想转到20万外包，我们定个小目标——每月做1个项目！\n</p>\n<p>\n	那么第二个问题来了：<strong>如果有人不间断的推荐业务，一个月是否足够完成业务？</strong> \n</p>\n<p>\n	我们把业务分为BS、CS架构，目前BS架构中有各种CMS/商城/业务系统开源架构，因此推荐用基于PHP架构的开源系统，TP和Laravel值得推荐，你只需要深入挖掘需求，找一个程序模板，用最快的速度搞前端和定制功能，一个月时间内上线交付基本上是可行的。\n</p>\n<p>\n	<strong>那么问题来了，CS架构的业务该怎么做呢？</strong> \n</p>\n<p>\n	CS架构的业务和BS的最大区别是要实现美观的效果是每月开源产品支持的，如果是一些特别的功能例如报表、甘特图、Grid等，不仅要美观还得要有各种自定义事件，想通过自己开发，可能一年都完成不了一个项目，更何况我们的需求是一个月完成一个啊。\n</p>\n<p>\n	<strong>桌面软件产品的外包，板砖推荐第三方控件</strong> \n</p>\n<p>\n	有的甲方或许对第三方控件的使用成本有意见，那么板砖还是推荐你给甲方讲清楚利害关系，除非是一些简单的录入和读取系统，否则板砖不建议你接这样的项目。\n</p>\n<p>\n	那么有哪些第三方控件是可以提升开发效果和速度呢？请往下看！\n</p>\n<p>\n	UI界面类：DevExpress\\GrapeCity\\（.NET）&nbsp;BCGSoft\\Developer Machines\\<a>Xtreme Toolkit Pro</a>(C++)\n</p>\n<p>\n	文档格式处理类：ASPOSE\\<span id=\"__kindeditor_bookmark_start_28__\">Spire.Office （.NET、C++和Java都可以）</span> \n</p>\n<p>\n	报表处理类：FastReport(.NET)/Stimulsoft(.NET/Java)\n</p>\n<p>\n	工业控制领域：Iocomp/Xgantt(.NET/Activex)\n</p>\n<p>\n	GIS/CAD类：CADSoft/GigaSoft\n</p>\n<p>\n	至于这些厂商软件怎么用，板砖推荐一个好工具给你：百度\n</p>\n<p>\n	当然，我们国产有没有一些优秀的控件可以使用呐？cskin是我认为相当不错的一个控件了，虽然功能上比不上上面的那些厂商有优势，但是对于预算不足的甲方来说已经是很不错的选择了，当然这款控件免费支持45种优化控件。\n</p>\n<p>\n	<strong>最后，话题回到</strong><strong>二三线程序员如何才能一年赚到20万外包这个问题上来，其实大多外包业务在技术上是不存在问题的，最大的问题是——如何一个月能够接到一个订单！</strong>\n</p>', '今天看到一个标题《二三线站长，如何才能一年赚到20万》特别有感触，因为我不是一个合格的站长，也不是一个合格的程序员，只能算一个标准的平台经理和半搭子程序员，那么二三线程序员如何才能一年赚到20万外包呢，让我给你说点干货。', '', 0, 1, 0, NULL, '', '', 1, 1600940361, 1600997159, 0);
INSERT INTO `article` VALUES (62, 4, '原创', '//cdn.dmake.cn/attachment/images/20200929/16013435363.png', '微软决定用78块钱干掉中国一般低端码农', NULL, '<p style=\"text-align:center;\">\n	<img src=\"http://cdn.dmake.cn/attachment/images/20200929/16013435363.png\" /> \n</p>\n<p style=\"text-align:left;\">\n	构建一个这样的跨平台APP需要多少钱？\n</p>\n<p style=\"text-align:left;\">\n	在中国，大多数企业选择寻找项目外包或者购买第三方SAAS应用，甚至亲自招聘程序猿来开发，从人力成本到应用部署成本，都是一笔糊涂账。\n</p>\n<p style=\"text-align:left;\">\n	比如购买SAAS应用，分为各种版本逐步加价，如果是聘请程序猿来开发，除了工资还得有一笔运维投入，幸运点一年需要几千块，不幸一点一年十万也是可以消费的。\n</p>\n<p style=\"text-align:left;\">\n	然而，Power Apps打破了信息不对称的平衡，仅需78元/年即可解决上述问题。\n</p>\n<p style=\"text-align:left;\">\n	<strong>实现业务创新</strong><br />\n使用 Power Apps，可创建大量应用方案，将数字转型注入手动流程和过时流程。使用画布和模型驱动的应用构建 Power Apps，在检查、现场销售支持、“从目标客户到现金”和整合式营销视图等任务和角色特定方案中解决业务问题。\n</p>\n<p style=\"text-align:center;\">\n	<img src=\"http://cdn.dmake.cn/attachment/keditor/image/20200929/20200929094503_85581.png\" alt=\"\" /> \n</p>\n<p style=\"text-align:center;\">\n	<br />\n</p>\n<div style=\"text-align:left;\">\n	<strong>构建高度定制的应用程序</strong> \n</div>\n<p style=\"text-align:left;\">\n	首先在 Power Apps 画布上设计用户体验。自定义应用的每个细节，针对特定任务和角色进行优化。为任何使用多种控件（包括相机和位置）的设备创建应用，或者从一个展示常见业务方案（如费用报告或现场检查）的示例应用开始。\n</p>\n<p style=\"text-align:center;\">\n	<img src=\"http://cdn.dmake.cn/attachment/keditor/image/20200929/20200929094612_25508.png\" alt=\"\" /> \n</p>\n<p style=\"text-align:left;\">\n	<br />\n</p>\n<p>\n	<strong></strong><strong>根据数据创建复杂的应用</strong>\n</p>\n<p>\n	从数据模型和业务流程开始，自动生成能够在任何设备上运行的沉浸式、响应式应用程序。使用简单的拖放设计器自定义业务实体，并根据特定角色定制用户体验。\n</p>\n<p>\n	<img src=\"http://cdn.dmake.cn/attachment/keditor/image/20200929/20200929094715_64564.png\" alt=\"\" /> \n</p>\n<p>\n	<br />\n</p>\n<p>\n	<br />\n</p>\n<p>\n	上面所述的这一切在普通码农那里又是一个大工程，然而在Power Apps这里只是一个小操作，仅需每年78元人民币就能解决！难道这是危言耸听吗？不，它已经来临！\n</p>\n<p>\n	<br />\n</p>\n<p>\n	<strong>为 Power Apps 设计逻辑</strong><br />\n使用 Power Automate 为 Power Apps 设计逻辑。无需编写代码，只需使用点击流设计器即可构建业务逻辑。使用按钮、操作和数据输入在应用中轻松运行流。这些流可以即发即弃，也可将数据返回到应用以向用户显示信息。\n</p>\n<p>\n	<img src=\"http://cdn.dmake.cn/attachment/keditor/image/20200929/20200929095340_43928.png\" alt=\"\" />\n</p>\n<p>\n	<strong>在整个流程中为应用用户提供指导</strong><br />\n帮助用户保持进度，并确保多阶段业务流程中任何位置输入的数据均保持一致。例如，可以创建一个流，让每个人都以相同的方式处理客户服务请求，或者要求在提交订单之前获得批准。\n</p>\n<p>\n	<img src=\"http://cdn.dmake.cn/attachment/keditor/image/20200929/20200929095353_40602.png\" alt=\"\" />\n</p>\n<p>\n	<strong>跨 200 多个已连接的源实现自动化</strong><br />\n连接到数据（无论它在哪里）以创建自动化工作流，实现业务协作并提高业务效率。在整个团队中无缝共享此任务自动化。与 Excle、SharePoint、OneDrive for Business 和 Dynamics 365 深度集成，直接在每天使用的应用程序上下文中实现自动化。\n</p>\n<p>\n	<img src=\"http://cdn.dmake.cn/attachment/keditor/image/20200929/20200929095406_71780.png\" alt=\"\" />\n</p>\n<p>\n	<strong>扩展业务流程</strong><br />\n使用类似 Excel 的简单表达式语言创建更强大的流。利用面向专业开发人员的内置扩展性连接到更多系统并增强控制。要进行全面管理，请从自动化工作流创建 Azure 逻辑应用，以便在 Azure 中进行管理（当然，这一步也仅需要有一定电脑基础的人员即可完成）。\n</p>\n<p>\n	<img src=\"http://cdn.dmake.cn/attachment/keditor/image/20200929/20200929095419_85051.png\" alt=\"\" />\n</p>\n<p>\n	<span>庆幸的是Power Apps在国内还未推广开来，还有一定时间留给没有准备转型的码农们。</span>那么未来在研发能力上偏弱的码农该如何生存呢？\n</p>\n<p>\n	是生存还是毁灭，这不会是一个问题，几遍Power Apps已经到来，作为有一定开发基础的码农可以转型为企业做自定义程序管理，前提条件下就是你得对公司业务有深入了解，方可做出符合业务流程的App等应用。\n</p>\n<p>\n	板砖一直认为coder们最大的优势在于学习，不断地学习各种业务流程，也不断的学习各种业务在应用上的表达方式，这或许是coder们立于不败之地的最后一根稻草吧。\n</p>', '在微软产品的生态链中出现了一位新秀——Power Apps。使用 Power Apps 构建应用，使用不需要代码的直观可视化工具解决业务问题，使用支持数据集成和分发的平台提高工作速度，使用专业开发人员的构建基块扩展平台。在任何设备上轻松构建和共享。', '', 0, 1, 0, NULL, '', '', 1, 1601343818, 1601344814, 0);
INSERT INTO `article` VALUES (63, 4, '原创', '//cdn.dmake.cn/attachment/images/20201010/16022938456.jpeg', '看完这篇，DBA从入门到删库跑路还是平步青云就好选了', NULL, '<p>\n	据了解，绝大多数中小IT企业是没有专职DBA这个岗位的，因为程序猿大多数是具备数据库安装、备份等基本维护能力的，但是DBA这个岗位绝对不仅限于字面上的管理数据库这么简单。\n</p>\n<p>\n	一般情况下在具备多种业务结构、平台和海量数据量和数据交互的企业中才具备专职DBA，例如日活上百万的APP、应用，或者专门做机房运营的IDC们，因此用万中挑一来形容DBA也是比较贴切的。\n</p>\n<p style=\"text-align:center;\">\n	<img src=\"http://cdn.dmake.cn/attachment/keditor/image/20201010/20201010094540_75910.png\" alt=\"\" /> \n</p>\n<p style=\"text-align:left;\">\n	从职友集网站收集的数据中心可以看到，DBA平均工资 ￥17.8K，其中拿 20K-30K 工资的占比最多，达 30.5%，dba工资按学历统计，中专工资￥7.7K，大专工资￥14.8K，本科工资￥18.2K。忽略统计方法中存在的误差，DBA绝对是IT界超越同等年限工程师收入水平。\n</p>\n<p style=\"text-align:left;\">\n	别看DBA工资高，但是职能和技能上的要求也不会低。简单来说，你得是一个程序猿，然后才是一个DBA：如何写程序不需要太专业，但是如何从数据库方面优化程序运行效率必须很专业。\n</p>\n<p style=\"text-align:left;\">\n	<br />\n</p>\n<p>\n	<strong>DBA的职能</strong> \n</p>\n<p>\n	产品的整个生命周期里数据库管理员的职责重要而广泛，这催生了各个纵向的运维技术方向，凡是关系到数据库质量、效率、成本、安全等方面的工作，及涉及到的技术、组件，主要包括：\n</p>\n<ul>\n	<li>\n		数据库监控技术：包括监控平台的研发、应用，服务监控准确性、实时性、全面性的保障。\n	</li>\n	<li>\n		数据库故障管理：包括服务的故障预案设计，预案的自动化执行，故障的总结并反馈到产品/系统的设计层面进行优化以提高产品的稳定性。\n	</li>\n	<li>\n		数据库容量管理：测量服务的容量，规划服务的机房建设，扩容、迁移等工作。\n	</li>\n	<li>\n		数据库性能优化：从各个方向，包括SQL优化、参数优化、应用优化、客户端优化等，提高数据库的性能和响应速度，改善用户体验。\n	</li>\n	<li>\n		数据库安全保障：包括数据库的访问安全、防攻击、权限控制等。\n	</li>\n	<li>\n		数据库自动部署：部署平台/工具的研发，及平台/工具的使用，做到安全、高效的发布服务。\n	</li>\n	<li>\n		数据库集群管理：包括数据库的服务器管理、分布式集群管理等。\n	</li>\n	<li>\n		数据库模型设计：包括数据库逻辑和物理模型的设计，如何实现性能最优，架构可扩展，服务可运维等。\n	</li>\n</ul>\n<p>\n	<br />\n</p>\n<p style=\"text-align:center;\">\n	<img src=\"http://cdn.dmake.cn/attachment/images/20201010/16022938456.jpeg\" /> \n</p>\n<p>\n	<strong>DBA的基本能力</strong> \n</p>\n<p>\n	数据库管理员以技术为基础，通过技术保障数据库提供更高质量的服务。在这里板砖列举9大DBA基本能力，足以应对庞大的业务工作。<br />\n1.理解数据备份/恢复与灾难恢复\n</p>\n<blockquote>\n	<p>\n		恢复已损坏的数据库是每一个DBA应掌握的最重要的技能。DBA需要完全理解数据库所有可能的备份与恢复方法，以及不同备份方法与不同恢复策略的对应关系。最重要的并不是每一条数据都具有价值，因此和业务部门的沟通也必不可少。\n	</p>\n</blockquote>\n<p>\n	2.工具集的使用\n</p>\n<blockquote>\n	<p>\n		工具集指是的用于执行不同DBA任务的脚本。这个工具集应该包含不同的小代码片段，它们可以快速诊断问题或执行一个特定的任务。这些工具脚本应该按DBA的活动类型归类，如备份、索引维护、性能优化、容量管理等，一些优秀的DBA甚至会通过脚本实现每日、周、月固定工作的自动化。\n	</p>\n</blockquote>\n<p>\n	3.知道如何快速寻找答案\n</p>\n<blockquote>\n	<p>\n		<span>数据库问题造成的影响总是远远大于应用程序的。</span>数据库的故障从硬件到网络，从性能压力到程序bug，DBA都要从容应对，一一排除。即使是数据库大牛，也不可能是无所不知的，因此每个DBA一方面需要不断学习，积累操作系统、网络、硬件、存储系统、分布式计算等理论基础，另一方面还要有快速寻找新问题解决方法的能力。\n	</p>\n</blockquote>\n<p>\n	4.知道如何监控和优化数据库性能\n</p>\n<blockquote>\n	<p>\n		对于任何数据库产品，性能都尤其重要，它会直接影响产品的响应速度和用户体验。对于一个DBA来说，性能优化一般需要占用50%的工作时间，因此DBA需要知道如何监控和优化数据性能。因此使用数据库自带的DBMS或者第三方工具尤为重要。\n	</p>\n</blockquote>\n<p>\n	5.研究新版本\n</p>\n<blockquote>\n	<p>\n		数据库版本更新频率不慢，并且特性还贼多，DBA应该紧跟新版本所作的修改，测试版开放后马上下载和安装，尽快掌握第一手使用经验，然后提出一些合理的新建议，帮助组织更好地利用新版本数据库。\n	</p>\n</blockquote>\n<p>\n	6.理解代码最佳实践方法\n</p>\n<blockquote>\n	<p>\n		DBA应该了解如何编写高效的代码。一名好的DBA要能够理解和识别这些糟糕的编码实践方法，知道如何修改这些烂代码，让它们变成高效代码。此外，他们还要记录下写代码的最佳实践方法，并且将这些实践方法分享给其他人。\n	</p>\n</blockquote>\n<p>\n	7.数据库安全性\n</p>\n<blockquote>\n	<p>\n		安全性是一个热门话题。DBA应该完全掌握如何实现数据库的安全访问。他们应该理解操作系统身份验证和数据库身份验证的区别，以及它们各自的使用场合。\n	</p>\n</blockquote>\n<p>\n	8.数据库设计\n</p>\n<blockquote>\n	<p>\n		决定数据库性能的一个关键问题是数据库设计，普通程序猿设计的数据库表或许满足了一般使用，但是对高性能运行还是存在缺漏的。DBA需要理解为什么使用正确的索引、外键约束、主键、检查约束和使用数据类型能够保持数据库的数据完整性和实现高效的数据查询与更新。\n	</p>\n</blockquote>\n<p>\n	9.容量监控与规划\n</p>\n<blockquote>\n	<p>\n		数据库往往要使用大量的资源，包括CPU、内存、I/O及磁盘空间。DBA应该理解如何监控数据库所需要的不同主机资源的用量。他们应该能够理解这些资源在不同时间的使用情况，以及利用历史使用数据来规划未来的容量需求。在监控过程中，DBA应该能够预见到容量规划会在将来什么时候出现问题，然后采取必要的措施保持数据库不会因为容量限制而出现中断。\n	</p>\n</blockquote>\n<p>\n	<strong>DBA的入门工具</strong> \n</p>\n<p>\n	从DBA的8大职能和9大能力要求出发，很多1-3年的从业者还未完全掌握这些，因此除了突发性状况的应对经验以外，板砖为大家准备了2套标准的DB运管工具。为什么板砖不像其他博主一次性介绍TOP10工具呢？用一两套软件就能集中管理多种数据库，灵活应变多种管理要求难道不香吗？\n</p>\n<p style=\"text-align:center;\">\n	<img src=\"http://cdn.dmake.cn/attachment/keditor/image/20201010/20201010101820_52171.png\" alt=\"\" /> \n</p>\n<p>\n	<strong>Navicat全家桶 或许已经有不少DBA已经用上了Navicat其操作简便中文交互体验度很好，</strong>推荐Navicat Premium（覆盖所有数据库开发和管理工作）、Navicat Monitor（安全简单而且无代理的远程服务器监控工具，受监控的服务器包括 MySQL、MariaDB 和 SQL Server）、Navicat Data Modeler（功能强大、性价比高的数据库设计工具，可帮助你创建高质量的概念、逻辑和物理数据模型。它支持各种数据库系统，包括MySQL、MariaDB、Oracle、SQL Server、PostgreSQL 和 SQLite）。\n</p>\n<p style=\"text-align:center;\">\n	<img src=\"http://cdn.dmake.cn/attachment/keditor/image/20201010/20201010102011_86826.jpg\" alt=\"\" width=\"400\" height=\"267\" title=\"\" align=\"\" /> \n</p>\n<p>\n	<strong>Devart工具包（dbForge）同样作为全球大企业是用的最多的数据库管理工具包，它的特色体现的更为灵活</strong>（每种数据库都有管理工具<a href=\"https://blog.csdn.net/momo7777777/article/details/99566209\" target=\"_blank\">点击这里进入传送门</a>）\n</p>\n<p style=\"text-align:center;\">\n	<img src=\"https://image.evget.com/images/Red%20Gate/sql-dba-bundle210.png\" />\n</p>\n<p>\n	<p>\n		<strong>SQL DBA Bundle运管工具包&nbsp;</strong>SQL DBA Bundle可以帮您：集成备份并节省空间、压缩、验证、SQLServer备份加密、适用于脚本备份的静音压缩技术、SQL Sever监控和报警、实时性能监控和报警、基于网络的用户友好界面、智能灾难恢复与OLR、可在现场直接从备份中快速加载完整功能的数据库、无需使用物理手段进行还原、快速节省预算、减少所使用的磁盘空间、加快还原速度<a href=\"https://www.evget.com/product/3065\" target=\"_blank\">（传送门）</a>。\n	</p>\n</p>\n<p>\n	<strong><br />\n</strong> \n</p>\n<p>\n	<b>要说Devart和Navicat哪一个更适合DBA长久使用，推荐大家看这一篇<a href=\"https://blog.csdn.net/AABBbaby/article/details/96841899\" target=\"_blank\">《Devart和Navicat对比解析》</a>。</b> \n</p>\n<p>\n	<b><br />\n</b> \n</p>\n<p>\n	<b>当然，作为DBA有权选择更多的小而美的管理工具（板砖推荐的3大运管工具包表示我们可以打10个），欢迎有需求的管理员们参考：</b>\n</p>\n<p>\n	NO1: PD（PowerDesigner）<br />\n功能：SysBase公司提供的数据库设计工具，功能很强大，是做数据库设计时必备的工具；\n</p>\n<p>\n	NO2: Log Explorer<br />\n功能：数据库日志读取工具，主要用来恢复误操作的数据（目前只支持到2005版本），详见：<br />\nNO3：Tuning Advisor<br />\n功能：优化顾问，会根据数据库的运行情况，提示您做相关的优化（可靠性不是太高，需要自行判断）;<br />\nNO4：SSMSTools<br />\n功能：SSMS工具的一个插件，能提供格式化代码、追溯历史等功能（通过它，也许你可以开发自己的插件）；<br />\nNO5: DBDiff &amp; TableDiff<br />\n功能：第一个是比较两个数据库结构的差异，第二个可用来比较表中数据的差异（而且能生成相关的脚本）；<br />\nNO6：PAL Tool<br />\n功能：Performance Analysis of Logs，Perfmon日志分析工具；<br />\nNO7：RML<br />\n功能：它的四个主要功能组件；ReadTrace工具能读取数据库的Profile跟踪文件，并生成报告；Ostress能将ReadTrace生成的文件重播，而且还可以对数据库做压力测试；ORCA能保证重报时，按照事件发生的顺序播放；Reporter能将ReadTrace后的内容通过报表的形式展现，相当的有用。\n</p>\n<p>\n	NO8：SqlNexus<br />\n功能：先通过SQLServer自带的SQLdiag.exe工具收集信息，然后再用SqlNexus分析这些信息，它是前面一些工具的整合，为数据库管理人员寻找SQLServer服务器的性能瓶颈和排查故障提供了相当强大的支持；<br />\nNO9：SQLIO &amp; SQLIOSim<br />\n功能：磁盘IO压力测试工具，SQLIO主要是模拟随机或者顺序的方式来测试磁盘IO的性能；SQLIOSim是模拟SQLServer的行为来测试IO性能；<br />\nNO10:SqlMonitor &amp; SSBDiagnose<br />\n功能：SqlMonitor是监控Replication和Mirror的必会工具，SSBDiagnose是测试SSB配置的工具；\n</p>\n<p>\n	<br />\n</p>\n<p>\n</p>', 'DBA（数据库管理员）是企业关键业务应用中非常重要的角色， 数据库管理系统 (DBMS)管理和维护的守护神，位置举足轻重。然而在企业招聘中极少出现相关岗位，更多时候出现在删库跑路的段子里面。', '删库跑路', 0, 1, 0, NULL, '', '', 1, 1602293951, 1602297362, 0);
INSERT INTO `article` VALUES (64, 1, '原创', 'http://cdn.dmake.cn/attachment/images/20210129/16118860119.jpg', 'Java开发控件合集，利用第三方控件实现敏捷开发', NULL, '<p>\n	<strong><img src=\"http://cdn.dmake.cn/attachment/images/20210129/16118860119.jpg\" /><br />\n</strong>\n</p>\n<p>\n	<strong></strong><strong>文档转换与管理</strong> 将office和html文档来回转化\n</p>\n1. <span style=\"color:#337FE5;\">Aspose系列控件</span>，包含Word文档转出各种格式文件 、Cells控件对表格文件、代码精确管理 、 PDF图文生成、注释、加解密和格式转化等&nbsp; ··· Aspose系列控件是国外知名产品，授权多样化，具有在线demo和源码支持。<br />\n2. <span style=\"color:#337FE5;\">Spire系列控件</span>，包含Spire.Cloud是打造线上办公Office Online的接口 、 Spire.Doc支持各种文件转换成Word，以及编辑和标注&nbsp; ，Spire.PDF 、Spire.XLS 同样具备Aspose相关产品的功能&nbsp; ···Spire系列控件是国内冰蓝公司开发的，授权也很灵活，全中文支持，体验度稍好一些。<br />\n3. <span style=\"color:#337FE5;\">PDFlib </span>如其名，在Web服务器上围绕PDF的生成提供了专业化的服务，甚至支持CAD文件，适用于海量印刷打印业务。<br />\n4.<span style=\"color:#337FE5;\"> iText </span>和上述控件相比它的功能就要简约的多，适合数据库信息生成PDF文件，阅读和格式转化功能都没有。<br />\n5. <span style=\"color:#337FE5;\">WebOffice</span> 如其名，国内领先的在线编辑Office文档软件，支持各类型浏览器。是国家机关、保密单位等以及远程办公开发控件，跨平台支持近乎所有office文档在浏览器中处理。<br />\n<br />\n<strong>图表报表</strong> 数据绚丽多彩的表达方式<br />\n1. <span style=\"color:#337FE5;\">Stimulsoft Ultimate </span>用于创建报表和仪表板的通用工具集，支持多种报表导出格式，支持10多种数据源直连，这是一个终极包，可以根据需求分别使用Java、.NET、PHP等版本，特别是在Dashbord方面特别优秀。 <br />\n2. <span style=\"color:#337FE5;\">LightningChart JS</span> 这一款控件主要是引用在Java Web前端的，特别适合科研和金融工作，有丰富的图表表现形式，它还有一个免费开源版本的表兄弟“Echarts”<br />\n3. <span style=\"color:#337FE5;\">ChartDirector</span> 一款使用方便快捷、功能强大且交互性强的通用Web图表组件，小巧而精悍的它不仅能够输出普通柱状图饼状图等传统图表，还含有电子仪表盘和金融图标库，以及跨平台属性，适用范围大大增加。<br />\n4. <span style=\"color:#337FE5;\">TeeChart </span>适用于所有主流Java编程环境的TeeChart图表库，除了功能覆盖上述所有图表外，2D、3D效果也很瞩目，还提供了一些地图功能。<br />\n5. <span style=\"color:#337FE5;\">AnyChart </span>基于web前端的图表套件，可用于仪表盘、报表、数据分析、统计学、金融等领域。<br />\n<br />\n<strong>CAD</strong><strong> </strong>桌面和WEB端的CAD编辑/查看利器<br />\n1. <span style=\"color:#337FE5;\">CAD EditorX </span>提供易用的API和大量的例子方便开发者快速集成。将CAD功能添加到网页或正在开发的应用程序中。同时可以查看、编辑、转换、打印和测量DWG、DXF、SVG、HPGL、PDF、STEP、IGES、STL和其他CAD文件。<br />\n2. <span style=\"color:#337FE5;\">Aspose.CAD </span>开发的桌面软件不需要安装AutoCAD和任何其他软件，即可将AutoCAD DWG和DXF文件转换成高品质的PDF和光栅图像！<br />\n3. <span style=\"color:#337FE5;\">DXF Export Java </span>便于开发者创建特色的AutoCAD DXF文件库。支持多种元素和接口功能。<br />\n<br />\n<strong>条形码/甘特图/电子仪表盘等工业领域控件</strong><br />\n1.&nbsp;<span style=\"color:#337FE5;\">Aspose.BarCode </span>API开发高性能应用程序来创建和识别1D，2D和邮政条形码。还有.NET、C++等多种版本。<br />\n2.&nbsp;<span style=\"color:#337FE5;\">Barcode Xpress </span>用于检测、读写 1D 和 2D 的条形码，支持文档捕捉、索引、存档以及自动处理的应用程序。包含了 .NET 和 ActiveX 的工具包, 它可以在页面的任何位置查找条形码，解码条形码，报告内容，报告识别值以及生成条形码。<br />\n3.&nbsp;<span style=\"color:#337FE5;\">QRCode decoder SDK </span>高效率和快速搜索, 检测, 定位QRCode 条码的图像扫描开发工具包。QRCode条形码可以在一个小图像上储存大量的数据，最大可达到7089位数字，4296个字母，2953二进制数据。<br />\n4.<span style=\"color:#337FE5;\">&nbsp;VARCHART JGantt </span>是纯Java编写的控件，它可以让你轻松地整合甘特图表到你的应用程序中。Gantt图表显示事件、活动及分配给它们的时间。Gantt图表普遍应用于项目规划、生产控制和规划安排生产订单、物流和资源。<br />\n5.&nbsp;<span style=\"color:#337FE5;\">Visualization and HMI Toolkit </span>小到电器仪表，大到飞机仪表盘，都能做。同一公司还有GLG Widgets 工具集可以自定义开发其他图表。<br />\n6.&nbsp;<span style=\"color:#337FE5;\">FlexGantt </span>老牌甘特图控件，小巧但是功能没有上述3个全面。<br />\n7.&nbsp;<span style=\"color:#337FE5;\">AnyChart </span>基于web前端的图表套件，可用于仪表盘、报表、数据分析、统计学、金融等领域。<br />\n<br />\n<strong>开发工具/测试/封装/加密混淆</strong> Java开发必会的工具集<br />\n1.&nbsp;Java的开发工具大家都不陌生了，性价比最高的Myeclipse、免费的Eclipse、高颜值的IDEA基本占据了国内Java开发Top3，其他工具板砖也是不准备推荐的，接下来板砖主要讲一讲测试、封装打包、加密混淆这些工具。<br />\n2.<span style=\"color:#337FE5;\">&nbsp;DashO Pro</span> 是第三代的Java混淆器（obfuscator）、压缩机（compactor）、优化工具和水印工具（watermarker），有效防止Java逆向工程。<br />\n3.&nbsp;<span style=\"color:#337FE5;\">install4j </span>业界一致肯定的、最佳多平台Java安装文件生成工具，这个工具可以让您的应用程序在安装过程中炫酷多彩。<br />\n4.&nbsp;<span style=\"color:#337FE5;\">Parasoft Jtest </span>专业针对Java语言的开发测试方案，提升Java软件质量，有全汉化界面、支持用户自定义测试流程、遵守多项国际标准。<br />\n5.<span style=\"color:#337FE5;\">&nbsp;JProfiler</span> 和install4j同门，和parasoft相比更简单易上手，成本要低一点。<br />\n<br />\n<strong>网络通讯</strong> 通讯安全和自定义的便捷之选<br />\n1.&nbsp;<span style=\"color:#337FE5;\">Red Carpet Subscriptions</span> 适用任何主流Internet协议的通讯组件、SSL 和 SSH 安全组件、S/MIME加密组件、数字证书（Digital Certificates）组件、信用卡交易处理（Credit Card Processing）组件、ZIP压缩组件、即时消息（Instant Messaging）组件、甚至电子商务（EDI）交易组件，其中每个组件都可以独立使用和购买。<br />\n2.<span style=\"color:#337FE5;\">&nbsp;Aspose.Email </span>打造自己的Email服务器用它就够了。<br />\n<br />\n<strong>GIS地图</strong> 桌面软件地图渲染（2D/3D）<br />\n1. <span style=\"color:#337FE5;\">Highmaps </span>一个基于web的项目创建地图HTML5地图组件，当然你也可以使用百度、高德、腾讯等组件完成地图开发。<br />\n2. <span style=\"color:#337FE5;\">GIS Map Server</span> 是一款地图服务器控件，它将动态地图功能添加到了GLG工具包中并能与工具包一起使用或独立使用，带2D和3D效果。<br />\n3.&nbsp;<span style=\"color:#337FE5;\">GeoModeller </span>一款三维地质建模工具软件，可用于构建复杂、稳态的三维地质模型，支持3D文件和地理模型建模、演示、反转等。<br />\n<br />\n<strong>流程图 </strong> 企业级工作流解决方案的代表作<br />\n<span style=\"color:#337FE5;\">MindFusion.Diagramming for Java </span>桌面自定义流程图开发控件，用于设计自己的流程图工具。<br />\n<br />\n结语：在桌面应用和服务器开发应用方面.NET有很大的优势，但是Java开发也有不少的优秀第三方控件，借助这些控件提升我们的软件效果是一个高效率的方法。<br />', 'Windows开发中.NET具备特别多的开发控件以扶持软件实现敏捷开发，Java开发能用的第三方控件少之又少，今天板砖给大家推荐Java系列开发控件，满足你的各类开发需求。', '', 0, 1, 1, NULL, '', '', 1, 1603092743, 1611886025, 0);
INSERT INTO `article` VALUES (65, 2, '原创', '//cdn.dmake.cn/attachment/images/20200119/15794117754.jpg', '告别2020的运营管理心得分享，写给还不知道如何带团队的同行', NULL, '<p>\n	2018年的时候因为一个冲动，告别了自己组建的创业团队，来到这一家国内还算知名的平台从事网络运营部门的管理岗位。\n</p>\n<p>\n	因为以前是面向零售行业，所以行为性格方面都是很粗狂而开放的，这也给我在圈子里面带来一些良好的口碑。然而到这家公司来以后面向结果的行为方式显然行不通，80%的管理都是女性，感性和理性显然是对立的。因此在来到这个平台的第一年得罪了很多人，第二年为了业务需求主持了平台的全面升级，过得也是相安无事。\n</p>\n<p style=\"text-align:center;\">\n	<img src=\"http://cdn.dmake.cn/attachment/images/20200119/15794117754.jpg\" /> \n</p>\n<h4>\n	2020年的起步对任何人来说都是残酷的，这时候我不由得思考，我带团队的目的是什么？\n</h4>\n<p>\n	通过几个下属的反馈以及我的个人观察，虽然运营部门应该是一个互联网企业里举足轻重的部门，然而在这家公司只是一个编辑部，不，不能在这样下去了！\n</p>\n<p>\n	2020年我带团队的目的只有一个——让部门走上一个正常正规的轨道。\n</p>\n<h4>\n	为此我制定了3个计划：\n</h4>\n<p>\n	<br />\n</p>\n<ol>\n	<li>\n		全员回炉培训，文案编辑、渠道运营、用户运营和客户运营\n	</li>\n	<li>\n		通过运营手段，将平台在网络市场中再向上推动一个曝光等级\n	</li>\n	<li>\n		承担攻坚职责，每个运营都会和一个项目组协作，我要求每个运营除执行外争取成为项目领导者\n	</li>\n</ol>\n<p>\n	<br />\n</p>\n<h4>\n	为了实现这3个计划，培训是基础手段，如果培训者都不懂这写内容如何正确的培训和带领下属进步呢？\n</h4>\n<p>\n	在国内疫情解封之后，我迅速的找到了创业期间曾经合作过的优质企业老总，逐一拜访表示感谢，同时表示在今年的市场运营中我愿意和他们一起克服困难，相互学习。最终我和前投资人达成了运营顾问的合作关系。\n</p>\n<p>\n	在履行运营顾问的过程中，接触到了企业运作中面向客户、面向供应商、面向经营的多种应用场景，这都是目前正值公司都不能提供的，结合一些行业工具书及大V经验分享，逐一分解培训内容，并在顾问过程中实践检验，得到正向反馈之后应用到我的团队中。\n</p>\n<h4>\n	学而不思则罔，思而不学则殆\n</h4>\n<p>\n	在我的团队中有一半是5年老员工，一开始对我的团队升级目标是很反感的，增加工作量不说也得不到公司认可，得罪人不说也没有奖励支持。作为团队领导者，行政力量作为最后的推动力——全员必须启动执行。\n</p>\n<p>\n	归结到在执行前对平台系统做了多点数据监控，减少了下属对每日工作巨量的统计，启动培训和执行后大家积极的一面也开始展现：每周的培训从我一人主持变为“轮值主席”，每周对至少1个运营现象做1次培训和全员分享；一改以工作量论业绩，以质量为中心开展一切工作；用专业的运营思路和工具审视项目组决定，回绝一些拍屁想出来的决定（这一点真的很难啊）；每个人都从工作中感受到“意义”，获得成就感和价值感。\n</p>\n<p>\n	最终，2020年在业务线砍掉超过50%的情况下，同比去年任务完成度高大85%，当然也不排除老板的战略部署和各产线团队的贡献。更让我们部门值得肯定的是一年内覆盖行业4000个业务场景在搜索引擎中的曝光（SEO），网站日均流量也上涨30%以上。客观的数据是对全员培训结果的肯定，也是我在带团队专业化的反馈。\n</p>\n<h4>\n	作为一个管理者，可以作为直接参与者贡献业绩，但是更应该让团队成长，我认为这才是管理的价值\n</h4>\n<p>\n	2020年3次向老板请缨让我们部门在项目组中有更多的领导席位，3次被果断拒绝。但是这也并不阻碍我向部门同事宣导“争取和拼搏”的工作价值观。收入那是老板的，收获才是个人的。如果我没有运营顾问的经历我也不可能有相关经验，如果团队没有领导团队的经验那也就无法实现个人意识升级。\n</p>\n<p>\n	2020年对工作虽留有遗憾，但是对个人决定绝不后悔。\n</p>\n<p>\n	文末，也想吐个槽：在创业公司、战斗团队中，兵熊熊一个，将熊熊一窝，管理岗位的看官你认为呢？\n</p>\n<p>\n	<br />\n</p>', '我在运营团队管理岗位上已经有3年时间了，这段时间经历过从执行者到管理者的无序，也经历过管理却无权的烦恼，今年摒弃一切阻碍只为团队变得更好。', '', 0, 1, 0, NULL, '', '', 1, 1609380871, 1609380889, 0);
INSERT INTO `article` VALUES (66, 5, '原创', '//cdn.dmake.cn/attachment/images/20210111/16103427004.jpg', '把事情搞砸，让领导背锅', NULL, '<p>\n	周日陪同老小去图书馆还书，车子暂停党校，来回时间差不多也就10分钟左右。\n</p>\n<p>\n	不过这次有点区别的是，车停好了出校门的时候被一个大约50多的老妇人拦住了，说今天不准停，只准今天在此考试的人停车。我就纳闷儿了，此地长期是允许临时停靠的，今天哪怕有人考试，他们能停社会人就不能停？反驳说“我在此处停一下，过马路还了书立马回来” 。老妇人在门口骂骂咧咧“我们领导让不准停就不准停，哪怕唐老板来了都不准，你们这些个XXX”让人很是不爽，于是回“你这个事儿过分了，在法律范围内我该投诉你，只要是违反规定了一路撸到底”。\n</p>\n<p>\n	等还完书回来，大约10分钟不到，一个干部样子的中年人来阻拦离场，并且要求去办公室做检讨，说我威胁领导，这谁惯得了，现场又是一地鸡毛。最终是以和副主任的一句话告终<strong>“我们不允许自己的员工收到威胁侮辱，但是有没有考虑过员工反馈的内容并非实情”</strong>。\n</p>\n<p style=\"text-align:center;\">\n	<img src=\"http://cdn.dmake.cn/attachment/images/20210111/16103427004.jpg\" />\n</p>\n<p>\n	<strong>谁说了假话？</strong>\n</p>\n<p>\n	在管理岗位上，我们是员工的靠山，员工也是我们的门面。在一般情况下管理者会对下属维护团体形象而感到高兴还要给他嘉奖。殊不知有一些矛盾的出现是完全无意义的。\n</p>\n<p>\n	社会上一些新闻“坑爹”“坑老公”“坑儿子”“坑领导”等段子并非空穴来风，自己遇到事儿的情况下不是第一时间想着如何妥善处理，比如允许、拒绝、不收等动作，在自己一顿骚操作之下让另一方受体通过媒体、群众放大了事情，最终一地鸡毛，受伤的还是对此事根本“不明所以”的TA。\n</p>\n<p>\n	因此，身在管理岗位的你如果遇到了下属的委屈反馈，请先停一下，别忙着出头，避免“背锅”。\n</p>\n<p>\n	<strong>法治进程下，不需要认识谁谁谁！</strong>\n</p>\n<p>\n	在很长的一段时间里，我们群众办事说认识谁谁谁可以当做办事儿的敲门砖，可以少受一些白眼。但是在现在的法治进程下群众办事不需要认识谁谁谁，在中共中央印发《法治社会建设实施纲要(2020-2025年)》中，让人最感同身受的是法律为我们界定了不能做的边界，也为我们的可以做的行为提供了保障。\n</p>\n<p>\n	法制深入人心，我认识谁谁谁已经走在谢幕的路上，也别让领导再背锅了。\n</p>', '上周末亲自体验的一件故事，以往都是领导让下属背锅，我们来看看如何让领导背锅。所有领导都应该知道的管理故事。', '', 0, 1, 0, NULL, '', '', 1, 1610343818, NULL, 0);
INSERT INTO `article` VALUES (67, 2, '原创', '//cdn.dmake.cn/attachment/images/20210114/16106165035.png', '浅谈运营中的渠道运营', NULL, '<p style=\"text-align:left;\">\n	<strong>什么是渠道？</strong>\n</p>\n<p style=\"text-align:left;\">\n	简而言之通过他人之手、他人之地完成自己的运营目标，这样的手和地就是渠道。\n</p>\n<p style=\"text-align:left;\">\n	那么渠道长什么样？\n</p>\n<p style=\"text-align:left;\">\n	在大街上一个门面是一个渠道——销售渠道；\n</p>\n<p style=\"text-align:left;\">\n	在抖音、购物app中销售是一个渠道——网销售渠道；\n</p>\n<p style=\"text-align:left;\">\n	大街上的信息栏、公交车壁纸是一个渠道——社会媒介渠道；\n</p>\n<p style=\"text-align:left;\">\n	在写字楼里一个侃侃而谈的咨询顾问是一个渠道——人脉渠道；\n</p>\n<p style=\"text-align:left;\">\n	在QQ\\微信里一个群是一个渠道——信息渠道；\n</p>\n<p style=\"text-align:left;\">\n	在今日头条\\公众号\\微博\\B站\\抖音\\知乎里一个大V是一个渠道——自媒体渠道；\n</p>\n<p style=\"text-align:left;\">\n	报纸、电视台和广播是一个渠道——广告渠道；\n</p>\n<p style=\"text-align:left;\">\n	<strong>似乎每一个被人所看到的地方都可以是一个渠道！</strong> \n</p>\n<p style=\"text-align:left;\">\n	那么我们应该怎样去看渠道呢？\n</p>\n<p style=\"text-align:left;\">\n	如果我们只是做社区门店，那么信息渠道很关键；\n</p>\n<p style=\"text-align:left;\">\n	如果我们只是想让大家认识我们，广告渠道、社会媒介渠道就行了；\n</p>\n<p style=\"text-align:left;\">\n	如果我们想对社会产生影响力，自媒体渠道适合选择；\n</p>\n<p style=\"text-align:left;\">\n	如果我们想在特定的圈子里面产生影响力，人脉渠道是必选项；\n</p>\n<p style=\"text-align:left;\">\n	如果我们想把生意做遍全球，但是就是不想开门店，网销售渠道、自媒体渠道、信息渠道需要组合使用；\n</p>\n<p style=\"text-align:left;\">\n	如果我们想把生意做遍全球，但是就只是想开门店不搞网销，社会媒介、广告渠道、自媒体渠道需要组合使用；\n</p>\n<p style=\"text-align:left;\">\n	如果我们自己对很多人能够产生影响力，我们本身就是渠道。\n</p>\n<p style=\"text-align:left;\">\n	<br />\n</p>\n<p style=\"text-align:left;\">\n	下方导图的内容我将在板砖博客持续更新。\n</p>\n<p style=\"text-align:center;\">\n	<img src=\"http://cdn.dmake.cn/attachment/images/20210114/16106165035.png\" /> \n</p>', '哪怕一个微商在说自己如何做推广的时候都会受到渠道，那么在实际的业务运营中会用到哪些渠道呢？他们的特征和选择方式如何呢？看完本篇你也会实操。', '渠道运营', 0, 1, 0, NULL, '', '', 1, 1610616900, 1610678025, 0);
INSERT INTO `article` VALUES (71, 2, '原创', '//cdn.dmake.cn/attachment/images/20210128/16118128965.jpg', '短视频风口下企业要不要跟进？哪些坑需要注意？', NULL, '<p>\n	前情提要：在2019和2020年是各种短视频平台爆发的一年，主要体现在直播带货、个人网红这两项数据惊人。很多个人、机构或企业甚至国家单位都认可了这种销售和宣传模式。<span>南抖音北快手都是网站上B站，都是这个时代的掠影。</span> \n</p>\n<p>\n	那么短视频对一般企业而言有必要做吗？答案是肯定的！\n</p>\n<h4>\n	短视频不仅是宣传方式，更是经营思维方式的转变\n</h4>\n<p>\n	<strong>个人宣传</strong>的<span>传统</span>方式离不开话题炒作，无论是积极的还是消极的都需要对话题本身的打造付出诸多努力，但是成功概率极其微小，而且在传统的宣发方式中我们看到的低俗营销数量更为明显。\n</p>\n<p>\n	那么在短视频时代，个人的宣传往往基于“人设”做持久价值宣传。在网站平台上，人设是根据内容分类来贴标签的，按照质量反馈来做排名的；而在抖音等短视频上人设主要是根据个人打造的故事情节、人物职业、人物专业素养等方面。\n</p>\n<p>\n	<span>IP+专业标签=专业人设。</span>举两个例子，在B站“罗翔说刑法”中，个人IP是罗翔，专业标签是法律讲解；在抖音“小川说房”中，个人IP是小川，专业标签是说房。\n</p>\n<p>\n	而剧情号对于标签没有直接定义，因为剧情是多变的，也有一部分剧情号只针对一个场景做出不同的剧情。两种剧情号的效果对比没有非常大的波动，影响波动范围的往往是剧情质量、拍摄质量。\n</p>\n<p>\n	<strong>企业宣传</strong>的传统方式基本围绕<span>4P</span>营销理论，即产品本身、价格、渠道、促销。虽然全国电商化、信息化程度已经很高了，但是企业的营销思路也很久没有变化。\n</p>\n<p>\n	在2020爆火的短视频时代，很多企业虽然也跟风开启了直播、短视频，但是做好的企业寥寥无几，主要问题在于<strong>以我为主的经营思维</strong>难以转变。\n</p>\n<p>\n	那么<strong>以我为主的经营思维</strong>是什么思维呢？以下这几个场景您是否遇到过：\n</p>\n<ol>\n	<li>\n		&nbsp;我们这个产品的成本经过核算已经到最低了，但是价格还是下不来\n	</li>\n	<li>\n		&nbsp;我们产品的亮点功能这么明显，为什么关注的人寥寥无几\n	</li>\n	<li>\n		&nbsp;我们已经很有诚意的给出了返点政策，为什么还招不到代理\n	</li>\n	<li>\n		&nbsp;我的想法这么明确，但是搞执行的人为什么就是不按照我的要求来做\n	</li>\n</ol>\n<p>\n	这4个问题是不是很形象，那么我们用普通人的视角来解答一下为什么<strong>以我为主的经营思维</strong>不能收获&nbsp;好感：\n</p>\n<ol>\n	<li>\n		&nbsp;你们的成本不管我的事儿，我只想物美价廉\n	</li>\n	<li>\n		&nbsp;虽然你的产品亮点很多，但是我在生活中好像没有这么强的体验感\n	</li>\n	<li>\n		&nbsp;即便你的返点是30%，但是我们的运营成本更高，因此会选择返点更高的产品\n	</li>\n	<li>\n		&nbsp;虽然轮胎可以设计成正方形，但是这与使用场景和成本有明显的差距，设计者对此有异议也是正常现象\n	</li>\n</ol>\n<p style=\"text-align:center;\">\n	<img src=\"http://cdn.dmake.cn/attachment/images/20210128/16118128965.jpg\" /> \n</p>\n<h4>\n	案例\n</h4>\n<p>\n	接下来我要分享我在零售行业中亲自参与的短视频与直播的营销矩阵。\n</p>\n<p>\n	某零售企业周年庆会展现场第二天，人流减少明显，到访量约2000-3000人左右，我们现场启动了以企业抖音号+短视频+个人抖音号的营销，并取得一些数字上的成绩：\n</p>\n<ol>\n	<li>\n		企业号拍摄现场爆款产品的购买体验（价格+使用场景+感受体验）\n	</li>\n	<li>\n		开启直播，平台赠送流量，单次导流超过1万人\n	</li>\n	<li>\n		入镜大表妹个人号转型，以对各种活动的解读，拆解卖表演使用场景和感受体验，话题@企业号\n	</li>\n</ol>\n<p>\n	本次转变当天，明显感受到的是两个号关注粉丝增加，单个视频播放量过万。但是在接下来的一个月中DAU指标下降。分析原因是内容跟不上，因此第二次营销矩阵刺激。\n</p>\n<p>\n	接下来入镜大表妹继续按照剧情号日更内容，打造不那么惹人讨厌的卖货人设：\n</p>\n<ol>\n	<li>\n		&nbsp;和供应商配合做关于产品的购买场景、使用场景的轻松视频，单个视频不超过1分钟；\n	</li>\n	<li>\n		&nbsp;清理以前没有标签的视频，打造统一的风格；\n	</li>\n	<li>\n		尝试同款视频拍摄\n	</li>\n</ol>\n<p>\n	在此过程中，个人号粉丝数量稳定增长，但是还没有出现爆款视频。（至于后续···因为还没有发生，所以有新的进展会在此篇更新）\n</p>\n<h4>\n	复盘\n</h4>\n<ol>\n	<li>\n		&nbsp;视频的价值输出要明确，要么是送欢快等情感共鸣，要么送专业技能贴粉，要么给经济实惠，以此提升观看停留\n	</li>\n	<li>\n		&nbsp;形象要稳定，无论个人还是企业号，都不应该有第二个人格标签，多即是少，不能与受众产生共鸣无法提升关注\n	</li>\n	<li>\n		内容要以“你”为中心，打造观众喜欢的内容，而不是打造自己喜欢的内容\n	</li>\n</ol>', '本篇将以板砖在对短视频平台的分析以及个人在参与企业实践观察和反馈提出的个人想法，如果您的想法有所不同欢迎拍砖和补充。', '', 0, 1, 0, NULL, '', '', 1, 1611813496, 1611821393, 0);
INSERT INTO `article` VALUES (68, 2, '原创', '//cdn.dmake.cn/attachment/images/20210121/16112195664.png', '区域级别品牌连锁店的新零售运营探讨', NULL, '<p>\n	<img src=\"http://cdn.dmake.cn/attachment/images/20210121/16112195664.png\" /> \n</p>\n<p>\n	一、项目背景\n</p>\n<p>\n	委托公司是一个家族企业，有16家连锁门店，200人左右团队，30000会员，覆盖30万人城区。\n</p>\n<p>\n	目前企业遇到的问题是：\n</p>\n<blockquote>\n	<p>\n		1. 线上APP具备一定的价格优势\n	</p>\n	<p>\n		2. 门店新生代销售缺乏技巧、行动力\n	</p>\n	<p>\n		3. 线下门店竞争多样化，本地已经有一家其他品牌连锁门店倒闭\n	</p>\n</blockquote>\n<p>\n	目前企业提出的需求是：\n</p>\n<blockquote>\n	<p>\n		1. 结合门店优势拓展网销\n	</p>\n	<p>\n		2. 保持品牌在区域内的排名地位\n	</p>\n	<p>\n		3. 增强公司新零售和新媒体方面的基础能力\n	</p>\n</blockquote>\n<p>\n	二、运营难点\n</p>\n<p>\n	因为我曾经本就在该企业任职过运营部门经理，对该企业的基础能力和人员能力有一定的了解，因此省略了对其线下销售方面的调研，直接开启了工具（被人换过）、现有人员结构和能力方面的优化匹配。\n</p>\n<p>\n	根据工具功能体验、门店走访和关键人员1对1探讨发现了以下问题：\n</p>\n<blockquote>\n	<p>\n		1. 工具体验性差、便捷度不够、稳定性不强\n	</p>\n	<p>\n		2. 人员素质欠佳，大多新员工学历低社会经验弱销售能力差\n	</p>\n	<p>\n		3. 供应链短板，采购对货品甄别能力弱\n	</p>\n	<p>\n		4. 区域性网销案例少，需要培育垂直市场\n	</p>\n	<p>\n		5. 成本预算不高（其实工具部分也被坑了不少）\n	</p>\n	<p>\n		6. 异业合作商虽多，但是没有激活销售，属于单方向导流\n	</p>\n</blockquote>\n<p>\n	三、运营架构落地思路\n</p>\n<p>\n	说道运营，专业人事有专业的说法，普通人有普通人的理解，小白叫一问三不知。虽然我们意识很美好，但是很大可能性在做运营工作的时候面对的是一帮小白，领导的也是一帮小白。\n</p>\n<p>\n	因此，运营思路简单简介，运营说明大白话，运营落地流程化，运营工具傻瓜化，是我在做基层运营管理中的心得总结。\n</p>\n<p>\n	接下来我将用文字说明以上整体运营架构：\n</p>\n<blockquote>\n	<p>\n		1. 品牌影响力首先转化价格型客户；\n	</p>\n	<p>\n		2. 客户服务能力覆盖普通客户；\n	</p>\n	<p>\n		3. 用供应链能力打动所有客户；\n	</p>\n	<p>\n		4. 用行业领导力赚取场外客户；\n	</p>\n	<p>\n		5. 用盈利能力占据地区领导力。\n	</p>\n</blockquote>\n<p>\n	接下来我们分析一下我的具体举措是如何与运营架构对应上的。\n</p>\n<p>\n	<span><strong>1. 品牌影响力首先转化价格型客户 销售4P理论支持</strong></span> \n</p>\n<p>\n	<span>一个具有10多家连锁门店、具备十几年经营的企业一定是具备品牌效应的。当企业推出一个新的产品时首先购买的必然是狂热粉丝型客户，只要你敢出我就敢买；第二种人是价格敏感型的客户，只要你的产品、服务价格低又刚好具备替代的能力，对不起我要买它。</span> \n</p>\n<p>\n	很多企业在做运营的时候会陷入一个误区：全面覆盖狂轰滥炸。但是对不起您的资金池并不足以承担其成本，持久收益率也足以劝退后来者。\n</p>\n<p>\n	因此板砖的建议是：一个品牌的线上产品，首先应该是小而美的，虽有成本支出，但是人群稳定、流水稳定，可以做足够的市场反馈搜集，调整下一步运营计划。\n</p>\n<p>\n	<span> </span>\n</p>\n<p>\n	<strong>2. 客户服务能力覆盖普通客户 销售4C理论支持</strong> \n</p>\n<p>\n	当我们的第一单、第一个月、第一阶段分别达成了运营预期，搜集到了足够的购买反馈后，完善用户体验、扩展销售范围必须提上日程。\n</p>\n<p>\n	如果说第一阶段是在尝试自己能不能做网上业务，那么这个阶段便是思考如何做好网上业务。\n</p>\n<p>\n	很多零售企业在面对面销售中具有很强的优势，但是在无所面对的情况下就很迷茫：他们是谁？他们喜欢什么？他们愿意为什么付费？他们对我们的渠道有什么意见？\n</p>\n<p>\n	以上问题足以摧毁一个运营小白的意志，因为很多小白不懂销售不懂运营啊，这个时候板砖要提到销售4C理论值得学习一下。\n</p>\n<p>\n	4C分别指代Customer(顾客)、Cost(成本)、Convenience(便利)和Communication(沟通)。\n</p>\n<p>\n	<strong>3. 用供应链能力打动所有客户&nbsp;</strong> \n</p>\n<p>\n	供应链的能力并非单单压价的能力，而是：选品能力、货款周转能力、价格谈判能力等（对该岗位还有很多盲区认识，所以尽肯能不过多解读）。\n</p>\n<p>\n	传统企业中选品、货品库存的管理可以直接影响到企业的现金流，因此选品能力和货款周转能力是我最为崇拜的。但是在新零售中如何突破选品的认知范围是困难的，很大程度上企业在选品方面存在滞后性——采购不卖货 销售不选品。因此采购参与线上营销，一方面可以听到了解到足够多的用户需求，也能及时满足客户需求。\n</p>\n<p>\n	<strong>当火车票很难买的时候，如果你能够及时大量提供火车票，那么你的平台就是被热捧的。</strong> \n</p>\n<p>\n	<strong>4. 用行业领导力赚取场外客户&nbsp;</strong><strong>4R营销理论</strong> \n</p>\n<p>\n	很多独角兽企业具备行业的领导能力，但是独角兽提供的服务也不完全，因此利用行业领导力与其他企业合作，满足用户的购物、服务需求，促活其他企业与自己品牌的关联，可以建立起长期的流量导向、良性经济互助。\n</p>\n<p>\n	4R营销理论是以关系营销为核心，注重企业和客户关系的长期互动，重在建立顾客忠诚的一种理论。它既从厂商的利益出发又兼顾消费者的需求，是一个更为实际、有效的营销制胜术。\n</p>\n<p>\n	4RS理论的营销四要素：<br />\n第一，关联（Relevancy），即认为企业与顾客是一个命运共同体。建立并发展与顾客之间的长期关系是企业经营的核心理念和最重要的内容。<br />\n第二，反应（Reaction），在相互影响的市场中，对经营者来说最难实现的问题不在于如何控制、制定和实施计划，而在于如何站在顾客的角度及时地倾听和从推测性商业模式转移成为高度回应需求的商业模式。<br />\n第三，关系（Relationship），在企业与客户的关系发生了本质性变化的市场环境中，抢占市场的关键已转变为与顾客建立长期而稳固的关系。与此相适应产生了5个转向：从一次性交易转向强调建立长期友好合作关系；从着眼于短期利益转向重视长期利益；从顾客被动适应企业单一销售转向顾客主动参与到生产过程中来；从相互的利益冲突转向共同的和谐发展；从管理营销组合转向管理企业与顾客的互动关系。<br />\n第四，报酬（Reward），任何交易与合作关系的巩固和发展，都是经济利益问题。因此，一定的合理回报既是正确处理营销活动中各种矛盾的出发点，也是营销的落脚点<br />\n<strong>5. 用销售流水占据品牌领导力。</strong> \n</p>\n<p>\n	在网销当中，一定要考虑的是分销。无论拼多多的助力，还是微商拉人头，或是短视频带货，都是分销的一种表现模式：我帮你达成购买需求，我赚取佣金。\n</p>\n<p>\n	在此基础上的网销平台，无论是顾客、门店销售、合作企业，都能够在平台中赚取佣金的时候，那么平台销售流水、盈利能力也将大幅提高。\n</p>\n<p>\n	当以上营销框架都已满足的时候，自己的顾客愿意下单、关联企业有利润，那么品牌领导力自然持续。\n</p>\n<p>\n	<br />\n</p>', '运营思路简单简介，运营说明大白话，运营落地流程化，运营工具傻瓜化，是我在做基层运营管理中的心得总结，当然作为专业运营人营销理论4P\\4C\\4R还是需要了解一下。', '新零售运营，移动商城运营，行业整合', 0, 1, 0, NULL, '', '', 1, 1611219749, 1611281097, 0);
INSERT INTO `article` VALUES (73, 2, '原创', '//cdn.dmake.cn/attachment/images/20210208/161275085510.png', '传统零售企业在2021年新零售中的管理和团队约束讨论', NULL, '<p>\n	上一篇板砖分享了给一个中大型零售企业做的2021年线上运营规划，感兴趣的朋友可以往上翻回顾一下。\n</p>\n<h4>\n	企业经营是一场没有预设终点的马拉松，任何一次停顿都预示着终结\n</h4>\n<p style=\"text-align:center;\">\n	<img src=\"https://cdn.dmake.cn/attachment/images/20210208/161275085510.png\" /> \n</p>\n<p>\n	虽然每周我都会深入该企业的后台办公区和各门店，但是每次都有一些新的发现。或许做咨询顾问这个行当就是一个不断发现问题的吧。\n</p>\n<p>\n	在前天的检查中发现了以下问题：\n</p>\n<p>\n	1. 门店管理人员松懈，门店内务、商品陈列管理和场地利用率非常差；\n</p>\n<p>\n	2. 在新零售上并没有“新的理解”，门店销售依然靠站柜方式，人员并不主动支援做直播网销的同事\n</p>\n<p>\n	3. 采购体系对目前的公司发展计划落实不到位，虽然也要承担货品流通率的责任，但是在实际工作中并未改变“以我为主”的思想。\n</p>\n<p>\n	我为什么会认为这3个问题是新零售中的管理和团队约束重要问题呢，接下来我将分别讲解。\n</p>\n<h4>\n	管理人员思维常态化，部分效率将打折扣\n</h4>\n<p>\n	我们的门店管理人员基本上都是5-10年以上老员工，首先具备一定的销售水品，对公司也有很强的认同感，这是好处。但是在2021年的零售体系中又显得很不合群：\n</p>\n<p>\n	1. 对门店的体验感陷入了麻木，无法感知消费者在此的异样感。在此基础上，货品陈列、内务要求持续下滑。这个趋势要止住，建议对门店管理人员的考核以业绩为基础考核，以浮动任务完成程度为绩效考核。 例如2月第一周完成内务清洁、物料处理，第二周完成新年氛围布置等等。\n</p>\n<p>\n	<br />\n</p>\n<pre class=\"prettyprint lang-js\">案例场景：\n某母婴旗舰店有一条长30米的露台，平时作为杂物堆放处，不仅脏乱差还容易引发火灾。\n我的整改建议：\n1. 杂物需要快速处理不过周，露台处做好清洁卫生保持居家清洁状态（责任人门店店长）。\n2. 露台利用方法1：做客户喜欢的打卡场景，例如空中花园、绿植走廊等，邀约客户参与认领，每周有打理的视频分享（责任人店长）；\n3. 露台利用方法2：做不适合客户在室内体验的娱乐区，例如小朋友的石膏娃娃等，做好环境装扮，给小朋友沉浸其中的场景，锁定客户在店内滞留时间（可外包给游乐场）\n4. 露台利用方法3：玩具交易长廊，利用木条等搭建稳定的货架并划分区域和编号，邀约小朋友在此寄售小朋友的玩具，购买方式为会员积分，积分扣除后将发送购物券给卖方，形成了消费循环（新零售部）</pre>\n<p>\n	<br />\n</p>\n<p>\n	2. 对新零售认知偏差进入抵触或无感状态，支持这项工作的动力不强。新零售自提出以后有3年多时间了，但是很多主管并不知道这是什么，大概率的回答是网上卖货。这个概念板砖认为有很大偏差：新零售是借助互联网工具和其代表的各种营销手段促成销售的一种模式。 这种模式挑战的就是站柜这种不具创造力且正在被时代抛弃的方式。如果店内主管对这传统方式继续依赖，对新模式不主动学习和参与实践，首先淘汰的将是企业，这很可怕。 这个趋势要止住，建议门店管理人员主动参与新媒体部门的实际工作中。\n</p>\n<p>\n<pre class=\"prettyprint lang-js\">针对这一点要强调的是，作为才接触新零售的非专业群体，第一考核的不是买了多少，而是触达了多少人。如果都没有人看那怎么会有人买？内容的整理是非常核心的事情，一个5人门店至少有3人都应该参与其中。</pre>\n</p>\n<h4>\n	新零售不仅是在挑战销售，也是在挑战供应链\n</h4>\n<p>\n	部分企业认为新零售是一个部门的事，其实新零售是一个企业的事，核心之一是供应链。\n</p>\n<p>\n	无论是薇娅还是李佳琪，或者是罗永浩，首先成功的是供应链能力，其次才是主播的个人影响力。 试问你的偶像用1万元给你兜售1只普通马克笔你会要吗？\n</p>\n<p>\n	传统的采购是根据企业老板的偏好进行货品采购的，库存周转率完全是靠企业广告和运气，这在新零售方式上是完全行不通的。\n</p>\n<p>\n	关注市场需求、分析产品需求适配程度、参与销售渠道建设、严把成本与质量关是新零售模式中我认为的4大主要因子。这里的供应链就涉及的部门和人员因企业状况而异。\n</p>\n<p>\n	1. 市场需求：合适的时间卖合适的东西；\n</p>\n<p>\n	2. 分析产品需求适配程度：一个数量级的需求相似人群对产品的需求需要深度匹配，有人说需要护肤只是补水，有的人说护肤不仅需要补水还需要美白，有的人说护肤可能是病理性需求，不能根据产品字面意思和需求的字面意思就直接匹配，这样不对；\n</p>\n<p>\n	3.&nbsp;<span>参与销售渠道建设：采购经常说压力大，新零售不是自己的专业范畴。这是在推脱责任——任何时候采购都需要参与销售渠道建设！</span> \n</p>\n<p>\n	<span> </span>\n</p>\n<pre class=\"prettyprint lang-js\">采购参与销售渠道建设的方式和收获：\n1. 门店销售的时候参与陈列，因更先了解产品风格，可提升销售氛围营造\n2. 网络销售过程，可以了解客户对产品的反馈，及时调整品控、价格和备货量等\n3. 直播等方式销售，可以了解过程中顾客的需求匹配度，调整自己的组合策略。\n\n例如在本次采购节直播中，采购部提供商品杂乱无序无搭配，就是随便搞一下应付了事。既然是年货节，如何把零食、礼品、常用品打包，是否具有合理性都需要参与讨论制定（首先自己得知道为什么要这么做），可以想象的是2月7日的直播是一团糟。</pre>\n本篇是我在2月第一周随机调研的2件事，当然还有其他运营故事我会在后续的更新中分享。\n<p>\n	<br />\n</p>', '企业经营是一场没有预设终点的马拉松，如果对门店的体验感陷入了麻木，无法感知消费者在此的异样感，在此基础上，货品陈列、内务要求持续下滑···', '', 0, 1, 1, NULL, '', '', 1, 1612750888, 1612751924, 1);
INSERT INTO `article` VALUES (74, 1, '原创', '//cdn.dmake.cn/attachment/images/20210220/16137825423.jpg', '定制系统和购买成品哪个好？无论预算多少这篇都适用！', NULL, '<p>\n	这两月，板砖的一个小学弟频繁来咨询我软件系统定制的问题，我以为遇到了一个潜在的甲方，没成想是遇到了一个经常被人套路的小可爱。\n</p>\n<p>\n	Roger是个25岁出头的小伙子，现在接手了一个做软件外包的团队，主要是针对一些淘宝店主海外开网店的业务，经常会接到一些华人的系统定制咨询。\n</p>\n<p>\n	在国外除了Amazon、eBay等平台，很多企业和个人也乐于开设自己的网站网店，因此我们也能经常听说国外又火了什么品牌什么app等。\n</p>\n<p>\n	Roger就是承接这类业务的小伙子，但是接单率非常低。他的潜客一般会咨询 我的网店要English的能实现吗、我的网站要用Paypal支付能实现吗、我的网站要用visa支付能上实现吗等等问题，然而这些问题在一些电商专用建站系统中不是早就解决了吗？\n</p>\n<p style=\"text-align:center;\">\n	<img src=\"https://cdn.dmake.cn/attachment/images/20210220/16137825423.jpg\" /> \n</p>\n<h4>\n	那么定制系统和购买成品哪个好？在绝大多数情况下我推荐购买成品！\n</h4>\n<p style=\"text-align:left;\">\n	理由1 成品系统稳定bug少，减少了客户调试系统造成的长时间试错成本和时间成本\n</p>\n<p style=\"text-align:left;\">\n	理由2 成品系统行业功能齐全，系统有什么功能所见即所得，即便需要二开也比较方便\n</p>\n<p style=\"text-align:left;\">\n	理由3 首次建设成本低，系统+服务器&lt;20000RMB，不香吗？\n</p>\n<h4 style=\"text-align:left;\">\n	那么所有系统都适合购买成品吗？不是！\n</h4>\n<p style=\"text-align:left;\">\n	一般适合购买成品的行业主要是电商、地方门户、垂直门户网站类，例如网店、CMS、博客、旅游门户等。\n</p>\n<p style=\"text-align:left;\">\n	那么如果经营思路比较特殊的系统就必须要定制了，例如工具类系统（查号网）、业务类系统（查快递）等等。\n</p>\n<p style=\"text-align:left;\">\n	<br />\n</p>\n<p style=\"text-align:left;\">\n	总之，如果您对建站、APP有需求，可以参考以上两个建议。\n</p>', '那么定制系统和购买成品哪个好？那么所有系统都适合购买成品吗？不是！', '', 0, 1, 0, NULL, '', '', 1, 1613783805, 1613783815, 0);
INSERT INTO `article` VALUES (75, 2, '原创', '//cdn.dmake.cn/attachment/images/20210309/16152924919.jpg', '母婴连锁企业与儿童早教协作的体验活动营销', NULL, '<p>\n	本月收到某企业邀约去给他们的体验活动直播提意见。\n</p>\n<p>\n	据企业和异业商家前期多次的线下体验活动的市场反应而言，活动内容过度偏于商业性质，消费者对活动本身没有收获感到无趣，活动的筹备者也感到无措，因此我对线下体验活动做了4个简要分析：\n</p>\n<p>\n	<br />\n</p>\n<ol>\n	<li>\n		&nbsp;谁参与，谁邀约\n	</li>\n	<li>\n		&nbsp;参与者收获了什么\n	</li>\n	<li>\n		&nbsp;组织者收获了什么\n	</li>\n	<li>\n		&nbsp;能否持续，能否中止\n	</li>\n</ol>\n<p>\n	该母婴连锁和异业商家每周组织了妈妈班，原本旨在为家庭匹配相关的优质服务，但是长期以来都是讲师在上面讲产品、讲体验，顾客对此不感兴趣，次数一多参与者寥寥无几。为此我将按照上述4个问题剖析和解答该主题下的线下体验活动的可行性方法。\n</p>\n<p style=\"text-align:center;\">\n	<img src=\"https://cdn.dmake.cn/attachment/images/20210309/16152924919.jpg\" /> \n</p>\n<h4>\n	谁参与谁邀约\n</h4>\n<p>\n	据悉，以往活动都是由该企业承担邀约主力，主要面对群体都是该企业会员。会员特征为儿童年龄偏小，家长学历普遍不高，家庭社会体验感不强。\n</p>\n<h4>\n	参与者收获了什么\n</h4>\n<p>\n	线下体验活动中长期以现场甜点、水果为直接体验反馈，被动听取讲师宣讲。\n</p>\n<h4>\n	组织者收获了什么\n</h4>\n<p>\n	在多次体验后，企业会员对企业邀约产生了负面情绪，企业不仅没有直接受益，简介的还伤害了品牌价值。\n</p>\n<h4>\n	能否持续，能否中止\n</h4>\n<p>\n	根据组织者参与者反馈，此类体验活动不可持续，应我的建议直接中止，待优化后重启。\n</p>\n<h4>\n	体验活动方式方法和流程优化建议\n</h4>\n<p>\n	<br />\n</p>\n<ol>\n	<li>\n		精准筛选参与者\n	</li>\n	<li>\n		给参与者快乐，而非负担\n	</li>\n	<li>\n		给组织者情感赋能，间接预留销售契机\n	</li>\n	<li>\n		主题活动有周期性变化，赋予新鲜活力\n	</li>\n</ol>\n<p>\n	以上4个系列化思路其实是一个中心思想演化而成——给能够接受优秀服务的人以服务。试想每天只关心零碎价格的人会为服务买单吗？\n</p>\n<h4>\n	筛选参与者\n</h4>\n<p>\n	目前很多企业都有自己的CRM系统，或者手工台账记录客情，因此借助企业CRM筛选在2年内对非食品、纸品等育儿刚需产品有诸多消费的群体做筛选，这类人群普遍在经济上具有主动权，可以为以后的各种体验活动和体验活动后续消费作为种子顾客。\n</p>\n<h4>\n	给参与者快乐\n</h4>\n<p>\n	营销不是为了一下子赚钱，销售才是。线下体验活动更注重感官感受，所以组织者不要把卖货当做体验主题，而是收获快乐、健康、情感等因素排在第一。例如“手工比拼赢”、“XX运动体验赛”、“XX二手集市”、“XX烘焙体验”。\n</p>\n<p>\n	从经营的角度上来讲，我们好像没有看到什么销售痕迹，但是间接的我们分析可以拆分出“销售玩具”、“销售体育用品”、“销售二手对应的新品”、“销售食品”等契机。而参与者<span>，来则认同，</span>更多的是主动参与沉浸感更强，排斥感更弱。\n</p>\n<h4>\n	抓住销售契机\n</h4>\n<p>\n	从上述活动中，企业应该在活动规则中为自己的销售预埋契机。例如：\n</p>\n<p>\n	<br />\n</p>\n<ol>\n	<li>\n		手工比拼中，凡是参与手工比拼的前N名小朋友可以获得免费玩具，其余的可以领取X折玩具折扣券；\n	</li>\n	<li>\n		在体育比赛中，凡是参与就送体育礼包1份，例如头盔、护膝护肘等低成本用品，排名前N名小朋友可以获得免费课程，其余小朋友可以获得X折机会和X折相关商品券；\n	</li>\n	<li>\n		二手集市中，凡是参与并在现场有二手交易的，企业给交易者发放积分，积分可以兑换制定商品或服务，凡是参与者都可以获得相关商品券、服务体验机会；\n	</li>\n	<li>\n		在烘焙体验中，参与就是获得一次美食体验，并且可以获得企业额外赠送礼品、消费券等。\n	</li>\n</ol>\n<p>\n	为什么在上面几个方法中我都强调用“券”。 据我们在企业CRM中的数据反馈，顾客用券率达到80%以上，而且券的发放并不具备成本，但是券的使用一定存在利润。\n</p>\n<h4>\n	另外，体验容易忽视的地方\n</h4>\n<p>\n	以前组织者希望在主场邀约和体验，但是根据该母婴企业的组织者反应：在1个地方场景单一且体验性差，所以我的建议是打破场地限制，到适合做体验的场地去，收获体验感最强值。\n</p>\n<h4>\n	最终，主题活动的周期性变化\n</h4>\n<p>\n	在营销中心将就追热点，白话来说就是什么热门搞什么，什么冷清就放弃什么，不要把精力耗费在没有收获的事情上。\n</p>\n<p>\n	当然，每个主题不能用一次就丢，据我做过的多档餐饮、服务体验等活动中的经验而言，第一次活动成功率很高，第二次就是高潮，第三次就会感受到组织困难，第四次基本陷入了组织困境。因此我推荐每个主题活动组织次数不超过4次，当遇到了组织困境的情况下就需要立即变化主题。\n</p>\n<p>\n	以上，即是我对母婴连锁企业与儿童早教协作的体验活动营销中的一些看法和优化意见，感谢文末拍砖提意见。\n</p>\n<p>\n	<br />\n</p>\n<p>\n	<br />\n</p>\n<p>\n	<br />\n</p>', '该母婴连锁和异业商家每周组织了妈妈班，顾客对此不感兴趣，次数一多参与者寥寥无几。为此我将按照上述4个问题剖析和解答该主题下的线下体验活动的可行性方法。', '', 0, 1, 1, NULL, '', '', 1, 1615294673, 1615294703, 0);
INSERT INTO `article` VALUES (76, 2, '原创', '//cdn.dmake.cn/attachment/images/20210322/16163950527.png', '百度付费推广不在依赖企业自建站？2年内基木鱼完全替代付费推广方案', NULL, '<p>\n	今日收到百度推广方面消息，在今年9月份百度搜索中将全面使用基木鱼平台，完成付费推广赛道中服务与平台完全统一。\n</p>\n<p>\n	那么基木鱼是什么？基木鱼，可以根据不同客户投放需求，提供多场景解决方案。为不同的用户需求，精准匹配不同的优质连贯内容和服务，充分满足用户所需，同时搭载丰富多元的线索组件，与用户快速建立连接，实现多维度线索沉淀。\n</p>\n<p>\n	简而言之，基木鱼是一个不需要付费的企业模块化建站系统，其优势是无需开发技术，配置就能上线，其劣势是只有百度推广付费用户才能只在百度搜索中使用。比如你做好了不能在360里面使用，也不能在其他搜索引擎使用，基本上是百度产品（百度推广-爱番番-基木鱼）的完全闭环。\n</p>\n<p style=\"text-align:center;\">\n	<img src=\"https://cdn.dmake.cn/attachment/images/20210322/16163949006.png\" />\n</p>\n<p style=\"text-align:center;\">\n	<img src=\"https://cdn.dmake.cn/attachment/images/20210322/16163949414.png\" />\n</p>\n<p style=\"text-align:center;\">\n	<img src=\"https://cdn.dmake.cn/attachment/images/20210322/16163949729.png\" />\n</p>\n<p style=\"text-align:center;\">\n	<img src=\"https://cdn.dmake.cn/attachment/images/20210322/16163950037.png\" />\n</p>\n<p style=\"text-align:center;\">\n	<img src=\"https://cdn.dmake.cn/attachment/images/20210322/16163950191.png\" />\n</p>\n<p style=\"text-align:center;\">\n	<img src=\"https://cdn.dmake.cn/attachment/images/20210322/16163950353.png\" />\n</p>\n<p style=\"text-align:center;\">\n	<img src=\"https://cdn.dmake.cn/attachment/images/20210322/16163950527.png\" />\n</p>\n<p>\n	那么这对于企业来说，有1个优势也有1个劣势：\n</p>\n<p>\n	优势在于只做百度推广的企业不需要单独花钱建站，可以省下一大笔费用；而劣势在于多条赛道推广的企业不仅需要花钱维护其他渠道还需要额外花钱来维护百度推广的内容。\n</p>\n<p>\n	作为推广个人或团体这又无异于绽开了第二春——很多企业对基木鱼的0了解正是开单好时机，在今年9月之前，很多拥有市场敏锐度的团队和个人将赚的盆满钵满！\n</p>', '作为推广个人或团体这又无异于绽开了第二春——很多企业对基木鱼的0了解正是开单好时机，在今年9月之前，很多拥有市场敏锐度的团队和个人将赚的盆满钵满！', '', 0, 1, 0, NULL, '', '', 1, 1616395081, NULL, 0);
INSERT INTO `article` VALUES (77, 2, '原创', '//cdn.dmake.cn/attachment/images/20210324/16165512765.png', '百度付费推广收拢企业自建网站，对企业而言反而利好？', NULL, '<p style=\"text-align:center;\">\n	<img src=\"https://cdn.dmake.cn/attachment/images/20210324/16165512765.png\" /> \n</p>\n<p>\n	近日我们运营团队收到百度付费推广代理商的一个重磅通知——今年内所有推广企业需要将自建站推广内容转移至基木鱼平台。\n</p>\n<p>\n	一时间团队内部也是怨声载道：基木鱼平台操作十分不便、展现效果奇差、推广内容转移困难、推广落地页下有贡献率受影响等问题。\n</p>\n<h4>\n	塞翁失马焉知非福\n</h4>\n<p>\n	今天我们简单讲一下基木鱼带来的推广红利：不正规的企业、小企业运营团队不够强大的情况下很难再在推广市场掀起波浪了。在基木鱼平台的统一管理下百度可以利用AI技术识别和下线一些触碰红线的内容，“魏则西”这样的悲剧将极大减少。\n</p>\n<p>\n	另外一个对SEM、SEO和网站建设从业人员而言也不一定是坏消息。自2019年起我们运营团队只采用1个策略——白帽SEO冲排名，网站一共打造了3500个左右的行业精准关键词，搜索业务排名有300多个排TOP10以内，给网站带来了66%的流量，因此在本次事件中我们的推广业务应该受到的波及很小。\n</p>\n<p>\n	那么SEO和网站建设从业人员在本次事件中专业性突出明显：SEO快排、白帽SEO、网站建设专业优化等工作都能够为企业带来额外流量。\n</p>\n<h4>\n	企业利好并非空谈\n</h4>\n<p>\n	做SEO和SEM的同行经常会发现目前我们的竞争压力特别大，此事件后通过复杂的操作手段和不好的体验直接排除掉尾部企业，可以直接拉低竞争成本；而且部分老牌企业如果早期已经完成了自己平台的内容建设和优化的，在这一次事件中将直接收获交接棒时的流量红利。\n</p>\n<h4>\n	立个Flag\n</h4>\n<p>\n	百度在2021年作出如此巨大的决定并非空穴来风，也是市场净化的一种表现，无论是主动还是被动的。在2021年百度推广的业绩必然会有一个下滑，但是疯狂的市场也会跟着一起寻找推广的替代品，短视频或自媒体，或寻找基木鱼的漏洞。因为改变的时间紧迫，我更相信在2021年里钻基木鱼的漏洞将是第一批受到青睐的方式。\n</p>\n<p>\n	如果您想对此事件展开讨论，也请在下方留言哟。\n</p>', '一时间团队内部也是怨声载道：基木鱼平台操作十分不便、展现效果奇差、推广内容转移困难、推广落地页下有贡献率受影响等问题。塞翁失马焉知非福！', '基木鱼，企业建站', 0, 1, 0, NULL, '', '', 1, 1616552118, 1616552311, 0);
INSERT INTO `article` VALUES (78, 1, '原创', '//cdn.dmake.cn/attachment/images/20210412/16182106868.jpg', '选择软件定制还是成品软件？行业从业者高速你真相！', NULL, '<h3 style=\"font-family:&quot;font-weight:normal;color:#333333;font-size:1.75rem;background-color:#FFFFFF;\">\n	<span style=\"color:#555555;font-family:&quot;font-size:14px;background-color:#FFFFFF;font-weight:normal;\">在国内各行业业态不同、企业管理模式不同、商业逻辑的区别带来了很大一部分软件定制的需求，那么软件真的需要定制吗？如果定制是外包还在自建更好？这里面存在哪些风险？本系列文章将把这些问题详细分解。</span>\n</h3>\n<p style=\"color:#555555;font-family:&quot;font-size:13px;background-color:#FFFFFF;\">\n	更新周期不固定，固定的是每周有更新！如果你正面临软件定制的需求，一定记得看完本篇。\n</p>\n<p style=\"color:#555555;font-family:&quot;font-size:13px;background-color:#FFFFFF;\">\n	也欢迎对软件定制感兴趣的企业和个人和我展开一对一讨论，QQ号554305954 微信daneas2014\n</p>\n<p style=\"color:#555555;font-family:&quot;font-size:13px;background-color:#FFFFFF;\">\n	<hr />\n	<p style=\"color:#555555;font-family:&quot;font-size:13px;background-color:#FFFFFF;\">\n		客观的去理解自己的需求选择适合的方式获得软件系统，需要提前了解软件定制和成品软件的区别，最终可以防止企业过度投入、节省开支、早日用上合适的系统软件。\n	</p>\n	<h4 style=\"font-family:&quot;font-weight:normal;color:#333333;font-size:1.5rem;background-color:#FFFFFF;\">\n		什么是成品软件\n	</h4>\n	<p style=\"color:#555555;font-family:&quot;font-size:13px;background-color:#FFFFFF;\">\n		成品软件就是安装即用，例如电脑上的钉钉、QQ、微信等软件，成品软件是软件公司针对某一类型的业务、人群等开发的通用型工具。虽然说是软件，但是也包含了桌面程序、手机应用和网站这三个常规方式。\n	</p>\n	<h4 style=\"font-family:&quot;font-weight:normal;color:#333333;font-size:1.5rem;background-color:#FFFFFF;\">\n		什么是定制软件\n	</h4>\n	<p style=\"color:#555555;font-family:&quot;font-size:13px;background-color:#FFFFFF;\">\n		一些企业因为自己的商业模式独特、行业软件稀少等原因没法获得可以用于业务的软件，因此只有根据自己的业务需求做一套只属于自己的系统。\n	</p>\n	<h4 style=\"font-family:&quot;font-weight:normal;color:#333333;font-size:1.5rem;background-color:#FFFFFF;\">\n		成品软件的优势\n	</h4>\n	<p style=\"color:#555555;font-family:&quot;font-size:13px;background-color:#FFFFFF;\">\n		成品软件一般由其开发公司持续投入研发，动态的去收集市场需求迭代软件功能，企业只需要不定时更新升级即可。并且成品软件因为普适性强出货量大，因此企业花费的成本较少、软件提供的一些新功能可以覆盖甚至给企业带来业务增长。\n	</p>\n	<h4 style=\"font-family:&quot;font-weight:normal;color:#333333;font-size:1.5rem;background-color:#FFFFFF;\">\n		定制软件的优势\n	</h4>\n	<p style=\"color:#555555;font-family:&quot;font-size:13px;background-color:#FFFFFF;\">\n		定制软件一般由公司先行梳理业务需求，在自筹或寻找外包企业针对需求做功能实现，比如哪个功能需要做哪些表、怎么填报、数据怎么展现等。虽然说需求就是一句话，但是工作量非常大，唯一优势即适合自己的业务发展，自己想怎么改就怎么改。\n	</p>\n	<hr />\n	<p style=\"color:#555555;font-family:&quot;font-size:13px;background-color:#FFFFFF;\">\n		以下是我做的对比评测表，如果想知道更多的软件定制和成品软件的区别，欢迎来找我讨论。\n	</p>\n	<table cellpadding=\"2\" cellspacing=\"0\" border=\"1\" bordercolor=\"#000000\" style=\"color:#666666;font-family:&quot;font-size:13px;background-color:#FFFFFF;width:814px;\">\n		<tbody>\n			<tr>\n				<td>\n					评测项目\n				</td>\n				<td>\n					成品软件\n				</td>\n				<td>\n					定制软件\n				</td>\n			</tr>\n			<tr>\n				<td>\n					功能\n				</td>\n				<td>\n					安装即可知道所有功能，体验性好\n				</td>\n				<td>\n					需要和开发团队一起探讨，前期不可视\n				</td>\n			</tr>\n			<tr>\n				<td>\n					价格\n				</td>\n				<td>\n					成本低\n				</td>\n				<td>\n					成本高\n				</td>\n			</tr>\n			<tr>\n				<td>\n					更新周期\n				</td>\n				<td>\n					软件公司决定\n				</td>\n				<td>\n					业务需求决定\n				</td>\n			</tr>\n			<tr>\n				<td>\n					维护团队\n				</td>\n				<td>\n					基本不要人\n				</td>\n				<td>\n					需要留有技术人员做系统维护\n				</td>\n			</tr>\n			<tr>\n				<td>\n					管理团队\n				</td>\n				<td>\n					不需要人\n				</td>\n				<td>\n					业务部门经常做功能探讨，去伪存真求增\n				</td>\n			</tr>\n			<tr>\n				<td>\n					便利程度\n				</td>\n				<td>\n					普遍存在电脑和手机端应用\n				</td>\n				<td>\n					需要定制\n				</td>\n			</tr>\n			<tr>\n				<td>\n					交付周期\n				</td>\n				<td>\n					付款即可使用\n				</td>\n				<td>\n					签约、开发、调试大约用时1-2个月\n				</td>\n			</tr>\n		</tbody>\n	</table>\n	<p style=\"color:#555555;font-family:&quot;font-size:13px;background-color:#FFFFFF;\">\n		<br />\n板砖评价：不到万不得已不要考虑软件定制。\n	</p>\n	<p style=\"color:#555555;font-family:&quot;font-size:13px;background-color:#FFFFFF;\">\n		<hr />\n	</p>\n	<p style=\"color:#555555;font-family:&quot;font-size:13px;background-color:#FFFFFF;\">\n		很多行业已经约定俗成的选择了成品软件，例如OA、零售、库房、旅游、房产、汽车等，但是也有很多选择定制，例如教育教学、园区管理、数据管理、平台业务等。\n	</p>\n	<p style=\"color:#555555;font-family:&quot;font-size:13px;background-color:#FFFFFF;\">\n		目前稍微整理了下各类成品软件系统：\n	</p>\n	<p style=\"color:#555555;font-family:&quot;font-size:13px;background-color:#FFFFFF;\">\n		零售商超：管家婆、思迅、客如云、易订货等\n	</p>\n	<p style=\"color:#555555;font-family:&quot;font-size:13px;background-color:#FFFFFF;\">\n		招聘网站：骑士CMS\n	</p>\n	<p style=\"color:#555555;font-family:&quot;font-size:13px;background-color:#FFFFFF;\">\n		旅游平台：思途、泽园等\n	</p>\n	<p style=\"color:#555555;font-family:&quot;font-size:13px;background-color:#FFFFFF;\">\n		汽车管理：金麦、喜养车、66公里等\n	</p>\n	<p style=\"color:#555555;font-family:&quot;font-size:13px;background-color:#FFFFFF;\">\n		办公OA：钉钉、泛微、飞书、雨木、蓝凌、索昂等\n	</p>\n	<p style=\"color:#555555;font-family:&quot;font-size:13px;background-color:#FFFFFF;\">\n		财务办公ERP：金蝶、用友等\n	</p>\n	<p style=\"color:#555555;font-family:&quot;font-size:13px;background-color:#FFFFFF;\">\n		通用平台建站：Wordpress、eshop等\n	</p>\n	<p style=\"color:#555555;font-family:&quot;font-size:13px;background-color:#FFFFFF;\">\n		这些平台软件等一个普遍的特征就是开发企业长期聚焦行业，对功能模块、企业需求、客户管理等都具备很完备的理解。很大程度上这些软件系统提供的功能都要大于企业需求。\n	</p>\n	<p style=\"color:#555555;font-family:&quot;font-size:13px;background-color:#FFFFFF;\">\n		然而另一面，在很多需求满足的情况下，独角兽企业的独特经营方式、数据分析呈现、成本流通率控制方面都有和同行大不一样的地方，因此企业可以根据自身情况选择是部分软件定制还是完全软件定制。\n	</p>\n	<p style=\"color:#555555;font-family:&quot;font-size:13px;background-color:#FFFFFF;\">\n		部分软件定制就是二次开发，比如零售企业想通过经营数据做大数据分析，挖掘经营潜力；财务办公可以通过软件定制实现更丰富的经营业务分析呈现，实现按照企业经营思路的便捷管理等。\n	</p>\n	<p style=\"color:#555555;font-family:&quot;font-size:13px;background-color:#FFFFFF;\">\n		总之，很多企业在购买了成品软件后总会发现有部分功能缺失，那么软件定制、二次开发能够解决这些问题的情况下尽量定制，当然会存在二次开发比成品软件还贵的情况。这是因为成品软件批量销售成本很低，定制开发又具备独立性，因此成本高了也很正常。\n	</p>\n	<p style=\"color:#555555;font-family:&quot;font-size:13px;background-color:#FFFFFF;\">\n		<hr />\n		<p style=\"color:#555555;font-family:&quot;font-size:13px;background-color:#FFFFFF;\">\n			很多企业对软件定制的概念是懂了，但是如何实施容易做出错误的判断，最终导致钱花了、团队松散了、项目也做的不痛不痒的，这是因为缺乏企业软件定制的配套考虑。\n		</p>\n		<p style=\"color:#555555;font-family:&quot;font-size:13px;background-color:#FFFFFF;\">\n			企业软件定制的配套考虑一般是组织协调、培训<span>、数据上线</span>、搜集反馈四个步骤。\n		</p>\n		<h4 style=\"font-family:&quot;font-weight:normal;color:#333333;font-size:1.5rem;background-color:#FFFFFF;\">\n			组织协调\n		</h4>\n		<p style=\"color:#555555;font-family:&quot;font-size:13px;background-color:#FFFFFF;\">\n			在企业确定了需要软件定制之后，需要指定人员组织业务部门对功能用途、解决问题、录入展示等需求做文字汇总，以及和技术团队分析需求，并将需求报告返回业务部门确认，如此再三确定开发项目。\n		</p>\n		<h4 style=\"font-family:&quot;font-weight:normal;color:#333333;font-size:1.5rem;background-color:#FFFFFF;\">\n			培训\n		</h4>\n		<p style=\"color:#555555;font-family:&quot;font-size:13px;background-color:#FFFFFF;\">\n			一般情况下软件公司在完成项目后会给甲方做系统说明书，以及面对面软件操作培训，但是培训次数有限，很多业务部门没有掌握操作的需要公司内部在此培训，因此公司内的培训人员需要指定，并且熟悉整个系统。\n		</p>\n		<h4 style=\"font-family:&quot;font-weight:normal;color:#333333;font-size:1.5rem;background-color:#FFFFFF;\">\n			数据上线\n		</h4>\n		<p style=\"color:#555555;font-family:&quot;font-size:13px;background-color:#FFFFFF;\">\n			系统上线后，以往的历史数据需要导入新系统。有电子数据的会很方面，但是手工数据都需要人工录入，或者约定从某个时间段开始录入。因此该项工作需要指定一名懂业务、能协调的人员，以组织各部门能够在切换时间点做好历史数据的存档、转录。\n		</p>\n		<h4 style=\"font-family:&quot;font-weight:normal;color:#333333;font-size:1.5rem;background-color:#FFFFFF;\">\n			搜集反馈\n		</h4>\n		<p style=\"color:#555555;font-family:&quot;font-size:13px;background-color:#FFFFFF;\">\n			定制软件难免会有BUG和新的需求，因此公司需要指定人手不定时的搜集各业务部门反馈，以及与技术团队交涉解决bug问题。\n		</p>\n		<p style=\"color:#555555;font-family:&quot;font-size:13px;background-color:#FFFFFF;\">\n			以上四个步骤，公司一定要考虑这个人员配置，如果每个步骤由不同的人来组织，那么可能会存在很大的衔接问题，如果是由1个人来组织，那么公司需要明文为这个职员赋值。\n		</p>\n		<hr />\n		<p style=\"color:#555555;font-family:&quot;font-size:13px;background-color:#FFFFFF;\">\n			企业确定了要定制软件后，需要确定是谁来做开发。绝大多数企业是没有IT研发部门的，因此推荐寻找可以驻场开发的外包或者是有实力的软件开发企业。\n		</p>\n		<p style=\"color:#555555;font-family:&quot;font-size:13px;background-color:#FFFFFF;\">\n			现在很多企业会选择一些分包平台例如猪八戒、一品威客等寻找外包，渠道是没有问题，问题是你不知道接包团队是否靠谱。所以在一些分包要求上都会看到要求“驻地开发”。\n		</p>\n		<p style=\"color:#555555;font-family:&quot;font-size:13px;background-color:#FFFFFF;\">\n			什么是驻地开发呢，就是说你人来我公司，包食宿，该怎么付款就怎么付款。这个条件呢可以晒出掉一些兼职人员，很多兼职人员可能本来工作比较清闲，但是万一接到外包项目后自己的工作也忙碌起来了，这样就不能保证项目质量。\n		</p>\n		<p style=\"color:#555555;font-family:&quot;font-size:13px;background-color:#FFFFFF;\">\n			还有呢就是找有实力的软件开发企业。这样的企业每个城市都有，但是建议你要去实地考察一下，很多初创公司的员工是才毕业的实习生，没有多少工作经验，开发质量也没有保证。实地考察可以看到整体的团队规模、成熟度，雇主也能够放心将项目外包。\n		</p>\n	</p>\n</p>\n<p style=\"color:#555555;font-family:&quot;font-size:13px;background-color:#FFFFFF;\">\n	<br />\n</p>', '看完本篇你就知道是定制软件还是选择成品软件，看完之后基本上不会选择错误。', '', 0, 1, 0, NULL, '', '', 1, 1618210728, NULL, 0);
INSERT INTO `article` VALUES (79, 3, '原创', 'https://www.dmake.cn/static/images/16087054367.png', 'DMake建站系统和FastAdmin谁更好？', NULL, '<p>\n	首先承认，DMAKE建站系统和FastAdmin存在一定的共同性——便捷建站、TP5+Mysql+bootstrap，除此之外好像没有其他技术上的共同性了。\n</p>\n<p>\n	其次，在功能上我们都存在一定的共同性——CMS系统、商城、社区。\n</p>\n<p>\n	最后我们来分析一下DMake建站系统和FastAdmin谁更好。\n</p>\n<p style=\"text-align:center;\">\n	<img src=\"https://www.dmake.cn/static/images/16087054367.png\" alt=\"板砖博客系统\" />\n</p>\n<h4>\n	技术方面\n</h4>\n<p>\n	FastAdmin是一种标准化建站框架，所有功能是需要以插件形式实现，对软件开发企业而言有很好的可持续开发支持（不怕人员波动）。\n</p>\n<p>\n	DMAKE建站系统是一种非标准化建设框架，所有的功能是按照一定的业务需求组装的，对开发者而言适合做减法（加减字段、删减功能），不需要特别大的开发团队。\n</p>\n<p>\n	从技术上简单总结，FastAdmin是毛坯房，DMAKE是精装修，如何选择需要根据业务需求而定。\n</p>\n<h4>\n	使用终端\n</h4>\n<p>\n	FastAdmin和DMAKE都采用的前后端分离的方式，移动端都使用了API调用且用UNIAPP框架，因此都具备很强的扩展性，目前都适用WEB、WAP、小程序等。\n</p>\n<h4>\n	业务场景方面\n</h4>\n<p>\n	FastAdmin因为是一种建站思维和技术框架，因此系统没有内设可以直接运营的业务模块（<a href=\"https://www.fastadmin.net/demo.html\" target=\"_blank\">直达体验</a>），更多的是一种理论上的实现能力验证。\n</p>\n<p>\n	DMAKE建站系统是根据作者多年业务经验汇总的运营平台，如果企业没有更多要求的情况下是可以直接拿来运营使用的，更多的是经营思维的实现。\n</p>\n<p>\n	从业务场景方面，和技术总结相同，<span>FastAdmin是毛坯房，DMAKE是精装修，如果企业是想寻找一套可以直接上手即用的系统，DMAKE可能更好。</span>\n</p>\n<p>\n	<br />\n</p>\n<hr />\n不吹不黑，作为一个开发人员要在技术上去提炼个人能力我推荐FastAdmin，这套系统在国内的确存在很强的实用性；作为一个运营人员我推荐DMAKE这套系统，这套系统的业务思路在两个全国性平台实践后提炼而来，业务完毕体验迭代也适合企业使用。\n<p>\n	<br />\n</p>\n<p>\n	<br />\n</p>', 'FastAdmin是一种标准化建站框架，所有功能是需要以插件形式实现，对软件开发企业而言有很好的可持续开发支持。DMAKE建站系统是一种非标准化建设框架，所有的功能是按照一定的业务需求组装的，对开发者而言适合做减法，不需要特别大的开发团队。', 'FastAdmin，DMAKE，系统建站', 0, 1, 1, NULL, '', '', 1, 1618541972, NULL, 0);
INSERT INTO `article` VALUES (80, 6, '原创', '//cdn.dmake.cn/attachment/images/20200119/15794117754.jpg', '《老人与海》观后感', NULL, '<p>\n	或许只有体验过绝望的人，以及在困境中不断对抗的人，在看老人与海的过程中心心相印。\n</p>\n<p style=\"text-align:right;\">\n	《老人与海》观后感&nbsp; 2021年2月1日\n</p>\n<p>\n	<br />\n</p>', '或许只有体验过绝望的人，以及在困境中不断对抗的人，在看老人与海的过程中心心相印。', '', 0, 1, 0, NULL, '', '', 1, 1620805051, NULL, 0);
INSERT INTO `article` VALUES (81, 2, '原创', '//cdn.dmake.cn/attachment/images/20210701/162511936410.jpg', '做甲方信息顾问的一个经验：没有做好四步准备信息化/业务系统定制白搭', NULL, '<p style=\"color:#555555;font-family:\" font-size:13px;background-color:#ffffff;\"=\"\">\n	很多企业对软件定制的概念是懂了，但是如何实施容易做出错误的判断，最终导致钱花了、团队松散了、项目也做的不痛不痒的，这是因为缺乏企业软件定制的配套考虑。\n	</p>\n<p style=\"color:#555555;font-family:\" font-size:13px;background-color:#ffffff;\"=\"\">\n	企业软件定制的配套考虑一般是组织协调、培训<span>、数据上线</span>、搜集反馈四个步骤。\n</p>\n<h4 style=\"font-family:\" font-weight:normal;color:#333333;font-size:1.5rem;background-color:#ffffff;\"=\"\">\n	组织协调\n	</h4>\n<p style=\"color:#555555;font-family:\" font-size:13px;background-color:#ffffff;\"=\"\">\n	在企业确定了需要软件定制之后，需要指定人员组织业务部门对功能用途、解决问题、录入展示等需求做文字汇总，以及和技术团队分析需求，并将需求报告返回业务部门确认，如此再三确定开发项目。\n		</p>\n<h4 style=\"font-family:\" font-weight:normal;color:#333333;font-size:1.5rem;background-color:#ffffff;\"=\"\">\n	培训\n			</h4>\n<p style=\"color:#555555;font-family:\" font-size:13px;background-color:#ffffff;\"=\"\">\n	一般情况下软件公司在完成项目后会给甲方做系统说明书，以及面对面软件操作培训，但是培训次数有限，很多业务部门没有掌握操作的需要公司内部在此培训，因此公司内的培训人员需要指定，并且熟悉整个系统。\n				</p>\n<h4 style=\"font-family:\" font-weight:normal;color:#333333;font-size:1.5rem;background-color:#ffffff;\"=\"\">\n	数据上线\n					</h4>\n<p style=\"color:#555555;font-family:\" font-size:13px;background-color:#ffffff;\"=\"\">\n	系统上线后，以往的历史数据需要导入新系统。有电子数据的会很方面，但是手工数据都需要人工录入，或者约定从某个时间段开始录入。因此该项工作需要指定一名懂业务、能协调的人员，以组织各部门能够在切换时间点做好历史数据的存档、转录。\n						</p>\n<h4 style=\"font-family:\" font-weight:normal;color:#333333;font-size:1.5rem;background-color:#ffffff;\"=\"\">\n	搜集反馈\n							</h4>\n<p style=\"color:#555555;font-family:\" font-size:13px;background-color:#ffffff;\"=\"\">\n	定制软件难免会有BUG和新的需求，因此公司需要指定人手不定时的搜集各业务部门反馈，以及与技术团队交涉解决bug问题。\n								</p>\n<p style=\"color:#555555;font-family:\" font-size:13px;background-color:#ffffff;\"=\"\">\n	以上四个步骤，公司一定要考虑这个人员配置，如果每个步骤由不同的人来组织，那么可能会存在很大的衔接问题，如果是由1个人来组织，那么公司需要明文为这个职员赋值。\n							</p>\n							<hr />\n							<span style=\"color:#666666;font-family:\" font-size:13px;background-color:#ffffff;\"=\"\">以上内容便是企业软件定制的配套考虑，如果想和我展开一对一讨论，QQ号554305954 微信daneas2014，<a href=\"https://www.evget.com/mis\" target=\"_blank\">点击这里看一看我在信息系统定制研发方面的总结</a>。</span>\n							<div>\n								<span style=\"color:#666666;font-family:\" font-size:13px;background-color:#ffffff;\"=\"\"><br />\n</span> \n							</div>', '很多企业对软件定制的概念是懂了，但是如何实施容易做出错误的判断，最终导致钱花了、团队松散了、项目也做的不痛不痒的，这是因为缺乏企业软件定制的配套考虑。', '', 0, 1, 0, NULL, '', '', 1, 1625119388, 1625119442, 1);
INSERT INTO `article` VALUES (82, 2, '原创', '//cdn.dmake.cn/attachment/images/20210728/16274468813.jpeg', '亚马逊大规模封店背后给中国贸易企业与B端市场带来的机遇', NULL, '<p>\n	近日国内外贸圈儿再次爆出热门消息——亚马逊集中封店造成5-10万外贸企业生存困难。\n</p>\n<p>\n	今天我这里不讨论为什么会有大批量店铺被封的原因，基本上原因也会在合规化与政策之间反复横跳。\n</p>\n<p style=\"text-align:center;\">\n	<img src=\"https://cdn.dmake.cn/attachment/images/20210728/16274468813.jpeg\" /> \n</p>\n<p style=\"text-align:left;\">\n	2010年是中国电商爆发元年，基于支付宝产品重塑成功之后，各类自建电商平台与论坛、QQ群交互活跃，为“电商”深入群众打下了坚实基础。\n</p>\n<h4 style=\"text-align:left;\">\n	隐患\n</h4>\n<p style=\"text-align:left;\">\n	2012年电商出海成为很多企业的重要举措，甚至至今也是贸易圈儿的重要渠道。在海外打通销售主要有以下3个渠道：\n</p>\n<p style=\"text-align:left;\">\n	1. 低价供应走线下渠道，优点是销售稳定，缺点是利润低；\n</p>\n<p style=\"text-align:left;\">\n	2. 上亚马逊、ebay等平台，优点是销售稳定、利润可控，缺点是引流被动随时可能被平台K；\n</p>\n<p style=\"text-align:left;\">\n	3. 独立建站，结合搜索引擎与Twitter等平台引流打造IP，优点是利润高、操作灵活、不受平台监管，缺点是推广成本高、专业团队需求大。\n</p>\n<p style=\"text-align:left;\">\n	国内很多企业在短期效益和资深资源体系的双重考量下大多数会选择渠道2，1个店铺销售不高那就开50个、100个，流量入口多了销售额自然就上去了。当然这也为今日爆发的亚马逊封店潮影响巨大做好了铺垫。\n</p>\n<p style=\"text-align:left;\">\n	本次封店潮中，只要被盯上的店铺一经封店，该主体下的所有店铺全部关店，简直是“壕无人性”。此时一些企业反应过来在平台再怎么做也是为他人做嫁衣，独立建站需求反应徒增。包括在内陆重庆的一些贸易企业也增加了海外站点运营的相关岗位。\n</p>\n<h4 style=\"text-align:left;\">\n	机遇\n</h4>\n<p style=\"text-align:left;\">\n	在国内，很多中小IT企业的主营业务是网站建设、app和小程序等开发，主要客户是来面向国内市场的企业，而市场饱和度是肉眼可见。 而目前跨境电商独立建站主要平台和产品有shopify、shopee，以及部分供应链提供的与ERP打通的电商平台，供应方选择并不多。\n</p>\n<p>\n	因此，板砖认为在亚马逊封店潮的契机之下，部分国内IT企业的经营目光可以转移到全球业务市场，为国内数十万外贸企业提供工具，避开国内竞争。\n</p>\n<p>\n	同时，国内软件市场/商城平台从以往的面向国内需求做产品分发单独成立面向全球贸易的宣发板块，能够精准的快速占领需求蓝海，成为下一个平台业务增长点。\n</p>\n<h4>\n	总结\n</h4>\n<p>\n	没有哪一个市场是亘古不变的，也没有哪一个需求是长期存在的。企业经营可以为短期利益妥协，但是也不要忘记长远布局，人无远虑必有近忧！\n</p>', '部分国内IT企业的经营目光可以转移到全球业务市场，为国内数十万外贸企业提供工具，避开国内竞争。同时，国内软件市场/商城平台从以往的面向国内需求做产品分发单独成立面向全球贸易的宣发板块，能够精准的快速占领需求蓝海，成为下一个平台业务增长点。', '', 0, 1, 0, NULL, '', '', 1, 1627448146, 1627451809, 0);
INSERT INTO `article` VALUES (83, 5, '原创', '//cdn.dmake.cn/attachment/images/20210831/16303815263.jpg', '我在互联网创业园看到的最像互联网企业的居然是一家奶茶店', NULL, '<p>\n	我在互联网创业园看到的最像互联网企业的居然是一家奶茶店！\n</p>\n<p>\n	在今年上半年，园区食堂老板又又又开了新产业——奶茶店！敢在一个人口数量不过千人的档口开非刚需的奶茶店实属让人佩服！\n</p>\n<p>\n	最让我佩服的是，不到3个月，这家奶茶店不仅活下来了，而且还承包了园区的下午茶时间！\n</p>\n<p>\n	思来想去，这不仅是一家奶茶店，而且还是一家有新零售思维的互联网企业。\n</p>\n<p>\n	<img src=\"//cdn.dmake.cn/attachment/images/20210831/16303815263.jpg\" alt=\"\" /> \n</p>\n<p>\n	互联网企业拉新三件套：免费、团购砍价、优惠券。在这几个月里甜露王把这三个基础操作玩的风声水起！\n</p>\n<p>\n	<strong>免费&nbsp;</strong> \n</p>\n<p>\n	每天中午1点进群抢红包，没有手气最佳者免费1杯奶茶，不限最高额！\n</p>\n<p>\n	点评：我遇到的很多店铺虽然都在用免费，但是每一个活下来的，这家店的操作可以让实体店老板们都参考！\n</p>\n<p>\n	甜露王这家店为什么能用一个最简单的套路活下来有以下几点：\n</p>\n<p>\n	1. 抢红包无门槛，人都是厌恶损失型人格，因此有便宜不赚王八蛋，每天中午至少100个潜在客户被聚拢。第一次老板发红包没有控制成本，大概发了20来块吧，后来发现这个漏洞以后，每天中午总共发的红包不过2块。没有领到怎么办？下次请早！完全不用担心没抢到的抱怨，为什么？因为这是奖品而不是福利！\n</p>\n<p>\n	2. 赠送无上限，切断套路的思维。\n</p>\n<p>\n	点评：扭扭捏捏的套路免费，不如光明正大的白送给力！\n</p>\n<p>\n	抢红包的最佳手气每天只有1个，到店后菜单任选1，如果按照菜单价最高价点单差不多是28元，很多格局小的老板认为自己是损失了28元！\n</p>\n<p>\n	<strong>优惠券</strong> \n</p>\n<p>\n	我还没有去用过，但是在零售行业中的经验预判可能有两种\n</p>\n<ul>\n	<li>\n		直接抵现（有的商品100抵5，这家店直接抵扣10元，拿券最低消费支付不足10元，团队点餐相当于一半人面单，冲击力强，单日限量30张，对于社群促活而言足以）\n	</li>\n	<li>\n		翻倍使用\n	</li>\n</ul>\n<p>\n	至于拼团砍价什么的，社群都有了，抢完红包拼一个又何尝不可呢？\n</p>\n<p>\n	<br />\n</p>', '在今年上半年，园区食堂老板又又又开了新产业——奶茶店！敢在一个人口数量不过千人的档口开非刚需的奶茶店实属让人佩服！', '', 0, 1, 0, NULL, '', '', 1, 1630381609, 1630546222, 0);
INSERT INTO `article` VALUES (84, 1, '原创', '//cdn.dmake.cn/attachment/images/20210831/16303903783.jpg', '40款被Angular官方推荐使用的UI组件', NULL, '<p style=\"text-align:center;\">\n	<img src=\"//cdn.dmake.cn/attachment/images/20210831/16303903783.jpg\" alt=\"\" /> \n</p>\n<p style=\"text-align:left;\">\n	学会用Angular构建应用，项目用时节省1/3，学会用Angular的UI界面组件构建应用，项目用时再节省1/3！\n</p>\n<p>\n	<br />\n<span><a href=\"https://www.jqwidgets.com/angular/\" target=\"_blank\">jQWidgets</a></span><br />\n<span>Angular UI 组件，包括数据网格、树形网格、多维网格、调度器、图表、编辑器和多种功能的组件</span><br />\n<br />\n<span><a href=\"https://www.telerik.com/kendo-angular-ui\" target=\"_blank\">Kendo UI</a></span> \n</p>\n<p>\n	用 TypeScript 编写的 Angular UI 组件的专业级库，其中包括我们的数据网格、TreeView、图表、编辑器、下拉框、DatePickers 等。功能包括支持 AOT 编译、支持高性能的摇树优化、支持本地化和无障碍性。\n</p>\n<p>\n	<br />\n</p>\n<span><a href=\"https://www.grapecity.com/wijmo\" target=\"_blank\">Wijmo</a></span><br />\n<span>具有最完整的 Angular 支持的高性能 UI 控件。 Wijmo 的控件全部用 TypeScript 编写，并且具有零依赖性。 FlexGrid 控件包括完整的声明性标记语言，包括单元格模板。</span><span></span><br />\n<br />\n<span><a href=\"https://js.devexpress.com/Overview/Angular/\" target=\"_blank\">DevExtreme</a></span><br />\n<span>50 多个 UI 组件，包括数据网格、数据透视网格、调度器、图表、编辑器、地图和其他多功能控件，用于为触摸设备和传统台式机创建高响应性的 Web 应用程序。</span><br />\n<p>\n	<br />\n</p>\n<a href=\"https://www.ag-grid.com/best-angular-2-data-grid/\" target=\"_blank\">ag-Grid</a><br />\nag-Grid具有企业风格特性的 Angular 数据网格，例如排序、过滤、自定义渲染、编辑、分组、聚合和透视。<br />\n<br />\n<a href=\"https://alyle-ui.firebaseapp.com/\" target=\"_blank\">Alyle UI</a><br />\n最小化设计，一组用于 Angular 的组件。<br />\n<br />\n<a href=\"https://amexio.tech/\" target=\"_blank\">Amexio - Angular 扩展</a><br />\nAmexio 是一套由 HTML5 和 CSS3 支持的丰富的 Angular 组件，用于响应式 Web 设计和 80 多种内置的 Material Design 主题。 Amexio 有 3 个版本，标准版、企业版和创意版。标准版由基本的 UI 组件组成，其中包括网格、选项卡、表单输入等。企业版由日历、树形选项卡、社交媒体登录（Facebook，GitHub，Twitter 等）组成，而创意版则专注于构建美观的网站。具有 200 多个组件/功能。所有版本都是基于 Apache 2 许可证的开源和免费版本。<br />\n<br />\n<a href=\"https://mdbootstrap.com/docs/b5/angular/\" target=\"_blank\">Angular &amp; Material Design 2.0 for Bootstrap 5</a><br />\nFree components, templates &amp; plugins for the latest Bootstrap 5 styled in accordance with Material Design 2.0 (guidelines introduced in 2020)<br />\n<br />\n<a href=\"https://mdbootstrap.com/docs/angular/\" target=\"_blank\">Angular &amp; Material Design for Bootstrap 4</a><br />\nFree components, templates &amp; plugins for the latest Bootstrap 4 styled in accordance with Material Design (guidelines introduced in 2015)<br />\n<br />\n<a href=\"https://material.angular.io/\" target=\"_blank\">Angular Material</a><br />\nAngular 的 Material Design 组件库<br />\n<br />\n<a href=\"https://www.angular-ui-tools.com/\" target=\"_blank\">Angular UI 工具包</a><br />\nAngular UI Toolkit：115 个专业维护的 UI 组件，包括健壮的网格、图表等等。可免费试用并能更快地构建 Angular 应用。<br />\n<br />\n<a href=\"https://github.com/ghiscoding/Angular-Slickgrid\" target=\"_blank\">Angular-Slickgrid</a><br />\nAngular-SlickGrid 是具有 Bootstrap 3,4 主题的快速、可自定义的 SlickGrid 数据网格库的包装<br />\n<br />\n<a href=\"http://ng.mobile.ant.design/#/docs/introduce/en\" target=\"_blank\">供 Angular 使用的 Ant Design Mobile（ng-zorro-antd-mobile）</a><br />\n一组基于 Ant Design Mobile 和 Angular 的企业级移动 UI 组件<br />\n<br />\n<a href=\"https://ng.ant.design/docs/introduce/en\" target=\"_blank\">供 Angular 使用的 Ant Design（ng-zorro-antd）</a><br />\n一组基于 Ant Design 和 Angular 的企业级 UI 组件<br />\n<br />\n<a href=\"https://jigsaw-zte.gitee.io/\" target=\"_blank\">Awade Jigsaw（中文）</a><br />\nJigsaw 提供了一组基于 Angular 的 Web 组件。它支持中兴通讯大数据产品（ &lt;https://www.zte.com.cn&gt; ）的所有应用程序的开发。<br />\n<br />\n<a href=\"https://github.com/src-zone/material\" target=\"_blank\">Blox Material</a><br />\n轻量级的 Angular Material Design 库，基于 Google 的 Web Material 组件<br />\n<br />\n<a href=\"https://angular.carbondesignsystem.com/\" target=\"_blank\">Carbon Design 的 Angular 组件库</a><br />\nIBM 的 Carbon Design System 的 Angular 实现。<br />\n<br />\n<a href=\"https://vmware.github.io/clarity/\" target=\"_blank\">Clarity Design System</a><br />\n让 UX 准则、HTML/CSS 框架和 Angular 组件一起工作，以创造出非凡的体验<br />\n<br />\n<a href=\"https://www.syncfusion.com/products/angular-js2\" target=\"_blank\">Essential JS 2</a><br />\nEssential JS 2 for Angular 是基于现代 TypeScript 的真正 Angular 组件的集合。它支持预先（AOT）编译和摇树优化。所有组件都是从零开始开发的，从而使其轻巧、响应迅速、模块化且对触控友好。<br />\n<br />\n<a href=\"https://fancygrid.com/docs/getting-started/angular\" target=\"_blank\">FancyGrid</a><br />\n具有图表集成和企业服务器通信功能的 Angular 网格库。<br />\n<br />\n<a href=\"https://www.infragistics.com/products/ignite-ui-angular?utm_source=angular.io&utm_medium=Referral&utm_campaign=Angular\" target=\"_blank\">Ignite UI for Angular</a><br />\nIgnite UI for Angular 是用于构建现代 Web 应用程序的无依赖 Angular 工具包。<br />\n<br />\n<a href=\"https://github.com/tiaguinho/material-community-components\" target=\"_blank\">Material 社区组件</a><br />\n社区开发的一些 Material 组件<br />\n<br />\n<a href=\"https://www.npmjs.com/package/@tabuckner/material-dayjs-adapter\" target=\"_blank\">material-dayjs-adapter</a><br />\n@ angular/material 的 DateAdapter 的 DayJS 实现，其包比相应的 MomentJS 版小。<br />\n<br />\n<a href=\"https://github.com/positive-js/mosaic\" target=\"_blank\">Mosaic - Angular UI 组件</a><br />\n基于 Angular 的正面技术 UI 组件<br />\n<br />\n<a href=\"https://akveo.github.io/nebular/\" target=\"_blank\">Nebular</a><br />\n为你的下一个 Angular 应用程序准备的主题体系、UI 组件、身份验证和安全性。<br />\n<br />\n<a href=\"https://ng-bootstrap.github.io/\" target=\"_blank\">ng-bootstrap</a><br />\nAngular UI Bootstrap 库的 Angular 版本。该库是使用 Bootstrap 4 CSS 框架在 Typescript 中从头开始构建的。<br />\n<br />\n<a href=\"https://ng-lightning.github.io/ng-lightning/#/\" target=\"_blank\">ng-lightning</a><br />\nLightning 设计体系的原生 Angular 组件和指令<br />\n<br />\n<a href=\"https://www.npmjs.com/package/ngx-skeleton-loader\" target=\"_blank\">NGX 骨架装载器</a><br />\nNGX Skeleton Loader 是一种轻巧且 A11Y 友好的解决方案，用于快速加载可自动适配 Angular 应用程序的骨架。<br />\n<br />\n<a href=\"https://valor-software.com/ngx-bootstrap/#/\" target=\"_blank\">ngx-bootstrap</a><br />\nBootstrap 的原生 Angular 指令<br />\n<br />\n<a href=\"https://biig-io.github.io/ngx-smart-modal\" target=\"_blank\">ngx-smart-modal</a><br />\nAngular 智能、轻便、快速的模态框处理程序，可随时随地管理模态框和数据。<br />\n<br />\n<a href=\"https://onsen.io/v2/\" target=\"_blank\">Onsen UI</a><br />\n具有 Angular 和 AngularJS 绑定的混合式移动应用的 UI 组件。<br />\n<br />\n<a href=\"https://www.primefaces.org/primeng/\" target=\"_blank\">Prime Faces</a><br />\nPrimeNG 是 Angular 的富 UI 组件集合<br />\n<br />\n<a href=\"https://www.sencha.com/products/extangular/\" target=\"_blank\">Sencha for Angular</a><br />\n使用 115+ 个预构建的 UI 组件更快地构建现代 Web 应用程序。免费试用并立即下载。<br />\n<br />\n<a href=\"https://sq-ui.github.io/ng-sq-ui/#/\" target=\"_blank\">Simple Quality UI</a><br />\nSimple Quality UI（SQ-UI）是一种灵活且易于自定义的 UI 工具包，旨在以尽可能少的开销提供最大的效率。它严格遵守“供开发人员使用，由开发人员创造”的思想，每个新的特性发行版都包含使用它的开发人员所要求的功能。<br />\n<br />\n<a href=\"https://www.htmlelements.com/angular/\" target=\"_blank\">Smart Web 组件库</a><br />\nAngular 的 Web 组件。一些无依赖的 Angular 组件，用于构建现代且移动友好的 Web 应用程序<br />\n<br />\n<a href=\"https://taiga-ui.dev/\" target=\"_blank\">Taiga UI</a><br />\nTaiga UI is fully-treeshakable Angular UI Kit consisting of multiple base libraries and several add-ons. We have 130+ components, 100+ directives, dozens of tokens, utils and tools. They are easily customizable and well engineered.<br />\n<br />\n<a href=\"https://truly-ui.com/\" target=\"_blank\">Truly UI</a><br />\nTrulyUI 是一个 Angular UI 框架，它是使用世界上最先进的技术为基于 Web 组件的桌面应用程序专门开发的。<br />\n<br />\n<a href=\"https://vaadin.com/elements\" target=\"_blank\">Vaadin</a><br />\n受 Material Design 启发的用于构建出色的 Web 应用程序的 UI 组件库。适用于手机和台式机。', '学会用Angular构建应用，项目用时节省1/3，学会用Angular的UI界面组件构建应用，项目用时再节省1/3！', '', 0, 1, 0, NULL, '', '', 1, 1630391022, 1630391044, 0);
INSERT INTO `article` VALUES (85, 1, '原创', '//cdn.dmake.cn/attachment/images/20210831/16303903783.jpg', '5款被Angular推荐的开发工具（IDE）', NULL, '<p>\n	<img src=\"//cdn.dmake.cn/attachment/images/20210831/16303903783.jpg\" alt=\"\" />\n</p>\n<div>\n	<br />\n</div>\n<p>\n	板砖首推VS Code，因为不要钱，前端特别合适，Java开发者推荐Codemix，也就是文中提到的Webclipse！\n</p>\n<p>\n	<br />\n</p>\n<p>\n	MetaMagic 的 Amexio Canvas 是基于 Web 的拖放式 IDE\n</p>\nAmexio Canvas 是一个拖放式环境，用于创建完全响应式的 Web 和智能设备的 HTML5 / Angular 应用程序。代码将由 Canvas 自动生成并进行热部署以进行实时测试。开箱即用的 50 多种 Material Design Theme 支持。可以将你的代码提交到 GitHub 公共或私有存储库。<br />\n<br />\nWebclipse 的 Angular IDE<br />\n首先为 Angular 构建。对初学者提供交钥匙设置；对专家提供强大的功能。<br />\n<br />\nIntelliJ IDEA<br />\n功能强大且符合人体工程学的 Java * IDE<br />\n<br />\nVisual Studio Code<br />\nVS Code 是用于编辑和调试 Web 应用程序的免费、轻量级工具。<br />\n<br />\nWebStorm<br />\n轻巧但功能强大的 IDE，完全适合通过 Node.js 进行复杂的客户端开发和服务器端开发<br />', '板砖首推VS Code，因为不要钱，前端特别合适，Java开发者推荐Codemix，也就是文中提到的Webclipse！', '', 0, 1, 0, NULL, '', '', 1, 1630393669, NULL, 0);
INSERT INTO `article` VALUES (86, 5, '原创', '//cdn.dmake.cn/attachment/images/20210208/161275085510.png', '年轻人的不讲武德，给多年管理经验的我再上了一课', NULL, '<p>\n	昨日部门招聘来一员工，招聘过程中多次给我的感受是“不真实”：面试过程过于完美、1年多的工作经验反馈过于流畅，让人不禁想起是否是“面霸”。\n</p>\n<p>\n	到岗当日，将工作学习资料分享后，当天下午就提出离职，理由是自己想从事产品运营一岗，而我们的运营体系包含了内容运营、推广运营、产品运营等诸多综合性工作不适应。后来面谈良久，分享了我们的运营体系及相关工作经验，建议再做考虑。让我意想不到的情况发生了：对方将整个谈话过程不仅散布出去还做了大量改编，另外再散播了自己的薪资，再加上一些老员工“拱火”，导致了部分同事提离职！\n</p>\n<p>\n	年轻人的不讲武德，给多年管理经验的我再上了一课！\n</p>\n<p>\n	最近的管理烦心事儿挺多的，一些现象也明显起来：\n</p>\n<ol>\n	<li>\n		新入职员工容易拉起小团队，部分人嘴不把门的，一部分人被当枪使很苦恼\n	</li>\n	<li>\n		相关知识体系过强，会冲击一些知识技能面弱的同事，小心被搅混水\n	</li>\n	<li>\n		追求圆满的人，最容易被跳动起情绪，挑拨的人在后面只看热闹\n	</li>\n	<li>\n		管理注意亲民尺度，在心中有爱的人看来是关心关注，在心怀叵测的人开是无能妥协\n	</li>\n	<li>\n		朋友的情况下请客办招待是小事儿，花自己的钱办部门的招待引起一些比必要的猜测\n	</li>\n</ol>\n<p>\n	事后与其他朋友的复盘，这里有几个事儿我做错了：\n</p>\n<ol>\n	<li>\n		工作岗位只有上下与师徒，没有值得去过多关照的对象；\n	</li>\n	<li>\n		切忌和新人谈论过多，对方的知识体系很可能造成信息扭曲；\n	</li>\n	<li>\n		招聘严格强调薪资保密，民企里面团队动荡的主要原因是薪资倒挂，让一个大喇叭出现必然轩然大波；\n	</li>\n	<li>\n		与下级保持一定的神秘，很多人都想干掉的你时候那么神秘感是不多的保护色之一\n	</li>\n	<li>\n		不要和老板站在对立面（你是员工不是老板），自己不要去承担相关损失\n	</li>\n	<li>\n		和老员工的相处，不要太热情，容易导致“我是他得罪不起”的错觉\n	</li>\n</ol>\n<p>\n	以此谨记，岁月静好，度人前先度自己！\n</p>', '让我意想不到的情况发生了：对方将整个谈话过程不仅散布出去还做了大量改编，另外再散播了自己的薪资，再加上一些老员工“拱火”，导致了部分同事提离职！', '', 0, 1, 0, NULL, '', '', 1, 1630994456, NULL, 0);

-- ----------------------------
-- Table structure for article_attribute
-- ----------------------------
DROP TABLE IF EXISTS `article_attribute`;
CREATE TABLE `article_attribute`  (
  `articleid` int(11) NOT NULL,
  `clickcount` int(11) NOT NULL,
  `commentcount` int(11) NOT NULL,
  `sharecount` int(11) NOT NULL,
  `up` int(11) NOT NULL,
  `down` int(11) NOT NULL,
  PRIMARY KEY (`articleid`) USING BTREE,
  INDEX `IX_clickcount`(`clickcount`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Fixed;

-- ----------------------------
-- Records of article_attribute
-- ----------------------------
INSERT INTO `article_attribute` VALUES (1, 31, 0, 0, 1, 1);
INSERT INTO `article_attribute` VALUES (2, 57, 0, 0, 1, 0);
INSERT INTO `article_attribute` VALUES (9, 136, 0, 0, 0, 0);
INSERT INTO `article_attribute` VALUES (20, 65, 0, 0, 0, 0);
INSERT INTO `article_attribute` VALUES (6, 78, 0, 0, 0, 0);
INSERT INTO `article_attribute` VALUES (4, 70, 0, 0, 0, 0);
INSERT INTO `article_attribute` VALUES (3, 31, 0, 0, 0, 0);
INSERT INTO `article_attribute` VALUES (8, 58, 0, 0, 0, 0);
INSERT INTO `article_attribute` VALUES (5, 67, 0, 0, 1, 0);
INSERT INTO `article_attribute` VALUES (15, 72, 0, 0, 0, 0);
INSERT INTO `article_attribute` VALUES (10, 77, 0, 0, 0, 0);
INSERT INTO `article_attribute` VALUES (14, 70, 0, 0, 0, 0);
INSERT INTO `article_attribute` VALUES (21, 75, 0, 0, 0, 0);
INSERT INTO `article_attribute` VALUES (17, 55, 0, 0, 0, 0);
INSERT INTO `article_attribute` VALUES (19, 65, 0, 0, 0, 0);
INSERT INTO `article_attribute` VALUES (18, 65, 0, 0, 0, 0);
INSERT INTO `article_attribute` VALUES (24, 93, 0, 0, 0, 0);
INSERT INTO `article_attribute` VALUES (29, 170, 0, 0, 0, 0);
INSERT INTO `article_attribute` VALUES (26, 367, 0, 0, 8, 0);
INSERT INTO `article_attribute` VALUES (28, 75, 0, 0, 0, 0);
INSERT INTO `article_attribute` VALUES (13, 79, 0, 0, 0, 0);
INSERT INTO `article_attribute` VALUES (7, 98, 0, 0, 0, 0);
INSERT INTO `article_attribute` VALUES (11, 77, 0, 0, 0, 0);
INSERT INTO `article_attribute` VALUES (27, 85, 0, 0, 0, 0);
INSERT INTO `article_attribute` VALUES (22, 63, 0, 0, 0, 0);
INSERT INTO `article_attribute` VALUES (23, 53, 0, 0, 0, 0);
INSERT INTO `article_attribute` VALUES (12, 69, 0, 0, 0, 0);
INSERT INTO `article_attribute` VALUES (25, 98, 0, 0, 0, 0);
INSERT INTO `article_attribute` VALUES (16, 107, 0, 0, 0, 0);
INSERT INTO `article_attribute` VALUES (30, 68, 0, 0, 0, 0);
INSERT INTO `article_attribute` VALUES (31, 103, 0, 0, 10, 2);
INSERT INTO `article_attribute` VALUES (32, 182, 0, 0, 2, 0);
INSERT INTO `article_attribute` VALUES (33, 22, 0, 0, 0, 0);
INSERT INTO `article_attribute` VALUES (34, 32, 0, 0, 0, 0);
INSERT INTO `article_attribute` VALUES (35, 117, 0, 0, 3, 0);
INSERT INTO `article_attribute` VALUES (36, 60, 0, 0, 5, 2);
INSERT INTO `article_attribute` VALUES (37, 40, 0, 0, 0, 0);
INSERT INTO `article_attribute` VALUES (38, 188, 0, 0, 4, 0);
INSERT INTO `article_attribute` VALUES (39, 43, 0, 0, 0, 0);
INSERT INTO `article_attribute` VALUES (40, 38, 0, 0, 0, 0);
INSERT INTO `article_attribute` VALUES (41, 123, 0, 0, 0, 0);
INSERT INTO `article_attribute` VALUES (42, 93, 0, 0, 3, 1);
INSERT INTO `article_attribute` VALUES (43, 179, 0, 0, 3, 0);
INSERT INTO `article_attribute` VALUES (44, 41, 0, 0, 0, 0);
INSERT INTO `article_attribute` VALUES (45, 10, 0, 0, 0, 0);
INSERT INTO `article_attribute` VALUES (46, 231, 0, 0, 9, 0);
INSERT INTO `article_attribute` VALUES (47, 93, 0, 0, 0, 0);
INSERT INTO `article_attribute` VALUES (48, 52, 0, 0, 1, 0);
INSERT INTO `article_attribute` VALUES (49, 238, 0, 0, 5, 3);
INSERT INTO `article_attribute` VALUES (50, 96, 0, 0, 0, 0);
INSERT INTO `article_attribute` VALUES (51, 15, 0, 0, 0, 0);
INSERT INTO `article_attribute` VALUES (52, 25, 0, 0, 0, 0);
INSERT INTO `article_attribute` VALUES (53, 362, 0, 0, 6, 0);
INSERT INTO `article_attribute` VALUES (54, 146, 0, 0, 3, 0);
INSERT INTO `article_attribute` VALUES (55, 77, 0, 0, 0, 0);
INSERT INTO `article_attribute` VALUES (56, 76, 0, 0, 0, 0);
INSERT INTO `article_attribute` VALUES (57, 147, 0, 0, 1, 2);
INSERT INTO `article_attribute` VALUES (58, 37, 0, 0, 0, 0);
INSERT INTO `article_attribute` VALUES (59, 85, 0, 0, 3, 5);
INSERT INTO `article_attribute` VALUES (60, 41, 0, 0, 0, 0);
INSERT INTO `article_attribute` VALUES (61, 74, 0, 0, 0, 0);
INSERT INTO `article_attribute` VALUES (62, 72, 0, 0, 1, 0);
INSERT INTO `article_attribute` VALUES (63, 16, 0, 0, 0, 0);
INSERT INTO `article_attribute` VALUES (64, 51, 0, 0, 2, 0);
INSERT INTO `article_attribute` VALUES (65, 17, 0, 0, 0, 0);
INSERT INTO `article_attribute` VALUES (66, 8, 0, 0, 0, 0);
INSERT INTO `article_attribute` VALUES (67, 12, 0, 0, 0, 0);
INSERT INTO `article_attribute` VALUES (68, 20, 0, 0, 0, 0);
INSERT INTO `article_attribute` VALUES (69, 35, 0, 0, 0, 0);
INSERT INTO `article_attribute` VALUES (70, 18, 0, 0, 0, 0);
INSERT INTO `article_attribute` VALUES (71, 27, 0, 0, 0, 0);
INSERT INTO `article_attribute` VALUES (72, 55, 0, 0, 1, 0);
INSERT INTO `article_attribute` VALUES (73, 25, 0, 0, 0, 0);
INSERT INTO `article_attribute` VALUES (74, 36, 0, 0, 0, 0);
INSERT INTO `article_attribute` VALUES (75, 23, 0, 0, 0, 0);
INSERT INTO `article_attribute` VALUES (76, 30, 0, 0, 0, 0);
INSERT INTO `article_attribute` VALUES (77, 44, 0, 0, 0, 0);
INSERT INTO `article_attribute` VALUES (78, 29, 0, 0, 0, 0);
INSERT INTO `article_attribute` VALUES (79, 29, 0, 0, 1, 0);
INSERT INTO `article_attribute` VALUES (80, 1, 0, 0, 0, 0);
INSERT INTO `article_attribute` VALUES (81, 16, 0, 0, 0, 0);
INSERT INTO `article_attribute` VALUES (82, 10, 0, 0, 0, 0);
INSERT INTO `article_attribute` VALUES (83, 5, 0, 0, 0, 0);
INSERT INTO `article_attribute` VALUES (84, 5, 0, 0, 0, 0);
INSERT INTO `article_attribute` VALUES (85, 2, 0, 0, 0, 0);
INSERT INTO `article_attribute` VALUES (86, 4, 0, 0, 0, 0);

-- ----------------------------
-- Table structure for article_category
-- ----------------------------
DROP TABLE IF EXISTS `article_category`;
CREATE TABLE `article_category`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `name` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `pid` int(11) NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `keywords` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `description` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` int(11) NOT NULL,
  `sort` int(11) NOT NULL,
  `adminid` int(11) NOT NULL,
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of article_category
-- ----------------------------
INSERT INTO `article_category` VALUES (1, 'developer', '开发思维', 0, '板砖博客开发教程，php快速开发教程', '开发教程', '教会新手如何快速上手php并应用于项目中，使用composer快速完成项目上线。', 1, 3, 0, 1569404518, 1577411712);
INSERT INTO `article_category` VALUES (2, 'operate', '运营分享', 0, '平台运营规范和经验之谈，平台企业化正式运营方法', '平台运营', '运营规范为平台运营人员讲解基于产品、服务和技术的综合运营技巧，初入平台管理人员必备教程。', 1, 1, 0, 1569404610, 1577411544);
INSERT INTO `article_category` VALUES (3, '', '系统说明', 0, '板砖博客模块介绍：文章模块、供应链模块、微信营销模块、B2C模块等', '模块介绍', '板砖博客模块介绍：文章模块、供应链模块、微信营销模块、B2C模块等，前后端对比分析让您更容易上手使用！', 1, 4, 1, 1571895157, 1577411722);
INSERT INTO `article_category` VALUES (4, '', 'IT资讯', 0, 'IT行业最近发生的新鲜事儿', 'IT行业，新鲜事儿', '看IT行业最近发生的新鲜事儿，一起来吐槽吧！', 1, 2, 1, 1575295417, 1584112293);
INSERT INTO `article_category` VALUES (5, '', '100个故事', 0, '板砖职业生涯值得铭记的100个故事', '100个故事', '从职业小白，到创业社畜，再到200人公司管理的步步惊心。', 1, 0, 1, 1577411321, 1577411351);
INSERT INTO `article_category` VALUES (6, '', '书评', 0, '看完一本书写一遍感受', '读书笔记，书评', '开始习惯每周看书的过程，看完一本书写一遍感受，希望10年后的我还能记起TA们是怎样影响自己的成长和进步。', 1, 0, 1, 1619488608, 0);

-- ----------------------------
-- Table structure for article_group
-- ----------------------------
DROP TABLE IF EXISTS `article_group`;
CREATE TABLE `article_group`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `keywords` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `description` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `create_time` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `adminid` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of article_group
-- ----------------------------
INSERT INTO `article_group` VALUES (1, '平台编辑必修课', '平台编辑', '平台编辑必修课，3课时完成职业必杀技。', '', 1569422708, 1, 1, 0);

-- ----------------------------
-- Table structure for article_relations
-- ----------------------------
DROP TABLE IF EXISTS `article_relations`;
CREATE TABLE `article_relations`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `articleid` int(11) NOT NULL,
  `productid` int(11) NOT NULL DEFAULT 0,
  `supplierid` int(11) NOT NULL DEFAULT 0,
  `groupid` int(11) NOT NULL DEFAULT 0 COMMENT '教程系列的id',
  `sourcefrom` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '从哪儿加载的',
  `sort` int(11) NOT NULL DEFAULT 0 COMMENT '有些需要排序的地方使用',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 33 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of article_relations
-- ----------------------------
INSERT INTO `article_relations` VALUES (1, 28, 1, 1, 0, 'article', 0);
INSERT INTO `article_relations` VALUES (2, 30, 1, 1, 0, 'article', 0);
INSERT INTO `article_relations` VALUES (3, 29, 1, 1, 0, 'article', 0);
INSERT INTO `article_relations` VALUES (4, 27, 1, 1, 0, 'article', 0);
INSERT INTO `article_relations` VALUES (5, 26, 1, 1, 0, 'article', 0);
INSERT INTO `article_relations` VALUES (6, 25, 1, 1, 0, 'article', 0);
INSERT INTO `article_relations` VALUES (7, 24, 1, 1, 0, 'article', 0);
INSERT INTO `article_relations` VALUES (8, 23, 1, 1, 0, 'article', 0);
INSERT INTO `article_relations` VALUES (9, 21, 1, 1, 0, 'article', 0);
INSERT INTO `article_relations` VALUES (10, 22, 1, 1, 0, 'article', 0);
INSERT INTO `article_relations` VALUES (11, 20, 1, 1, 0, 'article', 0);
INSERT INTO `article_relations` VALUES (12, 19, 1, 1, 0, 'article', 0);
INSERT INTO `article_relations` VALUES (13, 18, 1, 1, 0, 'article', 0);
INSERT INTO `article_relations` VALUES (14, 16, 1, 1, 0, 'article', 0);
INSERT INTO `article_relations` VALUES (15, 15, 1, 1, 0, 'article', 0);
INSERT INTO `article_relations` VALUES (16, 14, 1, 1, 0, 'article', 0);
INSERT INTO `article_relations` VALUES (17, 13, 1, 1, 0, 'article', 0);
INSERT INTO `article_relations` VALUES (18, 12, 1, 1, 0, 'article', 0);
INSERT INTO `article_relations` VALUES (19, 11, 1, 1, 0, 'article', 0);
INSERT INTO `article_relations` VALUES (20, 10, 1, 1, 0, 'article', 0);
INSERT INTO `article_relations` VALUES (21, 9, 1, 1, 0, 'article', 0);
INSERT INTO `article_relations` VALUES (22, 8, 1, 1, 0, 'article', 0);
INSERT INTO `article_relations` VALUES (23, 7, 1, 1, 0, 'article', 0);
INSERT INTO `article_relations` VALUES (24, 6, 1, 1, 0, 'article', 0);
INSERT INTO `article_relations` VALUES (25, 5, 1, 1, 0, 'article', 0);
INSERT INTO `article_relations` VALUES (26, 4, 1, 1, 0, 'article', 0);
INSERT INTO `article_relations` VALUES (27, 3, 1, 1, 0, 'article', 0);
INSERT INTO `article_relations` VALUES (28, 2, 1, 1, 0, 'article', 0);
INSERT INTO `article_relations` VALUES (29, 1, 1, 1, 0, 'article', 0);
INSERT INTO `article_relations` VALUES (30, 31, 2, 1, 0, 'article', 0);
INSERT INTO `article_relations` VALUES (31, 32, 2, 1, 0, 'article', 0);
INSERT INTO `article_relations` VALUES (32, 86, 1, 1, 0, 'article', 0);

-- ----------------------------
-- Table structure for bbs_attach
-- ----------------------------
DROP TABLE IF EXISTS `bbs_attach`;
CREATE TABLE `bbs_attach`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `authorid` int(11) NOT NULL,
  `source` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `create_time` int(11) NOT NULL,
  `threadid` int(11) NOT NULL,
  `readperm` int(11) NOT NULL COMMENT '下载权限0-5配合会员体系使用',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of bbs_attach
-- ----------------------------

-- ----------------------------
-- Table structure for bbs_forum
-- ----------------------------
DROP TABLE IF EXISTS `bbs_forum`;
CREATE TABLE `bbs_forum`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '版块名称',
  `title` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `keywords` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `detail` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `supplierid` int(11) NOT NULL,
  `imagepath` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  `posts` int(11) NOT NULL COMMENT '帖子数量',
  `threads` int(11) NOT NULL,
  `sort` int(11) NOT NULL,
  `topicid` int(11) NULL DEFAULT NULL,
  `adminid` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of bbs_forum
-- ----------------------------
INSERT INTO `bbs_forum` VALUES (1, '反馈与建议', '博客反馈与建议，技术问题也会在本版块回答', '功能建议', '博客反馈与建议，有关本平台和Thinkphp、Mysql等技术问题也会在本版块回答！', '', 1, 'http://cdn.dmake.cn/attachment/images/20191031/15725114981.jpg', 1570864683, 1572511666, 0, 2, 0, NULL, 1);
INSERT INTO `bbs_forum` VALUES (2, '活动专区', '板砖博客本来是没有什么活动的，但是copy的多了就有了', '活动', '本版块分享近期互联网圈儿的各大活动，如果有需要还是能参与省不少钱的。', '<p>\n	服务器推荐：http://kuaiyun.cn/index.action?pand=t5H_paouvdg=&nbsp; &nbsp; 景安服务器不一定是最好的，但是一定是最便宜的\n</p>\n<p>\n	云存储推荐：https://portal.qiniu.com/signup?code=1hm7kzbx0yr0y&nbsp; 七牛的云存储，一定是性价比最好的\n</p>\n<p>\n	<br />\n</p>', 0, 'http://cdn.dmake.cn/attachment/images/20191031/15725111804.jpg', 1572511404, 0, 0, 0, 1, NULL, 1);
INSERT INTO `bbs_forum` VALUES (3, '更新日志', '板砖博客系统BUG反馈', 'BUG反馈', '如果您对试用了板砖博客系统后发现了任何BUG，请在此板块反馈。', '', 0, 'http://cdn.dmake.cn/attachment/images/20191031/15725117428.jpg', 1572511745, 1574057207, 0, 8, 0, NULL, 1);
INSERT INTO `bbs_forum` VALUES (4, '认证专区', '认证专区', '认证专区', '经过会员认证的朋友们可分享福利！', '', 0, 'http://cdn.dmake.cn/attachment/images/20191031/15725119713.jpg', 1572512004, 0, 0, 0, 0, NULL, 1);

-- ----------------------------
-- Table structure for bbs_integral
-- ----------------------------
DROP TABLE IF EXISTS `bbs_integral`;
CREATE TABLE `bbs_integral`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `threadid` int(11) NOT NULL,
  `postid` int(11) NOT NULL,
  `authorid` int(11) NOT NULL,
  `integral` int(11) NOT NULL,
  `integraltype` int(11) NOT NULL COMMENT '发帖，回帖，获得奖励',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Fixed;

-- ----------------------------
-- Records of bbs_integral
-- ----------------------------

-- ----------------------------
-- Table structure for bbs_post
-- ----------------------------
DROP TABLE IF EXISTS `bbs_post`;
CREATE TABLE `bbs_post`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tid` int(11) NOT NULL,
  `detail` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `author` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `authorid` int(11) NOT NULL,
  `create_time` int(11) NULL DEFAULT NULL,
  `update_time` int(11) NULL DEFAULT NULL,
  `delete_time` int(11) NULL DEFAULT NULL,
  `reward` int(11) NOT NULL DEFAULT 0 COMMENT '主题帖为悬赏，又被发起人采纳的时候记录一下',
  `pid` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of bbs_post
-- ----------------------------
INSERT INTO `bbs_post` VALUES (1, 1, '自己来顶一个·····', 'daneas', 1, 1573116089, NULL, NULL, 0, NULL);
INSERT INTO `bbs_post` VALUES (2, 4, '万年都不想改这个了····', 'daneas', 1, 1574172600, NULL, NULL, 0, NULL);
INSERT INTO `bbs_post` VALUES (3, 9, '联系作者拿最新源码部署即可', 'daneas', 1, 1609826597, NULL, NULL, 0, NULL);
INSERT INTO `bbs_post` VALUES (4, 10, '还可以呀····', 'daneas', 1, 1609993404, NULL, NULL, 0, NULL);
INSERT INTO `bbs_post` VALUES (5, 10, '如果有对这套系统感兴趣的小伙伴留下qq', 'daneas', 1, 1609995349, NULL, NULL, 0, NULL);
INSERT INTO `bbs_post` VALUES (6, 10, '优化在不断推进中', 'daneas', 1, 1609995493, NULL, NULL, 0, NULL);
INSERT INTO `bbs_post` VALUES (7, 11, '补充说明：除了人民币以外的货币结算都过了，为毛不能用CNY结算，这个是平台约束，因此PayPal方式怎么处理这个需求，或许需要站长们在订单阶段自动转换了先。', 'daneas', 1, 1612161020, NULL, NULL, 0, NULL);

-- ----------------------------
-- Table structure for bbs_thread
-- ----------------------------
DROP TABLE IF EXISTS `bbs_thread`;
CREATE TABLE `bbs_thread`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fid` int(11) NOT NULL,
  `title` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `keywords` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `description` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `detail` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `attachfiles` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `imagepath` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `author` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `authorid` int(11) NOT NULL,
  `lastpost` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `lastpostid` int(11) NOT NULL,
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NULL DEFAULT NULL,
  `tag` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `views` int(11) NULL DEFAULT NULL,
  `posts` int(11) NULL DEFAULT NULL COMMENT '回帖数量',
  `recommend_add` int(11) NULL DEFAULT NULL COMMENT '喜欢',
  `recommend_sub` int(11) NULL DEFAULT NULL COMMENT '不喜欢',
  `sort` int(11) NULL DEFAULT NULL,
  `reward` int(11) NULL DEFAULT NULL COMMENT '悬赏',
  `readperm` int(11) NULL DEFAULT NULL COMMENT '阅读权限，注册会员，VIP，用0-5代替，配合平台会员体系来做',
  `favtimes` int(11) NULL DEFAULT NULL COMMENT '收藏次数',
  `toplevel` int(11) NULL DEFAULT NULL COMMENT '置顶范围：0是不置顶，1是版块置顶，2是全局置顶',
  `topendtime` int(11) NULL DEFAULT NULL,
  `questionid` int(11) NULL DEFAULT NULL,
  `type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `productid` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 12 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of bbs_thread
-- ----------------------------
INSERT INTO `bbs_thread` VALUES (1, 1, '必看！板砖博客交流中心说明', NULL, NULL, '<p>板砖博客交流中心最终还是走上了轻论坛的道路——</p><p>不搞复杂的权限配置</p><p>不搞高扩展性的后端能力</p><p>简简单单，发帖-回帖-积分-等级</p><p>给平台用户更多的可拓展想象空间</p><p>板砖博客从来都不是一个博客，也不是一个论坛，而是一个企业/组织运营平台首先</p>', '', '', 'daneas', 1, 'daneas', 1, 1573107323, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, '', 0);
INSERT INTO `bbs_thread` VALUES (2, 3, '后端标准化数据管理更新', NULL, NULL, '<p>1. 本次更新了后端统一数据管理的快速创建、管理，将基础方法移入extend\\com\\fadmin.php文件中，可以减少重复的数据管理操作</p><p>2. 本次更新了后端的form表单提交机制，减少了重复script的书写</p>', '', '', 'daneas', 1, 'daneas', 1, 1574057086, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, '', 0);
INSERT INTO `bbs_thread` VALUES (3, 3, '网站后台UI升级Element1.4升级至Element2.12.0', NULL, NULL, '<p>网站后台UI升级Element1.4升级至Element2.12.0</p>', 'http://cdn.dmake.cn/attachment/canvs/201911/19/1574172308.jpg', 'http://cdn.dmake.cn/attachment/canvs/201911/19/1574172308.jpg', 'daneas', 1, 'daneas', 1, 1574172362, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, '', 0);
INSERT INTO `bbs_thread` VALUES (4, 3, 'uploadCanvs通用接口更新', '', '', '<p>\n	为了方便各种系统的切换，并且不影响上传结果，于是乎uploadCanvs接口采用物理路径上传图片\n</p>\n<p>\n	用户在根目录下定义根目录物理地址，接口即可生效\n</p>\n<p>\n	// \'/www/web/web\';\n</p>\n<p>\n	// \'D:/web\';\n</p>\n<p>\n	define(\'QINIU_DIR\', \'F:/centos/myprograms/public/fourinone/web\');\n</p>', 'http://cdn.dmake.cn/attachment/canvs/201911/19/1574172420.jpg', 'http://cdn.dmake.cn/attachment/canvs/201911/19/1574172420.jpg', 'daneas', 1, 'daneas', 1, 1574172568, 1574238047, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 2, -28800, NULL, '', 0);
INSERT INTO `bbs_thread` VALUES (5, 3, 'TP5+Easywechat实现小程序支付功能代码实例', '', '', '<p>\n	在Easywechat官方文档中，微信公众号网页支付和扫码支付都有代码示例，小程序端的示例缺失，我根据微信官方文档和lavarl的示例，整理出TP5+Easywechat小程序支付功能，以下是小程序端代码：\n</p>\n<pre class=\"prettyprint lang-js\">wxpay: function (e) {\nvar that = this;\nvar data = {\n\"ctl\": \'pay\',\n\"act\": \"min\",\n\"productid\": 1,\n\"licenseid\": 1,\n\"buynum\": 1,\n\"customerid\": 1,\n\"order_amount\": 1,\n\"openid\": wx.getStorageSync(\"openid\"),\n\'hash\': app.globalData.hash\n};\nutil.minRequest(data, function (res) {\nvar payres = res.data.data;\nif (payres.result_code == \"FAIL\") {\nwx.showModal({\ntitle: payres.err_code,\ncontent: payres.err_code_des,\n})\nreturn false;\n}\nvar payconfig = payres.data;\n// 唤起支付\nwx.requestPayment({\ntimeStamp: payconfig[\'timeStamp\'],\nnonceStr: payconfig[\'nonceStr\'],\npackage: payconfig[\'package\'],\nsignType: payconfig[\'signType\'],\npaySign: payconfig[\'paySign\'],\n\'success\': function (res) {\n},\n\'fail\': function (res) {\n},\n\'complete\': function (res) {\n}\n})\n}, \"http://localhost/api/minprogram\");\n},</pre>\n<p>\n	<br />\n</p>\n<p>\n	在网站后端实现支付参数的装配\n</p>\n<p>\n	<br />\n</p>\n<pre class=\"prettyprint lang-php\">$app = Factory::payment($config);\n$res = $app-&gt;order-&gt;unify([\n\'product_id\' =&gt; $order[\'productid\'],\n\'body\' =&gt; $order[\'subject\'], \'out_trade_no\' =&gt; $orderlog[\'out_trade_no\'],  \'total_fee\' =&gt; $order[\'total_fee\']*100,\n\'trade_type\' =&gt; \'JSAPI\',\n\'openid\' =&gt; $data[\'openid\']\n]);\nif (array_key_exists(\'result_code\', $res) &amp;&amp; $res[\'result_code\'] == \'FAIL\') {\n   return getJsonCode($res);\n}\n$prepay_id = $res[\'prepay_id\'];\n$paysign = $app-&gt;jssdk-&gt;sdkConfig($prepay_id);//如果这里是app的配置生成，改为 $app-&gt;jssdk-&gt;appConfig($prepay_id);\nreturn getJsonCode($paysign);</pre>', '', '', 'daneas', 1, 'daneas', 1, 1575439461, 1575439787, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, -28800, NULL, 'tutorial', 0);
INSERT INTO `bbs_thread` VALUES (6, 3, 'CDN资源管理器更迭，独立管理多品牌服务商资源存储', NULL, NULL, '<p style=\"text-align: left;\">1. 建立记录所有资源的系统表，这个表将你存在所有的CDN的资源路径、名称等记录下来，新增groupid即资源分组</p><p style=\"text-align: left;\"><img src=\"http://cdn.dmake.cn/attachment/keditor/image/20191226/20191226022119_75555.png\" alt=\"\"></p><p style=\"text-align: left;\">2. 建立资源的分组表，可以是功能分组，也可以是CDN品牌的分组，这个看自己了，这个分组表就不再赘述了，上图中groupid就是本表的主键。</p><p style=\"text-align: left;\">3. 使用板砖的资源管理工具即可，根据自己的需求改装。</p>', '', '', 'daneas', 1, 'daneas', 1, 1577337113, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, '', 0);
INSERT INTO `bbs_thread` VALUES (7, 1, 'win10子系统ubuntu搭建wdcp环境', NULL, NULL, '<p>关于Win10如何搭建ubuntu子系统我就不多说了，在刚配置完成并且可登录的ubuntu中建设WDCP运行面板我是遇到了不少问题，接下来简要的分享一下：</p><p>1. CMAKE等运行组件的建设：</p><p><b>Q0：需要安装git</b></p><p><b>　　解决方案：</b></p><p><b>#CentOS</b></p><p><b>yum install git&nbsp;</b></p><p><b>#ubuntu</b></p><p><b>apt-get install git</b></p><p>Q1：CMAKE_CXX_COMPILER could be found</p><p>　　具体报错信息如下：</p><p>-- Running cmake version 3.10.2</p><p>-- The CXX compiler identification is unknown</p><p>CMake Error at CMakeLists.txt:116 (PROJECT):</p><p>&nbsp; No CMAKE_CXX_COMPILER could be found.&nbsp;</p><p>&nbsp; Tell CMake where to find the compiler by setting either the environment</p><p>&nbsp; variable \"CXX\" or the CMake cache entry CMAKE_CXX_COMPILER to the full path</p><p>&nbsp; to the compiler, or to the compiler name if it is in the PATH.</p><p>-- Configuring incomplete, errors occurred!</p><p>See also \"/usr/local/src/mysql-5.6.41/CMakeFiles/CMakeOutput.log\".</p><p>See also \"/usr/local/src/mysql-5.6.41/CMakeFiles/CMakeError.log\".</p><p><b>　　解决方式：</b></p><p><b>apt-get install cmake gcc g++</b></p><p><b>　　如果是centos，可以使用yum来安装cmake、gcc、g++</b></p><p><b>yum install cmake gcc g++</b></p><p><br></p><p>Q2：Could NOT find Curses (missing: CURSES_LIBRARY CURSES_INCLUDE_PATH)&nbsp;</p><p>　　具体报错信息如下：</p><p>-- Could NOT find Curses (missing: CURSES_LIBRARY CURSES_INCLUDE_PATH)</p><p>CMake Error at cmake/readline.cmake:85 (MESSAGE):</p><p>&nbsp; Curses library not found.&nbsp; Please install appropriate package,&nbsp;</p><p>&nbsp; &nbsp; &nbsp; remove CMakeCache.txt and rerun cmake.</p><p>&nbsp; &nbsp; On Debian/Ubuntu, package name is libncurses5-dev,</p><p>&nbsp; &nbsp; on Redhat and derivates it is ncurses-devel.</p><p>Call Stack (most recent call first):</p><p>&nbsp; cmake/readline.cmake:128 (FIND_CURSES)</p><p>&nbsp; cmake/readline.cmake:218 (MYSQL_USE_BUNDLED_EDITLINE)</p><p>&nbsp; CMakeLists.txt:448 (MYSQL_CHECK_EDITLINE)</p><p>-- Configuring incomplete, errors occurred!</p><p>See also \"/usr/local/src/mysql-5.6.41/CMakeFiles/CMakeOutput.log\".</p><p>See also \"/usr/local/src/mysql-5.6.41/CMakeFiles/CMakeError.log\".</p><p>　　<b>Ubuntu的解决方案：</b></p><p><b>apt-get install libncurses5-dev</b></p><p><b>　　CentOS的解决方案：</b></p><p><b>yum install ncurses-devel</b></p><p><b>解决openssl库和&nbsp;the HTTP gzip module requires the zlib library</b></p><p><span>sudo apt-get install openssl</span><br></p><p><span>sudo apt-get install libssl-dev</span></p><p>sudo apt-get install&nbsp;libpcre3 libpcre3-dev</p><p>sudo apt-get install openssl libssl-dev</p><p>安装Nginx因为zlib出错的解决方法</p><p><span>sudo apt-get install zlib1g-dev</span></p><p></p>', '', '', 'daneas', 1, 'daneas', 1, 1579671717, 1579671717, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, '', 0);
INSERT INTO `bbs_thread` VALUES (8, 3, '更新缓存机制，新增分模块分功能缓存', NULL, NULL, '<p>【2020.1.18】</p><p>更新系统中dcache功能，新增分模块、分功能缓存，实现系统后台可以根据缓存关键词对部分缓存清空处理。</p>', '', '', 'daneas', 1, 'daneas', 1, 1579683018, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, '', 0);
INSERT INTO `bbs_thread` VALUES (9, 429, 'dmake这个系统怎么安装啊', NULL, NULL, '<p>dmake这个系统怎么安装啊，有没有教程。</p>', '', '', 'lqgeq1', 7, 'daneas', 1, 1593409983, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 'help', 0);
INSERT INTO `bbs_thread` VALUES (10, 3, '2021第一个功能修复：板砖博客完成商城优化，支持支付宝和微信支付，以及查账退款功能', NULL, NULL, '<p>1. 修复了商城零售流程</p><p>2. 改进了地址管理与发票管理的体验性</p><p>3. 支持支付宝和微信支付、查账、退款</p><p>4. 对个人中心做了改进优化</p>', '', '', 'daneas', 1, 'daneas', 1, 1609915765, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, '', 0);
INSERT INTO `bbs_thread` VALUES (11, 3, 'Dmake收银方式新增PayPal渠道，适应全球销售通道', NULL, NULL, '<p>Dmake建站使用的是PayPal官方SDK，目前已经过了沙盒模式测试无误。</p><p>在本平台中的具体展示效果如下图。</p><p>在国内还是推荐使用支付宝和微信支付渠道，在国外PayPal的集成成本比对接Visa等更方便，后续是否增加其他收银方式？看你们的评价了！</p>', '//cdn.dmake.cn/attachment/canvs/202102/01/1612160422.jpg', '//cdn.dmake.cn/attachment/canvs/202102/01/1612160422.jpg', 'daneas', 1, 'daneas', 1, 1612160564, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, '', 0);

-- ----------------------------
-- Table structure for campaign
-- ----------------------------
DROP TABLE IF EXISTS `campaign`;
CREATE TABLE `campaign`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `keywords` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `description` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `imagepath` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `url` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `content` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `starttime` datetime(0) NULL DEFAULT NULL,
  `endtime` datetime(0) NULL DEFAULT NULL,
  `adminid` int(11) NOT NULL,
  `activetype` int(11) NULL DEFAULT NULL,
  `css` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `scripts` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `sort` int(11) NOT NULL,
  `create_time` int(11) NULL DEFAULT NULL,
  `update_time` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of campaign
-- ----------------------------

-- ----------------------------
-- Table structure for comments
-- ----------------------------
DROP TABLE IF EXISTS `comments`;
CREATE TABLE `comments`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `targetid` int(11) NOT NULL,
  `star` int(11) NOT NULL DEFAULT 5,
  `customerid` int(11) NULL DEFAULT NULL,
  `moduletype` int(11) NOT NULL,
  `replyid` int(11) NULL DEFAULT NULL,
  `authorname` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `ip` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `up` int(11) NOT NULL,
  `down` int(11) NOT NULL,
  `Isapproved` tinyint(4) NOT NULL,
  `Isexcellence` tinyint(4) NOT NULL,
  `create_time` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of comments
-- ----------------------------
INSERT INTO `comments` VALUES (1, 31, 5, 1, 3, NULL, '', '我来打第一枪，发第一个言', NULL, 0, 0, 0, 0, 1573109157);
INSERT INTO `comments` VALUES (2, 59, 5, 1, 3, NULL, '', '可以么···············', NULL, 0, 0, 0, 0, 1588746858);

-- ----------------------------
-- Table structure for coupon
-- ----------------------------
DROP TABLE IF EXISTS `coupon`;
CREATE TABLE `coupon`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `code` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '优惠码',
  `amount` int(11) NOT NULL COMMENT '优惠金额',
  `productid` int(11) NOT NULL COMMENT '0是全局折扣，大于1是指定产品优惠',
  `type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'personal,all 个人使用还是都能用',
  `totalnum` int(11) NOT NULL,
  `leftnum` int(11) NOT NULL COMMENT '可以被领取多少次',
  `status` int(11) NOT NULL COMMENT '针对个人有效，1是使用过了',
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  `isused` int(11) NOT NULL,
  `used_time` int(11) NOT NULL COMMENT '最近使用时间',
  `end_time` int(11) NOT NULL COMMENT '有效期',
  `adminid` int(11) NOT NULL COMMENT '创建人的id',
  `pid` int(11) NULL DEFAULT NULL,
  `summary` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '使用说明',
  `pricelimit` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of coupon
-- ----------------------------

-- ----------------------------
-- Table structure for customer
-- ----------------------------
DROP TABLE IF EXISTS `customer`;
CREATE TABLE `customer`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nickname` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `headimage` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `sex` int(1) NULL DEFAULT NULL,
  `mobile` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `realname` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `accountlevel` int(11) NULL DEFAULT NULL,
  `job` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `company` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `fromtype` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `create_time` int(11) NOT NULL,
  `password` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `delete_time` int(11) NULL DEFAULT NULL,
  `update_time` int(11) NULL DEFAULT NULL,
  `integral` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `IX_Customer_Mobile`(`mobile`) USING BTREE,
  INDEX `IX_Customer_Email`(`email`) USING BTREE,
  INDEX `IX_Customer_UserName`(`realname`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 16 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of customer
-- ----------------------------
INSERT INTO `customer` VALUES (1, 'heyuemax@163.com', 'daneas', '/static/images/defaulthead.jpg', 1, '18996069665', '何大大', 4, '', '', '', 1572580065, '8d6dfeb3da1059f6b5922956eba0e7b0', 0, 1650963375, 10);
INSERT INTO `customer` VALUES (2, 'heyuemax@126.com', 'daneas2014', '/static/images/defaulthead.jpg', NULL, '18996069666', NULL, 1, NULL, NULL, '', 1572674696, 'a52ce6502c03e351b1c1d383e68f11c0', 0, NULL, 10);
INSERT INTO `customer` VALUES (4, NULL, '他们叫我大娃', 'http://qzapp.qlogo.cn/qzapp/101813645/BDD03A888951CB7A4A33A5A386344EF1/100', NULL, NULL, NULL, 2, NULL, NULL, '', 1573477812, NULL, 0, 1602230550, 0);
INSERT INTO `customer` VALUES (5, '1002801365@qq.com', 'nathantang1', '/static/images/defaulthead.jpg', NULL, '13208029184', NULL, 1, NULL, NULL, '', 1585694120, 'c48c19206a5ee02e148faece0595fc4f', 0, NULL, 10);
INSERT INTO `customer` VALUES (6, '295880619@qq.com', 'ashaiqing', '/static/images/defaulthead.jpg', NULL, '15628875879', NULL, 1, NULL, NULL, '', 1587967370, '88a92b7c11434f30f297de09eab45782', 0, NULL, 10);
INSERT INTO `customer` VALUES (7, '245424729@qq.com', 'lqgeq1', '/static/images/defaulthead.jpg', NULL, '18885082322', NULL, 1, NULL, NULL, '', 1593409925, '58a8be25c4e3a6b07f70b95962aade89', 0, NULL, 10);
INSERT INTO `customer` VALUES (8, NULL, '坏板砖', 'https://tva1.sinaimg.cn/crop.0.0.180.180.180/7a4501c1jw1e8qgp5bmzyj2050050aa8.jpg?KID=imgbed,tva&Expires=1602498307&ssig=TeNewcViry', NULL, NULL, NULL, 2, NULL, NULL, '', 1602487379, NULL, 0, 1602490363, 0);
INSERT INTO `customer` VALUES (9, '1161516363@qq.com', 'luojie', '/static/images/defaulthead.jpg', NULL, '13883824941', NULL, 1, NULL, NULL, '', 1611800268, 'cf09f161ce344522f3920049118cfd72', 0, NULL, 10);
INSERT INTO `customer` VALUES (10, NULL, '福', 'http://qzapp.qlogo.cn/qzapp/101813645/D8319D064456F4FE8DEC146B8965234E/100', NULL, NULL, NULL, 2, NULL, NULL, '', 1617971028, NULL, 0, 1617971028, 0);
INSERT INTO `customer` VALUES (11, '473731589@qq.com', 'JH', '/static/images/defaulthead.jpg', NULL, '18067236339', NULL, 1, NULL, NULL, '', 1627460557, '48c966e34789d4fddea602dbcc03dc6a', 0, NULL, 10);
INSERT INTO `customer` VALUES (12, '17111114444@163.com', 'jufeizhu', '/static/images/defaulthead.jpg', NULL, '17111114444', NULL, 1, NULL, NULL, '', 1628405819, '1ac1f0897bd03c411ed764d8177b80a7', 0, NULL, 10);
INSERT INTO `customer` VALUES (13, NULL, '钢筋平法客服', 'http://qzapp.qlogo.cn/qzapp/101813645/B7C416E6A51DE1A346D81D3DA6E914FD/100', NULL, NULL, NULL, 2, NULL, NULL, '', 1630576789, NULL, 0, 1630577517, 0);
INSERT INTO `customer` VALUES (14, '191766302@qq.com', '闲闲', '/static/images/defaulthead.jpg', NULL, '18660163237', NULL, 1, NULL, NULL, '', 1630577456, '1a7cfc7619e8e785ef518b5451fbb0ea', 0, NULL, 10);
INSERT INTO `customer` VALUES (15, '191766301@qq.com', 'xx', '/static/images/defaulthead.jpg', NULL, '18660163236', NULL, 1, NULL, NULL, '', 1630634713, '41c8006f6dcc0d8066f15b7ee12c1b15', 0, NULL, 10);

-- ----------------------------
-- Table structure for customer_address
-- ----------------------------
DROP TABLE IF EXISTS `customer_address`;
CREATE TABLE `customer_address`  (
  `customerid` int(11) NOT NULL,
  `contactphone` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `qq` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `wechat` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `zip` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `country` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `city` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `state` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `streetaddress` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`customerid`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of customer_address
-- ----------------------------
INSERT INTO `customer_address` VALUES (1, NULL, '', '', '', '重庆市', '重庆市市辖区', '沙坪坝区', '重庆市重庆市市辖区沙坪坝区');

-- ----------------------------
-- Table structure for customer_attention
-- ----------------------------
DROP TABLE IF EXISTS `customer_attention`;
CREATE TABLE `customer_attention`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customerid` int(11) NOT NULL,
  `attentioncustomerid` int(11) NOT NULL,
  `attentiondate` datetime(0) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Fixed;

-- ----------------------------
-- Records of customer_attention
-- ----------------------------

-- ----------------------------
-- Table structure for customer_attribute
-- ----------------------------
DROP TABLE IF EXISTS `customer_attribute`;
CREATE TABLE `customer_attribute`  (
  `customerid` int(11) NOT NULL,
  `create_time` int(11) NULL DEFAULT NULL,
  `ip` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `update_time` int(11) NULL DEFAULT NULL,
  `update_ip` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `logintime` int(11) NOT NULL,
  `emailvalitime` int(11) NULL DEFAULT NULL,
  `mobilevalitime` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`customerid`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of customer_attribute
-- ----------------------------
INSERT INTO `customer_attribute` VALUES (1, 1572580065, NULL, 1572580065, '', 1, 1612339500, 1612339840);
INSERT INTO `customer_attribute` VALUES (2, 1572674173, NULL, 1572674173, '', 1, 0, 0);
INSERT INTO `customer_attribute` VALUES (3, 1572674696, NULL, 1572674696, '', 1, 0, 0);
INSERT INTO `customer_attribute` VALUES (5, 1585694120, NULL, 1585694120, '', 1, 0, 0);
INSERT INTO `customer_attribute` VALUES (6, 1587967370, NULL, 1587967370, '', 1, 0, 0);
INSERT INTO `customer_attribute` VALUES (7, 1593409925, NULL, 1593409925, '', 1, 0, 0);
INSERT INTO `customer_attribute` VALUES (9, 1611800268, NULL, 1611800268, '', 1, 0, 0);
INSERT INTO `customer_attribute` VALUES (11, 1627460557, NULL, 1627460557, '', 1, 0, 0);
INSERT INTO `customer_attribute` VALUES (12, 1628405819, NULL, 1628405819, '', 1, 0, 0);
INSERT INTO `customer_attribute` VALUES (14, 1630577456, NULL, 1630577456, '', 1, 0, 0);
INSERT INTO `customer_attribute` VALUES (15, 1630634713, NULL, 1630634713, '', 1, 0, 0);

-- ----------------------------
-- Table structure for customer_coupon
-- ----------------------------
DROP TABLE IF EXISTS `customer_coupon`;
CREATE TABLE `customer_coupon`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customerid` int(11) NULL DEFAULT NULL,
  `couponid` int(11) NOT NULL COMMENT '优惠券编号',
  `adminid` int(11) NULL DEFAULT NULL COMMENT '发券人',
  `create_time` int(11) NULL DEFAULT NULL,
  `update_time` int(11) NULL DEFAULT NULL,
  `delete_time` int(11) NULL DEFAULT NULL,
  `orderid` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of customer_coupon
-- ----------------------------

-- ----------------------------
-- Table structure for customer_integral
-- ----------------------------
DROP TABLE IF EXISTS `customer_integral`;
CREATE TABLE `customer_integral`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `customerid` int(11) NOT NULL,
  `integral` int(11) NOT NULL,
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of customer_integral
-- ----------------------------

-- ----------------------------
-- Table structure for customer_integrallog
-- ----------------------------
DROP TABLE IF EXISTS `customer_integrallog`;
CREATE TABLE `customer_integrallog`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customerid` int(11) NOT NULL,
  `integral` int(11) NOT NULL,
  `summary` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `create_time` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of customer_integrallog
-- ----------------------------

-- ----------------------------
-- Table structure for customer_invoice
-- ----------------------------
DROP TABLE IF EXISTS `customer_invoice`;
CREATE TABLE `customer_invoice`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customerid` int(11) NOT NULL DEFAULT 0,
  `invoice_type` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '个人',
  `invoice_code` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `invoice_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `invoice_tel` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `invoice_address` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `invoice_bank` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `invoice_bankno` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `invoice_remark` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `contact_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `contact_address` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `contact_mobile` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `contact_email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `contact_zip` varchar(6) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '000000',
  `create_time` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of customer_invoice
-- ----------------------------
INSERT INTO `customer_invoice` VALUES (1, 1, '0', '', '', '', '', '', '', '', '何跃', '重庆沙坪坝区XXXXXX号', '18996069665', '', '400000', 1609318311);

-- ----------------------------
-- Table structure for customer_level
-- ----------------------------
DROP TABLE IF EXISTS `customer_level`;
CREATE TABLE `customer_level`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户分组主键',
  `adminid` int(11) NOT NULL,
  `groupname` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '分组名字',
  `grouptype` tinyint(4) NULL DEFAULT NULL COMMENT '分组类型1新注册2已完善3已购买',
  `create_time` int(11) NULL DEFAULT NULL COMMENT '创建时间',
  `status` tinyint(4) NULL DEFAULT NULL COMMENT '状态值1开启0禁用(默认开启)',
  `delete_time` int(11) NULL DEFAULT NULL COMMENT '删除时间',
  `update_time` int(11) NULL DEFAULT NULL COMMENT '更新时间',
  `group_explain` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '用户组说明',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户分组表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of customer_level
-- ----------------------------
INSERT INTO `customer_level` VALUES (1, 1, '注册会员', NULL, 1572677658, 1, NULL, NULL, '享受部分开放权限，例如文章阅读、购买等');

-- ----------------------------
-- Table structure for customer_message
-- ----------------------------
DROP TABLE IF EXISTS `customer_message`;
CREATE TABLE `customer_message`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `targetid` int(11) NULL DEFAULT NULL,
  `type` int(11) NULL DEFAULT NULL,
  `fromid` int(11) NULL DEFAULT NULL,
  `fromtype` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `toid` int(11) NULL DEFAULT NULL,
  `totype` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `create_time` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of customer_message
-- ----------------------------

-- ----------------------------
-- Table structure for customer_notice
-- ----------------------------
DROP TABLE IF EXISTS `customer_notice`;
CREATE TABLE `customer_notice`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customerid` int(11) NOT NULL,
  `noticetype` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `title` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `create_time` int(11) NULL DEFAULT NULL,
  `state` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of customer_notice
-- ----------------------------

-- ----------------------------
-- Table structure for customer_openidoauth
-- ----------------------------
DROP TABLE IF EXISTS `customer_openidoauth`;
CREATE TABLE `customer_openidoauth`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customerid` int(11) NOT NULL,
  `oauthopenid` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `accesstoken` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `oauthtype` int(11) NULL DEFAULT NULL,
  `oauthstate` tinyint(4) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of customer_openidoauth
-- ----------------------------
INSERT INTO `customer_openidoauth` VALUES (1, 4, 'BDD03A888951CB7A4A33A5A386344EF1', '', 0, NULL);
INSERT INTO `customer_openidoauth` VALUES (2, 8, '2051342785', '[object Object]', 0, NULL);
INSERT INTO `customer_openidoauth` VALUES (3, 10, 'D8319D064456F4FE8DEC146B8965234E', '', 0, NULL);
INSERT INTO `customer_openidoauth` VALUES (4, 13, 'B7C416E6A51DE1A346D81D3DA6E914FD', '', 0, NULL);

-- ----------------------------
-- Table structure for customer_preference
-- ----------------------------
DROP TABLE IF EXISTS `customer_preference`;
CREATE TABLE `customer_preference`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customerid` int(11) NOT NULL,
  `objectid` int(11) NOT NULL,
  `objectperferrence` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `obectname` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `preferrencetype` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `issubscribe` tinyint(4) NOT NULL,
  `create_time` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of customer_preference
-- ----------------------------

-- ----------------------------
-- Table structure for customer_validate
-- ----------------------------
DROP TABLE IF EXISTS `customer_validate`;
CREATE TABLE `customer_validate`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `valitype` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `valiaccount` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `code` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_time` int(11) NULL DEFAULT NULL,
  `deadline_time` int(11) NULL DEFAULT NULL,
  `valistatus` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of customer_validate
-- ----------------------------
INSERT INTO `customer_validate` VALUES (1, 'email', 'heyuemax@163.com', '887731', 1612338149, 1612339949, 0);
INSERT INTO `customer_validate` VALUES (2, 'email', 'heyuemax@163.com', '899795', 1612338459, 1612340259, 0);
INSERT INTO `customer_validate` VALUES (3, 'email', 'heyuemax@163.com', '310546', 1612338589, 1612340389, 0);
INSERT INTO `customer_validate` VALUES (4, 'email', 'heyuemax@163.com', '779220', 1612338652, 1612340452, 0);
INSERT INTO `customer_validate` VALUES (5, 'email', 'heyuemax@163.com', '185059', 1612338988, 1612340788, 0);
INSERT INTO `customer_validate` VALUES (6, 'phone', '18996069665', '324554', 1612339786, 1612341586, 0);

-- ----------------------------
-- Table structure for hooks
-- ----------------------------
DROP TABLE IF EXISTS `hooks`;
CREATE TABLE `hooks`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '钩子名称',
  `description` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '描述',
  `type` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '类型',
  `update_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '更新时间',
  `addons` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '钩子挂载的插件 \'，\'分割',
  `status` tinyint(2) NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `name`(`name`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 22 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of hooks
-- ----------------------------
INSERT INTO `hooks` VALUES (21, 'demo', 'demo钩子', 1, 1384481614, 'test', 1);

-- ----------------------------
-- Table structure for jobs
-- ----------------------------
DROP TABLE IF EXISTS `jobs`;
CREATE TABLE `jobs`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `queue` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `payload` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `attempts` tinyint(3) UNSIGNED NOT NULL,
  `reserved` tinyint(3) UNSIGNED NOT NULL,
  `reserved_at` int(10) UNSIGNED NULL DEFAULT NULL,
  `available_at` int(10) UNSIGNED NOT NULL,
  `created_at` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of jobs
-- ----------------------------

-- ----------------------------
-- Table structure for mall_salepr
-- ----------------------------
DROP TABLE IF EXISTS `mall_salepr`;
CREATE TABLE `mall_salepr`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `masterid` int(11) NOT NULL COMMENT '主产品',
  `relationid` int(11) NOT NULL COMMENT '副产品',
  `discount` double NOT NULL COMMENT '折扣价格',
  `status` int(11) NOT NULL DEFAULT 1 COMMENT '是否启用',
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  `adminid` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '关联销售' ROW_FORMAT = Fixed;

-- ----------------------------
-- Records of mall_salepr
-- ----------------------------

-- ----------------------------
-- Table structure for op_announcement
-- ----------------------------
DROP TABLE IF EXISTS `op_announcement`;
CREATE TABLE `op_announcement`  (
  `id` int(11) NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `imagepath` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `activetime` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `button` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `states` tinyint(4) NULL DEFAULT NULL,
  `sort` tinyint(4) NULL DEFAULT NULL,
  `create_time` int(11) NULL DEFAULT NULL,
  `update_time` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of op_announcement
-- ----------------------------

-- ----------------------------
-- Table structure for op_appuser
-- ----------------------------
DROP TABLE IF EXISTS `op_appuser`;
CREATE TABLE `op_appuser`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gh_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `openid` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nickname` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `headimgurl` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `sex` int(11) NULL DEFAULT NULL,
  `province` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `city` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `unionid` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `data` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `subscribe_time` int(11) NULL DEFAULT NULL,
  `qr_scene_str` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `qr_scene` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of op_appuser
-- ----------------------------

-- ----------------------------
-- Table structure for op_calendarsign
-- ----------------------------
DROP TABLE IF EXISTS `op_calendarsign`;
CREATE TABLE `op_calendarsign`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customerid` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `day` int(11) NOT NULL,
  `integral` int(11) NOT NULL,
  `create_time` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Fixed;

-- ----------------------------
-- Records of op_calendarsign
-- ----------------------------

-- ----------------------------
-- Table structure for op_coupon
-- ----------------------------
DROP TABLE IF EXISTS `op_coupon`;
CREATE TABLE `op_coupon`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `activityid` int(11) NOT NULL,
  `customerid` int(11) NOT NULL,
  `couponpid` int(11) NOT NULL,
  `couponcode` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `create_time` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of op_coupon
-- ----------------------------

-- ----------------------------
-- Table structure for op_findme
-- ----------------------------
DROP TABLE IF EXISTS `op_findme`;
CREATE TABLE `op_findme`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nickname` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '展示名',
  `contact` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '自定义联系方法',
  `latitude` double NOT NULL,
  `longitude` double NOT NULL,
  `openid` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `create_time` int(11) NOT NULL COMMENT '小程序id',
  `update_time` int(11) NOT NULL,
  `headimg` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `city` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `address` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of op_findme
-- ----------------------------
INSERT INTO `op_findme` VALUES (4, '何大大', '', 29.508499, 106.457762, 'o1TW55Zjlg9iNeHrJdXKraqexdH8', 0, 1610422470, 'https://wx.qlogo.cn/mmopen/vi_32/IMSzKuf3FGyTfIItG7urJIImD0MmQbEScUbwuhpufVNe1iblF1YMc8WB2WGcWd1wlpjWJBAWgyVvl9OegwmCFxQ/132', '', '');
INSERT INTO `op_findme` VALUES (5, '锅烟煤', '', 29.58207, 106.5914, 'o1TW55aNDbBIZIeKe2tcwhvRX6PE', 1578467510, 1578471312, 'https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTL3I4hjPkm7b81P7cuTcgKTIYVRhNKFsUlRFTnVvy0vZ2gPtZibWY2ibVK6m1Ks5vLZLxJDHSYwuU8Q/132', '', '');

-- ----------------------------
-- Table structure for op_findme_barrage
-- ----------------------------
DROP TABLE IF EXISTS `op_findme_barrage`;
CREATE TABLE `op_findme_barrage`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `openid` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `detail` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `create_time` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 51 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of op_findme_barrage
-- ----------------------------
INSERT INTO `op_findme_barrage` VALUES (17, 'o1TW55Zjlg9iNeHrJdXKraqexdH8', '哈哈哈', 1578376671);
INSERT INTO `op_findme_barrage` VALUES (16, 'o1TW55Zjlg9iNeHrJdXKraqexdH8', '阿斯蒂芬', 1577804293);
INSERT INTO `op_findme_barrage` VALUES (18, 'o1TW55Zjlg9iNeHrJdXKraqexdH8', '啊啊啊', 1578376773);
INSERT INTO `op_findme_barrage` VALUES (20, 'o1TW55Zjlg9iNeHrJdXKraqexdH8', '我在这里啊', 1578377086);
INSERT INTO `op_findme_barrage` VALUES (46, 'o1TW55Zjlg9iNeHrJdXKraqexdH8', '刚刚好', 1578542545);
INSERT INTO `op_findme_barrage` VALUES (45, 'o1TW55Zjlg9iNeHrJdXKraqexdH8', '吐槽的话放在右边', 1578472561);
INSERT INTO `op_findme_barrage` VALUES (44, 'o1TW55Zjlg9iNeHrJdXKraqexdH8', '这是干啥的', 1578472416);
INSERT INTO `op_findme_barrage` VALUES (43, 'o1TW55Zjlg9iNeHrJdXKraqexdH8', '大家都再玩儿个啥', 1578471709);
INSERT INTO `op_findme_barrage` VALUES (42, 'o1TW55aNDbBIZIeKe2tcwhvRX6PE', '不行不行', 1578470958);
INSERT INTO `op_findme_barrage` VALUES (41, 'o1TW55aNDbBIZIeKe2tcwhvRX6PE', 'asdf', 1578469595);
INSERT INTO `op_findme_barrage` VALUES (40, '', 'adf', 1578469497);
INSERT INTO `op_findme_barrage` VALUES (39, 'o1TW55aNDbBIZIeKe2tcwhvRX6PE', '呵呵', 1578467549);
INSERT INTO `op_findme_barrage` VALUES (38, 'o1TW55Zjlg9iNeHrJdXKraqexdH8', 'laile yo', 1578455769);
INSERT INTO `op_findme_barrage` VALUES (34, 'o1TW55Zjlg9iNeHrJdXKraqexdH8', '再来一条', 1578381599);
INSERT INTO `op_findme_barrage` VALUES (37, 'o1TW55Zjlg9iNeHrJdXKraqexdH8', '突发吐槽', 1578455667);
INSERT INTO `op_findme_barrage` VALUES (47, 'o1TW55Zjlg9iNeHrJdXKraqexdH8', '啊·······还有没有啊·······', 1581045702);
INSERT INTO `op_findme_barrage` VALUES (48, 'o1TW55Zjlg9iNeHrJdXKraqexdH8', '来一个吧', 1586422253);
INSERT INTO `op_findme_barrage` VALUES (49, 'o1TW55Zjlg9iNeHrJdXKraqexdH8', 'adsf', 1589349697);
INSERT INTO `op_findme_barrage` VALUES (50, 'o1TW55Zjlg9iNeHrJdXKraqexdH8', '我在二郎', 1610422431);

-- ----------------------------
-- Table structure for op_findme_info
-- ----------------------------
DROP TABLE IF EXISTS `op_findme_info`;
CREATE TABLE `op_findme_info`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `openid` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `slogan` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `duty` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of op_findme_info
-- ----------------------------

-- ----------------------------
-- Table structure for op_form
-- ----------------------------
DROP TABLE IF EXISTS `op_form`;
CREATE TABLE `op_form`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sendmail` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '通知邮件接收人',
  `subject` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表单主题',
  `posturl` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '填写的地址',
  `contact` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '联系人',
  `phone` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `mailjson` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '由于页面设置内容不一样，此处直接保存完整信息即可',
  `create_time` int(11) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of op_form
-- ----------------------------

-- ----------------------------
-- Table structure for op_gantt
-- ----------------------------
DROP TABLE IF EXISTS `op_gantt`;
CREATE TABLE `op_gantt`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `objects` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'from,to,desc,label,customClas',
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of op_gantt
-- ----------------------------
INSERT INTO `op_gantt` VALUES (1, 1, '甘特图上线流程', '平台人员甘特图', '[{label:\'数据构成\',desc:\'组装出demo\',from:\'1589155200000\',to:\'1589328000000\',customClass:\'ganttGreen\'},{label:\'数据保存\',desc:\'保存日程信息\',from:\'1589500800000\',to:\'1589760000000\',customClass:\'ganttGray\'},]', 1589178304, 1589189597);
INSERT INTO `op_gantt` VALUES (2, 1, '智能制造收尾', '修改图片文字并上线', '[{label:\'内容准备\',desc:\'搜集相关内容\',from:\'1589155200000\',to:\'1589241600000\',customClass:\'ganttOrange\'},{label:\'代码修改\',desc:\'上线修改结果\',from:\'1589328000000\',to:\'1589500800000\',customClass:\'ganttOrange\'},]', 1589179231, 1589189607);
INSERT INTO `op_gantt` VALUES (3, 1, 'DMAKE正式发布', 'gitee公开master', '[{label:\'权力修补bug\',desc:\'找到这个bug\',from:\'1589155200000\',to:\'1592438400000\',customClass:\'ganttGray\'},]', 1589189310, 1589189538);

-- ----------------------------
-- Table structure for op_mailsender
-- ----------------------------
DROP TABLE IF EXISTS `op_mailsender`;
CREATE TABLE `op_mailsender`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sendto` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `subject` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `body` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `create_time` int(11) NULL DEFAULT NULL,
  `send_time` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of op_mailsender
-- ----------------------------

-- ----------------------------
-- Table structure for op_subscribe
-- ----------------------------
DROP TABLE IF EXISTS `op_subscribe`;
CREATE TABLE `op_subscribe`  (
  `id` int(11) NOT NULL,
  `customerid` int(11) NULL DEFAULT NULL,
  `datatype` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `dataid` int(11) NULL DEFAULT NULL,
  `create_time` int(11) NULL DEFAULT NULL,
  `form_id` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of op_subscribe
-- ----------------------------

-- ----------------------------
-- Table structure for op_subscribe_msg
-- ----------------------------
DROP TABLE IF EXISTS `op_subscribe_msg`;
CREATE TABLE `op_subscribe_msg`  (
  `id` int(11) NOT NULL,
  `customerid` int(11) NULL DEFAULT NULL,
  `openid` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `title` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `detail` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `datatype` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `dataid` int(11) NULL DEFAULT NULL,
  `create_time` int(11) NULL DEFAULT NULL,
  `isview` int(11) NULL DEFAULT NULL,
  `issend` int(11) NULL DEFAULT NULL,
  `subjectid` int(11) NULL DEFAULT NULL,
  `form_id` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of op_subscribe_msg
-- ----------------------------

-- ----------------------------
-- Table structure for op_subscribe_subject
-- ----------------------------
DROP TABLE IF EXISTS `op_subscribe_subject`;
CREATE TABLE `op_subscribe_subject`  (
  `id` int(11) NOT NULL,
  `datatype` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `dataid` int(11) NULL DEFAULT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `members` int(11) NULL DEFAULT NULL,
  `create_time` int(11) NULL DEFAULT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of op_subscribe_subject
-- ----------------------------

-- ----------------------------
-- Table structure for order_cart
-- ----------------------------
DROP TABLE IF EXISTS `order_cart`;
CREATE TABLE `order_cart`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `productid` int(11) NOT NULL,
  `licenseid` int(11) NOT NULL,
  `number` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  `buycheck` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '购物车' ROW_FORMAT = Fixed;

-- ----------------------------
-- Records of order_cart
-- ----------------------------

-- ----------------------------
-- Table structure for order_history
-- ----------------------------
DROP TABLE IF EXISTS `order_history`;
CREATE TABLE `order_history`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `orderid` int(11) NOT NULL,
  `orderno` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `changedstate` varchar(12) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `remark` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `create_time` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_history
-- ----------------------------
INSERT INTO `order_history` VALUES (1, 1, 'PRODUCT20201230165256', 'WP', '创建了一条订单，等待支付,订单详情：授权ID[2]收费-商业版', 1609318376);
INSERT INTO `order_history` VALUES (2, 2, 'P20210105165149', 'WP', '创建了一条订单，等待支付,订单详情：授权ID[2]收费-商业版', 1609836709);
INSERT INTO `order_history` VALUES (3, 3, 'P20210106102754', 'WP', '创建了一条订单，等待支付,订单详情：授权ID[2]收费-商业版', 1609900074);
INSERT INTO `order_history` VALUES (4, 4, 'P20210106110106', 'WP', '创建了一条订单，等待支付,订单详情：授权ID[2]收费-商业版', 1609902066);
INSERT INTO `order_history` VALUES (5, 5, 'P20210201093414', 'WP', '创建了一条订单，等待支付,订单详情：授权ID[2]收费-商业版', 1612143254);
INSERT INTO `order_history` VALUES (6, 6, 'P20210201131803', 'WP', '创建了一条订单，等待支付,订单详情：授权ID[3]收费-跨境版', 1612156684);
INSERT INTO `order_history` VALUES (7, 7, 'P20210201214247', 'WP', '创建了一条订单，等待支付,订单详情：授权ID[3]收费-跨境版', 1612186967);

-- ----------------------------
-- Table structure for order_item
-- ----------------------------
DROP TABLE IF EXISTS `order_item`;
CREATE TABLE `order_item`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `orderid` int(11) NOT NULL,
  `productid` int(11) NOT NULL,
  `licenseid` int(11) NOT NULL,
  `cost` decimal(18, 2) NOT NULL,
  `qty` int(11) NOT NULL,
  `orderyear` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Fixed;

-- ----------------------------
-- Records of order_item
-- ----------------------------
INSERT INTO `order_item` VALUES (1, 1, 1, 2, 10000.00, 1, 1);
INSERT INTO `order_item` VALUES (2, 2, 1, 2, 10000.00, 1, 1);
INSERT INTO `order_item` VALUES (3, 3, 1, 2, 10000.00, 9, 1);
INSERT INTO `order_item` VALUES (4, 4, 1, 2, 10000.00, 9, 1);
INSERT INTO `order_item` VALUES (5, 5, 1, 2, 10000.00, 1, 1);
INSERT INTO `order_item` VALUES (6, 6, 1, 3, 647.00, 1, 1);
INSERT INTO `order_item` VALUES (7, 7, 1, 3, 647.00, 1, 1);

-- ----------------------------
-- Table structure for orders
-- ----------------------------
DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `orderno` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `totalamount` decimal(18, 2) NOT NULL,
  `paymenttype` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `status` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `createby` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `customerid` int(11) NOT NULL,
  `customer_name` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `customer_mobil` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `customer_email` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `customer_address` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `customer_zip` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `customer_company` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `note` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `paymentid` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '支付平台的预生成的业务id',
  `invoice` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `invoice_head` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `invoice_number` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `invoice_email` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `invoice_remark` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `discount` decimal(18, 2) NULL DEFAULT NULL,
  `discountcode` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_time` int(11) NOT NULL,
  `trade_no` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `pay_time` int(11) NOT NULL,
  `cancel_time` int(11) NOT NULL,
  `productid` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `remark` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `subject` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `IX_ProductOrder_OrderNo`(`orderno`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of orders
-- ----------------------------
INSERT INTO `orders` VALUES (1, 'PRODUCT20201230165256', 10000.00, 'alipay', 'WP', NULL, 1, '何跃', '18996069665', NULL, '重庆', '400000', NULL, '', '', NULL, '', '', '', '', 0.00, '', 1609318376, NULL, 0, 1609318376, '1', '{\"singleprice\":10000,\"buynum\":\"1\",\"total_amount\":10000,\"rate_amount\":0,\"order_amount\":10000,\"discount\":0,\"discountcode\":\"\"}', '板砖博客系统PHP版');
INSERT INTO `orders` VALUES (2, 'P20210105165149', 10000.00, 'alipay', 'WP', NULL, 1, '何跃', '18996069665', NULL, '重庆', '400000', NULL, '', '', NULL, '', '', '', '', 0.00, '', 1609836709, NULL, 0, 1609836709, '1', '{\"singleprice\":10000,\"buynum\":\"1\",\"total_amount\":10000,\"rate_amount\":0,\"order_amount\":10000,\"discount\":0,\"discountcode\":\"\"}', '板砖博客系统PHP版');
INSERT INTO `orders` VALUES (3, 'P20210106102754', 90000.00, 'alipay', 'WP', NULL, 1, '何跃', '18996069665', NULL, '重庆', '400000', NULL, '', '', NULL, '', '', '', '', 0.00, '', 1609900074, NULL, 0, 1609900074, '1', '{\"singleprice\":10000,\"buynum\":\"9\",\"total_amount\":90000,\"rate_amount\":0,\"order_amount\":90000,\"discount\":0,\"discountcode\":\"\"}', '板砖博客系统PHP版');
INSERT INTO `orders` VALUES (4, 'P20210106110106', 90000.00, 'alipay', 'WP', NULL, 1, '何跃', '18996069665', NULL, '重庆', '400000', NULL, '', '', NULL, '', '', '', '', 0.00, '', 1609902066, NULL, 0, 1609902066, '1', '{\"singleprice\":10000,\"buynum\":\"9\",\"total_amount\":90000,\"rate_amount\":0,\"order_amount\":90000,\"discount\":0,\"discountcode\":\"\"}', '板砖博客系统PHP版');
INSERT INTO `orders` VALUES (5, 'P20210201093414', 10000.00, 'PayPal', 'WP', NULL, 1, '何跃', '18996069665', NULL, 'cqw2d63', '400000', NULL, '', '', NULL, '', '', '', '', 0.00, '', 1612143254, NULL, 0, 1612143254, '1', '{\"singleprice\":10000,\"buynum\":\"1\",\"total_amount\":10000,\"rate_amount\":0,\"order_amount\":10000,\"discount\":0,\"discountcode\":\"\"}', '板砖博客系统PHP版');
INSERT INTO `orders` VALUES (6, 'P20210201131803', 647.00, 'PayPal', 'WP', NULL, 1, '何跃', '18996069665', NULL, 'cqw2d63', '400000', NULL, '', '', NULL, '', '', '', '', 0.00, '', 1612156683, NULL, 0, 1612156683, '1', '{\"singleprice\":647,\"buynum\":\"1\",\"total_amount\":647,\"rate_amount\":0,\"order_amount\":647,\"discount\":0,\"discountcode\":\"\"}', '板砖博客系统PHP版');
INSERT INTO `orders` VALUES (7, 'P20210201214247', 647.00, 'PayPal', 'WP', NULL, 1, '何跃', '18996069665', NULL, '重庆沙坪坝区XXXXXX号', '400000', NULL, '', 'PAYID-MANDEUA4XK43976JV415423U', NULL, '', '', '', '', 0.00, '', 1612186967, NULL, 0, 1612186967, '1', '{\"singleprice\":647,\"buynum\":\"1\",\"total_amount\":647,\"rate_amount\":0,\"order_amount\":647,\"discount\":0,\"discountcode\":\"\"}', '板砖博客系统PHP版');

-- ----------------------------
-- Table structure for product
-- ----------------------------
DROP TABLE IF EXISTS `product`;
CREATE TABLE `product`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `keywords` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `description` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `version` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `categoryid` int(11) NOT NULL,
  `supplierid` int(11) NULL DEFAULT NULL,
  `producttype` int(11) NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `summary` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `slogan` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `iconpath` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `imagepath` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `grade` int(11) NULL DEFAULT NULL COMMENT '产品评级',
  `platform` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `downloadprompts` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `priceprompts` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `supplierrecommend` tinyint(4) NOT NULL DEFAULT 0,
  `headlinetime` datetime(0) NULL DEFAULT '2020-01-01 00:00:00',
  `sort` int(11) NULL DEFAULT NULL,
  `status` int(11) NULL DEFAULT 1,
  `create_time` int(11) NULL DEFAULT NULL,
  `update_time` int(11) NULL DEFAULT NULL,
  `adminid` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `IX_CatalogProduct_SupplierId`(`supplierid`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of product
-- ----------------------------
INSERT INTO `product` VALUES (1, '板砖博客系统PHP版快速搭建平台系统和企业网站', '板砖博客', '板砖博客系统PHP版快速搭建平台系统和企业网站，含微信公众号和小程序。', '板砖博客系统PHP版', '1.0', 1, 1, 0, '<style>\n.category-block{box-shadow:0 2px 6px 0 rgba(0,0,0,.09);padding:20px 25px;border-radius:3px;margin-bottom:25px}\n.category-block .header{margin-bottom:20px;border-bottom:3px solid #f4f7f9;text-align:center}\n.category-block .header h4{font-size:18px;font-weight:700;margin-top:5px}\n.category-block .header .icon-bg-1{background:#ccdb38;box-shadow:0 0 0 4px rgba(204,219,56,.35)}\n.category-block .header .icon-bg-2{background:#a676b8;box-shadow:0 0 0 4px rgba(166,118,184,.35)}\n.category-block .header .icon-bg-3{background:#fe7e17;box-shadow:0 0 0 4px rgba(254,126,23,.35)}\n.category-block .header .icon-bg-4{background:#ec3a56;box-shadow:0 0 0 4px rgba(236,58,86,.35)}\n.category-block .header .icon-bg-5{background:#1dbfff;box-shadow:0 0 0 4px rgba(29,191,255,.35)}\n.category-block .header .icon-bg-6{background:#02d3a4;box-shadow:0 0 0 4px rgba(2,211,164,.35)}\n.category-block .header .icon-bg-7{background:#bc02d3;box-shadow:0 0 0 4px rgba(188,2,211,.35)}\n.category-block .header .icon-bg-8{background:#025fd3;box-shadow:0 0 0 4px rgba(2,95,211,.35)}\n.category-block .header i{margin-right:6px;color:#fff;font-size:13px;width:35px;height:35px;line-height:35px;text-align:center;margin-bottom:6px;border-radius:40px}\n.category-block .category-list li{display:block;font-size:14px;border-bottom:1px solid #f4f7f9;padding:10px 0}\n.category-block .category-list li:last-child{border:none}\n.category-block .category-list li a{display:block;color:#777;font-size:13px}\n.category-block .category-list li a span{float:right;font-size:10px;background:#f4f7f9;border-radius:10px;padding:3px 8px;color:#84919b}\n.category-block .category-list li a:hover{text-decoration:underline}\n.category-block .category-list li a i{margin-right:6px;font-size:12px;color:#5672f9;vertical-align:middle}\n</style>\n<div class=\"row\">\n	<div class=\"col-12\">\n		<p>\n			简介：DMAKE轻量级PHP平台建站系统（板砖博客）是基于前后端分离的理念开发而来，经过门户网站、微信公众平台、资讯网站、资源网站和企业站群等多种运营模式和建站模式产品化，方便更多的个人站长和企业能够快速建立互联网品牌。<br />\n<br />\n特征：前端采用了Bootstrap模块化布局，业务模块无缝组合；后端采用了CRUD标准化设计，新建业务模块从设计到上线不超过30分钟。<br />\n<br />\n系统参数：Thinkphp5 + PHP7.2 + File/Memcache/Redis缓存<br />\n<br />\n系统环境：LNMP\n		</p>\n		<p>\n			系统版本：V201910\n		</p>\n		<div class=\"row\">\n			<div class=\"col-lg-6 offset-lg-0 col-md-5 offset-md-1 col-sm-6 col-6\">\n				<div class=\"category-block\">\n					<div class=\"header\">\n						<i class=\"fa fa-newspaper icon-bg-1\"></i> \n						<h4>\n							资讯模块\n						</h4>\n					</div>\n					<ul class=\"category-list\">\n						<li>\n							<a href=\"/article/2\">分类导航<span>无限子分类</span></a> \n						</li>\n						<li>\n							<a href=\"/article/3\">独立频道<span>独立资讯站</span></a> \n						</li>\n						<li>\n							<a href=\"/article/4\">友好的SEO<span>百度收录快</span></a> \n						</li>\n						<li>\n							<a href=\"/article/5\">伪静态 <span>自定义路径</span></a> \n						</li>\n					</ul>\n				</div>\n			</div>\n			<div class=\"col-lg-6 offset-lg-0 col-md-5 offset-md-1 col-sm-6 col-6\">\n				<div class=\"category-block\">\n					<div class=\"header\">\n						<i class=\"fa fa-user icon-bg-2\"></i> \n						<h4>\n							会员模块\n						</h4>\n					</div>\n					<ul class=\"category-list\">\n						<li>\n							<a href=\"/article/6\">PC&amp;微信&amp;小程序<span>无缝对接</span></a> \n						</li>\n						<li>\n							<a href=\"/article/7\">自定义会员等级<span>权限区分</span></a> \n						</li>\n						<li>\n							<a href=\"/article/8\">会员行为分析<span>图形报表更直观</span></a> \n						</li>\n						<li>\n							<a href=\"/article/9\">安全认证<span>短信邮箱验证码</span></a> \n						</li>\n					</ul>\n				</div>\n			</div>\n			<div class=\"col-lg-6 offset-lg-0 col-md-5 offset-md-1 col-sm-6 col-6\">\n				<div class=\"category-block\">\n					<div class=\"header\">\n						<i class=\"fa fa-money-bill-alt icon-bg-3\"></i> \n						<h4>\n							供应商模块\n						</h4>\n					</div>\n					<ul class=\"category-list\">\n						<li>\n							<a href=\"/article/10\">产品子模块<span>多种规格</span></a> \n						</li>\n						<li>\n							<a href=\"/article/11\">B2C商城模块<span>微信支付宝支付</span></a> \n						</li>\n						<li>\n							<a href=\"/article/12\">资源下载模块<span>可做资源站</span></a> \n						</li>\n						<li>\n							<a href=\"/article/13\">系列视频（分组） <span>多种播放模式</span></a> \n						</li>\n					</ul>\n				</div>\n			</div>\n			<div class=\"col-lg-6 offset-lg-0 col-md-5 offset-md-1 col-sm-6 col-6\">\n				<div class=\"category-block\">\n					<div class=\"header\">\n						<i class=\"fab fa-forumbee icon-bg-4\"></i> \n						<h4>\n							社区模块\n						</h4>\n					</div>\n					<ul class=\"category-list\">\n						<li>\n							<a href=\"/article/14\">轻论坛模式<span>配置简易</span></a> \n						</li>\n						<li>\n							<a href=\"/article/15\">积分悬赏模式<span>有偿回帖</span></a> \n						</li>\n						<li>\n							<a href=\"/article/16\">热门排序<span>板块/主题/会员</span></a> \n						</li>\n						<li>\n							<a href=\"/article/17\">纯净模式<span>高读写高性能</span></a> \n						</li>\n					</ul>\n				</div>\n			</div>\n			<div class=\"col-lg-6 offset-lg-0 col-md-5 offset-md-1 col-sm-6 col-6\">\n				<div class=\"category-block\">\n					<div class=\"header\">\n						<i class=\"fa fa-microchip icon-bg-5\"></i> \n						<h4>\n							APIs\n						</h4>\n					</div>\n					<ul class=\"category-list\">\n						<li>\n							<a href=\"/article/18\">全系统数据调用<span>统一接口</span></a> \n						</li>\n						<li>\n							<a href=\"/article/19\">行为数据采集<span>无缝采集</span></a> \n						</li>\n						<li>\n							<a href=\"/article/20\">Composer SDK<span>持续更新</span></a> \n						</li>\n					</ul>\n				</div>\n			</div>\n			<div class=\"col-lg-6 offset-lg-0 col-md-5 offset-md-1 col-sm-6 col-6\">\n				<div class=\"category-block\">\n					<div class=\"header\">\n						<i class=\"fa fa-chart-pie icon-bg-6\"></i> \n						<h4>\n							智能报表\n						</h4>\n					</div>\n					<ul class=\"category-list\">\n						<li>\n							<a href=\"/article/21\">图形报表<span>运营指标及时反馈</span></a> \n						</li>\n						<li>\n							<a href=\"/article/22\">数据统计<span>客服 / 运营必备</span></a> \n						</li>\n						<li>\n							<a href=\"/article/23\">可拓展数据池<span>元数据采集丰富</span></a> \n						</li>\n					</ul>\n				</div>\n			</div>\n			<div class=\"col-lg-6 offset-lg-0 col-md-5 offset-md-1 col-sm-6 col-6\">\n				<div class=\"category-block\">\n					<div class=\"header\">\n						<i class=\"fab fa-weixin icon-bg-7\"></i> \n						<h4>\n							微信运营能力\n						</h4>\n					</div>\n					<ul class=\"category-list\">\n						<li>\n							<a href=\"/article/24\">营销模块<span>微信公众号SDK已集成</span></a> \n						</li>\n						<li>\n							<a href=\"/article/25\">小程序商城<span>可拓展</span></a> \n						</li>\n						<li>\n							<a href=\"/article/26\">消息主动推送<span>服务消息送达个人</span></a> \n						</li>\n					</ul>\n				</div>\n			</div>\n			<div class=\"col-lg-6 offset-lg-0 col-md-5 offset-md-1 col-sm-6 col-6\">\n				<div class=\"category-block\">\n					<div class=\"header\">\n						<i class=\"fa fa-cloud icon-bg-8\"></i> \n						<h4>\n							云存储\n						</h4>\n					</div>\n					<ul class=\"category-list\">\n						<li>\n							<a href=\"/article/27\">七牛云存储<span>资费便宜存储稳定</span></a> \n						</li>\n						<li>\n							<a href=\"/article/28\">上传即上云<span>单图/文件/编辑器</span></a> \n						</li>\n						<li>\n							<a href=\"/article/29\">资源可管理<span>本地管理云上资源</span></a> \n						</li>\n					</ul>\n				</div>\n			</div>\n		</div>\n	</div>\n</div>', '板砖博客是功能完备的PHP建站系统，稍微有编程基础的人都能够快速自定义本系统。', '功能完备的PHP建站系统', NULL, 'http://cdn.dmake.cn/attachment/images/20191014/15710449722.jpg', 5, '|PHP|', '本系统仅用于个人学习使用', '个人免费使用', 1, NULL, 0, 1, NULL, 1583393683, NULL);
INSERT INTO `product` VALUES (2, '.NET Framework底层代码源码生成工具，一键生成DAL和Model源码', '.NET源码生成，代码生成器', '.NET底层代码源码生成组件，一键生成DAL和Model源码，开放式代码生成器，含各类开发工具包，大幅提升小项目的开发时间。', '.NET底层代码生成源码', '1.0', 2, 1, 2, '<p>\n	.NET Framework底层代码源码生成工具由两个部分组成：代码生成和开发工具集。\n</p>\n<p>\n	<br />\n</p>\n<p>\n	本工具集仅需简单配置即可实现下方所有功能\n</p>\n<p>\n	<br />\n</p>\n<pre class=\"prettyprint lang-xml\">&lt;configuration&gt;\n  &lt;connectionStrings&gt;\n    &lt;add name=\"default\" connectionString=\"Data Source=.;Initial Catalog=SuperSiteData;user id=sa;password=000000\" providerName=\"System.Data.SqlClient\"/&gt;\n  &lt;/connectionStrings&gt;\n\n &lt;appSettings&gt;\n    &lt;add key=\"BaseCodePath\" value=\"E:/SuperSite/\"/&gt;  //项目地址\n    &lt;add key=\"ModelNameSpace\" value=\"Newtec.Model\"/&gt;  //Model命名空间\n    &lt;add key=\"DALNameSpace\" value=\"Newtec.DAL\"/&gt;  //DAL命名空间\n    &lt;add key=\"connName\" value=\"default\"/&gt;\n  &lt;/appSettings&gt;\n&lt;/configuration&gt;</pre>\n<p>\n	<br />\n</p>\n<p>\n	在项目中引入dll文件后，设置触发事件即可。\n</p>\n<p>\n	<br />\n</p>\n<pre class=\"prettyprint lang-cs\">        protected void btModel_Click(object sender, EventArgs e)\n        {\n            new CodeGenerator().CreateModels();\n            Response.Write(\"model is finished\");\n        }\n\n        protected void btDal_Click(object sender, EventArgs e)\n        {\n            new CodeGenerator().CreateBaseDAL();\n            Response.Write(\"dal  is finished\");\n        }</pre>\nModel类生成效果如下，比较基础，可以在生成工具中修改\n<p>\n	<br />\n</p>\n<p>\n	<br />\n</p>\n<pre class=\"prettyprint lang-cs\">using System;\nusing Daneas.Utility.Data;\n\nnamespace Newtec.Model \n{\n    /// &lt;summary&gt;\n    /// APP_Account:实体类(属性说明自动提取数据库字段的描述信息)\n    /// &lt;/summary&gt;\n    [Serializable]\n    public partial class APP_Account\n    {\n        ///&lt;summary&gt;\n        ///字段描述：\n        ///&lt;/summary&gt;\n        public System.Int32 aid { get; set; }\n\n        ///&lt;summary&gt;\n        ///字段描述：公司id\n        ///&lt;/summary&gt;\n        public System.Int32 cid { get; set; }\n\n        ///&lt;summary&gt;\n        ///字段描述：余额\n        ///&lt;/summary&gt;\n        public System.Double blance { get; set; }\n\n        ///&lt;summary&gt;\n        ///字段描述：总收入\n        ///&lt;/summary&gt;\n        public System.Double getin { get; set; }\n\n        ///&lt;summary&gt;\n        ///字段描述：总支出\n        ///&lt;/summary&gt;\n        public System.Double getout { get; set; }\n\n        ///&lt;summary&gt;\n        ///字段描述：\n        ///&lt;/summary&gt;\n        public System.DateTime createdtime { get; set; }\n\n    }\n}\n\n</pre>\nDAL类生成16个固定方法，满足普通数据库操作和事务操作：\n<p>\n	<br />\n</p>\n<p>\n	<br />\n</p>\n<pre class=\"prettyprint lang-cs\">using System;\nusing System.Data;\nusing System.Text;\nusing System.Data.SqlClient;\nusing System.Collections.Generic;\nusing Daneas.Utility.Data;\nusing Daneas.Utility.Universals;\nusing Newtec.Model;\n\nnamespace Newtec.DAL \n{\n	////// APP_Account:基础数据操作类(属性说明自动提取数据库字段的描述信息)\n	///public partial class APP_AccountBaseDAL\n	{\n		 ////// 判断数据是否存在 \n		 ///key：字段名称、value字段值///true为存在public static bool IsExist(Dictionary<string,object> dic) \n		 { \n			code````\n		 } \n\n		////// 获取实例\n		//////ID必须是数字///public static APP_Account GetById(System.Int32  ID)\n		{ \n			code````\n		}\n		\n		////// 获取实例(事务)\n		//////id必须是数字///public static APP_Account GetById(System.Int32 ID,SqlCommand cmd)\n		{ \n			code````\n		}\n\n		////// 新增实例(返回ID)\n		/////////public static System.Int32 Create(APP_Account model)\n		{ \n			code````\n		}\n		\n		////// 新增实例（事务）(返回ID)\n		/////////public static System.Int32 Create(APP_Account model,SqlCommand cmd)\n		{ \n			code````\n		}\n\n		////// 更新实例\n		/////////public static bool Update(APP_Account model)\n		{ \n			code````\n		}\n		\n		////// 更新实例（事务）\n		////////////public static bool Update(APP_Account model,SqlCommand cmd)\n		{ \n			code````\n		}\n\n		////// 删除实例\n		//////id必须是数字///public static bool DeleteById(Int64 Id)\n		{ \n			code````\n		}\n\n		////// 删除实例（事务）\n		//////id必须是数字//////public static bool DeleteById(Int64 Id,SqlCommand cmd)\n		{ \n			code````\n		}\n\n		////// 获取实体列表，不分页\n		//////查询字段，值，And关系///public static ListGetList(Dictionary<string,object> dic)\n		{ \n			code````\n		}\n\n		////// 获取实例对象\n		//////实例对象中的参数,或关系///public static ListGetList(APP_Account model,int pageIndex,int pageSize,out int total)\n		{ \n			code````\n		}\n\n		////// 获取实体列表，分页\n		//////查询字段，值，或关系///public static ListGetQueryList(Dictionary<string,object> dic,string sortColumn,int pageIndex,int pageSize,out int total)\n		{ \n			code````\n		}\n		\n		////// 获取实体列表，分页\n		//////查询字段，值，Dic1或关系,Dic2与关系///public static ListGetQueryList(Dictionary<string,object> dic1,Dictionary<string,object> dic2,string sortColumn,int pageIndex,int pageSize,out int total)\n		{ \n			code````\n		}\n\n		////// 获取所有实例对象\n		//////public static ListGetAllModels()\n		{ \n			code````\n		}\n\n		////// 获取所有实例对象\n		//////public static ListGetTopBy(int top,string orderby)\n		{ \n			code````\n		}\n	}\n}\n\n</string,object></string,object></string,object></string,object></string,object></pre>\n而开发工具集主要包含了类型转换、数据处理相关工具等，如有需要请下载试用,<a href=\"https://download.csdn.net/download/daneas/6288453\" target=\"_blank\"><strong>CSDN下载地址请点击这里</strong></a><strong>。</strong>\n<p>\n	<img src=\"http://cdn.dmake.cn/attachment/images/20210121/16112053268.png\" alt=\"\" />\n</p>', '配置数据库链接即可生成Model代码和DAL代码，另外包含.NET开发工具集。', '配置数据库链接即可生成代码', NULL, 'http://cdn.dmake.cn/attachment/images/20191014/15710475472.jpg', 5, '.NET', '本组件仅针对DAL层和Model层生成有效', '本组件仅供交流使用', 1, '2021-01-21 00:00:00', 0, 1, 1571045202, 1611205388, 1);
INSERT INTO `product` VALUES (3, '小程序商城自助建站不需要300认证费', '小程序商城', '小程序商城自助建站不需要300认证费，只要您具备个体工商户或企业资质，最快20分钟即可建设完毕和上线。', '商城小程序专业版', '4.9', 4, 1, 0, '<p>\n	<img src=\"http://cdn.dmake.cn/attachment/keditor/image/20200311/20200311052300_97392.jpg\" style=\"width:100%;\" alt=\"\" /> \n</p>\n<!--手边小程序 start-->\n<div class=\"shouBian\">\n	<div class=\"w1100 shouMain\">\n		<div class=\"shouMainTitle\">\n			<h5>\n				手边小程序——实体门店一站式客流解决方案\n			</h5>\n			<p>\n				简单易用，精准找客，锁客复购\n			</p>\n		</div>\n		<div class=\"shouBian-side\">\n			<img src=\"/static/youanmi/1.png\" alt=\"\" /> <img src=\"/static/youanmi/2.png\" alt=\"\" /> <img src=\"/static/youanmi/3.png\" alt=\"\" /> <img src=\"/static/youanmi/4.png\" alt=\"\" /> <img src=\"/static/youanmi/5.png\" alt=\"\" /> <img src=\"/static/youanmi/6.png\" alt=\"\" /> <img src=\"/static/youanmi/7.png\" alt=\"\" /> <img src=\"/static/youanmi/8.png\" alt=\"\" /> <img src=\"/static/youanmi/9.png\" alt=\"\" /> <img src=\"/static/youanmi/10.png\" alt=\"\" /> <img src=\"/static/youanmi/11.png\" alt=\"\" /> <img src=\"/static/youanmi/12.png\" alt=\"\" /> <img src=\"/static/youanmi/13.png\" alt=\"\" /> <img src=\"/static/youanmi/14.png\" alt=\"\" /> <img src=\"/static/youanmi/15.png\" alt=\"\" /> <img src=\"/static/youanmi/16.png\" alt=\"\" /> <img src=\"/static/youanmi/17.png\" alt=\"\" /> <img src=\"/static/youanmi/18.png\" alt=\"\" /> <img src=\"/static/youanmi/19.png\" alt=\"\" /> <img src=\"/static/youanmi/20.png\" alt=\"\" /> <img src=\"/static/youanmi/21.png\" alt=\"\" /> <img src=\"/static/youanmi/22.png\" alt=\"\" /> <img src=\"/static/youanmi/23.png\" alt=\"\" /> <img src=\"/static/youanmi/24.png\" alt=\"\" /> <img src=\"/static/youanmi/25.png\" alt=\"\" /> <img src=\"/static/youanmi/26.png\" alt=\"\" /> <img src=\"/static/youanmi/27.png\" alt=\"\" /> <img src=\"/static/youanmi/28.png\" alt=\"\" /> <img src=\"/static/youanmi/29.png\" alt=\"\" /> <img src=\"/static/youanmi/30.png\" alt=\"\" /> <img src=\"/static/youanmi/31.png\" alt=\"\" /> <img src=\"/static/youanmi/32.png\" alt=\"\" /> <img src=\"/static/youanmi/33.png\" alt=\"\" /> <img src=\"/static/youanmi/34.png\" alt=\"\" /> <img src=\"/static/youanmi/35.png\" alt=\"\" /> <img src=\"/static/youanmi/36.png\" alt=\"\" /> <img src=\"/static/youanmi/37.png\" alt=\"\" /> <img src=\"/static/youanmi/38.png\" alt=\"\" /> <img src=\"/static/youanmi/39.png\" alt=\"\" /> <img src=\"/static/youanmi/40.png\" alt=\"\" /> <img src=\"/static/youanmi/41.png\" alt=\"\" /> <img src=\"/static/youanmi/42.png\" alt=\"\" /> \n		</div>\n		<div class=\"shouBian-list\">\n			<ul>\n				<li>\n					广告展示\n				</li>\n|\n				<li class=\"w59\">\n					分类导航\n				</li>\n|\n				<li class=\"w73\">\n					自定义模板\n				</li>\n|\n				<li class=\"w59\">\n					商品展示\n				</li>\n|\n				<li class=\"w59\">\n					图文介绍\n				</li>\n|\n				<li class=\"w59\">\n					视频介绍\n				</li>\n			</ul>\n			<ul>\n				<li class=\"w59\">\n					附近的店\n				</li>\n|\n				<li class=\"w31\">\n					搜索\n				</li>\n|\n				<li class=\"w59\">\n					聊天分享\n				</li>\n|\n				<li class=\"w73\">\n					公众号关联\n				</li>\n|\n				<li class=\"w59\">\n					常用列表\n				</li>\n|\n				<li class=\"w73\">\n					小程序记录\n				</li>\n			</ul>\n			<ul>\n				<li class=\"w73\">\n					附近小程序\n				</li>\n|\n				<li class=\"w59\">\n					地图导航\n				</li>\n|\n				<li class=\"w73\">\n					同步朋友圈\n				</li>\n|\n				<li class=\"w59\">\n					到店自提\n				</li>\n|\n				<li class=\"w73\">\n					朋友圈广告\n				</li>\n			</ul>\n			<ul>\n				<li class=\"w31\">\n					拼团\n				</li>\n|\n				<li class=\"w31\">\n					砍价\n				</li>\n|\n				<li class=\"w31\">\n					预约\n				</li>\n|\n				<li class=\"w45\">\n					限时购\n				</li>\n|\n				<li class=\"w31\">\n					特价\n				</li>\n|\n				<li class=\"w31\">\n					积分\n				</li>\n|\n				<li class=\"w31\">\n					分销\n				</li>\n|\n				<li class=\"w45\">\n					会员卡\n				</li>\n|\n				<li class=\"w45\">\n					优惠券\n				</li>\n			</ul>\n			<ul>\n				<li class=\"w59\">\n					在线购买\n				</li>\n|\n				<li class=\"w59\">\n					在线支付\n				</li>\n|\n				<li class=\"w59\">\n					刷脸支付\n				</li>\n|\n				<li class=\"w59\">\n					在线客服\n				</li>\n|\n				<li class=\"w45\">\n					购物车\n				</li>\n|\n				<li class=\"w59\">\n					物流配送\n				</li>\n			</ul>\n			<ul>\n				<li class=\"w59\">\n					访问留存\n				</li>\n|\n				<li class=\"w59\">\n					社群交流\n				</li>\n|\n				<li class=\"w73\">\n					大数据分析\n				</li>\n|\n				<li class=\"w73\">\n					APP消息\n				</li>\n			</ul>\n			<ul>\n				<li class=\"w73\">\n					社群机器人\n				</li>\n|\n				<li class=\"w59\">\n					群动态码\n				</li>\n|\n				<li class=\"w73\">\n					小程序进群\n				</li>\n|\n				<li class=\"w59\">\n					智能管群\n				</li>\n|\n				<li class=\"w73\">\n					微信群营销\n				</li>\n			</ul>\n		</div>\n	</div>\n</div>\n<!--手边小程序 end--> \n  <!-- 全行业解决方案 start -->\n<div class=\"trageAll\">\n	<div class=\"dataList w1100\">\n		<h5>\n			全行业解决方案\n		</h5>\n		<p>\n			简单易用，精准找客，锁客复购\n		</p>\n		<ol class=\"tabs clearfix\">\n			<li class=\"active\">\n				全部\n			</li>\n			<li>\n				行业解决方案\n			</li>\n			<li>\n				平台解决方案\n			</li>\n			<li>\n				营销模块\n			</li>\n			<li>\n				辅助工具\n			</li>\n		</ol>\n		<ul class=\"list clearfix\" id=\"homeTrageList\">\n			<li>\n				<img class=\"img\" src=\"/static/youanmi/hangye-1.jpg\" alt=\"\" /><span class=\"maxImg\"><img src=\"/static/youanmi/hangye-1-1.jpg\" alt=\"\" /></span> \n			</li>\n			<li>\n				<img class=\"img\" src=\"/static/youanmi/hangye-2.jpg\" alt=\"\" /><span class=\"maxImg\"><img src=\"/static/youanmi/hangye-2-1.jpg\" alt=\"\" /></span> \n			</li>\n			<li>\n				<img class=\"img\" src=\"/static/youanmi/hangye-3.jpg\" alt=\"\" /><span class=\"maxImg\"><img src=\"/static/youanmi/hangye-3-1.jpg\" alt=\"\" /></span> \n			</li>\n			<li>\n				<img class=\"img\" src=\"/static/youanmi/hangye-4.jpg\" alt=\"\" /><span class=\"maxImg\"><img src=\"/static/youanmi/hangye-4-1.jpg\" alt=\"\" /></span> \n			</li>\n			<li>\n				<img class=\"img\" src=\"/static/youanmi/hangye-5.jpg\" alt=\"\" /><span class=\"maxImg\"><img src=\"/static/youanmi/hangye-5-1.jpg\" alt=\"\" /></span> \n			</li>\n			<li>\n				<img class=\"img\" src=\"/static/youanmi/hangye-7.jpg\" alt=\"\" /><span class=\"maxImg\"><img src=\"/static/youanmi/hangye-7-1.jpg\" alt=\"\" /></span> \n			</li>\n			<li>\n				<img class=\"img\" src=\"/static/youanmi/hangye-8.jpg\" alt=\"\" /><span class=\"maxImg\"><img src=\"/static/youanmi/hangye-8-1.jpg\" alt=\"\" /></span> \n			</li>\n			<li>\n				<img class=\"img\" src=\"/static/youanmi/hangye-9.jpg\" alt=\"\" /><span class=\"maxImg\"><img src=\"/static/youanmi/hangye-9-1.jpg\" alt=\"\" /></span> \n			</li>\n			<li>\n				<img class=\"img\" src=\"/static/youanmi/hangye-10.jpg\" alt=\"\" /><span class=\"maxImg\"><img src=\"/static/youanmi/hangye-10-1.jpg\" alt=\"\" /></span> \n			</li>\n			<li>\n				<img class=\"img\" src=\"/static/youanmi/hangye-11.jpg\" alt=\"\" /><span class=\"maxImg\"><img src=\"/static/youanmi/hangye-11-1.jpg\" alt=\"\" /></span> \n			</li>\n			<li>\n				<img class=\"img\" src=\"/static/youanmi/pingtai-1.jpg\" alt=\"\" /><span class=\"maxImg\"><img src=\"/static/youanmi/pingtai-1-1.jpg\" alt=\"\" /></span> \n			</li>\n			<li>\n				<img class=\"img\" src=\"/static/youanmi/pingtai-2.jpg\" alt=\"\" /><span class=\"maxImg\"><img src=\"/static/youanmi/pingtai-2-1.jpg\" alt=\"\" /></span> \n			</li>\n			<li>\n				<img class=\"img\" src=\"/static/youanmi/pingtai-3.jpg\" alt=\"\" /><span class=\"maxImg\"><img src=\"/static/youanmi/pingtai-3-1.jpg\" alt=\"\" /></span> \n			</li>\n			<li>\n				<img class=\"img\" src=\"/static/youanmi/pingtai-4.jpg\" alt=\"\" /><span class=\"maxImg\"><img src=\"/static/youanmi/pingtai-4-1.jpg\" alt=\"\" /></span> \n			</li>\n			<li>\n				<img class=\"img\" src=\"/static/youanmi/pingtai-5.jpg\" alt=\"\" /><span class=\"maxImg\"><img src=\"/static/youanmi/pingtai-5-1.jpg\" alt=\"\" /></span> \n			</li>\n		</ul>\n	</div>\n</div>\n<!-- 全行业解决方案 start --> \n  <!--产品购买start-->\n<div class=\"productPurchase\">\n	<div class=\"w1100\">\n		<div class=\"productPurchaseTitle\">\n			<h5>\n				我们的产品\n			</h5>\n			<p>\n				简单易用，精准找客，锁客复购\n			</p>\n		</div>\n		<div class=\"productPurchaseList clearfix\">\n			<ul class=\"clearfix\">\n				<li class=\"buyConTop\">\n					<h1>\n						基础版小程序\n					</h1>\n					<p>\n						店铺互动名片<br />\n社群交流系统<br />\nAPP管理工具<br />\n基础功能免费升级\n					</p>\n<!-- <b class=\"btnCode mt\">立即咨询</b> -->\n					<div class=\"codeBg\">\n						<span></span> <img src=\"/static/youanmi/wangCodeName.png\" alt=\"\" /> \n						<h3>\n							温馨提示：请使用微信扫描上面的二维码\n						</h3>\n					</div>\n				</li>\n				<li class=\"buyConTop\">\n					<h1>\n						专业版小程序\n					</h1>\n					<p>\n						店铺互动名片<br />\n社电商系统<br />\n强势引流卖货<br />\n超级会员(包含分销系统)<br />\n拼团/砍价/优惠券<br />\nAPP管理工具<br />\n基础功能免费升级<br />\n即时聊天客服系统<br />\n自定义动态商户圈\n					</p>\n<!-- <b class=\"btnCode pt\">立即咨询</b> -->\n					<div class=\"codeBg\">\n						<span></span> <img src=\"/static/youanmi/zengShengBingCode.png\" alt=\"\" /> \n						<h3>\n							温馨提示：请使用微信扫描上面的二维码\n						</h3>\n					</div>\n				</li>\n				<li class=\"buyConTop\">\n					<h1>\n						社群版小程序\n					</h1>\n					<p>\n						含专业版小程序所有功能<br />\n动态码进群<br />\n智能管群<br />\n定时群营销<br />\n自动禁止广告<br />\n群直播<br />\n定时播课\n					</p>\n<!-- <b class=\"btnCode pt\">立即咨询</b> -->\n					<div class=\"codeBg\">\n						<span></span> <img src=\"/static/youanmi/zengShengBingCode.png\" alt=\"\" /> \n						<h3>\n							温馨提示：请使用微信扫描上面的二维码\n						</h3>\n					</div>\n				</li>\n				<li class=\"buyConTop\">\n					<h1>\n						平台小程序\n					</h1>\n					<p>\n						资源整合平台<br />\n社群交流系统<br />\n平台信息交换/平台资源共享<br />\n推荐商家(附近的店/人气排名/<br />\n领券排行/按区域查询)<br />\n稀有广告位展示<br />\n付费入驻/发帖/置顶<br />\n发帖裂变<br />\n基础功能免费升级\n					</p>\n<!-- <b class=\"btnCode vip\">立即咨询</b> -->\n					<div class=\"codeBg\">\n						<span></span> <img src=\"/static/youanmi/zengShengBingCode.png\" alt=\"\" /> \n						<h3>\n							温馨提示：请使用微信扫描上面的二维码\n						</h3>\n					</div>\n				</li>\n			</ul>\n		</div>\n	</div>\n</div>\n<!--N大流量入口，发现你的小程序 start-->\n<div class=\"indexEntry max1920\">\n	<div class=\"w1100\">\n		<div class=\"entryTitle\">\n			<h5>\n				N大流量入口，发现你的小程序\n			</h5>\n			<p>\n				微信不断给小程序开放新入口，源源不断将线上线下流量导给小程序\n			</p>\n		</div>\n		<div class=\"entryList t-mod1\">\n			<ul class=\"clearfix\">\n				<li>\n					<a class=\"t-c1\"> \n					<div class=\"t-sec t-bgi-bg-a\">\n						<i class=\"t-bgi-b1\"></i> \n					</div>\n					<h3 class=\"font20 grey333\">\n						附近的小程序\n					</h3>\n					<div class=\"inter inter-a\">\n						<i class=\"t-bgi-more t-bgi-a\"></i> <span>附近的小程序</span> \n						<p class=\"f-mgt1\">\n							5公里内的微信用户，<br />\n随时发现你的小程序\n						</p>\n					</div>\n</a> \n				</li>\n				<li>\n					<a class=\"t-c1\"> \n					<div class=\"t-sec t-bgi-bg-b\">\n						<i class=\"t-bgi-b1\"></i> \n					</div>\n					<h3 class=\"font20 grey333\">\n						模糊搜索\n					</h3>\n					<div class=\"inter inter-b\">\n						<i class=\"t-bgi-more t-bgi-b\"></i> <span>模糊搜索</span> \n						<p class=\"f-mgt1\">\n							用关键字模糊搜索，11亿<br />\n用户发现你的小程序\n						</p>\n					</div>\n</a> \n				</li>\n				<li>\n					<a class=\"t-c1\"> \n					<div class=\"t-sec t-bgi-bg-c\">\n						<i class=\"t-bgi-b1\"></i> \n					</div>\n					<h3 class=\"font20 grey333\">\n						聊天分享\n					</h3>\n					<div class=\"inter inter-c\">\n						<i class=\"t-bgi-more t-bgi-c\"></i> <span>聊天分享</span> \n						<p class=\"f-mgt1\">\n							好友和群成员，通过<br />\n你的分享，发现你的小程序\n						</p>\n					</div>\n</a> \n				</li>\n				<li>\n					<a class=\"t-c1\"> \n					<div class=\"t-sec t-bgi-bg-d\">\n						<i class=\"t-bgi-b1\"></i> \n					</div>\n					<h3 class=\"font20 grey333\">\n						公众号关联\n					</h3>\n					<div class=\"inter inter-d\">\n						<i class=\"t-bgi-more t-bgi-d\"></i> <span>公众号关联</span> \n						<p class=\"f-mgt1\">\n							将小程序关联到公众号，<br />\n粉丝马上发现你的小程序\n						</p>\n					</div>\n</a> \n				</li>\n				<li>\n					<a class=\"t-c1\"> \n					<div class=\"t-sec t-bgi-bg-e\">\n						<i class=\"t-bgi-b1\"></i> \n					</div>\n					<h3 class=\"font20 grey333\">\n						常用列表\n					</h3>\n					<div class=\"inter inter-e\">\n						<i class=\"t-bgi-more t-bgi-e\"></i> <span>常用列表</span> \n						<p class=\"f-mgt1\">\n							访问过你的小程序的用户，<br />\n通过常用列表快速访问<br />\n你的小程序\n						</p>\n					</div>\n</a> \n				</li>\n				<li class=\"ml\">\n					<a class=\"t-c1\"> \n					<div class=\"t-sec t-bgi-bg-f\">\n						<i class=\"t-bgi-b1\"></i> \n					</div>\n					<h3 class=\"font20 grey333\">\n						文章推送\n					</h3>\n					<div class=\"inter inter-f\">\n						<i class=\"t-bgi-more t-bgi-f\"></i> <span>文章推送</span> \n						<p class=\"f-mgt1\">\n							推送公众号文章时添加小程序<br />\n链接，文章阅读者发现你的<br />\n小程序\n						</p>\n					</div>\n</a> \n				</li>\n				<li>\n					<a class=\"t-c1\"> \n					<div class=\"t-sec t-bgi-bg-g\">\n						<i class=\"t-bgi-b1\"></i> \n					</div>\n					<h3 class=\"font20 grey333\">\n						扫一扫\n					</h3>\n					<div class=\"inter inter-g\">\n						<i class=\"t-bgi-more t-bgi-g\"></i> <span>扫一扫</span> \n						<p class=\"f-mgt1\">\n							将小程序太阳码线上分享，<br />\n线下打印张贴，任何人扫一扫<br />\n就发现你的小程序\n						</p>\n					</div>\n</a> \n				</li>\n				<li>\n					<a class=\"t-c1\"> \n					<div class=\"t-sec t-bgi-bg-h\">\n						<i class=\"t-bgi-b1\"></i> \n					</div>\n					<h3 class=\"font20 grey333\">\n						支付完成页\n					</h3>\n					<div class=\"inter inter-h\">\n						<i class=\"t-bgi-more t-bgi-h\"></i> <span>支付完成页</span> \n						<p class=\"f-mgt1\">\n							用户购买支付后，可通过支付<br />\n完成页，直接访问你的小程序\n						</p>\n					</div>\n</a> \n				</li>\n				<li>\n					<a class=\"t-c1\"> \n					<div class=\"t-sec t-bgi-bg-i\">\n						<i class=\"t-bgi-b1\"></i> \n					</div>\n					<h3 class=\"font20 grey333\">\n						群小程序记录\n					</h3>\n					<div class=\"inter inter-i\">\n						<i class=\"t-bgi-more t-bgi-i\"></i> <span>群小程序记录</span> \n						<p class=\"f-mgt1\">\n							群成员通过查看群分享过<br />\n的小程序记录，快速访问<br />\n你的小程序\n						</p>\n					</div>\n</a> \n				</li>\n				<li>\n					<a class=\"t-c1\"> \n					<div class=\"t-sec t-bgi-bg-j\">\n						<i class=\"t-bgi-b1\"></i> \n					</div>\n					<h3 class=\"font20 grey333\">\n						搜索界面小程序\n					</h3>\n					<div class=\"inter inter-j\">\n						<i class=\"t-bgi-more t-bgi-j\"></i> <span>搜索界面小程序</span> \n						<p class=\"f-mgt1\">\n							微信用户打开搜索界面，直接<br />\n访问已使用过的你的小程序\n						</p>\n					</div>\n</a> \n				</li>\n				<li>\n					<a class=\"t-c1\"> \n					<div class=\"t-sec t-bgi-bg-k\">\n						<i class=\"t-bgi-b1\"></i> \n					</div>\n					<h3 class=\"font20 grey333\">\n						小程序互跳\n					</h3>\n					<div class=\"inter inter-k\">\n						<i class=\"t-bgi-more t-bgi-k\"></i> <span>小程序互跳</span> \n						<p class=\"f-mgt1\">\n							将你的小程序关联到其它的<br />\n小程序，它的粉丝就会发现<br />\n你的小程序\n						</p>\n					</div>\n</a> \n				</li>\n				<li class=\"ml\">\n					<a class=\"t-c1\"> \n					<div class=\"t-sec t-bgi-bg-l\">\n						<i class=\"t-bgi-b1\"></i> \n					</div>\n					<h3 class=\"font20 grey333\">\n						更多入口\n					</h3>\n					<div class=\"inter inter-l\">\n						<i class=\"t-bgi-more t-bgi-l\"></i> <span>更多入口</span> \n						<p class=\"f-mgt1\">\n							N个小程序流量入口，<br />\n让你的小程序畅享微信<br />\n9亿流量红利\n						</p>\n					</div>\n</a> \n				</li>\n			</ul>\n		</div>\n	</div>\n</div>\n<!--N大流量入口，发现你的小程序 end--> \n  <!--手边小程序引领实体门店转型互联网+ end-->\n<div class=\"indexAdd\">\n	<div class=\"w1100\">\n		<div class=\"indexAddTitle\">\n			<h5>\n				手边小程序引领实体门店转型互联网+\n			</h5>\n			<p>\n				简单易用，精准找客，锁客复购\n			</p>\n		</div>\n		<div class=\"indexAddList\">\n			<ul class=\"clearfix\">\n				<li class=\"dd\">\n					<div>\n						<h5>\n							轻松开店\n						</h5>\n						<p>\n							注册、授权、设置，三步<br />\n拥有一个专属的小程序线上店\n						</p>\n					</div>\n				</li>\n				<li class=\"jj\">\n					<div>\n						<h5>\n							零成本获客\n						</h5>\n						<p>\n							共享微信11亿流量红利，<br />\n零成本获得更多客户\n						</p>\n					</div>\n				</li>\n				<li class=\"ll mr\">\n					<div>\n						<h5>\n							借力微信平台\n						</h5>\n						<p>\n							基于微信平台，极具爆发力的社交<br />\n力量让你的小程序线上店红利不断\n						</p>\n					</div>\n				</li>\n				<li class=\"qq\">\n					<div>\n						<h5>\n							独创1+N倍流量模式\n						</h5>\n						<p>\n							享受1个独立小程序自带流量 + N个<br />\n平台叠加流量的优势\n						</p>\n					</div>\n				</li>\n				<li class=\"yy\">\n					<div>\n						<h5>\n							轻运营\n						</h5>\n						<p>\n							你只要会发朋友圈，就<br />\n可以正常推广运营小程序\n						</p>\n					</div>\n				</li>\n				<li class=\"zz mr\">\n					<div>\n						<h5>\n							终身免费升级\n						</h5>\n						<p>\n							你无需再交任何费用，<br />\n即可享受产品功能升级\n						</p>\n					</div>\n				</li>\n			</ul>\n		</div>\n	</div>\n</div>\n<!--手边小程序引领实体门店转型互联网+ end--> \n  <!--手边小程序使用前后效果对比+ end-->\n<div class=\"indexduibi\">\n	<div class=\"w1100\">\n		<div class=\"indexduibiTitle\">\n			<h5>\n				手边小程序使用前后效果对比\n			</h5>\n			<p>\n				小程序带来质的提升\n			</p>\n		</div>\n		<div class=\"clearfix indexduibiList\">\n			<ul class=\"clearfix ul1\">\n				<li>\n					聘请萌妹子派单，一天8小时，成本180元\n				</li>\n				<li>\n					客流的多少依靠店铺地段\n				</li>\n				<li>\n					需专人负责品牌建设\n				</li>\n				<li>\n					砸钱请团队表演，或开展线下价格战\n				</li>\n				<li>\n					顾客出店就失去联系，何时再来全看顾客心情\n				</li>\n				<li>\n					引流全靠店铺的各种促销打折营销手段\n				</li>\n			</ul>\n			<ul class=\"clearfix ul2\">\n				<li>\n					成本\n				</li>\n				<li>\n					客流\n				</li>\n				<li>\n					品牌\n				</li>\n				<li>\n					营销\n				</li>\n				<li>\n					复购\n				</li>\n				<li>\n					平台\n				</li>\n			</ul>\n			<ul class=\"clearfix ul3\">\n				<li class=\"li1\">\n					小程序24小时全年不打烊站街，<span>成本不到2元/天</span> \n				</li>\n				<li>\n					<span>N多入口</span>发现你的小程序店，<br />\n微信11亿用户都可能变成你的顾客\n				</li>\n				<li>\n					一张<span>富媒体店铺名片</span>，随时随地分享小程序店，口碑相传\n				</li>\n				<li>\n					营销活动促销基于微信社交体系的<span>裂变式传播</span>，<br />\n花费少效果好\n				</li>\n				<li>\n					小程序访问即留存，<br />\n顾客随时再次访问小程序店，<span>二次购买门槛低</span> \n				</li>\n				<li>\n					除小程序自带流量外，还有N个平台小程序为你的<br />\n小程序店带来<span>N倍流量</span> \n				</li>\n			</ul>\n		</div>\n	</div>\n</div>\n<!--手边小程序使用前后效果对比+ end--> \n  <!--我们已服务超过10000家实体门店+ start-->\n<div class=\"indexServer clearfix\">\n	<div class=\"w1100\">\n		<div class=\"indexServerTitle\">\n			<h5>\n				我们已服务超过数十万家实体门店\n			</h5>\n			<p>\n				支持不同行业，让你拥有你的专属小程序店\n			</p>\n		</div>\n<!-- /*新添加的 start*/ -->\n		<div class=\"serverList clearfix\" id=\"serverList\">\n			<ul class=\"clearfix\">\n				<li>\n					<img src=\"/static/youanmi/11ec46ed34ba4557a8a47c08d53cf597.jpg\" /> \n					<div class=\"title\">\n						<p>\n							水果店\n						</p>\n<i>生鲜水果</i> \n					</div>\n<span><img src=\"/static/youanmi/1b054f9c489f4501b17f0f755c70ac15.jpg\" alt=\"\" /></span> \n				</li>\n				<li>\n					<img src=\"/static/youanmi/a6ecdbc164e1409f85e6341de8e3fdc9.jpg\" /> \n					<div class=\"title\">\n						<p>\n							超市/便利店\n						</p>\n<i>便利店,超市</i> \n					</div>\n<span><img src=\"/static/youanmi/c7b24faaf0ec4feeac78075e01c8df32.png\" alt=\"\" /></span> \n				</li>\n				<li>\n					<img src=\"/static/youanmi/b45dba21eeec498095f70d8a2094197e.png\" /> \n					<div class=\"title\">\n						<p>\n							手机数码类\n						</p>\n<i>数码</i> \n					</div>\n<span><img src=\"/static/youanmi/a1b5351ba35041ea91d3bcd6bbca7d74.png\" alt=\"\" /></span> \n				</li>\n				<li>\n					<img src=\"/static/youanmi/e89e7afd2b1343fa82f27f80d85a3bf9.jpg\" /> \n					<div class=\"title\">\n						<p>\n							美发类-A\n						</p>\n<i>时尚美发</i> \n					</div>\n<span><img src=\"/static/youanmi/a5da2a3ed2db40e9a5d0fbc7f4fd0ea6.jpg\" alt=\"\" /></span> \n				</li>\n				<li>\n					<img src=\"/static/youanmi/83a7368ca67e4752b4572a9b19e786fd.jpg\" /> \n					<div class=\"title\">\n						<p>\n							美妆类-A\n						</p>\n<i>云模板</i> \n					</div>\n<span><img src=\"/static/youanmi/e3360087d9bf4d2981783a33340c8c43.jpg\" alt=\"\" /></span> \n				</li>\n				<li>\n					<img src=\"/static/youanmi/51a935c6a7764f39b53d228cfc241200.jpg\" /> \n					<div class=\"title\">\n						<p>\n							教育培训类\n						</p>\n<i>培训</i> \n					</div>\n<span><img src=\"/static/youanmi/e234d88d879c4e1fa76cccc3046e2bec.png\" alt=\"\" /></span> \n				</li>\n				<li>\n					<img src=\"/static/youanmi/2d77534087d5413f89748a98642a5926.jpg\" /> \n					<div class=\"title\">\n						<p>\n							服装类-B\n						</p>\n<i>云模板</i> \n					</div>\n<span><img src=\"/static/youanmi/9a744d1c46a94372b5d02977395467b5.jpg\" alt=\"\" /></span> \n				</li>\n				<li>\n					<img src=\"/static/youanmi/32b750a0c7bd47d998583c658f6df0ed.jpg\" /> \n					<div class=\"title\">\n						<p>\n							饰品类-A\n						</p>\n<i>云模板</i> \n					</div>\n<span><img src=\"/static/youanmi/a8db0546ba094f16ba426c0eea237271.jpg\" alt=\"\" /></span> \n				</li>\n				<li>\n					<img src=\"/static/youanmi/d4945ed3bab5458bb9998b49e1e2622c.jpg\" /> \n					<div class=\"title\">\n						<p>\n							饰品类-B\n						</p>\n<i>云模板</i> \n					</div>\n<span><img src=\"/static/youanmi/c5b1f6e049894f42a5ef352b08ab398d.jpg\" alt=\"\" /></span> \n				</li>\n			</ul>\n		</div>\n<!-- /*新添加的 * end/ -->\n	</div>\n</div>\n<!--我们已服务超过10000家实体门店+ end--> \n  <!--我们已扶持超过1000个代理商伙伴打造行业／区域平台+ start-->\n<div class=\"indexDaili\">\n	<div class=\"w1100 indexDailiMain\">\n		<div class=\"indexDailiMainTitle\">\n			<h5>\n				将您的资源汇聚在平台，打造您专属的平台小程序\n			</h5>\n		</div>\n	</div>\n</div>\n<div class=\"indexDailiBg\">\n</div>\n<!--我们已扶持超过1000个代理商伙伴打造行业／区域平台end--> \n  <!--我们已扶持超过1000个代理商伙伴打造行业／区域平台end-->\n<div class=\"cooperate clearfix\">\n	<div class=\"cooperate-top\">\n		<h5>\n			我们与合作伙伴共成长\n		</h5>\n		<p>\n			合作双赢，共享未来\n		</p>\n	</div>\n	<div class=\"w1100\">\n		<ul class=\"coperationlist\">\n			<li>\n				<img src=\"/static/youanmi/1(1).png\" alt=\"\" /> <img src=\"/static/youanmi/5(1).png\" alt=\"\" /> <img src=\"/static/youanmi/9(1).png\" alt=\"\" /> <img src=\"/static/youanmi/2(1).png\" alt=\"\" /> <img src=\"/static/youanmi/6(1).png\" alt=\"\" /> <img src=\"/static/youanmi/10(1).png\" alt=\"\" /> <img src=\"/static/youanmi/3(1).png\" alt=\"\" /> <img src=\"/static/youanmi/7(1).png\" alt=\"\" /> <img src=\"/static/youanmi/11(1).png\" alt=\"\" /> <img src=\"/static/youanmi/4(1).png\" alt=\"\" /> <img src=\"/static/youanmi/8(1).png\" alt=\"\" /> <img src=\"/static/youanmi/12(1).png\" alt=\"\" /> <img src=\"/static/youanmi/13(1).png\" alt=\"\" /> <img src=\"/static/youanmi/17(1).png\" alt=\"\" /> <img src=\"/static/youanmi/21(1).png\" alt=\"\" /> <img src=\"/static/youanmi/14(1).png\" alt=\"\" /> <img src=\"/static/youanmi/18(1).png\" alt=\"\" /> <img src=\"/static/youanmi/22(1).png\" alt=\"\" /> <img src=\"/static/youanmi/15(1).png\" alt=\"\" /> <img src=\"/static/youanmi/19(1).png\" alt=\"\" /> <img src=\"/static/youanmi/23(1).png\" alt=\"\" /> <img src=\"/static/youanmi/16(1).png\" alt=\"\" /> <img src=\"/static/youanmi/20(1).png\" alt=\"\" /> <img src=\"/static/youanmi/24(1).png\" alt=\"\" /> <img src=\"/static/youanmi/25(1).png\" alt=\"\" /> <img src=\"/static/youanmi/29(1).png\" alt=\"\" /> <img src=\"/static/youanmi/33(1).png\" alt=\"\" /> <img src=\"/static/youanmi/26(1).png\" alt=\"\" /> <img src=\"/static/youanmi/30(1).png\" alt=\"\" /> <img src=\"/static/youanmi/34.jpg\" alt=\"\" /> <img src=\"/static/youanmi/27(1).png\" alt=\"\" /> <img src=\"/static/youanmi/31(1).png\" alt=\"\" /> <img src=\"/static/youanmi/35.jpg\" alt=\"\" /> <img src=\"/static/youanmi/28(1).png\" alt=\"\" /> <img src=\"/static/youanmi/32(1).png\" alt=\"\" /> <img src=\"/static/youanmi/42.jpg\" alt=\"\" /> <img src=\"/static/youanmi/39.jpg\" alt=\"\" /> <img src=\"/static/youanmi/43.jpg\" alt=\"\" /> <img src=\"/static/youanmi/40.jpg\" alt=\"\" /> <img src=\"/static/youanmi/44.jpg\" alt=\"\" /> \n			</li>\n		</ul>\n	</div>\n</div>\n<!--我们已扶持超过1000个代理商伙伴打造行业／区域平台end-->', '', '从小程序建设到上线仅需20分钟', NULL, 'http://cdn.dmake.cn/attachment/images/20191014/15710487993.jpg', 5, '', '', '', 1, NULL, 0, 1, 1571048855, 1584112046, 1);
INSERT INTO `product` VALUES (4, '建站博客自助式建设网站平台', '平台', '', '建站宝盒', '11', 1, 1, 0, NULL, '网站、小程序、公众号建站超时，自助服务即可上线。', '域名购买到平台上线只在建站宝盒即可', NULL, 'http://cdn.dmake.cn/attachment/images/20191014/15710504994.jpg', 0, '', '', '', 1, NULL, 0, 1, 1571050503, 1571050682, 1);
INSERT INTO `product` VALUES (5, '指尖进销存_仓储仓库库存管理软件_手机电脑开单软件_云ERP系统网络版', '库存软件，进销存，仓储管理，云ERP', '指尖进销存提供全渠道生意解决方案，指尖进销存ERP下载，指尖进销存打印机驱动下载,电脑进销存管理商品。', '指尖进销存', '', 5, 4, 0, NULL, '基础班与高级版本的指尖进销存软件赋予企业更多选择权，按需购买进销存服务！', '非规模产品，可拓展空间大', NULL, 'https://www.zhijianjxc.com/images_web/product-service/paid-product-imgO.png', 3, '', '', '', 1, '2020-01-01 00:00:00', 0, 0, 1631176489, 1631176827, 1);
INSERT INTO `product` VALUES (6, '利亚方舟影楼ERP管理系统', '', '通过数据互通，将影楼营销引流拓客系统、影楼客资管理系统、影楼ERP管理系统全面打通，实现无缝对接解决影楼管理过程中引流难，营销难，拓客难，留存难，转化难的问题。', '利亚方舟影楼ERP管理系统', '', 5, 5, 0, '<p>\n	利亚方舟影楼ERP管理系统以影楼流程管理、财务管理和客户管理为核心，利用技术、信息和运营的整合手段，实现数据的互通。以实际应用角度出发，以用户为核心，摒弃固化的管理思维模式，整体加入人性化设计理念，在实现智能化管理的基础上，更注重功能的实用性及操作的简洁度，所有设计基于影楼员工工作实际需求，帮助影楼老板实现轻松经营、科学管理、身心解放。\n</p>\n<p style=\"text-align:center;\">\n	<br />\n</p>\n<p style=\"text-align:center;\">\n	<img src=\"https://9170362.s21i.faiusr.com/2/1/ABUIABACGAAgjPvl9AUoo7mXuQUwgAo40AU!1000x1000.jpg\" alt=\"\" width=\"100%\" /> \n</p>\n<p style=\"text-align:center;\">\n	<img src=\"http://cdn.dmake.cn/Fiu-FkfMuBu8Km6wrgcFcYwv35Y6\" alt=\"\" /> \n</p>\n<p style=\"text-align:center;\">\n	<img src=\"https://9170362.s21i.faiusr.com/2/1/ABUIABACGAAgnP3l9AUooN-xzAIwgAo40AU!1000x1000.jpg\" alt=\"\"  width=\"100%\" /> \n</p>\n<p style=\"text-align:center;\">\n	<img src=\"https://9170362.s21i.faiusr.com/4/1/ABUIABAEGAAgvpDm9AUozdGD7gMwpg04rAY!1000x1000.png.webp\" alt=\"\"  width=\"100%\" /> \n</p>\n<p style=\"text-align:center;\">\n	<br />\n</p>\n<p style=\"text-align:center;\">\n	<img src=\"https://9170362.s21i.faiusr.com/4/1/ABUIABAEGAAgrY-m9AUo3Ky93QUwvg84_As!800x800.png.webp\" alt=\"\"  width=\"100%\" /> \n</p>', '以影楼流程管理、财务管理和客户管理为核心，利用技术、信息和运营的整合手段，实现数据的互通。以实际应用角度出发，以用户为核心，摒弃固化的管理思维模式，整体加入人性化设计理念，在实现智能化管理的基础上，更注重功能的实用性及操作的简洁度，所有设计基于影楼员工工作实际需求，帮助影楼老板实现轻松经营、科学管理、身心解放。', '更注重功能的实用性及操作的简洁度，所有设计基于影楼员工工作实际需求', NULL, '//cdn.dmake.cn/attachment/images/20210923/16323847990.png', 5, '', '', '', 0, '2020-01-01 00:00:00', 0, 0, 1632385009, 1632387001, 1);

-- ----------------------------
-- Table structure for product_category
-- ----------------------------
DROP TABLE IF EXISTS `product_category`;
CREATE TABLE `product_category`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parentid` int(11) NOT NULL,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `iconpath` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `imagepath` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `sort` int(11) NOT NULL,
  `title` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `keywords` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `description` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `status` int(11) NOT NULL,
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  `delete_time` int(11) NOT NULL,
  `adminid` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of product_category
-- ----------------------------
INSERT INTO `product_category` VALUES (1, 0, '网站系统', '', '', 0, '网站建站系统快速建站小程序公众号', '网站建站系统，小程序建设，公众号开发', '网站建站系统快速建站小程序公众号', 1, 1571040460, 1571041859, 0, 1);
INSERT INTO `product_category` VALUES (2, 0, '.NET源码', '', '', 1, '.NET源码分享', '.NET源码分享', '.NET源码分享，控件产品分享。', 1, 1571041989, 0, 0, 1);
INSERT INTO `product_category` VALUES (3, 0, 'PHP模块', '', '', 2, 'PHP模块源码', 'PHP模块源码', 'PHP模块源码，小程序API模块，公众号API模块', 1, 1571042134, 0, 0, 1);
INSERT INTO `product_category` VALUES (4, 0, '公众平台', '', '', 4, '微信公众平台', '', '', 1, 1571048102, 0, 0, 1);
INSERT INTO `product_category` VALUES (5, 0, '国产软件', '', '', 50, '国产软件查询_ERP_收银软件_OA软件，一站式寻找打破信息不平衡', '国产软件，国产ERP，国产收银软件，国产OA', '收录国内近年来优秀的独立软件，包含ERP、收银软件、进销存软件、OA软件等，获得中立公平的信息，打破信息不平衡。', 1, 1631175363, 0, 0, 1);

-- ----------------------------
-- Table structure for product_download
-- ----------------------------
DROP TABLE IF EXISTS `product_download`;
CREATE TABLE `product_download`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `productid` int(11) NOT NULL,
  `type` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `filepath` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `filesize` int(11) NOT NULL,
  `sort` int(11) NOT NULL,
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  `downloadqty` int(11) NOT NULL,
  `seotitle` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `seodescription` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `delete_time` int(11) UNSIGNED ZEROFILL NOT NULL,
  `keywords` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `description` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `imagepath` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `summary` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `adminid` int(11) NULL DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `IX_productdownload_productid`(`productid`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of product_download
-- ----------------------------

-- ----------------------------
-- Table structure for product_downloadrecord
-- ----------------------------
DROP TABLE IF EXISTS `product_downloadrecord`;
CREATE TABLE `product_downloadrecord`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `productid` int(11) NOT NULL,
  `customerid` int(11) NOT NULL,
  `downloadnumber` int(11) NOT NULL,
  `downloadtime` datetime(0) NULL DEFAULT NULL,
  `downloadtype` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `filetype` int(11) NULL DEFAULT NULL,
  `fileid` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of product_downloadrecord
-- ----------------------------

-- ----------------------------
-- Table structure for product_home
-- ----------------------------
DROP TABLE IF EXISTS `product_home`;
CREATE TABLE `product_home`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parentid` int(11) NOT NULL,
  `productid` int(11) NOT NULL,
  `productname` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `tags` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `description` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `create_time` int(11) NOT NULL,
  `imagepath` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `sort` int(11) NOT NULL,
  `update_time` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of product_home
-- ----------------------------

-- ----------------------------
-- Table structure for product_images
-- ----------------------------
DROP TABLE IF EXISTS `product_images`;
CREATE TABLE `product_images`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `productid` int(11) NOT NULL,
  `title` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `path` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `thumbnailpath` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `sort` int(11) NOT NULL,
  `create_time` int(11) NULL DEFAULT NULL,
  `update_time` int(11) NULL DEFAULT NULL,
  `adminid` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `IX_productimages_productid`(`productid`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of product_images
-- ----------------------------

-- ----------------------------
-- Table structure for product_price
-- ----------------------------
DROP TABLE IF EXISTS `product_price`;
CREATE TABLE `product_price`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `productid` int(11) NOT NULL,
  `groupname` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `currencytype` int(11) NOT NULL DEFAULT 0 COMMENT '0123中美英欧',
  `amount` decimal(18, 2) NULL DEFAULT NULL,
  `stock` int(11) NOT NULL DEFAULT 100 COMMENT '库存',
  `description` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `sort` int(11) NOT NULL,
  `sale_amount` decimal(18, 2) NULL DEFAULT NULL,
  `sale_deadline` int(11) NULL DEFAULT NULL,
  `status` int(11) NOT NULL,
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL DEFAULT 0,
  `adminid` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `IX_CatalogProductLicense_ProductId`(`productid`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 13 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of product_price
-- ----------------------------
INSERT INTO `product_price` VALUES (1, 1, '免费', '社区版', 0, 0.00, 100, '可以用于个人学习和商业修改，记得留下版权信息！', 1, NULL, NULL, 1, 1609221229, 1609222407, 1);
INSERT INTO `product_price` VALUES (2, 1, '收费', '商业版', 0, 10000.00, 100, '如果您觉得模板还可以，本版提供商业上线服务！', 2, NULL, NULL, 1, 1609222464, 0, 1);
INSERT INTO `product_price` VALUES (3, 1, '收费', '跨境版', 1, 99.00, 100, 'Its for paypal checkout', 2, NULL, NULL, 1, 1612149719, 0, 1);
INSERT INTO `product_price` VALUES (4, 5, NULL, '基础版', 0, 279.00, 100, '年费基础版本', 0, NULL, NULL, 1, 1631176878, 1631178506, 1);
INSERT INTO `product_price` VALUES (5, 5, NULL, '高级版', 0, 359.00, 100, '永久使用版本', 0, NULL, NULL, 1, 1631176890, 1631178491, 1);
INSERT INTO `product_price` VALUES (6, 4, NULL, '企业官网套餐', 0, 1834.00, 100, '多端合一，数据同步，可视化拖拽式编辑', 0, NULL, NULL, 1, 1631954298, 1631954375, 1);
INSERT INTO `product_price` VALUES (7, 4, NULL, '电商网站套餐', 0, 4000.00, 100, '满赠满减等多种营销功能，支持订单物流信息查询', 0, NULL, NULL, 1, 1631954366, 0, 1);
INSERT INTO `product_price` VALUES (8, 4, NULL, '分销商城套餐', 0, 8000.00, 100, '无限级分销，最多三级返佣，自定义分销商名称、等级、价格', 0, NULL, NULL, 1, 1631954407, 0, 1);
INSERT INTO `product_price` VALUES (9, 3, NULL, '官网展示型', 0, 300.00, 100, '可发新闻动态、产品库、免认证费、不可做销售', 0, NULL, NULL, 1, 1631954464, 0, 1);
INSERT INTO `product_price` VALUES (10, 3, NULL, '营销专业版', 0, 1979.00, 100, '可销售商品、分销、团购、砍价、限时购、主动推送消息至客户等', 0, NULL, NULL, 1, 1631954504, 0, 1);
INSERT INTO `product_price` VALUES (11, 2, NULL, '源码出售', 0, 1.00, 100, '该程序集源码全开放', 0, NULL, NULL, 1, 1631954542, 0, 1);
INSERT INTO `product_price` VALUES (12, 6, NULL, 'ERP标准版', 0, 10000.00, 100, '只是做产品展示，价格是随便写的', 0, NULL, NULL, 1, 1632387458, 0, 1);

-- ----------------------------
-- Table structure for product_promotion
-- ----------------------------
DROP TABLE IF EXISTS `product_promotion`;
CREATE TABLE `product_promotion`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `articleid` int(11) NULL DEFAULT NULL,
  `productid` int(11) NOT NULL,
  `title` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `imagepath` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `begintime` datetime(0) NOT NULL,
  `endtime` datetime(0) NOT NULL,
  `promotiontype` int(11) NOT NULL,
  `createdtime` datetime(0) NOT NULL,
  `discountlevel` double NULL DEFAULT NULL,
  `discountprice` decimal(18, 2) NULL DEFAULT NULL,
  `currencytype` int(11) NULL DEFAULT NULL,
  `sort` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `IX_CatalogProductPromotion_ArticleId`(`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of product_promotion
-- ----------------------------

-- ----------------------------
-- Table structure for product_recommend
-- ----------------------------
DROP TABLE IF EXISTS `product_recommend`;
CREATE TABLE `product_recommend`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `keywords` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `description` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `dataid` int(11) NOT NULL,
  `datatype` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `cateid` int(11) NULL DEFAULT NULL,
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  `sort` int(11) NOT NULL,
  `iconpath` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of product_recommend
-- ----------------------------

-- ----------------------------
-- Table structure for product_relations
-- ----------------------------
DROP TABLE IF EXISTS `product_relations`;
CREATE TABLE `product_relations`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `productid` int(11) NOT NULL,
  `parentproductid` int(11) NOT NULL,
  `productrelationtype` int(11) NOT NULL,
  `cannotbuysinglly` tinyint(4) NOT NULL,
  `cannotdownloadtry` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `IX_CatalogProduct_CatalogProduct_ParentProductId`(`parentproductid`) USING BTREE,
  INDEX `IX_CatalogProduct_CatalogProduct_ProductId`(`productid`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Fixed;

-- ----------------------------
-- Records of product_relations
-- ----------------------------

-- ----------------------------
-- Table structure for product_state
-- ----------------------------
DROP TABLE IF EXISTS `product_state`;
CREATE TABLE `product_state`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clickcount` int(11) NOT NULL COMMENT '点击数',
  `followcount` int(11) NOT NULL COMMENT '关注数',
  `downloadcount` int(11) NOT NULL,
  `commentcount` int(11) NOT NULL,
  `isrecommended` tinyint(4) NOT NULL,
  `editcount` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Fixed;

-- ----------------------------
-- Records of product_state
-- ----------------------------
INSERT INTO `product_state` VALUES (1, 10, 10, 10, 0, 0, 0);
INSERT INTO `product_state` VALUES (4, 10, 10, 10, 0, 0, 0);
INSERT INTO `product_state` VALUES (2, 10, 10, 10, 0, 0, 0);
INSERT INTO `product_state` VALUES (3, 10, 10, 10, 0, 0, 0);
INSERT INTO `product_state` VALUES (5, 10, 10, 10, 0, 0, 0);
INSERT INTO `product_state` VALUES (6, 10, 10, 10, 0, 0, 0);

-- ----------------------------
-- Table structure for report_pvcrunms
-- ----------------------------
DROP TABLE IF EXISTS `report_pvcrunms`;
CREATE TABLE `report_pvcrunms`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `adminid` int(11) NULL DEFAULT NULL COMMENT '类型:adid,memberid',
  `supplierid` int(11) NULL DEFAULT NULL COMMENT '产生结果对应的id',
  `productid` int(11) NULL DEFAULT NULL COMMENT '身份识别，MD5(IP+TIME)',
  `datatype` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `dataid` int(11) NULL DEFAULT NULL COMMENT '产生结果对应的id',
  `create_time` int(11) NULL DEFAULT NULL,
  `pv` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of report_pvcrunms
-- ----------------------------
INSERT INTO `report_pvcrunms` VALUES (1, 1, 1, 1, 'article', 1, 1, 1);

-- ----------------------------
-- Table structure for report_pvsource
-- ----------------------------
DROP TABLE IF EXISTS `report_pvsource`;
CREATE TABLE `report_pvsource`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '类型:article,product,solution,resource',
  `datacode` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '解决方案用的code',
  `dataid` int(11) NULL DEFAULT NULL COMMENT '工具产品线使用',
  `channel` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '来源渠道：search,outlink,other',
  `channelname` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '渠道名称，例如baidu,360,csdn,qq,weixin',
  `ip` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '单日内唯一确认',
  `create_time` int(11) NOT NULL,
  `checkdate` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `contribut` int(11) NOT NULL COMMENT '下游贡献率，访问下一个页面的时候增减',
  `crm` int(11) NULL DEFAULT NULL,
  `token` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '身份识别，MD5(IP+TIME)',
  `vurls` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '访问了那些页面',
  `customerid` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1555 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of report_pvsource
-- ----------------------------

-- ----------------------------
-- Table structure for report_worklog
-- ----------------------------
DROP TABLE IF EXISTS `report_worklog`;
CREATE TABLE `report_worklog`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `adminid` int(11) NOT NULL,
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  `type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '类型',
  `title` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `url` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `bits` int(11) NULL DEFAULT NULL COMMENT '阅读量',
  `nextbits` int(11) NOT NULL,
  `standard` int(11) NOT NULL COMMENT '标准分值',
  `score` int(11) NOT NULL,
  `cosehour` double NOT NULL COMMENT '用时',
  `reason` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `outlinks` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `outlinksbits` int(11) NULL DEFAULT NULL COMMENT '所有外链的阅读总量',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of report_worklog
-- ----------------------------

-- ----------------------------
-- Table structure for series_access
-- ----------------------------
DROP TABLE IF EXISTS `series_access`;
CREATE TABLE `series_access`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customerid` int(11) NOT NULL,
  `orderid` int(11) NULL DEFAULT NULL,
  `create_time` int(11) NULL DEFAULT NULL,
  `update_time` int(11) NOT NULL DEFAULT 0,
  `deadline` int(11) NULL DEFAULT NULL,
  `courseid` int(11) NULL DEFAULT NULL,
  `courseitem` int(11) NULL DEFAULT NULL,
  `coursename` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `coursesummary` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `courseimage` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `adminid` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of series_access
-- ----------------------------

-- ----------------------------
-- Table structure for series_category
-- ----------------------------
DROP TABLE IF EXISTS `series_category`;
CREATE TABLE `series_category`  (
  `Id` int(11) NOT NULL,
  `Name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `Recommend` tinyint(4) NOT NULL,
  `IsOnly` tinyint(4) NOT NULL
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of series_category
-- ----------------------------

-- ----------------------------
-- Table structure for series_description
-- ----------------------------
DROP TABLE IF EXISTS `series_description`;
CREATE TABLE `series_description`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `keywords` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `description` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `imagepath` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  `views` int(11) NOT NULL,
  `adminid` int(11) NOT NULL,
  `supplierid` int(11) NOT NULL,
  `oldid` int(11) DEFAULT 0,
  `productid` int(11) DEFAULT 0,
  `isfree` int(11) DEFAULT 1,
  `recommend` int(11) DEFAULT 0,
  `categoryid` int(11) NULL DEFAULT NULL,
  `saleprice` decimal(18, 2) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of series_description
-- ----------------------------
INSERT INTO `series_description` VALUES (1, '来自pexels免费素材网的展示图片，仅做系列视频示例使用', 'pexels免费素材网', '来自pexels免费素材网的展示图片，仅做系列视频示例使用。本系列视频主要是为大家展示网课视频以及系列视频在本系统中的展现方式。', '<p>\n	<img src=\"http://cdn.dmake.cn/attachment/images/20210128/16118039913.jpg\" /> \n</p>\n<p>\n	pexels是一家国外的免费素材分享平台，这里有免费的照片图片、视频和音频素材，这些素材来自于独立摄影师、设计师等免费分享。\n</p>\n<p>\n	在中国，很多企业和个人会受到来自于素材资源的困扰，然而在Pexels网站中一切皆免费。您甚至可以集成网站提供的API大量调取，当然这种行为是有数量限制的。\n</p>\n<p>\n	那么，一起来看一看Pexels的视屏以系列形式在本系统的示例展示吧。\n</p>', '//cdn.dmake.cn/attachment/images/20210128/16118039913.jpg', 1573827182, 1611804487, 0, 1, 0, 0, NULL, 1, 1, NULL, 0.00);

-- ----------------------------
-- Table structure for series_group
-- ----------------------------
DROP TABLE IF EXISTS `series_group`;
CREATE TABLE `series_group`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `courseid` int(11) NOT NULL,
  `create_time` int(11) NULL DEFAULT NULL,
  `update_time` int(11) NULL DEFAULT NULL,
  `adminid` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of series_group
-- ----------------------------
INSERT INTO `series_group` VALUES (1, '自然', 1, 1573827390, NULL, 1);
INSERT INTO `series_group` VALUES (2, '城市', 1, 1573828302, NULL, 1);
INSERT INTO `series_group` VALUES (3, '人文', 1, 1573828989, NULL, 1);

-- ----------------------------
-- Table structure for series_item
-- ----------------------------
DROP TABLE IF EXISTS `series_item`;
CREATE TABLE `series_item`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `courseid` int(11) NOT NULL,
  `title` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `keywords` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `description` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `summary` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `duration` int(11) NOT NULL,
  `imagepath` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `resource` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `caption` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  `views` int(11) NOT NULL,
  `needpay` int(11) NOT NULL,
  `price` double NOT NULL DEFAULT 0 COMMENT '显示单价',
  `sort` int(11) NOT NULL,
  `groupid` int(11) NULL DEFAULT NULL,
  `adminid` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of series_item
-- ----------------------------
INSERT INTO `series_item` VALUES (1, 1, '视频模块展示：海滩礁石与浪花', '视频模块展示，海滩礁石与浪花', '视频模块展示，本次展示的是远程地址，这个地址呢也可以是本地的。', '视频模块展示，本次展示的是远程地址，这个地址呢也可以是本地的。', 206, 'http://cdn.dmake.cn/attachment/images/20210128/16118030459.jpg', '//cdn.dmake.cn/bench.mp4', NULL, 1573828046, 1611804987, 0, 0, 0, 0, 1, 1);
INSERT INTO `series_item` VALUES (2, 1, '视频模块展示：夜观银河系', '视频模块展示，夜观银河系', '视频模块展示：夜观银河系，视频可以上传CDN、本地或者引用其他平台地址。', '视频模块展示：夜观银河系，视频可以上传CDN、本地或者引用其他平台地址。', 23, '//cdn.dmake.cn/attachment/images/20210128/16118035855.jpg', '//cdn.dmake.cn/galaxy.mp4', NULL, 1573828222, 1611805039, 0, 0, 0, 0, 1, 1);
INSERT INTO `series_item` VALUES (3, 1, '视频模块展示：城市之夜', '视频模块展示，城市之夜', '视频模块展示：城市之夜，上传CDN，也可以上传本地。', '视频模块展示：城市之夜，上传CDN，也可以上传本地。', 6, '//cdn.dmake.cn/attachment/images/20210128/16118032829.jpg', '//cdn.dmake.cn/citynight.mp4', NULL, 1573828453, 1611805093, 0, 0, 0,  0, 2, 1);
INSERT INTO `series_item` VALUES (4, 1, '视频模块展示：乡村', '视频模块展示，乡村', '视频模块展示：乡村，视频可以上传CDN、本地或者引用其他平台连接', '视频模块展示：乡村，视频可以上传CDN、本地或者引用其他平台连接', 69, '//cdn.dmake.cn/attachment/images/20210128/16118034190.jpg', '//cdn.dmake.cn/countryside.mp4', NULL, 1573828547, 1611805140, 0, 0, 0, 0, 2, 1);
INSERT INTO `series_item` VALUES (5, 1, '视频模块展示：独自旅行', '视频模块展示，独自旅行', '视频模块展示：独自旅行，视频可上传CDN、本地或者其他视频平台。', '视频模块展示：独自旅行，视频可上传CDN、本地或者其他视频平台。', 24, '//cdn.dmake.cn/attachment/images/20210128/16118036919.jpg', '//cdn.dmake.cn/walkalong.mp4', NULL, 1573829098, 1611805299,  0, 0, 0, 0, 3, 1);
INSERT INTO `series_item` VALUES (7, 1, '视频模块展示：雾景观的鸟瞰图', '视频模块展示，雾景观的鸟瞰图', '视频模块展示：雾景观的鸟瞰图，本视频来自Pexels素材Tom fisk仅供展示。', '视频模块展示：雾景观的鸟瞰图，本视频来自Pexels素材Tom fisk仅供展示。', 53, 'http://cdn.dmake.cn/attachment/images/20210128/16118422830.jpg', '//cdn.dmake.cn/forkside.mp4', NULL, 1611842219, 1611842285, 0, 0,  0, 0, 3, 1);
INSERT INTO `series_item` VALUES (8, 1, '视频模块展示：流过森林的小河', '视频模块展示,流过森林的小河', '视频模块展示：流过森林的小河,Pexels展示视频', '视频模块展示：流过森林的小河,Pexels展示视频。', 27, '//cdn.dmake.cn/attachment/images/20210201/16121865313.jpg', '//cdn.dmake.cn/lm5faiLdnS9fXoDEJ9lgLqCwymzS', NULL, 1612186676, 0, 0, 0,  0, 0, 1, 1);

-- ----------------------------
-- Table structure for supplier
-- ----------------------------
DROP TABLE IF EXISTS `supplier`;
CREATE TABLE `supplier`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `website` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `logopath` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `clickcount` int(11) NULL DEFAULT NULL,
  `title` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `keywords` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL DEFAULT 0,
  `status` int(4) NOT NULL,
  `disabledreason` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `announcement` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `adminid` int(11) NOT NULL DEFAULT 0,
  `remark` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of supplier
-- ----------------------------
INSERT INTO `supplier` VALUES (1, '板砖博客', '<p>\n	板砖博客是作者依托于“铜梁视窗CMS”优化而来，根据CMS在资讯网站、小说网站、B2C网站、视频教学网站、资源下载网站、小程序、公众号营销的应用中反复提炼，留下了可扩展性高的PHP企业建站系统。\n</p>\n<p>\n	<strong>适用业务场景</strong> \n</p>\n板砖博客平台系统是基于前后端分离的理念开发而来，经过门户网站、微信公众平台、资讯网站、资源网站和企业站群等多种运营模式和建站模式产品化，方便更多的个人站长和企业能够快速建立互联网品牌。<br />\n<p>\n	<strong>系统特征</strong> \n</p>\n<p>\n	前端采用了Bootstrap模块化布局，业务模块无缝组合；后端采用了CRUD标准化设计，新建业务模块从设计到上线不超过30分钟。\n</p>\n<p>\n	<strong>推荐环境</strong> \n</p>\n<p>\n	系统参数：PHP7.2 + File/Memcache/Redis缓存\n</p>\n<p>\n	推荐系统集成环境：LNMP\n</p>', 'https://www.dmake.cn', '/logo.png', 468, '轻量级PHP企业平台建站系统', 'PHP平台建站', '板砖的自研产品，根据资讯网站、小说网站、B2C网站、视频教学网站、资源下载网站、小程序、公众号营销的应用中反复提炼，是一个可扩展性高的PHP企业建站系统。', 1569426330, 1610507351, 1, NULL, '', 1, NULL);
INSERT INTO `supplier` VALUES (4, '指尖进销存', '指尖进销存是成都寒峰科技有限公司的一款收银软件，<span>成都寒峰科技有限公司</span>成立于2017年,是一家专注移动互联网开发的软件科技公司, 拥有多个自主研发软件产品。现主营产品指尖进销存,已累计服务两万批发零售商家, 帮助中小商家科学管理店铺、提高工作效率、拓展线上营销渠道,助力商家快速步入 移动互联网新时代。', 'https://www.zhijianjxc.com/', 'https://www.zhijianjxc.com/images_web/Home/left-topLOGO.png', NULL, '指尖进销存-成都寒峰科技有限公司', '指尖进销存', '指尖进销存是成都寒峰科技有限公司的一款收银软件，, 帮助中小商家科学管理店铺、提高工作效率、拓展线上营销渠道,助力商家快速步入 移动互联网新时代。', 1631176015, 1631176421, 1, NULL, '', 1, NULL);
INSERT INTO `supplier` VALUES (5, '利亚方舟科技', '专注影楼管理软件研发11年<br />\n*专业高效的百强工程师研发团队<br />\n*全天24小时在线常年无休的售后服务团队<br />\n*全年365天守护在全国各地市场的技术实施团队<br />\n帮助中国影楼管理者进行影楼接单管理系统标准化、影楼流程管理系统标准化、影楼选片管理系统标准化、影楼客户管理系统标准化、影楼会员管理系统标准化、影楼客服管理系统标准化、影楼客资管理系统标准化、影楼礼服管理系统标准化、影楼财务管理系统标准化。<br />', 'https://www.lyfz.net/', '//cdn.dmake.cn/attachment/images/20210923/16323842212.png', NULL, '利亚方舟科技-专注影楼管理软件研发', '', '帮助中国影楼管理者进行影楼接单管理系统标准化、影楼流程管理系统标准化、影楼选片管理系统标准化、影楼客户管理系统标准化、影楼会员管理系统标准化、影楼客服管理系统标准化、影楼客资管理系统标准化、影楼礼服管理系统标准化、影楼财务管理系统标准化。', 1632384353, 0, 1, NULL, '', 1, NULL);

-- ----------------------------
-- Table structure for sys_admin
-- ----------------------------
DROP TABLE IF EXISTS `sys_admin`;
CREATE TABLE `sys_admin`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户名',
  `password` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密码',
  `portrait` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '头像',
  `loginnum` int(11) NULL DEFAULT NULL COMMENT '登陆次数',
  `last_login_ip` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '最后登录IP',
  `last_login_time` int(11) NULL DEFAULT NULL COMMENT '最后登录时间',
  `real_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '真实姓名',
  `status` int(11) NULL DEFAULT NULL COMMENT '状态',
  `groupid` int(11) NULL DEFAULT NULL COMMENT '用户角色id',
  `token` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `tel` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `mid` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 73 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_admin
-- ----------------------------
INSERT INTO `sys_admin` VALUES (1, 'admin', 'dd1f0da638434afd45f27dbeec46ee19', 'loginnum', 521, '127.0.0.1', 1652769955, '管理员', 1, 1, '3188504eafe6e3628d36bd6fa96565fd', 'tel', 'oldid', NULL);

-- ----------------------------
-- Table structure for sys_auth_group
-- ----------------------------
DROP TABLE IF EXISTS `sys_auth_group`;
CREATE TABLE `sys_auth_group`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` char(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `status` tinyint(4) NOT NULL,
  `rules` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `create_time` int(11) NULL DEFAULT NULL,
  `update_time` int(11) NULL DEFAULT NULL,
  `adminid` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_auth_group
-- ----------------------------
INSERT INTO `sys_auth_group` VALUES (1, '超级管理员', 1, '', 1446535750, 1446535750, NULL);
INSERT INTO `sys_auth_group` VALUES (2, '编辑部', 1, '13,14,22,110,146,24,25,40,41,42,43,26,44,45,46,47,98,99,48,49,50,51,52,53,54,55,56,57,58,149,70,71,72,73,74,80,75,76,77,78,79,128,134,81,100,101,104,102,106,103,105,135,139,145,83,84,85,132,147,148,86,87,88,89,90,91,92,97,108,109,111,112,113,114,115,116,117,118,119,120,122,129,136,138,137,131,130', 1446535750, 1581907145, NULL);
INSERT INTO `sys_auth_group` VALUES (3, '销售部', 1, '13,14,22,110,146,70,71,72,73,74,80,75,76,77,78,79,128,134,81,100,101,104,102,106,103,105,135,139,145,82,83,84,85,132,147,148,89,90,108,129,136,138,137,131,130', 1551176804, 1559807678, NULL);
INSERT INTO `sys_auth_group` VALUES (4, '客服部', 1, '70,71,72,73,74,80,75,76,77,78,79,128,134,89,90,108', 1551176813, 1559807710, NULL);
INSERT INTO `sys_auth_group` VALUES (8, '独立站', 1, NULL, 1652409694, 1652409824, 1);

-- ----------------------------
-- Table structure for sys_auth_group_access
-- ----------------------------
DROP TABLE IF EXISTS `sys_auth_group_access`;
CREATE TABLE `sys_auth_group_access`  (
  `uid` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Fixed;

-- ----------------------------
-- Records of sys_auth_group_access
-- ----------------------------
INSERT INTO `sys_auth_group_access` VALUES (65, 2);
INSERT INTO `sys_auth_group_access` VALUES (66, 2);
INSERT INTO `sys_auth_group_access` VALUES (67, 3);
INSERT INTO `sys_auth_group_access` VALUES (68, 4);
INSERT INTO `sys_auth_group_access` VALUES (69, 2);
INSERT INTO `sys_auth_group_access` VALUES (70, 2);
INSERT INTO `sys_auth_group_access` VALUES (71, 6);

-- ----------------------------
-- Table structure for sys_auth_rule
-- ----------------------------
DROP TABLE IF EXISTS `sys_auth_rule`;
CREATE TABLE `sys_auth_rule`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` char(80) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `title` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `type` tinyint(4) NULL DEFAULT NULL,
  `status` tinyint(4) NOT NULL,
  `css` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '样式',
  `condition` char(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `pid` int(11) NOT NULL COMMENT '父栏目ID',
  `sort` int(11) NOT NULL COMMENT '排序',
  `create_time` int(11) NOT NULL COMMENT '添加时间',
  `update_time` int(11) NULL DEFAULT NULL,
  `adminid` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 170 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_auth_rule
-- ----------------------------
INSERT INTO `sys_auth_rule` VALUES (1, '#', '系统管理', 1, 1, 'fa fa-tachometer-alt', '', 0, 0, 1446535750, 1571969006, 0);
INSERT INTO `sys_auth_rule` VALUES (2, 'admin/user/index', '用户管理', 1, 1, 'fas fa-users', '', 1, 10, 1446535750, 1580091662, 0);
INSERT INTO `sys_auth_rule` VALUES (3, 'admin/role/index', '角色管理', 1, 1, 'fas fa-user-check', '', 1, 20, 1446535750, 1580091698, 0);
INSERT INTO `sys_auth_rule` VALUES (4, 'admin/menu/index', '菜单管理', 1, 1, 'fas fa-bars', '', 1, 30, 1446535750, 1580091777, 0);
INSERT INTO `sys_auth_rule` VALUES (5, '#', '数据库管理', 1, 1, 'fa fa-database', '', 0, 1, 1446535750, 1477312169, 0);
INSERT INTO `sys_auth_rule` VALUES (6, 'admin/data/index', '数据库备份', 1, 1, 'fas fa-save', '', 5, 50, 1446535750, 1580092027, 0);
INSERT INTO `sys_auth_rule` VALUES (7, 'admin/data/optimize', '优化表', 1, 1, '', '', 6, 50, 1477312169, 1477312169, 0);
INSERT INTO `sys_auth_rule` VALUES (8, 'admin/data/repair', '修复表', 1, 1, '', '', 6, 50, 1477312169, 1477312169, 0);
INSERT INTO `sys_auth_rule` VALUES (9, 'admin/user/add', '添加用户', 1, 1, '', '', 2, 50, 1477312169, 1477312169, 0);
INSERT INTO `sys_auth_rule` VALUES (10, 'admin/user/edit', '编辑用户', 1, 1, '', '', 2, 50, 1477312169, 1477312169, 0);
INSERT INTO `sys_auth_rule` VALUES (11, 'admin/user/del', '删除用户', 1, 1, '', '', 2, 50, 1477312169, 1477312169, 0);
INSERT INTO `sys_auth_rule` VALUES (12, 'admin/user/state', '用户状态', 1, 1, '', '', 2, 20, 1477312169, 1477312169, 0);
INSERT INTO `sys_auth_rule` VALUES (13, '#', '日志管理', 1, 1, 'fa fa-tasks', '', 0, 14, 1477312169, 1477312169, 0);
INSERT INTO `sys_auth_rule` VALUES (14, 'admin/log/index', '行为日志', 1, 1, 'far fa-file-alt', '', 13, 50, 1477312169, 1580093705, 0);
INSERT INTO `sys_auth_rule` VALUES (22, 'admin/log/del_log', '删除日志', 1, 1, '', '', 14, 50, 1477312169, 1477316778, 0);
INSERT INTO `sys_auth_rule` VALUES (24, '#', '资讯频道', 1, 1, 'fa fa-paste', '', 0, 3, 1477312169, 1582852383, 0);
INSERT INTO `sys_auth_rule` VALUES (25, 'admin/article/index_cate', '文章分类', 1, 1, 'fas fa-sort-alpha-up', '', 24, 10, 1477312260, 1580092499, 0);
INSERT INTO `sys_auth_rule` VALUES (26, 'admin/article/index', '文章列表', 1, 1, 'fas fa-book', '', 24, 20, 1477312333, 1580092549, 0);
INSERT INTO `sys_auth_rule` VALUES (27, 'admin/data/import', '数据库还原', 1, 1, 'far fa-save', '', 5, 50, 1477639870, 1580092061, 0);
INSERT INTO `sys_auth_rule` VALUES (28, 'admin/data/revert', '还原', 1, 1, '', '', 27, 50, 1477639972, 1477639972, 0);
INSERT INTO `sys_auth_rule` VALUES (29, 'admin/data/del', '删除', 1, 1, '', '', 27, 50, 1477640011, 1477640011, 0);
INSERT INTO `sys_auth_rule` VALUES (30, 'admin/role/add', '添加角色', 1, 1, '', '', 3, 50, 1477640011, 1477640011, 0);
INSERT INTO `sys_auth_rule` VALUES (31, 'admin/role/edit', '编辑角色', 1, 1, '', '', 3, 50, 1477640011, 1477640011, 0);
INSERT INTO `sys_auth_rule` VALUES (32, 'admin/role/del', '删除角色', 1, 1, '', '', 3, 50, 1477640011, 1477640011, 0);
INSERT INTO `sys_auth_rule` VALUES (33, 'admin/role/state', '角色状态', 1, 1, '', '', 3, 50, 1477640011, 1477640011, 0);
INSERT INTO `sys_auth_rule` VALUES (34, 'admin/role/giveAccess', '权限分配', 1, 1, '', '', 3, 50, 1477640011, 1477640011, 0);
INSERT INTO `sys_auth_rule` VALUES (35, 'admin/menu/add', '添加菜单', 1, 1, '', '', 4, 50, 1477640011, 1477640011, 0);
INSERT INTO `sys_auth_rule` VALUES (36, 'admin/menu/edit', '编辑菜单', 1, 1, '', '', 4, 50, 1477640011, 1477640011, 0);
INSERT INTO `sys_auth_rule` VALUES (37, 'admin/menu/del', '删除菜单', 1, 1, '', '', 4, 50, 1477640011, 1477640011, 0);
INSERT INTO `sys_auth_rule` VALUES (38, 'admin/menu/state', '菜单状态', 1, 1, '', '', 4, 50, 1477640011, 1477640011, 0);
INSERT INTO `sys_auth_rule` VALUES (39, 'admin/menu/ruleOrderBy', '菜单排序', 1, 1, '', '', 4, 50, 1477640011, 1477640011, 0);
INSERT INTO `sys_auth_rule` VALUES (40, 'admin/article/add_cate', '添加分类', 1, 1, '', '', 25, 50, 1477640011, 1477640011, 0);
INSERT INTO `sys_auth_rule` VALUES (41, 'admin/article/edit_cate', '编辑分类', 1, 1, '', '', 25, 50, 1477640011, 1477640011, 0);
INSERT INTO `sys_auth_rule` VALUES (42, 'admin/article/del_cate', '删除分类', 1, 1, '', '', 25, 50, 1477640011, 1477640011, 0);
INSERT INTO `sys_auth_rule` VALUES (43, 'admin/article/cate_state', '分类状态', 1, 1, '', '', 25, 50, 1477640011, 1477640011, 0);
INSERT INTO `sys_auth_rule` VALUES (44, 'admin/article/add_article', '添加文章', 1, 1, '', '', 26, 50, 1477640011, 1477640011, 0);
INSERT INTO `sys_auth_rule` VALUES (45, 'admin/article/edit_article', '编辑文章', 1, 1, '', '', 26, 50, 1477640011, 1477640011, 0);
INSERT INTO `sys_auth_rule` VALUES (46, 'admin/article/del_article', '删除文章', 1, 1, '', '', 26, 50, 1477640011, 1477640011, 0);
INSERT INTO `sys_auth_rule` VALUES (47, 'admin/article/article_state', '文章状态', 1, 1, '', '', 26, 50, 1477640011, 1477640011, 0);
INSERT INTO `sys_auth_rule` VALUES (48, '#', '广告管理', 1, 1, 'fa fa-image', '', 0, 0, 1477640011, 1477640011, 0);
INSERT INTO `sys_auth_rule` VALUES (49, 'admin/ad/index_position', '广告位', 1, 1, 'fas fa-route', '', 48, 10, 1477640011, 1580092664, 0);
INSERT INTO `sys_auth_rule` VALUES (50, 'admin/ad/add_position', '添加广告位', 1, 1, '', '', 49, 50, 1477640011, 1547780866, 0);
INSERT INTO `sys_auth_rule` VALUES (51, 'admin/ad/edit_position', '编辑广告位', 1, 1, '', '', 49, 50, 1477640011, 1547780886, 0);
INSERT INTO `sys_auth_rule` VALUES (52, 'admin/ad/del_position', '删除广告位', 1, 1, '', '', 49, 50, 1477640011, 1547780897, 0);
INSERT INTO `sys_auth_rule` VALUES (53, 'admin/ad/position_state', '广告位状态', 1, 1, '', '', 49, 50, 1477640011, 1547780911, 0);
INSERT INTO `sys_auth_rule` VALUES (54, 'admin/ad/index', '广告列表', 1, 1, 'fas fa-image', '', 48, 20, 1477640011, 1580092692, 0);
INSERT INTO `sys_auth_rule` VALUES (55, 'admin/ad/add', '添加广告', 1, 1, '', '', 54, 50, 1477640011, 1547780968, 0);
INSERT INTO `sys_auth_rule` VALUES (56, 'admin/ad/edit', '编辑广告', 1, 1, '', '', 54, 50, 1477640011, 1547780982, 0);
INSERT INTO `sys_auth_rule` VALUES (57, 'admin/ad/del', '删除广告', 1, 1, '', '', 54, 50, 1477640011, 1547780998, 0);
INSERT INTO `sys_auth_rule` VALUES (58, 'admin/ad/state', '广告状态', 1, 1, '', '', 54, 50, 1477640011, 1547781009, 0);
INSERT INTO `sys_auth_rule` VALUES (61, 'admin/config/index', '配置管理', 1, 1, 'fas fa-cog', '', 1, 50, 1479908607, 1580091799, 0);
INSERT INTO `sys_auth_rule` VALUES (62, 'admin/config/index', '配置列表', 1, 1, '', '', 61, 50, 1479908607, 1487943813, 0);
INSERT INTO `sys_auth_rule` VALUES (63, 'admin/config/save', '保存配置', 1, 1, '', '', 61, 50, 1479908607, 1487943831, 0);
INSERT INTO `sys_auth_rule` VALUES (70, '#', '会员频道', 1, 1, 'fas fa-user-alt', '', 0, 5, 1484103066, 1582852393, 0);
INSERT INTO `sys_auth_rule` VALUES (71, 'admin/member/group', '会员组', 1, 1, 'fas fa-users-cog', '', 70, 10, 1484103304, 1580092813, 0);
INSERT INTO `sys_auth_rule` VALUES (72, 'admin/member/add_group', '添加会员组', 1, 1, '', '', 71, 50, 1484103304, 1484103304, 0);
INSERT INTO `sys_auth_rule` VALUES (73, 'admin/member/edit_group', '编辑会员组', 1, 1, '', '', 71, 50, 1484103304, 1484103304, 0);
INSERT INTO `sys_auth_rule` VALUES (74, 'admin/member/del_group', '删除会员组', 1, 1, '', '', 71, 50, 1484103304, 1484103304, 0);
INSERT INTO `sys_auth_rule` VALUES (75, 'admin/member/index', '会员列表', 1, 1, 'fas fa-users', '', 70, 20, 1484103304, 1580092806, 0);
INSERT INTO `sys_auth_rule` VALUES (76, 'admin/member/add_member', '添加会员', 1, 1, '', '', 75, 50, 1484103304, 1484103304, 0);
INSERT INTO `sys_auth_rule` VALUES (77, 'admin/member/edit_member', '编辑会员', 1, 1, '', '', 75, 50, 1484103304, 1484103304, 0);
INSERT INTO `sys_auth_rule` VALUES (78, 'admin/member/del_member', '删除会员', 1, 1, '', '', 75, 50, 1484103304, 1484103304, 0);
INSERT INTO `sys_auth_rule` VALUES (79, 'admin/member/member_status', '会员状态', 1, 1, '', '', 75, 50, 1484103304, 1487937671, 0);
INSERT INTO `sys_auth_rule` VALUES (80, 'admin/member/group_status', '会员组状态', 1, 1, '', '', 71, 50, 1484103304, 1484103304, 0);
INSERT INTO `sys_auth_rule` VALUES (81, 'admin/commodity/index', '供应链频道', 1, 1, 'fa fa-server', '', 0, 2, 1547107209, 1582852370, 0);
INSERT INTO `sys_auth_rule` VALUES (83, 'admin/activity/campaigns', '活动管理', 1, 1, 'fa fa-object-group', '', 0, 12, 1547108111, 1547108111, 0);
INSERT INTO `sys_auth_rule` VALUES (84, 'admin/activity/campaigns', '营销活动', 1, 1, '', '', 83, 4, 1547108137, 1547108137, 0);
INSERT INTO `sys_auth_rule` VALUES (85, 'admin/activity/topics', '资源合集', 1, 1, '', '', 83, 3, 1547108158, 1547108158, 0);
INSERT INTO `sys_auth_rule` VALUES (86, 'admin/cloud/manage', 'CDN管理', 1, 1, 'fas fa-server', '', 0, 13, 1547108214, 1608705173, 0);
INSERT INTO `sys_auth_rule` VALUES (87, 'admin/cloud/qiniu', '七牛上传', 1, 1, 'fas fa-cloud-upload-', '', 86, 50, 1547108247, 1608707204, 0);
INSERT INTO `sys_auth_rule` VALUES (88, 'admin/cloud/upai', '又拍云上传', 1, 1, 'fas fa-cloud-upload-', '', 86, 50, 1547108274, 1608707215, 0);
INSERT INTO `sys_auth_rule` VALUES (89, 'admin/report/index', '报表统计', 1, 1, 'fa fa-table', '', 0, 0, 1547108342, 1547108342, 0);
INSERT INTO `sys_auth_rule` VALUES (90, 'admin/record/index', '下载报表', 1, 1, '', '', 89, 0, 1547108378, 1558663578, 0);
INSERT INTO `sys_auth_rule` VALUES (91, 'admin/report/videohistory', '视频报表', 1, 1, '', '', 89, 4, 1547108412, 1560410239, 0);
INSERT INTO `sys_auth_rule` VALUES (93, '#', '论坛管理', 1, 1, 'fab fa-forumbee', '', 0, 8, 1547108511, 1571969343, 0);
INSERT INTO `sys_auth_rule` VALUES (94, 'admin/forum/index', '版块管理', 1, 1, 'fas fa-th-large', '', 93, 50, 1547108709, 1580092976, 0);
INSERT INTO `sys_auth_rule` VALUES (95, 'admin/forum/threads', '主题管理', 1, 1, 'fas fa-tasks', '', 93, 50, 1547108777, 1580092998, 0);
INSERT INTO `sys_auth_rule` VALUES (98, 'admin/article/serializes', '文章连载', 1, 1, 'far fa-bookmark', '', 24, 50, 1547541611, 1580092617, 0);
INSERT INTO `sys_auth_rule` VALUES (99, 'admin/article/serializes', '连载清单', 1, 1, '', '', 98, 50, 1547542295, 1547542295, 0);
INSERT INTO `sys_auth_rule` VALUES (101, 'admin/commodity/suppliers', '合作伙伴', 1, 1, 'fas fa-handshake', '', 81, 0, 1548061435, 1580092087, 0);
INSERT INTO `sys_auth_rule` VALUES (102, 'admin/commodity/products', '产品列表', 1, 1, 'fas fa-grip-vertical', '', 81, 1, 1548061464, 1580092131, 0);
INSERT INTO `sys_auth_rule` VALUES (103, 'admin/commodity/catagories', '产品分类', 1, 1, 'fas fa-sort-alpha-up', '', 81, 2, 1548061499, 1580092167, 0);
INSERT INTO `sys_auth_rule` VALUES (104, 'admin/commodity/saveSupplier', '保存供应商', 1, 1, '', '', 101, 50, 1548061540, 1574141712, 0);
INSERT INTO `sys_auth_rule` VALUES (105, 'admin/commodity/saveCatagory', '保存分类', 1, 1, '', '', 103, 50, 1548061590, 1574141979, 0);
INSERT INTO `sys_auth_rule` VALUES (106, 'admin/commodity/saveProduct', '保存产品', 1, 1, '', '', 102, 50, 1548061611, 1574141887, 0);
INSERT INTO `sys_auth_rule` VALUES (107, 'admin/cachemanage/index', '缓存刷新', 1, 1, 'fas fa-eraser', '', 1, 50, 1548207408, 1580091822, 0);
INSERT INTO `sys_auth_rule` VALUES (108, 'admin/report/pv', '流量报表', 1, 1, '', '', 89, 1, 1550725532, 1550725532, 0);
INSERT INTO `sys_auth_rule` VALUES (109, 'admin/report/workanly', '工作统计', 1, 1, '', '', 89, 3, 1550728886, 1550728886, 0);
INSERT INTO `sys_auth_rule` VALUES (110, 'admin/log/errorlog', '错误日志', 1, 1, 'fas fa-bug', '', 13, 50, 1551157081, 1651049136, 1);
INSERT INTO `sys_auth_rule` VALUES (111, 'admin/report/pvsource', '流量碎片', 1, 1, '', '', 89, 2, 1551158810, 1551158810, 0);
INSERT INTO `sys_auth_rule` VALUES (112, '#', '下载频道', 1, 1, 'fa fa-hashtag', '', 0, 4, 1551330336, 1582852561, 0);
INSERT INTO `sys_auth_rule` VALUES (113, 'admin/tags/index', '标签管理', 1, 1, 'fas fa-bookmark', '', 112, 50, 1551330398, 1580092714, 0);
INSERT INTO `sys_auth_rule` VALUES (114, 'admin/tags/savetag', '保存标签', 1, 1, '', '', 113, 50, 1551330427, 1551330427, 0);
INSERT INTO `sys_auth_rule` VALUES (115, 'admin/tags/sources', '标签关联', 1, 1, '', '', 113, 50, 1551330458, 1551330458, 0);
INSERT INTO `sys_auth_rule` VALUES (116, 'admin/tags/sourcerelation', '关联标签', 1, 1, '', '', 113, 50, 1551330489, 1551330489, 0);
INSERT INTO `sys_auth_rule` VALUES (117, 'admin/video/video', '独立视频', 1, 1, 'fas fa-play-circle', '', 129, 50, 1551675530, 1582853017, 0);
INSERT INTO `sys_auth_rule` VALUES (118, 'admin/video/savevideo', '保存视频', 1, 1, '', '', 117, 50, 1551675550, 1551675550, 0);
INSERT INTO `sys_auth_rule` VALUES (119, 'admin/resource/attachments', '附件管理', 1, 1, 'fas fa-archive', '', 112, 50, 1551675582, 1580092761, 0);
INSERT INTO `sys_auth_rule` VALUES (120, 'admin/resource/saveatta', '保存附件', 1, 1, '', '', 119, 50, 1551675604, 1551675604, 0);
INSERT INTO `sys_auth_rule` VALUES (121, 'admin/resource/savefile', '保存文档', 1, 1, '', '', 121, 50, 1551675643, 1551675643, 0);
INSERT INTO `sys_auth_rule` VALUES (123, 'admin/resource/savedemo', '保存demo', 1, 1, '', '', 123, 50, 1552271694, 1552271694, 0);
INSERT INTO `sys_auth_rule` VALUES (124, 'admin/forum/posts', '回帖审核', 1, 1, 'fas fa-paper-plane', '', 93, 50, 1553581927, 1580093011, 0);
INSERT INTO `sys_auth_rule` VALUES (126, 'admin/sites/index', '绑定域名', 1, 1, '', '', 126, 50, 1553754714, 1553754714, 0);
INSERT INTO `sys_auth_rule` VALUES (127, 'admin/sites/articles', '绑定文章', 1, 1, '', '', 126, 50, 1553754736, 1553754736, 0);
INSERT INTO `sys_auth_rule` VALUES (128, 'admin/member/course', '视频会员', 1, 1, 'fas fa-rss-square', '', 70, 50, 1553831833, 1651049216, 1);
INSERT INTO `sys_auth_rule` VALUES (129, '#', '视频频道', 1, 1, 'fab fa-leanpub', '', 0, 11, 1553839440, 1582852682, 0);
INSERT INTO `sys_auth_rule` VALUES (132, 'admin/coupon/index', '造优惠券', 1, 1, '', '', 83, 0, 1555557985, 1558587118, 0);
INSERT INTO `sys_auth_rule` VALUES (134, 'admin/orders/index', '会员订单', 1, 1, 'fab fa-alipay', '', 70, 50, 1556182312, 1580092907, 0);
INSERT INTO `sys_auth_rule` VALUES (135, 'admin/commodityrecommend/index', '分类推荐', 1, 1, 'fas fa-thumbs-up', '', 81, 3, 1556270324, 1652669545, 1);
INSERT INTO `sys_auth_rule` VALUES (136, 'admin/video/index', '系列视频', NULL, 1, 'fas fa-tv', NULL, 129, 50, 1556506573, 1582852713, 0);
INSERT INTO `sys_auth_rule` VALUES (138, 'admin/video/charts', '课程章节', NULL, 1, '', NULL, 136, 50, 1556506644, 1556506644, 0);
INSERT INTO `sys_auth_rule` VALUES (139, 'admin/portalhome/index', '板块推荐', NULL, 1, 'far fa-thumbs-up', NULL, 81, 5, 1557023108, 1580092468, 0);
INSERT INTO `sys_auth_rule` VALUES (140, 'admin/index/mailtest', '邮件测试', NULL, 1, 'far fa-envelope', NULL, 1, 50, 1557214192, 1580091837, 0);
INSERT INTO `sys_auth_rule` VALUES (142, '#', '公众平台', NULL, 1, 'fab fa-weixin', NULL, 0, 10, 1557370149, 1580453501, 0);
INSERT INTO `sys_auth_rule` VALUES (143, 'admin/wemenu/index', '菜单管理', NULL, 1, 'fas fa-th-list', NULL, 142, 50, 1557370193, 1580093317, 0);
INSERT INTO `sys_auth_rule` VALUES (144, 'admin/index/pushtobaidu', '百度推送', NULL, 1, 'fas fa-paw', NULL, 1, 50, 1557743534, 1580093538, 0);
INSERT INTO `sys_auth_rule` VALUES (145, 'admin/commodity/pricelist', '产品售价', NULL, 1, 'fas fa-yen-sign', NULL, 81, 4, 1557826285, 1609213672, 0);
INSERT INTO `sys_auth_rule` VALUES (146, 'admin/sysfaq/index', '系统说明', NULL, 1, 'fas fa-rss-square', NULL, 13, 50, 1558074977, 1580093768, 0);
INSERT INTO `sys_auth_rule` VALUES (147, 'admin/coupon/membercoupon', '发优惠券', NULL, 1, '', NULL, 83, 1, 1558587157, 1558676860, 0);
INSERT INTO `sys_auth_rule` VALUES (150, 'admin/weprogram/mincode', '小程序', NULL, 1, 'fas fa-comment', NULL, 142, 50, 1568099871, 1580093352, 0);
INSERT INTO `sys_auth_rule` VALUES (151, 'admin/wekeywords/index', '关键词', NULL, 1, 'fas fa-comments', NULL, 142, 50, 1568099916, 1580093371, 0);
INSERT INTO `sys_auth_rule` VALUES (152, 'admin/sitemap/index', '站点地图', NULL, 1, 'fas fa-sitemap', NULL, 1, 50, 1571810649, 1580091937, 0);
INSERT INTO `sys_auth_rule` VALUES (155, 'admin/sysseo/index', 'SEO管理', NULL, 1, 'fab fa-google', NULL, 1, 50, 1573111971, 1573111971, 0);
INSERT INTO `sys_auth_rule` VALUES (156, 'admin/weprogram/tknav', '小程序导航', NULL, 1, 'fas fa-globe', NULL, 142, 0, 1575596632, 1580093199, 1);
INSERT INTO `sys_auth_rule` VALUES (157, 'admin/user/gantt', '营销日历', NULL, 1, '', NULL, 89, 0, 1589189187, NULL, 1);
INSERT INTO `sys_auth_rule` VALUES (167, 'admin/baseforminit/index', '后端表单生成', NULL, 0, 'fas fa-table', NULL, 1, 0, 1648013597, NULL, 1);
INSERT INTO `sys_auth_rule` VALUES (168, 'admin/addons/index', '插件管理', NULL, 1, 'fas fa-plug', NULL, 1, 60, 1651036407, 1651041630, 1);
INSERT INTO `sys_auth_rule` VALUES (169, 'admin/apiaccount/index', '接口账号', NULL, 1, 'fas fa-user-lock', NULL, 1, 60, 1652412279, 1652412308, 1);

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '配置名称',
  `value` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '配置值',
  `channel` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 15 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES (1, 'web_site_title', '板砖博客', 'site');
INSERT INTO `sys_config` VALUES (2, 'web_site_description', '', 'site');
INSERT INTO `sys_config` VALUES (3, 'web_site_keyword', '', 'site');
INSERT INTO `sys_config` VALUES (4, 'web_site_icp', '', 'site');
INSERT INTO `sys_config` VALUES (5, 'web_site_cnzz', '', 'site');
INSERT INTO `sys_config` VALUES (6, 'web_site_copy', '板砖博客 版权所有 Copyright 2015-2019', 'site');
INSERT INTO `sys_config` VALUES (7, 'web_site_close', '1', 'site');
INSERT INTO `sys_config` VALUES (8, 'list_rows', '10', 'site');
INSERT INTO `sys_config` VALUES (9, 'admin_allow_ip', '', 'site');
INSERT INTO `sys_config` VALUES (12, 'alisms_appkey', '', 'site');
INSERT INTO `sys_config` VALUES (13, 'alisms_appsecret', '', 'site');
INSERT INTO `sys_config` VALUES (14, 'alisms_signname', '', 'site');

-- ----------------------------
-- Table structure for sys_editpage
-- ----------------------------
DROP TABLE IF EXISTS `sys_editpage`;
CREATE TABLE `sys_editpage`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `adminid` int(11) NOT NULL,
  `create_time` int(11) NOT NULL,
  `bakpath` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `note` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_editpage
-- ----------------------------

-- ----------------------------
-- Table structure for sys_faq
-- ----------------------------
DROP TABLE IF EXISTS `sys_faq`;
CREATE TABLE `sys_faq`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `keywords` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `description` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `detail` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `create_time` int(11) NULL DEFAULT NULL,
  `update_time` int(11) NULL DEFAULT NULL,
  `delete_time` int(11) NULL DEFAULT NULL,
  `createdby` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `solutedby` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `solution` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 42 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_faq
-- ----------------------------
INSERT INTO `sys_faq` VALUES (1, '图片LOGO等尺寸标准', NULL, '文章、产品、供应商等一系列网站内容封面图片和内容图片的标准', '1. 文章封面图、产品封面图尺寸为360*240px\r\n2. 供应商logo尺寸为242*150px\r\n3.文章内容图片尺寸宽要小于800px\r\n4. 所有图片必须按照全路径保存，如果是保存云盘择保存http路径，如果是保存本地择是从根目录开始记录', 1558075117, NULL, NULL, NULL, NULL, '系统说明', '');
INSERT INTO `sys_faq` VALUES (2, '系统运行环境', NULL, '本篇说明在windows环境下的配置', '1. php7.2\r\n2. memcached扩展\r\n3. sqlsrv nts扩展', 1558075352, NULL, NULL, NULL, NULL, '系统说明', '');
INSERT INTO `sys_faq` VALUES (3, 'memcache错误提示的解决办法', NULL, '提示memcache:get错误', '这种错误一般是由于memcache未启动或无反应造成\r\n1. 如果是未启动状态，全站都会爆发这个错误\r\n2. 如果只是偶尔无反应，那只有小部分页面出现问题', 1558075510, 1558075570, NULL, NULL, 'admin', '操作系统BUG', '进入服务器打开cmd输入\r\ncd c:/memcached\r\nmemcached.exe -d stop\r\nmemcached.exe -d start\r\n以上命令是为了重启服务');

-- ----------------------------
-- Table structure for sys_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_log`;
CREATE TABLE `sys_log`  (
  `log_id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_id` int(11) NULL DEFAULT NULL COMMENT '用户ID',
  `admin_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户姓名',
  `description` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '描述',
  `ip` char(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'IP地址',
  `status` tinyint(4) NULL DEFAULT NULL COMMENT '1 成功 2 失败',
  `add_time` int(11) NULL DEFAULT NULL COMMENT '添加时间',
  PRIMARY KEY (`log_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1019 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_log
-- ----------------------------

-- ----------------------------
-- Table structure for sys_seo
-- ----------------------------
DROP TABLE IF EXISTS `sys_seo`;
CREATE TABLE `sys_seo`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `controller` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `action` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `title` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `keywords` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `description` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  `adminid` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 13 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_seo
-- ----------------------------
INSERT INTO `sys_seo` VALUES (1, 'achive', 'article', 'index', '博客首页', '板砖博客，开发教程，运营经验', '板砖博客资讯频道为开发者和运营者分享我们的相关经验。', 1573112093, 0, 1);
INSERT INTO `sys_seo` VALUES (2, 'forum', 'index', 'index', '板砖博客建站技术交流中心，php插件教程，PHP|MYSQL|DIV+CSS', 'php插件教程,板砖博客', '板砖博客建站技术交流中心，来都来了，一起来写php插件教程吧，PHP|MYSQL|DIV+CSS也不要放过！', 1573112190, 0, 1);
INSERT INTO `sys_seo` VALUES (3, 'forum', 'index', 'newthread', '发帖', ' ', ' ', 1573112241, 1573112395, 1);
INSERT INTO `sys_seo` VALUES (5, 'index', 'index', 'index', 'DMAKE轻量级PHP企业建站系统完全开源免费', '板砖博客，DMAKE，轻量级PHP,php开源系统，平台建设，运营运营方法', 'DMAKE轻量级PHP企业建站系统基于Thinkphp5和Mysql专为企业级门户网站、处置领域内容发布、商城平台快速建设和拓展微信公众号小程序，适用于企业级运营平台，集合了平台基础内容发布、PC和微信活动、淘宝客、小程序API、流量分析等全套必备功能。', 1573478145, 1610504543, 1);
INSERT INTO `sys_seo` VALUES (4, 'forum', 'index', 'threads', '板砖博客建站技术交流中心，php插件教程，PHP|MYSQL|DIV+CSS', 'php插件教程,板砖博客', '板砖博客建站技术交流中心，来都来了，一起来写php插件教程吧，PHP|MYSQL|DIV+CSS也不要放过！', 1573112286, 0, 1);
INSERT INTO `sys_seo` VALUES (6, 'apps', 'fontawesome', 'fontawesome', 'Fontawesome4.7图标合集中文站，Fonta wesome4.7下载, Fonta wesome 4.7 CDN，Web开发Icon选择助手', 'Fontawesome', 'Fontawesome最新的4.7.0版，收录了675个图标…。', 1573478179, 1574840734, 1);
INSERT INTO `sys_seo` VALUES (7, 'apps', 'fontawesome', 'fontawesomeplus', 'Fontawesome4.7合集放大版中文站，Fonta wesome4.7下载, Fonta wesome 4.7 CDN，Web开发Icon选择助手', 'Fontawesome', 'Fontawesome最新的4.7.0版，收录了675个图标…。', 1573478221, 1574840716, 1);
INSERT INTO `sys_seo` VALUES (8, 'apps', 'fontawesome', 'fontawesomev', 'Fontawesome5.12字体图标中文网站，Fonta wesome5.11下载, Fonta wesome 5.11 CDN，Web开发Icon选择助手2020.1.14更新', 'Fontawesome', 'Fontawesome字体图标最新的5.12版，收录了1553 个免费图标…。', 1573478254, 1579009800, 1);
INSERT INTO `sys_seo` VALUES (9, 'forum', 'index', 'group', '板砖博客建站技术交流中心，php插件教程，PHP|MYSQL|DIV+CSS', 'php插件教程,板砖博客', '板砖博客建站技术交流中心，来都来了，一起来写php插件教程吧，PHP|MYSQL|DIV+CSS也不要放过！', 1574237641, 0, 1);
INSERT INTO `sys_seo` VALUES (10, 'apps', 'bullshit', 'index', 'BullshitGenerator个人总结年终总结活动总结自我检讨自我批评生成器', 'BullshitGenerator,个人总结,年终总结,活动总结,自我检讨,自我批评,生成器', 'BullshitGenerator个人总结年终总结活动总结自我检讨自我批评生成器', 1574843854, 0, 1);
INSERT INTO `sys_seo` VALUES (11, 'archive', 'article', 'index', '网站运营经验分享与php开源开发', '网站运营，php开源', '碎片式网站运营经验分享与php开源开发经验分享。', 1580283727, 1580283971, 1);
INSERT INTO `sys_seo` VALUES (12, 'mall', 'index', 'index', '国产软件商城_ERP_收银软件_OA软件_免费宣传平台', '国产软件商城，ERP，收银软件,OA软件', '满足国产软件的宣传、方便企业获得更多的国产软件信息对比，打破信息部平台！国产软件商城分享国内自研ERP、收银软件、OA软件，欢迎企业免费入驻。', 1631173717, 0, 1);

-- ----------------------------
-- Table structure for tags
-- ----------------------------
DROP TABLE IF EXISTS `tags`;
CREATE TABLE `tags`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `type` int(11) NOT NULL,
  `number` int(11) NOT NULL,
  `logincheck` int(11) NOT NULL,
  `keywords` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `description` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `IX_Tags_Name`(`name`) USING BTREE,
  INDEX `IX_Tags_Type`(`type`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tags
-- ----------------------------

-- ----------------------------
-- Table structure for tags_relation
-- ----------------------------
DROP TABLE IF EXISTS `tags_relation`;
CREATE TABLE `tags_relation`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `oid` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `tid` int(11) NOT NULL,
  `targettype` int(11) NOT NULL,
  `signtime` datetime(0) NOT NULL,
  `signtype` int(11) NOT NULL,
  `accounttype` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `IX_Object_Tag_Relation_oid`(`oid`) USING BTREE,
  INDEX `IX_Object_Tag_Relation_uid`(`uid`) USING BTREE,
  INDEX `IX_Object_Tag_Relation_tid`(`tid`) USING BTREE,
  INDEX `IXtarget_type`(`targettype`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tags_relation
-- ----------------------------

-- ----------------------------
-- Table structure for topic
-- ----------------------------
DROP TABLE IF EXISTS `topic`;
CREATE TABLE `topic`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `keywords` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `context` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `categoryid` int(11) NOT NULL,
  `supplierid` int(11) NOT NULL,
  `logincheck` int(11) NOT NULL,
  `imagepath` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `sort` int(11) NOT NULL,
  `create_time` int(11) NULL DEFAULT NULL,
  `update_time` int(11) NULL DEFAULT NULL,
  `delete_time` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of topic
-- ----------------------------

-- ----------------------------
-- Table structure for topic_partition
-- ----------------------------
DROP TABLE IF EXISTS `topic_partition`;
CREATE TABLE `topic_partition`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `topicid` int(11) NULL DEFAULT NULL,
  `partitionid` int(11) NOT NULL,
  `otype` int(11) NOT NULL,
  `oid` int(11) NOT NULL,
  `title` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `url` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_time` int(11) NULL DEFAULT NULL,
  `update_time` int(11) NULL DEFAULT NULL,
  `delete_time` int(11) NULL DEFAULT NULL,
  `imagepath` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of topic_partition
-- ----------------------------

-- ----------------------------
-- Table structure for topic_partitionmodule
-- ----------------------------
DROP TABLE IF EXISTS `topic_partitionmodule`;
CREATE TABLE `topic_partitionmodule`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `topicid` int(11) NOT NULL,
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `imagepath` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `context` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of topic_partitionmodule
-- ----------------------------

-- ----------------------------
-- Table structure for video
-- ----------------------------
DROP TABLE IF EXISTS `video`;
CREATE TABLE `video`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `duration` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `resource` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `islocal` tinyint(4) NOT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `keywords` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `imagepath` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `clickcount` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `headlinetime` datetime(0) NULL DEFAULT NULL,
  `productid` int(11) NOT NULL,
  `adminid` int(11) NULL DEFAULT NULL,
  `create_time` int(11) NULL DEFAULT NULL,
  `update_time` int(11) NOT NULL,
  `delete_time` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of video
-- ----------------------------
INSERT INTO `video` VALUES (1, '视频模块展示：海滩礁石与浪花', '视频模块展示，本次展示的是远程地址，这个地址呢也可以是本地的。', '3分26秒', '//cdn.dmake.cn/bench.mp4', 0, '视频模块展示，本次展示的是远程地址，这个地址呢也可以是本地的。', '视频模块展示：海滩礁石与浪花', '//cdn.dmake.cn/attachment/images/20210128/16118030459.jpg', 98, 0, '0000-00-00 00:00:00', 1, 1, 1573134196, 1611804925, NULL);
INSERT INTO `video` VALUES (2, '视频模块展示：城市之夜', '视频模块展示：城市之夜，上传CDN，也可以上传本地。', '6秒', '//cdn.dmake.cn/citynight.mp4', 0, '视频模块展示：城市之夜，上传CDN，也可以上传本地。', '视频模块展示：城市之夜', '//cdn.dmake.cn/attachment/images/20210128/16118032829.jpg', 9, 0, '0000-00-00 00:00:00', 1, 1, 1611803338, 0, NULL);
INSERT INTO `video` VALUES (3, '视频模块展示：乡村', '视频模块展示：乡村，视频可以上传CDN、本地或者引用其他平台连接', '1分9秒', '//cdn.dmake.cn/countryside.mp4', 0, '视频模块展示：乡村，视频可以上传CDN、本地或者引用其他平台连接', '视频模块展示：乡村', '//cdn.dmake.cn/attachment/images/20210128/16118034190.jpg', 17, 0, '0000-00-00 00:00:00', 1, 1, 1611803466, 0, NULL);
INSERT INTO `video` VALUES (4, '视频模块展示：夜观银河系', '视频模块展示：夜观银河系，视频可以上传CDN、本地或者引用其他平台地址。', '23秒', '//cdn.dmake.cn/galaxy.mp4', 0, '视频模块展示：夜观银河系，视频可以上传CDN、本地或者引用其他平台地址。', '', '//cdn.dmake.cn/attachment/images/20210128/16118035855.jpg', 4, 0, '0000-00-00 00:00:00', 1, 1, 1611803607, 0, NULL);
INSERT INTO `video` VALUES (5, '视频模块展示：独自旅行', '视频模块展示：独自旅行，视频可上传CDN、本地或者其他视频平台。', '24秒', '//cdn.dmake.cn/walkalong.mp4', 0, '视频模块展示：独自旅行，视频可上传CDN、本地或者其他视频平台。', '', '//cdn.dmake.cn/attachment/images/20210128/16118036919.jpg', 11, 0, '0000-00-00 00:00:00', 1, 1, 1611803725, 0, NULL);
INSERT INTO `video` VALUES (6, '视频模块展示：雾景观的鸟瞰图', '视频模块展示：雾景观的鸟瞰图，本视频来自Pexels素材Tom fisk仅供展示。', '53秒', '//cdn.dmake.cn/forkside.mp4', 0, '视频模块展示：雾景观的鸟瞰图，本视频来自Pexels素材Tom fisk仅供展示。', '视频模块展示，雾景观的鸟瞰图', 'http://cdn.dmake.cn/attachment/images/20210128/16118421101.jpg', 1, 0, '0000-00-00 00:00:00', 0, 1, 1611842152, 0, NULL);
INSERT INTO `video` VALUES (7, '视频模块展示：流过森林的小河', '视频模块展示：流过森林的小河,Pexels展示视频。', '27秒', '//cdn.dmake.cn/lm5faiLdnS9fXoDEJ9lgLqCwymzS', 0, '视频模块展示：流过森林的小河,Pexels展示视频', '视频模块展示,流过森林的小河', '//cdn.dmake.cn/attachment/images/20210201/16121865313.jpg', 4, 0, '0000-00-00 00:00:00', 1, 1, 1612186669, 0, NULL);

-- ----------------------------
-- Table structure for video_history
-- ----------------------------
DROP TABLE IF EXISTS `video_history`;
CREATE TABLE `video_history`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customerid` int(11) NOT NULL,
  `videoid` int(11) NOT NULL,
  `productid` int(11) NULL DEFAULT NULL,
  `supplierid` int(11) NULL DEFAULT NULL,
  `create_time` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Fixed;

-- ----------------------------
-- Records of video_history
-- ----------------------------

-- ----------------------------
-- Table structure for yun_source
-- ----------------------------
DROP TABLE IF EXISTS `yun_source`;
CREATE TABLE `yun_source`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `title` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL DEFAULT 0,
  `adminid` int(11) NULL DEFAULT 0,
  `groupid` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 206 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of yun_source
-- ----------------------------
INSERT INTO `yun_source` VALUES (44, '//cdn.dmake.cn/attachment/keditor/image/20191030/20191030080206_11901.png', 'image', '轻论坛模式，简约而不简单', 1572422528, 1577251471, 0, 1);
INSERT INTO `yun_source` VALUES (116, '//cdn.dmake.cn/attachment/keditor/image/20191226/20191226022408_38918.png', 'image', 'CDN那么多，怎样批量管理七牛云、易速云、又拍云等多个CDN资源解决方案', 1577327049, 0, 0, 0);
INSERT INTO `yun_source` VALUES (50, '//cdn.dmake.cn/attachment/keditor/image/20191101/20191101052313_13001.png', 'image', '.NET代码生成器解析，10年前是这样解决数据底层和模型的自动生成的', 1572585795, 0, 0, 0);
INSERT INTO `yun_source` VALUES (57, '//cdn.dmake.cn/attachment/keditor/image/20191112/20191112195729_20258.jpg', 'image', '优秀文案不能流水线产出，只有走心和投入才有可能', 1573559850, 0, 0, 0);
INSERT INTO `yun_source` VALUES (58, '//cdn.dmake.cn/attachment/keditor/image/20191112/20191112195759_70983.jpg', 'image', '优秀文案不能流水线产出，只有走心和投入才有可能', 1573559879, 0, 0, 0);
INSERT INTO `yun_source` VALUES (61, '//cdn.dmake.cn/attachment/keditor/image/20191115/20191115221207_19581.jpg', 'image', '', 1573827127, 0, 0, 0);
INSERT INTO `yun_source` VALUES (69, '//cdn.dmake.cn/attachment/keditor/image/20191117/20191117155316_55237.png', 'image', '电商平台真的还是停留在卖货的阶段吗？不，电商早已开启带货模式！', 1573977197, 0, 0, 0);
INSERT INTO `yun_source` VALUES (115, '//cdn.dmake.cn/attachment/keditor/image/20191226/20191226022354_99006.png', 'image', 'CDN那么多，怎样批量管理七牛云、易速云、又拍云等多个CDN资源解决方案', 1577327035, 0, 0, 0);
INSERT INTO `yun_source` VALUES (113, '//cdn.dmake.cn/attachment/images/20191226/15773264370.png', 'image', '20191226/15773264370.png', 1577326438, 0, 0, 0);
INSERT INTO `yun_source` VALUES (183, '//cdn.dmake.cn/attachment/canvs/202102/01/1612160422.jpg', 'image', '60179da6af5a6.png', 1612160423, 0, 0, 0);
INSERT INTO `yun_source` VALUES (109, '//cdn.dmake.cn/attachment/keditor/image/20191226/20191226014009_13760.jpg', 'image', '随风来小程序码', 1577324410, 1578147430, 0, 4);
INSERT INTO `yun_source` VALUES (114, '//cdn.dmake.cn/attachment/keditor/image/20191226/20191226022119_75555.png', 'image', 'CDN那么多，怎样批量管理七牛云、易速云、又拍云等多个CDN资源解决方案', 1577326880, 0, 0, 0);
INSERT INTO `yun_source` VALUES (92, '//cdn.dmake.cn/attachment/keditor/image/20191128/20191128114106_49357.png', 'image', '免费的萤火小程序上可以怎么样和企业官网和电商网站融合？', 1574912467, 0, 0, 0);
INSERT INTO `yun_source` VALUES (95, '//cdn.dmake.cn/attachment/keditor/image/20191209/20191209065414_84582.png', 'image', '浅谈互联网产品设计，应用模块在运营中的使用和案例', 1575874456, 0, 0, 0);
INSERT INTO `yun_source` VALUES (120, '//cdn.dmake.cn/attachment/keditor/image/20191227/20191227130904_71310.png', 'image', '如何在系统中集成淘宝客/京东客/拼多多客接口？基于阿里妈妈SDK案例分享', 1577423345, 0, 0, 0);
INSERT INTO `yun_source` VALUES (119, '//cdn.dmake.cn/attachment/images/20191227/15774228158.png', 'image', '20191227/15774228158.png', 1577422815, 0, 0, 0);
INSERT INTO `yun_source` VALUES (118, '//cdn.dmake.cn/attachment/images/20191227/15774227750.png', 'image', '20191227/15774227750.png', 1577422776, 0, 0, 0);
INSERT INTO `yun_source` VALUES (117, '//cdn.dmake.cn/attachment/images/20191227/15774124079.jpg', 'image', '20191227/15774124079.jpg', 1577412408, 0, 0, 0);
INSERT INTO `yun_source` VALUES (106, '//cdn.dmake.cn/attachment/keditor/image/20191213/20191213103617_52946.png', 'image', 'Visual Studio Code 开发PHP项目和Composer的配置', 1576204577, 0, 0, 0);
INSERT INTO `yun_source` VALUES (121, '//cdn.dmake.cn/attachment/keditor/image/20191230/20191230033421_48990.jpg', 'image', '', 1577676862, 0, 0, 0);
INSERT INTO `yun_source` VALUES (122, '//cdn.dmake.cn/attachment/images/20200108/15784598656.png', 'image', '20200108/15784598656.png', 1578459866, 0, 0, 0);
INSERT INTO `yun_source` VALUES (123, '//cdn.dmake.cn/attachment/keditor/image/20200108/20200108130446_94872.png', 'image', '微信小程序开发文字弹幕实例，无限轮播的规范化代码', 1578459888, 0, 0, 0);
INSERT INTO `yun_source` VALUES (124, '//cdn.dmake.cn/attachment/images/20200109/157853716310.png', 'image', '20200109/157853716310.png', 1578537164, 0, 0, 0);
INSERT INTO `yun_source` VALUES (125, '//cdn.dmake.cn/attachment/images/20200113/157892306610.jpg', 'image', '20200113/157892306610.jpg', 1578923067, 0, 0, 0);
INSERT INTO `yun_source` VALUES (126, '//cdn.dmake.cn/attachment/images/20200119/15794117754.jpg', 'image', '浪花黄昏沙滩', 1579411775, 1579411799, 0, 1);
INSERT INTO `yun_source` VALUES (127, '//cdn.dmake.cn/attachment/images/20200120/15795058056.jpg', 'image', '20200120/15795058056.jpg', 1579505806, 0, 0, 0);
INSERT INTO `yun_source` VALUES (128, '//cdn.dmake.cn/attachment/keditor/image/20200123/20200123102458_19560.png', 'image', 'PHP系统配套数据库不要在用Mysql5.7了，Mysql8.0负载翻了近4倍', 1579746299, 0, 0, 0);
INSERT INTO `yun_source` VALUES (129, '//cdn.dmake.cn/attachment/keditor/image/20200123/20200123102510_19250.png', 'image', 'PHP系统配套数据库不要在用Mysql5.7了，Mysql8.0负载翻了近4倍', 1579746311, 0, 0, 0);
INSERT INTO `yun_source` VALUES (130, '//cdn.dmake.cn/attachment/keditor/image/20200123/20200123102518_52401.png', 'image', 'PHP系统配套数据库不要在用Mysql5.7了，Mysql8.0负载翻了近4倍', 1579746318, 0, 0, 0);
INSERT INTO `yun_source` VALUES (131, '//cdn.dmake.cn/attachment/keditor/image/20200123/20200123102555_62037.jpg', 'image', 'PHP系统配套数据库不要在用Mysql5.7了，Mysql8.0负载翻了近4倍', 1579746355, 0, 0, 0);
INSERT INTO `yun_source` VALUES (132, '//cdn.dmake.cn/attachment/images/20200127/158009443310.png', 'image', '20200127/158009443310.png', 1580094434, 0, 0, 0);
INSERT INTO `yun_source` VALUES (133, '//cdn.dmake.cn/attachment/keditor/image/20200127/20200127111309_49462.jpg', 'image', '如何利用金字塔原理推导公司业务和团队架构', 1580094790, 0, 0, 0);
INSERT INTO `yun_source` VALUES (134, '//cdn.dmake.cn/attachment/images/20200128/15801785017.png', 'image', '20200128/15801785017.png', 1580178502, 0, 0, 0);
INSERT INTO `yun_source` VALUES (135, '//cdn.dmake.cn/attachment/keditor/image/20200128/20200128104936_64286.png', 'image', '从PV/UV到点击率跳出率解答百度统计中的平台运营哲学', 1580179777, 0, 0, 0);
INSERT INTO `yun_source` VALUES (136, '//cdn.dmake.cn/attachment/keditor/image/20200128/20200128104948_36816.png', 'image', '从PV/UV到点击率跳出率解答百度统计中的平台运营哲学', 1580179788, 0, 0, 0);
INSERT INTO `yun_source` VALUES (137, '//cdn.dmake.cn/attachment/images/20200226/15826961676.png', 'image', '20200226/15826961676.png', 1582696168, 0, 0, 0);
INSERT INTO `yun_source` VALUES (138, '//cdn.dmake.cn/attachment/keditor/image/20200226/20200226143708_86301.jpg', 'image', '在自媒体中巧妙使用话题营销3大原理和3大步骤打造营销爆款', 1582699029, 0, 0, 0);
INSERT INTO `yun_source` VALUES (139, '//cdn.dmake.cn/attachment/keditor/image/20200311/20200311052300_97392.jpg', 'image', '小程序商城自助建站不需要300认证费', 1583904181, 0, 0, 0);
INSERT INTO `yun_source` VALUES (140, '//cdn.dmake.cn/attachment/canvs/202004/01/1585694163.jpg', 'image', '5e83c5d3c5eb6.jpg', 1585694164, 0, 0, 0);
INSERT INTO `yun_source` VALUES (141, '//cdn.dmake.cn/attachment/images/20200409/15864202224.png', 'image', '20200409/15864202224.png', 1586420223, 0, 0, 0);
INSERT INTO `yun_source` VALUES (142, '//cdn.dmake.cn/attachment/images/20200414/15868352330.png', 'image', '20200414/15868352330.png', 1586835233, 0, 0, 0);
INSERT INTO `yun_source` VALUES (143, '//cdn.dmake.cn/attachment/keditor/image/20200414/20200414113443_82212.png', 'image', 'Myeclipse官方下载和百度网盘很慢怎么办？来看看最新的CDN加速香不香！', 1586835284, 0, 0, 0);
INSERT INTO `yun_source` VALUES (144, '//cdn.dmake.cn/attachment/keditor/image/20200414/20200414115306_75686.png', 'image', 'Myeclipse官方下载和百度网盘很慢怎么办？来看看最新的CDN加速香不香！', 1586836387, 0, 0, 0);
INSERT INTO `yun_source` VALUES (145, '//cdn.dmake.cn/attachment/keditor/image/20200415/20200415113455_18829.png', 'image', 'Myeclipse官方下载和百度网盘很慢怎么办？来看看最新的CDN加速香不香！', 1586921696, 0, 0, 0);
INSERT INTO `yun_source` VALUES (146, '//cdn.dmake.cn/attachment/images/20200427/15879648918.png', 'image', '20200427/15879648918.png', 1587964892, 0, 0, 0);
INSERT INTO `yun_source` VALUES (147, '//cdn.dmake.cn/attachment/images/20200827/15985176082.jpg', 'image', '20200827/15985176082.jpg', 1598517609, 0, 0, 0);
INSERT INTO `yun_source` VALUES (148, '//cdn.dmake.cn/attachment/images/20200924/16009402762.jpg', 'image', '20200924/16009402762.jpg', 1600940277, 0, 0, 0);
INSERT INTO `yun_source` VALUES (149, '//cdn.dmake.cn/attachment/images/20200929/16013435363.png', 'image', '20200929/16013435363.png', 1601343537, 0, 0, 0);
INSERT INTO `yun_source` VALUES (150, '//cdn.dmake.cn/attachment/keditor/image/20200929/20200929094503_85581.png', 'image', '微软决定用78块钱干掉中国一般低端码农', 1601343904, 0, 0, 0);
INSERT INTO `yun_source` VALUES (151, '//cdn.dmake.cn/attachment/keditor/image/20200929/20200929094612_25508.png', 'image', '微软决定用78块钱干掉中国一般低端码农', 1601343972, 0, 0, 0);
INSERT INTO `yun_source` VALUES (152, '//cdn.dmake.cn/attachment/keditor/image/20200929/20200929094715_64564.png', 'image', '微软决定用78块钱干掉中国一般低端码农', 1601344036, 0, 0, 0);
INSERT INTO `yun_source` VALUES (153, '//cdn.dmake.cn/attachment/keditor/image/20200929/20200929095340_43928.png', 'image', '微软决定用78块钱干掉中国一般低端码农', 1601344421, 0, 0, 0);
INSERT INTO `yun_source` VALUES (154, '//cdn.dmake.cn/attachment/keditor/image/20200929/20200929095353_40602.png', 'image', '微软决定用78块钱干掉中国一般低端码农', 1601344434, 0, 0, 0);
INSERT INTO `yun_source` VALUES (155, '//cdn.dmake.cn/attachment/keditor/image/20200929/20200929095406_71780.png', 'image', '微软决定用78块钱干掉中国一般低端码农', 1601344447, 0, 0, 0);
INSERT INTO `yun_source` VALUES (156, '//cdn.dmake.cn/attachment/keditor/image/20200929/20200929095419_85051.png', 'image', '微软决定用78块钱干掉中国一般低端码农', 1601344460, 0, 0, 0);
INSERT INTO `yun_source` VALUES (157, '//cdn.dmake.cn/attachment/images/20201010/16022938456.jpeg', 'image', '20201010/16022938456.jpeg', 1602293845, 0, 0, 0);
INSERT INTO `yun_source` VALUES (158, '//cdn.dmake.cn/attachment/keditor/image/20201010/20201010094540_75910.png', 'image', '看完这篇，DBA从入门到删库跑路还是平步青云就好选了', 1602294341, 0, 0, 0);
INSERT INTO `yun_source` VALUES (159, '//cdn.dmake.cn/attachment/keditor/image/20201010/20201010101820_52171.png', 'image', '看完这篇，DBA从入门到删库跑路还是平步青云就好选了', 1602296301, 0, 0, 0);
INSERT INTO `yun_source` VALUES (160, '//cdn.dmake.cn/attachment/keditor/image/20201010/20201010102011_86826.jpg', 'image', '看完这篇，DBA从入门到删库跑路还是平步青云就好选了', 1602296412, 0, 0, 0);
INSERT INTO `yun_source` VALUES (161, '//cdn.dmake.cn/attachment/images/20201019/16030927392.jpeg', 'image', '20201019/16030927392.jpeg', 1603092739, 0, 0, 0);
INSERT INTO `yun_source` VALUES (162, '//cdn.dmake.cn/attachment/images/20201223/16087054367.png', 'image', '20201223/16087054367.png', 1608705436, 0, 0, 0);
INSERT INTO `yun_source` VALUES (163, '//cdn.dmake.cn/attachment/images/20210111/16103427004.jpg', 'image', '20210111/16103427004.jpg', 1610342700, 0, 0, 0);
INSERT INTO `yun_source` VALUES (164, '//cdn.dmake.cn/attachment/images/20210114/16106165035.png', 'image', '20210114/16106165035.png', 1610616503, 0, 0, 0);
INSERT INTO `yun_source` VALUES (165, '//cdn.dmake.cn/attachment/images/20210121/16112053268.png', 'image', '20210121/16112053268.png', 1611205326, 0, 0, 0);
INSERT INTO `yun_source` VALUES (166, '//cdn.dmake.cn/attachment/images/20210121/16112195664.png', 'image', '20210121/16112195664.png', 1611219567, 0, 0, 0);
INSERT INTO `yun_source` VALUES (167, '//cdn.dmake.cn/attachment/images/20210125/16115488735.jpg', 'image', '20210125/16115488735.jpg', 1611548874, 0, 0, 0);
INSERT INTO `yun_source` VALUES (168, '//cdn.dmake.cn/attachment/images/20210125/16115557173.png', 'image', '20210125/16115557173.png', 1611555717, 0, 0, 0);
INSERT INTO `yun_source` VALUES (169, '//cdn.dmake.cn/attachment/images/20210125/16115557557.png', 'image', '20210125/16115557557.png', 1611555759, 0, 0, 0);
INSERT INTO `yun_source` VALUES (170, '//cdn.dmake.cn/attachment/images/20210126/16116400125.jpg', 'image', '20210126/16116400125.jpg', 1611640012, 0, 0, 0);
INSERT INTO `yun_source` VALUES (171, '//cdn.dmake.cn/attachment/images/20210128/16118030459.jpg', 'image', '20210128/16118030459.jpg', 1611803046, 0, 0, 0);
INSERT INTO `yun_source` VALUES (172, '//cdn.dmake.cn/attachment/images/20210128/16118032829.jpg', 'image', '20210128/16118032829.jpg', 1611803282, 0, 0, 0);
INSERT INTO `yun_source` VALUES (173, '//cdn.dmake.cn/attachment/images/20210128/16118034190.jpg', 'image', '20210128/16118034190.jpg', 1611803419, 0, 0, 0);
INSERT INTO `yun_source` VALUES (174, '//cdn.dmake.cn/attachment/images/20210128/16118035855.jpg', 'image', '20210128/16118035855.jpg', 1611803586, 0, 0, 0);
INSERT INTO `yun_source` VALUES (175, '//cdn.dmake.cn/attachment/images/20210128/16118036919.jpg', 'image', '20210128/16118036919.jpg', 1611803691, 0, 0, 0);
INSERT INTO `yun_source` VALUES (176, '//cdn.dmake.cn/attachment/images/20210128/16118039913.jpg', 'image', '20210128/16118039913.jpg', 1611803993, 0, 0, 0);
INSERT INTO `yun_source` VALUES (177, '//cdn.dmake.cn/attachment/images/20210128/16118128965.jpg', 'image', '20210128/16118128965.jpg', 1611812897, 0, 0, 0);
INSERT INTO `yun_source` VALUES (178, '//cdn.dmake.cn/attachment/images/20210128/16118421101.jpg', 'image', '20210128/16118421101.jpg', 1611842110, 0, 0, 0);
INSERT INTO `yun_source` VALUES (179, '//cdn.dmake.cn/attachment/images/20210128/16118422830.jpg', 'image', '20210128/16118422830.jpg', 1611842283, 0, 0, 0);
INSERT INTO `yun_source` VALUES (180, '//cdn.dmake.cn/attachment/images/20210129/16118860119.jpg', 'image', '20210129/16118860119.jpg', 1611886011, 0, 0, 0);
INSERT INTO `yun_source` VALUES (182, '//cdn.dmake.cn/lu_JG74oGkA_EYifKmkWUJG7KD65', 'video/mp4', 'Single Trail Mountain Bike Race.mp4', 1611910057, 0, 0, 0);
INSERT INTO `yun_source` VALUES (184, '//cdn.dmake.cn/lm5faiLdnS9fXoDEJ9lgLqCwymzS', 'video/mp4', 'river.mp4', 1612186460, 0, 0, 0);
INSERT INTO `yun_source` VALUES (185, '//cdn.dmake.cn/attachment/images/20210201/16121865313.jpg', 'image', '20210201/16121865313.jpg', 1612186531, 0, 0, 0);
INSERT INTO `yun_source` VALUES (186, '//cdn.dmake.cn/attachment/images/20210208/16127508459.jpg', 'image', '20210208/16127508459.jpg', 1612750845, 0, 0, 0);
INSERT INTO `yun_source` VALUES (187, '//cdn.dmake.cn/attachment/images/20210208/161275085510.png', 'image', '20210208/161275085510.png', 1612750857, 0, 0, 0);
INSERT INTO `yun_source` VALUES (188, '//cdn.dmake.cn/attachment/images/20210220/16137825423.jpg', 'image', '20210220/16137825423.jpg', 1613782543, 0, 0, 0);
INSERT INTO `yun_source` VALUES (189, '//cdn.dmake.cn/attachment/images/20210309/16152924919.jpg', 'image', '20210309/16152924919.jpg', 1615292491, 0, 0, 0);
INSERT INTO `yun_source` VALUES (190, '//cdn.dmake.cn/attachment/images/20210322/16163949006.png', 'image', '20210322/16163949006.png', 1616394900, 0, 0, 0);
INSERT INTO `yun_source` VALUES (191, '//cdn.dmake.cn/attachment/images/20210322/16163949414.png', 'image', '20210322/16163949414.png', 1616394941, 0, 0, 0);
INSERT INTO `yun_source` VALUES (192, '//cdn.dmake.cn/attachment/images/20210322/16163949729.png', 'image', '20210322/16163949729.png', 1616394972, 0, 0, 0);
INSERT INTO `yun_source` VALUES (193, '//cdn.dmake.cn/attachment/images/20210322/16163950037.png', 'image', '20210322/16163950037.png', 1616395003, 0, 0, 0);
INSERT INTO `yun_source` VALUES (194, '//cdn.dmake.cn/attachment/images/20210322/16163950191.png', 'image', '20210322/16163950191.png', 1616395020, 0, 0, 0);
INSERT INTO `yun_source` VALUES (195, '//cdn.dmake.cn/attachment/images/20210322/16163950353.png', 'image', '20210322/16163950353.png', 1616395035, 0, 0, 0);
INSERT INTO `yun_source` VALUES (196, '//cdn.dmake.cn/attachment/images/20210322/16163950527.png', 'image', '20210322/16163950527.png', 1616395052, 0, 0, 0);
INSERT INTO `yun_source` VALUES (197, '//cdn.dmake.cn/attachment/images/20210324/16165512765.png', 'image', '20210324/16165512765.png', 1616551276, 0, 0, 0);
INSERT INTO `yun_source` VALUES (198, '//cdn.dmake.cn/attachment/images/20210412/16182106868.jpg', 'image', '个人工作', 1618210687, 1625119502, 1, 1);
INSERT INTO `yun_source` VALUES (199, '//cdn.dmake.cn/attachment/images/20210701/162511936410.jpg', 'image', '团队合作', 1625119365, 1625119489, 1, 1);
INSERT INTO `yun_source` VALUES (200, '//cdn.dmake.cn/attachment/images/20210728/16274468813.jpeg', 'image', '20210728/16274468813.jpeg', 1627446882, 0, 0, 0);
INSERT INTO `yun_source` VALUES (201, '//cdn.dmake.cn/attachment/images/20210831/16303815263.jpg', 'image', '20210831/16303815263.jpg', 1630381527, 0, 0, 0);
INSERT INTO `yun_source` VALUES (202, '//cdn.dmake.cn/attachment/images/20210831/16303903783.jpg', 'image', '20210831/16303903783.jpg', 1630390379, 0, 0, 0);
INSERT INTO `yun_source` VALUES (203, '//cdn.dmake.cn/attachment/images/20210923/16323842212.png', 'image', '20210923/16323842212.png', 1632384222, 0, 0, 0);
INSERT INTO `yun_source` VALUES (204, '//cdn.dmake.cn/attachment/images/20210923/16323847990.png', 'image', '20210923/16323847990.png', 1632384801, 0, 0, 0);
INSERT INTO `yun_source` VALUES (205, '//cdn.dmake.cn/Fiu-FkfMuBu8Km6wrgcFcYwv35Y6', 'image/png', 'x.png', 1632385230, 0, 0, 0);

-- ----------------------------
-- Table structure for yun_source_group
-- ----------------------------
DROP TABLE IF EXISTS `yun_source_group`;
CREATE TABLE `yun_source_group`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  `adminid` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of yun_source_group
-- ----------------------------
INSERT INTO `yun_source_group` VALUES (1, '文章封面', 1577245079, 0, 1);
INSERT INTO `yun_source_group` VALUES (2, '内容插图', 1577245132, 0, 1);
INSERT INTO `yun_source_group` VALUES (3, '人物素材', 1577251498, 0, 1);
INSERT INTO `yun_source_group` VALUES (4, '二维码等', 1577325801, 0, 1);
INSERT INTO `yun_source_group` VALUES (5, '装饰用图标', 1577326012, 0, 1);

SET FOREIGN_KEY_CHECKS = 1;
