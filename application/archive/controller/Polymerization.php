<?php
namespace app\archive\controller;

use app\index\controller\Base;
use app\common\model\PolymerizationBase;
use app\index\model\ProductModel;

class Polymerization extends Base
{

    public function index()
    {
        $topic = new PolymerizationBase();
        
        $type = input('param.type');
        $supplierid = input('param.sid');
        $keyword = input('param.kw');
        
        $map = [];
        if ($type > 0) {
            $map['Categoryid'] = $type;
        }
        if ($supplierid > 0) {
            $map['Supplierid'] = $supplierid;
        }
        if ($keyword != '') {
            $map['Name'] = [
                'like',
                '%' . $keyword . '%'
            ];
        }
        
        $list = $topic
            ->where($map)
            ->order('Id desc')
            ->paginate(16);
                
        $this->assign('page_method', $list->render());
        $this->assign('list', $list);
        $this->assign([
            'title' => '',
            'keywords' => '',
            "description" => ''
        ]);
        return $this->fetch();
    }

    public function detail()
    {
        $id=input('id');
        
        $topic = new PolymerizationBase();
        $data = $topic->getTopicZone($id);
        
        if($data==null){
            return redirect('/topics');
        }
        
        $topics = $topic->getTopics(10);
                
        $pm = new ProductModel();
        
        $categoryid=$data['Categoryid'];
        $categoryid=$categoryid?$categoryid:20;
        
        $suppliers=$pm->getRelateSuppliers(0, $categoryid);
        $products=$pm->getRelateProducts(0, $categoryid);
        
        $this->assign([
            'title' => $data['Name'],
            'keywords' => $data['Keywords'],
            'description' => $data['Context'],
            'topic' => $data,
            'topics' => $topics,
            'suppliers'=>$suppliers,
            'products'=>$products
        ]);
        
        return $this->fetch();
    }
}