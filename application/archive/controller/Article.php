<?php
namespace app\archive\controller;

use app\common\model\CommentsBase;
use app\common\model\SupplierBase;
use app\common\model\TopicModel;
use app\index\controller\Base;
use app\index\model\ArticleModel;

class Article extends Base
{

    /**
     * 资讯首页
     */
    public function index()
    {
        $am = new ArticleModel();

        $list = $am->alias('a')
            ->field('a.id,a.title,creativetype,a.create_time,categoryid,imagepath,summary,b.clickcount,c.name')
            ->join('article_attribute b', 'b.articleid=a.id', 'left')
            ->join('article_category c', 'c.id=a.categoryid', 'left')
            ->order('id desc')
            ->paginate(10);

        $groups = $am->getArticleAllGroups(10);
        $headlines = $am->getHeadLines();

        $scroll = $am->where('scroll', 1)->field('id,title,imagepath,summary')->order('id desc')->select();

        $assign = [
            'scroll' => $scroll,
            'groups' => $groups,
            'headlines' => $headlines,
            'page_method' => $list->render(),
            'category'=>$am->getArticleCates(),
            'list' => $list,
        ];

        return $this->view->assign($assign)->fetch();
    }

    /**
     * 资讯详情
     */
    public function detail()
    {
        $id = input('param.id');

        if ($id == null || $id == '') {
            return redirect('/articles');
        }

        $am = new ArticleModel();
        $cm = new CommentsBase();

        $data = $am->getDetail($id);

        if ($data == null) {
            return redirect('/articles');
        }

        $relation_suppliers = $am->getArticleSuppliers($id);

        $supplierid = null;
        $form = '/forum/threads/?o=new';
        if ($relation_suppliers) {
            $supplierid = $relation_suppliers[0]['supplierid'];
            $form = '/forum/0?supplier=' . $supplierid;
        }

        $assign = [
            'title' => $data['title'] . '-' . $data['categoryname'],
            'keywords' => $data['keywords'],
            'description' => $data['summary'],
            'aid' => $id,
            'data' => $data,
            'commentid' => $id,
            'forumurl' => $form,
            'supplierid' => $supplierid,
            'rsuppliers' => $relation_suppliers,
            'rproducts' => $am->getArticlProducts($id),
            'rgroups' => $am->getArticleGroups($data['groupid'], 20, $id),
            'hotarticles' => $am->getRelationArticles(0, 10, 'clickcount desc'),
            'newarticles' => $am->getArticleByProduct($id, 10),
            'rarticles' => $am->getRelationArticles($data['categoryid'], 10),
            'relations' => $am->getArticleRelations($data['id'], $data['categoryid']),
            'comments' => $cm->getArticleComments($id, 1, 10),
        ];

        return $this->view->assign($assign)->fetch();
    }

    /**
     * 资讯列表
     */
    function list() {
        $id = input('param.id');
        $sid = input('param.sid');
        $kw = input('param.kw');
        if ($kw && $kw != '') {
            $map['a.title|a.summary|a.keywords'] = ['like', '%' . $kw . '%'];
        }

        $am = new ArticleModel();

        if ($id && $id > 0) {
            $category = $am->getArticlecategory($id);
            $map['categoryid'] = $category['id'];

            $list = $am->alias('a')
                ->field('a.id,a.title,creativetype,a.create_time,categoryid,imagepath,summary,b.clickcount,c.name')
                ->join('article_attribute b', 'b.articleid=a.id', 'left')
                ->join('article_category c', 'c.id=a.categoryid', 'left')
                ->where($map)
                ->order('id desc')
                ->paginate(10);

            $assign = [
                'title' => $category['title'] ? $category['title'] : $category['name'],
                'keywords' => $category['keywords'],
                'description' => $category['description'],
                'data' => $category,
            ];

        } else if ($sid && $sid > 0) {
            $sm = new SupplierBase();
            $supplier = $sm->get($sid);

            $list = $am->alias('a')
                ->field('a.id,a.title,creativetype,a.create_time,categoryid,imagepath,summary,b.clickcount,c.name')
                ->join('article_attribute b', 'b.articleid=a.id', 'left')
                ->join('article_category c', 'c.id=a.categoryid', 'left')
                ->where('a.id', 'in', function ($query) use ($sid) {
                    $query->table('article_relations')
                        ->where('supplierid', $sid)
                        ->field('articleid');
                })
                ->order('id desc')
                ->paginate(10);

            $assign = [
                'title' => $supplier['title'] ? $supplier['title'] : $supplier['name'],
                'keywords' => $supplier['keywords'],
                'description' => $supplier['description'],
                'data' => $supplier,
            ];
        } else if ($kw && $kw != '') {
            $list = $am->alias('a')
                ->field('a.id,a.title,creativetype,a.create_time,categoryid,imagepath,summary,b.clickcount,c.name')
                ->join('article_attribute b', 'b.articleid=a.id', 'left')
                ->join('article_category c', 'c.id=a.categoryid', 'left')
                ->where($map)
                ->order('id desc')
                ->paginate(10);

            $assign = [
                'title' => '搜索' . $kw,
                'keywords' => '搜索' . $kw,
                'description' => '搜索' . $kw,
                'data' => null,
            ];
        }

        $assign = array_merge($assign, [
            'list' => $list,
            'page_method' => $list->render(),
            'groups' => $am->getArticleAllGroups(10),
            'category'=>$am->getArticleCates(),
            'headlines' => $am->getHeadLines($id),
        ]);

        return $this->view->assign($assign)->fetch();
    }
}
