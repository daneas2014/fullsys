<?php

/**
 * 1.获取code跳转连接:
 * 状态参数
 * 跳转地址
 * 服务号是否需要获取到详细信息[$wxasnum 0是铜梁视窗，1是同城土货的,2是贝贝佳,0是只需要获取openid即可]
 */
function wx_get_code($state, $redirecturl, $config, $base = 0)
{
    if (!strstr($redirecturl, 'office')) {
        $redirecturl = WX_AUTH_DOMAIN . 'tom_oauth.php?oauth_back_url=' . urlencode($redirecturl);
    }

    $param['appid'] = $config['appid']; // AppID
    $param['redirect_uri'] = $redirecturl; // 获取code后的跳转地址
    $param['response_type'] = 'code'; // 不用修改
    $param['scope'] = 'snsapi_userinfo';
    $param['state'] = $state; // 可在行定义该参数

    return 'https://open.weixin.qq.com/connect/oauth2/authorize?' . http_build_query($param) . '#wechat_redirect';
}

/**
 * 2.获取网页授权access_token，此access_token非普通的access_token，详情请看微信公众号开发者文档
 * {"access_token":"ACCESS_TOKEN","expires_in":7200,"refresh_token":"REFRESH_TOKEN","openid":"OPENID","scope":"SCOPE","unionid": "o6_bmasdasdsad6_2sgVt7hMZOPfL"}
 */
function wx_get_access_token($config)
{
    $param['appid'] = $config['appid'];
    $param['secret'] = $config['secret'];
    $param['code'] = $_GET['code'];
    $param['grant_type'] = 'authorization_code';

    $url = 'https://api.weixin.qq.com/sns/oauth2/access_token?' . http_build_query($param);
    $token = file_get_contents($url);
    $token = json_decode($token, true);

    // token过期验证
    // wx_reflash_token($token['access_token'],$token['openid'],$token['refresh_token']);

    if (!empty($token['errmsg'])) {
        return false;
    }

    return $token;
}

/**
 * token过期刷新
 * {"access_token":"ACCESS_TOKEN","expires_in":7200,"refresh_token":"REFRESH_TOKEN","openid":"OPENID","scope":"SCOPE","unionid": "o6_bmasdasdsad6_2sgVt7hMZOPfL"}
 */
function wx_reflash_token($token, $openid, $reflash, $config)
{
    // 1. token是否过期
    $url = 'https://api.weixin.qq.com/cgi-bin/user/info?access_token=' . $token . '&openid=' . $openid;
    $reback = file_get_contents($url);
    $json = json_decode($reback, true);

    // 2. token过期后重新拿刷新，跟步骤1一样的结果
    if (array_key_exists('errcode', $json) && $json['errcode'] == 40001) {

        $param['appid'] = $config['appid']; // AppID
        $url = 'https://api.weixin.qq.com/sns/oauth2/refresh_token?appid=' . $param['appid'] . '&grant_type=refresh_token&refresh_token=' . $reflash;
        $reback = file_get_contents($url);
        $content = json_decode($reback, true);
        return $content;
    } else {
        return true;
    }
}

/**
 * 服务号通过授权获取用户信息, $content 是数组类型
 */
function wx_get_userinfo($token)
{
    $url = 'https://api.weixin.qq.com/sns/userinfo?access_token=' . $token['access_token'] . '&openid=' . $token['openid'] . '&lang=zh_CN';
    $user = file_get_contents($url);
    $user = json_decode($user, true);
    return $user;
}

/**
 * 服务号通过静默授权获得用户的openid[$wxasnum 0是铜梁视窗，1是同城土货的,2是贝贝佳]
 */
function wx_get_useropenid($config)
{
    $appid = $config['appid']; // AppID
    $secret = $config['secret']; // AppSecret
    $url = 'https://api.weixin.qq.com/sns/oauth2/access_token?appid=' . $appid . '&secret=' . $secret . '&code=' . $_GET['code'] . '&grant_type=authorization_code';
    $json = file_get_contents($url);

    return $json;
}

/**
 * 回复文字给微信会员
 *
 * @param unknown $to 接收方的openid
 * @param unknown $from 开发者微信号
 * @param unknown $content
 */
function puttxt($to, $from, $content)
{
    $array = array(
        'ToUserName' => $to,
        'FromUserName' => $from,
        'CreateTime' => time(),
        'MsgType' => 'text',
        'Content' => $content,
    );
    $xml = arrayToXml($array);
    return $xml;
}

/**
 * 回复单个图片素材
 *
 * @param unknown $to 接收方的openid
 * @param unknown $from 开发者微信号
 * @param unknown $mediaid
 */
function putpic($to, $from, $mediaid)
{
    $array = array(
        'ToUserName' => $to,
        'FromUserName' => $from,
        'CreateTime' => time(),
        'MsgType' => 'image',
        'MediaId' => $mediaid,
    );
    $xml = arrayToXml($array);
    return $xml;
}

/**
 * 返回图文列表
 *
 * @param unknown $to
 * @param unknown $from
 * @param unknown $title
 * @param unknown $articles[title,description,picurl,url]
 *            最多8条
 */
function putarticles($to, $from, $title, $articles)
{
    $array = array(
        'ToUserName' => $to,
        'FromUserName' => $from,
        'CreateTime' => time(),
        'MsgType' => 'news',
        'ArticleCount' => count($articles),
    );
    $xml = arrayToXml($array, $articles);
    return $xml;
}

/**
 * 生成微信标准xml
 *
 * @param unknown $array
 */
function arrayToXml($array, $articles = null)
{
    $xml = '<xml>\n';
    foreach ($array as $key => $value) {
        $xml .= '<' . $key . '><![CDATA[' . $value . ']]></' . $key . '>\n';
    }

    if ($articles != null) {
        $xml .= '<Articles>';

        foreach ($articles as $key => $value) {
            $xml .= '<item>';
            $xml .= '<Title><![CDATA[' . $value['title'] . ']]></Title>';
            $xml .= '<Description><![CDATA[' . $value['description'] . ']]></Description>';
            $xml .= '<PicUrl><![CDATA[' . $value['picurl'] . ']]></PicUrl>';
            $xml .= '<Url><![CDATA[' . $value['url'] . ']]></Url>';
            $xml .= '</item>';
        }

        $xml .= '</Articles>';
    }

    $xml .= '</xml>';
}

// 将XML转为array
function xmlToArray($xml)
{
    // 禁止引用外部xml实体
    libxml_disable_entity_loader(true);
    $values = json_decode(json_encode(simplexml_load_string($xml, 'SimpleXMLElement', LIBXML_NOCDATA)), true);
    return $values;
}

/**
 * 以七牛为准
 *
 * @param [type] $url
 * @return 正确的地址
 * @author dmakecn@163.com
 * @since 2019-12-17
 */
function makeimg($url)
{
    if (strstr($url, QINIU_DOMAIN)) {
        return $url;
    }
    if ($url == '') {
        return SITE_DOMAIN . "/static/images/default.jpg";
    }
    if (!strstr($url, '//')) {
        return SITE_DOMAIN . $url;
    }
    return $url;
}

/**
 * 返回网页路径
 *
 * @param [type] $id
 * @param [type] $type
 * @return void
 * @author dmakecn@163.com
 * @since 2019-12-17
 */
function makeurl($id, $type)
{
    switch ($type) {
        case "news":
            return SITE_DOMAIN . "/article/" . $id;
        case "need":
            return SITE_DOMAIN . "/need/" . $id;
        case "vote":
            return SITE_DOMAIN . "/vote/" . $id;
        case "voteitem":
            return SITE_DOMAIN . "/voteitem/" . $id;
        case "exam":
            return SITE_DOMAIN . "/exam/" . $id;
        case "tao":
            return SITE_DOMAIN . "/tao/" . $id;
    }
}
