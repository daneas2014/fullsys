<?php
namespace app\package\controller;

use app\common\model\CustomerInvoiceBase;
use app\index\controller\Base;
use think\Db;

class Checkout extends Base {

	public function vip() {

		$data = input('param.');
		$courseid = $data['courseid'];
		$user = session('memberinfo');

		if ($user == null) {
			return $this->success('您还未登录，请登陆后再试！', '/customer/sign-up');
		}

		if ($courseid > 0) {
			$license = Db::name('course_description')
				->where('id', $courseid)->find();
		} else if ($courseid == 0) {
			$license['id'] = 0;
			$license['saleprice'] = 299;
			$license['title'] = '大会员全站免费';
			$license['isfree'] = 0;
			$license['imagepath'] = '';
		}

		if ($license['saleprice'] < 1 && $license['isfree'] == 1) {
			return $this->error('当前视频无需购买，您可以免费观看，谢谢！');
		}

		$invoice = Db::name('customer_invoice')->where('customerid', $user['id'])->find();

		$assign = ['title' => '订单支付确认',
			'keywords' => '',
			'description' => '',
			'invoice' => $invoice,
			'license' => $license];

		return $this->view->assign($assign)->fetch();
	}

	public function vippay() {

		if (request()->isPost()) {

			$input = input('post.');

			$courseid = $input['courseid'];

			$user = session('memberinfo');

			$subject = '大会员购买';

			// 更新记录用户的发票信息，最新的那种

			//$this->logCustomerInvoice($input,$user['Id']);

			//1. 罗列购物清单

			if ($courseid > 0) {
				$license = Db::name('course_description')->where('id', $courseid)->find();
				$subject = $license['title'];
			} else if ($courseid == 0) {
				$license['id'] = 0;
				$license['saleprice'] = 2999;
				$license['title'] = '大会员全站免费';
				$license['isfree'] = 0;
				$license['imagepath'] = '';
			}

			if ($license['saleprice'] < 1 && $license['isfree'] == 1) {
				return $this->error('当前视频无需购买，您可以免费观看，谢谢！');
			}

			$amount = $license['saleprice'];

			//2. 保存订单，保存订单详细
 
            $order['customer_name'] = $input['contact_name'];
            $order['customer_mobil'] = $input['contact_mobile'];
            $order['customer_address'] = $input['contact_address'];
            $order['customer_zip'] = $input['contact_zip'];
            $order['note'] = $input['remark']; //留言
 

            // **********************这个钱啷个算很重要，要和输入的值做对比**************************

            $order['totalamount'] = $amount['order_amount']; //此处需要计算<============================================================
            $order['discount'] = $amount['discount'];
            $order['discountcode'] = $amount['discountcode'];

            if ($input['order_amount'] != $order['totalamount']) {
                return $this->error('参数错误，请核对后再试！ERROCODE:110');
            }

            $order['orderno'] = 'S' . date('YmdHis', time());
            $order['productid'] = $license['productid'];
            $order['customerid'] = $user['id'];
            $order['paymenttype'] = $input['paytype'];
            $order['status'] = 'WP';
            $order['create_time'] = time();
            $order['pay_time'] = 0;
            $order['cancel_time'] = 0;

            $order['paymentid'] = ''; //真的是paypal/alipay的订单号

            // $order['invoice'] = $license['invoice'] > 0 ? (int) $license['invoice'] : 3; //发票,一般系统都包含了的所以取消了这个字段

            $order['invoice_number'] = $input['invoice_code']; //税号
            $order['invoice_head'] = $input['invoice_name']; //发票抬头
            $order['invoice_email'] = $input['contact_email']; //收货邮箱
            $order['invoice_remark'] = ''; // 未知
            $order['remark'] = json_encode($amount);

            $order['subject'] = $subject;

            $orderid = Db::name('orders')->insertGetId($order);

            $orderitem['orderid'] = $orderid;
            $orderitem['productid'] = $license['id'];
            $orderitem['licenseid'] = $license['id'];
            $orderitem['cost'] = $amount['singleprice'];
            $orderitem['qty'] = 1;
            $orderitem['orderyear'] = 1;
            Db::name('order_item')->insert($orderitem);

            //4. 记录

            $orderlog['orderno'] = $order['orderno'];
            $orderlog['orderid'] = $orderid;
            $orderlog['changedstate'] = 'WP';
            $orderlog['remark'] = '创建了一条订单，等待支付' . ',订单详情：授权ID[' . $license['id'] . ']' . $license['title'] . '-用户ID' . $user['id'];
            $orderlog['create_time'] = time();
            Db::name('order_history')->insert($orderlog);

            //5. 提交给支付接口
            $url = url('pay/index/payorder') . '?orderid=' . $orderid;

            return redirect($url);
		}

	}

	private function logCustomerInvoice($data, $mid) {

		$cim = new CustomerInvoiceBase();

		$db = Db::name('customer_invoice');

		$invoice = $cim->getByCustomer($mid);
		$invoice['customerid'] = $mid;

		foreach ($data as $key => $value) {
			$invoice[$key] = $value;
		}

		return $cim->saveOne($invoice);
	}
}