<?php
namespace app\package\controller;

use think\Db;
use app\common\model\CrmModel;
use app\index\controller\Base;
use app\index\model\SourceModel;
use app\index\model\ProductModel;
use app\ucenter\model\DetailModel;
use app\common\model\SubscribeModel;

class Index extends Base
{

    /**
     * 资源合集首页
     */
    public function index()
    {
        $rm = new SourceModel();
        
        $downloads = $rm->getProductAttchs('', 0, 0, 1, 20);
        $files = $rm->getProductAttchs('', 1, 0, 1, 20);
        $other = $rm->getProductAttchs('', 4, 0, 1, 20);
        $ext = $rm->getProductAttchs('', 5, 0, 1, 20);
        
        $demo1 = $rm->getDemos('', 0, 1, 1, 20);
        $demo2 = $rm->getDemos('', 0, 2, 1, 20);
        $demo3 = $rm->getDemos('', 0, 3, 1, 20);
        
        $this->assign([
            'title' => '',
            'keywords' => '',
            'description' => '',
            'download' => $downloads['list'],
            'file' => $files['list'],
            'ext' => $ext['list'],
            'other' => $other['list'],
            'demofile' => $demo1['list'],
            'demoonline' => $demo2['list'],
            'demodownload' => $demo3['list']
        ]);
        
        return $this->fetch();
    }

    /**
     * 下载分类页
     */
    public function download()
    {
        $rm = new SourceModel();
        
        $id = input('param.id');
        
        $map['DownloadType'] = 0;
        if ($id > 0) {
            $map['DownloadType'] = $id;
        }
        
        $kw = input('kw');
        if ($kw != '') {
            $map['Name'] = [
                'like',
                '%' . $kw . '%'
            ];
        }
        
        $productid=input('param.productid');
        if($productid&&$productid>0){
            $map['ProductId']=$productid;
        }
        
        $all = $rm->where($map)
            ->order('CreatedTime desc')
            ->paginate(20,false,['query' => ['kw'=>$kw,'id'=>$id,'productid'=>$productid]]);
        
        $this->assign('page_method', $all->render());
        $this->assign('download', $all);
        
        $this->assign([
            'title' => '',
            'keywords' => '',
            'description' => ''
        ]);
        
        return $this->fetch();
    }

    /**
     * demo分类页
     */
    public function demo()
    {
        $id = input('param.id');
        
        $db = Db::name('Productresource');
        
        $map = [];
        if ($id > 0) {            
            if($id==1){
                $map['HasFile']=0;
            }
            else{
                $map['ResourceType'] = $id;
            }
        }
        
        $kw = input('kw');
        if ($kw != '') {
            $map['Name'] = [
                'like',
                '%' . $kw . '%'
            ];
        }

        $productid=input('param.productid');
        if($productid&&$productid>0){
            $map['ProductId']=$productid;
        }
        
        $all = $db->where($map)
            ->order('CreatedTime desc')
            ->paginate(20,false,['query' => ['kw'=>$kw]]);
        
        $count = $db->where($map)->count();
        $this->assign('page_method', $all->render());
        $this->assign('news_count', $count);
        $this->assign('list', $all);
        
        $this->assign([
            'title' => '',
            'keywords' => '',
            'description' => ''
        ]);
        
        return $this->fetch();
    }

    /**
     * 下载详细页
     */
    public function detail()
    {
        $type = input('type');
        $id = input('id');
        $data = null;
        
        if(strstr($_SERVER['REQUEST_URI'], 'demo-sample')){
            return redirect('/');
        }
                
        $rm = new SourceModel();
        
        if ($type == 'download') {
            $data = $rm->getAttach($id);
            
            if($data==null){
                return $this->error('很抱歉该资源已下架','/resource/download/0');
            }
            
            $productid = $data->ProductId;
            $data['Summary']=$data['description']?$data['description']:$data['NameE'];
            $data['url']='https://www.dmake.cn/resource/detail-download-'.$id;
        }
        
        if ($type == 'demo') {
            $data = $rm->getDemo($id);
            if($data==null){
                return $this->error('很抱歉该资源已下架，请尝试其他途径下载或咨询客服！','/resource/download/0');
            }
            
            $data['Summary']=$data['description']?$data['description']:$data['NameE'];            
            $productid = $data['productid'];
            $data['ProductId']=$productid;
            $data['url']='https://www.dmake.cn/resource/detail-demo-'.$id;
        }
                        
        $pm = new ProductModel();
                        
        $this->assign([
            'title' => $data['Name'],
            'keywords' => $data['Name'],
            'description' =>array_key_exists('SEODescription', $data)?$data['SEODescription']:$data['Name'],
            'files' => $pm->getProductDownloads($productid, 10),
            'type'=>$type,
            'data'=>$data
        ]);
        
        return $this->fetch();
    }

    /**
     * 执行下载流程
     */
    public function downloadfile()
    {                
        $type = input('post.type');
        $id = input('post.id');
        $ding=input('post.ding');
                
        if($type!='download'&&$type!='demo'){
            return json(['code'=>-1,'data'=>'','msg'=>'ERROR PARAMS 1']);
        }
        
        if(!$id){
            return json(['code'=>-1,'data'=>'','msg'=>'ERROR PARAMS 2']);            
        }
        
        
        $data = null;
        $productname='';
        
        $rm = new SourceModel();
        
        if ($type == 'download') {
            $data = $rm->getAttach($id);
            $filetype=$data['DownloadType'];
            $productid = $data['ProductId'];
            $productname=$data['ProductName'];
        }
        
        if ($type == 'demo') {
            $data = $rm->getDemo($id);
            $data['ProductId']=$data['productid'];
            $filetype=$data['ResourceType'];
            $productid = $data['productid'];
            $productname=$data['ProductName'];
        }
        
        if($data==null){
            return json(['code'=>-1,'data'=>'','msg'=>'ERROR PARAMS 3']);
        }
        
        $downloadpath=$data['FilePath'];
        
        if (!strstr($downloadpath,"http") && !strstr($downloadpath,"ftp:") && !strstr($downloadpath,"www")) {
            $downloadpath = sprintf("https://www.dmake.cn/downloadurl/%s", $downloadpath);
        }
        
        $um=new DetailModel();
        $mid=session('mid');
        $user=$um->getOneInfo($mid);
        
        if($user['AccountLevel']==false||$user['AccountLevel']<4){
            return json(['code'=>100,'data'=>'','msg'=>'尊敬的会员您好，本站资源仅提供认证会员下载（手机认证/邮箱认证/微信认证）！']);
        }
                
        
        $dbdr=Db::name('Catalogdownloadrecord');
        $map['DownloadTime']=['between',[date('Y-m-d 00:00:00'),date('Y-m-d 23:59:59')]];
        $map['UserId']=$mid;
        if($dbdr->where($map)->count()==0){
            
            // 下载记录
            $rid=$dbdr->insertGetId([
                'downloadtype'=>$type,
                'filetype'=>$filetype,
                'fileid'=>$id,
                'UserId'=>$mid,
                'DownloadTime'=>date('Y-m-d H:i:s'),
                'ProductId'=>$productid,
                'SyncCrm'=>0
            ]);
                        
            $cm=new CrmModel();
            $data['Name']=$user['Nickname'];
            $data['Mobile']=$user['Mobile'];
            $data['Email']=$user['Email'];
            $data['Product']=$productname .'-'. $type;            
               
            if ($type == 'download' && $filetype == 0) {
                Db::name('Catalogproductdownload')->where('Id', $data['Id'])->setInc('DownloadQty');
            }
            
            //用户需要订阅厂商的一切资源
            if($ding==0){
                $sm=new SubscribeModel();
                $sm->dingProduct($mid, $productid);
            }
        }
                
        return json(['code'=>1,'data'=>$downloadpath,'msg'=>'']);
    }
}