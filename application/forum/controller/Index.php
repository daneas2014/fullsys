<?php
namespace app\forum\controller;

use app\index\controller\Base;
use app\index\model\ForumModel;
use think\Db;


use think\Loader;

Loader::import('aip/AipContentCensor', EXTEND_PATH);

class Index extends Base
{

    // 论坛版块有需要重新全局定义的东西，都写在下面
    public function _initialize()
    {
        parent::_initialize();


        $toplevel = [];
        // 1. 全局置顶
        $fm = new ForumModel();
        $toplevel2 = $fm->getToplevel(2);
        $toplevel = $toplevel2;

        // 2. 版块置顶
        $fid = input('param.fid');
        if ($fid && $fid > 0 && $toplevel != null) {
            $toplevel1 = $fm->getToplevel(1, $fid);
            array_merge($toplevel, $toplevel2);
        }

        $this->assign([
            'forums' => $fm->getForums(5),
            'hotweeklythreads' => $fm->getHotThreads(),
            'toplevel' => $toplevel,
        ]);
    }

    /**
     * 首页
     */
    public function index()
    {
        $fm = new ForumModel();

        $this->assign([
            'latest' => $fm->getThreads('', 'new'),
            'recommend' => $fm->getThreads('', 'recommend', 5),
            'hot' => $fm->getThreads('', 'hot'),
            'reward' => $fm->getThreads('', 'reward'),
            'morepost' => $fm->getThreads('', 'post'),
            'activity' => $fm->getThreads('activity'),
            'vip' => $fm->getThreads('vip'),
            'bug' => $fm->getThreads('bug'),
            'fun' => $fm->getThreads('fun'),
            'hotforums' => $fm->getForums(12),
            'fid' => 0,
        ]);

        return $this->fetch();
    }

    /**
     * 版块合集
     */
    public function groups()
    {
        $db = Db::name('bbs_forum');

        $all = $db->order('sort desc')->paginate(20);

        $this->assign('page_method', $all->render());
        $this->assign('list', $all);

        return $this->fetch();
    }

    /**
     * 版块中的帖子
     */
    public function forum()
    {
        $id = input('param.fid');
        $supplier = input('param.supplier');

        $fm = new ForumModel();
        $forum = $fm->getForum($id, $supplier);

        if ($forum && $supplier > 0) {
            return redirect('/forum/' . $forum['id']);
        }

        $db = Db::name('bbs_thread');

        $all = $db->field('bbs_thread.id,bbs_thread.create_time,title,author,authorid,lastpost,lastpostid,headimage,views,posts,type')
            ->join('customer', 'authorid=customer.Id', 'left')
            ->where('fid', $forum['id'])
            ->order('id desc')
            ->paginate(20);

        $this->assign([
            'title' => $forum['title'],
            'keywords' => $forum['keywords'],
            'description' => $forum['description'],
            'forum' => $forum,
            'page_method' => $all->render(),
            'list' => $all,
            'fid' => $id,
        ]);

        return $this->fetch();
    }

    public function newthread()
    {
        $fm = new ForumModel();

        if (request()->isPost()) {
            $input = input('post.');

            if (!captcha_check($input['yzm'])) {
                return $this->error('验证码错误!');
            }

            $u = session('memberinfo');

            $input['author'] = $u->nickname;
            $input['authorid'] = $u->id;
            $input['lastpost'] = $u->nickname;
            $input['lastpostid'] = $u->id;
            
            // 过滤违规
            $res = $this->ckPost($input['title'] . $input['detail'], $input['authorid'], request()->ip(), 1);
            if ($res !== 'OK') {
                return $this->error($res);
            }

            if (array_key_exists('file', $input)) {
                unset($input['file']);
            }

            if (array_key_exists('yzm', $input)) {
                unset($input['yzm']);
            }

            if (array_key_exists('rptoken', $input)) {
                unset($input['rptoken']);
            }


            $input['create_time'] = time();

            $result = $fm->saveThread($input);

            if ($result && $input['id'] == '') {
                return $this->success('发帖完毕', '/forum/thread/' . $result);
            }

            if ($result && $input['id'] > 0) {
                return $this->success('发帖完毕', '/forum/thread/' . $input['id']);
            }

            return $this->error('保存失败');
        }

        $forumnames = $fm->getAllForums();

        $fid = input('fid');

        $tid = input('tid');

        $productid = input('productid') ? input('productid') : 0;

        $data = null;

        if ($tid && $tid > 0) {
            $data = $fm->getThread($tid);
        }

        $this->assign([
            'data' => $data,
            'fid' => $fid,
            'allforum' => $forumnames,
            'forums' => $fm->getForums(12),
            'productid' => $productid,
        ]);

        return $this->fetch();
    }

    public function threadpost()
    {
        $fm = new ForumModel();

        if (request()->isPost()) {
            $input = input('post.');

            if (!captcha_check($input['yzm'])) {

                return $this->error('验证码错误!');
            }

            // 过滤违规
            $res = $this->ckPost($input['detail'], $input['authorid'], request()->ip(), 0);
            if ($res !== 'OK') {
                return $this->error($res);
            }

            $post['tid'] = $input['id'];
            $post['detail'] = $input['detail'];
            $post['authorid'] = session('mid');
            $post['author'] = session('memberinfo')['nickname'];
            $post['create_time'] = time();

            $result = $fm->savePost($post);

            if ($result > 0) {

                $db = Db::name('bbs_thread');

                $db->where('id', $input['id'])->setInc('posts');

                $update['lastpost'] = session('memberinfo')['nickname'];
                $update['lastpostid'] = session('memberinfo')['id'];

                $db->where('id', $input['id'])->update($update);

                if (array_key_exists('fid', $post) && $post['fid'] && $post['fid'] > 0) {
                    Db::name('bbs_forum')->where('id', $post['fid'])->setInc('threads');
                }

                return $this->success('您已回帖，即将预览', null, '我们网站将对您的回帖进行审核，如有违反网站条例将对该回帖屏蔽！');
            }

            return $this->error('发布失败');
        }
    }

    /**
     * 帖子
     */
    public function thread()
    {
        $fm = new ForumModel();

        $id = input('param.tid');

        $thread = $fm->getThread($id);

        $files = $thread['attachfiles'];

        $array = null;

        if ($files != '') {
            $array = explode(',', $files);
        }

        $this->assign([
            'thread' => $thread,
            'files' => $array,
        ]);

        $db = Db::name('bbs_post');

        $all = $db->where('tid', $id)
            ->field('bbs_post.*,customer.headimage')
            ->join('customer', 'bbs_post.authorid=customer.id', 'left')
            ->order('id asc')
            ->paginate(20);

        if ($thread['reward'] > 0) {
            $reward = $fm->getThreadReward($thread->id);
            $this->assign('reward', $reward);
        }

        $this->assign('page_method', $all->render());
        $this->assign('list', $all);

        $this->assign([
            'title' => $thread['title'],
            'keywords' => $thread['keywords'],
            'description' => $thread['description'],
        ]);

        return $this->fetch();
    }

    /**
     * 帖子列表，直接传入参数即可
     */
    public function threads()
    {
        $t = input('t') ? input('t') : '';
        switch ($t) {
            case "activity":
                $map['tag'] = [
                    'like',
                    '%active%',
                ];
                break;
            case "vip":
                $map['readperm'] = 1;
                break;
            case "bug":
                $map['tag'] = [
                    'like',
                    '%bug%',
                ];
                break;
            case "fun":
                $map['tag'] = [
                    'like',
                    '%fun%',
                ];
                break;
            default:
                $map = [];
        }

        $o = input('o') ? input('o') : 0;
        switch ($o) {
            case "recommend":
                $order = 'recommend_add desc';
                break;
            case "hot":
                $order = 'views desc';
                break;
            case "reward":
                $order = 'reward desc';
                break;
            case "post":
                $order = 'posts desc';
                break;
            case "new":
                $order = 'id desc';
                break;
            default:
                $order = 'id desc';
        }

        $k = input('kw') ? input('kw') : '';
        if ($k && $k != '') {
            $map['title'] = [
                'like',
                '%' . $k . '%',
            ];
        }

        $db = Db::name('bbs_thread');
        $all = $db->alias('a')
            ->field('a.*,b.headimage')
            ->join('customer b', 'a.authorid=b.id', 'left')
            ->where($map)
            ->order($order)
            ->paginate(20);

        $this->assign('page_method', $all->render());
        $this->assign('list', $all);

        return $this->fetch();
    }

    /**
     * 检查违规信息
     *
     * @param [type] $detail
     * @param [type] $authorid
     * @param [type] $ip
     * @param  $torp 1是topic，0是post
     * @return void
     * @author dmakecn@163.com
     * @since 2021-06-30
     */
    private function ckPost($detail, $authorid, $ip, $torp = 1)
    {

        $client = new \AipContentCensor('16509191', '8pYtCxA5YoGL1w8zehrMmbb1', '1P6UC4pmf4LvTYP08fNCBknkjMWFw3AL');
        $result = $client->textCensorUserDefined($detail);
        // $result = $client->imageCensorUserDefined(file_get_contents('example.jpg'));
        // $result = $client->imageCensorUserDefined('http://www.example.com/image.jpg');// 如果图片是url调用如下

        if (array_key_exists('error_code', $result) && $result['error_code']) {
            return $result['error_msg'];
        }

        if ($result['conclusion'] == '不合规') {
            return $result['data'][0]['msg'];
        }

        /*
        // 违禁词
        $fuck = config('fuck');
        foreach ($fuck['words'] as $f) {
            if (strpos($detail, $f)) {
                return '您的帖子包含屏蔽词汇！';
            }
        }

        //封禁作者
        if (in_array($authorid, $fuck['id'])) {
            return '您的发帖已被拒绝！';
        }
         */

        $ipmap['create_time'] = ['>', time() - 600];
        $ipmap['ip'] = $ip;

        $timemap['authorid'] = $authorid;
        $timemap['create_time'] = ['>', time() - 600];

        if ($torp == 1) {
            // 相同IP下10分钟才能发一次
            $count = Db::name('bbs_thread')->where($ipmap)->count();
            if ($count > 0) {
                return '您的发帖已被拒绝，相同IP发帖间隔时间不能低于10分钟！';
            }

            // 同一用户发帖时间10分钟限制
            $lasttime_t = Db::name('bbs_thread')->where($timemap)->count();
            if ($lasttime_t > 0) {
                return '您的发帖已被拒绝，发帖间隔时间不能低于10分钟！';
            }
        } else {
            $lasttime_p = Db::name('bbs_post')->where($timemap)->count();
            if ($lasttime_p > 0) {
                return '您的发帖已被拒绝，发帖间隔时间不能低于10分钟！';
            }
        }

        // 未认证的人不准发帖
        if (Db::name('customer')->where('Id', $authorid)->value('AccountLevel') < 4) {
            return '您的账号还未实名认证，请认证后再发帖！';
        }

        // 状态不对不准发布
        if (Db::name('customer')->where('Id', $authorid)->value('delete_time') > 0) {
            return '您的账号涉嫌违规已禁止发帖！';
        }

        return 'OK';
    }
}
