<?php
namespace app\forum\controller;

use app\api\controller\UploadApi;

class Upload extends UploadApi
{

    public function thread()
    {
        $res = parent::upload_base64();
        
        $array=[
            "code"=> 0 //0表示成功，其它失败
            ,"msg"=> "" //提示信息 //一般上传失败后返回
            ,"data"=> ["src"=> $res,"title"=> "图片名称" ]
        ];
        
        return json($array);
    }
    
    public function canvas(){
        
        return parent::upload_base64();
    }
}