<?php
namespace app\forum\controller;

use think\Controller;

class Member extends Controller
{

    /**
     * 个人主页，显示积分、收藏、悬赏、发帖内容的汇总
     */
    public function index()
    {}

    /**
     * 积分
     */
    public function integral()
    {}

    /**
     * 收藏
     */
    public function collect()
    {}

    /**
     * 被采纳的
     */
    public function reward()
    {}

    /**
     * 他的主题帖
     */
    public function thread()
    {}

    /**
     * 我的回帖
     *
     * @return void
     * @author dmakecn@163.com
     * @since 2021-02-03
     */
    public function mypost(){
        
    }
}