<?php
namespace app\iotadmin\controller;

use app\iotadmin\controller\Base;
use think\Db;

/**
 * 保存基础信息，区域、设备、网关
 *
 * @author dmakecn@163.com
 * @since 2023-11-17
 */
class Areas extends Base {

	public function list() {
		$db = Db::name('iot_area');

		$map = [];

		$p_parent_id = input("param.key") ? input("param.key") : "";
		if ($p_parent_id && $p_parent_id != "") {
			$map["parent_id"] = $p_parent_id;
		}

		$p_projectid = input('param.project_id') ? input('param.project_id') : 0;
		if ($p_projectid && $p_projectid != "" && $p_projectid > -1) {
			$map["project_id"] = $p_projectid;
		}
		// $p_area_name = input("param.key") ? input("param.key") : "";
		// if ($p_area_name && $p_area_name != "") {
		// 	$map["area_name"] = ['like', "%$p_area_name%"];
		// }

		$admins = Db::name('sys_admin')->field('id,real_name')->where('status', 1)->select();
		$allareas = Db::name('iot_area')->field('area_id,parent_id,area_name')->where('type', '<', 3)->select();

		return parent::vueQuerySingle($db, $map, 'sort asc', ['admins' => $admins, 'areas' => $allareas]);
	}

	public function save() {

		$data = input('param.');

		$db = Db::name('iot_area');
		$allareas = Db::name('iot_area')->field('area_id,parent_id,area_name')->select();
		$tree = childList1($allareas, 'parent_id', 'area_id', 102);
		$admins = Db::name('sys_admin')->field('id,real_name')->where('status', 1)->select();

		return parent::singleDataSave($db, $data, '区域表', ['areas' => $tree, 'admins' => $admins], 'area_id');
	}

	public function uploadpic() {

		$data = input('param.');

		$db = Db::name('iot_area');

		return parent::singleDataSave($db, $data, '区域表');
	}

}