<?php
namespace app\iotadmin\controller;

class Cloud extends Base {

	public function upai() {
		//必须需要修改的参数
		//#################################################################################################
		$bucket = config('cloud')['UP_BUCKET']; //又拍云的服务名
		$form_api_secret = config('cloud')['UP_SECRETKEY']; //表单密钥：后台——>空间——>通用——>基本设置
		$operator = config('cloud')['UP_ACCOUNT']; //授权的操作员
		$password = md5(config('cloud')['UP_PW']); // 授权的操作员密码
		//#################################################################################################
		$GMTdate = gmdate('D, d M Y H:i:s') . ' GMT';
		$method = 'POST';
		$URI = '/' . $bucket;
		$options = array();
		$options['bucket'] = $bucket;
		$options['expiration'] = time() + 3600;
		$options['save-key'] = '/{year}/{mon}/{day}/{filename}{.suffix}'; //save-key 详细说明可以看官方文档
		$options['date'] = $GMTdate;
		$policy = base64_encode(json_encode($options)); //policy 生成
		$str = $method . '&' . $URI . '&' . $GMTdate . '&' . $policy;
		$signature = base64_encode(hash_hmac('sha1', $str, $password, true));
		$authorization = "UPYUN {$operator}:{$signature}";
		$signature = md5($policy . '&' . $form_api_secret); // sigenature生成

		$this->assign('policy', $policy);
		$this->assign('sign', $signature);

		return $this->fetch();
	}

	public function qiniu() {
		$auth = new \Qiniu\Auth(config('cloud')['QINIU_ACCESSKEY'], config('cloud')['QINIU_SECRETKEY']);

		// 初始化上传类
		$upToken = $auth->uploadToken(config('cloud')['QINIU_BUCKET']);

		return $this->view->assign('token', $upToken)->fetch();
	}
}