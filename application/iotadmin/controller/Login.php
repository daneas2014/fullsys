<?php
namespace app\iotadmin\controller;

use app\iotadmin\model\UserType;
use think\Controller;
use think\Db;

class Login extends Controller {

	/**
	 * 登录页面
	 *
	 * @return
	 *
	 */
	public function index() {
		$this->assign('verify_type', config('verify_type'));
		return $this->fetch('/login');
	}

	/**
	 * 登录操作
	 *
	 * @return
	 *
	 */
	public function doLogin() {

		$username = input("param.username");
		$password = input("param.password");
		$code = input("param.code");

		// 验证账号密码
		$result = $this->validate(compact('username', 'password'), 'AdminValidate');
		if (true !== $result) {
			return json([
				'code' => -5,
				'url' => '',
				'msg' => $result,
			]);
		}

		if (config('verify_type') == 1 && !captcha_check($code)) {
			return json([
				'code' => -4,
				'data' => '',
				'msg' => '验证码错误',
			]);
		}

		$hasUser = Db::name('sys_admin')->where('username', $username)->find();
		if (empty($hasUser)) {
			return json([
				'code' => -1,
				'url' => '',
				'msg' => '管理员不存在',
			]);
		}

		if (md5($password . config('auth_key')) != $hasUser['password']) {
			writelog($hasUser['id'], $username, '用户【' . $username . '】登录失败：密码错误', 2);
			return json([
				'code' => -2,
				'url' => '',
				'msg' => '账号或密码错误',
			]);
		}

		if (1 != $hasUser['status']) {
			writelog($hasUser['id'], $username, '用户【' . $username . '】登录失败：该账号被禁用', 2);
			return json([
				'code' => -6,
				'url' => '',
				'msg' => '该账号被禁用',
			]);
		}

		//测试账号拥有管理员权限，但是无法增删改，见Base.php 85行
		if ($hasUser['username'] == 'test') {
			$hasUser['groupid'] = 1;
		}

		// 获取该管理员的角色信息
		$user = new UserType();
		$info = $user->getRoleInfo($hasUser['groupid']);

		session('uid', $hasUser['id'], 'iot'); // 用户ID
		session('username', $hasUser['username'], 'iot'); // 用户名
		session('portrait', $hasUser['portrait'], 'iot'); // 用户头像
		session('rolename', $info['title'], 'iot'); // 角色名
		session('rule', $info['rules'], 'iot'); // 角色节点
		session('name', $info['name'], 'iot'); // 角色权限
		cookie('iot_adint', time() + 3600 * 3);

		// 更新管理员状态
		$param = [
			'loginnum' => $hasUser['loginnum'] + 1,
			'last_login_ip' => request()->ip(),
			'last_login_time' => time(),
			'token' => md5($hasUser['username'] . $hasUser['password']),
		];

		Db::name('sys_admin')->where('id', $hasUser['id'])->update($param);
		writelog($hasUser['id'], session('username', '', 'iot'), '用户【' . session('username', '', 'iot') . '】登录成功', 1);
		return json([
			'code' => 1,
			'url' => url('index/index'),
			'msg' => '登录成功！',
		]);
	}

	/**
	 * 退出登录
	 *
	 * @return
	 *
	 */
	public function loginOut() {
		session(null);
		cache('db_config_data', null); // 清除缓存中网站配置信息
		$this->redirect('login/index');
	}
}
