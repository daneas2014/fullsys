<?php
namespace app\iotadmin\controller;

use app\iotadmin\model\IotInputModel;
use think\Controller;

class Api extends Controller {

	/**
	 * 基于Index.php的这个接口处置方法，要实现可以重写
	 * 用于IOT服务端的请求接收，
	 * IOT还可能遇到对外请求，那么访问就要写在其他action里面了
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2023-11-15
	 */
	public function index() {

		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Methods: POST');
		header('Access-Control-Max-Age: 1000');

		if (!request()->isPost()) {
			return getJsonCode(false, 'API ERRO');
		}

		$data = input('post.');

		if (!array_key_exists('c', $data)) {
			echo '';exit();
		}

		if (!array_key_exists('a', $data)) {
			echo '';exit();
		}

		if (!array_key_exists('t', $data)) {
			echo '';exit();
		}

		$model = $data['c'];
		$act = $data['a'];
		//$auth = phpnullck($data, 'auth', '');

		//1. 鉴权，来自哪个项目、哪个网关
		//TODO
		$token = $data['t'];

		//2.去除重复的参数
		unset($data['c']);
		unset($data['a']);
		unset($data['t']);

		$api = null;

		// 如果是入库时序库，那么需要对这些数据基本组装一下
		if ($model == 'IotInput' && $act == 'save') {
			$obj = json_decode($data['data'], true);
			$api = new IotInputModel();
			$type = $obj['type'];
			$table = $obj['table'];
			$pk = $obj['pk'];
			$params = $obj['params'];
			$tags = $obj['tags'];
			return $api->saveSenserData($table, $pk, $params, $tags);

		}

		$modelName = '\\app\\iotadmin\\model\\' . $model . 'Model';
		$api = new $modelName();

		$param_data = urldecode($data['data']);

		$params = explode('&', $param_data);
		$postdata = [];

		foreach ($params as $one) {
			$key = explode('=', $one)[0];
			$postdata[$key] = explode('=', $one)[1];
		}

		try {
			// return $api->$act($postdata); // public 方法的情况下
			return $api->initialize($postdata); //标准方法，隐藏model中的public方法
		} catch (\Exception $e) {
			return json(['code' => -1, 'data' => $e->getMessage(), 'msg' => '']);
		}

	}
}
