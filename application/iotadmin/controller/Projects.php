<?php
namespace app\iotadmin\controller;

use app\iotadmin\model\ProjectModel;
use think\Db;

class Projects extends Base {

	/**
	 * 获取所有项目
	 *
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2023-11-24
	 */
	public function list() {
		$db = Db::name('project');

		$map['delete_time'] = null;

		$p_key = input("param.key") ? input("param.key") : "";
		if ($p_key && $p_key != "") {
			$map['company'] = ["like", "%$p_key%"];
		}

		$p_address = input("param.address") ? input("param.address") : "";
		if ($p_address && $p_address != "") {
			$map['address'] = ["like", "%$p_address%"];
		}

		return parent::vueQuerySingle($db, $map, 'project_id desc');
	}

	/**
	 * 保存项目，地址用下拉框选择区域规范一点
	 *
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2023-11-24
	 */
	public function save() {
		$db = Db::name('project');
		$data = input('param.');

		/**
		 * 项目只能软删除
		 */
		if (request()->isPost() && $data['act'] == 'del') {
			$pm = new ProjectModel();
			return $pm->deleteProject($data['project_id']);
		}

		return parent::singleDataSave($db, $data, '项目', [], 'project_id');
	}

}