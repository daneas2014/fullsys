<?php

namespace app\iotadmin\controller;
use think\Db;

class Device extends Base {

	/**
	 * 网关列表:网关中绑定了项目id和区域id，主要是面向B端用户的；如果是面向P用户，那么iot_gateway+project+iot_map组合成综合表
	 * 1个网关对应1个区域的情况下，其实也应该把map_id放网关表中，这样才能避免1个地图点多个网关的情况。
	 *
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2023-11-20
	 */
	public function gateways() {
		$db = Db::name('iot_gateway');

		$map = [];

		$p_projectid = input('param.project_id') ? input('param.project_id') : 0;
		if ($p_projectid && $p_projectid != "" && $p_projectid > -1) {
			$map["project_id"] = $p_projectid;
		}

		$p_key = input("param.key") ? input("param.key") : "";
		if ($p_key && $p_key != "") {
			$map['name'] = ["like", "%$p_key%"];
		}
		$p_keepalive = input("param.keepalive") ? input("param.keepalive") : "";
		if ($p_keepalive && $p_keepalive != "" && $p_keepalive > -1) {
			$map["keepalive"] = $p_keepalive;
		}
		$p_shield = input("param.shield") ? input("param.shield") : "";
		if ($p_shield && $p_shield != "" && $p_shield > -1) {
			$map["shield"] = $p_shield;
		}

		return parent::vueQuerySingle($db, $map, 'gateway_id');
	}

	/**
	 * 保存网关
	 *
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2023-11-20
	 */
	public function savegateway() {
		$db = Db::name('iot_gateway');
		$data = input('param.');

		$areas = Db::name('iot_area')->field('area_id,area_name')->select();
		$maps = Db::name('iot_map')->field('id,name,location')->order('id desc')->select();

		$parentid = input('get.parentid') ? input('get.parentid') : 0;
		$this->assign('parentid', $parentid);

		return parent::singleDataSave($db, $data, '网关', ['maps' => $maps, 'areas' => $areas], 'gateway_id');
	}

	/**
	 * 设备类型
	 *
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2023-11-20
	 */
	public function devicetypies() {
		$db = Db::name('iot_device_type');

		$map = [];

		$p_key = input("param.key") ? input("param.key") : "";
		if ($p_key && $p_key != "") {
			$map['name'] = ["like", "%$p_key%"];
		}
		$p_name = input("param.name") ? input("param.name") : "";
		if ($p_name && $p_name != "") {
			$map["name"] = $p_name;
		}

		return parent::vueQuerySingle($db, $map, 'dev_type_id');
	}

	/**
	 * 编辑设备类型
	 *
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2023-11-20
	 */
	public function savedevicetype() {
		$db = Db::name('iot_device_type');
		$data = input('param.');
		return parent::singleDataSave($db, $data, '设备类型表', [], 'dev_type_id');
	}

	/**
	 * 设备列表
	 *
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2023-11-20
	 */
	public function devicelist() {
		$db = Db::name('iot_device');

		$map = [];
		$Nowpage = input('get.page') ? input('get.page') : 1;
		$limits = input('get.rows') ? input('get.rows') : config('list_rows');

		$p_projectid = input('param.project_id') ? input('param.project_id') : 0;
		if ($p_projectid && $p_projectid != "" && $p_projectid > 0) {
			$map["a.project_id"] = $p_projectid;
		}
		$p_key = input("param.key") ? input("param.key") : "";
		if ($p_key && $p_key != "") {
			$map['name'] = ["like", "%$p_key%"];
		}
		$p_dev_type_id = input("param.dev_type_id") ? input("param.dev_type_id") : "";
		if ($p_dev_type_id && $p_dev_type_id != "") {
			$map["dev_type_id"] = $p_dev_type_id;
		}
		$p_area_id = input("param.area_id") ? input("param.area_id") : "";
		if ($p_area_id && $p_area_id != "") {
			$map["area_id"] = $p_area_id;
		}

		$list = $db->alias('a')
			->join('iot_device_type b', 'a.dev_type_id=b.dev_type_id', 'left')
			->join('iot_area c', 'a.area_id=c.area_id', 'left')
			->order('dev_id desc')
			->field('a.*,b.name as dev_type_name,c.area_name')
			->where($map)
			->page($Nowpage, $limits)
			->paginate($limits);

		$data['list'] = $list->items();
		$data['count'] = $list->total();
		$data['page'] = $Nowpage;

		$parentid = input('get.parentid') ? input('get.parentid') : 0;
		$this->assign('parentid', $parentid);

		if (input('get.page')) {
			return json($data);
		}

		return $this->view->fetch();
	}

	/**
	 * 编辑设备
	 *
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2023-11-20
	 */
	public function savedevice() {
		$db = Db::name('iot_device');
		$data = input('param.');

		$gateways = Db::name('iot_gateway')->field('gateway_id,name,project_id,area_id')->order('gateway_id desc')->select();
		$dtypes = Db::name('iot_device_type')->select();

		$parentid = input('get.parentid') ? input('get.parentid') : 0;
		$this->assign('parentid', $parentid);

		return parent::singleDataSave($db, $data, '设备管理', ['gateway' => $gateways, 'types' => $dtypes], 'dev_id');
	}
}