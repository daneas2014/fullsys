<?php
namespace app\iotadmin\controller;

use think\Db;

class Maps extends Base {

	public function list() {
		$db = Db::name('iot_map');

		$map = [];

		$p_key = input("param.key") ? input("param.key") : "";
		if ($p_key && $p_key != "") {
			$map['name'] = ["like", "%$p_key%"];
		}
		$p_project_id = input("param.project_id") ? input("param.project_id") : "";
		if ($p_project_id && $p_project_id != "") {
			$map["a.project_id"] = $p_project_id;
		}

		$parentid = input('get.parentid') ? input('get.parentid') : 0; //parentid就是项目id
		$this->assign('parentid', $parentid);

		$Nowpage = input('get.page') ? input('get.page') : 1;
		$limits = input('get.rows') ? input('get.rows') : config('list_rows'); // 获取总条数

		$list = $db->alias('a')->field('a.*,b.company')
			->join('project b', 'a.project_id=b.project_id', 'left')
			->where($map)
			->page($Nowpage, $limits)
			->paginate($limits);

		$data['list'] = $list->items();
		$data['count'] = $list->total();
		$data['page'] = $Nowpage;

		if (input('get.page')) {
			return json($data);
		}

		return $this->view->fetch();
	}

	public function savemap() {
		$db = Db::name('iot_map');
		$data = input('param.');
		$gateways = Db::name('iot_gateway')->field('gateway_id,name')->order('gateway_id desc')->select();
		return parent::singleDataSave($db, $data, '地图', ['gateways' => $gateways]);
	}
}