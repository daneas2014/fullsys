<?php

namespace app\iotadmin\controller;

use app\iotadmin\model\ConfigModel;

class Config extends Base {

	/**
	 * 获取配置参数
	 */
	public function index() {
		$configModel = new ConfigModel();
		$list = $configModel->getAllConfig();
		$config = [];
		foreach ($list as $k => $v) {
			$config[trim($v['name'])] = $v['value'];
		}
		$this->assign('config', $config);

		$banips = include ROOT_PATH . '/public/banip.php';

		$this->assign('ips', implode(',', $banips));

		return $this->fetch();
	}

	/**
	 * 批量保存配置
	 */
	public function save($config) {

		$configModel = new ConfigModel();
		if ($config && is_array($config)) {
			foreach ($config as $name => $value) {
				$map = array('name' => $name);
				$configModel->SaveConfig($map, $value);
			}
		}
		cache('db_config_data', null,null,'iot');
		$this->success('保存成功！');
	}

	/**
	 * 这个特例在web index.php中就过滤，免得外部IP乱搞，适合小流量网站，大流量会在入口处阻塞？
	 *
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2022-07-26
	 */
	public function banips() {

		$ar = explode(',', input('post.ips'));

		$x = "";
		foreach ($ar as $a) {
			$x .= '"' . $a . '",';
		}
 
		file_put_contents(ROOT_PATH . '/public/banip.php', "<?php return [ $x ];");

		return $this->success('保存成功！',url('index'));
	}
}