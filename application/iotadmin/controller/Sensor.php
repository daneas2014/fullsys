<?php
namespace app\iotadmin\controller;

class Sensor extends Base {

	//加载extra配置里的iotability配置
	protected $sensors;

	public function _initialize() {
		parent::_initialize();
		$this->sensors = config('iotability')['sensor'];
	}

	/**
	 * 各传感器列表通用
	 *
	 * @param [type] $type
	 * @param [type] $title
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2024-01-02
	 */
	private function sensorlist($type, $title) {

		$this->assign(['sensor' => $type,
			'columns' => $this->sensors[$type]['relations'],
			'window' => "$title 传感器",
			'parentid' => input('param.parentid') ? input('param.parentid') : 0,
		]);
		return $this->fetch('index');
	}

	/**
	 * 查看图谱，这里用于接收各类参数并赋值
	 *
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2023-12-06
	 */
	public function atlas() {
		$sensor = input('sensor');
		$legend = $this->sensors[$sensor]['relations'];
		$this->assign(
			[
				'dev_id' => input('dev_id'),
				'legend' => $legend,
				'sensor'=>$sensor,
				'abilitystart' => 3,
				'abilityend' => $this->sensors[$sensor]['length'],
				'charttitle' => input('chart') . '时间序列',
			]
		);
		return $this->fetch();
	}

	/**
	 * 烟雾传感器
	 *
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2023-12-05
	 */
	public function smoke() {
		return $this->sensorlist('smoke', '烟雾');
	}

	/**
	 * 电气火灾传感器
	 *
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2023-12-05
	 */
	public function electronic() {
		return $this->sensorlist('electronic', '电气火灾');
	}

	/**
	 * 可燃气体传感器
	 *
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2023-12-05
	 */
	public function harmfulgas() {
		return $this->sensorlist('harmfulgas', '可燃气体');
	}

	/**
	 * 土壤墒情传感器
	 *
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2023-12-06
	 */
	public function soil() {
		return $this->sensorlist('soil', '土壤墒情');
	}

	/**
	 * 水体传感器
	 *
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2023-12-06
	 */
	public function water() {
		return $this->sensorlist('water', '养殖水体');
	}

	/**
	 * 物流追踪传感器
	 *
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2023-12-06
	 */
	public function transport() {
		return $this->sensorlist('transport', '物流追踪');
	}


	/**
	 * 获取有哪些个sensor名字清单
	 *
	 * @return array
	 * @author dmakecn@163.com
	 * @since 2023-12-06
	 */
	private function getSensorsName() {
		$names = [];
		foreach ($this->sensors as $key => $value) {
			array_push($names, $key);
		}

		return $names;
	}

	/**
	 * 根据传感器类型组装echarts需要的legend
	 *
	 * @param [type] $type
	 * @param [type] $abilitystart  数据表中的能力起始位置-1，同步iotability结构
	 * @return array
	 * @author dmakecn@163.com
	 * @since 2023-12-06
	 */
	private function getAtlasLegend($type, $abilitystart) {
		$legend = [];
		$ability = $this->sensors[$type];

		for ($i = $abilitystart; $i < count($ability) - $abilitystart; $i++) {
			array_push($legend, $ability[$i]);
		}

		return $legend;
	}

}
