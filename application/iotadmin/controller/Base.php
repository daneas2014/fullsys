<?php
namespace app\iotadmin\controller;

use app\iotadmin\model\Node;
use app\iotadmin\model\ProjectModel;
use com\Fastadmin;
use think\Controller;

class Base extends Controller {

	public function _initialize() {
		$uid = session('uid', '', 'iot');
		$username = session('username', '', 'iot');

		if (!$uid || !$username) {
			$this->redirect('login/index');
		}

		$auth = new \com\Auth();
		$module = strtolower(request()->module());
		$controller = strtolower(request()->controller());
		$action = strtolower(request()->action());
		$url = $module . "/" . $controller . "/" . $action;

		//跳过检测以及主页权限
		if (!in_array($uid, [1, 2])) {
			if (!in_array($url, ['admin/index/index', 'admin/index/indexpage', 'admin/upload/upload', 'admin/index/uploadface'])) {
				if (!$auth->check($url, $uid)) {
					$this->error('抱歉，您没有操作权限');
				}
			}
		}

		// 项目节点是核心，给所有功能都要加一个下拉，因此最先加载
		$projects = dcache(IOT_C_P, 'LIST');
		if ($projects == null || $projects == []) {
			$pm = new ProjectModel();
			$projects = $pm->getProjects();
			array_unshift($projects, ['project_id' => 0, 'company' => '默认项目', 'description' => '用于本地部署的项目，当config中project_mult=true时就是平台版本了']);
			dcache(IOT_C_P, 'LIST', $projects);
		}

		$node = new Node();
		$this->assign([
			'username' => session('username', '', 'iot'),
			'portrait' => session('portrait', '', 'iot'),
			'rolename' => session('olename', '', 'iot'),
			'menu' => $node->getMenu(session('rule', '', 'iot')),
			'project_mult' => config('project_mult'),
			'project_map' => config('project_map'),
			'projects' => $projects,
		]);

		$config = cache('db_config_data', '', null, 'iot');

		if (!$config) {
			$config = load_config();
			cache('db_config_data', $config, null, 'iot');
		}

		config($config);

		if (config('web_site_close') == 0 && session('iot_uid') != 1) {
			$this->error('站点已经关闭，请稍后访问~');
		}

		if (config('admin_allow_ip') && session('uid', '', 'iot') != 1) {
			if (in_array(request()->ip(), explode('#', config('admin_allow_ip')))) {
				$this->error('403:禁止访问');
			}
		}
	}

	/**
	 * 适用于单表查询，有id,title,keywords,description的表，前端用VUE展示
	 *
	 * @param unknown $db
	 * @param array $map
	 * @param array $orderby
	 * @param $assign 传入要渲染的参数
	 * @param string $fields 是否需要精准搜索字段
	 * @return \think\response\Json|unknown
	 */
	public function vueQuerySingle($db, $map = [], $orderby = 'id desc', $assign = [], $fields = null) {
		return (new Fastadmin())->vueQuerySingle($db, $map, $orderby, $assign, $fields);
	}

	/**
	 * 适用于双表left join查询，有id,title,keywords,description的表，前端用VUE展示
	 *
	 * @param [type] 表1 alias a
	 * @param [type] 表2 alias b
	 * @param array $map
	 * @param string left join条件 a.x=b,x
	 * @param array $assign
	 * @param [type] $fields
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2024-02-27
	 */
	public function vueQueryDouble($table1, $table2, $map = [], $joincondition, $orderby = 'id desc', $assign = [], $fields = null) {
		return (new Fastadmin())->vueQueryDouble($table1, $table2, $map, $joincondition, $orderby, $assign, $fields);
	}

	/**
	 * 适用于单表保存，主键为id的表,有update_time,create_time
	 *
	 * @param unknown $db
	 * @param unknown $data
	 * @param $assign 传入要渲染的参数
	 * @param $pkfield 自定义的主键
	 * @return \think\response\Json
	 */
	public function singleDataSave($db, $data, $dbname = '', $assign = [], $pkfield = 'id') {

		if (request()->isAjax() && session('username', '', 'iot') == 'test') {
			return json(['code' => -1, 'msg' => '该账号禁止该操作']);
		}

		return (new Fastadmin())->singleDataSave($db, $data, $dbname, $assign, [], [], $pkfield);
	}
}
