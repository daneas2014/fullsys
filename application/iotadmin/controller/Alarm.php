<?php
namespace app\iotadmin\controller;

use think\Db;

class Alarm extends Base {

	public function list() {
		$db = Db::name('iot_alarm');

		$map = [];

		$p_projectid = input('param.project_id') ? input('param.project_id') : 0;
		if ($p_projectid && $p_projectid != "" && $p_projectid > -1) {
			$map["a.project_id"] = $p_projectid;
		}

		$area_id = input('param.area_id') ? input('param.area_id') : 0;
		if ($area_id && $area_id != "" && $area_id > -1) {
			$map["a.area_id"] = $area_id;
		}

		$Nowpage = input('get.page') ? input('get.page') : 1;
		$limits = input('get.rows') ? input('get.rows') : config('list_rows');

		$list = $db->alias('a')
			->join('iot_device b', 'a.dev_id=b.dev_id', 'left')
			->join('iot_area c', 'a.area_id=c.area_id', 'left')
			->join('project p', 'p.project_id=a.project_id', 'left')
			->order('alarm_id desc')
            ->field('a.*,b.name as dev_name,c.area_name,p.company,b.dev_type_id')
			->where($map)
			->page($Nowpage, $limits)
			->paginate($limits);

		$data['list'] = $list->items();
		$data['count'] = $list->total();
		$data['page'] = $Nowpage;

		$parentid = input('get.parentid') ? input('get.parentid') : 0;
		$this->assign('parentid', $parentid);

		if (input('get.page')) {
			return json($data);
		}

		return $this->view->fetch();
	}

	public function save() {
		$data = input('param.');
		$data = Db::name('iot_alarm')->alias('a')
			->join('iot_device b', 'a.dev_id=b.dev_id', 'left')
			->join('iot_area c', 'a.area_id=c.area_id', 'left')
			->join('project p', 'p.project_id=a.project_id', 'left')
			->field('a.*,b.name as dev_name,c.area_name,p.company,b.dev_type_id')
			->find();

		$sensor = config('iotability')['sensor'];

		$type = '';
		switch ($data['dev_type_id']) {
		case "100":
			$type = 'soil';
			break;
		case "101":
			$type = 'water';
			break;
		case "104":
			$type = 'smoke';
			break;
		case "103":
			$type = 'electronic';
			break;
		case "102":
			$type = 'harmfulgas';
			break;
		}

		$legend = $sensor[$type]['relations'];
		$this->assign(
			[
				'data' => $data,
				'dev_id' => input('dev_id'),
				'legend' => $legend,
				'sensor' => $sensor,
				'start' => $data['alarm_time'] - 1800,
				'end' => $data['alarm_time'] + 1800,
				'type' => $type,
				'abilitystart' => 3,
				'abilityend' => $sensor[$type]['length'],
			]
		);

		return $this->fetch();
	}

}