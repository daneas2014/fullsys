<?php
namespace app\iotadmin\controller;
use think\Db;

class Workorder extends Base {

	public function list() {
		$db = Db::name('common_work_order');

		$map = [];

		$p_subject = input("param.key") ? input("param.key") : "";
		if ($p_subject && $p_subject != "") {
			$map["subject"] = $p_subject;
		}

		$p_status = input("param.status") ? input("param.status") : "";
		if ($p_status && $p_status != "") {
			$map["status"] = $p_status;
		}

		$p_gateway_id = input("param.gateway_id") ? input("param.gateway_id") : "";
		if ($p_gateway_id && $p_gateway_id != "") {
			$map["gateway_id"] = $p_gateway_id;
		}

		return parent::vueQuerySingle($db, $map, 'order_id desc');
	}

	public function save() {
		$db = Db::name('common_work_order');
		$data = input('param.');

		if (request()->isGet() && phpnullck($data, 'id', 0) > 0) {
			$one = $db->alias('a')
				->join('iot_gateway b', 'a.gateway_id=b.gateway_id')
				->join('iot_device c', 'a.dev_id=c.dev_id')
				->where('order_id', $data['id'])
				->field('b.name as gateway,c.name as device')
				->find();

			$logs = Db::name('common_work_order_process')
				->where('work_order_id', 'in', $data['id'])
				->order('proc_id desc')
				->select();

			$this->assign(['gateway' => $one['gateway'], 'device' => $one['device'], 'logs' => $logs, 'devices' => []]);
		}

		if (request()->isGet() && phpnullck($data, 'id', 0) == 0) {
			$device = Db::query('SELECT a.dev_id,a.gateway_id,a.name as device,c.name as gateway FROM `iot_device` a
			left join iot_gateway c on a.gateway_id=c.gateway_id');

			$this->assign(['gateway' => '', 'device' => '', 'logs' => [], 'devices' => $device]);
		}

		if(request()->isPost()){
			if(phpnullck($data,'id',0)==0){
				$data['create_time']=date('Y-m-d H:i:s');
			}
		}

		return parent::singleDataSave($db, $data, '工单', [], 'order_id');
	}
}