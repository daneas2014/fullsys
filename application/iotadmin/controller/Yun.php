<?php
namespace app\iotadmin\controller;

use think\Db;

/**
 * 管理云资源
 *
 * @author dmakecn@163.com
 * @since 2019-12-23
 */
class Yun extends Base
{
    /**
     * 展示分类和分类资源
     *
     * @return void
     * @author dmakecn@163.com
     * @since 2019-12-23
     */
    public function index()
    {
        $db = Db::name('yun_source');
        $groupid = input('groupid');
        $kw = input('kw');

        $map['url'] = ['like', '//cdn.dmake.cn%'];
        if ($groupid && $groupid > 0) {
            $map['groupid'] = $groupid;
        }

        if ($kw && $kw != '' && $kw != 'undefined') {
            $map['title'] = ['like', "%$kw%"];
        }

        $orderby = 'groupid desc';

        $Nowpage = input('param.page') ? input('param.page') : 1;
        $limits = input('param.rows') ? input('param.rows') : config('list_rows'); // 获取总条数

        $list = $db->where($map)
            ->page($Nowpage, $limits)
            ->order($orderby)
            ->paginate($limits);

        $data['list'] = $list->items();
        $data['count'] = $list->total();
        $data['page'] = $Nowpage;

        return json($data);
    }

    public function groups()
    {
        return getJsonCode(Db::name('yun_source_group')->select());
    }

    /**
     * 增删分类
     *
     * @return void
     * @author dmakecn@163.com
     * @since 2019-12-23
     */
    public function savegroup()
    {

        $dbg = Db::name('yun_source_group');
        return parent::singleDataSave($dbg, input('param.'));
    }

    /**
     * 修改资源分组和名称
     *
     * @return void
     * @author dmakecn@163.com
     * @since 2019-12-23
     */
    public function savesource()
    {
        $db = Db::name('yun_source');
        return parent::singleDataSave($db, input('param.'));
    }

    public function getsource()
    {
        $item = Db::name('yun_source')->where('id', input('param.id'))->find();
        return getJsonCode($item);
    }

    /**
     * 保存编辑器内的图片地址
     *
     * @return void
     * @author dmakecn@163.com
     * @since 2019-12-25
     */
    public function log()
    {
        if (request()->isPost()) {
            $data = input('param.');
            $data['create_time'] = time();
            return Db::name('yun_source')->insertGetId($data);
        }
    }
}
