<?php

namespace app\iotadmin\controller;

class Gtrackdevice extends Base {

	/**
	 * 获取所有受追踪的设备，展示其tid、trid、以及供宝宝的运输编号，会存在1台设备多个记录的情况
	 *
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2024-04-02
	 */
	public function tracklogs() {

	}

	/**
	 * 选择1台设备，在猎鹰服务里面注册，获得tid，回传给iot_device 的gtrack_tid字段
	 *
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2024-04-02
	 */
	public function regdevice() {

	}

	/**
	 * 获取所有已经注册的设备，以及其tid
	 *
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2024-04-02
	 */
	public function regdevices() {

	}
}