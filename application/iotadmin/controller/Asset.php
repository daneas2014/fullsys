<?php
namespace app\iotadmin\controller;

use app\iotadmin\model\ProjectModel;
use app\iotadmin\model\UserModel;
use think\Db;

class Asset extends Base {

	public function plan() {
		$db = Db::name('iot_asset_plan');

		$map = [];

		$p_key = input("param.key") ? input("param.key") : "";
		if ($p_key && $p_key != "") {
			$map['expire_time'] = ["between", "%$p_key%"];
		}

		$p_dev_id = input("param.dev_id") ? input("param.dev_id") : "";
		if ($p_dev_id && $p_dev_id != "") {
			$map["dev_id"] = $p_dev_id;
		}

		return parent::vueQueryDouble('iot_asset_plan a', 'sys_admin b', $map, 'a.adminid=b.id', 'a.id desc', [], 'a.*,b.real_name');
	}

	public function save() {
		$db = Db::name('iot_asset_plan');
		$data = input('param.');

		//维保人员ID
		$um = new UserModel();
		$users = $um->getUsersByWhere(['groupid' => 2], 1, 100);
		return parent::singleDataSave($db, $data, '维保计划', ['admins' => $users]);
	}

	public function processplan() {
		$db = Db::name('iot_asset_plan_process');
		$post = input('post.');
		$post['adminid'] = session('uid', '', 'iot');
		if (phpnullck($post, 'isend', 0) == 1) {
			Db::name('iot_asset_plan')->where('id', $post['plan_id'])->update(['isend' => 1]);
		}
		return parent::singleDataSave($db, $post, '维保计划执行', ['title' => input('get.title'), 'detail' => input('get.detail'), 'parentid' => input('get.parentid')]);
	}

	public function planlog() {
		$planid = input('get.plan_id');

		$logs = Db::name('iot_asset_plan_process')->alias('a')
			->join('sys_admin b ', 'a.adminid=b.id')
			->where('a.plan_id', $planid)
			->field('a.*,b.real_name')
			->select();

		return $this->assign('logs', $logs)->fetch();
	}

	public function loglist() {
		$map = [];

		$p_key = input("param.key") ? input("param.key") : "";
		if ($p_key && $p_key != "") {
			$map['a.dev_id'] = $p_key;
		}

		return parent::vueQueryDouble('iot_asset_log a', 'iot_device b', $map, 'a.dev_id=b.dev_id', 'expire_time desc', [], 'a.*,b.name,b.description');
	}

	/**
	 * 在上项目的时候就要上项目设备的维保日志啊
	 *
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2024-02-27
	 */
	public function savelog() {
		$db = Db::name('iot_asset_log');
		$data = input('param.');

		//维保人员ID
		$um = new UserModel();
		$users = $um->getUsersByWhere(['groupid' => 2], 1, 100);

		//设备选取
		$pm = new ProjectModel();
		$devs = $pm->getAllDevices();

		return parent::singleDataSave($db, $data, '资产日志表', ['admins' => $users, 'devs' => $devs]);
	}

}