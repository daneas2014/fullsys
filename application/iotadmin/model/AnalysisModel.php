<?php
namespace app\iotadmin\model;

use think\Db;

/**
 * 数据统计和数据分析用的
 *
 * @author dmakecn@163.com
 * @since 2024-03-02
 */
class AnalysisModel {

	/**
	 * 要考虑两个用例：分页查询和图标连续点位
	 *
	 * @param array $input 现在没有用，以后可以拿来做统计日期条件
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2023-12-06
	 */
	public function initialize($input = []) {
		if ($input) {
			switch ($input['act']) {

			// 1. 初始化某天的统计数据
			case "initreport":
				return self::initDayly($input);

			// 2. 时间内各业务的告警趋势
			case "anaarea":
				return self::anaDayly('area', $input);

			// 3. 时间内各项目的告警趋势
			case "anaproject":
				return self::anaDayly('project', $input);

			// 4. 时间内各子系统的告警趋势
			case "anadevice":
				return self::anaDayly('device', $input);

			// 5. 时间内告警类型比例，alarm表里面哪些个能力值数量比例
			case "anability":
				return self::anaAbility($input);
			// 6. 报警数量 日 周 年
			case "datestat":
				return self::statByDate($input);
			case "alarmweek":
				return self::statAlarmByWeek($input);
			}

		}
	}

	/**
	 * 初始化任务，推荐是写入系统cron里面去
	 *
	 * @param [type] $input
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2024-03-02
	 */
	private function initDayly($input) {

		$day = $input['day'];
		$sqlcheck = "delete from iot_dayly_report where date_time='$day'";

		Db::execute($sqlcheck);

		$initdevice = "select '$day' as 'date_time',
        UNIX_TIMESTAMP(now()) as 'create_time',
        c.name as 'dataid',
        'device' as 'datatype',
        sum(case when a.level>3 then 1 else 0 end) as 'danger_num',
        count(a.alarm_id) as 'warn_num',
        sum(case when b.status=1 then 1 else 0 end )  as 'things_normal',
        sum(case when b.status=0 then 1 else 0 end)  as 'things_offline',
        sum(case when b.shield=1 then 1 else 0 end)  as 'things_shield'
        from iot_device b
        left join iot_device_type c on b.dev_type_id=c.dev_type_id
        left join iot_alarm a on a.dev_id=b.dev_id
        group by b.dev_type_id";

		$device = Db::query($initdevice);

		$initarea = "select '$day' as 'date_time',
        UNIX_TIMESTAMP(now()) as 'create_time',
        c.area_name as 'dataid',
        'area' as 'datatype',
        sum(case when a.level>3 then 1 else 0 end) as 'danger_num',
        count(a.alarm_id) as 'warn_num',
        sum(case when b.status=1 then 1 else 0 end )  as 'things_normal',
        sum(case when b.status=0 then 1 else 0 end)  as 'things_offline',
        sum(case when b.shield=1 then 1 else 0 end)  as 'things_shield'
        from iot_device b
				left join iot_area c on b.area_id=c.area_id
        left join iot_alarm a on a.dev_id=b.dev_id
        group by b.area_id";

		$area = Db::query($initarea);

		$initproject = "select '$day' as 'date_time',
        UNIX_TIMESTAMP(now()) as 'create_time',
        c.name as 'dataid',
        'project' as 'datatype',
        sum(case when a.level>3 then 1 else 0 end) as 'danger_num',
        count(a.alarm_id) as 'warn_num',
        sum(case when b.status=1 then 1 else 0 end )  as 'things_normal',
        sum(case when b.status=0 then 1 else 0 end)  as 'things_offline',
        sum(case when b.shield=1 then 1 else 0 end)  as 'things_shield'
        from iot_device b
				left join project c on b.project_id=c.project_id
        left join iot_alarm a on a.dev_id=b.dev_id
        group by b.project_id";

		$project = Db::query($initproject);

		$list = array_merge($device, $area, $project);

		$res = Db::name('iot_dayly_report')->insertAll($list);

		if ($res > 0) {
			return getJsonCode(true, "已初始化当日报告");
		}

		return getJsonCode(false, "初始化失败");
	}

	//按分类获取走势
	private function anaDayly($datatype, $input) {

		$data['list'] = Db::name('iot_dayly_report')->where('datatype', $datatype)
			->where('date_time', 'between', [$input['start'], $input['end']])
			->order('date_time asc')
			->select();

		$data['timepoints'] = Db::name('iot_dayly_report')->where('datatype', $datatype)
			->where('date_time', 'between', [$input['start'], $input['end']])
			->group('date_time')
			->field('date_time')
			->order('date_time asc')
			->select();

		return getJsonCode($data);
	}

	/**
	 * 能力值报警
	 *
	 * @param [type] $input
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2024-03-02
	 */
	private function anaAbility($input) {
		$start = strtotime($input['start']);
		$end = strtotime($input['end']);
		$sql = "SELECT dev_attr_ids,count(alarm_id) as num FROM  iot_alarm where alarm_time between $start and $end group by dev_attr_ids";

		$statAttrs = Db::query($sql);

		$attr = [];
		$res = [];

		foreach ($statAttrs as $e) {
			$es = explode(",", $e['dev_attr_ids']);

			// 给每个能力值建一个档案
			foreach ($es as $s) {
				if (!in_array($s, $attr)) {
					// 给新建能力值赋值
					$res[$s] = $e['num'];
					array_push($attr, $s);
					continue;
				} else {
					// 修改已有的能力值
					$res[$s] += $e['num'];
					continue;
				}

			}
		}

		$data['stat_attrs'] = $res;
		$data['name_attrs'] = Db::name('iot_device_attr')->select();
		$data['native_attrs'] = $statAttrs;

		return getJsonCode($data);
	}

	private function statByDate($input) {

		$n = time();
		$d_s = strtotime("now");
		$w_s = strtotime("-1 week");
		$y_s = strtotime("-1 year");
		$sql = "(SELECT count(1) as 'num','day' FROM iot_alarm where alarm_time between $d_s and $n)
		union (SELECT count(1) as 'num','week' FROM iot_alarm where alarm_time between $w_s and $n)
		union (SELECT count(1) as 'num','year' FROM iot_alarm where alarm_time between $y_s and $n)";

		$data = Db::query($sql);

		return getJsonCode($data);
	}

	private function statAlarmByWeek($input) {

		$sql = "(SELECT count(1) as 'num','".date('d')."' as day FROM iot_alarm where alarm_time between " . strtotime('-1 day') . " and " . time() . ")
		union (SELECT count(1) as 'num','".date('d', strtotime('-1 day'))."' FROM iot_alarm where alarm_time between " . strtotime('-2 day') . " and " . strtotime('-1 day') . ")
		union (SELECT count(1) as 'num','".date('d', strtotime('-2 day'))."' FROM iot_alarm where alarm_time between " . strtotime('-3 day') . " and " . strtotime('-2 day') . ")
		union (SELECT count(1) as 'num','".date('d', strtotime('-3 day'))."' FROM iot_alarm where alarm_time between " . strtotime('-4 day') . " and " . strtotime('-3 day') . ")
		union (SELECT count(1) as 'num','".date('d', strtotime('-4 day'))."' FROM iot_alarm where alarm_time between " . strtotime('-5 day') . " and " . strtotime('-4 day') . ")
		union (SELECT count(1) as 'num','".date('d', strtotime('-5 day'))."' FROM iot_alarm where alarm_time between " . strtotime('-6 day') . " and " . strtotime('-5 day') . ")
		union (SELECT count(1) as 'num','".date('d', strtotime('-6 day'))."' FROM iot_alarm where alarm_time between " . strtotime('-7 day') . " and " . strtotime('-6 day') . ")";

		$data = Db::query($sql);

		return getJsonCode($data);
	}
}