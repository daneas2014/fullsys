<?php

namespace app\iotadmin\model;

use app\iotadmin\model\IotOutputModel;
use think\Db;

class AdminNeedsModel {

	public function AdminIndex() {
		$db_a = Db::name('iot_alarm');
		$today = $db_a->where('alarm_time', 'between', [strtotime(date('Y-m-d')), time()])->count();
		$month = $db_a->where('alarm_time', 'between', [strtotime(date('Y-m-1')), time()])->count();

		$device = Db::name('iot_device')->count();

		$project = Db::name('project')->count();

		$iot = new IotOutputModel();
		$data_n_d = $iot->statDataCount(strtotime(date('Y-m-d')), time());
		$data_n_m = $iot->statDataCount(strtotime(date('Y-m-1')), time());

		$data['a_d'] = $today;
		$data['a_m'] = $month;
		$data['d_t'] = $device;
		$data['p_t'] = $project;
		$data['td_d'] = $data_n_d;
		$data['td_m'] = $data_n_m;

		return $data;
	}
}