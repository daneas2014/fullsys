<?php
namespace app\iotadmin\model;

use think\Db;

class NewsModel {

	public function initialize($input = []) {
		if ($input) {
			switch ($input['act']) {
			case "projects":
				return self::serviceProjects($input);
			case "g_alarm":
				$data = [
					['title' => '预计2024年4月11日有大暴雨', 'notice_time' => '2024年3月10日', 'alarm_type' => 'danger'],
					['title' => '预计2024年3月20日有大幅升温', 'notice_time' => '2024年3月9日', 'alarm_type' => 'warning'],
					['title' => '预计2024年3月11日有大幅降温', 'notice_time' => '2024年3月1日', 'alarm_type' => 'normal'],
				];
				return getJsonCode($data);
			}
		}
	}

	private function serviceProjects($input) {
		$type = $input['type'];
		$area_id = $input['area_id'];
		$cond = [];
		$data = [];
		$db = Db::name('project');

		if ($area_id > 0) {
			$cond['g.area_id'] = $area_id;
		}

		if ($type == 'list') {

			$data = $db->alias('p')->join('iot_gateway g', 'p.project_id=g.project_id', 'left')
				->where($cond)
				->distinct(true)
				->field('p.project_id,p.company,p.overdate_time,p.status,p.address,p.mobile')
				->select();

		}

		if ($type == 'pages') {

			$page = $input['p_i'];
			$limit = $input['p_s'];
			$page = $page ? $page : 1;
			$limit = $limit ? $limit : 10;
			$status = $input['overdate'];
			$today = date('Y-m-d');
			if ($status == 1) {
				$cond['p.overdate_time'] = ['< time', $today];
			}
			if ($status == 0) {
				$cond['p.overdate_time'] = ['> time', $today];
			}
			$data['list'] = $db->alias('p')
				->join('iot_gateway g', 'p.project_id=g.project_id', 'left')
				->where($cond)
				->distinct(true)
				->page($page, $limit)
				->field('p.project_id,p.company,p.overdate_time,p.status,p.address,p.mobile')
				->select();

			$data['count'] = $db->alias('p')
				->join('iot_gateway g', 'p.project_id=g.project_id', 'left')
				->distinct('company')
				->where($cond)
				->count();

		}

		return getJsonCode($data);
	}
}