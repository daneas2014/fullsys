<?php
namespace app\iotadmin\model;

use think\Db;
use Yurun\TDEngine\ClientConfig;
use Yurun\TDEngine\Exception\OperationException;
use Yurun\TDEngine\TDEngineManager;

/**
 * 输入时序数据
 *
 * @author dmakecn@163.com
 * @since 2023-12-02
 */
class IotInputModel {

	public function initialize($input = []) {
		if ($input) {
			switch ($input['act']) {
			case "alarmreg": // 来自外部的报警登记
				return self::setAlarmReg($input);
			case "alarmproc": //处置告警
				return self::setAlarmProc($input);
			}
		}
	}

	private function setAlarmReg($input) {
		$alarm['dev_id'] = $input['DEV_ID'];

		$dev = Db::name('iot_device')->where('dev_id', $alarm['dev_id'])->find();
		if ($dev == null || $dev == []) {
			return getJsonCode(-1, '该设备未注册');

		}

		$alarm['gateway_id'] = $dev['gateway_id'];
		$alarm['area_id'] = $dev['area_id'];
		$alarm['project_id'] = $dev['project_id'];

		$alarm['level'] = $input['ALARM_LEVEL'];
		$alarm['data_id'] = $input['ALARM_DATA_ID'];
		$alarm['status'] = 0;
		$alarm['cap_url'] = '';
		$alarm['code_id'] = $input['ALARM_CODE'];
		$alarm['dev_attr_ids'] = $input['ALARM_TAG']; //逗号分割的字符串哦
		$alarm['alarm_time'] = $input['CREATE_TIME'];
		$alarm['update_time'] = 0;
		$alarm['create_time'] = time();

		$res = Db::name('iot_alarm')->insertGetId($alarm);

		//自动生成工单
		Db::name('common_work_order')->insert(['user_id' => 0, 'process_user_id' => 0, 'subject' => '物联设备监测告警', 'type' => 2, 'status' => 0, 'alarm_id' => $res, 'gateway_id' => $alarm['gateway_id'], 'dev_id' => $alarm['dev_id'], 'gard' => $alarm['level'], 'processes' => '', 'update_time' => time(), 'create_time' => time()]);

		if ($res > 0) {
			return getJsonCode(true, '报警已入库');
		}

		return getJsonCode(-1, '报警入库失败');

	}

	/**
	 * TODO : 这里需要用事务来做
	 *
	 * @param [type] $input
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2024-03-24
	 */
	private function setAlarmProc($input) {
		$alarm_id = $input['alarm_id']; //报警id
		$proc_type = $input['proc_type']; //报警评价
		$proc_content = $input['proc_content']; //处置结果

		// 关停告警
		Db::name('iot_alarm')->where('alarm_id', $alarm_id)->update(['status' => 1, 'update_time' => time()]);

		// 处置告警工单
		$order = Db::name('common_work_order')->where('alarm_id', $alarm_id)->find();

		// 生成工单详情
		$process = Db::name('common_work_order_process')->insertGetId(['work_order_id' => $order['order_id'], 'user_id' => 0, 'before' => 1, 'current' => 1, 'message' => "[$proc_type]$proc_content", 'update_time' => time(), 'create_time' => time()]);

		Db::name('common_work_order')->where('order_id', $order['order_id'])->update(['processes' => ($order['processes'] ? "" : $order['processes'] . ",") . $process]);

		return getJsonCode(true, '告警已处置');
	}

	/**
	 * 保存传感器的数据
	 *
	 * @param [type] $table 表名
	 * @param [type] $pk 设备ID
	 * @param [type] $data 数据
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2024-08-19
	 */
	public function saveSenserData($table, $pk, $data, $tag, $type = 'single') {
		//1. 检查是否存在超级表
		if (!$this->checkStable($table)) {
			return ['code' => -1, 'msg' => '不存在数据表'];
		}
		if (!$data) {
			return ['code' => -1, 'msg' => '数据参数有误'];
		}

		if (!$tag) {
			return ['code' => -1, 'msg' => '数据标识有误'];
		}

		//2. 生成插入数据sql
		switch ($type) {
		case 'single':
			$sql = $this->writeSubtable($pk, $table, $data, $tag);
			break;
		case 'mult':
			$sql = $this->writeSubtableMult($pk, $table, $data, $tag);
			break;
		default:
			return ['code' => -1, 'msg' => '未指定操作类型'];
		}

		//3. 执行
		$res = $this->tdconnector($sql);
		die($res);

		//4. 反馈

		return $res;
	}

	//////////////////////////////////////以下为应用心得/////////////////////////////////////////

	/**
	 * https://www.taosdata.com/docs/cn/v2.0/connector/php
	 * tdengine的查询工具
	 * 注意注意：这个东西是给admin用的，因为时序库主要是在图表中体现点位
	 * 所以原生的将分类、数据点分离是正确的，不用加工
	 *
	 * @param string $sql
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2023-12-01
	 */
	private function tdconnector($sql = '') {
		// 增加名称为 test 的连接配置
		TDEngineManager::setClientConfig('test', new ClientConfig(config('tdengine')));
		// 设置默认数据库为test
		TDEngineManager::setDefaultClientName('test');
		// 获取客户端对象（\Yurun\TDEngine\Client）
		$client = TDEngineManager::getClient();

		try
		{
			// 执行查询
			$result = $client->sql($sql);
			return $result->getResponse();
		} catch (OperationException $e) {
			return ['code' => -1, 'msg' => $e->getMessage()];
		}

	}

	/**
	 * 检查是否存在指定超级表
	 *
	 * @param [type] 表名
	 * @return void
	 * @author dmakecn@163.com https://docs.taosdata.com/develop/model/
	 * @since 2023-12-25
	 */
	private function checkStable($name) {
		$tables = ['', '', '', ''];

		if (in_array($name, $tables)) {
			return true;
		}

		// 这里字段不能用varchar、decimal等，要用binary(4n)和float，因此在设计的时候把位数想好，TAGS后面的是外键，用于定位子表
		$example = 'CREATE STABLE meters (ts timestamp, current float, voltage int, phase float) TAGS (location binary(64), groupId int);';
		return true;
	}

	/**
	 * 创建超级表的子表并插入数据
	 *
	 * @param [type] $pk  主键，一般是设备ID
	 * @param [type] $table  存的超级表
	 * @param [type] $data  顺序存入的数据'type,value'
	 * @param [type] $tags 写入库中的标识'type,value'
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2024-08-22
	 */
	private function writeSubtable($pk, $table, $data = [], $tags = []) {
		// 承接上一个function，d1001属于子表的别名，用英文和编号的好处是规范，using后面这一坨，就是上述的外键，我想应该是方便快速查询吧
		// 案例同时做了2件事：用超级表的结构，向tags的标识内写入了数据  INSERT INTO d1001 USING metra TAGS ("California.SanFrancisco", 2) VALUES (NOW, 10.2, 219, 0.32);";
		$sb = $table . '_' . $pk;

		$sql = "INSERT INTO $sb USING $table TAGS(";

		// 1. 组装标识
		$first = true;
		foreach ($tags as $key => $v) {
			$sql .= ($first ? $first = false : ",") . ($key == 'nchar' ? "'$v'" : $v);
		}
		$sql .= ") VALUES (NOW,";

		// 2.组装value
		$first = true;
		foreach ($data as $key => $v) {
			$sql .= ($first ? $first = false : ",") . ($key == 'nchar' ? "'$v'" : $v);
		}
		$sql .= ");";

		return $sql;
	}

	/**
	 * 写入单表：一条记录不能超过 48KB，一条 SQL 语句总长度不能超过 1MB。
	 * https://docs.taosdata.com/develop/insert-data/sql-writing/
	 *
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2023-12-25
	 */
	private function writeSubtableMult($pk, $table, $data) {
		// 承接上一个function，给子表写数据，属于直接写咯，tags就没有用到，随便写咯
		$sb = $table . '_' . $pk;
		$datastr = '';

		for ($i = 1; $i <= count($data); $i++) {
			$datastr = "(ts$i," . implode(',', $data[$i - 1]) . ")";
		}

		return "INSERT INTO $sb USING $table  VALUES $datastr;";
	}

	/**
	 * 写入多表：一条记录不能超过 48KB，一条 SQL 语句总长度不能超过 1MB。
	 * https://docs.taosdata.com/develop/insert-data/sql-writing/
	 *
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2023-12-25
	 */
	public function writeMultTables() {
		// 同时向多个子表写入数据
		$example = 'INSERT INTO d1001 VALUES (ts1, 10.3, 219, 0.31) (ts2, 12.6, 218, 0.33) d1002 VALUES (ts3, 12.3, 221, 0.31);';
	}
}