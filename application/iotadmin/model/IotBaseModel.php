<?php

namespace app\iotadmin\model;

use think\Db;

class IotBaseModel {

	public function initialize($input = []) {
		if ($input) {
			switch ($input['act']) {
			case "businessgateways": //获取大屏的一级二级导航
				return self::getBusinessGateways($input);
			case "map": //点击一二级导航显示地图的点位
				return self::getMaps($input);
			case "maps": //分页获取项目的点位信息
				return self::getMapList($input);
			case "projects": //获取全部项目或指定的项目id
				return self::getProjects($input);
			case "mappoint": //点击项目基地点位，获取项目详情和设备数量统计
				return self::getMapPoint($input);
			case "subsys": //各子系统的设备列表
				return self::getSysDivices($input);
			case "subgateways":
				return self::getSubSysGateway($input);
			case "iotdevice": //获取系统的硬件设备，包含网关和监测设备
				return self::getIotDevices($input);
			case "subnav": //大屏子系统导航
				$list = [
					["subsys_type" => "soil", "sort" => 0, "subsys_name" => "土壤墒情监测", "sub_url" => "default"],
					["subsys_type" => "water", "sort" => 1, "subsys_name" => "水源监测", "sub_url" => "default"],
					["subsys_type" => "harmfulgas", "sort" => 2, "subsys_name" => "有害气体监测", "sub_url" => "default"],
					["subsys_type" => "electronic", "sort" => 3, "subsys_name" => "电气监测", "sub_url" => "default"],
					["subsys_type" => "transport", "sort" => 4, "subsys_name" => "物流追踪", "sub_url" => "_blank"],
					["subsys_type" => "smoke", "sort" => 5, "subsys_name" => "火灾监测", "sub_url" => "default"],
					//["subsys_type" => "wather", "sort" => 5, "subsys_name" => "农业气象监测","sub_url"=>"http://products.weather.com.cn/sk.html?page=NY"],
				];
				return getJsonCode($list);
			case "devinfo":
				return self::getDevInfo($input);
			case "alarminfo":
				return self::getAlarmInfo($input);
			case "abilities":
				$sensors = config('iotability')['sensor'];
				$type = $input['type'];
				$legend = $sensors[$type]['relations'];
				$end_index = $sensors[$type]['length'];

				return getJsonCode(array_slice($legend, 3, $end_index));

			}
		}
	}

	private function getAlarmInfo($input) {
		$alarm_id = $input['alarm_id'];
		$data = Db::name('iot_alarm')->where('alarm_id', $alarm_id)->find();
		return getJsonCode($data);
	}

	/**
	 * 一个项目可能有多个点位，加载该项目的所有点位
	 *
	 * @param [type] $input
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2023-11-28
	 */
	private function getMaps($input) {

		$area_id = $input['area_id'];
		$projectid = $input['project_id'];
		$cond = [];
		if ($area_id > 0) {
			$cond['b.area_id'] = $area_id;
		}
		if ($projectid > 0) {
			$cond['b.project_id'] = $projectid;
		}

		$list = Db::name('iot_map')->alias('a')
			->join('(select DISTINCT map_id,area_id,project_id from iot_gateway) b', 'a.id=b.map_id', 'left')
			->field('a.name,a.thumbnail,a.description,a.location,b.area_id,b.project_id,map_id')
			->where($cond)
			->select();

		return getJsonCode($list);
	}

	/**
	 * 获取项目列表
	 *
	 * @param [type] $input 传入project_ids就传入指定的，否则就传输整个
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2023-11-28
	 */
	private function getProjects($input) {

		if ($input == [] || phpnullck($input, 'project_ids', 0) == 0) {
			$list = Db::name('project')->select();
		} else {
			$list = Db::name('project')->where('project_id', 'in', $input['project_ids'])->select();
		}

		return getJsonCode($list);
	}

	/**
	 * 分页获取地图点位
	 *
	 * @param [type] $input 传入map_ids=0就是返回所有地图信息
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2023-11-28
	 */
	private function getMapList($input) {

		if (phpnullck($input, 'map_ids', 0) == 0) {
			$maps = Db::name('iot_map')->select();
			return getJsonCode($maps);
		}

		$key = array_key_exists('key', $input) ? $input['key'] : '';
		$page = $input['p_i'];
		$limit = $input['p_s'];
		$cond = getCond($input['c_d']);
		if ($key && $key != '') {
			$cond['a.title'] = [
				'like',
				'%' . $key . '%',
			];
		}

		$page = $page ? $page : 1;
		$limit = $limit ? $limit : 10;

		$data = Db::name('iot_map')->alias('a')
			->field('a.*,b.name as project_name')
			->join('project b', 'b.project_id=a.project_id', 'left')
			->where($cond)
			->page($page, $limit)
			->order('id desc')
			->select();

		return getJsonCode($data);
	}

	/**
	 * 获取某业务分区下的所有网关信息
	 *
	 * @param [type] $input
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2024-02-28
	 */
	private function getBusinessGateways() {
		// 获取了项目、区域、基地的关联表
		$pma = Db::name('iot_gateway')->alias('g')
			->join('iot_map m', 'g.map_id=m.id', 'left')
			->join('iot_area a', 'a.area_id=g.area_id', 'left')
			->join('project p', 'p.project_id=g.project_id', 'left')
			->order('a.sort asc')
			->field('DISTINCT a.area_name,a.area_id,a.sort,p.company,p.project_id,p.name as project_name,p.mobile,p.address,g.lonlat')
			->select();

		$areas = Db::name('iot_area')->field('area_id,area_name')->order('sort')->select();

		$list = [];
		foreach ($areas as $a) {

			$projects = [];
			foreach ($pma as $t) {
				if ($t['area_id'] == $a['area_id']) {
					array_push($projects, $t);
					continue;
				}
			}
			$a['projects'] = $projects;
			array_push($list, $a);
		}

		return getJsonCode($list);
	}

	/**
	 * 点击项目基地点位，获取项目详情和设备数量统计
	 *
	 * @param [type] $input
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2024-02-28
	 */
	private function getMapPoint($input) {
		$project = Db::name('project')
			->where('project_id', $input['project_id'])
			->field('company,name,mobile,thumbnail,project_id,province,street,city,address')
			->find();

		$nums = Db::query("select count(dev_id) as dev_total,count(gateway_id) as gateway_total from (select d.dev_id,g.gateway_id from iot_device d left join iot_gateway g on d.gateway_id=g.gateway_id where g.project_id=" . $input['project_id'] . ") as t");

		$map = Db::name('iot_map')
			->where('id', $input['map_id'])
			->field('id as map_id,name,location,description,thumbnail')
			->find();
		$project['total'] = $nums;
		$project['map'] = $map;
		return getJsonCode($project);
	}

	/**
	 * 获取子系统列表
	 *
	 * @param [type] $input
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2024-02-28
	 */
	private function getSysDivices($input) {
		$type = $input['type'];
		$page = $input['p_i'];
		$limit = $input['p_s'];

		$page = $page ? $page : 1;
		$limit = $limit ? $limit : 10;

		$dev_type = 0;
		switch ($type) {
		case "soil":
			$dev_type = 100;
			break;
		case "water":
			$dev_type = 101;
			break;
		case "harmfulgas":
			$dev_type = 102;
			break;
		case "electronic":
			$dev_type = 103;
			break;
		case "transport":
			$dev_type = 104;
			break;
		case "smoke":
			$dev_type = 105;
			break;
		}

		$cond['dev_type_id'] = $dev_type;

		if ($input['area_id'] > 0) {
			$cond['area_id'] = $input['area_id'];
		}

		if ($input['project_id'] > 0) {
			$cond['project_id'] = $input['project_id'];
		}

		if ($input['status'] > -1) {
			$cond['status'] = $input['status'];
		}

		if ($input['shield'] > -1) {
			$cond['shield'] = $input['shield'];
		}

		$data = Db::name('iot_device')
			->where($cond)
			->page($page, $limit)
			->select();

		return getJsonCode($data);

	}

	/**
	 * 获取系统设备，包含网关、监测站
	 *
	 * @param [type] $input
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2024-03-01
	 */
	private function getIotDevices($input) {

		$type = $input['devicetype'];
		$cond = getCond(phpnullck($input, 'c_d', ''));
		$page = phpnullck($input, 'p_i', 0);
		$limit = phpnullck($input, 'p_s', 10);
		$page = $page ? $page : 1;
		$limit = $limit ? $limit : 10;

		$data = [];

		if ($type == 'gateway') { //网关设备
			$data['list'] = Db::name('iot_gateway')->alias('g')
				->join('project p', 'p.project_id=g.project_id', 'left')
				->where($cond)
				->page($page, $limit)
				->order('gateway_id desc')
				->field('g.gateway_id,g.project_id,g.name as gateway_name,g.lonlat,g.status,g.shield,g.create_time,p.company,p.mobile ')
				->select();

			$data['count'] = Db::name('iot_gateway')->alias('g')
				->where($cond)
				->count();

		} else if ($type == 'device') { //监测站
			$data['list'] = Db::name('iot_device')->alias('d')
				->join('iot_gateway g', 'd.gateway_id=g.gateway_id', 'left')
				->join('project p', 'p.project_id=d.project_id', 'left')
				->where($cond)
				->page($page, $limit)
				->order('gateway_id desc')
				->field('d.dev_id,d.name as device_name, d.dev_type_id,d.status,d.shield,d.create_time,g.gateway_id,g.project_id,g.name as gateway_name,g.lonlat,p.company,p.mobile')
				->select();

			$data['count'] = Db::name('iot_device')->alias('d')
				->where($cond)
				->count();

		} else if ($type == 'all') { //所有设备
			$data['list'] = Db::query("(select gateway_id as id,name,'gateway' as dtype ,status,shield,create_time from iot_gateway g) union (select dev_id as id,name,'device' as dtype ,status,shield,create_time from iot_device)");
		}

		return getJsonCode($data);
	}

	/**
	 * 获取子系统下所有的网关，用于子系统地图展示
	 *
	 * @param [type] $input
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2024-03-02
	 */
	private function getSubSysGateway($input) {

		$dev_type = 0;
		switch ($input['subsys']) {
		case "soil":
			$dev_type = 100;
			break;
		case "water":
			$dev_type = 101;
			break;
		case "harmfulgas":
			$dev_type = 102;
			break;
		case "electronic":
			$dev_type = 103;
			break;
		case "smoke":
			$dev_type = 104;
			break;
		}

		$cond['d.dev_type_id'] = $dev_type;

		$gateways = Db::name('iot_gateway')->alias('g')
			->join('(select gateway_id, dev_type_id,count(dev_id) as dev_num from iot_device group by dev_type_id,gateway_id)  d', 'g.gateway_id=d.gateway_id', 'left')
			->where($cond)
			->field('g.gateway_id,g.name as gateway_name,g.lonlat,d.dev_num,g.project_id')
			->distinct(true)
			->select();

		return getJsonCode($gateways);
	}

	private function getDevInfo($input) {

		$dev_id = $input['dev_id'];

		$dev = Db::name('iot_device')->where('dev_id', $dev_id)->find();

		$gateway = Db::name('iot_gateway')->where('gateway_id', $dev['gateway_id'])->find();

		$project = Db::name('project')->where('project_id', $dev['project_id'])->find();

		$data['dev'] = $dev;
		$data['project'] = $project;
		$data['gateway'] = $gateway;

		return getJsonCode($data);

	}
}