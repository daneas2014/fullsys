<?php
namespace app\iotadmin\model;

use think\Model;
use traits\model\SoftDelete;

class ProjectModel extends Model {

	protected $name = 'project';

	// 开启自动写入时间戳字段
	protected $autoWriteTimestamp = true;

	use SoftDelete;
	protected $deleteTime = "delete_time";

	/**
	 * 获取全部正常的项目，默认输出id及名称,排除的*，用哪些字段传入哪些字段
	 */
	public function getProjects($fields = 'project_id,company') {

		$list = dcache(IOT_C_P, 'ALL');

		if (!$list || $list == []) {

			$list = $this->field($fields)->where('status', 1)->select();

			dcache(IOT_C_P, 'ALL', $list);
		}
		return $list;
	}

	/**
	 * 系统中的项目只能软删除
	 *
	 * @param [type] $id
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2023-11-27
	 */
	public function deleteProject($id) {
		if (ProjectModel::destroy($id) == 1) {
			return ['code' => 1, 'msg' => '软删除成功'];
		}

		return ['code' => 0, 'msg' => '软删除失败'];
	}

	/**
	 * 获取苏哦有项目与设备的关联，方便select组件使用
	 *
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2024-02-27
	 */
	public function getAllDevices() {
		$list = $this->alias('a')
			->join('iot_device b', 'a.project_id=b.project_id', 'right')
			->field('a.name,b.project_id,b.name as device,b.dev_id,b.create_time')
			->select();
		return $list;
	}
}