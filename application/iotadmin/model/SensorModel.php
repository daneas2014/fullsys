<?php
namespace app\iotadmin\model;

class SensorModel {
    
	protected $sensors = [];

	public function __construct() {
		$this->sensors = config('iotability');
	}

    public function getSensorColumns($type){
        return $this->sensors[$type];
    }
}