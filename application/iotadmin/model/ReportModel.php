<?php
namespace app\iotadmin\model;

use think\Db;

/**
 * 数据统计和数据分析用的
 *
 * @author dmakecn@163.com
 * @since 2024-03-02
 */
class ReportModel {

	/**
	 * 要考虑两个用例：分页查询和图标连续点位
	 *
	 * @param array $input 现在没有用，以后可以拿来做统计日期条件
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2023-12-06
	 */
	public function initialize($input = []) {
		if ($input) {
			switch ($input['act']) {
			// 数据统计：各区域项目数量
			case "area_projects":
				return self::statAreaProjects($input);
			// 数据统计：各项目的网关数量
			case "project_gateways":
				return self::statProjectG($input);
			// 数据统计：各项目的设备数量
			case "project_devices":
				return self::statProjectD($input);
			// 数据统计：各业务区的网关数量
			case "area_gateways":
				return self::statAreaGateways($input);
			// 数据统计：各业务区的设备数量
			case "area_devices":
				return self::statAreaDevice($input);
			// 数据统计：网关总数，在线网关数，离线网关数，屏蔽网关数
			case "gateway_count":
				return self::statGateways($input);
			// 数据统计：设备总数，在线设备数，离线设备书，屏蔽设备数
			case "device_count":
				return self::statDevices($input);

			// 业务的告警统计
			case "alarm_area":
				return self::statAlarmArea($input);
			// 项目的告警统计
			case "alarm_project":
				return self::statAlarmProject($input);
			// 子系统的告警统计
			case "alarm_device":
				return self::statAlarmDevice($input);
			// 设备类型统计
			case "dev_type_num":
				return self::statDeviceType($input);
			case "area_alarm_list":
				return self::listAreaAlarm($input);
			}
		}
	}

	private function statDevices($input) {
		$sql = "select count(1) as total,
        sum(case when status=0 then 1 else 0 end)  as offline,
        sum(case when status=1 then 1 else 0 end)  as online,
        sum(case when shield=1 then 1 else 0 end)  as shieldnum
        from iot_device";

		$data = Db::query($sql);

		return getJsonCode($data);
	}

	private function statGateways($input) {
		$sql = "select count(1) as total,
        sum(case when status=0 then 1 else 0 end)  as offline,
        sum(case when status=1 then 1 else 0 end)  as online,
        sum(case when shield=1 then 1 else 0 end)  as shieldnum
        from iot_gateway";

		$data = Db::query($sql);

		return getJsonCode($data);
	}

	private function statAreaDevice($input) {
		$sql = "select
        case when dev_type_id=100 then '土壤'  when dev_type_id=101 then '水情' when dev_type_id=102 then '气体' when dev_type_id=103 then '电气' when dev_type_id=104 then '火灾' end as '设备类型',
        d.area_id,area_name,dev_type_id,count(dev_id) as dev_num from iot_device d left join iot_area a on a.area_id=d.area_id GROUP BY d.area_id,d.dev_type_id";

		$data = Db::query($sql);

		return getJsonCode($data);
	}

	private function statAreaGateways($input) {
		$sql = "select g.area_id,area_name,count(gateway_id) as gateway_num from iot_gateway g left join iot_area a on a.area_id=g.area_id GROUP BY g.area_id";

		$data = Db::query($sql);

		return getJsonCode($data);
	}

	private function statAreaProjects($input) {
		$sql = "select g.area_id,area_name,count(g.project_id) as project_num from iot_gateway g left join iot_area a on a.area_id=g.area_id GROUP BY g.area_id";

		$data = Db::query($sql);

		return getJsonCode($data);
	}

	private function statProjectG($input) {
		$sql = "select p.project_id,p.name as project_name,p.company,count(g.gateway_id) as gate_num from iot_gateway g left join project p on g.project_id=p.project_id GROUP BY g.project_id";

		$data = Db::query($sql);

		return getJsonCode($data);
	}

	private function statProjectD($input) {
		$sql = "select p.project_id,p.name as project_name,p.company,count(d.dev_id) as dev_num from iot_device d left join project p on d.project_id=p.project_id GROUP BY d.project_id";

		$data = Db::query($sql);

		return getJsonCode($data);
	}

	private function statAlarmArea($input) {
		$sql = "select a.area_id,b.area_name,count(alarm_id) as alarm_num,
        sum(case when a.status=0 then 1 else 0 end)  as '未处理',
        sum(case when a.status=1 then 1 else 0 end)  as '已处理'
        from iot_alarm a
        left join iot_area b on a.area_id=b.area_id
        group by a.area_id";

		$data = Db::query($sql);

		return getJsonCode($data);

	}

	private function statAlarmProject($input) {
		$sql = "select a.project_id,b.company,b.name,count(alarm_id) as alarm_num,
        sum(case when a.status=0 then 1 else 0 end)  as '未处理',
        sum(case when a.status=1 then 1 else 0 end)  as '已处理'
        from iot_alarm a
        left join project b on a.project_id=b.project_id
        group by a.project_id";
		$data = Db::query($sql);
		
		/*		
		$cond = [];
		if($input['start']&&$input['end']){
			$cond['a.alarm_time']=['between',[$input['start'],$input['end']]];
		}
		$data= Db::name('iot_alarm')->alias('a')
		->join('project b','a.project_id=b.project_id','left')
		->join('iot_area ar','ar.area_id=a.area_id','left')
		->where($cond)
		->field("select a.project_id,b.company,b.name,count(alarm_id) as alarm_num,ar.name as area_name
        sum(case when a.status=0 then 1 else 0 end)  as '未处理',
        sum(case when a.status=1 then 1 else 0 end)  as '已处理'")
		->select();*/

		return getJsonCode($data);

	}

	private function statAlarmDevice($input) {
		$sql = "select c.name as dev_type,count(alarm_id) as alarm_num,
        sum(case when a.status=0 then 1 else 0 end)  as '未处理',
        sum(case when a.status=1 then 1 else 0 end)  as '已处理'
        from iot_alarm a
        left join iot_device b on a.dev_id=b.dev_id
        left join iot_device_type c on b.dev_type_id=c.dev_type_id
        group by b.dev_type_id";
		$data = Db::query($sql);

		return getJsonCode($data);

	}

	private function statDeviceType($input) {
		$cond = [];

		if ($input['area_id'] > 0) {
			$cond['d.area_id'] = $input['area_id'];
		}

		$list = Db::name('iot_device')->alias('d')
			->join('iot_device_type t', 'd.dev_type_id=t.dev_type_id', 'left')
			->group('d.dev_type_id')
			->field('d.dev_type_id,count(d.dev_type_id) as dev_type_num,t.name as dev_type_name,
			sum(case when d.status=0 then 1 else 0 end) as offline_num,
			sum(case when d.status=1 then 1 else 0 end) as online_num,
			sum(case when d.shield=1 then 1 else 0 end) as shield_num')
			->where($cond)
			->select();

		return getJsonCode($list);
	}

	private function listAreaAlarm($input) {
		$cond = [];
		$areaid = $input['area_id'];
		if ($areaid > 0) {
			$cond['d.area_id'] = $areaid;
		}

		$status = $input['status'];
		if ($status > 0) {
			$cond['a.status'] = $status;
		}

		if ($input['type'] == 'all') {
			$list = Db::name('iot_alarm')->alias('a')
				->join('iot_device d', 'a.dev_id=d.dev_id', 'left')
				->join('project p', 'd.project_id=p.project_id', 'left')
				->join('iot_area ar','ar.area_id=a.area_id','left')
				->field('a.alarm_id,a.alarm_time,a.level,a.dev_id,d.name as dev_name,p.company,p.name as project_name,ar.area_name,(case when d.dev_type_id=100 then "soil" when d.dev_type_id=101 then "water" when d.dev_type_id=102 then "harmfulgas" when d.dev_type_id=103 then "electronic" else "smoke" end) as dev_type')
				->where($cond)
				->select();

			return getJsonCode($list);
		}
		if ($input['type'] == 'pages') {

			$page = $input['p_i'];
			$limit = $input['p_s'];
			$page = $page ? $page : 1;
			$limit = $limit ? $limit : 10;
			$data['list'] = Db::name('iot_alarm')->alias('a')
				->join('iot_device d', 'a.dev_id=d.dev_id', 'left')
				->join('project p', 'd.project_id=p.project_id', 'left')
				->join('iot_area ar','ar.area_id=a.area_id','left')
				->field('a.alarm_id,a.alarm_time,a.level,a.dev_id,d.name as dev_name,p.company,p.name as project_name,ar.area_name,(case when d.dev_type_id=100 then "soil" when d.dev_type_id=101 then "water" when d.dev_type_id=102 then "harmfulgas" when d.dev_type_id=103 then "electronic" else "smoke" end) as dev_type')
				->where($cond)
				->page($page, $limit)
				->select();

			$data['count'] = Db::name('iot_alarm')->alias('a')
				->join('iot_device d', 'a.dev_id=d.dev_id', 'left')
				->where($cond)
				->count();
			return getJsonCode($data);

		}

	}
}
