<?php

namespace app\iotadmin\model;

/**
 * 百度鹰眼的接口
 *
 * @author dmakecn@163.com
 * @since 2024-03-31
 */
class BTrackModel {

	private $ak = 'WAuxl7g4e6KYXZGOlZUvABmWrPdlM0k0';
	private $service_id = '239466';
	// 坐标拾取https://api.map.baidu.com/lbsapi/getpoint/index.html

	public function initialize($input = []) {
		if ($input) {
			switch ($input['act']) {
			case "track":
				$entity_name = $input['entity'];
				$start = $input['start'];
				$end = $input['end'];
				return self::getTrack($entity_name, $start, $end);

			}
		}
	}

	/**
	 * 添加一个实体
	 *
	 * @param string $entity_name
	 * @param string $entity_desc
	 * @param string $entity_order_no
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2024-03-31
	 */
	public function addEntity($entity_name = '渝DGXXX0', $entity_desc = '永丰蔬菜物流追踪车辆', $entity_order_no = '456789436456') {

		$url = 'https://yingyan.baidu.com/api/v3/entity/add';

		$param['ak'] = $this->ak;
		$param['service_id'] = $this->service_id;
		$param['entity_name'] = $entity_name;
		$param['entity_desc'] = $entity_desc;
		$param['entity_order_no'] = $entity_order_no;

		$param = http_build_query($param);
		$res = request_post($url, $param);

		return $res;

		// 将原始返回的结果打印出来
		//print("请求的原始返回结果为: $res ");

	}

	/**
	 * 实体按点上报轨迹
	 *
	 * @param string $entity_name
	 * @param string $longitude
	 * @param string $latitude
	 * @param string $loc_time
	 * @param string $coord_type_input
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2024-03-31
	 */
	public function addEntityPoint($entity_name = '渝DGXXX0', $longitude = '116.34181', $latitude = '40.07683', $loc_time = '0', $coord_type_input = 'bd09ll') {

		$url = 'https://yingyan.baidu.com/api/v3/track/addpoint';

		$param['ak'] = $this->ak;
		$param['service_id'] = $this->service_id;
		$param['entity_name'] = $entity_name;
		$param['coord_type_input'] = $coord_type_input;
		$param['loc_time'] = $loc_time == '0' ? time() . "" : $loc_time . "";
		$param['longitude'] = $longitude;
		$param['latitude'] = $latitude;

		$param = http_build_query($param);
		$res = request_post($url, $param);

		return $res;

		// 将原始返回的结果打印出来
		//print("请求的原始返回结果为: $res ");

	}

	/**
	 * 给一个鹰眼终端传一串行车轨迹
	 *
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2024-03-31
	 */
	public function addEntityPoints() {

		$url = 'https://yingyan.baidu.com/api/v3/track/addpoints';

		$param['ak'] = $this->ak;
		$param['service_id'] = $this->service_id;
		$param['point_list'] = '[{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711889778,"longitude":116.34181,"latitude":40.07683},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711889783,"longitude":116.343,"latitude":40.07683},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711889788,"longitude":116.34349,"latitude":40.07682},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711889793,"longitude":116.34364,"latitude":40.07682},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711889798,"longitude":116.34382,"latitude":40.07682},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711889803,"longitude":116.34484,"latitude":40.07681},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711889808,"longitude":116.34496,"latitude":40.0768},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711889813,"longitude":116.34534,"latitude":40.0768},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711889818,"longitude":116.34605,"latitude":40.07679},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711889823,"longitude":116.34627,"latitude":40.07679},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711889828,"longitude":116.34627,"latitude":40.07679},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711889833,"longitude":116.34626,"latitude":40.07687},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711889838,"longitude":116.34626,"latitude":40.07687},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711889843,"longitude":116.34626,"latitude":40.07715},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711889848,"longitude":116.34626,"latitude":40.07721},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711889853,"longitude":116.34627,"latitude":40.0775},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711889858,"longitude":116.34627,"latitude":40.07766},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711889863,"longitude":116.34627,"latitude":40.07786},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711889868,"longitude":116.34627,"latitude":40.07795},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711889873,"longitude":116.3463,"latitude":40.07837},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711889878,"longitude":116.34634,"latitude":40.07848},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711889883,"longitude":116.34635,"latitude":40.07889},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711889888,"longitude":116.34635,"latitude":40.0791},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711889893,"longitude":116.34637,"latitude":40.07971},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711889898,"longitude":116.34638,"latitude":40.08037},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711889903,"longitude":116.34639,"latitude":40.08119},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711889908,"longitude":116.34639,"latitude":40.08125},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711889913,"longitude":116.34639,"latitude":40.08125},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711889918,"longitude":116.34665,"latitude":40.08125},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711889923,"longitude":116.3481,"latitude":40.08124},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711889928,"longitude":116.3481,"latitude":40.08124},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711889933,"longitude":116.34839,"latitude":40.08124},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711889938,"longitude":116.34847,"latitude":40.08124},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711889943,"longitude":116.34927,"latitude":40.08123},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711889948,"longitude":116.35036,"latitude":40.08123},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711889953,"longitude":116.35127,"latitude":40.08123},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711889958,"longitude":116.35127,"latitude":40.08123},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711889963,"longitude":116.35135,"latitude":40.08115},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711889968,"longitude":116.35146,"latitude":40.08104},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711889973,"longitude":116.35146,"latitude":40.08043},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711889978,"longitude":116.35146,"latitude":40.08036},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711889983,"longitude":116.35146,"latitude":40.08015},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711889988,"longitude":116.35146,"latitude":40.08004},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711889993,"longitude":116.35145,"latitude":40.07956},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711889998,"longitude":116.35145,"latitude":40.07919},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711890003,"longitude":116.35142,"latitude":40.07852},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711890008,"longitude":116.35141,"latitude":40.07825},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711890013,"longitude":116.35143,"latitude":40.07709},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711890018,"longitude":116.35143,"latitude":40.07706},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711890023,"longitude":116.35143,"latitude":40.07688},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711890028,"longitude":116.35143,"latitude":40.07688},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711890033,"longitude":116.35144,"latitude":40.0768},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711890038,"longitude":116.35144,"latitude":40.0768},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711890043,"longitude":116.35152,"latitude":40.0768},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711890048,"longitude":116.35163,"latitude":40.07681},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711890053,"longitude":116.35661,"latitude":40.07679},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711890058,"longitude":116.35683,"latitude":40.07679},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711890063,"longitude":116.35694,"latitude":40.07679},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711890068,"longitude":116.35695,"latitude":40.07679},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711875378,"longitude":116.34181,"latitude":40.07683},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711875383,"longitude":116.343,"latitude":40.07683},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711875388,"longitude":116.34349,"latitude":40.07682},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711875393,"longitude":116.34364,"latitude":40.07682},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711875398,"longitude":116.34382,"latitude":40.07682},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711875403,"longitude":116.34484,"latitude":40.07681},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711875408,"longitude":116.34496,"latitude":40.0768},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711875413,"longitude":116.34534,"latitude":40.0768},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711875418,"longitude":116.34605,"latitude":40.07679},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711875423,"longitude":116.34627,"latitude":40.07679},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711875428,"longitude":116.34649,"latitude":40.07679},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711875433,"longitude":116.35112,"latitude":40.0768},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711875438,"longitude":116.35144,"latitude":40.0768},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711875443,"longitude":116.35152,"latitude":40.0768},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711875448,"longitude":116.35163,"latitude":40.07681},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711875453,"longitude":116.35661,"latitude":40.07679},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711875458,"longitude":116.35683,"latitude":40.07679},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711875463,"longitude":116.35694,"latitude":40.07679},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711875468,"longitude":116.35695,"latitude":40.07679},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711875483,"longitude":116.35695,"latitude":40.07679},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711875493,"longitude":116.35695,"latitude":40.07679},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711875503,"longitude":116.35695,"latitude":40.07679},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711875513,"longitude":116.35695,"latitude":40.07679},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711875523,"longitude":116.35695,"latitude":40.07679},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711875533,"longitude":116.35695,"latitude":40.07679},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711875543,"longitude":116.35695,"latitude":40.07679},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711875553,"longitude":116.35695,"latitude":40.07679},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711875563,"longitude":116.35695,"latitude":40.07679},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711875573,"longitude":116.35695,"latitude":40.07679},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711875583,"longitude":116.35695,"latitude":40.07679},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711875593,"longitude":116.35695,"latitude":40.07679},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711875603,"longitude":116.35695,"latitude":40.07679},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711875613,"longitude":116.35695,"latitude":40.07679},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711875623,"longitude":116.35695,"latitude":40.07679},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711875633,"longitude":116.35695,"latitude":40.07679},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711875643,"longitude":116.35695,"latitude":40.07679},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711875653,"longitude":116.35695,"latitude":40.07679},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711875663,"longitude":116.35695,"latitude":40.07679},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711875673,"longitude":116.35695,"latitude":40.07679},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711875683,"longitude":116.35695,"latitude":40.07679},{"entity_name":"渝DGXXX0","coord_type_input":"bd09ll","loc_time":1711875693,"longitude":116.35695,"latitude":40.07679}]';

		$param = http_build_query($param);
		$res = request_post($url, $param);

		return $res;

		// 将原始返回的结果打印出来
		//print("请求的原始返回结果为: $res ");
	}

	/**
	 * 查询某实体 时间段的轨迹
	 *
	 * @param string $entity_name
	 * @param string $start
	 * @param string $end
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2024-03-31
	 */
	private function getTrack($entity_name = '渝DGXXX0', $start = '1711875693', $end = '1711889778') {
		$url = 'https://yingyan.baidu.com/api/v3/track/gettrack';

		// 此处设置过滤条件
		$param['ak'] = $this->ak;
		$param['service_id'] = $this->service_id;
		$param['entity_name'] = $entity_name;
		$param['start_time'] = $start;
		$param['end_time'] = $end;

		$res = request_get($url, $param);

		return $res;

		// 将原始返回的结果打印出来
		//print("请求的原始返回结果为: $res ");
	}
}