<?php
namespace app\iotadmin\model;
use think\Db;

class GTrackModel {

	private $ak = 'f60573c08411405eeed753bcf88a5cb7';
	private $service_id = '1024196';
	// 坐标拾取 https://developer.amap.com/tools/picker

	public function initialize($input = []) {
		if ($input) {
			switch ($input['act']) {
			case "track":
				$tid = $input['tid'];
				$trid = $input['trid'];
				$start = $input['start'];
				$end = $input['end'];
				return self::getTrack($tid, $trid, $start, $end);
			case "trackdevice":
				$entity = $input['entity'];
				$start = $input['start'];
				$end = $input['end'];
				return self::getDeviceTrack($entity, $start, $end);

			}
		}
	}

	/**
	 * step1 创建服务，理论上1个业务只使用1次，作为综合平台可以给不同业务分配不同的service
	 *
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2024-04-01
	 */
	public function addService($name,$desc){
	    $url='https://tsapi.amap.com/v1/track/service/add';
		$param['key'] = $this->ak;
		$param['name'] = $name;
		$param['desc'] = $desc;
		$param = http_build_query($param);
		$res = request_post($url, $param);
		return $res;
	    //{"errcode":10000,"errmsg":"OK","data":{"name":"物流追踪服务","sid":1024196}}
	}



	/**
	 * 删除服务，极少使用
	 *
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2024-04-01
	 */
	public function delService($sid){
	    $url='https://tsapi.amap.com/v1/track/service/delete';
		$param['key'] = $this->ak;
		$param['sid'] = $sid;
		$param = http_build_query($param);
		$res = request_post($url, $param);
		return $res;
	    //{"errcode":10000,"errmsg":"OK","data":{"name":"物流追踪服务","sid":1024196}}
	}


	/**
	 * 修改服务名称，极少使用
	 *
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2024-04-01
	 */
	public function editService($sid,$name,$desc=''){
	    $url='https://tsapi.amap.com/v1/track/service/update';
		$param['key'] = $this->ak;
		$param['sid'] = $sid;
		$param['name'] = $name;
		$param['desc'] = $desc;
		$param = http_build_query($param);
		$res = request_post($url, $param);
		return $res;
	    //{"errcode":10000,"errmsg":"OK","data":{"name":"物流追踪服务","sid":1024196}}
	}

	/**
	 * step2 添加一个实体
	 *
	 * @param string $entity_name
	 * @param string $entity_desc
	 * @param string $entity_order_no
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2024-03-31
	 */
	public function addEntity($entity_name = '渝DGXXX0', $entity_desc = '永丰蔬菜物流追踪车辆') {

		$url = 'https://tsapi.amap.com/v1/track/terminal/add';
		$param['key'] = $this->ak;
		$param['sid'] = $this->service_id;
		$param['name'] = $entity_name;
		$param['desc'] = $entity_desc;
		$param = http_build_query($param);
		$res = request_post($url, $param);
		return $res;
		//返回 {"errcode":10000,"errmsg":"OK","data":{"name":"渝DGXXX0","tid":870564511,"sid":1024196}}

	}

    /**
    *  当设备信息删除时就要删除高德上面的设备对应
    *  实际情况是，需要永久保留，除非存在设备上限时才删除。
    * @param int $entity_tid
    */
    public function delEntity($entity_tid){
        $url = 'https://tsapi.amap.com/v1/track/terminal/delete';
		$param['key'] = $this->ak;
		$param['sid'] = $this->service_id;
		$param['tid'] = $entity_tid;
		$param = http_build_query($param);
		$res = request_post($url, $param);
		return $res;
    }


    /**
    *  【常用】当设备名称修改时，同步修改猎鹰上面的设备信息
    * @param int $entity_tid
    * @param int $entity_name
    * @param int $entity_desc
    */
    public function editEntity($entity_tid,$entity_name,$entity_desc){
        $url = 'https://tsapi.amap.com/v1/track/terminal/update';
		$param['key'] = $this->ak;
		$param['sid'] = $this->service_id;
		$param['tid'] = $entity_tid;
		$param['name'] = $entity_name;
		$param['desc'] = $entity_desc;
		$param = http_build_query($param);
		$res = request_post($url, $param);
		return $res;
    }


    /**
    *  【常用】查询猎鹰上面的设备终端，和本地信息相匹配校验，看是否存在命名不清晰的
    */
    public function delEntity(){
        $url = 'https://tsapi.amap.com/v1/track/terminal/list';
		$param['key'] = $this->ak;
		$param['sid'] = $this->service_id;
		$param = http_build_query($param);
		$res = request_get($url, $param);
		return $res;
    }
    

	/**
	 * step3 给实体创建一条轨迹任务，需要给iot_gtrack_device添加一个对应的log
	 *
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2024-04-01
	 */
	public function addEntityTrace($tid = 870564511, $entity_name = '渝DGXXX0') {

		$url = 'https://tsapi.amap.com/v1/track/trace/add';
		$param['key'] = $this->ak;
		$param['sid'] = $this->service_id;
		$param['tid'] = $tid;
		$param['trname'] = $entity_name . '的行车轨迹';
		$param = http_build_query($param);
		$res = request_post($url, $param);
		return $res;
		//返回 {"errcode":10000,"errmsg":"OK","data":{"trname":"渝DGXXX0的行车轨迹","trid":20}}
	}

	/**
	 * step4 实体按点上报轨迹
	 *
	 * @param string $entity_name
	 * @param string $longitude
	 * @param string $latitude
	 * @param string $loc_time
	 * @param string $coord_type_input
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2024-03-31
	 */
	public function addEntityPoint($tid = 870564511, $location = '106.050455,29.883583', $loc_time = '0', $trid = 20) {

		$url = 'https://tsapi.amap.com/v1/track/point/upload';

		$param['key'] = $this->ak;
		$param['sid'] = $this->service_id;
		$param['tid'] = $tid;
		$param['trid'] = $trid;
		$param['points'] = '[{"location":"' . $location . '","locatetime":' . $loc_time * 1000 . '}]';

		$param = http_build_query($param);
		$res = request_post($url, $param);

		return $res;

		// {"errcode":20100,"errmsg":"OK","errdetail":"PARTIAL_SUCCESS","data":{"errorpoints":[{"_param_err_info":"locatetime","_err_point_index":0,"location":"106.050455,29.883583","locatetime":1711888578}]}}

	}

	/**
	 * step5 查询某实体 时间段的轨迹
	 *
	 * @param string $entity_name
	 * @param string $start
	 * @param string $end
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2024-03-31
	 */
	private function getTrack($tid = 870564511, $trid = 20, $start = '1711875693', $end = '1711889778') {
		$url = 'https://tsapi.amap.com/v1/track/terminal/trsearch';

		// 此处设置过滤条件
		$param['key'] = $this->ak;
		$param['sid'] = $this->service_id;
		$param['tid'] = $tid;
		$param['trid'] = $trid;
		$param['starttime'] = $start * 1000;
		$param['endtime'] = $end * 1000;
		$res = request_get($url, $param);
		return $res;
	}

	/**
	 * 按GPS设备来寻找他的轨迹
	 *
	 * @param string $entity
	 * @param string $start
	 * @param string $end
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2024-04-01
	 */
	private function getDeviceTrack($entity = '渝DGXXX0', $start = '1711875693', $end = '1711889778') {
		$url = 'https://tsapi.amap.com/v1/track/terminal/trsearch';

		$device = Db::name('iot_gtrack_device')
			->where('entity_name', $entity)
			->where('create_time', 'between', [$start, $end])
			->find();

		// 此处设置过滤条件
		$param['key'] = $this->ak;
		$param['sid'] = $device['gtrack_sid'];
		$param['tid'] = $device['gtrack_tid'];
		$param['trid'] = $device['gtrack_trid'];
		$param['starttime'] = $start * 1000;
		$param['endtime'] = $end * 1000;
		$res = request_get($url, $param);
		return $res;
	}
}