<?php
namespace app\iotadmin\model;
use think\Db;
use Yurun\TDEngine\ClientConfig;
use Yurun\TDEngine\TDEngineManager;

/**
 * 输出时序数据
 *
 * @author dmakecn@163.com
 * @since 2023-12-02
 */
class IotOutputModel {

	/**
	 * 要考虑两个用例：分页查询和图标连续点位
	 *
	 * @param array $input
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2023-12-06
	 */
	public function initialize($input = []) {
		if ($input) {
			switch ($input['act']) {
			case "smoke":
				return self::getSmokeList($input);
			case "transport":
				return self::sensorDataInit('iot_data_transport', $input);
			case "soil":
				return self::sensorDataInit('iot_data_soil', $input);
			case "water":
				return self::sensorDataInit('iot_data_water', $input);
			case "electronic":
				return self::sensorDataInit('iot_data_electronic', $input);
			case "harmfulgas":
				return self::sensorDataInit('iot_data_harmfulgas', $input);
			case "devalarm":
				return self::getDevAlarm($input);

			}
		}
	}

	/**
	 * 获取烟感数据示例，用于在方法内处理逻辑事务
	 * 注意！！！没得逻辑事务的时候不要这么写浪费空间纯粹为了增加代码量
	 *
	 * @param [type] $input
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2023-12-01
	 */
	private function getSmokeList($input) {
		return self::sensorDataInit('iot_data_sensor_smoke', $input);
	}

	/**
	 * tdengine的查询工具
	 * 注意注意：这个东西是给admin用的，因为时序库主要是在图表中体现点位
	 * 所以原生的将分类、数据点分离是正确的，不用加工
	 *
	 * @param string $sql
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2023-12-01
	 */
	private function tdconnector($sql = '', $datatype = 'lines') {
		// 增加名称为 test 的连接配置
		TDEngineManager::setClientConfig('test', new ClientConfig(config('tdengine')));
		// 设置默认数据库为test
		TDEngineManager::setDefaultClientName('test');
		// 获取客户端对象（\Yurun\TDEngine\Client）
		$client = TDEngineManager::getClient();
		// 执行查询
		$result = $client->sql($sql);

		switch ($datatype) {
		case "lines":
			// 获取接口原始返回数据
			return $result->getResponse();
		case "pages":
			// 获取分页数据中的数据段
			$keys = [];
			foreach ($result->getColumns() as $column) {
				$k = $column->getName();
				array_push($keys, $k);
			}

			// 获得数据集,组装成标准json格式
			$list = [];
			foreach ($result->getData() as $row) {
				$item = [];
				foreach ($keys as $kk) {
					$v = $row[$kk];
					if ($kk == 'ts') {
						$v = date("Y-m-d H:i:s", strtotime($v));
					}
					$item[$kk] = $v;
				}
				array_push($list, $item);
			}

			return $list;
		case "count":
			// 获取分页数据总数
			$result->getResponse();
			foreach ($result->getData() as $row) {
				return $row['total'];
			}
		default:
			return $result->getResponse();
		}
	}

	/**
	 * 时序库的2种基础用法：分页、图表折线
	 *
	 * @param [type] $table
	 * @param [type] $input
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2023-12-06
	 */
	private function sensorDataInit($table, $input) {
		$sql = '';
		$data = [];
		$cond = getCond(urldecode($input['c_d']));

		switch (phpnullck($input, 'datatype', 'pages')) {
		case "pages":
			//分页类型的查询
			$page = $cond['p_i'];
			$limit = $cond['p_s'];
			$where = 'where 1=1';
			if ($cond && $cond != []) {
				foreach ($cond as $k => $v) {
					// 1. 排除
					if (in_array($k, ['datatype', 'p_i', 'p_s'])) {
						continue;
					}

					//1.1 当dev_id==0时就查全部
					if ($k == 'dev_id' && $v == 0) {
						continue;
					}

					// 2. 拼接where
					if ($k == 'start') {
						$where .= " and (ts between '" . date('Y-m-d H:i:s', $v) . "' and ";
					} else if ($k == 'end') {
						$where .= " '" . date('Y-m-d H:i:s', $v) . "' ) ";
					} else {
						$where .= " and $k='$v' ";
					}
				}
			}

			// 3. 构造sql
			$page = $page ? $page : 1;
			$limit = $limit ? $limit : 10;
			$pagefrom = $limit * ($page - 1);
			$sqllist = "select * from $table $where order by ts desc limit $pagefrom,$limit";
			$sqltotal = "select count(1) as total from $table $where";
			// 4. 返回结果
			$data['list'] = self::tdconnector($sqllist, 'pages');
			$data['count'] = self::tdconnector($sqltotal, 'count');
			$data['page'] = $page;
			break;
		case "lines":
			//组装数据图表类型
			$start = $cond['start'];
			$end = $cond['end'];
			$sql = "select * from $table where ts between '" . date('Y-m-d H:i:s', $start) . "' and '" . date('Y-m-d H:i:s', $end) . "'";
			$data = self::tdconnector($sql, 'lines');
			break;
		}

		return getJsonCode($data);
	}

	private function getDevAlarm($input) {
		$dev_id = $input['dev_id'];
		$alarm_id = $input['alarm_id'];
		$alarm_time = $input['alarm_time'];
		$alarm_star = $alarm_time - 1800;
		$alarm_end = $alarm_time + 1800;

		$dev = Db::name('iot_device')->where('dev_id', $dev_id)->find();
		$dev_type = $dev['dev_type_id'];
		$input['datatype'] = 'lines';
		$input['c_d'] = "start*$alarm_star,end*$alarm_end";

		switch ($dev_type) {
		case "100":
			return self::sensorDataInit('iot_data_sensor_soil', $input);
		case "101":
			return self::sensorDataInit('iot_data_sensor_water', $input);
		case "104":
			return self::sensorDataInit('iot_data_sensor_transport', $input);
		case "103":
			return self::sensorDataInit('iot_data_sensor_electronic', $input);
		case "102":
			return self::sensorDataInit('iot_data_sensor_harmfulgas', $input);
		case "105":
			return self::sensorDataInit('iot_data_sensor_smoke', $input);
		}
	}

	/**
	 * 查询表的数据量
	 *
	 * @param [type] $start
	 * @param [type] $end
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2024-03-31
	 */
	public function statDataCount($start, $end, $table = '') {
		return 10002222;
	}

}