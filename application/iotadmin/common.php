<?php
use think\Db;

/**
 * 指定iot缓存分组：项目、地图、设备、网关、区域
 */
define('IOT_C_P', 'CACHE_IOT_PROJECT_');
define('IOT_C_M', 'CACHE_IOT_MAP_');
define('IOT_C_D', 'CACHE_IOT_DEVICE_');
define('IOT_C_G', 'CACHE_IOT_GATEWAY_');
define('IOT_C_A', 'CACHE_IOT_AREA_');

/**
 * 将字符解析成数组
 * @param $str
 */
function parseParams($str) {
	$arrParams = [];
	parse_str(html_entity_decode(urldecode($str)), $arrParams);
	return $arrParams;
}

/**
 * 记录日志
 * @param  [type] $uid         [用户id]
 * @param  [type] $username    [用户名]
 * @param  [type] $description [描述]
 * @param  [type] $status      [状态]
 * @return [type]              [description]
 */
function writelog($uid, $username, $description, $status) {

	$data['admin_id'] = $uid;
	$data['admin_name'] = $username;
	$data['description'] = $description;
	$data['status'] = $status;
	$data['ip'] = request()->ip();
	$data['add_time'] = time();
	$log = Db::name('sys_log')->insert($data);
}

/**
 * 整理菜单树方法,如果需要排除就在此功能中实施
 * @param $param
 * @return array
 */
function prepareMenu($param) {
	$parent = []; //父类
	$child = []; //子类
	
	$project_map=config('project_map');
	//如果不启用高德或者百度地图，那么该项目的admin就不要展现相关功能
	$project_mult=config('project_mult');
	//如果不启用高德或者百度地图，那么该项目的admin就不要展现相关功能

	foreach ($param as $key => $vo) {

		if(!$project_mult&&$vo['title']=='项目管理'){		
			continue;
		}

		if(!$project_map&&$vo['title']=='地图管理'){		
			continue;
		}

		if ($vo['pid'] == 0) {
			$vo['href'] = '#';
			$parent[] = $vo;
		} else {
			$vo['href'] = url($vo['name']); //跳转地址
			$child[] = $vo;
		}
	}

	foreach ($parent as $key => $vo) {
		foreach ($child as $k => $v) {
			if ($v['pid'] == $vo['id']) {
				$parent[$key]['child'][] = $v;
			}
		}
	}
	unset($child);
	return $parent;
}

/**
 * 给echart生成符合规范的数据数组
 * $data 数据源
 * $showcolumn 数据字段
 * $type 是否需要encode,0或者1都可以
 */
function echartInit($data, $showcolumn, $type = '') {
	if ($data == null) {
		return [];
	}
	if (count($data) == 0 || $data == []) {
		return [];
	}

	$colunm = [];

	foreach ($data as $d) {
		if (array_key_exists($showcolumn, $d)) {
			array_push($colunm, $d[$showcolumn]);
		}

	}

	if ($type != '') {
		return $colunm;
	}

	return json_encode($colunm);
}