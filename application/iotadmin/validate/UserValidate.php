<?php

namespace app\iotadmin\validate;

use think\Validate;

class UserValidate extends Validate
{
    protected $rule = [
        ['username', 'unique:Sys_admin', '管理员已经存在']
    ];

}