<?php

namespace app\admin\validate;

use think\Validate;

class AdPositionValidate extends Validate
{
    protected $rule = [
       'Name|广告位名称'  => 'require',
       'PKey|广告位代码'  	  => 'require',
    ];

}