<?php

namespace app\admin\validate;
use think\Validate;

class MemberGroupValidate extends Validate
{
    protected $rule = [
        ['groupname', 'unique:customer_level', '会员组已经存在']
    ];

}