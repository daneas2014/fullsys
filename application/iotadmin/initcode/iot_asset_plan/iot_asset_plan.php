<?php
namespace app\admin\controller;

use think\Db;

class iot_asset_plan extends Base
{

    public function index()
    {
        $db = Db::name('iot_asset_plan');
        
        $map=[];

        
			$p_key=input("param.key")?input("param.key"):"";
			if($p_key&&$p_key!="")
			{
				$map[key]=["like","%$p_key%"];
			}
		
				$p_dev_id=input("param.dev_id")?input("param.dev_id"):"";
                if($p_dev_id&&$p_dev_id!=""){
                    $map["dev_id"]=$p_dev_id;
                }
				
        
        return parent::vueQuerySingle($db,$map);
    }

    public function save()
    {
        $db = Db::name('iot_asset_plan');
        $data = input('param.');
        return parent::singleDataSave($db, $data);
    }
}