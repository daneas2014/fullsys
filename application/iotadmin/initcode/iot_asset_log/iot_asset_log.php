<?php
namespace app\admin\controller;

use think\Db;

class iot_asset_log extends Base
{

    public function index()
    {
        $db = Db::name('iot_asset_log');
        
        $map=[];

        
			$p_key=input("param.key")?input("param.key"):"";
			if($p_key&&$p_key!="")
			{
				$map[key]=["like","%$p_key%"];
			}
		
				$p_=input("param.")?input("param."):"";
                if($p_&&$p_!=""){
                    $map[""]=$p_;
                }
				
        
        return parent::vueQuerySingle($db,$map);
    }

    public function save()
    {
        $db = Db::name('iot_asset_log');
        $data = input('param.');
        return parent::singleDataSave($db, $data);
    }
}