<?php

/**
 * 格式化字节大小
 * @param  number $size      字节数
 * @param  string $delimiter 数字和单位分隔符
 * @return string            格式化后的带单位的大小
 */
function format_kb($size, $delimiter = '')
{
    $units = ['KB', 'MB', 'GB', 'TB', 'PB'];
    for ($i = 0; $size >= 1024 && $i < 5; $i++) {
        $size /= 1024;
    }

    $size = number_format($size, 2);

    return $size . $delimiter . $units[$i];
}

/**
 * 格式化字节大小
 * @param  number $size      字节数
 * @param  string $delimiter 数字和单位分隔符
 * @return string            格式化后的带单位的大小
 */
function format_bytes($size, $delimiter = '')
{
    $units = ['B', 'KB', 'MB', 'GB', 'TB', 'PB'];
    for ($i = 0; $size >= 1024 && $i < 5; $i++) {
        $size /= 1024;
    }

    $size = number_format($size, 2);

    return $size . $delimiter . $units[$i];
}

/*
 * 获取当月有多少天
 * @param date $unix
 * return int
 */
function getDaysByMonth($year, $month)
{
    if (in_array($month, array(1, 3, 5, 7, 8, 10, 12))) {
        return 31;
    }
    if ($month == 2) {
        if ($year % 400 == 0 || ($year % 4 == 0 && $year % 100 !== 0)) {
            return 29;
        } else {
            return 28;
        }
    }
    return 30;
}

/**
 * 秒变时间，用于倒计时
 * @param unknown $num
 */
function secondFormat($num)
{
    $hour = floor($num / 3600);
    $hour = $hour == 0 ? '00' : $hour;
    $minute = floor(($num - 3600 * $hour) / 60);
    $minute = $minute < 10 ? '0' . $minute : $minute;
    $second = floor((($num - 3600 * $hour) - 60 * $minute) % 60);
    $second = $second < 10 ? "0" . $second : $second;
    echo $hour . ':' . $minute . ':' . $second;
}

/**
 * utf8字符转换成Unicode字符
 *
 * @param [type] $utf8_str
 *            Utf-8字符
 * @return [type] Unicode字符
 */
function utf8_str_to_unicode($utf8_str)
{
    $unicode = 0;
    $unicode = (ord($utf8_str[0]) & 0x1F) << 12;
    $unicode |= (ord($utf8_str[1]) & 0x3F) << 6;
    $unicode |= (ord($utf8_str[2]) & 0x3F);
    return dechex($unicode);
}

/**
 * Unicode字符转换成utf8字符
 *
 * @param [type] $unicode_str
 *            Unicode字符
 * @return [type] Utf-8字符
 */
function unicode_to_utf8($unicode_str)
{
    $utf8_str = '';
    $code = intval(hexdec($unicode_str));
    // 这里注意转换出来的code一定得是整形，这样才会正确的按位操作
    $ord_1 = decbin(0xe0 | ($code >> 12));
    $ord_2 = decbin(0x80 | (($code >> 6) & 0x3f));
    $ord_3 = decbin(0x80 | ($code & 0x3f));
    $utf8_str = chr(bindec($ord_1)) . chr(bindec($ord_2)) . chr(bindec($ord_3));
    return $utf8_str;
}

// 时间格式化1
function formatTime($time,$timestrap=1)
{
    if($timestrap==0){
        $time=strtotime($time);
    }

    $now_time = time();
    $t = $now_time - $time;
    $mon = (int) ($t / (86400 * 30));
    if ($mon >= 1) {
        return '一个月前';
    }
    $day = (int) ($t / 86400);
    if ($day >= 1) {
        return $day . '天前';
    }
    $h = (int) ($t / 3600);
    if ($h >= 1) {
        return $h . '小时前';
    }
    $min = (int) ($t / 60);
    if ($min >= 1) {
        return $min . '分钟前';
    }
    return '刚刚';
}

// 时间格式化2
function pincheTime($time)
{
    $today = strtotime(date('Y-m-d')); // 今天零点
    $here = (int) (($time - $today) / 86400);
    if ($here == 1) {
        return '明天';
    }
    if ($here == 2) {
        return '后天';
    }
    if ($here >= 3 && $here < 7) {
        return $here . '天后';
    }
    if ($here >= 7 && $here < 30) {
        return '一周后';
    }
    if ($here >= 30 && $here < 365) {
        return '一个月后';
    }
    if ($here >= 365) {
        $r = (int) ($here / 365) . '年后';
        return $r;
    }
    return '今天';
}

/**
 * 生产GUID
 */
function guid()
{
    mt_srand((double) microtime() * 10000);
    $charid = strtolower(md5(uniqid(rand(), true)));
    $hyphen = ''; // chr(45);chr(123) .
    $uuid = substr($charid, 0, 8) . $hyphen . substr($charid, 8, 4) . $hyphen . substr($charid, 12, 4) . $hyphen . substr($charid, 16, 4) . $hyphen . substr($charid, 20, 12);
    return $uuid;
}
