<?php
namespace app\api\controller;

use app\common\model\OpModel;
use think\Controller;

class Op extends Controller
{

    public function index()
    {
        if (request()->isPost()) {
            $input = input('param.');

            $m = new OpModel();

            if ($input['json'] && $input['json'] != '') {
                $data = explode('&', $input['json']);
                $postdata = [];
                foreach ($data as $one) {
                    $key = explode('=', $one)[0];
                    $postdata[$key] = explode('=', $one)[1];
                }

                $res = $m->saveOpquestionnaire($postdata['subject'], $postdata['title'], $postdata['evuid'], $postdata['tel'], $postdata['email'], $postdata['url'], $postdata['rptoken']);
                return json(['code' => 1, 'data' => '', 'msg' => '您的反馈我们已经收到，我们会立即在1个工作日内解开始决这个问题']);
            } else {
                $res = $m->saveOpquestionnaire($input['subject'], $input['title'], $input['evuid']);
                return json(['code' => 1, 'data' => '', 'msg' => '您的心愿已成功加入' . $input['subject']]);
            }
        }
    }
}
