<?php
namespace app\api\controller;

use think\Controller;
use think\Db;

/**
 * cron方法完结自动任务的执行
 *
 * @author dmakecn@163.com
 * @since 2020-01-03
 */
class Queuekill extends Controller
{

    public function index()
    {
        $db = Db::name('jobs');

        $list = $db->limit(5)
            ->where('reserved_at is null')
            ->order('id asc')
            ->select();

        if (!$list) {
            return json(['code' => 0, 'data' => 0, 'msg' => '自动任务已完结']);
        }

        foreach ($list as $l) {

            $payload = json_decode($l['payload'], true);

            $class = new \app\common\queue\QueueClient();

            $act = $l['queue'];

            $data = $payload['data'];

            $res = $class->$act($data);

            if ($res == true) {
                $db->where('id', $l['id'])->update(['reserved_at' => time()]);
            } else {
                return json(['code' => -1, 'data' => '', 'msg' => $l['id'] . '完成失败']);
            }
        }

        return json(['code' => 1, 'data' => count($list), 'msg' => '自动任务本次完成' . count($list) . '条']);
    }
}
