<?php
namespace app\api\controller;

use com\Geetestlib;
use think\Controller;

class Gee extends Controller
{

    private $CAPTCHA_ID = '0072afb91a3927dfe961405b4cfea52b';
    private $PRIVATE_KEY = '7f2def06d5ea0f08d480b06fbda8ffc5';
    public function index()
    {
        $GtSdk = new Geetestlib($this->CAPTCHA_ID, $this->PRIVATE_KEY);
        session_start();

        $data = array(
            "user_id" => "test", // 网站用户id
            "client_type" => "web", // web:电脑上的浏览器；h5:手机上的浏览器，包括移动应用内完全内置的web_view；native：通过原生SDK植入APP应用的方式
            "ip_address" => request()->ip(), // 请在此处传输用户请求验证时所携带的IP
        );

        $status = $GtSdk->pre_process($data, 1);
        $_SESSION['gtserver'] = $status;
        $_SESSION['user_id'] = $data['user_id'];
        echo $GtSdk->get_response_str();
    }

    public function verify()
    {
        session_start();
        $GtSdk = new Geetestlib($this->CAPTCHA_ID, $this->PRIVATE_KEY);

        $data = array(
            "user_id" => $_SESSION['user_id'], // 网站用户id
            "client_type" => "web", // web:电脑上的浏览器；h5:手机上的浏览器，包括移动应用内完全内置的web_view；native：通过原生SDK植入APP应用的方式
            "ip_address" => request()->ip(), // 请在此处传输用户请求验证时所携带的IP
        );

        if ($_SESSION['gtserver'] == 1) { // 服务器正常
            $result = $GtSdk->success_validate($_POST['geetest_challenge'], $_POST['geetest_validate'], $_POST['geetest_seccode'], $data);
            if ($result) {
                echo '{"status":"success"}';
            } else {
                echo '{"status":"fail"}';
            }
        } else { // 服务器宕机,走failback模式
            if ($GtSdk->fail_validate($_POST['geetest_challenge'], $_POST['geetest_validate'], $_POST['geetest_seccode'])) {
                echo '{"status":"success"}';
            } else {
                echo '{"status":"fail"}';
            }
        }
    }
}
