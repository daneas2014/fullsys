<?php
namespace app\api\controller;

class Index
{

    /**
     * 笨方法用于其他API的导流，做映射处理更方便,下面那个方法是假设
     */
    public function index()
    {
        $data = input('post.');

        if (!array_key_exists('c', $data)) {
            echo '';exit();
        }

        if (!array_key_exists('a', $data)) {
            echo '';exit();
        }

        if (!array_key_exists('t', $data)) {
            echo '';exit();
        }

        $model = $data['c'];
        $act = $data['a'];
        $rtoken = $data['t'];

        //1.判断token是否有效
        //if(strlen($rtoken)!=32){
        //    echo '';exit();
        //}

        //2.去除重复的参数
        unset($data['c']);
        unset($data['a']);
        unset($data['t']);

        $api = null;

        // 3. 总有一些例外
        if ($model == 'comments') {
            if ($act == 'mcomment' || $act == 'ncomment') {
                $data['data'] = $data['data'] . '&content=' . $data['content'];
            }
            $api = new \app\api\model\CommentsAPIModel();
        } else {
            $modelName = '\\app\\api\\model\\' . $model . 'Model';
            $api = new $modelName();
        }

        $params = explode('&', $data['data']);
        $postdata = [];

        foreach ($params as $one) {
            $key = explode('=', $one)[0];
            $postdata[$key] = explode('=', $one)[1];
        }

        try {
            return json($api->$act($postdata));
        } catch (\Exception $e) {
            return json(['code' => -1, 'data' => $e->getMessage(), 'msg' => '']);
        }

    }
}
