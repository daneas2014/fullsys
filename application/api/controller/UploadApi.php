<?php
namespace app\api\controller;

use think\Controller;
use think\Db;

class UploadApi extends Controller {

	public function upload_base64() {
		$base64 = input('img');

		$base64_image = str_replace(' ', '+', $base64);
		// post的数据里面，加号会被替换为空格，需要重新替换回来，如果不是post的数据，则注释掉这一行
		if (preg_match('/^(data:\s*image\/(\w+);base64,)/', $base64_image, $result)) {
			// 匹配成功
			if ($result[2] == 'jpeg') {
				$image_name = uniqid() . '.jpg';
				// 纯粹是看jpeg不爽才替换的
			} else {
				$image_name = uniqid() . '.' . $result[2];
			}

			$dir = "/attachment/canvs/" . date('Ym') . '/' . date('d');
			$image_file = $dir . '/' . time() . '.jpg';

			if (!is_dir(ROOT_PATH . 'web' . $dir)) {
				mkdir(ROOT_PATH . 'web' . $dir, 0755, true);
			}

			// 服务器文件存储路径
			if (file_put_contents('.' . $image_file, base64_decode(str_replace($result[1], '', $base64_image)))) {

				// 上传到云盘
				$path = self::uploadtoqiniu($image_file, $image_name);

				return $path;
			} else {
				return json([
					'code' => -1,
					'data' => '保存失败',
				]);
			}
		} else {
			return json([
				'code' => -1,
				'data' => '类型不匹配',
			]);
		}
	}

	public function upload_local() {
		$file = request()->file('file');

		$info = $file->move(ROOT_PATH . 'public' . DS . 'uploads/images');

		if ($info) {
			echo $info->getSaveName();
		} else {
			echo $file->getError();
		}
	}

	// 图片上传
	public function a_upload() {
		$act = input('param.act');

		$file = request()->file('file');

		$dir = ROOT_PATH . 'web' . DS . 'attachment/images'; // 上传到目录

		$info = $file->move($dir);

		if ($info) {

			$filename = $info->getSaveName();

			unset($info);

			$filepath = '/attachment/images/' . $filename;
			$res = $this->uploadtoqiniu($filepath, str_replace("\\", "/", $filename));
			echo $res;
		} else {
			echo $file->getError();
		}
	}

	/**
	 * 上传图片到七牛云，是否删除本地图片,上传成功返回路径，上传失败返回本地路径
	 *
	 * @param [type] 本地路径
	 * @param string 图片标题
	 * @param boolean 是否删除
	 * @return json
	 * @author dmakecn@163.com
	 * @since 2019-12-26
	 */
	public function uploadtoqiniu($src, $title = '', $delold = true) {
		if (config('saveatlocal') == 1) {
			return $src;
		}

		$fileName = substr($src, 1);
		$fileName = str_replace("\\", "/", $fileName);
		$filePath = '.' . $src;

		// 配置参数【七牛云没有文件夹概念，只有将文件命名为本地文件存储路径,$filename就是路径】

		$auth = new \Qiniu\Auth(config('cloud')['QINIU_ACCESSKEY'], config('cloud')['QINIU_SECRETKEY']);

		// 初始化上传类
		$upToken = $auth->uploadToken(config('cloud')['QINIU_BUCKET']);
		$uploadMgr = new \Qiniu\Storage\UploadManager();

		// 上传
		list($ret, $err) = $uploadMgr->putFile($upToken, $fileName, $_SERVER['DOCUMENT_ROOT'] . $src);

		// 返回全路径
		if ($err !== null) {
			return $src;
		} else {
			if ($delold) {
				unlink($filePath);
			}

			$cdnurl = config('cloud')['QINIU_DOMAIN'] . $fileName;

			$source['url'] = $cdnurl;
			$source['type'] = 'image';
			$source['title'] = $title;
			$source['create_time'] = time();
			Db::name('yun_source')->insert($source);

			return $cdnurl;
		}
	}
}
