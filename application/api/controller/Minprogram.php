<?php
namespace app\api\controller;

use EasyWeChat\Factory;
use think\Controller;

class Minprogram extends Controller
{

    public function Index()
    {
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: POST');
        header('Access-Control-Max-Age: 1000');

        if (!request()->isPost()) {
            return getJsonCode(false, 'API ERRO');
        }

        $input = input('post.');

        // 2018过年后就要开始微信、app的校验工作
        $hash = $input['hash'];
        $timestr = date('Y-m-d H:0:0', time());
        $realhas = md5(strtotime($timestr) . 'woshidashabi');

        if (strlen($hash) != 32) { // $hash != null && $hash != $realhas) {
            return getJsonCode(false, 'HASH ERRO:' . $hash);
        }

        $model = $input['ctl'];

        $api = null;

        switch ($model) {
            case "am":
                $api = new \app\api\model\ArticleModel();
                break;
            case "video":
                $api = new \app\api\model\VideoModel();
                break;
            case "pay":
                $api = new \app\api\model\PayModel();
                break;
            case "um":
                $api = new \app\api\model\UserModel();
                break;
            case "apps":
                $api = new \app\api\model\AppsModel();
                break;
            case "subscribe":
                $api = new \app\api\model\SubscribeModel();
                break;
            case "product":
                $api = new \app\api\model\ProductModel();
                break;

            case "tk":
                if (array_key_exists('pid', $input) && array_key_exists('appkey', $input) && array_key_exists('secret', $input)) {
                    $api = new \app\api\model\TaokeModel($input['appkey'], $input['secret'], $input['pid']); // 如果其他人要用自己传参数进来
                } else {
                    $api = new \app\api\model\TaokeModel(); // 自用
                }

                break;
            case "area":
                $api = new \app\api\model\AreaModel();
                break;
            case "ad":
                $ad = new \app\common\model\AdBase();
                $list = $ad->getAdByKey('minindex');
                return getJsonCode($list);

            case "wxgetsessionkey":
                $minprogram = $this->getMinConfig(array_key_exists('appid', $input) ? $input['appid'] : null);

                $api = new \app\api\model\WxSession();
                $data = $api->getSessionCode($minprogram['app_id'], $minprogram['secret'], $input['code']);
                return getJsonCode($data);

            case "wxgetphone":
                $appid = $this->getMinConfig(array_key_exists('appid', $input) ? $input['appid'] : null)['app_id'];

                $pc = new \app\api\model\WxBizDataCrypt();
                $data = $pc->decryptData($appid, input('sessionKey'), $input['ed'], $input['iv']);
                return getJsonCode($data);

            case "xcx": // 小程序功能
                $app = Factory::miniProgram($this->getMinConfig(array_key_exists('appid', $input) ? $input['appid'] : null));

                $api = new \app\api\model\XcxModel();
                return $api->initialize($app, $input);

            default:
                return getJsonCode(false, 'ERROR API');
        }

        return $api->initialize($input);
    }

    /**
     * 返回不同的小程序配置，如果做成开放平台后，需要有后台表动态读取config
     *
     * @param [type] $appid
     * @return void
     * @author dmakecn@163.com
     * @since 2020-05-06
     */
    private function getMinConfig($appid = null)
    {
        // 铜城购小程序
        if ($appid && $appid == 'wx6c309364fa3bc0b8') {
            return ['app_id' => $appid, 'secret' => 'd542f2545ab9739d2f5a76296feb4631'];
        }
        
        // 随风来
        return config('wxauth')['minprogram'];
    }
}
