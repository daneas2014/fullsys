<?php
namespace app\api\model;

use think\Model;
use EasyWeChat\Factory;
use app\pay\model\OrderModel;

class PayModel extends Model
{

    public function initialize()
    {
        $data = input('post.');
        
        switch ($data['act']) {
            case "min":
                return $this->minPay($data);
        }
    }

    public function minPay($data)
    {
        $om = new OrderModel();       
        
        $config = [
            'app_id' => 'wxxxxxxx',
            'mch_id' => 'xxx',
            'key' => '32位加密字符串',
            'notify_url' =>SITE_DOMAIN.'/pay/wenotify',
        ];
        
        $app = Factory::payment($config);
        
        $res = $app->order->unify([
            'product_id' => $order['productid'],
            'body' => $params['body'],
            'out_trade_no' => $params['out_trade_no'],
            'total_fee' => $params['total_fee'], 
            'trade_type' => 'JSAPI', 
            'openid' => $data['openid']
        ]);
        
        if(array_key_exists('return_code', $res)&&$res['return_code']=='FAIL'){
            return getJsonCode(false);
        }
                
        $prepay_id=$res['prepay_id'];
        
        $om->where('OrderNo',$params['out_trade_no'])->update(['trade_no'=>$prepay_id]);        
        
        $paysign=$app->jssdk->sdkConfig($prepay_id);
                
        return getJsonCode($paysign);
    }        
}