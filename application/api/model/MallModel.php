<?php
namespace app\api\model;

use think\Model;

/**
 * 主要是用于购物车的运作
 *
 * @author Daneas
 *        
 */
class MallModel extends Model
{

    protected $name = 'order_cart';

    /**
     * 我的购物车
     */
    public function mycart()
    {}

    public function cartprocess()
    {}
}