<?php
namespace app\api\model;

use app\common\model\CommentsBase;

class CommentsAPIModel extends CommentsBase {

	public function getPcomments($data) {
		return $this->getProductComments($data['id'], $data['p'], $data['l']);
	}

	/**
	 * 获取评论列表
	 */
	public function getcommentlist($data) {

		$result = $this->getComments($data['type'], $data['id'], $data['p'], $data['l']);

		return json($result);
	}

	/**
	 * 内容评论
	 * @param unknown $data
	 * @return number[]|string[]|\think\false[]
	 */
	public function ncomment($data) {
		$result = $this->saveComment($data['authorid'], $data['moduletype'], $data['id'], $data['content']);

		if ($result) {
			return ['code' => 1, 'data' => $result, 'msg' => '发布评论成功'];
		}

		return ['code' => -1, 'data' => $result, 'msg' => '系统异常，请稍后再试'];
	}

	/**
	 * 评论的评论
	 * @param unknown $data
	 */
	public function mcomment($data) {
		$result = $this->saveChildComment($data['authorid'], $data['id'], $data['content']);

		if ($result) {
			return ['code' => 2, 'data' => $result, 'msg' => '发布评论成功'];
		}

		return ['code' => -1, 'data' => $result, 'msg' => '系统异常，请稍后再试'];
	}

    /**
     * 给评论点赞
     *
     * @param [type] $data
     * @return void
     * @author dmakecn@163.com
     * @since 2024-02-26
     */
	public function zancomment($data) {
		$res = $this->where(['targetid' => $data['id'], 'moduletype' => $data['type']])->setInc('up');
		return ['code' => 1, 'data' => $res, 'msg' => '已点赞'];
	}
}