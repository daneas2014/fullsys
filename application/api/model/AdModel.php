<?php
namespace app\api\model;

use app\common\model\AdModel as adm;
use think\Model;

class AdModel extends Model
{

    public function initialize($input = [])
    {
        if ($input) {
            switch ($input['act']) {
                case "get":
                    return self::getAdbyKey($input['k']);
            }
        }
    }

    /**
     * 找对应的广告，要缓存
     *
     * @param unknown $key：广告的key值
     */
    private function getAdbyKey($key)
    {
        $adm = new adm();

        return $adm->getAdByKey($key);
    }
}
