<?php
namespace app\api\model;

use think\Db;

class XcxModel
{

    public function initialize($app, $input = [])
    {
        if ($input) {
            switch ($input['act']) {
                case "getqr":
                    return $this->GetPoster($app);
                case "apps":
                    $apps = Db::name('tb_nav')->where(['position' => "apps", 'status' => 1])->select();
                    return getJsonCode($apps);
                default:
                    break;
            }
        }
    }

    /**
     * 生成二维码
     *
     * @param [type] easywechatapp
     * @param integer 宽
     * @return json
     * @author dmakecn@163.com
     * @since 2019-12-13
     */
    private function GetQRcode($app, $width = 120)
    {
        $minpath = input('param.path');

        $response = $app->app_code->get($minpath, [
            'width' => $width,
            'line_color' => [
                'r' => 0,
                'g' => 0,
                'b' => 0,
            ],
        ]);

        if ($response instanceof \EasyWeChat\Kernel\Http\StreamResponse) {

            $dir = './attachment/minqrcorde';

            $name = 'appcode' . md5($minpath) . '.png';

            $file = $dir . '/' . $name;

            if (!file_exists($file)) {
                $response->saveAs($dir, $name);
            }

            $uploader = new \app\api\controller\Upload();
            $newfile = $uploader->uploadtoqiniu('/attachment/minqrcorde/' . $name);
            $data = $newfile; // 'http://www.dmake.cn/attachment/minqrcode/' . $name;

            return getJsonCode($data);
        }
    }

    private function GetPoster($app)
    {
        $poster = './attachment/images/201706/17/1497671696.jpg'; // input('param.poster');
        $qr = './static/images/kfwx.jpg'; // self::GetQRcode($app)['data'];

        $dir = './attachment/minqrcorde';

        $image = \think\Image::open($poster);
        // 给原图左上角添加透明度为50的水印并保存alpha_image.png

        $name = md5($poster . $qr) . '.png';

        $image->water($qr, \think\Image::WATER_SOUTHEAST)->save($dir . '/' . $name);

        $file = $dir . '/' . $name;

        return getJsonCode($file);
    }
}
