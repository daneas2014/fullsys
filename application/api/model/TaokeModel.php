<?php
namespace app\api\model;

use app\common\model\TaobaoBase;
use think\Db;

class TaokeModel extends TaobaoBase
{

    public function initialize($input = [])
    {
        if ($input) {
            switch ($input['act']) {
                case "index":
                    return $this->getIndex();

                case 'search':
                    return $this->findGoods($input);

                case "sales":
                    return $this->getSales($input);

                case "ticks":
                    return $this->getCateTicks($input);

                case "detail":
                    return $this->getDetail($input);

                case "ifeeds":
                    return $this->getFeeds($input);

                case "tcode":
                    return $this->getCode($input);

                case "materials":
                    $items = $this->getMaterialitems(0, 0, 0, $input['pageindex'], $input['id']);
                    return getJsonCode($items);

                case "indexnav":
                    return getJsonCode($this->getNav('index'));

                case "content":
                    $before_timestamp = $input['last_timestamp'] ? $input['last_timestamp'] : time() . '000';
                    $content = $this->Content(1, $before_timestamp);
                    return getJsonCode($content, $before_timestamp);
            }
        }
    }

    /**
     * 获得导航
     * @param unknown $name
     * @return \think\Collection|\think\db\false|PDOStatement|string
     */
    public function getNav($name)
    {
        return Db::name('tb_nav')->where('position', $name)->order('sort asc')->select();
    }

    /*
     * 加载首页广告
     * 0. 首页轮播广告、分类导航等（要借助数据库），搜索栏
     * 1. 接下来的优惠券20个（女装、食品、男装、母婴、户外、用品）
     * 2. 接下来一小时内的淘抢购20个（按照价格和热度排列）
     * 3. 其他热销商品20个（除此之外的）
     */
    public function getIndex()
    {
        $key = "MIN_TK_DATA";

        $data = dcache(CACHE_API, $key);
        $db = Db::name('tb_item');

        if ($data == null) {

            $start = date('Y-m-d H:00:00');
            $end = date('Y-m-d H:00:00', strtotime("+1 hour"));

            $data = [
                'nav' => Db::name('tb_nav')->where('position', 'index')->order('sort asc')->select(),
                'subnav' => Db::name('tb_nav')->where('position', 'indexsub')->order('sort asc')->select(),
                'scroll' => $this->getMaterialitems(0, 0, 0, 1, 28026, 6),
                'goodcoupon'=> ['banner'=>'https://www.dmake.cn/static/images/tk/goodcoupon.png?1=1','items'=>$this->getMaterialitems(0, 0, 0, 1, 3756, 6)],
                'bigcoupon'=> ['banner'=>'https://www.dmake.cn/static/images/tk/bigcoupon.png?1=1','items'=>$this->getMaterialitems(0, 0, 0, 1, 27446, 6)],
                'recomend'=> ['banner'=>'https://www.dmake.cn/static/images/tk/recommend.png?1=1','items'=>$this->getMaterialitems(0, 0, 0, 1, 31362, 6)],
                'brandsales'=> ['banner'=>'https://www.dmake.cn/static/images/tk/brandsales.png?1=1','items'=>$this->getMaterialitems(0, 0, 0, 1, 3786, 8)],
                'xuanpin' => $this->Xuanpin(),
            ];

            dcache(CACHE_API, $key, $data);
        }

        return getJsonCode($data);
    }

    /**
     * 大额券
     *
     * @param [type] $input
     * @return void
     * @description
     * @example
     * @author daneas
     * @since 2019-12-31
     */
    public function getCateTicks($input)
    {
        $fields = 'title,num_iid,goodstype,coupon_info,coupon_remain_count,coupon_start_time,coupon_total_count,pict_url,pic_url,user_type,volume,zk_final_price,quan_price,start_time,end_time,category_name';

        $cate = $input['cate'];
        $page = $input['page'];

        $cond['goodstype'] = 0;
        if ($cate && $cate !== '') {
            $cond['category_name'] = $cate;
        }

        $list = $this->distinct(true)
            ->field($fields)
            ->where($cond)
            ->limit(($page - 1) * self::pagesize, self::pagesize)
            ->order('commission_rate desc')
            ->select();

        return getJsonCode($list, 'OK');
    }

    /**
     * 聚划算淘抢购
     *
     * @param [type] $input
     * @return void
     * @description
     * @example
     * @author daneas
     * @since 2019-12-31
     */
    public function getSales($input)
    {
        $time = $input['time'];
        $page = $input['page'];
        $start = date('Y-m-d') . ' ' . $time . ':00';
        $end = date('Y-m-d 24:00:00', strtotime("5 day"));

        $list = $this->JuTqg($start, $end, $page);

        return getJsonCode($list);
    }

    /**
     * 商品详情
     *
     * @param [type] $input
     * @return void
     * @description
     * @example
     * @author daneas
     * @since 2019-12-31
     */
    public function getDetail($input)
    {
        $num_iid = $input['num_iid'];

        $detail = $this->InfoGet($num_iid);

        return getJsonCode($detail);
    }

    /**
     * 找商品，如果每月找到就去淘抢购里面去找
     * @return \think\response\Json
     */
    public function findGoods($input)
    {
        $resp = $this->getDG($input['kw'], $input['page'],20,500,$input['material_id']);

        return getJsonCode($resp);
    }

    /**
     * 获得淘口令
     *
     * @param [type] $input
     * @return void
     * @description
     * @example
     * @author daneas
     * @since 2019-12-31
     */
    public function getCode($input)
    {
        $num_iid = $input['num_iid'];

        $item = Db::name('tb_item')->where('num_iid', $num_iid)->find();

        $code = $this->Link2code($num_iid, $item);

        return getJsonCode($code);
    }

    /**
     * 获得商品清单
     *
     * @param [type] $input
     * @return void
     * @description
     * @example
     * @author daneas
     * @since 2019-12-31
     */
    public function getFeeds($input)
    {

        $num_iid = $input['num_iids'];

        $detail = $this->ItemsGet($num_iid);

        return getJsonCode($detail);
    }
}
