<?php
namespace app\api\model;

use app\common\model\VipVideoModel;
use app\index\model\SaleOnlineModel;
use think\Db;
use think\Model;

class ProductModel extends Model
{

    protected $name = 'catalogproductstat';

    public function initialize($input = [])
    {
        parent::initialize();
        if ($input) {
            switch ($input['act']) {
                case "list":
                    return $this->getproduct($input);
                case "detail":
                    return $this->getdetail($input);
                default:
                    $act = $input['act'];
                    return $this->$act();
            }
        }
    }

    /**
     * 获得要订阅的厂商和相关资料
     */
    public function subsupplier()
    {
        $supplierid = input('dataid');
        $supplier = Db::name('catalogsupplier')->where(['Id' => $supplierid, 'IsPublish' => 1, 'IsDisabled' => 0])->find();
        if (!$supplier) {
            return getJsonCode(false, "供应商不存在或已停止合作");
        }
        $articles = Db::name('Article')->alias('a')
            ->field('a.Id,Title,CreativeType,CreatedTime,SubcategoryId,ImagePath,Summary')
            ->join('(select DISTINCT(articleid) from Articlerelations where supplierid=' . $supplierid . ' group by articleid) b', 'a.Id=b.articleid', 'right')
            ->join('ArticleAttribute c', 'c.ArticleId=a.Id', 'inner')
            ->order('CreatedTime desc')
            ->limit(10)
            ->select();

        $videos = Db::name('video')->field('video.*')
            ->join('Catalogproduct', 'Catalogproduct.Id=video.productid')
            ->where('SupplierId', $supplierid)
            ->order('Id desc')
            ->limit(10)
            ->select();

        $data['supplier'] = $supplier;
        $data['articles'] = $articles;
        $data['videos'] = $videos;

        return getJsonCode($data);
    }

    public function getproduct($input)
    {

        $id = array_key_exists('id', $input) ? $input['id'] : '';
        $key = $input['key'];
        $page = $input['p_i'];
        $limit = $input['p_s'];
        $map = [];
        if ($key && $key != '') {
            $map['a.Name'] = [
                'like',
                '%' . $key . '%',
            ];
        }

        if ($id && $id > 0) {
            $map['CategoryId'] = $id;
        }
        $data = Db::name('Catalogproduct')->alias('a')
            ->field('a.Id,a.SupplierId,a.Name,a.ImagePath,b.Name as Supplier,c.dn,c.ProductId')
            ->join('Catalogsupplier b', 'a.SupplierId=b.Id', 'left')
            ->join('(select count(1)as dn,ProductId from CatalogDownloadRecord group by ProductId) c', 'a.Id=c.ProductId', 'left')
            ->where($map)
            ->where('a.IsDisabled=0')
            ->where('a.IsPublish=1')
            ->page($page, $limit)
            ->order('dn desc')
            ->select();

        return getJsonCode($data);

    }

    public function getdetail($input)
    {
        // $pm = new app\index\model\ProductModel();

        $id = $input['id'];

        if ($id < 1) {
            return getJsonCode(-1);
        }

        $product = Db::name('catalogproduct')->where('Id', $id)->find();

        if ($product == null) {
            return null;
        }

        $catagory = Db::name('catalogcategory')->field('Name')->where(['Id' => $product['CategoryId'], 'ParentId' => ['>', 0]])->find();

        $supplier = Db::name('Catalogsupplier')->field('Name')->where('Id', $product['SupplierId'])->find();
        $catalogproduct = Db::name('Catalogproduct_catalogproduct')
            ->alias('a')
            ->field('a.*,b.Name')
            ->join('Catalogproduct b', 'a.ParentProductId=b.Id', 'left')
            ->where('ProductId', $id)
            ->select();

        $vm = new VipVideoModel();

        $edu = $vm->order('id desc')
            ->limit(4)
            ->select();

        $product['Edu'] = $edu;
        $product['catagory'] = $catagory;
        $product['Supplier'] = $supplier;
        $product['CatalogProduct'] = $catalogproduct;

        $data = ['product' => $product];

        return json(['code' => 1, 'data' => $data, 'msg' => '']);

    }

    public function productattr($data)
    {
        $id = $data['id'];

        $type = $data['type'];

        $rownum = $this->where('Id', $id)->setInc($type);

        if (strtolower($type) == 'followcount' && array_key_exists('evuid', $data)) {
            $pre['CustomerId'] = $data['evuid'];
            $pre['ObjectId'] = $id;
            $pre['ObjectUrl'] = '/product/' . $id;
            $pre['ObjectName'] = '';
            $pre['PreferenceType'] = 'p';
            $pre['IsSubscribe'] = 1;
            $pre['CreatedTime'] = date('Y-m-d');
            $pre['AccountType'] = 'customer';
            Db::name('customerpreference')->insert($pre);
        }

        return json(['code' => $rownum, 'data' => '', 'msg' => '']);
    }

    /**
     * 异步获取相关的产品的在线销售
     * @param unknown $data
     */

    public function rosales($data)
    {

        $productid = $data['pid'];

        $categoryid = $data['cid'];

        $name = 'APIPRsoS' . $productid . '_' . $categoryid;

        $list = dcache(CACHE_API, $name);

        if ($list) {
            return ['code' => 1, 'data' => $list, 'msg' => ''];
            $sm = new SaleOnlineModel();

            $list = $sm->Recommends($productid, $categoryid);

            dcache($name, $list);

            return ['code' => 1, 'data' => $list, 'msg' => ''];
        }
    }
}
