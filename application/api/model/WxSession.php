<?php
namespace app\api\model;

class WxSession
{

    public function getSessionCode($appid, $secret, $code)
    {
        $api = 'https://api.weixin.qq.com/sns/jscode2session';
        $api .= '?appid=' . $appid;
        $api .= '&secret=' . $secret;
        $api .= '&js_code=' . $code;
        $api .= '&grant_type=authorization_code';
        $token = file_get_contents($api);
        return json_decode($token, true);
    }
}