<?php
namespace app\api\model;

use think\Model;

class FontawesomeModel extends Model
{
    protected $name = 'op_fontawesomev';

    /**
     * 获得fontawesom列表
     *
     * @param [type] $data
     * @return void
     * @author dmakecn@163.com
     * @since 2020-01-14
     */
    function list($data) {

        $fa = $data['fa'];
        $category = $data['category'];

        $map['type'] = 'free';

        if ($fa == '' && $category == '') {
        } else {
            if ($fa != '') {
                $map['fa'] = ['in', $fa];
            }
            if ($category != '') {
                $map['category'] = ['in', $category];
            }
        }

        $data['list'] = $this->where($map)->select();
        $data['count'] = $this->where($map)->count();

        return $data;
    }
}
