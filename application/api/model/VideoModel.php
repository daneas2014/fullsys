<?php
namespace app\api\model;

use app\common\model\VipVideoModel;
use Qiniu\Auth;
use think\Db;
use think\Model;

class VideoModel extends Model
{

    protected $name = 'video';
    private $accessKey = 'SSvxZz8hqjWq-9lLOKYcp6st2hIUobvVJ3HbML0b';
    private $secretKey = 'AGHW4jtuefMbPRCBqo7LuAkA4234aMPCfrE-eyqM';
    private $bucket = 'video';

    public function initialize($input = [])
    {
        if ($input) {
            switch ($input['act']) {

                case "vipvideo":
                    return $this->getvipvideo($input);

                case "vipone":
                    return $this->getvipone($input);

                case "productvideo":
                    return $this->productvideo($input);

                case "videoview":
                    return $this->videohistory($input);

                case "one":
                    $one = Db::name('video')->where('id', $input['id'])->find();
                    Db::name('video')->where('id', $input['id'])->setInc('clickcount');
                    return json(['code' => 1, 'data' => $one, 'msg' => '']);
            }
        }
    }

    public function getvipvideo($input)
    {
        $page = $input['p_i'];
        $limit = $input['p_s'];
        $cate = $input['category'] ? $input['category'] : 0;
        $free = $input['type'] ? $input['type'] : '';
        $kw = $input['kw'] ? $input['kw'] : '';

        $page = $page ? $page : 1;
        $limit = $limit ? $limit : 10;

        $map = [];
        if ($cate > 0) {
            $map['categoryid'] = $cate;
        }
        if ($free == 'free') {
            $map['isfree'] = 1;
        }
        if ($free == 'vip') {
            $map['isfree'] = 0;
            $map['saleprice'] = ['>', 0];
        }
        if ($kw && $kw != '') {
            $map['title|keywords|description'] = ['like', '%' . $kw . '%'];
        }
        $data = Db::name('course_description')->alias('a')
            ->field('id,title,keywords,imagepath,views,duration')
            ->join("(select sum(duration)as duration,courseid from course_item group by courseid) b", "a.id=b.courseid", 'left')
            ->where($map)
            ->page($page, $limit)
            ->order('Id desc')
            ->select();

        return getJsonCode($data);
    }

    public function getvipone($input)
    {

        $vm = new VipVideoModel();

        $id = $input['id'];
        $userid = $input['customerid'] ? $input['customerid'] : 0;

        $item = $vm->getCourseItemDetail($id);

        $courseid = $input['cid'];

        $course = $vm->getCourseFullDetail($courseid);

        $authcheck = false;

        if ($course['course']['isfree'] == 1 || $course['course']['saleprice'] < 1) {
            $authcheck = true;
        } else {
            $authcheck = $vm->getUserAccess($userid, $courseid);
        }

        $playlist = Db::name('course_item')
            ->field('id,title,description,resource,imagepath,caption')
            ->where(['courseid' => $courseid, 'duration' => ['>', 0]])
            ->select();

        if ($id == 0) {
            $item = $playlist[0];
        }

        $auth = new Auth($this->accessKey, $this->secretKey);

        // 处理视频路径
        if (!strstr($item['resource'], 'http')) {
            // 构建Auth对象
            // 私有空间中的外链 http://<domain>/<file_key>
            $baseUrl = 'http://cdn.dmake.cn/' . $item['resource'];
            // 对链接进行签名
            $signedUrl = $auth->privateDownloadUrl($baseUrl);

            $item['resource'] = $signedUrl;
        }

        $data = ['item' => $item, 'course' => $course['course'], 'groups' => $course['groups'], 'items' => $course['items'], 'commentid' => $courseid, 'authcheck' => $authcheck];

        return json(['code' => 1, 'data' => $data, 'msg' => '']);

    }

    public function productvideo($input)
    {
        $page = $input['p_i'];
        $limit = $input['p_s'];
        $cate = $input['category'] ? $input['category'] : 0;
        $free = $input['type'] ? $input['type'] : '';
        $kw = $input['kw'] ? $input['kw'] : '';

        $page = $page ? $page : 1;
        $limit = $limit ? $limit : 10;

        $map = [];

        if ($free && $free > 0) {
            $map['productid'] = $free;
        }
        if ($kw && $kw != '') {
            $map['title'] = ['like', '%' . $kw . '%'];
        }

        $data = Db::name('video')->field('id,title,duration,imagepath')
            ->where($map)
            ->page($page, $limit)
            ->order('id desc')
            ->select();

        if ($cate > 0) {
            $map['categoryid'] = $cate;

            $data = Db::name('video')->alias('a')
                ->field('id,title,duration,imagepath')
                ->join('product b', 'a.productid=b.Id')
                ->where($map)
                ->page($page, $limit)
                ->order('id desc')
                ->select();
        }

        return getJsonCode($data);
    }

    public function videoattr($data)
    {

        $id = $data['id'];

        $type = $data['type'];

        $rownum = $this->where('id', $id)->setInc($type);

        return ['code' => $rownum, 'data' => '', 'msg' => ''];
    }

    /**
     * 用户观看历史
     * @param unknown $data
     * @return number[]|string[]|number[]|string[]
     */
    public function videohistory($data)
    {
        $db = Db::name('video_history');

        $history['customerid'] = $data['evuid'];
        $history['videoid'] = $data['id'];
        $history['productid'] = $data['productid'];

        if ($db->where($history)->find()) {
            $db->where($history)->update(['create_time' => time()]);
            return ['code' => 1, 'data' => '', 'msg' => ''];
        }

        $history['supplierid'] = Db::name('product')->where('id', $data['productid'])->value('supplierid');
        $history['create_time'] = time();

        $id = $db->insertGetId($history);

        return ['code' => 1, 'data' => $id, 'msg' => ''];
    }
}
