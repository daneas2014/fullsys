<?php
namespace app\api\model;

use think\Model;

class SeedModel extends Model{

    /**
     * 这个功能用于统计平台所有流量来源，用户关系，JSONP获取的
     * 功能实现：
     * 1.
     * 访问第一次前端cookie记录来源等信息，并写入统计，并计入cookie 来源url，有效期1个小时
     * 2. 页面跳转，写入访问，对比来源url,写入下游贡献率
     */
    public function seed($param)
    {
        // 这里只分析了外链来源，但是还没有和会员做统计，因此在会员系统稳定后需要做会员与外链的关系
        $url = urldecode($param['coming']);
        $type = $param['act'];
        $ip = $_SERVER['REMOTE_ADDR'] ? $_SERVER['REMOTE_ADDR'] : '127.0.0.1';
        
        $report = new \app\index\model\ReportModel();
        
        switch ($type) {
            
            case 'newcoming':
                // 1. 从连接中获取信息
                
                $info = $this->analyzeUrl($url);
                
                if ($info == null) {
                    return [
                        'code' => -1,'msg'=>'001'
                    ];
                }
                
                $data['type'] = $info['type'];
                $data['datacode'] = $info['datacode'];
                $data['dataid'] = intval($info['dataid']);
                $data['channel'] = $info['channel'];
                $data['channelname'] = $info['channelname'];
                $data['customerid'] = $param['evuid'];
                
                // 2. 以上内容有1获取解析
                
                $data['ip'] = $ip;
                $data['create_time'] = time();
                $data['checkdate'] = date('Y-m-d');
                $data['contribut'] = 1;
                $data['token'] = md5($data['ip'] . $data['type'] . $data['create_time']);
                
                $report->insertSeed($data);
                
                return [
                    'code' => 1,
                    'data' => $data['token']
                ];
            
            case 'contribut':
                
                $token = $param['token'];
                $urls = $param['vurls'];
                $uid = $param['evuid'];
                
                $report->contribut($token, $urls, $uid);
                
                return [
                    'code' => 1,'msg'=>'002'
                ];
            
            case 'live800':
                
                $report->live800($param['token']);
                
                return [
                    'code' => 1,'msg'=>'003'
                ];
            
            default:
                
                return [
                    'code' => - 1,'msg'=>'004'
                ];
        }
    }
    
    
    /**
     * 解析url
     *
     * @param unknown $url
     */
    private function analyzeUrl($url)
    {            
        try{
            $url = strtolower($url);
            
            $info['type'] = '';
            $info['datacode'] = '';
            $info['dataid'] = 0;
            $info['channel'] = '';
            $info['channelname'] = '';
            
            preg_match('/channel=([^&]*)/i', $url, $channel);
            
            if (count($channel) > 0) {
                $info['channel'] = $channel[1];
                $info['channelname'] = ['baidu' => '百度','360' => '360','share' => '社群','outlink' => '外链','adpush'=>'广告内推'][$info['channel']];
            }
            
            if (strstr($url, 'article')) {
                preg_match('/article\/([1-9]\d*)/i', $url, $article);
                $info['type'] = 'article';
                $info['dataid'] = $article[1];
                $info['datacode'] = '';
            }
            else if (strstr($url, 'product')) {               
                preg_match('/commodity\/([1-9]\d*)/i', $url, $product);
                $info['type'] = 'product';
                $info['dataid'] = $product[1];
                $info['datacode'] = '';
            }
            else if (strstr($url, 'vendor')) {               
                preg_match('/commodity/v\/([1-9]\d*)/i', $url, $vendor);
                $info['type'] = 'vendor';
                $info['dataid'] = $vendor[1];
                $info['datacode'] = '';
            }
            else if (strstr($url, 'video')) {
                preg_match('/video\/([1-9]\d*)/i', $url, $video);
                $info['type'] = 'video';
                $info['dataid'] = $video[1];
                $info['datacode'] = '';
            }
            else if (strstr($url, 'download')) {
                preg_match('/download/([1-9]\d*)/i', $url, $download);
                $info['type'] = 'download';
                $info['dataid'] = $download[1];
                $info['datacode'] = '';
            }
            else if (strstr($url, 'mall')) {
                preg_match('/mall\/d\/([^?]*)/i', $url, $mall);
                $info['type'] = 'mall';
                $info['dataid'] = $mall[1];
                $info['datacode'] = '';
            }
            else if (strstr($url, 'tag')) {
                preg_match('/tag\/([^?]*)/i', $url, $tag);
                $info['type'] = 'tag';
                $info['datacode'] = $tag[1];
                $info['dataid'] = 0;
            }
            else{
                return null;
            }
            
            return $info;
        }
        catch (\Exception $e){
            return null;
        }
    }
}