<?php
namespace app\api\model;

use think\Db;
use think\Model;

/**
 * 订阅对象处理机构
 *
 * @author dmakecn@163.com
 * @since 2020-01-03
 */
class SubscribeModel extends Model
{

    protected $name = "op_subscribe";

    public function initialize($input = [])
    {

        if ($input) {
            $a = $input['act'];

            $data = $this->$a($input);

            return $data;
        }
    }

    /**
     * 消息推送最多放7天，所以要想尽办法让人去点formsubmit获得更新
     * @param unknown $data
     * @return \think\response\Json
     */
    public function reflashformid($data)
    {
        $customerid = $data['customerid'];
        $openid = $data['openid'];
        $formid = $data['formid'];

        if (!$formid) {
            return json(['code' => -1, 'data' => '', 'msg' => '无formid']);
        }

        if ($customerid > 0) {
            Db::name('op_subscribe')->where('customerid', $customerid)->update(['form_id' => $formid]);
            return json(['code' => 1, 'data' => '', 'msg' => 'formid已更新']);
        }

        if ($openid) {
            $customerid = Db::name('Customeropenidoauth')->where(['OAuthType' => 2, 'OAuthOpenId' => $openid])->field('CustomerId');
            if ($customerid > 0) {
                Db::name('op_subscribe')->where('customerid', $customerid)->update(['form_id' => $formid]);
                return json(['code' => 1, 'data' => '', 'msg' => 'formid已更新']);
            }

            return json(['code' => -1, 'data' => '', 'msg' => '无账号关联']);
        }
    }

    /**
     * 订阅的供应商
     * @param unknown $data
     */
    public function suppliers($data)
    {

        $pagesize = 50;
        if (array_key_exists('p_s', $data)) {
            $pagesize = $data['p_s'];
        }

        $sub = Db::name('op_subscribe a')->join('catalogsupplier b', 'a.dataid=b.Id', 'inner')
            ->where('customerid', $data['customerid'])
            ->field('b.Id,Name,LogoPath,a.id as subid,customerid')
            ->select();

        $limit = $pagesize ? $pagesize : 50 - count($sub);

        $where['IsPublish'] = 1;
        $where['IsDisabled'] = 0;

        if (count($sub) > 0) {
            $where['Id'] = ['not in', columtoarray($sub, 'Id', 'int')];
        }

        $list = Db::name('catalogsupplier')->where($where)->limit($limit)->field('Id,Name,LogoPath,ClickCount')->order('ClickCount desc')->select();

        $list = array_merge($sub, $list);

        $new = [];

        foreach ($list as $l) {
            $l['LogoPath'] = $l['LogoPath'];
            array_push($new, $l);
            unset($l);
        }

        return json(['code' => 1, 'data' => $new, 'msg' => '']);
    }

    /**
     * 我的订阅
     */
    public function mysub($data)
    {
        $list = $this->where('customerid', $data['customerid'])->select();

        return json(['code' => 1, 'data' => $list, 'msg' => '']);
    }

    /**
     * 订阅
     */
    public function subscribe($data)
    {
        $count = $this->where(['customerid' => $data['customerid'], 'datatype' => $data['datatype'], 'dataid' => $data['dataid']])->count();

        if ($count > 0) {
            return json(['code' => 1, 'data' => '', 'msg' => '您已经订阅！']);
        }

        $subid = $this->insertGetId(['customerid' => $data['customerid'], 'form_id' => $data['form_id'], 'datatype' => $data['datatype'], 'dataid' => $data['dataid'], 'create_time' => time()]);

        return json(['code' => 1, 'data' => $subid, 'msg' => '订阅成功']);
    }

    /**
     * 取消订阅
     */
    public function unsubscribe($data)
    {
        $this->where('customerid', $data['customerid'])->update(['form_id' => $data['form_id']]);

        $count = $this->where(['customerid' => $data['customerid'], 'datatype' => $data['datatype'], 'id' => $data['id']])->delete();
        return json(['code' => 1, 'data' => '', 'msg' => '已取消订阅']);
    }

    /**
     * 订阅消息
     */
    public function submsglist($data)
    {
        $list = Db::name('op_subscribe_msg')->where('customerid', $data['customerid'])->limit(10)->order('id desc')->select();

        return json(['code' => 1, 'data' => $list, 'msg' => '']);
    }

    /**
     * 订阅消息详情
     */
    public function submsg($data)
    {
        $db = Db::name('op_subscribe_msg');

        $one = $db->where('id', $data['id'])->find();

        $db->where('id', $data['id'])->setInc('isview');

        return json(['code' => 1, 'data' => $one, 'msg' => '']);
    }
}
