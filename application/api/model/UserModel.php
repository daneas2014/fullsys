<?php
namespace app\api\model;

use app\ucenter\controller\Uauth;
use think\Db;
use think\Model;

class UserModel extends Model
{

    public function initialize($input = [])
    {
        if ($input) {
            switch ($input['act']) {
                case "oplogin":
                    return $this->opLogin($input);
                case "bind":
                    return $this->bindOne($input);
                case "register":
                    return $this->register($input);
                default:
                    return $this->$act();
            }
        }
    }

    /**
     * 获得所有活动券
     * @return \think\response\Json
     */
    private function coupons()
    {
        $list = Db::name('coupon')
            ->where(['type' => 'activity', 'leftnum' => ['>', 0], 'pid' => 0, 'status' => 0, 'end_time' => ['>', time()]])
            ->order('id desc')
            ->limit(2)
            ->select();

        if ($list != null && count($list) > 0) {
            return getJsonCode($list, $list[0]['create_time']);
        }

        return getJsonCode($list);
    }

    private function mycoupons()
    {
        $data = input('post.');

        $id = $data['customerid'];

        if ($id == 'undefined') {
            return getJsonCode(false);
        }

        $cm = new \app\common\model\CouponModel();

        $list = $cm->customerCoupons($id);

        if (count($list) > 0) {
            foreach ($list as $l) {
                if ($l['summary']) {
                    $l['name'] = $l['summary'];
                }
            }
        }

        return getJsonCode($list);
    }

    private function getcoupon()
    {
        $data = input('post.');

        $id = $data['customerid'];

        if ($id == 'undefined') {
            return getJsonCode(false);
        }

        $couponid = $data['couponid'];

        $cm = new \app\common\model\CouponModel();

        $res = $cm->activeCoupon($couponid, $id);

        return getJsonCode($res);
    }

    private function accountcheck()
    {
        $data = input('post.');
        $map['Mobile|Email'] = $data['account'];
        $list = Db::name('Customer')->where($map)->count();

        if ($list > 0) {
            return json(['code' => -1, 'data' => '', 'msg' => '已存在']);
        }

        return json([
            'code' => 1,
            'data' => $list,
            'msg' => '',
        ]);
    }

    /**
     * 下载记录
     */
    private function downloadrecord()
    {
        $data = input('post.');
        $map['UserId'] = $data['customerid'];
        $list = Db::name('Catalogdownloadrecord')->alias('a')
            ->join('Catalogproductdownload b', 'a.fileid=b.Id')
            ->where($map)
            ->field('a.*,b.Name')
            ->order('id desc')
            ->select();

        return json([
            'code' => 1,
            'data' => $list,
            'msg' => '',
        ]);
    }

    /**
     * 观看记录
     */
    private function myvideos()
    {

        $data = input('post.');
        $map['customerid'] = $data['customerid'];
        $map['a.productid'] = ['>', 0];
        $list = Db::name('video_history')->alias('a')
            ->join('video b', 'a.videoid=b.Id')
            ->where($map)
            ->field('a.*,b.Title,b.Image')
            ->order('id desc')
            ->select();

        return json([
            'code' => 1,
            'data' => $list,
            'msg' => '',
        ]);
    }

    private function myorders()
    {

        $data = input('post.');
        $map['AccountId'] = $data['customerid'];
        $list = Db::name('Productorder')
            ->where($map)
            ->order('Id desc')
            ->select();

        return json([
            'code' => 1,
            'data' => $list,
            'msg' => '',
        ]);
    }

    /**
     * 2021-5-18补充其他常用小程序认证方式
     * 检查openid有没有注册过，没有注册过就不管，否则就登录
     */
    private function opLogin($input)
    {
        if (array_key_exists('openid', $input) || $input['openid'] == '') {
            return json(['code' => -1, 'data' => '', 'msg' => 'empty openid']);
        }

        if (array_key_exists('apitype', $input) || $input['apitype'] == '') {
            return json(['code' => -1, 'msg' => '获取小程序类型失败']);
        }

        $apitype = 2;
        switch ($input['apitype']) {
            case 'MP-WEIXIN':$apitype = 2;
                break;
            case 'MP-BAIDU':$apitype = 3;
                break;
            case 'MP-TOUTIAO':$apitype = 4;
                break;
            case 'MP-ALIPAY':$apitype = 5;
                break;
        }

        $map['oauthtype'] = $apitype;
        $map['oauthopenid'] = $input['openid'];
        $one = Db::name('customer_openidoauth')->where($map)->find();

        if (!$one) {
            return json(['code' => -1, 'data' => '', 'msg' => '']);
        }

        $userinfo = Db::name('customer')->field('create_time,password,passwordsalt,password_update,delete_time,update_time', true)
            ->where('id', $one['customerid'])
            ->find();

        return json([
            'code' => 1, 'data' => $userinfo, 'msg' => '']);
    }

    /**
     * 如果注册过，并且没有绑定过，那就登录顺便绑定了，以后就不管了
     */
    private function bindOne($data)
    {
        $openid = $data['openid'];
        $username = $data['username'];
        $password = $data['password'];

        $uauth = new Uauth();
        $res = $uauth->dologin($data);

        if ($res['code'] == -1) {
            return json($res);
        }

        $user = session('memberinfo');

        // 第三方关系绑定
        $this->saveoauth($user['id'], $openid);

        // 如果用户没有昵称，顺便就给他取了
        if (!$user['nickname']) {
            Db::name('customerid')->where('id', $user['id'])->update([
                'nickname' => $data['nickname'],
            ]);
        }

        return json([
            'code' => 1,
            'data' => $user,
            'msg' => '',
        ]);
    }

    /**
     * 没有注册过的，直接注册并绑定openid
     */
    private function register($data)
    {
        $openid = $data['openid'];

        $uauth = new Uauth();
        $res = $uauth->doRegister($data);

        if ($res['code'] == -1 && $res['msg'] != '手机已经被人注册') {
            return json($res);
        }

        $user = Db::name('customer')->field('password', true)->where('mobile', $data['mobile'])->find();

        $this->saveoauth($user['id'], $openid);

        return getJsonCode($user, '注册成功');
    }

    private function saveoauth($uid, $openid)
    {
        $db = Db::name('customer_openidoauth');

        if ($db->where('oauthopenid', $openid)->find()) {
            return true;
        }

        $oauth['oauthtype'] = 2;
        $oauth['oauthopenid'] = $openid;
        $oauth['customerid'] = $uid;
        $oauth['accesstoken'] = '';
        $oauth['oauthstate'] = 0;

        $db->insert($oauth);
    }

    /**
     * 日历签到记录
     */
    private function calendarSignRecord()
    {
        $param = input('param.');

        $list = Db::name('op_calendarsign')
            ->where(['uid' => $param['uid'], 'year' => $param['year'], 'month' => $param['month']])
            ->field('day')
            ->select();

        return $list ? getJsonCode($list) : getJsonCode(false);
    }

    /**
     * 签到
     * @return \think\response\Json
     */
    private function calendarSign()
    {
        $param = input('post.');
        $str = substr($param['checkDate'], 0, 10);
        $date = explode('-', $str);
        $day = $date[2];
        $month = $date[1];
        $year = $date[0];

        // 这里进行逻辑操作
        $db = Db::name('op_calendarsign');
        $data = ['customerid' => $param['uid'], 'year' => $year, 'month' => $month, 'day' => $day];

        if ($db->where($data)->count() > 0) {
            return getJsonCode(false);
        }

        $data['integral'] = 1;

        // 逻辑，签到几天加倍啊？

        $data['create_time'] = strtotime(date('Y-m-d 00:00:00'));
        $id = $db->insertGetId($data);

        // 积分到账
        if ($id > 0) {
            $im = new \app\common\model\IntegralModel();
            $log = $im->integralProcess($param['uid'], $data['integral'], 'signin', '签到送积分');
        }

        $msg = "您知道吗，签到积分可以兑换优惠券和实物礼品呢！";

        return $id > 0 ? getJsonCode($msg, '签到成功') : getJsonCode($msg, '您已签到');
    }
}
