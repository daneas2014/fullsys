<?php
namespace app\api\model;

use app\common\model\ArticleBase;
use think\Db;

class ArticleModel extends ArticleBase {

	public function initialize($input = []) {
		if ($input) {
			switch ($input['act']) {
			case "list":
				return $this->getlist($input);
			case "detail":
				return $this->getdetail($input);
			case "categories":
				$categories = Db::name('article_category')->field('id,name,title')->order('sort asc')->select();
				return getJsonCode($categories);
			}
		}
	}

	public function getlist($input) {
		$key = array_key_exists('key', $input) ? $input['key'] : '';
		$page = $input['p_i'];
		$limit = $input['p_s'];
		$cond = getCond($input['c_d']);
		if ($key && $key != '') {
			$cond['a.title'] = [
				'like',
				'%' . $key . '%',
			];
		}

		$page = $page ? $page : 1;
		$limit = $limit ? $limit : 10;

		$data = Db::name('article')->alias('a')
			->field('a.id,a.title,a.creativetype,a.create_time,a.categoryid,a.imagepath,a.summary,b.clickcount,c.name as catename')
			->join('article_attribute b', 'b.articleid=a.id', 'left')
			->join('article_category c', 'a.categoryid=c.id', 'left')
			->where($cond)
			->page($page, $limit)
			->order('id desc')
			->select();

		return getJsonCode($data);
	}

	public function getdetail($input) {
		$id = $input['id'];

		if ($id < 1) {

			return getJsonCode(-1);
		}

		$data = Db::name('article')->alias('a')
			->field('a.*,b.clickcount')
			->join('article_attribute b', 'b.articleid=a.id', 'left')
			->where('id', $id)->find();

		$this->articleattr([
			'id' => $id,
			'type' => 'clickcount',
		]);

		$data["detail"] = preg_replace('/(.*?style=\\").*?(\\".*?)/i', '$1$2', $data["detail"]);

		return getJsonCode($data);
	}

	public function articleattr($data) {
		$articleid = $data['id'];

		$type = $data['type'];

		$rownum = $this->setArticleAttr($articleid, $type);

		$key = CACHE_ARTICLE . 'DETAIL_' . $articleid;

		cache($key, null);

		return [
			'code' => $rownum,
			'data' => '',
			'msg' => '',
		];
	}

    /**
     * 文章输出pdf
     *
     * @param [type] $data
     * @return void
     * @author dmakecn@163.com
     * @since 2022-06-27
     */
	public function mpdf($data) {
		$id = $data['id'];

		$article = $this->getArticle($id);

		$html = "<h1>" . $article['title'] . "</h1><br/><hr/>" . $article['detail'];

		savempdf($html, __DIR__ . '/attachment/mpdf/a/' . $id . '.pdf');
	}

}
