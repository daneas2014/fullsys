<?php
namespace app\api\model;

use think\Model;

class CouponModel extends Model{
    
    protected $name='op_coupon';
    
    public function hafeyear($data){
        
        if(array_key_exists('evuid', $data)&&$data['evuid']>0){
            $customerid = $data['evuid'];
            $couponid = $data['cid'];
            $activityid = $data['aid'];
                
            // 1. 判断是否本次活动中奖过
            $count = $this->where('activityid', $activityid)->count();
            //if($count>0){
              //  return ['code'=>-1,'data'=>'','msg'=>'您的手气已经用完啦，请明天再接再厉'];
            //}
                
            // 2. 没有中奖生成优惠券一张
            $cm = new \app\common\model\CouponModel();
            
            $res = $cm->activeCoupon($couponid, $customerid);
            
            if($res['code']==-1){
                
                return ['code'=>-1,'data'=>'','msg'=>'您的好运气已经送到个人中心啦，请查看个人中心“我的优惠券”'];
            }
            // 3. 领到券的记录
            $this->insert(['activityid'=>$activityid,'customerid'=>$customerid,'couponpid'=>$couponid ,'couponcode'=>$res['data'],'create_time'=>time()]);
            
            return $res;
        }
        
        return ['code'=>-1,'data'=>'','msg'=>'您的手气已经用完啦，请明天再接再厉'];
    }
    
    /**
     * 活动领券，看中了什么就发什么
     * @param unknown $data
     * @return number[]|string[]|number[]|string[]|\app\common\model\number[]|\app\common\model\string[]|unknown
     */
    public function activecoupon($data){
        
        if(array_key_exists('evuid', $data)&&$data['evuid']>0){
            $customerid = $data['evuid']; // 会员ID
            $couponid = $data['cid']; //活动绑定的优惠券号
            $activityid = $data['aid']; // 活动ID
            
            // 1. 判断是否本次活动中奖过
            $count = $this->where('activityid', $activityid)->count();
            //if($count>0){
            //  return ['code'=>-1,'data'=>'','msg'=>'您的手气已经用完啦，请明天再接再厉'];
            //}
            
            // 2. 没有中奖生成优惠券一张
            $cm = new \app\common\model\CouponModel();
            
            $res = $cm->activeCoupon($couponid, $customerid);
            
            if($res['code']==-1){
                
                return ['code'=>-1,'data'=>'','msg'=>'您的好运气已经送到个人中心啦，请查看个人中心“我的优惠券”'];
            }
            // 3. 领到券的记录
            $this->insert(['activityid'=>$activityid,'customerid'=>$customerid,'couponpid'=>$couponid ,'couponcode'=>$res['data'],'create_time'=>time()]);
            
            return $res;
        }
        
        return ['code'=>-1,'data'=>'','msg'=>'您的手气已经用完啦，请明天再接再厉'];
    }
}