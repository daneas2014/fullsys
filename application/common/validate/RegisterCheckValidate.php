<?php
namespace app\common\validate;

use think\Validate;

class RegisterCheckValidate extends Validate{
    
    protected $rule = [
        ['Mobile', 'require|max:11|/^1[3-8]{1}[0-9]{9}$/|unique:Customer', '手机号不能为空|手机号长度为11位|请输入正确的手机号|手机已经被人注册'],
        ['password', 'require', '密码不能为空'],
    ];
}