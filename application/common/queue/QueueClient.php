<?php
namespace app\common\queue;

use think\queue\Job;
use app\common\model\LicenseModel;

class QueueClient
{

    /**
     * 通过这个queueact指引去执行哪个方法，$data一定是数组
     * @param Job $job
     * @param unknown $data
     */
    public function listJob(Job $job, $data)
    {        
                
        debuggerfile('x','crmupdate1');
        
        $act = $data['queueact'];
        // 1. 解析传入数据
        $isJobDone = $this->$act($data);
        // 2. 如果入库成功就删除这个任务
        if ($isJobDone) {
            // 成功删除任务
            $job->delete();
        } else {
            // 任务轮询4次后删除
            if ($job->attempts() > 3) {
                // 第1种处理方式：重新发布任务,该任务延迟10秒后再执行
                // $job->release(10);
                // 第2种处理方式：原任务的基础上1分钟执行一次并增加尝试次数
                // $job->failed();
                // 第3种处理方式：删除任务
                $job->delete();
            }
        }
    }

    public function updatelicense($data)
    {        
        $licensekey = $data['LicenseKey'];
        $licensename = $data['LicenseName'];
        $listprice = $data['PublicPrice'];
        $minqty = $data['minQty'];
        $productid = $data['ProductId'];
        $atypestr = $data['AtypeStr'];
        $licenseremark = $data['LicenseRemark'];
        $activejsonstr = $data['ProductActivity'];
        $active = json_decode($activejsonstr, true);
        
        $lm = new LicenseModel();
        
        return $lm->updateLicense($licensekey, $licensename, $productid, ceil($listprice), $licenseremark, $minqty, $atypestr, $active);
    }
    
}