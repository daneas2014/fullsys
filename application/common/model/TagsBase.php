<?php
namespace app\common\model;

use think\Db;
use think\Model;

class TagsBase extends Model
{

    protected $name = 'tags_relation';

    public function getTagsList()
    {
        return Db::name('tags')->field('id,name,number')->select();
    }

    public function getTag($tid)
    {
        return Db::name('tags')->where('id', $tid)->find();
    }

    /**
     * mysql下的pivot方法还未解决
     *
     * @param string $name
     * @param integer $page
     * @param integer $limit
     * @return void
     * @author dmakecn@163.com
     * @since 2022-05-18
     */
    public function getTags($name = '', $page = 1, $limit = 10)
    {

        return ['list' => [], 'count' => 0];

        $sql = "SELECT * FROM (select tid,name,number,targettype,COUNT(targettype) as typecount from tags_relation a inner join tags b on a.tid=b.id %s group by targettype,name,tid,number limit $page,$limit) as P
                PIVOT (sum(typecount) FOR targettype  IN ( [0],[1],[2],[3],[4],[5],[6],[7])) AS T )AS A where ";
        if ($name != '') {
            $sql = sprintf($sql, " and name like'%" . $name . "%' ");
        } else {
            $sql = sprintf($sql, '');
        }

        $list = $this->query($sql);

        $MAP = [];
        if ($name != '') {
            $MAP['name'] = ['like', '%' . $name . '%'];
        }

        $count = Db::name('tags')->where($MAP)->count();

        return ['list' => $list, 'count' => $count];
    }

    /**
     * 获取某一个Tag的元清单
     */
    public function getRelations($tid, $type, $page, $limit)
    {
        $map['a.tid'] = $tid;
        $map['a.targettype'] = $type;

        $list = null;

        switch ($type) {
            case 0: // 用户
                break;
            case 1: //产品
                $list = $this->alias('a')->field('a.id,b.name')->join('product b', 'a.oid=b.id', 'left')->where($map)->select();
                break;
            case 2: //文章
                $list = $this->alias('a')->field('a.id,b.title as name')->join('article b', 'a.oid=b.id', 'left')->where($map)->select();
                break;
            case 3: //供应商
                $list = $this->alias('a')->field('a.id,b.name')->join('supplier b', 'a.oid=b.id', 'left')->where($map)->select();
                break;
            case 4: //问题
                break;
            case 5: //视频
                $list = $this->alias('a')->field('a.id,b.title as name')->join('video b', 'a.oid=b.id', 'left')->where($map)->select();
                break;
            case 6: //demo
                $list = $this->alias('a')->field('a.id,b.name')->join('product_resource b', 'a.oid=b.id', 'left')->where($map)->select();
                break;
            case 7: //广告
                break;
        }

        return $list;
    }

    /**
     * 获取某个对象关联的标签,返回标签数据
     * @param unknown $id
     * @param unknown $type
     */
    public function getObjTags($id, $type)
    {
        return $this->alias('a')
            ->field('a.tid,b.name')
            ->join('tags b', 'a.tid=b.id', 'inner')
            ->where(['oid' => $id, 'targettype' => $type])
            ->group('a.tid,Name')
            ->select();
    }

    /**
     * 获取某个对象关联的标签，返回对象数据
     * @param unknown $id
     * @param unknown $type
     * @return unknown
     */
    public function getObjTagsDetail($id, $type)
    {
        return $this->alias('a')
            ->field('a.id,b.name')
            ->join('tags b', 'a.tid=b.id', 'inner')
            ->where(['oid' => $id, 'targettype' => $type])
            ->group('a.id,name')
            ->select();
    }

    /**
     * 热门标签
     * @param number $limit
     * @return mixed|\think\cache\Driver|boolean|NULL|\think\Collection|\think\db\false|PDOStatement|string
     */
    public function getHotTags($limit = 50)
    {
        $key = '-' . $limit;

        $list = dcache('HOTTAGS', $key);

        if ($list == null) {
            $list = Db::name('tags')->field('id,name,number')
                ->order('number desc')
                ->limit($limit)
                ->select();
            dcache('HOTTAGS', $key, $list);
        }

        return $list;
    }

}
