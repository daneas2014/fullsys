<?php
namespace  app\common\model;

use think\Model;

class SubscribeBase extends Model{
    
    /**
     * 用于记录用户关注的产品，如果系统有更新，可以通过邮件、微信、站内信通知其更新状态
     * @param unknown $uid
     * @param unknown $productid
     */
    public function dingProduct($uid,$productid){
        return false;
    }
}