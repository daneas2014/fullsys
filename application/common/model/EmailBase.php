<?php
namespace app\common\model;

use think\Db;
use think\Model;
use \Swift_Message;
use \Swift_SmtpTransport;

/**
 * 邮件发送统一类，对发件组件更新升级，php7.0及以下试用sendMail方法
 *
 * @author dmakecn@163.com
 * @since 2019-12-17
 */
class EmailBase extends Model
{

    private $sendto;

    public function __construct($sendto)
    {
        $this->sendto = $sendto;
    }

    public function sendRegmail($code)
    {

        $msg = '您的注册验证码为：' . $code . '，本验证码5分钟内有效，请登录' . SITE_NAME . '核实';

        $content = '<html><head><meta http-equiv="content-type" content="text/html; charset=utf-8"></head><body>' . $msg . '</body></html>';

        $res = $this->sendMailV2($this->sendto, '您在' . SITE_NAME . '注册通知邮件', $content);

        return json($res);
    }

    public function sendFindpwd($name, $pwd)
    {

        $msg = '<div>您的账号<b>' . $name . '</b>密码已重置，新密码为<b>' . $pwd . ',收到密码后请登录' . SITE_NAME . '修改<a href="' . SITE_DOMAIN . '" target="_blank">登录</a></b></div>';

        $content = '<html><head><meta http-equiv="content-type" content="text/html; charset=utf-8"></head><body>' . $msg . '</body></html>';

        $res = $this->sendMailV2($this->sendto, '您的密码重置通知邮件', $content);

        return json($res);
    }

    /**
     * 给客户发信息，给销售发信息
     * @param unknown $order
     */
    public function sendOrderPaid($order)
    {

        $msg = '<h4>订单：' . $order['trade_no'] . '已支付<h4>' . $html;

        $content = '<html><head><meta http-equiv="content-type" content="text/html; charset=utf-8"></head><body>' . $msg . '</body></html>';

        $res = $this->sendMailV2($this->sendto, '您有新的订单刚才已经被支付，请登录平台查看', $content);

        return json($res);
    }

    private function mailSenderCreate($sendto, $subject, $body)
    {
        $db = Db::name('op_mailsender');
        $mail['sendto'] = $sendto;
        $mail['subject'] = $subject;
        $mail['body'] = $body;
        $mail['create_time'] = time();
        $mail['send_time'] = 0;

        return $db->insertGetId($mail);
    }

    /**
     * 自动任务自动发信
     *
     * @return bool
     * @author dmakecn@163.com
     * @since 2019-12-17
     */
    public function mailSender()
    {
        $db = Db::name('op_mailsender');

        $list = $db->where('send_time', 0)
            ->order('id asc')
            ->limit(5)
            ->select();

        if ($list == null) {
            return true;
        }

        foreach ($list as $item) {
            $res = $this->sendMailV2($item['sendto'], $item['subject'], $item['body']);
            if ($res['code'] == 1) {
                $db->where('id', $item['id'])->setField('send_time', time());
            }
        }

        return true;
    }

    /**
     * 邮件发送升级，5.*版本试用common方法发送，7.*使用swiftmailer发送
     *
     * @param [type] 发送对象
     * @param [type] 发送主题
     * @param [type] html详情
     * @return []
     * @author dmakecn@163.com
     * @since 2019-12-17
     */
    public function sendMailV2($sendto, $subject, $content, $copyto = [])
    {
        $mail = config('mail');

        // PHP 5.*用本办法发送
        if (strstr(phpversion(), "5.")) {
            return sendMail($sendto, $subject, $content, $mail['mail'], $mail['nick'], $mail['password'], $mail['host'], $mail['port']);
        }

        // Create the Transport
        $transport = (new \Swift_SmtpTransport($mail['host'], $mail['port']))
            ->setUsername($mail['mail'])
            ->setPassword($mail['password'])
        ;

        // Create the Mailer using your created Transport
        $mailer = new \Swift_Mailer($transport);

        if($copyto){
            array_unshift($sendto, $copyto);
        }

        // Create a message
        $message = (new \Swift_Message($subject))
            ->setFrom([$mail['mail'] => $mail['nick']])
            ->setTo($copyto)
            ->setBody($content);

        // Send the message
        $result = $mailer->send($message);

        return $result ? ['code' => 1, 'msg' => '发送成功'] : ['code' => -1, 'msg' => '发送失败'];
    }
}
