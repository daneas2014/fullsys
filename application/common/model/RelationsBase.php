<?php
namespace app\common\model;

use think\Db;
use think\Model;

/**
 * 这个类有个问题，这个是手工绑定了的才算，产品相关的应该在文章中选中的所属产品后，自动关联厂商，或者是选中厂商后
 *
 * @author Administrator
 *
 */
class RelationsBase extends Model {

	protected $name = 'article_relations';

	/**
	 * 以后新增关联都这么的
	 *
	 * @param unknown $articleid
	 * @param unknown $productid
	 * @return boolean|unknown
	 */
	public function saveProductRelation($articleid, $productid) {
		$map['articleid'] = $articleid;
		$map['productid'] = $productid;
		$map['sourcefrom'] = 'article';
		if ($this->where($map)->find() != null) {
			return true;
		}

		$supplier = Db::name('product')->field('supplierid')
			->where('id', $productid)
			->find();
		$map['supplierid'] = $supplier['supplierid'];

		$id = $this->insertGetId($map);

		if ($id > 0) {
			return [
				'code' => 1,
				'data' => $id,
				'msg' => '文章-产品-供应商关联完毕',
			];
		}

		return [
			'code' => -1,
			'data' => '',
			'msg' => '关联失败',
		];
	}

	/**
	 * 以后新增关联都这么的
	 *
	 * @param unknown $articleid
	 * @param unknown $productid
	 * @return boolean|unknown
	 */
	public function saveSupplierRelation($articleid, $supplierid) {
		$map['articleid'] = $articleid;
		$map['supplierid'] = $supplierid;
		$map['sourcefrom'] = 'article';
		if ($this->where($map)->find() != null) {
			return true;
		}

		$id = $this->insertGetId($map);

		if ($id > 0) {
			return [
				'code' => 1,
				'data' => $id,
				'msg' => '文章-供应商关联完毕',
			];
		}

		return [
			'code' => -1,
			'data' => '',
			'msg' => '文章-供应商关联失败',
		];
	}

	/**
	 * 将文章和系列文章绑定
	 *
	 * @param unknown $groupid
	 * @param unknown $articleid
	 * @return boolean|number[]|string[]|unknown[]|number[]|string[]
	 */
	public function saveArticleGroupRelation($groupid, $articleid) {
		$map['articleid'] = $articleid;
		$map['groupid'] = $groupid;
		$map['sourcefrom'] = 'articlegroup';
		if ($this->where($map)->find() != null) {
			return true;
		}
        
		$id = $this->insertGetId($map);

		if ($id > 0) {
			return [
				'code' => 1,
				'data' => $id,
				'msg' => '系列文章-文章关联完毕',
			];
		}

		return [
			'code' => -1,
			'data' => '',
			'msg' => '系列文章-文章关联失败',
		];
	}

	/**
	 * 通过那个关系的id删除
	 *
	 * @param unknown $id
	 */
	public function delRalation($id) {
		if ($this->where('id', $id)->delete()) {
			return [
				'code' => 1,
				'data' => '',
				'msg' => '关系关联已删除',
			];
		}

		return [
			'code' => -1,
			'data' => '',
			'msg' => '关系关联删除失败',
		];
	}

	/**
	 * 获得文章关联的产品(简版)
	 *
	 * @param unknown $article
	 */
	public function getArticleRelations($articleid, $limit = 50) {
		return $this->alias('a')
			->field('a.id,a.productid,b.name,b.description,b.imagepath,b.producttype,slogan')
			->join('product b', 'a.productid=b.Id', 'inner')
			->where('a.articleid', $articleid)
			->limit($limit)
			->select();
	}

	/**
	 * 获得文章关联的产品信息（详细版）
	 *
	 * @param unknown $articleid
	 * @param number $limit
	 * @return unknown
	 */
	public function getArticleProductRelation($articleid, $limit = 50) {
		return $this->alias('a')
			->field('a.id,a.productid,b.name,b.slogan,b.version,b.categoryid,c.name as supplier,c.id as supplierid,b.description')
			->join('product b', 'a.productid=b.id', 'left')
			->join('supplier c', 'b.supplierid=c.id', 'left')
			->where('a.articleid', $articleid)
			->where('productid>0')
			->limit($limit)
			->select();
	}

	/**
	 * 获得文章绑定过的供应商，因为对应的太多了，所以需要去重
	 *
	 * @param unknown $articleid
	 * @param number $limit
	 * @return unknown
	 */
	public function getArticleSupplierRelation($articleid, $limit = 50) {
		$map['articleid'] = $articleid;
		$map['supplierid'] = [
			'>',
			0,
		];
		return $this->alias('a')
			->field('max(a.id) as id,supplierid,b.name,b.logopath')
			->join('supplier b', 'a.supplierid=b.id', 'left')
			->where($map)
			->limit($limit)
			->group('supplierid,name,logopath')
			->having('count(supplierid)>0')
			->select();
	}

	/**
	 * 获取系列文章的关联
	 *
	 * @param unknown $groupid
	 * @param number $limit
	 */
	public function getArticleGroupRelations($groupid, $limit = 50) {
		return $this->alias('a')
			->field('a.id,a.articleid,b.title')
			->join('article b', 'a.articleid=b.id', 'left')
			->where('a.groupid', $groupid)
			->limit($limit)
			->select();
	}

	/**
	 * 获取跟文章相关的视频
	 * @param unknown $articleid
	 * @param number $limit
	 * @return unknown
	 */
	public function getArticleVideoRelations($articleid, $limit = 10) {
		return $this->alias('a')
			->field('distinct(b.id),b.title,b.imagepath')
			->join('video b', 'a.productid=b.productid', 'inner')
			->where('articleid', $articleid)
			->order('id desc')
			->limit($limit)
			->select();
	}

	/**
	 * 获得产品相关的文章和供应商
	 *
	 * @param unknown $productid
	 */
	public function getProductRelations($productid, $limit = 20) {

	}

	/**
	 * 获得供应商关联的产品和文章
	 *
	 * @param unknown $supplierid
	 */
	public function getSupplierRelations($supplierid, $limit = 20) {

	}

	/**
	 * 获得最热门的关联产品
	 *
	 * @param number $limit
	 */
	public function getHotProductRelations($limit = 10) {
		$map['productid'] = ['>', 0];
		$map['status'] = 1;

		$products = $this->alias('a')
			->field('productid,COUNT(productid)as totalcount,name,imagepath,slogan')
			->join('product b', 'a.productid=b.id', 'left')
			->where($map)
			->group('productid,name,imagepath,slogan')
			->order('totalcount desc')
			->limit($limit)
			->select();

		return $products;
	}
}