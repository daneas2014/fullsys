<?php
namespace app\common\model;

use think\Model;
use think\Db;

/**
 *  领券操作
 * @author Administrator
 *
 */
class CouponBase extends Model
{

    protected $name = 'coupon';

    /**
     * 获取可以领取的活动券,一次为母券
     * @return unknown
     */
    public function freeCoupons($customerid)
    {

        $sql="select * from coupon where type='activity' and pid=0 and leftnum>0 and end_time>%d and id not in
                    (select pid from customer_coupon a inner join coupon b on a.couponid=b.id where customerid=%d)";
                
        $list=Db::query(sprintf($sql,time(),$customerid));
        
        return $list;
    }
    
    /**
     * 个人通过激活链接领券
     * @param unknown $couponid
     * @param unknown $customerid
     */
    public function activeCoupon($couponid,$customerid){
        
        $parent=$this->get(['id'=>$couponid]);
        
        if($parent['end_time']<time()){
            return ['code'=>-1,'data'=>'','msg'=>'领券活动已结束'];
        }
        
        if($parent['leftnum']<1){
            return ['code'=>-1,'data'=>'','msg'=>'活动券已经领光啦'];
        }
        
        if($parent['status']==1){
            return ['code'=>-1,'data'=>'','msg'=>'活动券已经领光啦'];
        }
        
        // 判断用户是否参与了活动
        
        $map['customerid']=$customerid;
        $map['pid']=$couponid;
        $check=$this->alias('a')->join('customer_coupon b','a.id=b.couponid','inner')
        ->where($map)
        ->count();
        
        if($check>0){
            return ['code'=>-1,'data'=>'','msg'=>'您已经领过本活动券啦'];
        }
        
        $newcode='A' . rand(10000, 99999);
        $coupon['name'] = $parent['name'];
        $coupon['code'] = $newcode;
        $coupon['amount'] = $parent['amount'];
        $coupon['productid'] = $parent['productid'];
        $coupon['type'] = $parent['type'];
        $coupon['totalnum'] = 1;
        $coupon['leftnum'] = 1;
        $coupon['status'] = 0;
        $coupon['end_time'] = $parent['end_time'];
        $coupon['adminid'] = $parent['adminid'];
        $coupon['pid'] = $parent['id'];
        
        $member['customerid'] = $customerid;
        $member['adminid'] = $parent['adminid'];
        
        $json = $this->sendCustomerCounpon($coupon, $member);        
        if ($json['code'] == 1) {            
            $this->where('id', $couponid)->setDec('leftnum');
            $json['data']=$newcode;
            $json['msg']=date('Y-m-d',$coupon['end_time']);
            $json['amount']=$coupon['amount'];
            $json['name']=$coupon['name'];
        }
        
        return $json;
    }
    
    /**
     * 将优惠券信息和会员信息同时拿来组装入库
     * @param unknown $coupon
     * @param unknown $member
     * @return number[]|string[]
     */
    public function sendCustomerCounpon($coupon,$member){
        $coupon['create_time'] = time();
        $coupon['update_time'] = 0;
        $coupon['used_time'] = 0;
        
        $couponid = $this->insertGetId($coupon);
        if($couponid<1){
            return ['code'=>-1,'msg'=>'优惠券创建失败','data'=>''];
        }
        
        $member['couponid'] = $couponid;
        $member['create_time'] = time();
        $member['update_time'] = 0;
        $member['delete_time'] = 0;
        
        if(Db::name('Customercoupon')->insert($member)){
            return ['code'=>1,'msg'=>'优惠券已创建，并发放成功','data'=>''];
        }
        
        return ['code'=>-1,'msg'=>'优惠券已创建,但发放失败','data'=>''];
    }

    /**
     * 获取某人的所有优惠券
     * 
     * @param unknown $customerid            
     */
    public function customerCoupons($customerid)
    {
        $map['customerid'] = $customerid;
        $list = $this->alias('a')
            ->field('a.*,b.create_time as revicetime')
            ->join('customer_coupon b', 'a.id=b.couponid', 'inner')
            ->where($map)
            ->order('end_time asc')
            ->select();
        
        return $list;
    }
}