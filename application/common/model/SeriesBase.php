<?php
namespace app\common\model;

use think\Db;
use think\Model;

class SeriesBase extends Model {

	protected $name = 'series_description';

	/**
	 * 获得单节课程
	 * @param unknown $id
	 * @return array|\think\db\false|PDOStatement|string|\think\Model
	 */
	public function getVideoItems($id) {
		return Db::name('series_item')->where('id', $id)->find();
	}

	/**
	 * 输出整个课程详情
	 * @param unknown $id
	 */
	public function getVideoSeries($id) {

		// 当前课程
		$course = $this->where('id', $id)->find();

		// 总课程情况
		$course['duration'] = Db::name('series_item')->where('courseid', $id)->sum('duration');

		// 章节
		$coursegroup = Db::name('series_group')->where('courseid', $id)->select();

		// 小课情况
		$courseitem = Db::name('series_item')->field('id,title,imagepath,duration,groupid')->where(['courseid' => $id, 'duration' => ['>', 0]])->select();

		if ($coursegroup == null) {
			$coursegroup = [
				['id' => 0, 'name' => '默认', 'items' => $courseitem],
			];
		} else {
			for ($x = 0; $x < count($coursegroup); $x++) {
				$items = [];
				foreach ($courseitem as $i) {
					if ($i['groupid'] == $coursegroup[$x]['id']) {
						array_push($items, $i);
					}
				}

				$coursegroup[$x]['items'] = $items;
				unset($items);
			}
		}

		return ['course' => $course, 'groups' => $coursegroup];
	}

	/**
	 * 获得课程分类
	 * @return \think\Collection|\think\db\false|PDOStatement|string
	 */
	public function getCategory() {
		return Db::name('series_category')->select();
	}

	public function getUserAccess($userid, $courseid) {
		$db = Db::name('series_access');

		$map['customerid'] = $userid;
		$map['courseid'] = ['in', [0, 1, $courseid]];
		$map['deadline'] = ['>', time()];

		$log = $db->where($map)->count();

		return $log == 0 ? false : true;
	}

	/**
	 * 本周热门视频，这里需要注意
	 * 要通过计算获得时间段的，目前这个功能仅做展示所以没有深入
	 *
	 * @param integer $limit
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2021-01-29
	 */
	public function getHotPlay($limit = 5) {
		return Db::name('series_item')->order('views desc')->limit($limit)->select();
	}
}