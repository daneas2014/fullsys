<?php
namespace app\common\model;

use think\Model;

class ProductBase extends Model
{

    protected $name = 'product';

    /**
     * 首页推荐产品
     * 
     * @param unknown $limit            
     * @return unknown
     */
    public function getRecommendProducts($limit)
    {
        $list = $this->alias('a')
            ->field('b.name as tagname,c.tid as tagid,a.id,a.imagepath,a.name,a.description,a.version')
            ->join('(select distinct tid,oid from tags_relation where targettype=1) c', 'c.oid=a.id')
            ->join('tags b', 'c.tid=b.id')
            ->where('a.status', 1) 
            ->limit($limit)
            ->select();
        
        return $list;
    }

    /**
     * 被热门关联的产品
     */
    public function getHotRalateProducts($limit=3)
    {
        $rm=new RelationsBase();        
        return $rm->getHotProductRelations($limit);
    }

    /**
     * 用于查询产品清单【供应商等信息】
     * 
     * @param unknown $map            
     * @param unknown $page            
     * @param unknown $limit            
     * @return $res['list'],$res['count']
     */
    public function getProducts($keyword, $page, $limit)
    {
        $map = [];
        if ($keyword && $keyword !== "") {
            $map['a.name'] = ['like',"%" . $keyword . "%"];
        }
        
        $list = $this->alias('a')
            ->field('a.id,a.name,a.description,b.name as supplier,a.version,a.imagepath')
            ->join('supplier b', 'a.supplierid=b.id', 'left')
            ->where($map)
            ->page($page, $limit)
            ->select();
        
        $count = $this->alias('a')->where($map)->count();
        
        $res['list'] = $list;
        $res['count'] = $count;
        
        return $res;
    }
    
    
}