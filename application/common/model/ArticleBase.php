<?php
namespace app\common\model;

use think\Model;
use think\Db;

class ArticleBase extends Model
{

    protected $name = 'article';

    protected $searchFields = 'a.id,title,imagepath,summary,create_time';

    /**
     * 获取单个资讯
     *
     * @param unknown $id            
     * @return unknown
     */
    public function getArticle($id)
    {
        return $this->alias("a")
            ->field('a.*,b.name as categoryname,d.real_name')
            ->join('article_category b', 'a.categoryid=b.id', 'left')
            ->join('sys_admin d', 'd.id=a.adminid', 'left')
            ->where('a.id', $id)
            ->find();
    }

    /**
     * 获取文章的前后两个文章
     *
     * @param unknown $id            
     * @param unknown $type            
     * @return unknown
     */
    public function getSiblings($id, $type)
    {
        $sql = "select * from (select id,title,imagepath,create_time from article where categoryid=$type and id<$id order by id desc limit 1 )
a union (select id,title,imagepath,create_time from article where categoryid=$type and id>$id order by id desc limit 1)";
        
        return $this->query($sql);
    }

    /**
     * 获取文章属性
     *
     * @param unknown $articleid            
     * @return array|\think\db\false|PDOStatement|string|\think\Model
     */
    public function getAttribute($articleid)
    {
        $db = Db::name('article_attribute');
        
        $detail = $db->where('articleid', $articleid)->find();
        
        if ($detail == null) {
            $detail['articleid'] = $articleid;
            $detail['clickcount'] = 1;
            $detail['commentcount'] = 0;
            $detail['up'] = 0;
            $detail['down'] = 0;
            $detail['sharecount'] = 0;
            
            $db->insert($detail);
        }
        
        return $detail;
    }

    /**
     * 设置文章属性增量
     *
     * @param unknown $articleid            
     * @param unknown $type            
     */
    public function setArticleAttr($articleid, $type)
    {
        return Db::name('article_attribute')->where('articleid', $articleid)->setInc($type);
    }

    /**
     * 根据搜索条件获取用户列表信息
     */
    public function getArticleByWhere($map, $Nowpage, $limits)
    {
        $count = $this->where($map)->count();
        
        $list = $this->alias('a')
            ->field('a.id,a.title,a.imagepath,a.ispublish,a.create_time,real_name,c.name,a.summary')
            ->join('article_category c', 'c.id=a.categoryid', 'inner')
            ->join('sys_admin d', 'd.id=a.adminid', 'left')
            ->where($map)
            ->page($Nowpage, $limits)
            ->order('id desc')
            ->select();
        
        $res['count'] = $count;
        $res['list'] = $list;
        
        return $res;
    }

    /**
     * 获取供应商的最新文章
     *
     * @param unknown $supplierid            
     * @param number $categoryid            
     * @param number $page            
     * @param number $limit            
     */
    public function getArticleSupplier($supplierid, $categoryid = 0, $page = 1, $limit = 5)
    {
        $key = CACHE_ARTICLE . 'ATOSUPPLIER' . $supplierid . '_' . $categoryid . '_' . $page . '_' . $limit;
        
        $list = cache($key);
        
        if ($list == null) {
            
            $db = Db::name('article_relations');
            
            $news = $db->alias('a')
                ->field('DISTINCT(b.id),b.title,b.imagepath,b.create_time,categoryid')
                ->join('article b', 'a.articleid=b.Id', 'left')
                ->where('a.supplierid', $supplierid)
                ->where('categoryid', 1)
                ->page($page, $limit)
                ->select();
            
            $tech = $db->alias('a')
                ->field('DISTINCT(b.id),b.title,b.imagepath,b.create_time,categoryid')
                ->join('article b', 'a.articleid=b.id', 'left')
                ->where('a.supplierid', $supplierid)
                ->where('categoryid', 2)
                ->page($page, $limit)
                ->select();
            
            $ping = $db->alias('a')
                ->field('DISTINCT(b.id),b.title,b.imagepath,b.create_time,categoryid')
                ->join('article b', 'a.articleid=b.id', 'left')
                ->where('a.supplierid', $supplierid)
                ->where('categoryid', 3)
                ->page($page, $limit)
                ->select();
            
            $updates = $db->alias('a')
                ->field('DISTINCT(b.id),b.title,b.imagepath,b.create_time,categoryid')
                ->join('article b', 'a.articleid=b.id', 'left')
                ->where('a.supplierid', $supplierid)
                ->where('categoryid', 4)
                ->page($page, $limit)
                ->select();
            
            $list['news'] = $news;
            
            $list['tech'] = $tech;
            
            $list['ping'] = $ping;
            
            $list['updates'] = $updates;
            
            cache($key, $list);
        }
        return $list;
    }
}
