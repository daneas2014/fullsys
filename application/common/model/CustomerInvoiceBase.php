<?php
namespace app\common\model;

use think\Model;

class CustomerInvoiceBase extends Model
{

    protected $name = 'customer_invoice';

    /**
     * 获取用户发票信息
     * @param unknown $customerid
     * @return unknown
     */
    public function getByCustomer($customerid)
    {
        return $this->get(['customerid' => $customerid]);
    }

    /**
     * 保存发票信息
     * @param unknown $data
     * @return number|\think\false
     */
    public function saveOne($data)
    {
        if ($data['id']) {
            return $this->allowField(true)->save($data, ['id' => $data['id']], 'id');
        }

        $data->create_time = time();
        return $this->allowField(true)->save($data);
    }
}
