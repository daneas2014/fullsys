<?php
namespace app\common\model;

use think\Model;

class SupplierBase extends Model
{

    protected $name = 'supplier';
    
    /**
     * 获取供应商资料，因为字段内容不多，所以做的全表展示
     * 
     * @param unknown $map            
     * @param unknown $pageindex            
     * @param unknown $limit            
     * @return unknown
     */
    public function getSuppliers($map, $pageindex, $limit)
    {
        $map['a.status'] = 1;
        
        $count = $this->alias('a')->where($map)->count();
        
        $list = $this->alias('a')->field('a.*,b.real_name')
            ->join('sys_admin b','a.adminid=b.id','left')
            ->where($map)
            ->page($pageindex, $limit)
            ->order('adminid desc')
            ->select();
        
        $res['count'] = $count;
        $res['list'] = $list;
        
        return $res;
    }

    /**
     * 获取供应商信息
     * 
     * @param unknown $id            
     * @return unknown
     */
    public function getSupplier($id)
    {
        return $this->alias('a')->field('a.*,b.real_name')->join('sys_admin b','a.adminid=b.id','left')->where('a.id', $id)->find();
    }

    /**
     * 保存供应商
     * 
     * @param unknown $data            
     * @return number|\think\false|unknown
     */
    public function saveSupplier($data)
    {
        if ($data['id'] > 0) {
            $where['id'] = $data['id'];
            unset($data['id']);
            return $this->update($data, $where);
        }
        unset($data['id']);
        return $this->allowField(true)->insertGetId($data);
    }

    /**
     * 产品编辑页获取供应商列表
     * 
     * @return number|\think\false|unknown
     */
    public function getSuppliersOnlyName()
    {
        $map['status'] = 1;
        return $this->field('id,name')
            ->where($map)
            ->order('id desc')
            ->select();
    }
}