<?php
namespace app\common\model;

use think\Db;
use think\Loader;
use think\Model;

Loader::import('taobaoke/TopSdk', EXTEND_PATH);

/*
 * 产品说明：本分类负责为公众号、小程序、网页提供各类接口
 */
class TaobaoBase extends Model
{
    protected $name = 'tb_item';

    const pagesize = 8;
    public $appkey;
    public $secret;
    public $uninid;
    public $siteid;
    public $adzoneid;
    public $subpid;

    /**
     * 公开对外使用的接口
     *
     * @param string $appkey
     * @param string $secret
     * @param string $pid
     * @author dmakecn@163.com
     * @since 2020-04-27
     */
    public function __construct($appkey = '27728681', $secret = 'f6eb4cc5f50185c77d5ca50f818fb2eb', $pid = 'mm_23250501_15176373_109838350085')
    {
        $codes = explode('_', $pid);
        $this->appkey = $appkey;
        $this->secret = $secret;
        $this->uninid = $codes[1];
        $this->siteid = $codes[2];
        $this->adzoneid = $codes[3];
        $this->subpid = $pid;
    }

    /**
     * 初始化client
     * @return \TopClient
     */
    public function tClient()
    {
        return new \TopClient('27728681', 'f6eb4cc5f50185c77d5ca50f818fb2eb');
    }

    /**
     * 获得关联产品或者选品库产品
     *
     * @param integer $goodid 商品id
     * @param integer $favid 选品库id
     * @param integer $contentid 图文id
     * @return void
     * 没有&meterialid采集不到  https://tbk.bbs.taobao.com/detail.html?appId=45301&postId=8576096
     * https://tbk.bbs.taobao.com/detail.html?appId=45301&postId=9373501
     * @example
     * @author daneas
     * @since 2020-03-29
     */
    public function getMaterialitems($goodid = 0, $favid = 0, $contentid = 0, $pageindex = 1, $meterialid = 0, $pagesize = 30)
    {
        $req = new \TbkDgOptimusMaterialRequest;
        $req->setAdzoneId($this->adzoneid);
        $req->setPageNo($pageindex);
        $req->setPageSize($pagesize);
        //$req->setHasCoupon("true");

        if ($meterialid > 0) {
            $req->setMaterialId("$meterialid");
        }
        if ($goodid > 0) {
            $req->setItemId("$goodid");
        }
        if ($favid > 0) {
            $req->setFavoritesId("$favid");
        }
        if ($contentid > 0) {
            $req->setContentId("$contentid");
        }

        $resp = self::tClient()->execute($req);

        $jsonStr = json_encode($resp);
        $jsonArray = json_decode($jsonStr, true);

        if (array_key_exists('code', $jsonArray)) {
            return null;
        }

        $items = $jsonArray['result_list']['map_data'];

        // 什么参数都不包含的情况下就是获取产品库清单了
        if ($meterialid == 31519) {
            return $items['favorites_info'];
        }

        // 产品推荐列表、选品库、图文内容所包含的产品列表、搜索特价
        if ($goodid > 0 || $favid > 0 || $contentid > 0 || $meterialid == 3786) {
            $urls = [];
            foreach ($items as $i) {
                if (!array_key_exists('coupon_click_url', $i)) {
                    continue;
                }

                if (strlen($i['click_url']) > 1000) {
                    continue;
                }

                array_push($urls, ['num_iid' => $i['item_id'], 'item_url' => '', 'click_url' => 'https:' . $i['click_url'], 'coupon_url' => 'https:' . $i['coupon_click_url'], 'create_time' => time()]);
            }

            $this->storeTkUrls($urls);

            unset($urls);

            return $items;
        }

        return $items;
    }

    /**
     * 全体搜索
     *
     * @param [type] $words
     * @param integer $page
     * @param integer $pricestart
     * @param integer $pricelimit
     * @return 结果清单，直接可用的数组
     * @author daneas
     */
    public function getDG($words, $page = 1, $pricestart = 20, $pricelimit = 500, $material_id = 2836,$pagesize=self::pagesize)
    {
        $words = $words == "全部" ? "特价" : $words;
        $req = new \TbkDgMaterialOptionalRequest;
        $req->setQ($words);
        $req->setAdzoneId($this->adzoneid);
        $req->setPageSize($pagesize);
        $req->setStartPrice($pricestart);
        $req->setEndPrice($pricelimit);
        $req->setPageNo($page);
        $req->setHasCoupon("true");
        $req->setSort("tk_total_sales_desc");
        $req->setNeedFreeShipment("true");

        if ($material_id != 2836 && $material_id != 0) {
            $req->setMaterialId($material_id);
        }

        $resp = self::tClient()->execute($req);

        $jsonStr = json_encode($resp);
        $jsonArray = json_decode($jsonStr, true);

        if (array_key_exists('code', $jsonArray)) {
            return $resp;
        }

        $items = $jsonArray['result_list']['map_data'];

        $urls = [];
        foreach ($items as $i) {
            if (!array_key_exists('coupon_share_url', $i)) {
                continue;
            }
            array_push($urls, ['num_iid' => $i['item_id'], 'item_url' => '', 'click_url' => '', 'coupon_url' => 'https:' . $i['coupon_share_url'], 'create_time' => time()]);
        }

        $this->storeTkUrls($urls);

        unset($urls);

        return $items;
    }

    /**
     * 选品库，需要经常手动更新
     * @param string 选品库ID
     * @param number 页码
     * @param number 单页数量
     * @return mixed
     */
    public function Xuanpin($favid = null, $page = 1, $pagesize = 30)
    {

        $favid = $favid ? $favid : $this->Xuanpingroup();

        $items['list'] = $this->getMaterialitems(0, $favid, 0, 1, 31539);

        $items['favid'] = $favid;

        return $items;
    }

    /**
     * 获得所有选品库
     *
     * @return 返回选品库列表
     * @author dmakecn@126.com
     * @since 2020-3-29
     */
    public function Xuanpingroup()
    {
        $groups = $this->getMaterialitems(0, 0, 0, 1, 31519);
        return $groups ? $groups['favorites_list']['favorites_detail']['favorites_id'] : null;
    }

    /**
     * 获得单个商品和关联商品
     * @param unknown $num_iid
     * @return mixed[]|unknown[]
     */
    public function InfoGet($num_iid,$platform=2)
    {
        $c = self::tClient();

        $req = new \TbkItemInfoGetRequest();
        $req->setNumIids($num_iid);
        $req->setPlatform($platform);
        $req->setIp("11.22.33.43");
        $resp = $c->execute($req);
        $jsonStr = json_encode($resp);
        $jsonArray = json_decode($jsonStr, true);
        $item = $jsonArray['results']['n_tbk_item'];
        unset($req);
        unset($resp);

        $items = $this->getMaterialitems($num_iid, 0, 0, 1, 13256); //理论上用13256，6708，但是不得行啊

        return ['item' => $item, 'items' => $items];
    }

    /**
     * 获取商品清单
     * @param unknown $num_iid
     * @return mixed
     */
    public function ItemsGet($num_iid)
    {

        $c = self::tClient();

        $req = new \TbkItemInfoGetRequest();
        $req->setNumIids($num_iid);
        $req->setPlatform("2");
        $req->setIp("11.22.33.43");
        $resp = $c->execute($req);
        $jsonStr = json_encode($resp);
        $jsonArray = json_decode($jsonStr, true);

        if (array_key_exists('code', $jsonArray) || !array_key_exists('results', $jsonArray)) {
            $items = null;
        } else {
            $items = $jsonArray['results']['n_tbk_item'];
        }

        return $items;
    }

    /**
     * 获得内容
     * @param number 1是图文，2是图集，3是短视频
     * @param number 时间戳*1000
     * @return unknown
     */
    public function Content($type = 1, $time)
    {
        $req = new \TbkContentGetRequest();
        $req->setAdzoneId($this->adzoneid);
        $req->setType($type);
        $req->setBeforeTimestamp($time);
        $req->setCount(self::pagesize);
        $req->setImageWidth("600");
        $req->setImageHeight("600");
        $req->setContentSet("1");
        $resp = self::tClient()->execute($req);

        return $resp;
    }

    /**
     * 聚划算淘抢购，若搜索不到内容就入库并返回
     * http://open.taobao.com/docs/api.htm?spm=a219a.7395905.0.0.waAYW7&apiId=27543
     * @param 开始时间
     * @param 结束时间
     * @param 页码
     * @return 搜索到的结果
     */
    public function JuTqg($start, $end, $page = 1)
    {
        $req = new \TbkJuTqgGetRequest();
        $req->setAdzoneId($this->adzoneid);
        $req->setFields("click_url,pic_url,reserve_price,zk_final_price,total_amount,sold_num,title,category_name,start_time,end_time");
        $req->setStartTime($start);
        $req->setEndTime($end);
        $req->setPageNo($page);
        $req->setPageSize(self::pagesize);

        $resp = self::tClient()->execute($req);

        return $this->CheckSave('JuTqg', $resp);
    }

    /**
     * 官方活动链接转化
     * https://open.taobao.com/api.htm?docId=41918&docType=2&scopeId=16358
     * @param string 官方活动ID，从官方活动页获取
     * @param string 渠道关系ID，仅适用于渠道推广场景
     * @return unknown
     */
    public function Tbactivity($promotion_scene_id = "12345678", $relation_id = "23")
    {
        $req = new \TbkActivitylinkGetRequest();
        $req->setPlatform("2");
        $req->setPromotionSceneId($promotion_scene_id);
        $req->setRelationId($relation_id);
        $req->setUnionId($this->uninid);
        $req->setAdzoneId($this->adzoneid);
        $req->setSubPid($this->subpid);
        $resp = self::tClient()->execute($req);

        return $resp;
    }

    /*
     * 普通链接转淘口令，这个是被动查询
     * http://open.taobao.com/docs/api.htm?spm=a219a.7395905.0.0.NukMgV&apiId=27832
     * @param number 商品id
     */
    public function Link2code($num_iid, $item)
    {
        $c = self::tClient();
        if ($item == null) {
            $req = new \TbkItemInfoGetRequest();
            $req->setNumIids($num_iid);
            $req->setPlatform("2");
            $req->setIp("11.22.33.43");
            $resp = $c->execute($req);
            $jsonStr = json_encode($resp);
            $jsonArray = json_decode($jsonStr, true);
            $item = $jsonArray['results']['n_tbk_item'];
        }

        //************************  从系统里面获取领券链接，没有就去爱淘宝 START********************** */
        $good = Db::name('tk_urls')->where('num_iid', $num_iid)->find();

        $url = $good == null ? '' : $good['coupon_url'];

        if (!strstr($url, 'https')) {
            $url = 'https:' . $url;
        }
        if ($url == '' || $url == 'https:') {
            $url = 'https://ai.m.taobao.com/search.html?atype=b&searchfrom=1&back=true&q=' . $item['title'];
        }
        //************************  从系统里面获取领券链接，没有就去爱淘宝 END********************** */

        $req = new \TbkTpwdCreateRequest();
        $req->setUserId("1470211439694");
        $req->setText('为您推荐：' . $item['title']);
        $req->setUrl($url);
        $req->setLogo($item['pict_url']);
        $req->setExt("{}");
        $resp = $c->execute($req);

        $jsonStr = json_encode($resp);
        $jsonArray = json_decode($jsonStr, true);

        //*********************普通链接，s.click都不行*************************** */
        if (array_key_exists('code', $jsonArray)) {
            $code['code'] = $jsonArray['sub_msg'];
            $code['errcode'] = $jsonArray['code'];
            $code['url'] = $url;
            return $code;
        }

        $code['code'] = $jsonArray['data']['model'];
        $code['msg'] = '哇~恭喜你获得一个超级折扣哟，复制口令到手机淘宝即可使用！';
        $code['url'] = $url;

        return $code;
    }


    /**
     * 淘宝客长链变短链
     *
     * @param [type] $longurl
     * @return void
     * @description
     * @example
     * @author daneas
     * @since 2021-05-07
     */
    public function long2short($longurl){
        $c = self::tClient();
        $req = new \TbkSpreadGetRequest;
        $requests = new \TbkSpreadRequest;
        $requests->url=$longurl;
        $req->setRequests(json_encode($requests));
        $resp = $c->execute($req);
                
        $jsonStr = json_encode($resp);
        $jsonArray = json_decode($jsonStr, true);
        $code['code'] = $jsonArray['results']['tbk_spread']['err_msg'];
        $code['msg'] = '';
        $code['url'] = $jsonArray['results']['tbk_spread']['content'];

        return $code;

    }

    /**
     * 获得聚划算产品清单
     * @param unknown $words
     * @param number $page
     * @return boolean
     */
    public function getJuItems($words = '', $page = 1)
    {
        $req = new \JuItemsSearchRequest;
        $param_top_item_query = new \TopItemQuery;
        $param_top_item_query->current_page = $page;
        $param_top_item_query->page_size = self::pagesize;
        $param_top_item_query->pid = "-";
        $param_top_item_query->postage = "true";
        $param_top_item_query->status = "2";
        //$param_top_item_query->taobao_category_id="1000";
        $param_top_item_query->word = $words;
        $req->setParamTopItemQuery(json_encode($param_top_item_query));
        $resp = self::tClient()->execute($req);

        $jsonStr = json_encode($resp);
        $jsonArray = json_decode($jsonStr, true);
        if (array_key_exists('code', $jsonArray) || !array_key_exists('result', $jsonArray)) {
            return false;
        }

        $list = $resp['result']['model_list'];
        return $list;
    }

    /**
     * 根据接口解析进入的数据并删除重复和保存[19年老方法，针对老接口的]
     * @param unknown 方法名
     * @param unknown 接口返回数据
     * @return \think\response\Json|boolean
     */
    private function CheckSave($action, $resp)
    {

        $jsonStr = json_encode($resp);
        $jsonArray = json_decode($jsonStr, true);

        if (array_key_exists('code', $jsonArray)) {
            return $resp;
        }

        $catename = 'category_name';

        switch ($action) {
            case 'JuTqg': // 聚划算和淘抢购
                $goodtype = 1;
                if (!array_key_exists('results', $jsonArray)) {
                    return $resp;
                }

                $items = $jsonArray['results']['results'];
                break;
            case "DG":
                $goodtype = 0;
                if (!array_key_exists('result_list', $jsonArray)) {
                    return $resp;
                }

                $items = $jsonArray['result_list']['map_data'];
                break;
        }

        if ($items == [] || count($items) < 1) {
            return false;
        }

        $ids = '';

        foreach ($items as $item) {
            $ids .= ',' . $item['num_iid'];
        }

        //set_time_limit(120);

        $recodes = $this
            ->where('num_iid', 'in', $ids)
            ->delete();

        $data = [];
        foreach ($items as $item) {

            $item['category_name'] = $item[$catename];

            if ($item['category_name'] == '其他') {
                continue;
            }

            $item['goodstype'] = $goodtype;
            $item['create_time'] = time();

            //https://open.taobao.com/api.htm?docId=35896&docType=2
            if ($goodtype == 0) {
                if (array_key_exists('small_images', $item) && array_key_exists('string', $item['small_images'])) {
                    $small = $item['small_images']['string'];
                    $item['small_images'] = json_encode($small);
                }

                $this->insertGetId($item);
                array_push($data, $item);
            }

            //https://open.taobao.com/api.htm?docId=27543&docType=2
            if ($goodtype == 1) {
                $this->insertGetId($item);
                array_push($data, $item);
            }

            // 正常的搜索
            if ($goodtype == 2) {
                array_push($data, $item);
            }

            unset($item);
        }

        return $data;
    }

    /**
     * 领券的链接入库前需要检查是否存在，并排除【2020获取商品清单的新特性】
     *
     * @param [type] $urls
     * @return void
     * @author dmakecn@163.com
     * @since 2020-03-31
     */
    private function storeTkUrls($urls = [])
    {
        $ids = '';

        foreach ($urls as $item) {
            $ids .= ',' . $item['num_iid'];
        }

        $db = Db::name('tk_urls');

        $db->where('num_iid', 'in', $ids)->delete();

        $db->insertAll($urls);
    }
}
