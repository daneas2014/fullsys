<?php
namespace app\common\model;

use think\Model;
use think\Db;

class OpBase extends Model
{
    protected $name='op_questionnaire';

    /**
     * 记录调查问卷
     *        
     * @param unknown $subject            
     * @param unknown $productname       
     * @param unknown $uid          
     * @param unknown $mobile            
     * @param unknown $email            
     * @param string $posturl           
     * @param string rptoken 
     */
    public function saveOpquestionnaire($subject, $productname, $uid=0, $mobile='', $email='', $posturl = '',$rptoken='')
    {
        $data['create_time']=time();
        $data['rptoken']=$rptoken;
        
        if($this->get(['rptoken'=>$rptoken])){
            return true;
        }
        
        if($uid>0){
            $user=Db::name('Customer')->where('Id',$uid)->find();
            $data['subject']=$subject;
            $data['meanproduct']=$productname;
            $data['username']=$user['LastName'];
            $data['mobile']=$user['Mobile'];
            $data['email']=$user['Email'];
            $data['posturl']=$posturl;
        }
        else{
            $data['subject']=$subject;
            $data['meanproduct']=$productname;
            $data['username']='游客';
            $data['mobile']=$mobile;
            $data['email']=$email;
            $data['posturl']=$posturl;
        }
        
        return $this->insert($data);
    }
}