<?php
namespace app\common\model;

use think\Model;
use think\Db;

/**
 * 产品授权
 * 
 * @author Daneas
 *        
 */
class LicenseBase extends Model
{

    protected $name = 'product_license';

    
    /**
     * 以分组形式返回产品的license,用户产品授权页和在线订购页面
     * 
     * @param unknown $productid            
     * @return NULL
     */
    public function getProductLicense($productid, $public = 0)
    {
        $map['ProductId'] = $productid;
        // $map['Activitydeadline']=['>',date('Y-m-d')];
        $map['LicenseKey'] = ['like','EVS%'];
        $map['ListPrice']=['>',0];
        $list = $this->where($map)
            ->order('LicenseKey asc')
            ->select();
        
        if ($list == null) {
            return null;
        }
        
        $groups = $this->field('LicenseType,GroupName')
            ->where($map)
            ->group('LicenseType,GroupName')
            ->select();
        
        $array = [];
        
        foreach ($groups as $g) {
            $tg['GroupName'] = $g->GroupName;
            $tg['LicenseType'] = $g->LicenseType;
            
            $titems = [];
            foreach ($list as $item) {
                if ($item['GroupName'] == $g['GroupName']) {
                    
                    if($item['CurrencyType']==1){
                        $item['ListPrice']=$item['ListPrice']*10;
                        $item['Activityprice']=$item['Activityprice']*10;
                    }
                    
                    
                    if($item['CurrencyType']==2){
                        $item['ListPrice']=$item['ListPrice']*12;
                        $item['Activityprice']=$item['Activityprice']*12;
                    }
                    
                    if($item['CurrencyType']==3){
                        $item['ListPrice']=$item['ListPrice']*14;
                        $item['Activityprice']=$item['Activityprice']*14;
                    }
                    array_push($titems, $item);
                }
            }
            $tg['items'] = $titems;
            
            array_push($array, $tg);
        }
        
        return $array;
    }

    public function getLicense($productid = 0, $supplierid = 0, $page = 1, $limit = 20)
    {
        if ($supplierid > 0) {
            $list = $this->alias('a')
                ->field('a.*')
                ->join('Catalogproduct b', 'a.ProductId=b.Id', 'left')
                ->where('SupplierId', $supplierid)
                ->select();
            
            return $list;
        }
        
        return $this->where('ProductId', $productid)->select();
    }
        
    /**
     * 用于CRM更新license的时候使用【用的事务，但是仅限SQLSERVER 和 Mysql innoDb有效】
     * 功能故事：各展示价格大于0的属于正常，当展示价格为负数的时候前面展示咨询客服
     * 功能检查：当展示价格小于0，厂商活动价价格等于0的情况下有时候入库就入不了，经检查属于偶发性事件
     * 功能待定：目前屏蔽了展示价格小于0且厂商无报价的产品入库
     * 功能推荐：入库产品授权，有厂商价，但是因为某种原因需要询价的则展示价格为负
     * @param unknown $licensekey
     * @param unknown $licensename
     * @param unknown $productid
     * @param unknown $listprice
     * @param unknown $licenseremark
     * @param unknown $minqty
     * @param unknown $atypestr
     * @param unknown $active
     */
    public function updateLicense($licensekey,$licensename,$productid,$listprice,$licenseremark,$minqty,$atypestr,$active){
        
        $steps='LICENSE STEP1';
        
        /*
         * if($listprice<0 && $active['Supplierprice']<=0){
            return true;
        }
        */
        Db::startTrans();
        try{
            
            $dbl=Db::name('Catalogproductlicense');
            $dbh=Db::name('Catalogproductlicensechange');
            
            $license=$dbl->where(['LicenseKey'=>$licensekey,'ProductId'=>$productid])->find();            
            
            $history=$dbh->where(['LicenseKey'=>$licensekey,'ApprovalStatus'=>0])->find();
            
            $licenseid=0;
                        
            if(!$license){
                $license['LicenseType']=-1;
                $license['ListPrice']=-1;
                $license['Activitydeadline']='1990-1-1';
                $license['Activityprice']=-1;
                $license['ActivityType']='';
                $license['Supplierprice']=-1;
                $license['CurrencyType']=0;
                $license['Invoice']=0;
            }
            
            $license['LicenseKey']=$licensekey;
            $license['ProductId']=$productid;
            $license['GroupName']=$atypestr;
            $license['Name']=$licensename;
            $license['ListPrice']=$listprice;
            $license['Description']=$licenseremark;
            $license['Remark']=$licenseremark;
            $license['CreatedTime']=date('Y-m-d H:i:s');
            $license['QuantityStart']=$minqty;
            $license['QuantityEnd']=$minqty;
            $license['QuantityText']=$minqty;
            
            
            $license['LicenseType']=1;//<==========
            $license['EditorId']=8;//<==============
            $license['IsSupportOnlinePurchase']=0;            
            $license['withinvoce']=0;
            
            $invoice=0;
            
            if($active)
            {
                $license['Activityprice']=$active['Activityprice'];
                $license['Activitydeadline']='1990-01-01 00:00:00';
                $license['ActivityType']=$active['ActivityType'];
                $license['Supplierprice']=$active['Supplierprice'];
                
                // 20190626分销有个新的价格展现标准，传过来的新的展现价格，用于替换以前的老价格,这个价格含税
                $newprice=$active['newprice'];
                if($newprice&&$newprice>0){                    
                    $license['ListPrice']=$newprice;
                    $license['withinvoce']=1;
                }
                
                $invoice=$active['Invoice']?$active['Invoice']:0;
                if($invoice>1){
                    $invoice=ceil($invoice);                    
                    $license['Invoice']=$invoice;
                }                
                if(array_key_exists('validity', $active)){
                    $license['validity']=$active['validity'];//授权有效期类型
                }
            }
            
            if(array_key_exists('ROW_NUMBER', $license)){
                unset($license['ROW_NUMBER']);
            }
            
            if(!array_key_exists('Id', $license)){
                $licenseid=$dbl->insertGetId($license);
            }
            else{                
                $licenseid=$license['Id'];
                unset($license['Id']);
                $this->where('Id',$licenseid)->update($license);
            }
                        
            $steps='LICENSE STEP2';
            
            if(!$history)
            {
                $history['LicenseKey']=$licensekey;
                $history['ChangeReason']='CRM同步更新价格';
                $history['CreatedBy']='CRM同步';
                $history['LicenseId']=$licenseid;
                $history['ApprovalDate']=date('Y-m-d H:i:s');
            }
            else{
                $history['ActivitypriceChangeBefore']=0;                
            }
            
            $steps='LICENSE STEP3';
            
            
            $history['ListPriceBalance']=$listprice-$license['ListPrice'];
            $history['ListPriceChangeAfter']=$listprice;
            $history['ListPriceChangeBefore']=$license['ListPrice'];
            
            $history['ActivitydeadlineChangeBefore']=$license['Activitydeadline'];
            $history['ActivitydeadlineChangeAfter']=$active['Activitydeadline']?$active['Activitydeadline']:'1990-01-01';
            
            $steps='LICENSE STEP4';
            
            $history['ActivitypriceChangeBefore']=$license['Activityprice'];
            $history['ActivitypriceChangeAfter']=$active['Activityprice']?$active['Activityprice']:0;
            $history['ActivitypriceBalance']=$history['ActivitypriceChangeBefore']-$history['ActivitypriceChangeAfter'];
            
            
            $history['ActivityTypeChangeBefore']=$license['ActivityType'];
            $history['ActivityTypeChangeAfter']=$active['ActivityType'];
            
            $history['SupplierpriceChangeBefore']=$license['Supplierprice'];
            $history['SupplierpriceChangeAfter']=$active['Supplierprice']?$active['Supplierprice']:0;
            
            
            $steps='LICENSE STEP5';
            
            $history['CurrencyTypeChangeBefore']=$license['CurrencyType'];
            $history['CurrencyTypeChangeAfter']=$license['CurrencyType'];
            
            
            $history['InvoiceChangeBefore']=$license['Invoice'];
            
            $history['InvoiceChangeAfter']=$invoice;            
            
            $history['CreatedDate']=date('Y-m-d H:i:s');
            
            $steps='LICENSE STEP6';         
            
            if(array_key_exists('ROW_NUMBER', $history)){
                unset($history['ROW_NUMBER']);
            }
            
            if(!array_key_exists('Id', $history)){
                $licenseid=$dbh->insertGetId($history);       
            }
            else{
                $id=$history['Id'];
                unset($history['Id']);
                $dbh->where('Id',$id)->update($history);
            }
            
            $steps='LICENSE STEP7';
            
            // 提交事务
            Db::commit();
            
            return true;
        } catch (\Exception $e) {
            // 回滚事务
            Db::rollback();       
            
            return false;
        }
    }
    
    public function delLicense($licensekey){
        return $this->where('LicenseKey',$licensekey)->delete();
    }
}