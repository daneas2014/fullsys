<?php
namespace app\common\model;

use think\Model;

/**
 * 网站页面SEO获取
 *
 * @author dmakecn@163.com
 * @since 2019-12-17
 */
class SeoBase extends Model
{

    protected $name = 'sys_seo';

    /**
     * 获取页面的seo
     * @param unknown $module
     * @param unknown $controller
     * @param unknown $action
     * @return mixed|\think\cache\Driver|boolean|NULL|\app\common\model\SeoBase|NULL
     */
    public function pageseo($module, $controller, $action)
    {
        $key = 'seo' . $module . $controller . $action;

        $sysseo = dcache('PAGESEO', $key);

        if ($sysseo) {
            return $sysseo;
        }

        $sysseo = $this->get([
            'module' => $module,
            'controller' => $controller,
            'action' => $action,
        ]);
        dcache('PAGESEO', $key, $sysseo);

        return $sysseo;
    }
}
