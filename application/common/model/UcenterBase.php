<?php
namespace app\common\model;

use think\Model;
use think\Db;

/**
 * 用户注册、登录、第三方登录注册Model，例如QQ，微博，微信，支付宝，淘宝
 */
class UcenterBase extends Model{
    
    protected $name='customer';
    
    protected $autoWriteTimestamp = true;
    

    public function thirdSideLogin($OAuthType,$OAuthOpenId,$AccessToken,$nickname='',$img=''){
        $map['oauthtype']=$OAuthType;
        $map['oauthopenid']=$OAuthOpenId;
        
        $db=Db::name('customer_openidoauth');
        
        //1. 找信息，如果找到了返回用户信息
        $auth=$db->where($map)->find();
        if($auth){
            $db->where($map)->setField('accesstoken',$AccessToken);
            return ['isnew'=>0,'customerid'=>$auth['customerid']];
        }
                
        //2. 没有找到，获取用户资料，基本信息
        $api=null;
               
        $Costomer['headimage']=$img;
        $Costomer['nickname']=$nickname;
        $Costomer['update_time']=time();
        $Costomer['create_time']=time();
        $Costomer['delete_time']=0;
        $Costomer['accountlevel']=2;
        
        $customerid=$this->insertGetId($Costomer);
        
        $map['accesstoken']=$AccessToken;
        $map['customerid']=$customerid;
        
        $db->insertGetId($map);
        
        return ['isnew'=>1,'customerid'=>$customerid];    
    }
    
    /**
     * 普通注册写入数据
     */
    public function doRegister($data){
        try{
            $result =  $this->allowField(true)->save($data);
            if($result === false){
                return ['code' => -1, 'data' => '', 'msg' => $this->getError()];
            }else{
                return ['code' => 1, 'data' => $result, 'msg' => 'Register done'];
            }
        }catch( \Exception $e){
            return ['code' => -2, 'data' => '', 'msg' => $e->getMessage()];
        }
    }
    
    /**
     * 用户业务清单查询
     * @param  [type]  $type   [order,question,article,messge]
     * @param  array   $params [查询参数数组]
     * @param  integer $page   [index]
     * @param  integer $limit  [limit]
     * @return [type]          [返回list,count数组]
     */
    public function getCommonBusQuery($type,$params=[],$page=1,$limit=10){
        
        $act=$type.'Query';
        
        // 如果这个是MSSQL
        
        return $this->$act($params,$page,$limit);
        
    }
    
    
    private function orderQuery($params,$page,$limit){
        
    }
    
    
    private function querstionQuery($params,$page,$limit){
        
    }
    
    private function messageQuery($params,$page,$limit){
        
    }
    
    private function courseQuery($params,$page,$limit){
        
        $db=Db::name('series_access');
        
        $list=$db->where($params)->page($page,$limit)->select();
        
        $count=$db->where($params)->count();
        
        return ['list'=>$list,'count'=>$count];
    }
    
    /**
     * [getBusDetail 获取业务详情]
     * @param  [type] $type [order,question,article,messge]
     * @param  [type] $id   [业务id]
     * @return [type]       [返回$data]
     */
    public function getCommonBusDetail($type,$id){
        
        $act=$type.'Detail';
        
        return $this->$act($id);
    }
    
    
    private function orderDetail($id){
        
    }
    
    private function questionDetail($id){
        
    }

}