<?php
namespace app\common\model;

use think\Model;

class MemberBase extends Model
{

    protected $name = 'Customer';

    public function getMember($id)
    {
        return $this->where('Id', $id)->find();
    }
}