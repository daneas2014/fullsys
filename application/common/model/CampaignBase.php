<?php
namespace app\common\model;

use think\Model;

class CampaignBase extends Model
{

    protected $name = 'campaign';

    public function getCampaigns($type, $page, $limit)
    {
        return $this->field('id,title,keywords,imagepath,url')
            ->where('activetype', 'in', $type)
            ->page($page, $limit)
            ->order('id desc')
            ->select();
    }
}
