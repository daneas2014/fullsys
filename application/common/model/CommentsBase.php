<?php
namespace app\common\model;

use think\Model;

/**
 * 这个是评论的表，通过ModuleType区分是评论的什么鬼，通过TargetId来区分表的主见,1是视频，2是产品，3是文章,4是VIP视频
 *
 * @author Administrator
 *
 */
class CommentsBase extends Model
{
    
    protected $name = 'comments';
        
    // 开启自动写入时间戳
    protected $autoWriteTimestamp = true;
    
    /**
     * 获取视频的评论
     *
     * @param unknown $videoid
     * @param unknown $page
     * @param unknown $pagesize
     * @return unknown
     */
    public function getVideoComments($videoid, $page, $pagesize)
    {
        return $this->getComments(1, $videoid, $page, $pagesize);
    }
    
    /**
     * 获取产品的评论
     *
     * @param unknown $productid
     * @param unknown $page
     * @param unknown $pagesize
     * @return unknown
     */
    public function getProductComments($productid, $page, $pagesize)
    {
        return $this->getComments(2, $productid, $page, $pagesize);
    }
    
    /**
     * 获取文章的评论
     *
     * @param unknown $articleid
     * @param unknown $page
     * @param unknown $pagesize
     * @return unknown
     */
    public function getArticleComments($articleid, $page, $pagesize)
    {
        return $this->getComments(3, $articleid, $page, $pagesize);
    }
    
    /**
     * 获取VIP课程的评论
     * @param unknown $courseid
     * @param unknown $page
     * @param unknown $pagesize
     * @return \app\common\model\unknown
     */
    public function getVipVideoComments($courseid, $page, $pagesize)
    {
        return $this->getComments(4, $courseid, $page, $pagesize);
    }
    /**
     * 获取评论总方法
     *
     * @param unknown $type
     * @param unknown $targetid
     * @param unknown $page
     * @param unknown $pagesize
     * @return unknown
     */
    public function getComments($type, $targetid, $page, $pagesize)
    {
        $map['moduletype'] = $type;
        if ($targetid > 0) {
            $map['targetid'] = $targetid;
        }
        
        $list= $this->alias('a')
            ->field('a.*,b.nickname,b.headimage')
            ->join('customer b','a.customerid=b.id','left')
            ->where($map)
            ->page($page, $pagesize)
            ->order('id desc')
            ->select();
                    
        return $list;
    }
    
    /**
     * 获取一个分类的评论
     * @param unknown $cateid
     * @param unknown $page
     * @param unknown $pagesize
     */
    public function getProductCategoryComments($cateid,$page, $pagesize){
        $list=$this->alias('a')
        ->field('a.*,b.name')
        ->join('product b','a.targetid=b.id','left')
        ->where('categoryid',$cateid)
        ->page($page,$pagesize)
        ->select();
        
        return $list;
    }
    
    /**
     * 发布评论
     * @param unknown $authoid
     * @param unknown $moduletype
     * @param unknown $id
     * @param unknown $content
     */
    public function saveComment($authoid,$moduletype,$id,$content){
        $comment['moduletype']=$moduletype;
        $comment['targetid']=$id;
        $comment['customerid']=$authoid;
        $comment['authorname']='';
        $comment['userip']='';
        $comment['content']=$content;
        $comment['createtime']=date('Y-m-d H:i:s');
        $comment['accounttype']='customer';
        
        return $this->allowField(true)->save($comment);
    }
    
    /**
     * 对评论回复，成功了回复整条数据，失败了返回错误
     * @param unknown $authoid
     * @param unknown $id
     * @param unknown $content
     * @return number|\think\false|boolean
     */
    public function saveChildComment($authoid,$id,$content){
        
        $parent=$this->where('Id',$id)->find();
        
        $comment['moduletype']=$parent['ModuleType'];
        $comment['targetid']=$parent['TargetId'];
        $comment['customerid']=$authoid;
        $comment['authorname']='';
        $comment['userip']='';
        $comment['content']=$content;
        $comment['createtime']=date('Y-m-d H:i:s');
        $comment['accounttype']='customer';
        $comment['replyid']=$id;
        
        $result= $this->allowField(true)->save($comment);
        
        if($result){
            
            $comment=$this->alias('a')
            ->field('a.*,b.headimage,b.nickname')
            ->join('customer b','a.customerid=b.id','left')->find();
            
            return $comment;
        }
        
        return false;
    }
    
}
