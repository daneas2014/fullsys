<?php
namespace app\common\model;

use think\Db;
use think\Model;

/**
 * 下载、视频等资源管理组件
 *
 * @author dmakecn@163.com
 * @since 2019-12-27
 */
class ResourceBase extends Model
{

    protected $name = 'product_download';
    protected $deleteTime = 'delete_time';

    /**
     * 保存资源：文档，demo
     *
     * @param unknown $attach
     * @return number|string
     */
    public function saveAttach($attach)
    {
        $attach['filesize'] = str_replace(',', '', $attach['filesize']);

        if (array_key_exists('id', $attach) && $attach['id'] != '') {
            $id = $attach['id'];
            unset($attach['id']);
            return $this->where('id', $id)->update($attach);
        }
        unset($attach['id']);
        return $this->insertGetId($attach);
    }

    /**
     * 软删除附件,delete(true)是物理删除
     *
     * @param [type] $id
     * @return void
     * @author dmakecn@163.com
     * @since 2019-12-27
     */
    public function delAttach($id)
    {
        return $this->where('id', $id)->delete();
    }

    /**
     * 获得附件
     * @param unknown $attacid
     * @return unknown
     */
    public function getAttach($attacid)
    {
        return $this->alias('a')->field('a.*,b.name as productname')->join('product b', 'a.productid=b.id', 'left')->where('a.id', $attacid)->find();
    }

    /**
     *
     * @param unknown $key
     * @param number $type 0是试用，1是文档与帮助文件，4是其他，5是产品扩展
     * @param number $productid
     * @param number $page
     * @param number $limit
     * @param array $attr
     * @return unknown[]
     */
    public function getAttchs($key = '', $type = 0, $productid = 0, $page = 1, $limit = 20, $attr = [])
    {
        $map = [];
        if ($key && $key != '') {
            $map['a.name'] = ['like', '%' . $key . '%'];
        }
        if ($type > 0) {
            $map['downloadtype'] = $type;
        }
        if ($productid > 0) {
            $map['productid'] = $productid;
        }
        if ($attr != []) {
            array_push($map, $attr);
        }

        $list = $this->alias('a')
            ->field('a.*,b.name as productname')
            ->join('product b', 'a.productid=b.id', 'left')
            ->where($map)
            ->order('id desc')
            ->page($page, $limit)
            ->select();

        $count = $this->alias('a')->where($map)->count();

        return ['list' => $list, 'count' => $count];
    }

    /**
     * 获得视频
     *
     * @param [type] $id
     * @return void
     * @author dmakecn@163.com
     * @since 2019-12-27
     */
    public function getVideo($id)
    {
        return Db::name('video')->where('id', $id)->find();
    }

    /**
     * 保存视频
     *
     * @param [type] $video
     * @return void
     * @author dmakecn@163.com
     * @since 2019-12-27
     */
    public function saveVideo($video)
    {
        if (array_key_exists('id', $video) && $video['id'] != '') {
            $id = $video['id'];
            unset($video['id']);
            return Db::name('video')->where('id', $id)->update($video);
        }

        unset($video['id']);
        $video['createdtime'] = date('Y-m-d H:i:s');
        $video['clickcount'] = 0;
        $video['status'] = 1;
        return Db::name('video')->insertGetId($video);
    }

    /**
     * 软删除视频
     *
     * @param [type] $vid
     * @return void
     * @author dmakecn@163.com
     * @since 2019-12-27
     */
    public function delVideo($vid)
    {
        return Db::name('video')->where('id', $vid)->setField(['delete_time', time()]);
    }

    /**
     * 获取视频集
     * @param number $type
     * @param number $productid
     * @param number $page
     * @param number $limit
     * @return \think\Collection|\think\db\false|PDOStatement|string
     */
    public function getVideos($key, $productid = 0, $page = 1, $limit = 20)
    {
        $map = [];
        if ($key && $key != '') {
            $map['title'] = ['like', '%' . $key . '%'];
        }
        if ($productid > 0) {
            $map['productid'] = $productid;
        }

        $list = Db::name('video')->where($map)
            ->page($page, $limit)
            ->order('id desc')
            ->select();

        $count = Db::name('video')->where($map)->count();

        return ['list' => $list, 'count' => $count];
    }

}
