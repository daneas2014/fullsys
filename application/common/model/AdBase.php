<?php
namespace app\common\model;

use think\Model;

/**
 * 这个是以广告位为基础的广告查询
 *
 * @author Administrator
 *
 */
class AdBase extends Model
{
    protected $name = 'advertisments';

    public function getAdByKey($key)
    {
        $map['pkey'] = $key;
        $map['endtime'] = ['>', date('Y-m-d')];
        $map['begintime'] = ['<=', date('Y-m-d 00:00:00')];
        $map['isdisabled'] = 0;

        $adlist = $this->alias('a')
            ->field('a.*,b.type')
            ->join('advertisment_position b', 'a.positionid=b.id', 'left')
            ->where($map)
            ->order('sort desc')
            ->select();

        return $adlist;
    }

}
