<?php
namespace app\common\model;

use think\Model;
use think\Db;

class IntegralBase extends Model
{

    protected $name = 'customer_integral';
    
    protected $autoWriteTimestamp = true;
    
    /**
     * 业务逻辑后操作：账户积分增减，主要发生在签到、登录、主题帖、回帖
     * 
     * @param 用户id            
     * @param 积分正负数            
     * @param signin,login,thread,post
     * @param 原因            
     * @return boolean
     */
    public function integralProcess($customerid, $integral, $integraltype, $res)
    {
        // 1. 积分账户更新
        // 2. 积分日志写入
        // 3. 个人账户更新
        
        $account = $this->get(['customerid' => $customerid,'type'=>$integraltype]);
        
        if ($account == null) {
            $r1=$this->save(['type'=>$integraltype,'integral'=>$integral,'customerid'=>$customerid]);
        } else {
            $r1=$this->save(['integral'=>($account->integral + $integral)],['id'=>$account->id]);
        }
        
        if (! $r1) {
            return false;
        }
        
        $l1=Db::name('customer_integrallog')->insertGetId(['customerid'=>$customerid,'create_time'=>time(),'integral'=>$integral,'summary'=>$res]);
        if ($l1 <= 0) {
            return false;
        }
        
        return Db::name('customer')->where('id',$customerid)->update(['integral'=>Db::raw("integral+$integral")]);
    }
}