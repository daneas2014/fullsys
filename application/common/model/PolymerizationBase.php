<?php
namespace app\common\model;

use think\Model;
use think\Db;

/**
 * 话题
 *
 * @author Administrator
 *        
 */
class PolymerizationBase extends Model
{

    protected $name = 'topic';

    public function getTopics($limit = 10)
    {
        return $this->limit($limit)
            ->order('Id desc')
            ->select();
    }
    
    /**
     * 获得话题列表
     * @param unknown $type
     * @param unknown $supplierid
     * @param unknown $keyword
     * @param unknown $page
     * @param unknown $limit
     */
    public function getTopicList($type=0,$supplierid=0,$keyword='',$page=1,$limit=10)
    {
        $map=[];
        if($type>0){
            $map['Categoryid']=$type;
        }
        if($supplierid>0){
            $map['Supplierid']=$supplierid;
        }
        if($keyword!=''){
            $map['Name']=['like','%'.$keyword.'%'];
        }
        
        $list= $this->field('Id,Name,ImagePath,Logincheck,CreateTime,Categoryid,Supplierid,Context')->where($map)->order('Id desc')->page($page,$limit)->select();
        
        $count=$this->where($map)->count();
        
        return ['list'=>$list,'count'=>$count];
        
    }
    
    /**
     * 新的话题管理，取消了所有外检表
     * @param unknown $topicid
     * @return unknown
     */
    public function getTopicZone($id){
        
        $topic = $this->where('Id', $id)->find();
        
        if($topic==null){
            return null;
        }
        
        $partitions = Db::name('topic_partitionmodule')
        ->where('TopicId', $id)
        ->order('Id desc')
        ->select();
        
        $partitionslist =  Db::name('topic_partition')
        ->where('TopicId', $id)
        ->order('Id desc')
        ->select();
        
        $ma['TopicId']=$id;
        $ma['Otype']=1;
        $articles=Db::name('topic_partition')->alias('a')->field('a.Title,a.Url,a.imagepath,b.Summary,b.CreatedTime')->join('Article b','Oid=b.Id','inner')->where($ma)->limit(10)->select();
        $mv['TopicId']=$id;
        $mv['Otype']=3;
        $videos=Db::name('topic_partition')->where($mv)->limit(10)->select();
        
        $topic['articles'] = $articles;
        $topic['videos'] = $videos;
        $topic['partions'] = $partitions;
        $topic['partionslist'] = $partitionslist;
        
        return $topic;
    }
}