<?php
namespace app\common\model;

use think\Model;

/**
 * 发短信统一类
 * @author Administrator
 *
 */
class SmsBase extends Model
{
    private $sendto;

    public function __construct($sendto)
    {
        $this->sendto = $sendto;
    }

    /**
     * 模版类型:
     * 验证码
     * 模版名称:
     * 注册验证
     * 模版CODE:
     * SMS_106280055
     * 模版内容:
     * 你的验证码是：${code}
     * 申请说明:
     * 网站注册验证手机号码
     *
     * @param unknown $code
     */
    public function sendRegmsg($code)
    {
        return self::sendout('SMS_106280055', ['code' => $code]);
    }

    /**
     * 模版类型:
     * 短信通知
     * 模版名称:
     * 支付提醒
     */
    public function sendOrderpaid($productname)
    {
        return self::sendout('SMS_154960955', ['product' => $productname]);
    }

    /**
     * 模版类型:
     * 短信通知
     * 模版名称:
     * 在线订购发货提醒
     */
    public function sendOrderdeliver($productname)
    {
        return self::sendout('SMS_154960960', ['product' => $productname]);
    }

    /**
     * 模版类型:
     * 短信通知
     * 模版名称:
     * 变更账号密码
     */
    public function sendAccountchange($password)
    {
        return self::sendout('SMS_155270591', ['password' => $password]);
    }

    /**
     * 模版类型:
     * 短信通知
     * 模版名称:
     * 活动发券
     */
    public function sendCouponMsg($amount)
    {
        return self::sendout('SMS_167195147', ['amount' => $amount]);
    }

    private function sendout($tpcode, $params)
    {
        $mobile = $this->sendto; // 手机号

        if ($params) {
            foreach ($params as $key => $value) {
                $tplParam[$key] = $value; // 验证码
            }
        }

        $msgStatus = sendMsg($mobile, $params['tpcode'], $tplParam);

        if ($msgStatus['Message'] == 'OK' || $msgStatus['Code'] == 'OK') {
            return json(['code' => 1, 'data' => '', 'msg' => '短信已发送，请注意查收']);
        }

        return json(['code' => -1, 'data' => '', 'msg' => '发送失败，' . $msgStatus['Message']]);
    }
}
