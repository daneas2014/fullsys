<?php
namespace app\media\controller;

use app\common\model\CommentsBase;
use app\common\model\SeriesBase;
use app\index\controller\Base;
use Qiniu\Auth;
use think\Db;

class Series extends Base {

	//七牛的配置在cloud里面，如果没有配置就会报错
	protected $qiniuconfig = [];

	public function _initialize() {
		parent::_initialize();
		$this->qiniuconfig = config('cloud');
	}

	/**
	 * VIP资源
	 */
	public function index() {

		$db = Db::name('series_description');

		$cate = input('category') ? input('category') : 0;
		$free = input('type') ? input('type') : '';
		$kw = input('kw') ? input('kw') : '';

		$map = [];

		if ($cate > 0) {
			$map['categoryid'] = $cate;
		}
		if ($free == 'free') {
			$map['isfree'] = 1;
		}
		if ($free == 'vip') {
			$map['isfree'] = 0;
			$map['saleprice'] = ['>', 0];
		}
		if ($kw && $kw != '') {
			$map['title|keywords|description'] = ['like', '%' . $kw . '%'];
		}

		$list = $db->where($map)
			->order('id desc')
			->paginate(20, false, ['query' => ['category' => $cate, 'type' => $free, 'kw' => $kw]]);

		$count = $db->where($map)->count();

		$this->assign('page_method', $list->render());
		$this->assign('list', $list);

		return $this->fetch();
	}

	/**
	 * 视频清单
	 */
	function list() {
		$id = input('param.id');

		$vm = new SeriesBase();

		// 视频内容详情
		$course = $vm->getVideoSeries($id);

		// 最近三条
		$last3 = $vm->order('id desc')->limit(3)->select();

		// 课程评价
		$cm = new CommentsBase();
		$comments = $cm->getVipVideoComments($id, 1, 100);

		// 右侧推荐视频
		$rvideo = $vm->order('id desc')->limit(4)->select();

		$assign = [
			'title' => $course['course']->title,
			'keywords' => $course['course']->keywords,
			'description' => $course['course']->description,
			'rvideo' => $rvideo,
			'course' => $course['course'],
			'groups' => $course['groups'],
			'last' => $last3,
			'commentid' => $id,
			'comments' => $comments,
		];

		return $this->view->assign($assign)->fetch();
	}

	/**
	 * VIP视频播放
	 */
	public function video() {

		$vm = new SeriesBase();

		$id = input('param.id');

		$item = $vm->getVideoItems($id);

		$courseid = $item['courseid'];

		$course = $vm->getVideoSeries($courseid);

		$authcheck = false;

		if ($course['course']['isfree'] == 1 || $course['course']['saleprice'] < 1) {
			$authcheck = true;
		} else {
			$userid = 0;
			$userinfo = session('memberinfo');
			if ($userinfo != null) {
				$userid = $userinfo['Id'];
			}

			$authcheck = $vm->getUserAccess($userid, $courseid);
		}

		$playlist = Db::name('series_item')
			->field('id,title,description,resource,imagepath,caption')
			->where(['courseid' => $courseid, 'duration' => ['>', 0]])
			->select();

		$temp = [];
		$auth = new Auth($this->qiniuconfig['QINIU_ACCESSKEY'], $this->qiniuconfig['QINIU_SECRETKEY']);
		foreach ($playlist as $p) {
			// 处理视频路径
			if (!strstr($p['resource'], 'http')) {
				// 构建Auth对象
				// 私有空间中的外链 http://<domain>/<file_key>
				$baseUrl = $this->qiniuconfig['QINIU_DOMAIN'] . $p['resource'];
				// 对链接进行签名
				$signedUrl = $auth->privateDownloadUrl($baseUrl);

				$p['resource'] = $signedUrl;
			}

			$t['image'] = $p['imagepath'];
			$t['title'] = $p['title'];
			$t['videoId'] = $p['id'];
			$t['description'] = '<span>' . $p['description'] . '</span>';
			$t['sources'] = [['file' => $p['resource'], 'type' => 'mp4']];
			$t['tracks'] = [['file' => $p['caption'] == null ? "" : $this->qiniuconfig['QINIU_DOMAIN'] . $p['caption'], 'kind' => 'captions', 'label' => '中文', 'default' => true]];

			if ($p['id'] == $id) {
				array_unshift($temp, $t);
			} else {
				array_push($temp, $t);
			}
			unset($t);
		}

		$playliststr = json_encode($temp, JSON_UNESCAPED_UNICODE);

		$cm = new CommentsBase();
		$comments = $cm->getVipVideoComments($courseid, 1, 100);

		$items=[];
		foreach($course['groups'] as $g){
			array_push($items,$g);
		}

		$assign = [
			'title' => $item['title'] . '_' . $course['course']['title'],
			'keywords' => $item['keywords'],
			'description' => $item['description'],
			'item' => $item,
			'items' => $items,
			'course' => $course['course'],
			'groups' => $course['groups'],
			'commentid' => $courseid,
			'comments' => $comments,
			'authcheck' => $authcheck,
			'playlist' => $playliststr,
			'rvideo' => $vm->getHotPlay(5),
		];

		return $this->view->assign($assign)->fetch();
	}
}
