<?php
namespace app\media\controller;

use app\common\model\CatagoryBase;
use app\common\model\CommentsBase;
use app\index\controller\Base;
use app\index\model\ProductModel;
use think\Db;

class Video extends Base {

	/**
	 * 视频汇总页面
	 */
	public function index() {

		$videos = Db::name('video')->limit(12)->select();
		$series = Db::name('series_description')->limit(12)->select();

		$assign = [
			'title' => '视频主页',
			'keywords' => '',
			'description' => '',
			'videos' => $videos,
			'series' => $series,
		];

		return $this->view->assign($assign)->fetch();
	}

	/**
	 * 普通产品视频
	 */
	public function list() {
		$tdk = [
			'title' => '视频列表',
			'keywords' => '',
			'description' => '视频列表',
		];

		$db = Db::name('video');
		$field = 'video.id,video.title,video.description,video.duration,video.imagepath,video.headlinetime';

		$type = input('param.type');
		$id = input('param.id');
		$kw = input('param.kw');
		$catalogid = $supplierid = $productid = 0;
		switch ($type) {
		case 'category':
			$catalogid = $id;
			break;
		case 'supplier':
			$supplierid = $id;
			break;
		case 'product':
			$productid = $id;
			break;
		}

		if ($productid > 0) {
			$list = $db->where('productid', $productid)
				->order('id desc')
				->paginate(18, false, [
					'query' => [
						'kw' => $kw,
						'type' => $type,
						'id' => $id,
					],
				]);

			$pm = new ProductModel();
			$one = $pm::get([
				'id' => $productid,
			]);

			$tdk = [
				'title' => $one['name'],
				'keywords' => $one['name'],
				'description' => $one['name'],
			];

		}

		if ($supplierid > 0) {
			$list = $db->field($field)
				->join('product', 'product.id=video.productid')
				->where('supplierid', $supplierid)
				->order('id desc')
				->paginate(18, false, [
					'query' => [
						'kw' => $kw,
					],
				]);

			$pm = new ProductModel();
			$one = $pm->getSupplier($supplierid);

			$tdk = [
				'title' => $one['name'],
				'keywords' => $one['name'],
				'description' => $one['name'],
			];
		}

		if ($catalogid > 0) {
			$list = $db->alias('a')
				->field('a.*')
				->join('product b', 'a.productid=b.Id')
				->where('categoryid', $catalogid)
				->order('id desc')
				->paginate(18, false, [
					'query' => [
						'kw' => $kw,
					],
				]);

			$cm = new CatagoryBase();
			$one = $cm::get(['id' => $catalogid]);

			$tdk = [
				'title' => $one['name'],
				'keywords' => $one['name'],
				'description' => $one['name'],
			];
		}

		if ($productid == 0 && $catalogid == 0 && $supplierid == 0) {
			$map['headlinetime'] = ['>', 0];
			if ($kw && $kw != '') {
				$map['title'] = [
					'like',
					'%' . $kw . '%',
				];
			}

			$list = $db->field($field)
				->where($map)
				->order('id desc')
				->paginate(18, false, [
					'query' => [
						'kw' => $kw,
					],
				]);
		}

		$start = strtotime('-1 week');
		$weeklyhots = $db->alias('a')
			->join("(select videoid,count(videoid) as cc from video_history where create_time>$start group by videoid) b", 'a.Id=b.videoid', 'left')
			->order('cc desc')
			->limit(10)
			->select();

		$assign = ['weeklyhots' => $weeklyhots, 'page_method' => $list->render(), 'list' => $list, $tdk];

		return $this->assign($assign)->fetch();
	}

	/**
	 * 产品视频播放
	 */
	public function detail() {
		$db = Db::name('video');

		$id = input('param.id');

		$data = $db->where('id', $id)->find();

		if ($data == null) {
			return $this->redirect('/video');
		}

		$playurl = '';

		if (strstr($data['resource'], 'http')) {
			$playurl = $data['resource'];
		} else {
			$playurl = SITE_DOMAIN . $data['resource'];
		}

		$cm = new CommentsBase();
		$comments = $cm->getVideoComments($id, 1, 100);

		$start = strtotime('-1 week');
		$weeklyhots = $db->alias('a')
			->join("(select videoid,count(videoid) as cc from video_history where create_time>$start group by videoid) b", 'a.Id=b.videoid', 'left')
			->order('cc desc')
			->limit(10)
			->select();

		$assign=[
			'title' => $data['title'],
			'keywords' => $data['keywords'],
			'description' => $data['description'],
			'data' => $data,
			'playurl' => $playurl,
			'comments' => $comments,
			'commentid' => $id,
			'weeklyhots' => $weeklyhots,
		];

		return $this->assign($assign)->fetch();
	}
}