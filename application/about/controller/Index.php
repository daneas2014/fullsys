<?php
namespace app\about\controller;

use app\index\model\ArticleModel;
use app\about\model\IndexModel;
use app\index\controller\Base;

class Index extends Base
{

    public function index()
    {
        try {
            $action = input('param.type');
            
            if ($action == null) {
                $action = 'intro';
            }
            
            $title = '关于我们';
            $keyword = '公司简介';
            $description = '关于关于我们';
            
            switch ($action) {
                case 'intro':
                    $am = new ArticleModel();
                    $evnews = $am->getRelationArticles(8);
                    $this->assign('evnews', $evnews);

                    $mapapi = new \app\api\model\AreaModel();
                    $areas = $mapapi->loadAreas();
                    
                    $this->assign('citys', $areas);

                    $pid = input('provinceid')?input('provinceid'):4;
                    $cid = input('cityid')?input('cityid'):1;
                    $aid = input('areaid')?input('areaid'):6;
                    
                    $provincename = '';
                    $cityname = '';
                    $areaname = '';
                    
                    if ($pid >= 0) {
                        $provincename = $areas[$pid]['province_name'];
                    
                        $city = $areas[$pid]['city'];
                    
                        if ($cid >= 0) {
                            $cityname = $city[$cid]['city_name'];
                    
                            $area = $city[$cid]['area'];
                    
                            if ($aid >= 0) {
                                $areaname = $area[$aid];
                            }
                        }
                    }
                    
                    $seotitle = $provincename . $cityname . $areaname;
                    
                    $this->assign('cityname', strlen($seotitle) == 0 ? '重庆' : $seotitle);
                    $title=$seotitle.'网站建设|平台建设|小程序商城';
                    
                    break;
            }
            
            $this->assign([
                'title' => $title,
                'keywords' => $keyword,
                'description' => $description
            ]);
            
            return $this->fetch($action);
        } catch (\Exception $e) {
            return $this->error('该页面已下线，即将为您跳转到新的页面', '/about');
        }
    }
}

?>