<?php
namespace app\about\model;

use think\Model;
use think\Db;

/**
 * 
 */
class IndexModel extends Model
{
	protected $name = 'Catalogsupplier';

	public function getPartner($map){
		return $this->field('Id,Name,LogoPath')->where($map)->order('Id desc')->select();
	}
}

?>