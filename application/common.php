<?php

use Aliyun\Core\Config;
use org\QRcode;
use think\Cache;
use think\Db;

/**
 * 为了预防cache出错，做的判断
 * @param unknown tag前缀
 * @param unknown 名称
 * @param string $value 当不写value的时候是获取，写的时候是赋值
 * @return mixed|\think\cache\Driver|boolean|NULL
 */
function dcache($tag, $name, $value = '', $outtime = 3600) {

	if (config('cache_status') == 0) {
		return null;
	}

	try {
		if ($value == '') {
			return cache($name);
		}

		if ($tag) {
			return Cache::tag($tag)->set($name, $value, $outtime);
		}

		return cache($name, $value, $outtime);

	} catch (Exception $e) {
		return null;
	}
}

/**
 * 针对PHP7.4对null值的判断，传入对象，先检查对象是否为空，
 *
 * @param [type] $obj 对象
 * @param [type] $key 剑指
 * @param [type] $type int,string？
 * @return void
 * @author dmakecn@163.com
 * @since 2023-11-17
 */
function phpnullck($obj, $key, $type) {

	if ($obj != null) {
		return isset($obj[$key]) ? $obj[$key] : $type;
	}

	switch ($type) {
	case 'int':
		return 0;
	case 'string':
		return '';
	case 'datetime':
		return date('Y-m-d H:i:s');
	default:
		return $type;
	}

}

function getproductplatform($v) {

	if ($v == null || $v == '') {
		return '';
	}

	$my_string = substr($v, 1, strlen($v) - 2);

	return $my_string;
}

function debuggerfile($data, $path = '') {

	$filename = './debugger.txt';

	if ($path != '') {
		$filename = './' . $path . '.txt';
	}

	$handle = fopen($filename, "a+");
	$str = fwrite($handle, date('Y-m-d H:i:s') . "Start======\n" . $data . "\n");
	fclose($handle);
}

/**
 * 提取表格中某一列组成数组
 * @param unknown $table
 * @param unknown $column
 * @param string $arrayorjson
 * @param string $colunmtype string int
 * @return unknown|string
 */
function columtoarray($table, $column, $arrayorjson = true, $colunmtype = '') {
	if ($table == null) {
		return [];
	}
	if (count($table) == 0 || $table == []) {
		return [];
	}

	$colunmb = [];

	foreach ($table as $d) {
		if (array_key_exists($column, $d)) {
			switch ($colunmtype) {
			case 'int':
				array_push($colunmb, (int) $d[$column]);
				break;
			case '':
			default:
				array_push($colunmb, $d[$column]);
				break;
			}
		}
	}

	if ($arrayorjson == true) {
		return $colunmb;
	}

	return json_encode($colunmb);
}

/**
 * 组装条件
 * @param unknown $params
 * @return Ambigous <string, mixed>
 */
function getCond($params) {

	if ($params == null || $params == '') {
		return [];
	}

	$arry = explode(',', $params);

	if (!$arry) {
		return [];
	}

	$cond = null;
	foreach ($arry as $item) {

		if ($item == null || $item == '') {
			continue;
		}

		$temp = array();

		if (strpos($item, '=')) { //这个要被切割不能转义，换成下面那个

			$temp = explode('=', $item);
			$cond[$temp[0]] = $temp[1];

		} else if (strpos($item, '*')) { //上面要被切割不能转义，换成这个

			$temp = explode('*', $item);
			$cond[$temp[0]] = $temp[1];

		} else if (strpos($item, '-')) {

			$temp = explode('-', $item);
			$cond[$temp[0]] = ["<>", $temp[1]];

		} else if (strpos($item, '<')) {

			$temp = explode('<', $item);
			$cond[$temp[0]] = ["<", $temp[1]];

		} else if (strpos($item, '>')) {

			$temp = explode('>', $item);
			$cond[$temp[0]] = [">", $temp[1]];

		} else if (strpos($item, '!')) {

			$temp = explode('!', $item);
			$cond[$temp[0]] = ["like", '%' . $temp[1] . '%'];

		}

		unset($temp);
	}

	return $cond;
}

/**
 * 打包返回json：false是权限不够，true是返回值为真，-1是默认错误
 * @param unknown $data
 * @param string $msg
 * @return \think\response\Json
 */
function getJsonCode($data, $msg = "") {
	if ($data === false) {
		if ($msg == "") {
			$msg = "AUTHEN ERROR";
		}
		return json(['code' => -1, 'data' => '', 'msg' => $msg]);
	}

	if ($data === true) {
		if ($msg == "") {
			$msg = "SUCCESS";
		}
		return json(['code' => 1, 'data' => '', 'msg' => $msg]);
	}

	if ($data === -1) {
		if ($msg == "") {
			$msg = "DEFAULT ERROR";
		}
		return json(['code' => -1, 'data' => '', 'msg' => $msg]);
	}

	if ($data != null) {
		return json(['code' => 1, 'data' => $data, 'msg' => $msg]);
	} else {
		return json(['code' => 0, 'data' => '', 'msg' => '没有更多数据啦']);
	}
}

/**
 * 字符串截取，支持中文和其他编码
 */
function msubstr($str, $start = 0, $length, $charset = "utf-8", $suffix = true) {
	if (function_exists("mb_substr")) {
		$slice = mb_substr($str, $start, $length, $charset);
	} elseif (function_exists('iconv_substr')) {
		$slice = iconv_substr($str, $start, $length, $charset);
		if (false === $slice) {
			$slice = '';
		}
	} else {
		$re['utf-8'] = "/[\x01-\x7f]|[\xc2-\xdf][\x80-\xbf]|[\xe0-\xef][\x80-\xbf]{2}|[\xf0-\xff][\x80-\xbf]{3}/";
		$re['gb2312'] = "/[\x01-\x7f]|[\xb0-\xf7][\xa0-\xfe]/";
		$re['gbk'] = "/[\x01-\x7f]|[\x81-\xfe][\x40-\xfe]/";
		$re['big5'] = "/[\x01-\x7f]|[\x81-\xfe]([\x40-\x7e]|\xa1-\xfe])/";
		preg_match_all($re[$charset], $str, $match);
		$slice = join("", array_slice($match[0], $start, $length));
	}
	return $suffix ? $slice . '...' : $slice;
}

/**
 * 读取配置
 * @return array
 */
function load_config() {
	$list = Db::name('Sys_config')->select();
	$config = [];
	foreach ($list as $k => $v) {
		$config[trim($v['name'])] = $v['value'];
	}

	return $config;
}

/**
 * 验证手机号是否正确
 * @author honfei
 * @param number $mobile
 */
function isMobile($mobile) {
	if (!is_numeric($mobile)) {
		return false;
	}
	return preg_match('#^13[\d]{9}$|^14[5,7]{1}\d{8}$|^15[^4]{1}\d{8}$|^17[0,6,7,8]{1}\d{8}$|^18[\d]{9}$#', $mobile) ? true : false;
}

/**
 * 调用系统的API接口方法（静态方法）
 * api('User/getName','id=5'); 调用公共模块的User接口的getName方法
 * api('Admin/User/getName','id=5'); 调用Admin模块的User接口
 *
 * @param string $name
 *            格式 [模块名]/接口名/方法名
 * @param array|string $vars
 *            参数
 */
function api($name, $vars = array()) {
	$array = explode('/', $name);
	$method = array_pop($array);
	$classname = array_pop($array);
	$module = $array ? array_pop($array) : 'common';
	$callback = 'app\\' . $module . '\\Api\\' . $classname . 'Api::' . $method;
	if (is_string($vars)) {
		parse_str($vars, $vars);
	}
	return call_user_func_array($callback, $vars);
}

/**
 * 获取配置的分组
 *
 * @param string $group
 *            配置分组
 * @return string
 */
function get_config_group($group = 0) {
	$list = config('config_group_list');
	return $group ? $list[$group] : '';
}

/**
 * 获取配置的类型
 *
 * @param string $type
 *            配置类型
 * @return string
 */
function get_config_type($type = 0) {
	$list = config('config_type_list');
	return $list[$type];
}

// 发送短信(参数：签名,模板（数组）,模板ID，手机号)
function sms($signname = '', $param = [], $code = '', $phone) {
	$alisms = new taobao\AliSms();
	$result = $alisms->sign($signname)
		->data($param)
		->code($code)
		->send($phone);
	return $result['info'];
}

//生成网址的二维码 返回图片地址
function Qrcode($token, $url, $size = 8) {
	$md5 = md5($token);
	$dir = date('Ymd') . '/' . substr($md5, 0, 10) . '/';
	$patch = 'qrcode/' . $dir;
	if (!file_exists($patch)) {
		mkdir($patch, 0755, true);
	}
	$file = 'qrcode/' . $dir . $md5 . '.png';
	$fileName = $file;
	if (!file_exists($fileName)) {

		$level = 'L';
		$data = $url;
		QRcode::png($data, $fileName, $level, $size, 2, true);
	}
	return $file;
}

/**
 * 百度推广
 * @param unknown $array
 */
function pushbaidu($array) {

	$api = 'http://data.zz.baidu.com/urls?site=www.dmake.cn&token=xtXl0C4MQGBzOwfW';
	$ch = curl_init();
	$options = array(
		CURLOPT_URL => $api,
		CURLOPT_POST => true,
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_POSTFIELDS => implode("\n", $array),
		CURLOPT_HTTPHEADER => array(
			'Content-Type: text/plain',
		),
	);
	curl_setopt_array($ch, $options);
	$result = curl_exec($ch);
	curl_close($ch);

	$res = json_decode($result);

	return $res;
}

/**
 * 循环删除目录和文件
 * @param string $dir_name
 * @return bool
 */
function delete_dir_file($dir_name) {
	$result = false;
	if (is_dir($dir_name)) {
		$handle = opendir($dir_name);
		if ($handle) {
			while (false !== ($item = readdir($handle))) {
				if ($item != '.' && $item != '..') {
					if (is_dir($dir_name . DS . $item)) {
						delete_dir_file($dir_name . DS . $item);
					} else {
						unlink($dir_name . DS . $item);
					}
				}
			}
			closedir($handle);
			if (rmdir($dir_name)) {
				$result = true;
			}
		}
	}

	return $result;
}

/**
 * 图灵机器人问答
 *
 * @param unknown $words
 */
function tulingchat($words, $uid = 10000) {
	$api = 'http://www.tuling123.com/openapi/api';
	$appid = 'e16355116e2861889b0da27aeb6e6cc9';
	$secret = '453a1da888faf5b7';

	$array = array(
		'key' => $appid,
		'info' => $words,
		'secret' => $secret,
		'loc' => '重庆市铜梁区',
		'userid' => $uid,
	);

	$curl = curl_init();
	curl_setopt($curl, CURLOPT_HTTPHEADER, array(
		"Content-Type:application/json; charset=utf-8",
	));
	curl_setopt($curl, CURLOPT_URL, $api);
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
	curl_setopt($curl, CURLOPT_POST, 1);
	curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($array));
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	$result = curl_exec($curl);
	curl_close($curl);

	return $result;
}

/**
 * 抓取图片保存到本地
 *
 * @param unknown $url
 *            图片链接
 * @param string $filename
 *            不填写则根据url生成一个MD5文件，可防止重复下载
 * @param string $dirName
 *            不填写则保存到downimg里面
 * @param number $type
 *            空
 * @return boolean|string 返回文件路径
 */
function getImage($url, $filename = '', $dirName = null, $type = 0) {
	if ($url == '') {
		return false;
	}
	// 获取文件原文件名
	$defaultFileName = basename($url);
	// 获取文件类型
	$suffix = substr(strrchr($url, '.'), 1);

	// 设置保存后的文件名
	$filename = $filename == '' ? time() . '.' . $suffix : $defaultFileName;

	// 获取远程文件资源
	if ($type) {
		$ch = curl_init();
		$timeout = 5;
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
		$file = curl_exec($ch);
		curl_close($ch);
	} else {
		ob_start();
		readfile($url);
		$file = ob_get_contents();
		ob_end_clean();
	}
	// 设置文件保存路径
	if ($dirName == null) {
		$dirName = PUBLIC_PATH . 'attachment' . DS . 'downimg';
	}

	$subdir = date('Ym', time()) . '/' . date('d', time()) . '/';

	$dirName = $dirName . '/' . $subdir;

	if (!file_exists($dirName)) {
		mkdir($dirName, 0777, true);
	}
	// 保存文件
	$res = fopen($dirName . $filename, 'a');
	fwrite($res, $file);
	fclose($res);

	// return $dirName . $filename;//文件物理路径
	return '/attachment/downimg/' . $subdir . $filename;
}

/**
 * 伪造抬头抓取信息
 *
 * @param unknown $URL
 */
function curl_get_file_contents($URL) {
	$c = curl_init();
	curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
	// curl_setopt($c, CURLOPT_HEADER, 1);//输出远程服务器的header信息
	curl_setopt($c, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 2.0.50727;http://www.baidu.com)');
	// curl_setopt($c, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36');
	curl_setopt($c, CURLOPT_URL, $URL);
	$contents = curl_exec($c);
	curl_close($c);
	if ($contents) {
		return $contents;
	} else {
		return false;
	}
}

/**
 * 获取文件大小
 * @param unknown $url
 * @param string $user
 * @param string $pw
 * @return string|unknown
 */
function curl_getfilesize($url, $user = '', $pw = '') {
	ob_start();
	$ch = curl_init($url);
	curl_setopt($ch, CURLOPT_HEADER, 1);
	curl_setopt($ch, CURLOPT_NOBODY, 1);
	if (!empty($user) && !empty($pw)) {
		$headers = array('Authorization: Basic ' . base64_encode($user . ':' . $pw));
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	}
	$okay = curl_exec($ch);
	curl_close($ch);
	$head = ob_get_contents();
	ob_end_clean();
	$regex = '/Content-Length:\s([0-9].+?)\s/';
	$count = preg_match($regex, $head, $matches);
	$size = isset($matches[1]) ? $matches[1] : 'unknown';
	return $size;
}

/**
 * 模拟post进行url请求
 * @param string $url
 * @param string $param
 */
function request_post($url, $param = '') {

	if (empty($url) || empty($param)) {
		return false;
	}

	$postUrl = $url;
	$curlPost = $param;
	$headers = array(
		'User-Agent:dmake',
		'TICKET:dmakesystem',
	);

	$ch = curl_init(); //初始化curl
	curl_setopt($ch, CURLOPT_URL, $postUrl); //抓取指定网页
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ch, CURLOPT_HEADER, 0); //设置header
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //要求结果为字符串且输出到屏幕上

	// 以下两行不对https做证书及服务器校验，真实项目中请根据实际情况调整。
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);

	curl_setopt($ch, CURLOPT_POST, 1); //post提交方式
	curl_setopt($ch, CURLOPT_POSTFIELDS, $curlPost);
	$data = curl_exec($ch); //运行curl
	curl_close($ch);

	return $data;
}

// 发起一个http get请求，并返回请求的结果
// $url字段为请求的地址
// $param字段为请求的参数
function request_get($url, $param = array()) {
	if (empty($url) || empty($param)) {
		return false;
	}

	$getUrl = $url . "?" . http_build_query($param);
	$curl = curl_init(); // 初始化curl
	curl_setopt($curl, CURLOPT_URL, $getUrl); // 抓取指定网页
	curl_setopt($curl, CURLOPT_TIMEOUT, 1000); // 设置超时时间1秒
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1); // curl不直接输出到屏幕
	curl_setopt($curl, CURLOPT_HEADER, 0); // 设置header
	$data = curl_exec($curl); // 运行curl

	if (!$data) {
		print("an error occured in function request_get(): " . curl_error($curl) . "\n");
	}

	curl_close($curl);

	return $data;
}

function arraytostr($array, $key) {
	$r = array();

	if ($array == null) {
		return '';
	}

	foreach ($array as $k => $vo) {
		array_push($r, $vo[$key]);
	}

	return implode(',', $r);
}

/**
 * 子孙树 用于菜单整理
 *
 * @param array $param
 * @param int $pid
 */
function subTree($param, $pid = 0) {
	static $res = [];
	foreach ($param as $key => $vo) {

		if ($pid == $vo['pid']) {
			$res[] = $vo;
			subTree($param, $vo['id']);
		}
	}
	return $res;
}

/**
 * 分级子孙树
 *
 * @param [type] $param 传入数据队列
 * @param string $name 显示队列字段默认name
 * @param string $pidkey 上级的key名字默认pid
 * @param string $childkey 下级的key名字默认id
 * @param integer $pid
 * @param integer $lvl
 * @param string $split
 * @return void
 * @author dmakecn@163.com
 * @since 2023-11-27
 */
function subTreeLevel($param, $name = 'name', $pidkey = 'pid', $childkey = 'id', $pid = 0, $lvl = 0, $split = '-') {
	static $res = [];
	foreach ($param as $key => $vo) {
		if ($pid == $vo[$pidkey]) {
			$vo[$name] = str_repeat($split, $lvl) . $vo[$name];
			$res[] = $vo;
			$temp = $lvl + 1;

			subTreeLevel($param, $name, $pidkey, $childkey, $vo[$childkey], $temp);
		}
	}
	return $res;
}

/**
 * 把列表按照中子分类弄到主表中去排列
 * @param unknown $list
 * @return array
 */
function childList($list, $parentkey = 'pid', $childkey = 'id', $pid = 0) {
	$level = [];
	foreach ($list as $key => $vo) {
		if ($vo[$parentkey] == $pid) {
			$childs = childList1($list, $parentkey, $childkey, $vo[$childkey]);
			if ($childs != []) {
				$vo['children'] = $childs;
			}
			array_push($level, $vo);
		}
	}

	return $level;
}
/**
 * 覆写分类
 *
 * @param [type] $list
 * @param [type] $parentkey
 * @param [type] $childkey
 * @param [type] $pid
 * @return void
 * @author dmakecn@163.com
 * @since 2023-11-20
 */
function childList1($list, $parentkey, $childkey, $pid) {

	$param = $list;
	$level = [];
	foreach ($list as $key => $vo) {
		if ($vo[$parentkey] == $pid) {
			$childs = childList2($list, $parentkey, $childkey, $vo[$childkey]);
			if ($childs != []) {
				$vo['children'] = $childs;
			}
			array_push($level, $vo);
		}
	}

	return $level;
}

function childList2($list, $parentkey, $childkey, $pid) {

	$param = $list;
	$level = [];
	foreach ($list as $key => $vo) {
		if ($vo[$parentkey] == $pid) {
			array_push($level, $vo);
		}
	}

	return $level;
}

/**
 * 分级子孙树，UL列表中允许配置连接
 *
 * @param [type] $param
 * @param string 文本
 * @param string 父键
 * @param string 主键
 * @param integer 父级值
 * @param string 例如 href="/doclib/s/%u"
 * @return void
 * @author dmakecn@163.com
 * @since 2020-11-11
 */
function subTreeUlLevel($param, $name = 'name', $pidkey = 'pid', $id = 'id', $parentvalue = 0, $href = '') {
	$str = '';

	for ($i = 0; $i < count($param); $i++) {

		$vo = $param[$i];
		$thref = '';
		$tul = '';

		if ($parentvalue == $vo[$pidkey]) {
			$thref = sprintf($href, $vo[$id]);
			$tul = "<ul>" . subTreeUlLevel($param, $name, $pidkey, $id, $vo[$id], $href) . "</ul>";
			$tul = str_replace('<ul></ul>', '', $tul);
			$str .= "<li><a data-id='" . $vo[$id] . "'" . $thref . ">" . $vo[$name] . "</a>" . $tul . "</li>";
		}
	}

	return $parentvalue == 0 ? '<ul>' . $str . '</ul>' : $str;
}

/**
 * 获取文件夹列表<ul>带多级分类
 * 原方法 https://www.cnblogs.com/xhen/p/9885578.html
 *
 * @param [type] $date
 * @return void
 * @author dmakecn@163.com
 * @since 2020-12-03
 */
function list_file($date) {
	$str = '';
	$htmls = '';
	//1、首先先读取文件夹
	$temp = scandir($date);

	$str .= "<ul>";
	//遍历文件夹
	foreach ($temp as $v) {
		$a = $date . '/' . $v;
		if (is_dir($a)) { //如果是文件夹则执行

			if ($v == '.' || $v == '..') { //判断是否为系统隐藏的文件.和..  如果是则跳过否则就继续往下走，防止无限循环再这里。
				continue;
			}

			$htmls = list_file($a); //因为是文件夹所以再次调用自己这个函数，把这个文件夹下的文件遍历出来;
			$htmls = str_replace('<ul></ul>', '', $htmls);
			$str .= "<li><b>$v</b> $htmls</li>"; //把文件夹红名输出

		} else {
			$str .= "<li><a data-dir='$date' data-path='$a'>$v</a></li>";
		}

	}
	$str .= "</ul>";

	return $str;
}

/**
 * 检查是否有屏蔽关键词，是否替换
 *
 * @param unknown $detail
 * @param unknown $ishide
 *            参数为false的时候返回是否包含关键词true/false
 */
function check($content, $ishide = true) {
	$str = "/你大爷|你麻痹|什么玩意|SB|你他妈/"; // 关键字正则字符串

	if ($ishide == true) {
		preg_replace($str, "*", $content); // preg_replace() 执行一个正则表达式的匹配和替换
		return false;
	} else {
		return strPosFuck($content);
	}
}

/**
 * 检验是否有屏蔽词，有的话就返回true，没得返回false
 *
 * @param unknown $content
 * @return boolean
 */
function strPosFuck($content) {
	$fuckArr = include 'shield.php';
	for ($i = 0; $i < count($fuckArr); $i++) {

		if ($fuckArr[$i] == "") {
			continue; // 如果关键字为空就跳过本次循环
		}
		if (strpos($content, $fuckArr[$i]) != false) {
			return true; // 如果匹配到关键字就返回关键字
		}
	}
	return false; // 如果没有匹配到关键字就返回 false
}

/*
 * 判断是什么浏览器
 */
function client_get_browser() {
	if (!key_exists('HTTP_USER_AGENT', $_SERVER)) {
		return false;
	}
	$agent = $_SERVER['HTTP_USER_AGENT'];
	if (empty($agent)) {
		return false;
	}
	if (false !== strpos($agent, 'MicroMessenger')) {
		return 'Wechat';
	}
	if (false !== strpos($agent, 'MSIE')) {
		return 'Internet Explorer';
	}
	if (false !== strpos($agent, 'Firefox')) {
		return 'Firefox';
	}
	if (false !== strpos($agent, 'Chrome')) {
		return 'Chrome';
	}
	if (false !== strpos($agent, 'Safari')) {
		return 'Safari';
	}
	if (false !== strpos($agent, 'Opera')) {
		return 'Opera';
	}
	if (false !== strpos($agent, '360SE')) {
		return '360SE';
	}
	if (false !== strpos($agent, 'Edge')) {
		return 'Edge';
	}
	return 'UNKNOWN';
}

/**
 * 过滤get参数
 * @param unknown $key
 * @param unknown $length
 * @return string
 */
function input_g($key, $length = null) {
	$str = $_GET[$key];
	$old = $str;
	$farr = array(
		"/<(\\/?)(script|alert|submit|function|post|i?frame|style|html|body|title|link|meta|object|textarea|\\?|\\%)([^>]*?)>/isU",
		"/(<[^>]*)on[a-zA-Z]+\s*=([^>]*>)/isU",
		"/select\b|concat\b|insert\b|update\b|delete\b|drop\b|;|\"|\'|\/\*|\*|\.\.\/|\.\/|union|into|load_file|outfile|dump/is",
	);

	$str = preg_replace($farr, '', strtolower($str));
	if ($str !== $old) {
		echo 'Wops';
		exit();
	}

	$str = strip_tags($str);
	if ($str !== $old) {
		echo 'Wops';
		exit();
	}

	if ($length != null && strlen($str) > $length) {
		echo 'Wops';
		exit();
	}

	return $str;
}

/*
 * 过滤脚本
 */
function filterWords(&$str) {
	$farr = array(
		"/<(\\/?)(script|i?frame|function|style|html|body|title|link|meta|object|\\?|\\%)([^>]*?)>/isU",
		"/(<[^>]*)on[a-zA-Z]+\s*=([^>]*>)/isU",
		"/select\b|insert\b|update\b|delete\b|drop\b|;|\"|\'|\/\*|\*|\.\.\/|\.\/|union|into|load_file|outfile|dump/is",
	);
	$str = preg_replace($farr, '', $str);
	$str = strip_tags($str);
	return $str;
}

/**
 *计算一个目录文件大小方法
 *$dirfile:传入文件目录名
 **/
function dirSize($dirfile) {
	$dir_size = 0;
	$handle = @opendir($dirfile);
	if ($handle) {
		$filename = readdir($handle);
		while ($filename) {
			if ($filename != '.' && $filename != '..') {
				$subfile = $dirfile . '/' . $filename;
				if (is_dir($subfile)) {
					$dir_size += dirSize($subfile); //递归再次调用
				}
				if (is_file($subfile)) {
					$dir_size += filesize($subfile);
				}
			}
		}
		closedir($handle);
		return $dir_size;
	}
}

/**
 * 获取文件夹尺寸
 * @param unknown $dirfile
 */
function getDirSize($dirfile) {
	intval($dir_size = dirSize($dirfile));

	if ($dir_size > 1024 * 1024 * 1024) {
		echo round($dir_size / 1024 / 1024 / 1024, 2) . "GB";
	} elseif ($dir_size > 1024) {
		echo round($dir_size / 1024 / 1024, 2) . "MB";
	} else {
		echo round($dir_size / 1024, 2) . "KB";
	}
}

/**
 * 向系统中所有模块、插件返回用户信息（对象），用null判断是否存在
 * 这样便于各模块开发者使用信息
 *
 * @return void
 * @author dmakecn@163.com
 * @since 2022-04-26
 */
function getUserInfo() {
	if (session("memberinfo")) {
		return session('memberinfo');
	}

	return null;
}
