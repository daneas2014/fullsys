<?php
namespace app\sites\controller;

use think\Db;
use think\Controller;

use app\index\model\ProductModel;
use app\index\model\ArticleModel as iarticle;
use app\index\model\AdModel;
use app\index\model\ResourceModel;
use app\common\model\SupplierModel;
use app\common\model\LicenseModel;
use app\sites\model\DataModel;

class Index extends Controller
{        
    
    private function getsite(){
        $domain = $_SERVER['HTTP_HOST'];
        $key =  'C_' . $domain;
        
        $result = dcache(CACHE_SITES ,$key);
        
        if (! $result) {
            $result = Db::name('sites')->where('domain', $domain)->find();
            dcache(CACHE_SITES ,$key, $result);
        }
        
        return $result;
    }
    
    public function _initialize()
    {
        $result=$this->getsite();        
        $this->assign('site', $result);
    }    
	
	
    public function adhtml()
    {
        $pkey = input('get.pkey');
        
        if($pkey==null||$pkey==''){
            echo '';
            exit();
        }
                
        $ad = new AdModel();
        
        $list = $ad->getAdHtml($pkey);        

        echo $list;
    }
    
    /**
     * 首页
     */
    public function index()
    {                 
        $rule=$this->getSupplier();       
        
        $site=$this->getsite();
        
        $am=new AdModel();
        
        $indexad=$am->getAd($rule['temp']);
                
        $sm=new SupplierModel();
        
        $supplier=$sm->getSupplier($rule['supplierid']);
                
        $pm=new ProductModel();
        
        $products=$pm->getProductList(0, $rule['supplierid']);                
        
        $am = new DataModel();
        
        $articles = $am->getArticles($rule['supplierid'], 'Id desc');
        
        $vm=new ResourceModel();
        
        $videos=$vm->videos(0,$rule['supplierid'],0,1, 6, 'Id desc');        
        
        $this->assign(['supplier'=>$supplier,'product'=>$products,'article'=>$articles,'indexad'=>$indexad,'videos'=>$videos]);
        
        $this->assign(['title'=>$site['title'],'keywords'=>$site['keywords'],'description'=>$site['description']]);
                
        return $this->fetch($rule['temp'].'/index');
    }
    
    public function channel(){
        
        $code=input('param.code');
        
        $rule=$this->getSupplier();
        
        if($code){
            $one=Db::name('sites_channel')->where(['channel'=>$code,'siteid'=>$rule['siteid']])->find();
            $this->assign('data',$one);
            $this->assign(['title'=>$one['title'],'keywords'=>$one['keywords'],'description'=>$one['description']]);
            return $this->fetch($rule['temp'].'/channel');
        }
        
    }

    /**
     * 资讯类
     */
    public function news()
    {
        $id = input('param.id');
        $map['IsPublish']=1;
        if($id&&$id>0){
            $map['SubcategoryId']=$id;
        }
        
        $rule=$this->getSupplier();
        
        $db=Db::name('Article');
        
        $list=$db->alias('a')
            ->field('a.Id,a.Title,a.ImagePath,a.CreativeType,a.CreatedTime,Summary,c.ClickCount')
            ->join('(select DISTINCT(articleid) from Articlerelations where supplierid='.$rule['supplierid'].' group by articleid) b','a.Id=b.articleid','right')
            ->join('ArticleAttribute c', 'c.ArticleId=a.Id', 'inner')
            ->where($map)
            ->order('Id desc')
            ->paginate(20,false,['query'=>['id'=>$id]]);
                
        $this->assign('page_method', $list->render());
        $this->assign('list', $list);
        
        $this->assign(['title'=>$rule['temp'].'最新教程','keywords'=>$rule['temp'].'最新教程','description'=>$rule['temp'].'最新教程和产品动态。']);
        
        return $this->fetch($rule['temp'].'/news');
    }

    /**
     * 文章详细
     */
    public function newsdetail()
    {
        $rule=$this->getSupplier();
        
        $id=input('param.id');

        $ar = new iarticle();
        
        $dm=new DataModel();
        
        $data = $dm->getArticleDetail($id,$rule['supplierid']);
        
        if ($data == null) {
            return redirect('/index/news');
        }

        $subcategoryid = $data['SubcategoryId'];
        $rarticles = $dm->getRelationArticles($rule['supplierid'],$subcategoryid, 10, null);
        
        
        $this->assign(['title'=>$data['Title'],'keywords'=>'','description'=>'']);
        $this->assign([
            'rarticles' => $rarticles,
            'data' => $data,
            'rgroups' => $ar->getArticleGroups($data['GroupId'],20,$id)
        ]);
        
        return $this->fetch($rule['temp'].'/newsdetail');
    }

    /**
     * 本页将所有的产品都遍历出来，详情页跳转
     */
    public function products()
    {        
        $rule=$this->getSupplier();
        
        $db=Db::name('Catalogproduct');
        $map=['SupplierId'=>$rule['supplierid'],'IsDisabled'=>0,'IsPublish'=>1];
        $kw=input('param.kw');
        if($kw && $kw != ''){
            $map['Name']=['like','%'.$kw.'%'];
        }
        $list=$db->field('*')
            ->where($map)
            ->paginate(20);
            

        $id=$rule['supplierid'];

        $this->assign('page_method', $list->render());
        $this->assign('list', $list);
        $this->assign('categoryid',$id);
        
        $this->assign(['title'=>$rule['temp'].'最新产品列表','keywords'=>'','description'=>'']);
        
        return $this->fetch($rule['temp'].'/products');
    }

    /**
     * 本页将所有的产品都遍历出来，详情页跳转
     */
    public function product()
    {        
        $rule=$this->getSupplier();
        $id=input('param.id');
        $pm = new ProductModel();
        $product = $pm->getProductCommon($id);
        
        if($product==null){
            return $this->error('产品不存在，即将为您跳转到产品首页','/index/products');
        }
        
        $map=['SupplierId'=>$rule['supplierid'],'IsDisabled'=>0,'IsPublish'=>1];
        $rproduct=$pm->field('Id as productid,Name,DescriptionWord')->where($map)->select();
        
        $this->assign([
            'product'=>$product,
            'images' => $pm->getProductImages($product->Id),
            'rproducts' => $rproduct,
        ]);

        $this->assign(['title'=>$product['Name'].'产品详情功能描述','keywords'=>$product['Keywords'],'description'=>$product['Summary']]);
        
        return $this->fetch($rule['temp'].'/product');
    }
    /**
     * 视频聚合页
     */
    public function videos()
    {        
        $rule=$this->getSupplier();
        $kw=input('param.kw');
        $map=[];
        $map['SupplierId']=$rule['supplierid'];
        if($kw&&$kw!=''){
            $map['Title']=['like','%'.$kw.'%'];
        }

        $db=Db::name('video');
        $list = $db->field('Video.*')
            ->join('CatalogProduct', 'CatalogProduct.Id=video.productid')
            ->where($map)
            ->order('Id desc')
            ->paginate(16);

        $this->assign([
            'videos'=>$list,
            'page_method'=>$list->render()
        ]);

        $this->assign(['title'=>$rule['temp'].'视频教学','keywords'=>'','description'=>'']);
        
        return $this->fetch($rule['temp'].'/videos');
    }
    /**
     * 视频详情页
     */
    public function video()
    {        
        $rule=$this->getSupplier();

        $db = Db::name('video');
        $id = input('param.id');
        
        $data = $db->where('Id', $id)->find();

        $playurl = '';
        
        if (strstr($data['Resource'], 'http')) {
            $playurl = $data['Resource'];
        } else {
            $playurl = "/index/videos" . $data['Resource'];
        }

        $this->assign([
            'title' => $data['Title'].'-'.$rule['temp'].'视频教学',
            'keywords'=> $data['SEOKeywords'],
            'description'=>$data['SEODescription'],
            'data' => $data,
            'playurl' => $playurl,
        ]);
        
        return $this->fetch($rule['temp'].'/video');
    }

    /**
     * 本页将程序下载、demo、视频分类展现
     */
    public function resource()
    {
        
        $rule=$this->getSupplier();        
        $supplierid=$rule['supplierid'];
        $type=input('param.type');
        $pageindex=0;
        
        if($type=='download'){           
            
            $db=Db::name('Catalogproductdownload');
            
            $list= $db->alias('a')
                ->field('a.*,b.Name as ProductName')
                ->join('Catalogproduct b','a.ProductId=b.Id','left')
                ->where('SupplierId', $supplierid)
                ->order('UpdateTime desc')
                ->paginate(20,false,['query'=>['type'=>$type]]);
            
                $this->assign('page_method',$list->render());
        }
        
        if($type=='demo'){           
            
            $db=Db::name('Productresource');
            
            $list= $db->alias('a')
                ->field('a.*,b.Name as ProductName')
                ->join('Catalogproduct b','a.productid=b.Id','left')
                ->where('SupplierId', $supplierid)
                ->order('Id desc')
                ->paginate(20,false,['query'=>['type'=>$type]]);
            
                $this->assign('page_method', $list->render());
        }
        
        if($type==null){
            $pageindex=1;
            
            $list['attach']=Db::name('Catalogproductdownload')->alias('a')
                            ->field('a.*')
                            ->join('Catalogproduct b','a.ProductId=b.Id','left')
                            ->where('SupplierId', $supplierid)
                            ->order('Id desc')
                            ->limit(15)
                            ->select();
            $list['demo']=Db::name('Productresource')->alias('a')
                            ->field('a.*')
                            ->join('Catalogproduct b','a.productid=b.Id','left')
                            ->where('SupplierId', $supplierid)
                            ->order('Id desc')
                            ->limit(15)
                            ->select();
            
        }
        
        $this->assign(['title'=>$rule['temp'].'支持中心，试用版本和Demo模板免费下载','keywords'=>$rule['temp'].'最新试用下载','description'=>$rule['temp'].'最新试用下载，更多DEMO和演示助您快速学习。']);
        $this->assign('list', $list);
        $this->assign('type',$type);
        $this->assign('pageindex',$pageindex);
        return $this->fetch($rule['temp'].'/resource');
    }
    
    /**
     * 本页将所有授权方式展现，购买跳转
     */
    public function license()
    {        
        $rule=$this->getSupplier();        
        
        $pm=new ProductModel();
        
        $products=$pm->getProductList(0, $rule['supplierid']);
        
        $lm=new LicenseModel();
        
        $dm=new DataModel();
        
        $license=$dm->getLicense($rule['supplierid']);        
        
        $this->assign(['product'=>$products,'license'=>$license]);
        
        $this->assign(['title'=>$rule['temp'].'正版授权购买','keywords'=>$rule['temp'].'授权,'.$rule['temp'].'购买','description'=>$rule['temp'].'正版授权购买请认准'.$rule['temp'].'中文网，价格实惠售后有保障！']);
        
        return $this->fetch($rule['temp'].'/license');
    }

    
    private function getSupplier(){               
        
        $rules = [
            [
                'siteid'=>1,
                'supplierid' => 1,
                'temp' => 'chiyan',
                'domain' => 'www.chiyan.top'
            ],
        ];
        
        $domain=$_SERVER['HTTP_HOST'];
        
        foreach ($rules as $rule){
            if($rule['domain']==$domain){
                return $rule;
            }
        }
    }
    
}