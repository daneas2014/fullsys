<?php
namespace app\sites\controller;

use think\Controller;
use think\Db;

class Base extends Controller
{

    public function _initialize()
    {
        $domain = $_SERVER['HTTP_HOST'];
        $key = CACHE_SITES . 'COMMON_' . $domain;
        
        $result = cache($key);
        if (! $result) {
            $site = Db::name('sites')->where('domain', $domain)->find();
            cache($key, $result);
        }
        
        $this->assign('site', $result);
    }
    
}