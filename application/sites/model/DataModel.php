<?php
namespace app\sites\model;

use app\common\model\ArticleModel;
use think\Db;
use think\Model;

class DataModel extends Model
{
    /**
     * 查找文章
     *
     * @param unknown $siteid
     * @param unknown $page
     * @param unknown $pagesize
     * @param unknown $orderby
     */
    public function getArticles($supplierid, $orderby, $page = 1, $pagesize = 10)
    {
        $list = Db::name('articlerelations')->alias('a')
            ->field('DISTINCT(b.Id),b.Title,b.ImagePath,Summary,CreatedTime,SubcategoryId')
            ->join('Article b', 'a.articleid=b.Id', 'left')
            ->where('a.supplierid', $supplierid)
            ->order($orderby)
            ->page($page, $pagesize)
            ->select();

        return $list;
    }

    /**
     * 获取站点文章
     *
     * @param unknown $siteid
     * @param unknown $articleid
     */
    public function getArticle($articleid)
    {
        $article = Db::name('article')->where('Id', $articleid)->find();

        return $article;
    }

    /**
     * 根据站点信息获得供应商信息
     *
     * @param unknown $siteid
     */
    public function getSupplier($siteid)
    {
        return $this->alias('a')
            ->join('CatalogSupplier b', 'a.supplierid=b.Id')
            ->find();
    }

    /**
     * 根据sites获取
     *
     * @param unknown $siteid
     */
    public function getProducts($siteid)
    {
        return $this->alias('a')
            ->field('b.*')
            ->join('CatalogProduct b', 'a.supplierid=b.SupplierId')
            ->where('siteid', $siteid)
            ->select();
    }

    /**
     * 根据sites获取
     *
     * @param unknown $supplierid
     */
    public function getLicense($supplierid)
    {
        return Db::name('Catalogproductlicense')->alias('a')
            ->field('a.*')
            ->join('CatalogProduct b', 'a.ProductId=b.Id', 'left')
            ->where(['supplierid' => $supplierid, 'LicenseKey' => ['like', 'EVS%'], 'ListPrice' => ['>', 0]])
            ->order('GroupName asc')
            ->select();
    }

    /**
     * 获取文章详情，包含了Attr，Creator，Siblings
     *
     * @param unknown $articleid
     * @param unknown $supplerid
     * @return mixed|\think\cache\Driver|boolean|unknown[]
     */
    public function getArticleDetail($articleid, $supplerid)
    {
        $key = 'GAD1S_' . $articleid . '-' . $supplerid;

        $detail = dcache(CACHE_SITES_A, $key);

        if ($detail) {
            return $detail;
        }

        $oa = new ArticleModel();

        $detail = $oa->getArticle($articleid);

        if ($detail == null) {
            return null;
        }

        $detail['Attr'] = $oa->getArticleAttr($articleid);

        $left = Db::query(sprintf('select top 1 a.Id,Title,CreatedTime from Article a inner join ArticleRelations b on a.Id=b.articleid where  a.Id<%d and supplierid=%d  order by a.Id desc', $articleid, $supplerid));
        $right = Db::query(sprintf('select top 1 a.Id,Title,CreatedTime from Article a inner join ArticleRelations b on a.Id=b.articleid where  a.Id>%d and supplierid=%d  order by a.Id desc', $articleid, $supplerid));

        $detail['pre'] = $left ? $left[0] : null;
        $detail['next'] = $right ? $right[0] : null;

        dcache(CACHE_SITES_A, $key, $detail);

        return $detail;
    }

    /**
     * 获取相关文章，和主站的区别在于这里全都是和厂商相关的内容
     * @param unknown $supplierid
     * @param number $subcategoryId
     * @param number $limit
     * @param unknown $keywords
     * @param string $order
     * @param number $page
     * @return mixed|\think\cache\Driver|boolean|NULL|NULL|\think\Collection|\think\db\false|PDOStatement|string
     */
    public function getRelationArticles($supplierid, $subcategoryId = 0, $limit = 10, $keywords = null, $order = 'Id desc', $page = 1)
    {
        $key = 'RA_' . $supplierid . '_' . $subcategoryId . '_' . $limit . '_' . $keywords . '_' . $page;

        $articles = dcache(CACHE_SITES_A, $key);
        if ($articles) {
            return $articles;
        }

        $map['supplierid'] = $supplierid;
        $map['IsPublish'] = 1;
        if ($subcategoryId > 0) {
            $map['SubcategoryId'] = $subcategoryId;
        }
        if ($keywords != null) {
            $map['Title'] = [
                'like',
                '%' . $keywords . '%',
            ];
        }

        $articles = Db::name('article')->alias('a')
            ->field('a.Id,Title,CreativeType,CreatedTime,SubcategoryId,ImagePath')
            ->join('ArticleRelations b', 'b.articleid=a.Id', 'inner')
            ->where($map)
            ->page($page, $limit)
            ->order($order)
            ->select();

        if ($articles == null) {
            return null;
        }

        dcache(CACHE_SITES_A, $key, $articles);

        return $articles;
    }
}
