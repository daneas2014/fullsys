<?php
namespace app\commodity\controller;

use app\common\model\CommentsBase;
use app\common\model\ProductBase;
use app\common\model\SeriesBase;
use app\common\model\SupplierBase;
use app\index\controller\Base;
use app\index\model\AdModel;
use app\index\model\ArticleModel;
use app\index\model\ForumModel;
use app\index\model\ProductModel;
use app\index\model\SourceModel;
use think\Db;

class Index extends Base
{

    /**
     * 首页
     */
    public function index()
    {
        $ad = new AdModel();

        $indexad = $ad->getAd('product-a1');
        $this->assign('indexad', $indexad);

        $cp = new ProductBase();

        $hotrelate = $cp->getHotRalateProducts(3);

        $product = new ProductModel();
        $online['all'] = $product->getOnlineSaleProducts(0, 6);
        $online['secret'] = $product->getOnlineSaleProducts(30, 6);
        $online['database'] = $product->getOnlineSaleProducts(37, 6);
        $online['tarcode'] = $product->getOnlineSaleProducts(29, 6);
        $online['ui'] = $product->getOnlineSaleProducts(29, 6);

        $productrecommend = $product->getProductHomeRecommend();

        $sourcem = new SourceModel();
        $resource = $sourcem->getProductAttchs()['list'];
        $demo = null;

        $ar = new ArticleModel();
        $all = $ar->getRelationArticles(0, 20);

        $this->assign([
            'title' => '供应商首页',
            'keywords' => '供应商首页关键词',
            'description' => '供应商首页SEO描述',
            'productrecommend' => $productrecommend,
            'hotrelate' => $hotrelate,
            'online' => $online,
            'all' => $all,
            'topics' => null,
            'campaigns' => null,
            'source' => $resource,
            'demo' => $demo,
        ]);

        return $this->fetch();
    }

    /**
     * 分类页
     */
    public function category()
    {
        $id = input('param.id');

        $order = input('param.o') ? input('param.o') : 'dn desc';

        if ($order == 'comment') {
            $order = '';
        }
        if ($order == 'update') {
            $order = 'LastUpdateTime';
        }
        if ($order == 'click') {
            $order = '';
        } else {
            $order = 'dn desc';
        }

        $cm = new CatagoryModel();
        $category = $cm::get([
            'Id' => $id,
        ]);

        if (!$category) {
            return redirect('/component');
        }

        // 1. 获取轮播广告
        $arkey = 'category' . $id . '-ad';
        $ad = new AdModel();
        $scroll = $ad->getAd($arkey);

        // 2. 获取最新上架*4
        $pm = new ProductModel();
        $list = $pm->getProductList($id, 0, 1, 10, 'new');

        // 3. 获取推荐产品*4 需要改一下
        $recommend = $pm->getProductList($id, 0, 1, 8, 'recommend');
        // $recommend=$pm->getRecommend($id,'normal');

        // 4. 获取热门推荐轮播*8
        $hots = $pm->getRecommends($id, 'hot');

        // 5. 获取产品动态*8
        $am = new ArticleModel();
        $news = $am->getProductCategoryArticles($id, 1, 1, 8);

        // 6. 获取热门文章*8

        $hotsnews = $am->getProductCategoryArticles($id, 1, 1, 8, 'hot');

        // 7. 获取视频资源*8

        $rm = new SourceModel();
        $video = $rm->videos(0, 0, $id, 1, 8, '');

        // 8. 获取最新评论*10

        $cm = new CommentsModel();
        $comments = $cm->getProductCategoryComments($id, 1, 10);

        // 9. 获取产品，分别是下载、评论、最近更新，需要分页

        $categoryproducts = Db::name('product')->alias('a')
            ->field('a.*,b.name as supplier,c.dn,c.productid')
            ->join('supplier b', 'a.supplierid=b.id', 'left')
            ->join('(select count(1)as dn,ProductId from product_downloadrecord group by productid) c', 'a.id=c.productid', 'left')
            ->where('categoryid', $id)
            ->where('a.isdisabled=0')
            ->order($order)
            ->paginate(10);

        // 10. 获取相关厂商
        $products = $pm->getRecommends($id, 'normal', 10, 'product');

        // 11. 获取10个推荐厂商
        $suppliers = $pm->getRecommends($id, 'normal', 10, 'supplier');

        $this->assign([
            'list' => $categoryproducts,
            'page_method' => $categoryproducts->render(),
            'lastproduct' => $list,
            'scroll' => $scroll,
            'toprecommend' => $recommend,
            'suppliers' => $suppliers,
            'products' => $products,
            'comments' => $comments,
            'news' => $news,
            'hotnews' => $hotsnews,
            'videos' => $video,
            'categoryid' => $id,
            'title' => $category['title'] == null ? $category['name'] : $category['title'],
            'keywords' => $category['keywords'],
            'description' => $category['description'] == null ? $category['name'] : $category['description'],
        ]);

        return $this->fetch();
    }

    /**
     * 供应商
     *
     * @return void
     * @author dmakecn@163.com
     * @since 2020-01-03
     */
    public function vendor()
    {
        $sid = input('id');

        $sm = new SupplierBase();

        $supplier = $sm->getSupplier($sid);

        $sm->where('id', $sid)->setInc('clickcount');

        $map1['supplierid'] = $sid;
        $map1['status'] = 1;

        $products = Db::name('product')->alias('a')
            ->where($map1)
            ->order('sort desc')
            ->paginate(10);

        $productcount = Db::name('product')->where($map1)->count();
        $supplier['productcount'] = $productcount;

        $am = new \app\common\model\ArticleBase();
        $articles = $am->getArticleSupplier($sid);

        $db = Db::name('video');
        $field = 'video.id,video.title,video.imagepath,video.duration';
        $videos = $db->field($field)
            ->join('product', 'product.id=video.productid')
            ->where('supplierid', $sid)
            ->order('id desc')
            ->limit(10)
            ->select();

        $assign = [
            'supplier' => $supplier,
            'product' => $products,
            'page_method' => $products->render(),
            'article' => $articles,
            'videos' => $videos,
            'title' => $supplier->title,
            'keywords' => $supplier->keywords,
            'description' => $supplier->description,
        ];

        return $this->view->assign($assign)->fetch();
    }

    public function search()
    {
        $categorys = dcache(CACHE_COMMODITY, 'SEARCHLIST');
        if (!$categorys) {
            $categorys = Db::name('product_category')->field('id,name')->select();
            dcache(CACHE_COMMODITY, 'SEARCHLIST', $categorys);
        }

        $kw = input('kw');
        $c = input('c');
        $o = input('o');
        $pl = input('pl');

        $map['a.status'] = 1;

        if ($c && $c > 0) {
            $map['categoryid'] = $c;
        }

        //============================== 搜索的重点 ===============================================

        if ($kw && $kw != '') {

            $kw = trim($kw);
            $kw = str_replace(' ', '%', $kw);

            $map['a.name|b.name|title'] = [
                'like',
                '%' . $kw . '%',
            ];
        }

        //============================== 搜索的重点 ===============================================

        if ($pl && $pl != '') {
            $map['platform'] = [
                'like',
                '%' . $pl . '%',
            ];
        }

        $products = Db::name('product')->alias('a')
            ->field('a.id,a.name,a.imagepath,a.	slogan,a.version,a.categoryid,a.producttype,a.platform,a.supplierid ,b.name as supplier,c.dn,c.parentid')
            ->join('supplier b', 'a.supplierid =b.id', 'left')
            ->join('(select count(1)as dn,productid from product_downloadrecord group by productid) c', 'a.id=c.productid', 'left')
            ->where($map)
            ->order('dn desc')
            ->paginate(10, false, [
                'query' => [
                    'kw' => $kw,
                    'c' => $c,
                    'pl' => $pl,
                ],
            ]);

        $list = [];
        $listfirst = [];
        $listlast = [];
        foreach ($products as $c) {
            $temp = explode(' ', $c['name']);
            $array = [];
            foreach ($temp as $a) {
                array_push($array, strtolower($a));
            }
            if (in_array(strtolower($kw), $array) || $c['headlinetime'] > time()) {
                array_push($listfirst, $c);
                continue;
            }

            array_push($listlast, $c);
        }

        $list = array_merge($listfirst, $listlast);

        $this->assign([
            'list' => $list,
            'page_method' => $products->render(),
            'kw' => $kw,
            'title' => '搜索' . $kw,
            'keywords' => '',
            'description' => '',
            'categorys' => $categorys,
        ]);

        return $this->fetch();
    }

    /**
     * 跳转供应商官网
     */
    public function jumpto()
    {
        $sid = input('sid');

        $supplier = SupplierBase::get($sid);

        return redirect($supplier['website']);
    }

    public function detail()
    {

        $id = input('param.id');

        $pm = new ProductModel();

        $product = $pm->getProductCommon($id);

        if ($product == null) {
            return $this->error('您正在查看的内容不存在，即将为您跳转到首页', '/');
        }

        // 评价
        $cm = new CommentsBase();
        // 资讯
        $article = new ArticleModel();
        // 视频
        $sb = new SeriesBase();
        //论坛
        $form = new ForumModel();

        $assign = array_merge([
            'title' => $product->title,
            'keywords' => $product->keywords,
            'description' => $product->description,
            'product' => $product,
            'rproducts' => $product['rproducts'],
            'rsuppliers' => $product['rsuppliers'],
            'myproducts' => $pm->getRelateProducts($id, 0, $product->supplierid), //同厂产品
            'oproduct' => $pm->getOnlineSaleProducts(0, 3), //销售产品
            'samproduct' => $pm->getSameProduct($product->categoryid), // 铜梁产品
            'comments' => $cm->getProductComments($product->id, 1, 8),
            'commentid' => $id,
            'supplierid' => $product['supplierid'],
            'images' => $pm->getProductImages($id), //缩略图
            'videos' => $pm->getProductVideos($id, 2), //视频
            'resources' => $pm->getProductResources($id, 2), //资源
            'newarticles' => $article->getProductArticles($id, 2), //最新资讯
            'rarticles' => $article->getProductArticles($id, 10), //相关资讯
            'eduvideo' => $sb->order('id desc')->limit(4)->select(), //系列视频
            'licens' => $pm->getProductPrices($id), //价格问题
            'files' => $pm->getProductDownloads($id, 10), //下载
            'mvp' => $pm->getProductDownloadsMvp($id), //下载最多
            'articles' => $pm->getProductArticle($id, 10), //相关资讯
            'demos' => $pm->getProductResources($id, 10), //demo
            'question' => $form->getProductThreads($id, 10), //论坛
        ]
        );

        return $this->view->assign($assign)->fetch();

    }
}
