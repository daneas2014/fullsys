<?php
namespace app\ucenter\model;

use think\Db;
use think\Model;

/**
 * 用户信息的读取修改
 */
class InfoModel extends Model
{

    protected $name = 'customer';

    // 根据id获取用户信息
    public function getOneInfo($id)
    {
        return $this->get($id);
    }

    public function getOneByPhone($phone)
    {
        return $this->where('mobile', $phone)->find();
    }

    public function getMore($id)
    {
        return Db::name('customer_address')->where('customerid', $id)->find();
    }

    public function setMore($param)
    {
        $uid = $param['id'];
        unset($param['id']);

        $more = $this->getMore($uid);

        if (!$more) {
            $param['customerid'] = $uid;
            $res = Db::name('customer_address')->insert($param);
            return ['code' => 1, 'data' => '', 'msg' => '保存完毕'];
        }

        $res = Db::name('customer_address')->where('customerid', $uid)->update($param);

        return ['code' => 1, 'data' => '', 'msg' => '保存完毕'];
    }

    public function getAttribute($customerid)
    {
        if (!$customerid) {
            return null;
        }

        $att = Db::name('customer_attribute')->where('customerid', $customerid)->find();

        if (!$att) {
            $att['customerid'] = $customerid;
            $att['emailvalitime'] = 0;
            $att['mobilevalitime'] = 0;
            $att['create_time'] = time();
            $att['update_time'] = time();
            $att['logintime'] = 1;

            Db::name('customer_attribute')->insert($att);
        }

        return $att;
    }

    public function updatecustomer_attribute($customerid, $type)
    {
        $level = $this->get(['id' => $customerid])['accountlevel'];
        if ($type == 'email') {
            if ($level == 4) {
                $this->update(['accountlevel' => 6], ['id' => $customerid]);
            } else {
                $this->update(['accountlevel' => 5], ['id' => $customerid]);
            }
            return Db::name('customer_attribute')->where('customerid', $customerid)->update(['emailvalitime' => time()]);
        }
        if ($type == 'phone') {
            if ($level == 5) {
                $this->update(['accountlevel' => 6], ['id' => $customerid]);
            } else {
                $this->update(['accountlevel' => 4], ['id' => $customerid]);
            }
            return Db::name('customer_attribute')->where('customerid', $customerid)->update(['mobilevalitime' => time()]);
        }
    }

    /**
     * 发送验证码
     * @param unknown $type
     * @param unknown $account
     * @param unknown $code
     * @param string $platform wechat  minprogram
     * @param string $platformid appid
     * @return unknown
     */
    public function createValidate($type, $account, $code, $platform = '', $platformid = '')
    {
        $data['valitype'] = $type; //phone,email
        $data['valiaccount'] = $account;
        $data['code'] = $code;
        $data['create_time'] = time();
        $data['deadline_time'] = time() + 1800; //30分钟分钟过期
        $data['valistatus'] = 0;

        $id = Db::name('customer_validate')->insertGetId($data);

        if ($type == 'phone') {
            $sms = new \app\common\model\SmsBase($account);
            $sms->sendRegmsg($code);
        } else {
            $ms = new \app\common\model\EmailBase($account);
            $ms->sendRegmail($code);
        }

        return $id;
    }

    /**
     * 对某个渠道的账号进行验证码验证
     *
     * @param [type] $type
     * @param [type] $account
     * @param [type] $code
     * @return void
     * @author dmakecn@163.com
     * @since 2021-02-03
     */
    public function validateGo($type, $account, $code)
    {

        $data['valitype'] = $type; //phone,email
        $data['valiaccount'] = $account;
        $data['code'] = $code;

        $one = Db::name('customer_validate')->where($data)->find();

        if ($one == null) {
            return ['code' => -1, 'data' => '', 'msg' => '验证码不存在'];
        }

        if ($one['valistatus'] == 1) {
            return ['code' => -1, 'data' => '', 'msg' => '验证码已经使用过了'];
        }

        if ($one['deadline_time'] < time()) {
            return ['code' => -1, 'data' => '', 'msg' => '验证码已经过期'];
        }


        if ($one['valitype'] == 'phone') {
            Db::name('customer')->where('mobile', $one['valiaccount'])->update(['accountlevel' => 7]);
        }

        return ['code' => 1, 'data' => '', 'msg' => '校验正确'];
    }

    /**
     * 编辑用户信息
     *
     * @param [type] $param
     *            [description]
     * @return [type] [description]
     */
    public function editInfo($param)
    {
        try {
            $result = $this->allowField(true)->save($param, ['id' => $param['id']]);

            if (false === $result) {
                return ['code' => 0, 'data' => '', 'msg' => $this->getError()];
            } else {
                return ['code' => 1, 'data' => '', 'msg' => '编辑成功'];
            }
        } catch (\Exception $e) {
            return ['code' => 0, 'data' => '', 'msg' => $e->getMessage()];
        }
    }

    /**
     * 账户安全
     */
    public function settings($id)
    {
        return $this->alias('a')->field('a.*,b.emailvalitime,b.mobilevalitime')
            ->join('customer_attribute b', 'customer.id = b.customerid')
            ->where('a.Id', $id)
            ->select();
    }

    /**
     * 订单信息
     */
    public function getOrderInfo($id, $limit = 5)
    {
        return $this->alias('a')->field('b.id,b.create_time,b.orderno,b.paymenttype,b.status,b.totalamount,b.subject')
            ->join('orders b', 'a.id = b.customerid')
            ->where('a.id', $id)
            ->order('create_time desc')
            ->limit($limit)
            ->select();
    }
    /**
     * 收藏产品信息
     */
    public function ColProInfo($id, $limit = 5)
    {

        return Db::name('customer_preference')->alias('a')->field('b.id,b.name,a.create_time as SignTime,a.objectid')
            ->join('product b', 'a.objectid = b.id', 'left')
            ->where('customerid', $id)
            ->order('SignTime desc')
            ->limit($limit)
            ->select();
    }

}
