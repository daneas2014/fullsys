<?php
namespace app\ucenter\model;

use app\common\model\UcenterBase;

/**
 * 用于客户订单查询、文章、问题...查询
 */
class BusinessModel extends UcenterBase
{

    /**
     * 用户业务清单查询
     * 
     * @param [type] $type
     *            [order,question,article,messge]
     * @param array $params
     *            [查询参数数组]
     * @param integer $page
     *            [index]
     * @param integer $limit
     *            [limit]
     * @return [type] [返回list,count数组]
     */
    public function getBusQuery($type, $params = [], $page = 1, $limit = 10)
    {
        return $this->getCommonBusQuery($type, $params, $page, $limit);
    }

    /**
     * [getBusDetail 获取业务详情]
     * 
     * @param [type] $type
     *            [order,question,article,messge]
     * @param [type] $id
     *            [业务id]
     * @return [type] [返回$data]
     */
    public function getBusDetail($type, $id)
    {
        return $this->getCommonBusDetail($type, $id);
    }
}
?>