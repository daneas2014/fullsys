<?php
namespace app\ucenter\model;

use think\Model;

class CouponModel extends Model
{
    protected $name = "customer_coupon";

    public function getMyCoupons($customerid, $limit = 5)
    {
        return $this->where(['customerid' => $customerid, 'update_time' => 0, 'delete_time' => 0])->limit(10)->select();
    }
}
