<?php
namespace app\ucenter\controller;

use think\Controller;

class Api extends Controller
{

    public function login()
    {
        $data = input('post.');
        $type = $data['type'];
        $appid = $data['appid'];
        $accesstoken = $data['accesstoken'];
        $nickname = $data['nickname'];
        $sex = $data['sex'];
        $figureurl = $data['figureurl'];

        $auth = new Uauth();
        $res = $auth->doLogin($data);

        return json($res);
    }

    /**
     * 根据订单ID来查询订单状态
     *
     * @return void
     * @author dmakecn@163.com
     * @since 2021-02-03
     */
    public function ordershow()
    {
        $data = input('post.');

        $om = new \app\pay\model\OrderModel();
        $detail = $om->getOrderDetail($data['id']);

        return json($detail);
    }
}
