<?php
namespace app\ucenter\controller;

use think\Controller;
use think\Db;

use app\common\model\UcenterBase;
use app\common\model\EmailBase;
use app\common\model\SmsBase;
use app\ucenter\model\DetailModel;

/**
 * 用户登录，注册，验证等一系列操作
 *
 * @author Administrator
 *        
 */
class Uauth extends Controller
{
    /**
     * 普通登录
     */
    public function login()
    {

        if(session('mid')||session('memberinfo'))
        {
            return redirect(url('info/index'));
        }

        if(request()->isPost()){
            $data = input('post.');            
            $username = $data['username'];
            $password = $data['password'];
            
            $result = $this->validate(compact('username', 'password'), 'LoginCheckValidate');
                        
            if (true !== $result) {
                return json(['code' => - 1, 'url' => '', 'msg' => $result]);
            }
                        
            $res = $this->dologin($data);
            
            return json($res);
        }
        
        $assign=['title'=>'登录/注册','keywords'=>'','description'=>''];

        return $this->assign($assign)->fetch();
    }
    
    
    
    /**
     * 登录操作，写入用户session
     * @param unknown $data
     */
    public function doLogin($data){
        
        $act_arr = ['normal','qq','wx','wb','twetter','facebook'];
        
        $in_res = in_array($data['type'],$act_arr);
        
        if($in_res === false){
            return ['code' => - 1, 'data' => '', 'msg' => '非法操作'];
        }
        
        $um=new UcenterBase();               
        
        // 1.正常的登录流程
        if($data['type']=='normal'){
                
            $map['mobile|nickname|email']=$data['username'];
            $map['delete_time']=0;
            
            $user=$um->where($map)->find();
            
            if($user==null){
                return ['code'=>-1,'data'=>'','msg'=>'用户不存在'];
            }
                            
            // 密码双MD5校验
            $md5=md5($data['password'].config('auth_key'));
            if($user['password']!=$md5){
                return ['code'=>-1,'data'=>$md5,'msg'=>'密码错误，如果忘记密码请点击上方找回[ERROCODE:1]'];
            }                                            
            
            $um->where('id',$user['id'])->setField('update_time',time());
                            
            session('mid',$user['id']);
            session('memberinfo',$user);
            cookie('mid', $user['id']);
            
            return [
                'code' => 1,
                'data' => $data['reffer'],
                'msg' => '登录成功'
            ];
        } else {
            
            $json = $um->thirdSideLogin($data['type'], $data['appid'], $data['accesstoken'], $data['nickname'], $data['figureurl']);
            
            $mid = $json['customerid'];            
            $user = $um->where('id', $mid)->find();            
            $um->where('id', $mid)->setField('update_time',time());
            
            session('mid',$user['id']);
            session('memberinfo',$user);
            cookie('mid',$user['id']);
            
            if($json['isnew']==1){
                return ['code'=>1,'data'=>url('ucenter/detail/info'),'msg'=>'登录成功'];  
            }
            
            return ['code'=>1,'data'=>url('ucenter/detail/index'),'msg'=>'登录成功'];    
        }
        
    }

    /**
     * 普通注册，姓名，电话，邮箱必填
     */
    public function register()
    {
        if (request()->isPost()) {
            
            $data = input('post.');
            
            $res = $this->doRegister($data);
            
            return json($res);
        }
        
        if (session('memberinfo') != null) {
            return redirect('/customer/main');
        }
        return $this->fetch();
    }
    
    public function doRegister($data){

        //1. 验证是否填写完了数据
        $result = $this->validate($data,'RegisterCheckValidate');
        
        if($result ===true){
        
            $um=new UcenterBase();
        
            //验证该手机是否注册
            $mobile = $data['mobile'];
            $where['mobile'] = $mobile;
            $userinfo = $um->where($where)->find();
        
            if($userinfo){
                return ['code' => - 1, 'data' => '', 'msg' => '该手机号已注册，请直接登录'];
            }
            
            //验证该邮箱是否注册
            $mail = $data['email'];
            $where1['email'] = $mail;
            $userinfo = $um->where($where1)->find();
            
            if($userinfo){
                return ['code' => - 1, 'data' => '', 'msg' => '该邮箱已注册，请直接登录'];
            }
        
            //写入数据库
            $password = $data['password'];
            $password_new= md5(md5($password) . config('auth_key'));
        
            $regUser['mobile'] = $mobile;
            $regUser['email']=$data['email'];
            $regUser['password'] = $password_new;
            $regUser['nickname'] = $data['nickname'];
            $regUser['headimage'] = '/static/images/defaulthead.jpg';//默认头像
            $regUser['create_time'] = time();
            $regUser['delete_time'] = 0;
            $regUser['accountlevel']=1;
            $regUser['integral']=10;
        
            $uid = $um->insertGetId($regUser);
                
            try{
                $att['customerid']=$uid;
                $att['emailvalitime']=0;
                $att['mobilevalitime']=0;
                $att['create_time']=time();
                $att['update_time']=time();
                $att['logintime']=1;
        
                Db::name('customer_attribute')->insert($att);
            }
            catch (\Exception $e){
        
            }
        
            if(!$uid){
                return ['code' => - 1, 'data' => '', 'msg' => '注册账号异常，请刷新后再试'];
            }else{
                //跳转页面完善用户信息
                $user=$um->get($uid);
                session('mid',$user['id']);
                session('memberinfo',$user);
        
                return ['code' =>  1, 'data' => '', 'msg' => '注册账号成功,请用当前账号密码登录吧！'];
            }
        }else{
            return ['code' => - 1, 'data' => '', 'msg' => $result];
        }
    }

    /**
     * 找回账号功能
     * @return \think\response\Json|unknown
     */
    public function findpwd(){
        if(request()->isPost()){
            
            $type=input('post.type');
            $username=input('post.username');            
            $um=new UcenterBase();
            
            if($type=='validate'){                
                $map['mobile|nickname|email|username']=$username;       
                $map['delete_time']=0;
                $user=$um->where($map)->find();
                if($user==null){
                    return json(['code'=>-1,'data'=>'','msg'=>'用户名不存在']);
                }
                
                $time=time();
                return json(['code'=>1,'data'=>$time,'msg'=>md5(md5($time).$username)]);
            }
            
            if($type=='cpwd'){                
                $code=input('post.code');
                $token=input('post.token');
                
                if($token!=md5(md5($code).$username)){
                    return json(['code'=>-1,'data'=>'','msg'=>'非法操作']);
                }
                
                $map['mobile|nickname|email|username']=$username;
                $map['delete_time']=0;
                $user=$um->where($map)->find();
                if($user==null){
                    return json(['code'=>-1,'data'=>'','msg'=>'用户名不存在']);
                }   
                
                
                $password = rand(111111,999999);
                
                $password_new= md5(md5($password) . config('auth_key'));    

                if($user['mobile']){
                    $result=$um->where('id',$user->id)->setField('password',$password_new);
                    $sms=new SmsBase($user['mobile']);
                    $res=$sms->sendAccountchange($password);
                
                    return $res;
                }
                
                //有邮箱才重置                
                if($user['email']){
                    $result=$um->where('id',$user->id)->setField('password',$password_new);
                    
                    $res=null;              
                    $em=new EmailBase($user['email']);
                    $res= $em->sendFindpwd($user->UserName,$password);                    
                    
                    return $res;
                }
                
                return json(['code'=>-1,'data'=>'','msg'=>'您的账号未绑定邮箱和手机，请联系客服处理']);
            }
        }
        
        return $this->fetch();
    }
            
    /**
     * 注销
     */
    public function logout(){
        //注销缓存信息
        session('mid',null);
        session('memberinfo',null);
        cookie('mid',null);
        //退出登录后跳转到首页
        $this->redirect('/');
    }

    /**
     * 验证验证码有效性
     * @return \think\response\Json
     */
    public function valireg(){
        
        $code=input('post.code');
        
        $account=input('post.ac');
        
        $type=input('post.type');
        
        $dm=new DetailModel();
        
        return json($dm->validateGo($type, $account, $code));
    }
    
    /**
     * 邮件获取验证码
     */
    public function getCode(){
        $data=input('post.');
        $msg=['code'=>-1,'data'=>'','msg'=>''];        
        $code=rand(100000,999999);
        $dm=new DetailModel();
        
        //3. 返回用户验证码 ，并写入数据库本次验证码的详情
        switch ($data['type']){
            case 'mail':
                $msg=$dm->createValidate('email', $data['email'], $code);
                break;
            break;
        }
        
        return json($msg);
    }    
    
}