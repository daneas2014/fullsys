<?php
namespace app\ucenter\controller;

use app\common\model\CouponBase;
use app\common\model\CustomerInvoiceBase;
use think\Db;

class Business extends Base
{

    /**
     * 获得活动优惠券
     */
    public function recivecoupon()
    {

        $couponid = input('couponid');

        $id = session('mid');

        $cm = new CouponBase();

        $res = $cm->activeCoupon($couponid, $id);

        if ($res['code'] == 1) {
            return redirect(url('coupons'));
        } else {
            return $this->error($res['msg'], '/customer/main');
        }
    }

    public function coupons()
    {
        $id = session('mid');

        $cm = new CouponBase();

        $list = $cm->customerCoupons($id);

        $this->assign('coupons', $list);

        return $this->fetch();
    }

    public function orders()
    {

        $id = session('mid');

        $list = Db::name('orders')->where('customerid', $id)
            ->order('id desc')
            ->paginate(20);

        $this->assign('page_method', $list->render());
        $this->assign('list', $list);

        return $this->fetch();
    }

    public function mycourse()
    {

        $id = session('mid');
        $start = strtotime(input('start'));
        $end = strtotime(input('end'));

        $map['customerid'] = $id;
        $map['create_time'] = ['between', [$start, $end]];
        $map['deadline'] = ['between', [$start, $end]];

        return parent::businessQuery('course', $map);
    }

    public function resource()
    {

        return $this->fetch();
    }

    public function product()
    {

        $id = session('mid');

        $list = Db::name('tag_relation')->alias('a')
            ->field('b.id,b.name,b.imagepath,a.signtime,a.id as oid')
            ->join('product b', 'a.oid=b.id', 'inner')
            ->where('customerid', $id)
            ->paginate(20);

        $this->assign('page_method', $list->render());
        $this->assign('list', $list);

        return $this->fetch();
    }

    /**
     * 评论  ----
     * @return [type] [description]
     */
    public function comment()
    {

        $id = session('mid');

        $list = Db::name('comments')->where('customerid', $id)
            ->paginate(20);

        $dba = Db::name('article');
        $dbp = Db::name('product');
        $dbv = Db::name('video');
        $dbc = Db::name('course_description');

        $newl = [];

        foreach ($list as $l) {
            switch ($l['moduletype']) {
                case 1:
                    $t = $dbv->field('id,title')->where('id', $l['targetid'])->find();
                    $l['title'] = $t['title'];
                    $l['url'] = '/video/' . $t['id'];
                    break;
                case 2:
                    $t = $dbp->field('id,name')->where('id', $l['targetid'])->find();
                    $l['title'] = $t['name'];
                    $l['url'] = '/product/' . $t['id'];
                    break;
                case 3:
                    $t = $dba->field('id,title')->where('id', $l['targetid'])->find();
                    $l['title'] = $t['title'];
                    $l['url'] = '/article/' . $t['id'];
                    break;
                case 4:
                    $t = $dba->field('id,title')->where('id', $l['targetid'])->find();
                    $l['title'] = $t['title'];
                    $l['url'] = '/course/play/' . $t['id'];
                    break;
            }

            array_push($newl, $l);
        }

        $this->assign('page_method', $list->render());
        $this->assign('list', $newl);

        return $this->fetch();
    }

    /**
     * 收货系信息
     *
     * @return void
     * @author dmakecn@163.com
     * @since 2021-01-06
     */
    public function financeinfo()
    {

        if (request()->isPost()) {

            try {
                $cim = new CustomerInvoiceBase();
                $data = input('param.');
                $res = $cim->saveOne($data);

                return $res ? getJsonCode(true, '已保存') : getJsonCode(false, '保存失败');

            } catch (\think\Exception $e) {

                return getJsonCode(false, '发生异常，ErroMsg：' . $e->getMessage());

            }
        }

        $id = session('mid');
        $invoice = Db::name('customer_invoice')->where('customerid', $id)->find();
        return $this->view->assign(['invoice' => $invoice, 'customerid' => $id])->fetch();
    }
}
