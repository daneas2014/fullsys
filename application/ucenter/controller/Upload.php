<?php
namespace app\ucenter\controller;

use app\api\controller\UploadApi;

class Upload extends UploadApi
{

    public function canvas(){        
        return parent::upload_base64();
    }
}