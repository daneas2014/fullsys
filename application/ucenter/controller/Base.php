<?php
namespace app\ucenter\controller;

use app\common\model\UcenterBase;
use think\Controller;

class Base extends Controller
{
    public $ub;

    public function _initialize()
    {
        $member = session('memberinfo');
        $this->assign('u', $member);

        if ($member == null) {
            return $this->error('您还未登录', '/customer/sign-up');
        }

        $this->ub = new UcenterBase();

    }

    public function businessQuery($type, $map)
    {

        $Nowpage = input('get.page') ? input('get.page') : 1;
        $limits = input('get.rows') ? input('get.rows') : config('list_rows'); // 获取总条数

        $res = $this->ub->getCommonBusQuery($type, $map, $Nowpage, $limits);

        $data['list'] = $res['list'];
        $data['count'] = $res['count'];
        $data['page'] = $Nowpage;

        if (input('get.page')) {
            return json($data);
        }

        return $this->fetch();

    }
}
