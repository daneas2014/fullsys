<?php
namespace app\ucenter\controller;

use app\common\model\ProductBase as mprotuct;
use app\index\model\AdModel;
use app\index\model\ArticleModel;
use app\ucenter\model\InfoModel;

class Info extends Base
{

    public function index()
    {
        $userinfo = session('memberinfo');

        // 1. 如果是第三方用户登录，检查登录账号密码
        if ($userinfo['mobile'] == null || $userinfo['email'] == null) {
            return $this->success('您的信息不完整，请及时更新！', url('info'));
        }
        
        if ($userinfo['password'] == null) {
            return $this->success('您还未设置初始密码，请前往设置！', url('info'));
        }

        $ad = new AdModel();
        $indexad = $ad->getAd('index-a1');

        $am = new ArticleModel();
        $evnews = $am->getRelationArticles();

        $cp = new mprotuct();
        $hotrelate = $cp->getHotRalateProducts(6);

        $cm = new \app\ucenter\model\CouponModel();

        $orders = new InfoModel();
        
        $assign = [
            'indexad' => $indexad,
            'evnews' => $evnews,
            'hotrelate' => $hotrelate,
            'coupons' => $cm->getMyCoupons($userinfo['id']),
            'list' => $orders->getOrderInfo($userinfo['id']),
            'collePro' => $orders->ColProInfo($userinfo['id'])];

        return $this->view->assign($assign)->fetch();
    }

    // 账号安全
    public function setting()
    {
        $dm = new InfoModel();

        $userinfo = session('memberinfo');

        $attribute = $dm->getAttribute($userinfo['id']);

        $this->assign('attribute', $attribute);
        $this->assign('userInfo', $userinfo);

        return $this->fetch();
    }

    // 编辑个人信息
    public function info()
    {
        $detail = new InfoModel();
        if (request()->isPost()) {

            $param = input('post.');

            $act = $param['act'];
            unset($param['act']);

            switch ($act) {
                case 'saveimage':
                // 下面的帮忙保存了
                case 'savebase':

                    if (array_key_exists('password', $param)) {
                        $param['password'] = md5(md5($param['password']) . config('auth_key'));
                    }

                    // 唯一性校验
                    if (array_key_exists('mobile', $param) && $param['mobile'] && $param['mobile'] != '') {
                        $map['mobile'] = $param['mobile'];
                        $map['id'] = ['<>', $param['id']];

                        $res = $detail->where($map)->count();
                        if ($res > 0) {
                            return json(['code' => -1, 'data' => '', 'msg' => '该电话已被注册']);
                        }

                        unset($map);
                    }

                    // 唯一性校验
                    if (array_key_exists('email', $param) && $param['email'] && $param['email'] != '') {
                        $map['email'] = $param['email'];
                        $map['id'] = ['<>', $param['id']];

                        $res = $detail->where($map)->count();
                        if ($res > 0) {
                            return json(['code' => -1, 'data' => '', 'msg' => '该邮箱已被注册']);
                        }

                        unset($map);
                    }

                    $flag = $detail->editInfo($param);

                    $userinfo = $detail->getOneInfo($param['id']);
                    session('memberinfo', $userinfo);

                    break;
                case 'savemore':
                    $flag = $detail->setMore($param);
                    break;
            }

            return json($flag);
        }

        $id = session('mid');

        $this->assign([
            'userInfo' => $detail->getOneInfo($id),
            'more' => $detail->getMore($id),
            'attribute' => $detail->getAttribute($id),
        ]);

        return $this->fetch();
    }

    public function checkaccount()
    {

        if (request()->isPost()) {

            $dm = new InfoModel();

            $type = input('post.type');
            $account = input('post.account');
            $userid = input('post.userid');

            if ($type == 'email') {

                $res = $dm->where(['Email' => $account, 'Id' => ['<>', $userid]])->count();
            } else if ($type == 'phone') {
                $res = $dm->where(['mobile' => $account, 'Id' => ['<>', $userid]])->count();
            }

            if ($res == 0) {
                return json(['code' => 1, 'data' => '', 'msg' => '']);
            }

            return json(['code' => -1, 'data' => '', 'msg' => '']);
        }
    }

    /**
     * 手机认证
     */
    public function phonevali()
    {

        $dm = new InfoModel();

        if (request()->isPost()) {
            $post = input('post.');
            $id = $post['id'];
            $phone = $post['phone'];
            $code = $post['code'];

            $user = $dm->getOneInfo($id);

            if ($user['mobile'] != $phone) {
                return json(['code' => -1, 'data' => '', 'msg' => '手机号异常']);
            }

            $json = $dm->validateGo('phone', $phone, $code);

            if ($json['code'] == 1) {
                $dm->updatecustomer_attribute($id, 'phone');
            }

            return json($json);
        }

        $id = session('mid');

        $userinfo = $dm->getOneInfo($id);

        if ($userinfo['mobile'] == null) {
            return $this->error('您还未填写您的手机！', url('info'));
        }

        $this->assign([
            'userInfo' => $userinfo,
            'more' => $dm->getMore($id),
            'attribute' => $dm->getAttribute($id),
        ]);

        return $this->fetch();
    }

    /**
     * 邮箱验证
     */
    public function mailvali()
    {

        $dm = new InfoModel();

        if (request()->isPost()) {
            $post = input('post.');
            $id = $post['id'];
            $mail = $post['mail'];
            $code = $post['code'];

            $user = $dm->getOneInfo($id);

            if ($user['email'] != $mail) {
                return json(['code' => -1, 'data' => '', 'msg' => '邮箱异常']);
            }

            $json = $dm->validateGo('email', $mail, $code);

            if ($json['code'] == 1) {
                $dm->updatecustomer_attribute($id, 'email');
            }

            return json($json);
        }
        $id = session('mid');

        $userinfo = $dm->getOneInfo($id);

        if ($userinfo['email'] == null) {
            return $this->error('您还未填写您的邮箱！', url('info'));
        }

        $this->assign([
            'userInfo' => $userinfo,
            'more' => $dm->getMore($id),
            'attribute' => $dm->getAttribute($id),
        ]);
        return $this->fetch();
    }

    /**
     * 发送验证码
     * @return number[]|string[]
     */
    public function valireg()
    {
        $detail = new InfoModel();
        $param = input('param.');

        $code = rand(100000, 999999);

        $type = $param['type'];
        $account = $param['account'];
        $data = $detail->createValidate($type, $account, $code);

        if ($data > 0) {
            return json(['code' => 1, 'data' => '', 'msg' => '验证码已发送' . ($type == 'email' ? ',请登录邮箱获取' : '')]);
        }

        return json(['code' => -1, 'data' => '', 'msg' => '验证码发送失败']);
    }

    public function chpwd()
    {
        $detail = new InfoModel();

        if (request()->isPost()) {

            $param = input('post.');

            $oldpassword = $param['oldpassword'];

            if (md5(md5($oldpassword) . config('auth_key')) != $param['md5']) {
                return json(['code' => -1, 'data' => '', 'msg' => '老密码不正确']);
            }
            unset($param['md5']);

            $newpassword = $param['password'];

            $customerid = $param['id'];

            unset($param['oldpassword']);

            if ($newpassword) {
                $param['password'] = md5(md5($newpassword) . config('auth_key'));
            }

            $flag = $detail->editInfo($param);

            return json([
                'code' => $flag['code'],
                'data' => $flag['data'],
                'msg' => $flag['msg'],
            ]);
        }

        $id = session('mid');

        $this->assign('userInfo', $detail->getOneInfo($id));
        
        return $this->fetch();
    }

    /**
     * 提问
     */
    public function add()
    {
        return $this->fetch();
    }

    public function question()
    {
        return $this->fetch();
    }

    public function answer()
    {
        return $this->fetch();
    }

    public function fans()
    {
        return $this->fetch();
    }

    public function tag()
    {
        return $this->fetch();
    }

    public function notice()
    {
        return $this->fetch();
    }

    public function message()
    {
        return $this->fetch();
    }

    public function attention()
    {
        return $this->fetch();
    }

    public function saveheadimg()
    {}
}
