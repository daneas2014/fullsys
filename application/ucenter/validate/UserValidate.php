<?php

namespace app\ucenter\validate;

use think\Validate;

/**
 * 验证登录信息
 * @author Administrator
 *
 */
class UserValidate extends Validate
{
    protected $rule = [
        'mobile'=> 'require|max:12',
        'password_one'=> 'require',
        'password_two'=> 'require',
    ];
    protected $message  =   [
        'mobile.require' => '电话号码不能为空',
        'mobile.max'     => '名称最多不能超过11个字符',
        'password_one.require'   => '密码不能为空',
        'password_two.require'   => '密码不能为空',
    ];
    protected $scene = [
        'register'  =>  ['mobile','password_one','password_two'],
    ];
}

