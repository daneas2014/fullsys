<?php
namespace app\ucenter\validate;

use think\Validate;

class RegisterCheckValidate extends Validate{
    
    protected $rule = [
        ['mobile', 'require|max:11|/^1[3-9]{1}[0-9]{9}$/|unique:customer', '手机号不能为空|手机号长度为11位|请输入正确的手机号|手机已经被人注册'],
        ['email', 'require|unique:customer', '邮箱不能为空|邮箱已存在'],
        ['nickname', 'require|unique:customer', '昵称不能为空|昵称已存在'],
        ['password', 'require', '密码不能为空'],
    ];
}