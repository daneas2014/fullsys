<?php

namespace app\ucenter\validate;

use think\Validate;

/**
 * 验证验证信息是否健全
 * @author Administrator
 *
 */
class LoginCheckValidate extends Validate
{
    protected $rule = [
        ['username', 'require', '用户名不能为空'],
        ['password', 'require', '密码不能为空'],
    ];
    
}