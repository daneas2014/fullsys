<?php
namespace app\mall\controller;

use app\index\controller\Base;
use app\index\model\AdModel;
use app\index\model\ProductModel;
use app\mall\model\MallModel;
use think\Db;

class Index extends Base
{

    /**
     * 在线订购首页
     */
    public function index()
    {
        $ad = new AdModel();

        $mm = new MallModel();

        $assign = [
            'contents' => [
                ['list' => $mm->getProducts('', 5, 1, 10)['list'], 'groupname' => '国产软件', 'id' => 5],
                ['list' => $mm->getProducts('', 4, 1, 10)['list'], 'groupname' => '公众平台', 'id' => 4],
                ['list' => $mm->getProducts('', 2, 1, 10)['list'], 'groupname' => '.NET源码', 'id' => 2],
                ['list' => $mm->getProducts('', 1, 1, 10)['list'], 'groupname' => '网站系统', 'id' => 1],
            ],
            'indexad' => $ad->getAd('mall-a1'),
            'sales' => $mm->getPromotes([1, 2, 3, 4]),
        ];

        return $this->view->assign($assign)->fetch();
    }

    /**
     * 分类
     */
    function list() {
        $title = '';
        $key = input('param.kw');
        $category = input('param.cateid');
        $order = input('param.order');

        if ($order == null || $order == '') {
            $order = 'productid';
        }
        if ($order == 'new') {
            $order = 'create_time';
        }
        if ($order == 'hot') {
            $order = 'sort';
        }
        $map['status'] = 1;

        if ($key && $key != '') {
            $map['a.name'] = ['like', '%' . $key . '%'];
            $title = $key;
        }

        if ($category > 0) {
            $map['a.categoryid'] = $category;
            $category = Db::name('product_category')->where('id', $category)->find();

            if ($category) {
                $title = $category['name'] . '购买';
            }
        }

        $mm = new MallModel();

        $db = Db::name('product');

        $list = $db->alias('a')
            ->field('a.name,a.imagepath,a.slogan,b.productid,pricemin,pricemax,sort')
            ->join("(select productid,MIN(amount) as pricemin,MAX(amount) AS pricemax  from product_price where amount>0 and status=1 group by productid ) as b ", 'a.id=b.productid', 'right')
            ->where($map)
            ->order($order . ' desc')
            ->paginate(18);

        $assign = ['list' => $list, 'page_method' => $list->render(), 'sales' =>null/* $mm->getPromotes([1, 2, 3, 4])*/];
        
        if ($title) {
            array_merge($assign, ['title' => $title]);
        }

        return $this->view->assign($assign)->fetch();
    }

    /**
     * 产品详情
     */
    public function detail()
    {
        $mm = new ProductModel();

        $id = input('param.id');

        $groupname = input('param.g');

        $licenceid = input('param.licenseid');

        $product = $mm->getProductCommon($id);

        $licens = $mm->getProductPrices($product->id);

        if ($licens == null) {
            return $this->error('您正在访问的商品已下线', '/mall');
        }

        $mm = new MallModel();
        $others = $mm->getOtherRelationProducts($product['categoryid'], 10);

        $this->assign([
            'product' => $product,
            'rproducts' => $product['rproducts'],
            'rsuppliers' => $product['rsuppliers'],
            'other' => $others,
            'licens' => $licens,
            'lid' => $licenceid,
            'gname' => $groupname,
            'title' => $product['name'] . '购买',
            'keywords' => $product['name'] . '购买',
            'description' => $product['name'] . '购买',
        ]);

        return $this->fetch();
    }

    public function buycheck()
    {
        $user = session('memberinfo');
        if (request()->isPost()) {

            $input = input('post.');
            if (array_key_exists('cd', $input)) {
                switch ($input['cd']) {
                    case 'change':
                        return $this->changenum();
                    case 'changetime':
                        return $this->changetime();
                    case 'remove':
                        return $this->removecar();
                }
            }

            $token = $input['token'];
            $ids = explode(',', $input['ids']);

            $list = Db::name('order_shoppingcart')->where('id', 'in', $ids)->select();

            if (count($list) == count($ids)) {
                Db::name('order_shoppingcart')->where('uid', $user['Id'])->update(['buycheck' => 0]);
                Db::name('order_shoppingcart')->where('id', 'in', $ids)->update(['buycheck' => 1]);
            }
            return getJsonCode(true);
        }

        // $user=session('memberinfo');
        $order = Db::name('order_shoppingcart')->alias('a')
            ->field('a.number,a.id as cid,a.ttype,a.tcount,b.Name as productname,ImagePath,c.*')
            ->join('Catalogproduct b', 'a.productid = b.Id', 'left')
            ->join('Catalogproductlicense c', 'a.licenseid = c.Id')
            ->where('uid', $user['Id'])
            ->select();

        $ck = new \app\mall\controller\Checkout();
        foreach ($order as &$o) {
            $amount = $ck->countOrderAmount($o, $o['number'], '', false, $o['ttype'], $o['tcount']);
            $o['singleprice'] = $amount['singleprice'];
        }

        $this->assign([
            'order' => $order,
            'title' => '购物车',
            'keywords' => '购物车',
            'description' => '购物车',
        ]);
        return $this->fetch();
    }

    public function changenum()
    {
        $id = input('param.id');
        $number = input('param.val');
        $param = ['id' => $id, 'number' => $number];
        try {
            $result = Db::name('order_shoppingcart')->update($param, ['id' => $param['id']]);
            if (false === $result) {
                return ['code' => 0, 'data' => '', 'msg' => $this->getError()];
            } else {
                return ['code' => 1, 'data' => '', 'msg' => '成功'];
            }
        } catch (\Exception $e) {
            return ['code' => 0, 'data' => '', 'msg' => '失败'];
        }
    }

    public function changetime()
    {
        $id = input('param.id');
        $tcount = input('param.val');
        $param = ['id' => $id, 'tcount' => $tcount];
        try {
            $result = Db::name('order_shoppingcart')->update($param, ['id' => $param['id']]);
            if (false === $result) {
                return ['code' => 0, 'data' => '', 'msg' => $this->getError()];
            } else {
                return ['code' => 1, 'data' => '', 'msg' => '成功'];
            }
        } catch (\Exception $e) {
            return ['code' => 0, 'data' => '', 'msg' => '失败了'];
        }
    }

    public function removecar()
    {
        $id = input('param.id');
        try {
            $where['id'] = array('in', $id);
            Db::name('order_shoppingcart')->where($where)->delete();
            return ['code' => 1, 'data' => '', 'msg' => '删除成功'];

        } catch (\Exception $e) {
            return ['code' => 0, 'data' => '', 'msg' => '删除失败'];
        }
    }
}
