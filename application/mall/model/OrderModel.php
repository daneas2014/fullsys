<?php
namespace app\mall\model;

use think\Model;

class OrderModel extends Model
{
    protected $name='orders';

    /**
     * 创建订单，把订单参数以数组形式传入，并发起支付【点击微信或支付宝支付后产生】 调用这个方法生产参数buildOrderParams
     * 
     * @param unknown $params            
     */
    public function createOrder($type='alipay',$params,$createby='productOrder')
    {
        $order['orderno']=time().rand(10000,99999);
        $order['createby']=$createby;
        
        $order['customerid']=$params['mid'];
        $order['paymenttype']=$params['type'];
        $order['totalamount']=$params['total_amount'];
        
        $order['create_time']=time();
        $order['pay_time']=0;
        $order['cancle_time']=0;
        
        return $this->insertGetId($order);
    }

    /**
     * 同时返回订单号和支付方订单号，
     * 
     * @param unknown $orderid            
     * @param unknown $tradeno            
     */
    public function payOrder($orderid = 0, $tradeno = '')
    {}

    /**
     * payorder处理完了之后这里开始根据订单类型处理，例如发邮件、发微信、发短信，处理之后改变order发货状态
     * 
     * @param unknown $orderid            
     */
    public function processOrder($orderid)
    {}
    
    public function getOrder($orderid){
        
    }
}