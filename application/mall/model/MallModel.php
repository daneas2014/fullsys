<?php
namespace app\mall\model;

use think\Model;

class MallModel extends Model
{

    protected $name = 'product';

    public function getPromotes($ids)
    {
        $key = CACHE_COMMODITY . 'MALLFUN1';

        $list = cache($key);

        if (!$list) {

            $child = "(select productid,MIN(amount) as pricemin,MAX(amount) AS pricemax  from product_price where amount>0 and status=1 group by productid ) as b ";

            $list = $this->alias('a')
                ->field('a.name,a.imagepath,a.slogan,b.productid,pricemin,pricemax ')
                ->join($child, 'a.id=b.productid', 'inner')
                ->where('productid', 'in', $ids)
                ->select();

            cache($key, $list);
        }

        return $list;
    }

    public function getProducts($key = '', $category = 0, $page = 1, $limit = 20)
    {
        if ($key && $key != '') {
            $map['a.name'] = ['like', '%' . $key . '%'];
        }

        if ($category > 0) {
            $map['a.categoryid'] = $category;
        }

        $name = CACHE_COMMODITY . 'MALLFUN2' . $key . '_' . $category . '_' . $page . '_' . $limit;

        $data = cache($name);

        if (!$data) {

            $list = $this->alias('a')
                ->field('a.name,a.imagepath,a.slogan,b.productid,pricemin,pricemax')
                ->join("(select productid,MIN(amount) as pricemin,MAX(amount) AS pricemax from product_price where productid>0 and amount>0 and status=1 group by productid ) as b ", 'a.id=b.productid', 'inner')
                ->where($map)
                ->order('productid desc')
                ->page($page, $limit)
                ->select();

            $count = $this->alias('a')
                ->join("(select productid from product_price where productid>0 and amount>0  and status=1 group by productid ) as b ", 'a.id=b.productid', 'inner')
                ->where($map)
                ->count();

            $data = [
                'list' => $list,
                'count' => $count,
            ];

            cache($name, $data);
        }

        return $data;
    }

    /**
     * 获取和产品同类的其他产品
     *
     * @param unknown $productid
     * @param unknown $limit
     */
    public function getOtherRelationProducts($CategoryId, $limit = 10)
    {
        $key = CACHE_COMMODITY . 'MALLFUN3' . $CategoryId . '_' . $limit;

        $list = cache($key);

        if (!$list) {

            $list = $this->alias('a')
                ->field('a.name,a.imagepath,a.slogan,b.productid,pricemin,pricemax')
                ->join("(select productid,MIN(amount) as pricemin,MAX(amount) AS pricemax  from product_price where productid>0  and status=1 group by productid ) as b ", 'a.id=b.productid', 'inner')
                ->where('categoryId', $CategoryId)
                ->order('productid desc')
                ->limit($limit)
                ->select();

            cache($key, $limit);
        }

        return $list;
    }

    /**
     * 限时促销的活动
     *
     * @param unknown $categoryid
     * @param number $limit
     */
    public function getSales($cateid = 0, $limit = 10)
    {

        if ($cateid > 0) {
            $map['categoryid'] = $cateid;
        }

        $child = "(select MIN(amount) as amin,MAX(amount) AS amax  from product_price where amount>0 and status=1 limit 5 group by productid order by amin asc ) as b ";

        $list = $this->alias('a')
            ->field('a.name,a.imagepath,a.descriptionshort,b.productid,amin,amax')
            ->join($child, 'a.id=b.productid', 'inner')
            ->where($map)
            ->limit($limit)
            ->select();

        return $list;
    }
}
