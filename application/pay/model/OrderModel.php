<?php
namespace app\pay\model;

use think\Model;
use think\Db;
use app\common\model\MailModel;
use app\common\model\SmsModel;

class OrderModel extends Model
{

    protected $name = 'orders';

    /**
     * 创建订单，把订单参数以数组形式传入，并发起支付【点击微信或支付宝支付后产生】 调用这个方法生产参数buildOrderParams
     *
     * @param unknown $params            
     */
    public function createOrder($memberid, $type = 'alipay', $params, $createdby = 'productOrder')
    {
        $order['orderno'] = time() . rand(10000, 99999);
        
        $order['customerid'] = $memberid;
        $order['paymenttype'] = $type;
        $order['totalamount'] = $params['total_amount'];
        
        $order['create_time'] = time();
        $order['pay_time'] = 0;
        $order['cancel_time'] = 0;
        
        return $this->insertGetId($order);
    }

    public function getOrder($orderid)
    {        
        $detail=$this->getOrderDetail($orderid);
        
        $order=$detail['order'];
        
        $items=$detail['items'];
        
        $name='';
        
        $body='';
        
        if(strstr($order['orderno'], 'S')){
            $name=$order['subject'];
            $body=$order['subject'];
        }
        else{
            foreach ($items as $i){
                $name.=$i['product'].'['.$i['license'].']';
                $body.=$i['product'].'['.$i['license'].']';
            }
        }
        
        $order['name']=$name;
        $order['body']=$body;
        
        return $order;
    }

    /**
     * 根据支付平台ID获取订单
     * @param unknown $out_trade_no
     * @return unknown
     */
    public function getPayOrder($out_trade_no)
    {
        return $this->where('orderno', $out_trade_no)->find();
    }

    /**
     * 完成订单支付,并执行接下来的方法
     *
     * @param unknown $out_trad_no            
     * @return number|\think\false
     */
    public function orderPaySuccess($out_trade_no, $order, $trade_no = '')
    {
        $update['pay_time'] = time();
        $update['status'] = 'OD';
        $order['pay_time']=$update['pay_time'];
        $order['status']=$update['status'];
        if ($trade_no != '') {
            $update['trade_no'] = $trade_no;
        }
        
        $res=$this->where('orderno',$out_trade_no)->update($update);
        
        if ($res) {
            
            $sms=new SmsModel($order['customer_mobil']);
            $sms->sendOrderpaid($order['subject']);
            
            $mm=new MailModel('heyuemax@163.com');
            $mm->sendOrderPaid($order);            
            
           return self::processPaidOrder($order);
        }
    }

    /**
     * 订单被支付成功后，执行下一步操作
     *
     * @param unknown $order            
     */
    private function processPaidOrder($order)
    {
        $res=false;
        
        // 视频类会员，例如优酷大会员，优酷剧集会员
        if (strstr( $order['orderno'],'S')) {
            $item = Db::name('order_item')->where('orderid', $order['id'])->find();
            $ac['courseid'] = $item['productid'];
            $ac['customerid'] = $order['customerid'];
            $ac['orderid'] = $order['Id'];
            $ac['create_time'] = time();
            $ac['deadline'] = strtotime("+1 year");
            
            
            if($ac['courseid']>0)
            {
                $course = Db::name('course_description')->where('id', $ac['courseid'])->find();
            }
            else{
                $course['title']='感谢您购买大会员';
                $course['summary']='限时1年';
                $course['imagepath']='';
            }
            
            $ac['coursename'] = $course['title'];
            $ac['coursesummary'] = $course['summary'];
            $ac['courseimage'] = $course['imagepath'];
            
            $res= Db::name('course_access')->insertGetId($ac);
        }
        
        if($res)
        {
            return $this->where('id',$order['id'])->setField('status','OV');
        }
    }
    
    public function getOrderDetail($orderid){
        
        $data['order'] = $this->get([
            'id' => $orderid
        ]);
        
        $data['items'] = Db::name('order_item')->alias('a')
            ->field('a.*,b.name as product,c.name as license')
            ->join('product b', 'a.productid=b.id', 'left')
            ->join('product_price c', 'a.licenseid=c.id', 'left')
            ->where('orderid', $orderid)
            ->select();
        
        return $data;
    }
}