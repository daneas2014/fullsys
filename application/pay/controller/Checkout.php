<?php
namespace app\pay\controller;

use app\index\controller\Base;
use think\Db;

/**
 * 线上收银，暂时全部关掉
 *
 * @author dmakecn@163.com
 * @since 2021-09-09
 */
class Checkout extends Base
{

    public function index()
    {

        return $this->error('本站不提供交易服务,如需购买可联系站点客服咨询！', null, '', 10);

        $data = input('param.');
        $licensekey = $data['LicenseKey'];
        $buynum = $data['buyNum'];
        $user = session('memberinfo');

        if ($user == null) {
            return $this->success('您还未登录，请登录后购买！', AUTHURL);
        }

        $license = Db::name('product_price')->alias('a')
            ->field('a.*,b.imagepath')
            ->join('product b', 'a.productid=b.id', 'left')
            ->where('a.id', $licensekey)
            ->where('a.status', 1)
            ->find();

        if (!$license) {
            return $this->error('当前商品已下架');
        }

        $amount = $this->countOrderAmount($license, $buynum);

        $invoice = Db::name('customer_invoice')->where('customerid', $user['id'])->find();

        $this->assign(['title' => '订单支付确认',
            'keywords' => '',
            'description' => '',
            'license' => $license,
            'buyNum' => $buynum,
            'total_amount' => $amount['total_amount'],
            'rate_amount' => $amount['rate_amount'],
            'order_amount' => $amount['order_amount'],
            'invoice' => $invoice,
            'singleprice' => $amount['singleprice']]);

        return $this->fetch();
    }

    public function checkcoupon()
    {
        if (request()->isPost()) {
            $code = input('param.code');
            $productid = input('param.productid');
            $dbcoupon = Db::name('coupon');

            $coupon = $dbcoupon->alias('a')
                ->field('a.*,b.customerid')
                ->join('customer_coupon b', 'a.id=b.couponid', 'left')
                ->where('code', $code)
                ->find();

            // 所有的优惠券必须满足未使用原则
            if ($coupon == null || $coupon['status'] == 1 || $coupon['leftnum'] <= 0) {
                return json(['code' => -1, 'data' => '', 'msg' => '优惠券不可用']);
            }

            // 检查是否过期
            if ($coupon['end_time'] < time()) {
                return json(['code' => -1, 'data' => '', 'msg' => '优惠券已过期']);
            }

            // 检查是否是对应了产品
            if ($coupon['productid'] > 0 && $coupon['productid'] != $productid) {
                return json(['code' => -1, 'data' => '', 'msg' => '本产品不能使用该优惠券']);
            }

            // 满额券的检查[20190904新增]
            if ($coupon['type'] == 'pricelimit' && input('param.amount') < $coupon['pricelimit']) {
                return json(['code' => -1, 'data' => '', 'msg' => '本单不满足满额优惠条件']);
            }

            // 2019-5-24 新增个人专项定向券
            $user = session('memberinfo');
            if ($coupon['customerid'] != null && $coupon['customerid'] > 0 && $coupon['customerid'] != $user['Id']) {
                return json(['code' => -1, 'data' => '', 'msg' => '本券为个人定向券，您没有权限使用']);
            }

            return json(['code' => 1, 'data' => $coupon['amount'], 'msg' => 'success']);
        }
    }

    public function productpay()
    {

        if (request()->isPost()) {

            $input = input('post.');

            $LicenseKey = $input['licenseid'];
            $buyNum = $input['buynum'];
            $user = session('memberinfo');
            $paytype = $input['paytype'];
            $discountcode = $input['couponcode'] ? $input['couponcode'] : '';

            //1. 罗列购物清单
            $license = Db::name('product_price')->where('id', $LicenseKey)->find();

            $amount = $this->countOrderAmount($license, $buyNum, $discountcode, true);

            //2. 保存订单，保存订单详细
            $order['customer_name'] = $input['contact_name'];
            $order['customer_mobil'] = $input['contact_mobile'];
            $order['customer_address'] = $input['contact_address'];
            $order['customer_zip'] = $input['contact_zip'];
            $order['note'] = $input['remark']; //留言

            // $order['customer_email'] = $input['contact_email']; 前端删除了
            // $order['customer_company'] = $input['customer_company'];  前端删除了

            // **********************这个钱啷个算很重要，要和输入的值做对比**************************

            $order['totalamount'] = $amount['order_amount']; //此处需要计算<============================================================
            $order['discount'] = $amount['discount'];
            $order['discountcode'] = $amount['discountcode'];

            if ($input['order_amount'] != $order['totalamount']) {
                return $this->error('参数错误，请核对后再试！ERROCODE:110');
            }

            $order['orderno'] = 'P' . date('YmdHis', time());
            $order['productid'] = $license['productid'];
            $order['customerid'] = $user['id'];
            $order['paymenttype'] = $input['paytype'];
            $order['status'] = 'WP';
            $order['create_time'] = time();
            $order['pay_time'] = 0;
            $order['cancel_time'] = 0;

            $order['paymentid'] = ''; //真的是paypal/alipay的订单号

            // $order['invoice'] = $license['invoice'] > 0 ? (int) $license['invoice'] : 3; //发票,一般系统都包含了的所以取消了这个字段

            $order['invoice_number'] = $input['invoice_code']; //税号
            $order['invoice_head'] = $input['invoice_name']; //发票抬头
            $order['invoice_email'] = $input['contact_email']; //收货邮箱
            $order['invoice_remark'] = ''; // 未知
            $order['remark'] = json_encode($amount);

            $product = Db::name('product')->field('name')->where('id', $license['productid'])->find();
            $order['subject'] = $product['name'];

            $orderid = Db::name('orders')->insertGetId($order);

            $orderitem['orderid'] = $orderid;
            $orderitem['productid'] = $license['productid'];
            $orderitem['licenseid'] = $LicenseKey;
            $orderitem['cost'] = $amount['singleprice'];
            $orderitem['qty'] = $buyNum;
            $orderitem['orderyear'] = 1;
            Db::name('order_item')->insert($orderitem);

            //4. 记录

            $orderlog['orderno'] = $order['orderno'];
            $orderlog['orderid'] = $orderid;
            $orderlog['changedstate'] = 'WP';
            $orderlog['remark'] = '创建了一条订单，等待支付' . ',订单详情：授权ID[' . $license['id'] . ']' . $license['groupname'] . '-' . $license['name'];
            $orderlog['create_time'] = time();
            Db::name('order_history')->insert($orderlog);

            //5. 提交给支付接口
            $url = url('pay/index/payorder') . '?orderid=' . $orderid;

            return redirect($url);
        }

    }

    /**
     * 计算订单的总金额(人民币转其他货币)
     * @param unknown $license
     * @param unknown $buynum
     * @param string $discountcode 一旦查询就使用了哟
     * @param bool $update 是否改变优惠券状态
     * @return number[]
     */
    private function countOrderAmount($license, $buynum, $discountcode = '', $update = false)
    {

        $total_amount = 0;

        $discountamount = 0;

        $order_amount = 0;

        $siglprice = 0;

        // 1.计算授权价格

        if ($license['amount'] > 0) { // 实际展示价格
            $siglprice = $license['amount'];
        }

        if ($license['sale_amount'] > 0) {
            $siglprice = $license['sale_amount'];
        }

        if ($license['sale_amount'] > 0 && time() < (strtotime($license['sale_deadline']))) {
            $siglprice = $license['sale_amount'];
        }

        // 2. 计算对应的货币价格 TIPS:目前可以不需要了，但是在其他币种环境需要考虑
        switch ($license['currencytype']) {
            case 1: // 美元
                $siglprice = $siglprice / config('exrates')['tousd'];
                break;
            case 2: // 欧元
                $siglprice = $siglprice / config('exrates')['togbp'];
                break;
            case 3: // 英镑
                $siglprice = $siglprice / config('exrates')['toeur'];
                break;
            default:break;
        }

        $singleprice = ceil($siglprice);

        // 3.计算总价,一年的金额*年限
        $total_amount = $buynum * $singleprice;

        //  优惠券分为个人券和批发券和满额券，个人券只能使用1次，批发券只需要在时间范围内即可
        if ($discountcode != '') {
            $dbcoupon = Db::name('coupon');
            $coupon = $dbcoupon->where('code', $discountcode)->find();

            // 个人使用
            if ($coupon && $coupon['type'] == 'personal' && $coupon['isused'] == 0) {
                $discountamount = $coupon['amount'];
                if ($update === true) {
                    $update = ['isused' => 1, 'used_time' => time()];
                    $dbcoupon->where('id', $coupon['id'])->update($update);
                }
            }

            // 满额券【20190904新增】
            if ($coupon && $coupon['type'] == 'pricelimit' && $coupon['isused'] == 0 && $coupon['pricelimit'] < $total_amount) {
                $discountamount = $coupon['amount'];
                if ($update === true) {
                    $update = ['isused' => 1, 'used_time' => time()];
                    $dbcoupon->where('id', $coupon['id'])->update($update);
                }
            }

            //群发
            else if ($coupon && $coupon['leftnum'] > 0) {
                $discountamount = $coupon['amount'];
                if ($update === true) {
                    $update = ['leftnum' => $coupon['leftnum'] - 1, 'used_time' => time()];
                    $dbcoupon->where('id', $coupon['id'])->update($update);
                }
            }

        }

        // 4.计算订单折扣、税率
        $rate = 0; //零售一般卖价就含税费
        // if ($license['invoice'] && $license['invoice'] > 0) {
        //     $rate = number_format($license['invoice'] / 100, 2);
        // }
        // if ($license['withinvoce'] == 1) {
        //     $rate = 0;
        // }

        // a 总价等于产品总价-折扣
        $total_amount = $total_amount - $discountamount;

        // b 税费等于折后价算税费
        $rate_amount = ceil($total_amount * $rate);

        // c 订单总价等于产品折后价+税费
        $order_amount = $total_amount + $rate_amount;

        return ['singleprice' => $singleprice, 'buynum' => $buynum, 'total_amount' => $total_amount, 'rate_amount' => $rate_amount, 'order_amount' => $order_amount, 'discount' => $discountamount, 'discountcode' => $discountcode];
    }
}
