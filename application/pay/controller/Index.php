<?php
namespace app\pay\controller;

use app\index\controller\Base;
use app\pay\model\OrderModel;
use EasyWeChat\Factory;
use think\Loader;

/**
 * 需要支付的地方先处理好订单关系，然后把支付需求发过来就是了
 * 这里是订单、视频、会员集中处理支付的地方
 *
 * @author Daneas
 *
 */
class Index extends Base
{

    /**
     * 创建支付订单，并提示发票、快递信息
     */
    public function payorder()
    {
        $om = new OrderModel();

        $order = $om->getOrder(input('param.orderid'));

        $params = self::buildOrderToParams($order['name'], $order['totalamount'], $order['orderno'], $order['subject'], $order['paymenttype']);

        // 阿里支付
        if ($order['paymenttype'] == 'alipay') {
            $res = $this->initAlipay($params, $order['productid'], $order['invoice']);

        }
        // 微信支付
        else if ($order['paymenttype'] == 'wechat') {
            $app = Factory::payment(config('pay')['wechat']);
            $res = $app->order->unify([
                'product_id' => $order['productid'],
                'body' => $params['body'],
                'out_trade_no' => $params['out_trade_no'],
                'total_fee' => $params['total_fee'],
                'trade_type' => 'NATIVE',
                'openid' => '', // $openid 在 trade_type JSAPI时填写
            ]);

            if (array_key_exists('return_code', $res) && $res['return_code'] == 'FAIL') {
                echo $res['return_msg'];
                exit();
            }

            // 微信支付二维码地址
            $path = $res['code_url'];
            $prepay_id = $res['prepay_id'];

            $om->where('orderno', $params['out_trade_no'])->update([
                'trade_no' => $prepay_id,
            ]);

            $this->assign([
                'order' => $order,
                'qrcode' => $path,
            ]);

            return $this->fetch('index/weixin');
        }

        // PayPal
        else if ($order['paymenttype'] == 'PayPal') {

            // $config = config('pay')['paypal'];
            $config = config('pay')['paypalsandbox'];

            $apiContext = $this->initPaypal($config);

            // 支付模式
            $payer = new \PayPal\Api\Payer();
            $payer->setPaymentMethod('paypal');

            // 金额管理
            $amount = new \PayPal\Api\Amount();
            $amount->setTotal($order['totalamount'] * config('exrates')['tousd']);
            $amount->setCurrency('USD');

            // 写入商品列表
            $itemList = new \PayPal\Api\ItemList();

            $item2 = new \PayPal\Api\Item();
            $item2->setName('Granola bars')
                ->setCurrency('USD')
                ->setQuantity(5)
                ->setSku("456456")
                ->setPrice(10);

            $itemList->setItems([$item2]);

            // 设定收货地址信息，防止用户自付款时可改
            $address = new \PayPal\Api\ShippingAddress();
            $address->setRecipientName('什么名字')
                ->setLine1('什么街什么路什么小区')
                ->setLine2('什么单元什么号')
                ->setCity('城市名')
                ->setState('浙江省')
                ->setPhone('12345678911')
                ->setPostalCode('12345')
                ->setCountryCode('CN');

            $itemList->setShippingAddress($address);

            // 支付明细
            $transaction = new \PayPal\Api\Transaction();
            $transaction->setAmount($amount);
            //$transaction->setItemList($itemList);
            $transaction->setDescription($order['subject']);

            // 返回实例
            $redirectUrls = new \PayPal\Api\RedirectUrls();

            $redirectUrls->setReturnUrl($config['returnurl'])
                ->setCancelUrl($config['cancelurl']);

            // 开始支付啦
            $payment = new \PayPal\Api\Payment();
            $payment->setIntent('sale')
                ->setPayer($payer)
                ->setTransactions(array($transaction))
                ->setRedirectUrls($redirectUrls);

            try {
                $payment->create($apiContext);
                $url = $payment->getApprovalLink();
                // 对本地订单和Paypal关联   PAYID-MAMQDYY8A559617K0274840T
                $om->where('id', $order['id'])->update([
                    'paymentid' => $payment->getId(),
                ]);

                return $this->view->assign(['url' => $url, 'order' => $order, 'title' => 'PayPal支付跳转'])->fetch("index/paypal");
            } catch (\PayPal\Exception\PayPalConnectionException $ex) {
                echo $ex->getMessage();
            }

        }
    }

    /**
     * BADCODE:WAITING TRANS
     * 微信取消订单,这个是在后台操作才行，先写到这里
     */
    public function canclePay()
    {
        $data = input('param.');
        $om = new OrderModel();
        $order = $om->getOrder($data['orderid']);

        if ($order['create_time'] > (time() - 5 * 60)) {
            return $this->error('订单超过5分钟后可以才能取消');
        }

        if ($data['type'] == 'wechat') {
            $app = Factory::payment(config('pay')['wechat']);
            $app->order->close($order['out_trade_no']);
            return $this->success('订单已取消');
        }
    }

    /**
     * 支付宝付款成功直接回调
     *
     * @return void
     * @author dmakecn@163.com
     * @since 2020-01-03
     */
    public function alinotify()
    {
        Loader::import('alipay.AlipayTradeService', EXTEND_PATH);

        $arr = $_POST;

        $out_trade_no = $_POST['out_trade_no'];

        $om = new OrderModel();

        $order = $om->getPayOrder($out_trade_no);

        $config = config('pay')['ali'];

        $alipaySevice = new \AlipayTradeService($config);

        // $alipaySevice->writeLog(var_export($_POST, true));

        $result = $alipaySevice->check($arr);

        if ($result) {

            $trade_no = $_POST['trade_no'];

            $trade_status = $_POST['trade_status'];

            if ($_POST['trade_status'] == 'TRADE_FINISHED') {

                if ($order['pay_time'] > 0) {
                    echo 'success';
                    exit();
                }

                $om->orderPaySuccess($out_trade_no, $order, $trade_no);

            } else if ($_POST['trade_status'] == 'TRADE_SUCCESS' && $_POST['total_amount'] == $order['totalamount']) {
                if ($order['pay_time'] > 0) {
                    echo 'success';
                    exit();
                }

                $om->orderPaySuccess($out_trade_no, $order, $trade_no);
            }
            echo "success";
        } else {
            echo "fail";
        }
    }

    /**
     * 支付宝异步回调
     *
     * @return void
     * @author dmakecn@163.com
     * @since 2020-01-03
     */
    public function alireturn()
    {
        Loader::import('alipay.AlipayTradeService', EXTEND_PATH);

        $arr = $_GET;

        // 商户订单号
        $out_trade_no = htmlspecialchars($_GET['out_trade_no']);

        // 支付宝交易号
        $trade_no = htmlspecialchars($_GET['trade_no']);

        $om = new OrderModel();

        $order = $om->getPayOrder($out_trade_no);

        $config = config('pay')['ali'];

        $alipaySevice = new \AlipayTradeService($config);

        $result = $alipaySevice->check($arr);

        if ($result) {

            if ($order['pay_time'] == 0) {
                $om->orderPaySuccess($out_trade_no, $order, $trade_no);
            }

            // ——请根据您的业务逻辑来编写程序（以上代码仅作参考）——
            return $this->success('交易成功，即将跳转到您的订单页面<br />支付宝交易号：' . $trade_no, '/ucenter/business/orders', null, 20);
        } else {
            // 验证失败
            echo "验证失败";
        }
    }

    /**
     * Paypal支付返回回调
     *
     * @return void
     * @description
     * @example
     * @author daneas
     * @since 2021-01-31
     */
    public function paypalreturn()
    {
        // $config = config('pay')['paypal'];
        $config = config('pay')['paypalsandbox'];
        // 实例初始化
        $apiContext = $this->initPaypal($config);

        // Get payment object by passing paymentId  支付预设的ID，和支付宝一样
        $paymentId = $this->request->param('paymentId');
        $payment = new \PayPal\Api\Payment();
        $payment = $payment->get($paymentId, $apiContext);
        $payerId = $this->request->param('PayerID');

        // Execute payment with payer ID   支付者的ID
        $execution = new \PayPal\Api\PaymentExecution();
        $execution->setPayerId($payerId);

        try {
            // Execute payment
            $result = $payment->execute($execution, $apiContext);
            if ($result && isset($result->state) && $result->state == 'approved') {

                $om = new OrderModel();
                $order = $om->where('paymentid', $paymentId)->find(); //$om->getPayOrder($param['orderno']); paypal不返回本平台的业务NO，所以只有预设ID来查找了
                $om->orderPaySuccess($order['orderno'], $order, $payerId); //payerid是支付订单的人的paypalid

                return $this->success('交易成功，即将跳转到您的订单页面<br />PayPal交易号：' . $paymentId, '/ucenter/business/orders', null, 20);
            } else {
                echo "支付失败";
                var_dump($result);
            }
        } catch (\PayPal\Exception\PayPalConnectionException $ex) {
            echo $ex->getCode() . '&&&&&' . $ex->getData();
            die($ex);
        } catch (\Exception $ex) {
            die($ex);
        }

    }

    /**
     * Paypal取消支付
     *
     * @return void
     * @description
     * @example
     * @author daneas
     * @since 2021-01-31
     */
    public function paypalcancel()
    {
        echo '取消支付';
    }

    /**
     * 微信回调
     *
     * @return void
     * @author dmakecn@163.com
     * @since 2020-01-03
     */
    public function wenotify()
    {
        try {

            $app = Factory::payment(config('pay')['wechat']);

            $response = $app->handlePaidNotify(function ($message, $fail) {

                $recive = $message;

                $om = new OrderModel();

                $out_trade_no = $message['out_trade_no'];

                $order = $om->getPayOrder($out_trade_no);

                // 如果订单不存在 或者 订单已经支付过了
                if (!$order || $order['pay_time'] > 0) {
                    return true;
                }

                if ($message['return_code'] === 'SUCCESS') {

                    if ($message["result_code"] === 'SUCCESS') {
                        $om->orderPaySuccess($out_trade_no, $order);
                    } elseif ($message["result_code"] === 'FAIL') {
                        $order['status'] = 'PF';
                        $om->save($order, [
                            'id' => $order['id'],
                            'orderno' => $out_trade_no,
                        ]);
                    }
                } else {
                    return $fail('通信失败，请稍后再通知我');
                }

                return true;
            });

            $response->send();
        } catch (\Exception $e) {

        }
    }

    /**
     * 订单包装成参数
     *
     * @param unknown 订单标题
     * @param unknown 订单金额
     * @param unknown 商户订单号
     * @param unknown 订单详情
     * @param unknown 支付方式
     * @return string[]|unknown[]|string[]|number[]|unknown[]
     */
    private function buildOrderToParams($name, $amount, $outtradno, $body, $paytype)
    {
        if ($paytype == 'alipay') {
            return [
                'subject' => $name,
                'total_amount' => $amount,
                'out_trade_no' => $outtradno,
                'body' => $body,
            ];
        }

        if ($paytype == 'wechat') {
            return [
                'body' => $name,
                'detail' => $body,
                'out_trade_no' => $outtradno,
                'total_fee' => $amount * 100,
            ];
        }
    }

    /**
     * 阿里支付
     *
     * @param unknown $params[out_trade_no,subject,total_amount,body]
     * @param string $licensekey
     */
    private function initAlipay($params, $licensekey = '', $invoice = 3)
    {
        $config = config('pay')['alisandbox']; //ali

        Loader::import('alipay.AlipayTradePagePayContentBuilder', EXTEND_PATH);
        Loader::import('alipay.AlipayTradeService', EXTEND_PATH);

        $out_trade_no = trim($params['out_trade_no']);
        $subject = trim($params['subject']);
        $total_amount = trim($params['total_amount']);
        $body = trim($params['body']);

        // 构造参数
        $payRequestBuilder = new \AlipayTradePagePayContentBuilder();
        $payRequestBuilder->setBody($body);
        $payRequestBuilder->setSubject($subject);
        $payRequestBuilder->setTotalAmount($total_amount);
        $payRequestBuilder->setOutTradeNo($out_trade_no);

        $aop = new \AlipayTradeService($config);
        $response = $aop->pagePay($payRequestBuilder, $config['return_url'], $config['notify_url']);
    }

    /**
     * 初始化Paypal
     *
     * @return void
     * @author dmakecn@163.com
     * @since 2021-02-01
     */
    private function initPaypal($config)
    {
        // 实例初始化
        $apiContext = new \PayPal\Rest\ApiContext(
            new \PayPal\Auth\OAuthTokenCredential($config['ClientID'], $config['ClientSecret'])
        );

        /*sandbox 模式*/
        $apiContext->setConfig(['mode' => 'sandbox',
            'log.LogEnabled' => true,
            'log.FileName' => './paypal.log',
            'log.LogLevel' => 'DEBUG', // PLEASE USE `INFO` LEVEL FOR LOGGING IN LIVE ENVIRONMENTS
            'cache.enabled' => true,

            // 'http.CURLOPT_CONNECTTIMEOUT' => 30
            // 'http.headers.PayPal-Partner-Attribution-Id' => '123123123'
            //'log.AdapterFactory' => '\PayPal\Log\DefaultLogFactory' // Factory class implementing \PayPal\Log\PayPalLogFactory
        ]);

        return $apiContext;
    }

    public function alinotify1()
    {
        return dump(input('param.'));
    }

    public function alireturn1()
    {
        return dump(input('param.'));
    }
}
