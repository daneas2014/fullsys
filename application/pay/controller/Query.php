<?php
namespace app\pay\controller;

use think\Controller;
use app\pay\model\OrderModel;

class Query extends Controller{
    
    public function index(){
        $out_trade_no=input('orderId');
        $paytype=input('paytype');
        
        $om=new OrderModel();
        
        $order=$om->getPayOrder($out_trade_no);
        
        if($order['Status']=='OD'){
            return json(['code'=>1,'data'=>'','msg'=>'/ucenter/business/orders']);
        }
        
        return json(['code'=>-1,'data'=>'','msg'=>$order['Status']]);
    }
}