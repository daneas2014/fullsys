<?php

//2020
use AlibabaCloud\Client\AlibabaCloud as AlibabaCloud;
use AlibabaCloud\Client\Exception\ClientException as ClientException;
use AlibabaCloud\Client\Exception\ServerException as ServerException;
use com\Smtp;
use think\Db;

/**
 * 发送SDK结果记录
 */
function saveSendlog($type, $to, $detial)
{
    $log['sendtype'] = $type;
    $log['sendto'] = $to;
    $log['detail'] = $detial;
    $log['create_time'] = time();

    try {
        Db::name('sendlog')->insert($log);
    } catch (Exception $e) {

    }

}

/**
 * 阿里云云通信发送短息 2017
 * @param string $mobile    接收手机号
 * @param string $tplCode   短信模板ID
 * @param array  $tplParam  短信内容
 * @return array
 */
function sendMsg($mobile, $tplCode, $tplParam)
{
    AlibabaCloud::accessKeyClient('appid', 'secret')
        ->regionId('cn-hangzhou')
        ->asDefaultClient();

    $query = [
        'RegionId' => "cn-hangzhou",
        'PhoneNumbers' => $mobile,
        'SignName' => "签名",
        'TemplateCode' => $tplCode,
        'TemplateParam' => json_encode($tplParam),
    ];

    try {
        $result = AlibabaCloud::rpc()
            ->product('Dysmsapi')
        // ->scheme('https') // https | http
            ->version('2017-05-25')
            ->action('SendSms')
            ->method('POST')
            ->host('dysmsapi.aliyuncs.com')
            ->options([
                'query' => $query,
            ])
            ->request();

        saveSendlog('sms', $mobile, $result->Message);

        return $result;

    } catch (ClientException $e) {
        echo $e->getErrorMessage() . PHP_EOL;
    } catch (ServerException $e) {
        echo $e->getErrorMessage() . PHP_EOL;
    }
}

/**
 * 通过企业邮箱发送找回密码邮件
 * @param unknown $to
 * @param unknown $subject
 * @param unknown $msg
 */
function sendMail($to, $subject, $msg)
{

    $cc = "";
    $bcc = "";
    $firstone = "";

    //***************判断发送的是1个人还是一群人
    $list = explode(',', $to);
    if (count($list) > 1) {
        $firstone = $list[0];
        $bcc = str_replace($firstone . ',', '', $to);
    } else {
        $firstone = $to;
    }

    //******************** 配置信息【需要自行配置,下列账号是假的】 ********************************
    $smtpserver = "ssl://smtp.qiye.163.com"; //SMTP服务器,ignore ssl is normal send type
    $smtpserverport = 994; //SMTP服务器端口 25,994 is ssl send
    $smtpusermail = "webmaster@dmake.cn"; //SMTP服务器的用户邮箱
    $smtpemailto = $firstone; //发送给谁
    $smtpuser = "webmaster@dmake.cn"; //SMTP服务器的用户帐号，注：部分邮箱只需@前面的用户名
    $smtppass = "VHKwUbjv5S7gSn65"; //"heyueMAX00";//SMTP服务器的授权码
    $mailtitle = $subject; //邮件主题
    $mailcontent = $msg; //邮件内容
    $mailtype = "HTML"; //邮件格式（HTML/TXT）,TXT为文本邮件

    //************************ 配置信息 ****************************
    $smtp = new Smtp($smtpserver, $smtpserverport, true, $smtpuser, $smtppass); //这里面的一个true是表示使用身份验证,否则不使用身份验证.
    $smtp->debug = false; //是否显示发送的调试信息
    $state = $smtp->sendmail($smtpemailto, $smtpusermail, $mailtitle, $mailcontent, $mailtype, $cc, $bcc);

    saveSendlog('mail', $to, $state);

    if ($state == "" && $bcc == "") {
        return false;
    }

    // 群发的时候不管3721都返回正确
    if ($state == "" && $bcc != "") {
        debuggerfile($subject . '存在发送失败的人', 'maillsend');
        return true;
    }

    return true;
}
