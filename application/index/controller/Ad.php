<?php
namespace app\index\controller;

use think\Controller;
use app\index\model\AdModel;
use app\index\model\ReportModel;

class Ad extends Controller
{

    public function show()
    {
        $pkey = input('get.pkey');
        
        if($pkey==null||$pkey==''){
            echo '';
            exit();
        }
        
        $ad = new AdModel();
        
        $list = $ad->getAd($pkey);
        
        $this->assign('list', $list);
        
        if ($list != null) {
            $this->assign('type', $list[0]['Type']);
        }
        
        return $this->fetch();
    }

    public function adhtml()
    {
        $pkey = input('get.pkey');
        
        if($pkey==null||$pkey==''){
            echo '';
            exit();
        }
                
        $ad = new AdModel();
        
        $list = $ad->getAdHtml($pkey);        

        echo $list;
    }

    public function ad()
    {
        $url = input('get.u');
        $id = input('get.id');
        
        
        $data['ip']=request()->ip();
        $data['customerid']=session('mid');
        $data['create_time']=time();
        $data['adid']=$id;
        $data['jumpurl']=$url;
        
        $r = new ReportModel();
        $r->addBit(cookie('rptoken'), $id,$data);      
        
        
        return redirect($url);
    }
}