<?php
namespace app\index\controller;

use app\common\model\SeoBase;
use think\Controller;

class Base extends Controller {

	/**
	 * 前端初始化鉴权、SEO赋值
	 *
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2022-05-20
	 */
	public function _initialize() {
		
		$userinfo = session('memberinfo');
		$this->assign('u', $userinfo);

		$module = strtolower(request()->module());
		$controller = strtolower(request()->controller());
		$action = strtolower(request()->action());

		$sb = new SeoBase();
		$sysseo = $sb->pageseo($module, $controller, $action);
		if ($sysseo) {
			$this->assign(['title' => $sysseo->title, 'keywords' => $sysseo->keywords, 'description' => $sysseo->description]);
		} else {
			$this->assign(['title' => '', 'keywords' => '', 'description' => '']);
		}
	}
}