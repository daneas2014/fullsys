<?php
namespace app\index\controller;

use app\common\model\TaobaoBase;
use think\Controller;
use think\Db;

/**
 * devexpress的个版本在线汉化工具
 * @author daneas
 *
 */
class Test extends Controller {

	public function index() {


		// $bt = new \app\iotadmin\model\BTrackModel();

		// $bt->addEntity();
		// $res = $bt->addEntityPoint('渝DGXXX0', '106.09069','29.864245', 1711889778-60*3, $coord_type_input = 'bd09ll');
		// $res = $bt->addEntityPoint('渝DGXXX0', '106.090258','29.865091', 1711889778-60*4, $coord_type_input = 'bd09ll');
		// $res = $bt->addEntityPoint('渝DGXXX0', '106.088031','29.864762', 1711889778-60*5, $coord_type_input = 'bd09ll');
		// $res = $bt->addEntityPoint('渝DGXXX0', '106.087977','29.859265', 1711889778-60*6, $coord_type_input = 'bd09ll');
		// $res = $bt->addEntityPoint('渝DGXXX0', '106.081814','29.855819', 1711889778-60*7, $coord_type_input = 'bd09ll');
		// $res = $bt->addEntityPoint('渝DGXXX0', '106.077592','29.853956', 1711889778-60*8, $coord_type_input = 'bd09ll');
		// $res = $bt->addEntityPoint('渝DGXXX0', '106.069921','29.851215', 1711889778-60*9, $coord_type_input = 'bd09ll');
		// $res = $bt->addEntityPoint('渝DGXXX0', '106.06922','29.84863', 1711889778-60*10, $coord_type_input = 'bd09ll');
		// $res = $bt->addEntityPoint('渝DGXXX0', '106.065932','29.847973', 1711889778-60*11, $coord_type_input = 'bd09ll');
		// $res = $bt->addEntityPoint('渝DGXXX0', '106.06516','29.850902', 1711889778-60*12, $coord_type_input = 'bd09ll');
		// $res = $bt->addEntityPoint('渝DGXXX0', '106.060614','29.85405', 1711889778-60*13, $coord_type_input = 'bd09ll');
		// $res = $bt->addEntityPoint('渝DGXXX0', '106.053248','29.859625', 1711889778-60*14, $coord_type_input = 'bd09ll');
		// $res = $bt->addEntityPoint('渝DGXXX0', '106.0474','29.864425', 1711889778-60*15, $coord_type_input = 'bd09ll');
		// $res = $bt->addEntityPoint('渝DGXXX0', '106.044184','29.872341', 1711889778-60*16, $coord_type_input = 'bd09ll');
		// $res = $bt->addEntityPoint('渝DGXXX0', '106.044508','29.874885', 1711889778-60*17, $coord_type_input = 'bd09ll');
		// $res = $bt->addEntityPoint('渝DGXXX0', '106.046493','29.880092', 1711889778-60*18, $coord_type_input = 'bd09ll');
		// $res = $bt->addEntityPoint('渝DGXXX0', '106.047463','29.882268', 1711889778-60*19, $coord_type_input = 'bd09ll');
		// $res = $bt->addEntityPoint('渝DGXXX0', '106.050455','29.883583', 1711889778-60*20, $coord_type_input = 'bd09ll');

		// print("返回 $res");

		// $gt = new \app\iotadmin\model\GTrackModel();

		// $res = $gt->addEntityPoint('870564511', '106.09069,29.864245', 1711889778 - 60 * 3, 20);
		// $res = $gt->addEntityPoint('870564511', '106.090258,29.865091', 1711889778 - 60 * 4, 20);
		// $res = $gt->addEntityPoint('870564511', '106.088031,29.864762', 1711889778 - 60 * 5, 20);
		// $res = $gt->addEntityPoint('870564511', '106.087977,29.859265', 1711889778 - 60 * 6, 20);
		// $res = $gt->addEntityPoint('870564511', '106.081814,29.855819', 1711889778 - 60 * 7, 20);
		// $res = $gt->addEntityPoint('870564511', '106.077592,29.853956', 1711889778 - 60 * 8, 20);
		// $res = $gt->addEntityPoint('870564511', '106.069921,29.851215', 1711889778 - 60 * 9, 20);
		// $res = $gt->addEntityPoint('870564511', '106.06922,29.84863', 1711889778 - 60 * 10, 20);
		// $res = $gt->addEntityPoint('870564511', '106.065932,29.847973', 1711889778 - 60 * 11, 20);
		// $res = $gt->addEntityPoint('870564511', '106.06516,29.850902', 1711889778 - 60 * 12, 20);
		// $res = $gt->addEntityPoint('870564511', '106.060614,29.85405', 1711889778 - 60 * 13, 20);
		// $res = $gt->addEntityPoint('870564511', '106.053248,29.859625', 1711889778 - 60 * 14, 20);
		// $res = $gt->addEntityPoint('870564511', '106.0474,29.864425', 1711889778 - 60 * 15, 20);
		// $res = $gt->addEntityPoint('870564511', '106.044184,29.872341', 1711889778 - 60 * 16, 20);
		// $res = $gt->addEntityPoint('870564511', '106.044508,29.874885', 1711889778 - 60 * 17, 20);
		// $res = $gt->addEntityPoint('870564511', '106.046493,29.880092', 1711889778 - 60 * 18, 20);
		// $res = $gt->addEntityPoint('870564511', '106.047463,29.882268', 1711889778 - 60 * 19, 20);
		// $res = $gt->addEntityPoint('870564511', '106.050455,29.883583', 1711889778 - 60 * 20, 20);
		// print("返回 $res");

		// echo strtotime("now").'<br/>';
		// echo strtotime("-1 week").'<br/>';
		// echo strtotime("-1 year").'<br/>';
		return $this->fetch();
	}

	public function tao() {
		$tao = new TaobaoBase();

		//$res = $tao->getMaterialitems(607902437475, 0, 0, 1, 13256);

		$res = $tao->getMaterialitems(0, 0, 0, 1, 31519);

		// $res=$tao->getMaterialitems(0,2012724034,0,1,31539);

		dump($res['favorites_list']['favorites_detail']['favorites_id']);
	}

	public function calendar() {
		return $this->fetch();
	}

	public function fl() {
		if (request()->IsGet()) {
			$c = input('c');
			$m = input('m');
			$r = input('r');

			$res = 0;

			for ($i = 0; $i < $m; $i++) {
				$res = ($res + $c) * (1 + $r);
			}

			echo $res;
		}
	}

	public function fontawesomev() {
		$str = file_get_contents('./fonta.txt');

		$arr = explode(',', $str);

		$category = 'writingf';
		return;

		$new = [];
		foreach ($arr as $a) {
			$temp['icon'] = $a;
			$temp['category'] = $category;
			$temp['fa'] = explode(' ', $a)[0];

			array_push($new, $temp);
		}

		$res = Db::name('op_fontawesomev')->insertAll($new, true);
		echo $category;
		dump($res);
	}

	// 这个方法可以翻译devextreme,更换file地址，然后执行translateall就能全部翻译了
	public function transjson() {
		$page = input('page') ? input('page') : 1;

		echo 'json的数据已经采集完了的，不要采集了';
		exit();

		$file = './devexpress/jsonresources/dx-analytics-core.zh-Hans.json';
		$file1 = './devexpress/jsonresources/dx-dashboard.zh-Hans.json';
		$file2 = './devexpress/jsonresources/dx-reporting.zh-Hans.json';
		$file3 = './devexpress/jsonresources/dx-rich.zh-Hans.json';
		$file4 = './devexpress/jsonresources/dx-spreadsheet.zh-Hans.json';

		$json_string = file_get_contents($file);

		$data = json_decode($json_string, true);

		$list = $this->translist($data, $file);

		$length = count($data);

		$size = 1000;

		$thislist = [];

		$rowindex = ($page - 1) * $size;

		$rowend = $page * $size > $length ? $length : $page * $size;

		for ($rowindex; $rowindex < $rowend; $rowindex++) {
			array_push($thislist, $list[$rowindex]);
		}

		$rows = Db::name('op_devexpress')->insertAll($thislist);

		if ($page > ($length / $size)) {
			echo '翻译完毕';
			exit();
		}

		// 一次处理几千条要死，所以一次处理100条以内
		return $this->success("搞完了 $rows 条", '/index/test/index?page=' . ($page + 1));
	}

	/**
	 * 这个是采集resx词条的步骤和方法
	 */
	public function transresx() {
		// 1. 遍历文件夹

		// 2. 读取文件夹中的.resx文件

		// 3. 记录.resx中的词条

		// 4. 写入.lock文件
	}

	/**
	 * devextreme文件的汉化写入到json文件中,记得格式化文件，删除最后一个","
	 */
	public function wirtetojson() {
		echo '汉化完了，不要点了';
		exit();

		$file = './devexpress/jsonresources/dx-analytics-core.zh-Hans.json';
		$file2 = './devexpress/jsonresources/dx-dashboard.zh-Hans.json';
		$file3 = './devexpress/jsonresources/dx-reporting.zh-Hans.json';
		$file4 = './devexpress/jsonresources/dx-rich.zh-Hans.json';
		$file5 = './devexpress/jsonresources/dx-spreadsheet.zh-Hans.json';

		$list = Db::name('op_devexpress')->where('dirpath', $file)
			->field('dvid,dvwords,mwords,translate_time')
			->select();

		$txt = '';

		$jsons = [];
		foreach ($list as $l) {
			$json[$l['dvid']] = $l['translate_time'] <= 0 ? $l['dvwords'] : $l['mwords'];
			array_push($jsons, $json);
			unset($json);
		}
		$txt = json_encode($jsons, JSON_UNESCAPED_UNICODE);

		file_put_contents(str_replace('jsonresources', 'json resources', $file), $txt);

		echo '生成完了' . $file;
	}

	/**
	 * 追加词条到zh-Hans.resx文件中去
	 */
	public function wirtetoresx() {}

	/**
	 * 一直翻译词条到结束
	 */
	public function translateall() {
		$db = Db::name('op_devexpress');

		$list = $db->where('mwords', '')
			->where('dvwords', '<>', '')
			->where('LENGTH(dvwords)>10')
			->limit(10)
			->select();

		if ($list == null) {
			echo '翻译完了';
			exit();
		}

		foreach ($list as $d) {
			$trans = translate($d['dvwords'], 'en', 'zh');
			if ($trans && array_key_exists('trans_result', $trans)) {
				$vtrans = $trans['trans_result'][0]['dst'];
				$db->where('id', $d['id'])->update([
					'mwords' => $vtrans,
					'translateid' => 0,
					'translate_time' => time(),
				]);
			}
		}

		return $this->success('又翻译了一页', '/index/test/translateall');
	}

	/**
	 * 把devextreme的格式文件转成可以写入的，也可以用于resx文件的词条写入
	 *
	 * @param unknown $data
	 * @param unknown $filepath
	 * @return array
	 */
	private function translist($data, $filepath) {
		$list = [];
		$errors = [];

		foreach ($data as $key => $value) {

			try {
				$n = $key;
				$v = $value;
			} catch (\Exception $e) {
				array_push($errors, $key);
			}

			$vtrans = '';

			/* 有中文就不翻译 */
			if (preg_match('/[^\x00-\x80]/', $v)) {
				$vtrans = $v;
			}

			$item['dvid'] = $n;
			$item['dvwords'] = $v;
			$item['mwords'] = $vtrans;
			$item['status'] = 0;
			$item['version'] = '';
			$item['groupname'] = '';
			$item['dirpath'] = $filepath;

			array_push($list, $item);
		}

		return $list;
	}
}
