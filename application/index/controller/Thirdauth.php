<?php

namespace app\index\controller;

use think\Controller;

class Thirdauth extends Controller {

	protected $jumpurl = '';

	/**
	 * 避免重复设置，直接初始化时解决跳转问题
	 *
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2022-05-20
	 */
	public function _initialize() {
		$jumpurl = $_SERVER['HTTP_REFERER'];

		if (isset($_SERVER['HTTP_REFERER'])) {
			$this->jumpurl = $_SERVER['HTTP_REFERER'];
		} else {
			$this->jumpurl = $_SERVER['SERVER_NAME'];
		}
	}

	public function qqauth() {
		setcookie('qqurl', $this->jumpurl, time() + 10 * 60, '/');
		require_once EXTEND_PATH . "uauth/qqsdk/qqConnectAPI.php";
		$qc = new \QC();
		$qc->qq_login();
	}

	public function qqcallback() {
		require_once EXTEND_PATH . "uauth/qqsdk/qqConnectAPI.php";
		$qc = new \QC();
		$acs = $qc->qq_callback();
		$oid = $qc->get_openid();
		$qc = new \QC($acs, $oid);
		$arr = $qc->get_user_info();

		$userinfo = ["openid" => $oid, "accesstoken" => "", "nickname" => $arr["nickname"], "sex" => $arr["gender"], "figureurl" => $arr['figureurl_2'], 'jump' => $_COOKIE['qqurl']];
		$script = 'var obj=' . json_encode($userinfo, JSON_FORCE_OBJECT) . ';';

		echo @"
        <!DOCTYPE HTML>
        <html>
        <head>
            <meta charset=\"UTF-8\">
            <script src=\"https://cdn.bootcss.com/jquery/3.4.1/jquery.min.js\"></script>
            <script src=\"/static/script/utils.js\"></script>
        </head>
        <body>
            <script type=\"text/javascript\">
                $script
                JsonAPI({
                        type: 'qq',
                        appid: obj.openid,
                        accesstoken: obj.accesstoken,
                        nickname: obj.nickname,
                        sex: obj.sex,
                        figureurl: obj.figureurl
                    }, function(res) {
                        location.href=obj.jump;
                    }, '/ucenter/api/login');
            </script>
        </body>
        </html>";
	}

	public function sinaauth() {
		$c = config('sinaauth');

		include_once EXTEND_PATH . 'uauth/sinasdk/saetv2.ex.class.php';
		$o = new \SaeTOAuthV2($c['appkey'], $c['appsecret']);
		$code_url = $o->getAuthorizeURL($c['callback']);

		$jumpurl = $_SERVER['HTTP_REFERER'];
		if (strstr($jumpurl, 'ucenter/uauth/login')) {
			$jumpurl = $_SERVER['SERVER_NAME'];
		}
		setcookie('sinaurl', $this->jumpurl, time() + 10 * 60, '/');
		echo "<meta http-equiv='Refresh' content='1,url=" . $code_url . "' >";

	}

	public function sinacallback() {

		$c = config('sinaauth');
		include_once EXTEND_PATH . 'uauth/sinasdk/saetv2.ex.class.php';
		$o = new \SaeTOAuthV2($c['appkey'], $c['appsecret']);

		if (isset($_REQUEST['code'])) {
			$keys = array();
			$keys['code'] = $_REQUEST['code'];
			$keys['redirect_uri'] = $c['callback'];
			try {
				$token = $o->getAccessToken('code', $keys);
			} catch (\OAuthException $e) {
				echo $e->getMessage();
				die();
			}
		}

		if ($token) {
			setcookie('weibojs_' . $o->client_id, http_build_query($token));

			$c = new \SaeTClientV2($c['appkey'], $c['appsecret'], $token['access_token']);
			$ms = $c->home_timeline();
			$uid_get = $c->get_uid();
			$uid = $uid_get['uid'];
			$user_message = $c->show_user_by_id($uid); //根据ID获取用户等基本信息

			$userinfo = ["openid" => $user_message['id'], "accesstoken" => $token, "nickname" => $user_message['name'], "sex" => ($user_message['gender'] == 'm' ? '男' : '女'), "figureurl" => $user_message['avatar_large'], 'jump' => $_COOKIE['sinaurl']];
		}

		$script = 'var obj=' . json_encode($userinfo, JSON_FORCE_OBJECT) . ';';

		echo @"
        <html>
            <head>
                <meta charset=\"UTF-8\">
                <script src=\"https://cdn.bootcss.com/jquery/3.4.1/jquery.min.js\"></script>
                <script src=\"/static/script/utils.js\"></script>
            </head>
            <body>
                <script type=\"text/javascript\">
                    $script
                    JsonAPI({
                        type: 'wb',
                        appid: obj.openid,
                        accesstoken: obj.accesstoken,
                        nickname: obj.nickname,
                        sex: obj.sex,
                        figureurl: obj.figureurl
                    }, function (res) {
                        location.href=obj.jump;
                    }, '/ucenter/api/login');
                </script>
            </body>
            </html>
        ";

	}
}