<?php
namespace app\index\controller;

use baiduspeak\AipSpeech;
use think\Controller;

class Tools extends Controller
{
    public function wordtovoice()
    {
        if (request()->isPost()) {

            $client = new AipSpeech('23813466', 'Axhbf2jvaxA3c98oOGRIgVWC', 'h59RSRNOojE9tlagrnIZ4tD7sNq9EUod');
            /*
            参数    可需    描述
            tex    必填    合成的文本，使用UTF-8编码。小于2048个中文字或者英文数字，文本在百度服务器内转换为GBK后，长度必须小于4096字节（5003、5118发音人需小于512个中文字或者英文数字）
            tok    必填    开放平台获取到的开发者access_token（见上面的“鉴权认证机制”段落）
            cuid    必填    用户唯一标识，用来计算UV值。建议填写能区分用户的机器 MAC 地址或 IMEI 码，长度为60字符以内
            ctp    必填    客户端类型选择，web端填写固定值1
            lan    必填    固定值zh。语言选择,目前只有中英文混合模式，填写固定值zh
            spd    选填    语速，取值0-15，默认为5中语速
            pit    选填    音调，取值0-15，默认为5中语调
            vol    选填    音量，取值0-15，默认为5中音量
            per（基础音库）    选填    度小宇=1，度小美=0，度逍遥（基础）=3，度丫丫=4
            per（精品音库）    选填    度逍遥（精品）=5003，度小鹿=5118，度博文=106，度小童=110，度小萌=111，度米朵=103，度小娇=5
            aue    选填    3为mp3格式(默认)； 4为pcm-16k；5为pcm-8k；6为wav（内容同pcm-16k）; 注意aue=4或者6是语音识别要求的格式，但是音频内容不是语音识别要求的自然人发音，所以识别效果会受影响。
             */
            $txt = input('param.code');
            $spd = input('param.spd');
            $per = input('param.per');

            $result = $client->synthesis($txt, 'zh', 1, array(
                'vol' => 12,
                'per' => $per,
                'spd' => $spd,
                'pit' => 6,
            ));

            $path = '/audio/' . time() . '.mp3';

            // 识别正确返回语音二进制 错误则返回json 参照下面错误码
            if (!is_array($result)) {
                file_put_contents('.' . $path, $result);
            }

            return getJsonCode($path, '生成完毕，浏览器将在新标签页打开音频，请自行保存');
        }

        return $this->fetch();
    }
}
