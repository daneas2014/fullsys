<?php
namespace app\index\controller;

use app\index\model\AdModel;
use app\index\model\ArticleModel;
use app\index\model\SearchModel;

class Index extends Base
{

    public function index()
    {        

        $ad = new AdModel();

        $am = new ArticleModel();
        $news[0] = $am->getRelationArticles(1, 4);
        $news[1] = $am->getRelationArticles(2, 4);
        $news[2] = $am->getRelationArticles(3, 5);

        $assign = [
            'indexad' => $ad->getAd('index-a1'),
            'news' => $news,
        ];

        return $this->view->assign($assign)->fetch();
    }

    /**
     * 模糊搜索
     *
     * @return void
     * @author dmakecn@163.com
     * @since 2020-01-03
     */
    public function search()
    {
        if (request()->isPost()) {
            $type = input('type');
            $key = input('key');
            $list = null;

            $sm = new SearchModel();

            $list = $sm->$type($key);

            return json([
                'code' => 1,
                'data' => $list,
                'msg' => '',
            ]);
        }
    }
}
