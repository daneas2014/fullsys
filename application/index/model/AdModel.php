<?php
namespace app\index\model;

use app\common\model\AdBase;

class AdModel extends AdBase
{

    /**
     * 输出广告列表
     *
     * @param unknown $pkey
     * @return string|mixed|\think\cache\Driver|boolean
     */
    public function getAd($pkey)
    {
        $key = 'GAD_' . $pkey;

        $list = dcache(CACHE_AD, $key);

        if ($list) {
            return $list;
        }

        $list = $this->getAdByKey($pkey);

        if ($list == null) {
            return '';
        }

        dcache(CACHE_AD, $key, $list);

        return $list;
    }

    /**
     * 输出广告代码
     *
     * @param unknown $pkey
     * @return string|string|mixed|\think\cache\Driver|boolean
     */
    public function getAdHtml($pkey)
    {
        $key = 'GADH_' . $pkey;

        $adhtml = dcache(CACHE_AD, $key);

        if ($adhtml) {
            return $adhtml;
        }

        $list = $this->getAdByKey($pkey);

        if ($list == null || count($list) == 0) {
            return '';
        } else {
            $adhtml = '<div class="ad">' . $this->buildAdhtmlList($list) . '</div>';
        }

        dcache(CACHE_AD, $key, $adhtml);

        return $adhtml;
    }

    /**
     * 按照图片、轮播、文字链接的方式组装广告
     *
     * @param unknown $list
     */
    private function buildAdhtmlList($list)
    {
        $type = $list[0]['type'];
        $adhtml = '';
        switch ($type) {
            case "图片":
                foreach ($list as $v) {
                    $adhtml = $adhtml . '<a href="/ad?id=' . $v['id'] . '&u=' . $v['url'] . '" target="_blank"><img src="' . $v['imagepath'] . '" alt="' . $v['title'] . '"/></a>';
                }
                break;
            case "轮播":
                $temp = '';
                foreach ($list as $v) {
                    $temp .= '<div class="swiper-slide"><a href="/ad?id=' . $v['id'] . '&u=' . $v['url'] . '" target="_blank"><img src="' . $v['imagepath'] . '" alt="' . $v['title'] . '"/></a></div>';
                }

                $adhtml = '<div class="swiper-container"><div class="swiper-wrapper">' . $temp . ' </div><div class="swiper-pagination"></div></div><script>var swiper = new Swiper(".swiper-container", {pagination: {el: ".swiper-pagination",},});</script>';

                break;
            case "文字":
                foreach ($list as $v) {
                    $adhtml = $adhtml . '<a href="/ad?id=' . $v['id'] . '&u=' . $v['url'] . '" target="_blank">' . $v['title'] . '</a>';
                }
                break;
            case "代码":
                foreach ($list as $v) {
                    $adhtml = $adhtml . $v->Content;
                }
                break;
            case "弹窗":
                $ad = $list[0];
                $closestyle = 'cursor: pointer;position: relative;right: 0px;top: 0px;float: RIGHT;font-size: 1.45em;color: #888;border: 1px solid #888;font-family: sans-serif;width: 30px;text-align: center;height: 30px;border-radius: 20px;';
                $pop = '<div style="position: fixed; z-index: 9999; right: 5px; bottom: 50px;" id="pop' . $ad['id'] . '">' .
                    '<span style="' . $closestyle . '" onclick="$(this).parent().remove();">X</span>' .
                    '<a href="/ad?id=' . $ad['id'] . '&u=' . $ad['url'] . '" target="_blank"><img src="' . $ad['imagepath'] . '" width="179px" alt="' . $ad['title'] . '"></a>' .
                    '</div>';
                $adhtml = "<script>
                        	if($('body').width() > 1200) {
                        	    var righpop = '" . $pop . "';
                        		$('body').append(righpop);
                        	}
                         </script>";
                break;
        }

        return $adhtml;
    }
}
