<?php
namespace app\index\model;

use think\Model;
use think\Db;

class SaleOnlineModel extends Model
{

    protected $name = 'product_license';

    public function Recommends($productid = 0, $categoryid = 0, $limit = 5)
    {
        $db=Db::name('product');
        
        if ($productid > 0) {
            $tt = $db->where('Id', $productid)
                ->field('categoryid')
                ->find();
            
            $categoryid = $tt['categoryid'];
        }
        
        $products = $db->alias('a')
            ->join('product_license b', 'a.id=b.productid', 'left')
            ->where('categoryid', $categoryid)
            ->where('activitytype', '在线订购')
            ->where('activityprice', '>', 0)
            ->field('DISTINCT(a.Id)')
            ->select();
        
        $productids = [];
        
        if ($products == null||count($products)<4) {
            array_push($productids, 1859,2380,3016,2294,2551);
        } else {
            foreach ($products as $p) {
                array_push($productids, $p['Id']);
            }
        }
                        
        $child = "(select productid,MIN(activityprice) as ap,MAX(listprice) AS lp  from product_license where listprice>0 and activitytype='在线订购'  group by productid ) as b ";
        
        $list = $db->alias('a')
            ->field('a.name as title,a.imagepath as imagepath,a.descriptionword as description,b.productid as pid,ap,lp')
            ->join($child, 'a.Id=b.ProductId', 'inner')
            ->where('productid', 'in', $productids)
            ->select();
        
        return $list;        
    }
}