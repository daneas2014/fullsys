<?php
namespace app\index\model;

use app\common\model\ArticleBase;
use app\common\model\RelationsBase;
use think\Db;
use think\Model;

class ArticleModel extends ArticleBase
{

    /**
     * 获得所有文章分类
     *
     * @return void
     * @author dmakecn@163.com
     * @since 2021-01-21
     */
    public function getArticleCates()
    {

        $key = 'AGS';
        $data = dcache(CACHE_ARTICLE, $key);
        if ($data) {
            return $data;
        }

        $data = Db::name('article_category')->order('sort desc')->select();
        dcache(CACHE_ARTICLE, $key, $data);

        return $data;
    }

    /**
     * 获取头条文章
     * @param unknown $type
     * @param unknown $page
     * @param unknown $limit
     */
    public function getHeadLines($type = 0, $page = 1, $limit = 10)
    {
        $map['isheadline'] = 1;
        if ($type > 0) {
            $map['categoryid'] = $type;
        }

        $key = 'GAHL_' . $type . '_' . $page . '_' . $limit;

        $list = dcache(CACHE_ARTICLE, $key);

        if ($list) {
            return $list;
        }

        $list = $this->field('id,title,imagepath,summary,create_time')->where($map)->page($page, $limit)->order('id desc')->select();

        dcache(CACHE_ARTICLE, $key, $list);

        return $list;
    }

    /**
     * 文章滚动画面
     *
     * @param number $cateid
     * @param number $productid
     * @param number $supplierid
     * @param number $limit
     */
    public function getScroll($cateid = 0, $productid = 0, $supplierid = 0, $limit = 5)
    {
        $key = 'GAS_' . $cateid . '_' . $productid . '_' . $supplierid . '_' . $limit;

        $list = dcache(CACHE_ARTICLE, $key);

        if ($list) {
            return $list;
        }

        if ($cateid > 0) {
            $list = $this->field('id,title,imagepath,creativetype,summary,create_time')
                ->where([
                    'categoryid' => $cateid,
                    'isheadline' => 1,
                ])
                ->limit($limit)
                ->order('Id desc')
                ->select();
        } else if ($productid > 0) {
            $list = $this->alias('a')
                ->field('a.Id,a.title,a.imagepath,a.creativetype,a.summary,a.create_time')
                ->join('article_relations b', 'a.id=b.articleid', 'right')
                ->where([
                    'b.productid' => $productid,
                    'a.isheadline' => 1,
                ])
                ->limit($limit)
                ->order('a.Id desc')
                ->select();
        } else if ($supplierid > 0) {
            $list = $this->alias('a')
                ->field('a.Id,a.title,a.imagepath,a.creativetype,a.summary,a.create_time')
                ->join('article_relations b', 'a.id=b.articleid', 'right')
                ->where([
                    'b.supplierid',
                    $supplierid,
                    'a.isheadline' => 1,
                ])
                ->limit($limit)
                ->order('a.Id desc')
                ->select();
        } else {
            $map['imagepath'] = ['<>', 'Null'];
            $map['categoryid'] = ['<>', 8];
            $map['isheadline'] = 1;
            $list = $this->field('id,title,imagepath,creativetype,summary,create_time')
                ->where($map)
                ->limit($limit)
                ->order('id desc')
                ->select();
        }

        if ($list == null) {
            return null;
        }

        dcache(CACHE_ARTICLE, $key, $list);

        return $list;
    }

    /**
     * 获取文章详情，包含了Attr，Creator，Siblings
     *
     * @param unknown $articleid
     * @return mixed|\think\cache\Driver|boolean|unknown[]
     */
    public function getDetail($articleid)
    {
        $key = 'GAD_' . $articleid;

        $detail = dcache(CACHE_ARTICLE, $key);

        if ($detail) {
            return $detail;
        }

        $detail = $this->getArticle($articleid);

        if ($detail == null) {
            return null;
        }

        $detail['attr'] = $this->getAttribute($articleid);
        $silbings = $this->getSiblings($articleid, $detail['categoryid']);
        if (count($silbings) == 2) {
            $detail['pre'] = $silbings[0];
            $detail['next'] = $silbings[1];
        }
        if (count($silbings) == 1) {
            $detail['pre'] = $silbings[0];
            $detail['next'] = null;
        }
        if (count($silbings) == 0) {
            $detail['pre'] = null;
            $detail['next'] = null;
        }

        dcache(CACHE_ARTICLE, $key, $detail);

        return $detail;
    }

    public function getArticlProducts($articleid)
    {
        $key = 'GAPS_' . $articleid;

        $products = dcache(CACHE_ARTICLE, $key);

        if ($products) {
            return $products;
        }

        $rm = new RelationsBase();
        $products = $rm->getArticleProductRelation($articleid);

        dcache(CACHE_ARTICLE, $key, $products);

        return $products;
    }

    public function getProductArticles($productid, $limit = 10)
    {
        $key = 'GPAS_' . $productid;

        $articles = dcache(CACHE_ARTICLE, $key);

        if ($articles) {
            return $articles;
        }

        $articles = $this->alias('a')
            ->field('a.id,a.title,a.imagepath,a.create_time,summary')
            ->join('article_relations b', 'a.id=b.articleid', 'right')
            ->where('b.productid', $productid)
            ->order('id desc')
            ->limit($limit)
            ->select();

        dcache(CACHE_ARTICLE, $key, $articles);

        return $articles;
    }

    /**
     * 获取文章绑定的供应商
     *
     * @param unknown $articleid
     * @return \app\common\model\unknown|mixed|\think\cache\Driver|boolean
     */
    public function getArticleSuppliers($articleid)
    {
        $key = 'GAS' . $articleid;

        $suppliers = dcache(CACHE_ARTICLE, $key);

        if ($suppliers) {
            return $suppliers;
        }

        $rm = new RelationsBase();
        $suppliers = $rm->getArticleSupplierRelation($articleid);

        dcache(CACHE_ARTICLE, $key, $suppliers);

        return $suppliers;
    }

    /**
     * 获取安全部系列教程
     *
     * @return mixed|\think\cache\Driver|boolean
     */
    public function getArticleAllGroups($limit = 10)
    {
        $key = 'GAAG' . $limit;

        $groups = dcache(CACHE_ARTICLE, $key);

        if ($groups) {
            return $groups;
        }

        $groups = Db::name('article_group')->alias('a')
            ->join('(select groupid,id as articleid,imagepath from article where groupid>0 order by articleid desc) b', 'a.id=b.groupid', 'left')
            ->order('articleid desc')
            ->limit($limit)
            ->select();

        dcache(CACHE_ARTICLE, $key, $groups);

        return $groups;
    }

    /**
     * 获取一个标准的系列教程明细
     *
     * @param unknown $groupid
     * @return mixed|\think\cache\Driver|boolean
     */
    public function getArticleGroups($groupid, $limit = 10, $articleid = 0)
    {
        if ($groupid == 0) {
            return null;
        }

        $key = 'GAGS' . $groupid . '_' . $limit . '_' . $articleid;

        $groups = dcache(CACHE_ARTICLE, $key);

        if ($groups) {
            return $groups;
        }

        if ($articleid > 0) {
            $limit = ceil($limit / 2);
            $left = $this->field('id,title,createdtime')->where(['groupid' => $groupid, 'Id' => ['<', $articleid]])->order('id desc')->limit($limit)->select();

            $newleft = [];
            for ($i = count($left); $i > 0; $i--) {
                array_push($newleft, $left[$i - 1]);
            }

            $right = $this->field('id,title,createdtime')->where(['groupid' => $groupid, 'Id' => ['>', $articleid]])->order('id asc')->limit($limit)->select();
            $groups = array_merge($newleft, $right);
        } else {
            $groups = $this->field('id,title,createdtime')->where('groupid', $groupid)->order('id asc')->limit($limit)->select();
        }

        dcache(CACHE_ARTICLE, $key, $groups);

        return $groups;
    }

    /**
     * 获取相关文章
     *
     * @param number $tag
     *            大分类1-8
     * @param number $limit
     *            展现数量
     * @param unknown $keywords
     *            关键词
     * @param string $order
     *            排序规则
     * @return NULL|unknown list
     */
    public function getRelationArticles($categoryid = 0, $limit = 10, $keywords = null, $order = 'id desc', $page = 1)
    {
        $key = 'RA_' . $categoryid . '_' . $limit . '_' . $keywords . '_' . $page;

        $articles = dcache(CACHE_ARTICLE, $key);
        if ($articles) {
            return $articles;
        }

        $map['ispublish'] = 1;
        if ($categoryid > 0) {
            $map['categoryid'] = $categoryid;
        }
        if ($keywords != null) {
            $map['a.title'] = [
                'like',
                '%' . $keywords . '%',
            ];
        }

        $articles = $this->alias('a')
            ->field('a.id,a.title,creativetype,a.create_time,categoryid,imagepath,c.name as catename,summary,b.clickcount')
            ->join('article_attribute b', 'b.articleid=a.id', 'left')
            ->join('article_category c', 'a.categoryid=c.id', 'left')
            ->where($map)
            ->page($page, $limit)
            ->order($order)
            ->select();

        if ($articles == null) {
            return null;
        }

        dcache(CACHE_ARTICLE, $key, $articles);

        return $articles;
    }

    /**
     * 获取文章相关的推荐
     *
     * @param unknown $articleid
     */
    public function getArticleRelations($articleid, $categoryid)
    {

        $key = 'GARS_' . $articleid . '_' . $categoryid;

        $data = dcache(CACHE_ARTICLE, $key);

        if ($data) {
            return $data;
        }

        $article = $this->getRelationArticles($categoryid, 5);

        $campaigns = Db::name('campaign')->field('id,title,imagepath')->limit(4)->order('id desc')->select();

        $rm = new RelationsBase();
        $product = $rm->getArticleRelations($articleid, 5);
        if ($product == null || count($product) < 5) {
            $product = $rm->getHotProductRelations(5);
        }

        $video = $rm->getArticleVideoRelations($articleid);
        if ($video == null || count($video) < 10) {
            $video = Db::name('video')->order('id desc')->limit(10)->select();
        }

        $data['video'] = $video;
        $data['campaign'] = $campaigns;
        $data['product'] = $product;
        $data['article'] = $article;

        dcache(CACHE_ARTICLE, $key, $data);

        return $data;
    }

    /**
     * 获取产品分类相关的文章
     * @param unknown $categoryid
     * @param unknown $page
     * @param unknown $pagesize
     * @param unknown $orderby
     */
    public function getProductCategoryArticles($categoryid = 0, $page = 1, $pagesize = 10, $orderby = '')
    {
        $order = 'id desc';
        if ($orderby == 'hot') {
            $order = 'isheadline,createdtime desc';
        }

        if ($categoryid > 0) {
            $map['a.categoryid'] = $categoryid;
        }

        return $this->alias('a')
            ->field('distinct(a.id),a.createdtime,a.title,a.imagepath,a.summary,a.isheadline')
            ->join('(select articleid,productid from article_relations where productid>0 group by articleid,productid) b', 'a.id=b.articleid', 'left')
            ->join('product c', 'b.productid=c.supplierid', 'inner')
            ->where($map)
            ->page($page, $pagesize)
            ->order($order)
            ->select();
    }

    /**
     * 获取咨询分类的详情
     * @param unknown $id
     */
    public function getArticlecategory($id)
    {
        $key = 'GASG' . $id;
        $data = dcache(CACHE_ARTICLE, $key);
        if ($data) {
            return $data;
        }

        $data = Db::name('article_category')->where('id', $id)->find();
        dcache(CACHE_ARTICLE, $key, $data);

        return $data;
    }

    /**
     * 产品最新文章
     * @param unknown $productid
     * @param number $limit
     * @return unknown|mixed|\think\cache\Driver|boolean|NULL
     */
    public function getArticleByProduct($articleid, $limit = 10)
    {
        $name = 'BYP' . $articleid . '_' . $limit;

        $list = dcache(CACHE_ARTICLE, $name);

        if ($list) {
            return $list;
        }

        $one = Db::name('article_relations')->where(['articleid' => $articleid, 'productid' => ['>', 0]])->find();

        if ($one == null) {
            return $this->getRelationArticles(0, 10, 'clickcount desc');
        }

        $productid = $one['productid'];

        $list = $this->field('id,create_time,title,imagepath,summary')
            ->where('id', 'in', function ($query) use ($productid) {
                $query->table('article_relations')->where('productid', $productid)->field('articleid');
            })
            ->order('id desc')
            ->select();

        dcache(CACHE_ARTICLE, $name, $list);

        return $list;
    }

}
