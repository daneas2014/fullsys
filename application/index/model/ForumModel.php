<?php
namespace app\index\model;

use think\Db;
use think\Model;

class ForumModel extends Model
{
    /**
     * 供应商对应的版块，返回10条最新的帖子
     * @param unknown $supplierid
     * @param number $limit
     */
    public function supllierForum($supplierid, $limit = 10)
    {

    }

    /**
     * 获得置顶帖子
     * @param number $level
     * @return mixed|\think\cache\Driver|boolean|NULL|\think\Collection|\think\db\false|PDOStatement|string
     */
    public function getToplevel($level = 2, $fid = 0)
    {

        $key = 'TOPLEVEL_THREADS' . $level;

        $list = dcache(CACHE_FORUM, $key);

        if ($list != null) {
            return $list;
        }

        $map['toplevel'] = $level;
        $map['topendtime'] = ['>', time()];
        if ($fid > 0) {
            $map['fid'] = $fid;
        }

        $list = Db::name('bbs_thread')->alias('a')
            ->field('a.*,b.headimage')
            ->join('customer b', 'a.authorid=b.Id', 'left')
            ->where($map)
            ->select();

        if ($list == null) {
            return null;
        }

        dcache(CACHE_FORUM, $key, $list);

        return $list;
    }

    public function getHotThreads($limit = 10)
    {

        $key = 'HOT_THREADS' . $limit;

        $list = dcache(CACHE_FORUM, $key);

        if ($list != null) {
            return $list;
        }

        $list = Db::name('bbs_thread')->field('id,title,lastpost')
            ->join('(select tid from bbs_post where create_time between 0 and 0 group by tid limit 10) b', 'id=tid')
            ->select();

        if ($list == null) {
            return null;
        }

        dcache(CACHE_FORUM, $key, $list);

        return $list;
    }

    /**
     * 获取全部论坛id和名字
     * @return \think\Collection|\think\db\false|PDOStatement|string
     */
    public function getAllForums()
    {
        return Db::name('bbs_forum')->field('id,name,imagepath,threads')->order('threads desc')->select();
    }

    public function getForums($limit = 10)
    {
        return Db::name('bbs_forum')->order('threads desc')->limit($limit)->select();
    }

    public function getForum($fid, $supplierid = 0)
    {

        if ($supplierid > 0) {
            return Db::name('bbs_forum')->where('supplierid', $supplierid)->find();
        }

        return Db::name('bbs_forum')->where('id', $fid)->find();
    }

    public function getThread($tid)
    {
        return Db::name('bbs_thread')->alias('a')
            ->join('customer b', 'a.authorid=b.Id', 'left')
            ->join('bbs_forum c', 'a.fid=c.id', 'left')
            ->field('a.*,b.headimage,c.name as forumname')
            ->where('a.id', $tid)
            ->find();
    }

    /**
     * 悬赏贴
     * @param unknown $tid
     * @return array|\think\db\false|PDOStatement|string|\think\Model
     */
    public function getThreadReward($tid)
    {
        return Db::name('bbs_post')->join('customer b', 'a.authorid=b.Id', 'left')->where(['tid' => $tid, 'reward' => ['>', 0]])->find();
    }

    /**
     * 获取一定数量的帖子，参见forum/index/threads
     * @param unknown $t
     * @param unknown $o
     * @param number $limit
     * @return \think\Collection|\think\db\false|PDOStatement|string
     */
    public function getThreads($t = '', $o = '', $limit = 10)
    {

        $db = Db::name('bbs_thread');

        switch ($t) {
            case "activity":$map['tag'] = ['like', '%active%'];
                break;
            case "vip":$map['readperm'] = 1;
                break;
            case "bug":$map['tag'] = ['like', '%bug%'];
                break;
            case "fun":$map['tag'] = ['like', '%fun%'];
                break;
            default:$map = [];
        }

        switch ($o) {
            case "new":$order = 'id desc';
                break;
            case "recommend":$order = 'recommend_add desc';
                break;
            case "hot":$order = 'views desc';
                break;
            case "reward":$order = 'reward desc';
                break;
            case "post":$order = 'posts desc';
                break;
            default:$order = 'id desc';
        }

        return $db->alias('a')
            ->field('a.*,b.headimage')->join('customer b', 'a.authorid=b.id', 'left')->where($map)->order($order)->limit($limit)->select();
    }

    /**
     * 获取产品的主题
     *
     * @param unknown $productid
     * @param number $limit
     */
    public function getProductThreads($productid, $limit = 10)
    {
        $db = Db::name('bbs_thread');

        return $db->alias('a')
            ->field('a.*,b.headimage')
            ->join('customer b', 'a.authorid=b.Id', 'left')
            ->where('productid', $productid)
            ->order('id desc')
            ->limit($limit)
            ->select();
    }

    /**
     * 回帖
     * @param unknown $post
     * @return number|string
     */
    public function savePost($post)
    {
        return Db::name('bbs_post')->insertGetId($post);
    }

    /**
     * 发帖
     * @param unknown $data
     * @return boolean|number|string
     */
    public function saveThread($data)
    {

        if ($data == null) {
            return false;
        }

        $db = Db::name('bbs_thread');

        if (!array_key_exists('id', $data) || $data['id'] == '' || $data['id'] == 0) {

            Db::name('bbs_forum')->where('id', $data['fid'])->setInc('threads');
            unset($data['id']);
            return $db->insertGetId($data);
        }

        if ($data['id'] && $data['id'] > 0) {
            $data['update_time'] = time();
            return $db->update($data, ['id' => $data['id']]);
        }

    }
}
