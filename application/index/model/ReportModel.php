<?php
namespace app\index\model;

use think\Model;
use think\Db;

class ReportModel extends Model{
    
    protected $name='report_pvsource';
    
    protected $auto='create_time';
    
    public function insertSeed($data)
    {        
        $map['ip']=$data['ip'];
        $map['token']=$data['token'];
        if($this->where($map)->count()==0){
            return $this->insert($data);
        }
    }
    
    /**
     * 下游贡献了
     * @param unknown $token
     * @param unknown $vurls
     * @return unknown
     */
    public function contribut($token,$vurls,$uid)
    {
        return $this->where('token', $token)->update([
            'vurls' => $vurls,
            'customerid' => $uid,
            'contribut' => Db::raw('contribut+1')
        ]);;
    }
    
    /**
     * 记录live800行为
     * @param unknown $token
     * @return unknown
     */
    public function live800($token)
    {
        
        return $this->where('token',$token)->setInc('live800');
    }
    
    /**
     * 点击广告的时候需要写个数据进去
     * @param unknown $token
     * @param unknown $adid
     * @return unknown
     */
    public function addBit($token,$adid,$data=null){
        
        if($data!=null){
            Db::name('advertisment_bits')->insert($data);
        }
        
        //$this->insert(['token'=>$token,'adid'=>$adid]);        
        return Db::name('advertisments')->where('id',$adid)->setInc('bits');
    }
}