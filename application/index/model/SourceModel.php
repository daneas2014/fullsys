<?php
namespace app\index\model;

use think\Model;
use think\Db;
use app\common\model\ResourceBase;

class SourceModel extends ResourceBase
{

    public function videotypes()
    {}

    /**
     * 获取视频清单
     * 
     * @param number $productid            
     * @param number $supplierid            
     * @param number $categoryid            
     * @param unknown $page            
     * @param unknown $pagesize            
     * @param unknown $orderby            
     * @return unknown
     */
    public function videos($productid = 0, $supplierid = 0, $categoryid = 0, $page, $pagesize, $orderby)
    {
        if ($productid > 0) {
            return Db::name('video')
                ->where('productid', $productid)
                ->page($page, $pagesize)
                ->orderby('id desc')
                ->select();
        }
        
        if ($supplierid > 0) {
            return Db::name('video')->alias('a')
                ->field('a.*')
                ->join('product', 'product.id=a.productid')
                ->where('supplierid', $supplierid)
                ->page($page, $pagesize)
                ->orderby('id desc')
                ->select();
        }
        
        if ($categoryid > 0) {
            $videos = Db::name('video')->alias('a')
                ->field('a.*')
                ->join('product b', 'a.productid=b.id')
                ->where('categoryid',$categoryid)
                ->select();
            return $videos;
        }
    }

    public function getProductAttchs($key = '', $type = 0, $productid = 0, $page = 1, $limit = 20, $attr = [])
    {
        return parent::getAttchs($key, $type, $productid, $page, $limit, $attr);
    }
}