<?php
namespace app\index\model;

use think\Model;
use think\Db;

class ResourceModel extends Model{
   
    public function videotypes(){
        
    }
    
    /**
     * 获取视频清单
     * @param number $productid
     * @param number $supplierid
     * @param number $categoryid
     * @param unknown $page
     * @param unknown $pagesize
     * @param unknown $orderby
     * @return unknown
     */
    public function videos($productid=0,$supplierid=0,$categoryid=0,$page,$pagesize,$orderby){
        $field = 'video.id,video.title,video.summary,video.duration,video.image,video.isheadline';
        if($productid>0){
            return Db::name('video')
            ->field($field)
            ->where('productId',$productid)
            ->page($page,$pagesize)
            ->order('id desc')
            ->select();
        }
        
        if($supplierid>0){            
            return Db::name('video')
            ->join('product', 'product.id=video.productid')
            ->where('supplierid', $supplierid)
            ->field($field)
            ->where('supplierid',$supplierid)
            ->page($page,$pagesize)
            ->order('id desc')
            ->select();
        }
        
        if($categoryid>0){            
            return Db::name('video')
            ->field($field)
            ->join('catalogproduct','catalogproduct.id=video.productid')
            ->where('categoryid',$categoryid)
            ->page($page,$pagesize)
            ->order('id desc')
            ->select();
        }
    }
    
    public function video($videoid){
        return Db::name('videos')->where('id',$videoid)->find();        
    }
    
}