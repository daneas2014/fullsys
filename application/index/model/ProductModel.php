<?php
namespace app\index\model;

use think\Db;
use think\Model;

class ProductModel extends Model
{

    protected $name = 'product';

    /**
     * 获取产品页中共享部分，一次性展现的，其他地方是否有复用未知，缓存，预估占用内存1G
     *
     * @param unknown $id
     */
    public function getProductCommon($id)
    {
        $key = 'GPC_' . $id;

        $product = dcache(CACHE_COMMODITY, $key);

        if ($product == null) {

            $product = $this->get(['id' => $id]);

            if ($product == null) {
                return null;
            }

            $attr = Db::name('product_state')->where('id', $id)->find();
            if ($attr == null) {
                $attr = ['id' => $id, 'downloadcount' => 10, 'followcount' => 10, 'clickcount' => 10, 'commentcount' => 0, 'editcount' => 0, 'isrecommended' => 0];
                Db::name('product_state')->insert($attr);
            }

            $priceCount = Db::name('product_price')->where('productid', $id)->count();

            $fileCount = Db::name('product_download')->where('productid', $id)->count();

            $onlineCount = Db::name('product_price')->where([
                'productid' => $id,
            ])->count();

            $catagory = Db::name('product_category')->where('id', $product['categoryid'])->find();

            $supplier = Db::name('supplier')->where('id', $product['supplierid'])->find();

            $editor = Db::name('sys_admin')->where('id', $supplier['adminid'])->find();

            $product['editor'] = $editor;
            $product['haveonlnesale'] = $onlineCount;
            $product['haveprice'] = $priceCount;
            $product['havefile'] = $fileCount;
            $product['attr'] = $attr;
            $product['catagory'] = $catagory;
            $product['revasks'] = null;
            $product['rsuppliers'] = $this->getRelateSuppliers($product['supplierid'], $product['categoryid']);
            $product['rproducts'] = $this->getRelateProducts($id, $product['categoryid']);
            $product['supplier'] = $supplier;

            dcache(CACHE_COMMODITY, $key, $product);
        }

        return $product;
    }

    /**
     * 获取产品价格
     *
     * @param [type] $productid
     * @return void
     * @author dmakecn@163.com
     * @since 2021-01-06
     */
    public function getProductPrices($productid)
    {
        $map['productid'] = $productid;

        $db = Db::name('product_price');
        $list = $db->where($map)
            ->order('id asc')
            ->select();

        if ($list == null) {
            return null;
        }

        $groups = [];
        foreach ($list as $l) {
            if ($l['groupname'] && !isset($groups[$l['groupname']])) {
                array_push($groups, $l['groupname']);
            }
        }

        foreach ($list as $item) {

            if ($item['currencytype'] == 1) {
                $item['amount'] = $item['amount'] * config('exrates')['tousd'];
                $item['sale_amount'] = $item['sale_amount'] * config('exrates')['tousd'];
            }

            if ($item['currencytype'] == 2) {
                $item['amount'] = $item['amount'] * config('exrates')['togbp'];
                $item['sale_amount'] = $item['sale_amount'] * config('exrates')['togbp'];
            }

            if ($item['currencytype'] == 3) {
                $item['amount'] = $item['amount'] * config('exrates')['toeur'];
                $item['sale_amount'] = $item['sale_amount'] * config('exrates')['toeur'];
            }
        }

        return ['groups' => $groups, 'list' => $list];
    }

    public function getProductImages($productid)
    {
        $key = 'GPI_' . $productid;

        $list = dcache(CACHE_COMMODITY, $key);

        if ($list) {
            return $list;
        }

        $list = Db::name('product_images')->where('productid', $productid)->select();
        dcache(CACHE_COMMODITY, $key, $list);

        return $list;
    }

    public function getProductResources($productid, $limit = 10)
    {
        $db = Db::name('product_download');

        $map['productid'] = $productid;

        $all = $db->where($map)->order('id desc')->limit($limit)->select();

        return $all;
    }

    /**
     * 下载资源，0是文件，1是文档，4是其他，5是产品扩展
     *
     * @param unknown $productid
     * @param number $limit
     * @return \think\Collection|\think\db\false|PDOStatement|string
     */
    public function getProductDownloads($productid, $limit = 10)
    {
        $map['delete_time'] = 0;
        $map['type'] = 0;

        $files['test'] = Db::name('product_download')->where('productid', $productid)
            ->where($map)
            ->limit($limit)
            ->order('id desc')
            ->select();

        $map['type'] = 1;
        $files['doc'] = Db::name('product_download')->where('productid', $productid)
            ->where($map)
            ->limit($limit)
            ->order('id desc')
            ->select();

        $map['type'] = 5;
        $files['ext'] = Db::name('product_download')->where('productid', $productid)
            ->where($map)
            ->limit($limit)
            ->order('id desc')
            ->select();

        $map['type'] = 4;
        $files['other'] = Db::name('product_download')->where('productid', $productid)
            ->where($map)
            ->limit($limit)
            ->order('id desc')
            ->select();

        return $files;
    }

    public function getProductDownloadsMvp($productid)
    {
        $one = Db::name('product_download')
            ->where('productid', $productid)
            ->where('type', 0)
            ->order('downloadqty desc')
            ->find();

        return $one['id'];
    }

    public function getProductVideos($produictid, $limit = 10)
    {
        $list = Db::name('video')
            ->where('productid', $produictid)
            ->limit($limit)
            ->order('id desc')
            ->select();

        return $list;
    }

    public function getProductArticle($produictid, $limit = 10)
    {
        $key = 'GPA' . $produictid . '-' . $limit;

        $list = dcache(CACHE_COMMODITY, $key);

        if ($list) {
            return $list;
        }

        $list = Db::name('article')->field('article.*')
            ->join('article_relations', 'article_relations.articleid=article.Id', 'inner')
            ->where('article_relations.productid', $produictid)
            ->limit($limit)
            ->order('id desc')
            ->select();

        dcache(CACHE_COMMODITY, $key, $list);

        return $list;
    }

    /**
     * 获取在线订购产品
     * @param unknown $subcateid
     * @param number $limit
     */
    public function getOnlineSaleProducts($subcateid = 0, $limit = 6)
    {
        $key = 'GOSP0' . $subcateid . '_' . $limit;

        $data = dcache(CACHE_COMMODITY, $key);

        if ($data) {
            return $data;
        }

        if ($subcateid != 0) {
            $sql = sprintf("select b.name,imagepath,slogan,product_price.productid,MIN(amount)as amount from product_price
                            inner join product b on product_price.ProductId=b.Id  and categoryid=%d
                            group by product_price.productid,b.name,b.imagepath,b.slogan
                            order by b.sort desc limit %d ", $subcateid, $limit);
        } else {
            $sql = sprintf("select b.name,imagepath,slogan,product_price.productid,MIN(amount)as amount from product_price
                            inner join product b on product_price.ProductId=b.Id
                            group by product_price.productid,b.name,b.imagepath,b.slogan
                            order by b.sort desc limit %d", $limit);
        }

        $data = Db::query($sql);

        dcache(CACHE_COMMODITY, $key, $data);

        return $data;
    }

    /**
     * 获取产品的清单
     * @param number $categoryid
     * @param number $supplierid
     * @param number $page
     * @param number $pagesize
     * @param string $orderby download,new,commit
     */
    public function getProductList($categoryid = 0, $supplierid = 0, $page = 1, $pagesize = 10, $orderby = 'new')
    {
        $field = 'id,name,imagepath,categoryid,supplierid,descriptionword,supplierrecommend';

        $order = '';

        switch ($orderby) {
            case 'new':$order = 'id desc';
                break;
            case 'recommend':$order = 'supplierrecommend desc';
                break;
        }

        if ($categoryid > 0) {
            $map['categoryid'] = $categoryid;
            $map['status'] = 1;

            $list = $this->field($field)
                ->where($map)
                ->page($page, $pagesize)
                ->order($order)
                ->select();

            return $list;
        }

        if ($supplierid > 0) {
            $map['supplierid'] = $supplierid;
            $map['status'] = 1;
            $list = Db::name('productid')
                ->field($field)
                ->where($map)
                ->page($page, $pagesize)
                ->select();

            return $list;
        }
    }

    /**
     * 直接粗暴的缓存供应商，预估总共占用内存100M以内
     * @param unknown $id
     */
    public function getSupplier($id)
    {
        return Db::name('supplier')->where('id', $id)->find();
    }

    /**
     * 获取供应商
     * @param unknown $categoryid
     * @param unknown $page
     * @param unknown $pagesize
     * @param unknown $order
     */
    public function getSuppliers($categoryid, $page, $pagesize, $order)
    {
        return Db::name('supplier')->where('categoryid', $categoryid)
            ->page($page, $pagesize)
            ->select();
    }

    /**
     * 获取产品分类页的推荐[没有改造，现在效率低下]
     * @param unknown $categoryid
     * @param unknown $type   normarl,hot
     * @param unknown $limit
     * @param unknown $model product,supplier
     */
    public function getRecommends($categoryid, $type, $limit = 5, $model = 'product')
    {
        $map['a.cateid'] = $categoryid;
        $map['a.datatype'] = $model;

        if ($model == 'product') {

            return Db::name('product_recommend')->alias('a')
                ->field('b.Id,a.title,a.description,b.imagepath')
                ->join('product b', 'a.dataid=b.Id', 'inner')
                ->where($map)
                ->limit($limit)
                ->select();
        }

        if ($model == 'supplier') {
            return Db::name('product_recommend')->alias('a')
                ->field('b.Id,b.logopath,b.name,a.title,a.description,a.iconpath')
                ->join('supplier b', 'a.dataid=b.Id', 'inner')
                ->where($map)
                ->limit($limit)
                ->select();
        }
    }

    /**
     *  获取同类产品
     * @param number $limit
     */
    public function getSameProduct($categoryid, $limit = 5)
    {

        $key = 'GSP' . $categoryid . '_' . $limit;

        $list = dcache(CACHE_COMMODITY, $key);

        if ($list) {
            return $list;
        }

        $map = ['categoryid' => $categoryid, 'status' => 1];
        $list = $this->field('id,name,imagepath,slogan')->where($map)->limit($limit)->select();

        dcache(CACHE_COMMODITY, $key, $list);

        return $list;
    }

    /**
     * 根据产品ID和分类获取相关产品
     * @param unknown $productid
     * @param unknown $categoryid
     * @return mixed|\think\cache\Driver|boolean|NULL|array
     */
    public function getRelateProducts($productid, $categoryid, $supplierid = 0)
    {

        $key = 'GRPS' . $productid . '-' . $categoryid . '-' . $supplierid;

        $products = dcache(CACHE_COMMODITY, $key);

        if ($products) {
            return $products;
        }

        if ($supplierid > 0) {
            $map = ['a.supplierid' => $supplierid, 'a.status' => 1, 'a.id' => ['<>', $productid]];
        } else {
            $map = ['a.categoryid' => $categoryid, 'a.status' => 1];
        }

        $products = $this->alias('a')
            ->field('a.id as productid,a.name,a.description,a.version,a.categoryid,b.name as supplier,b.id as supplierid, a.producttype,a.slogan,a.supplierrecommend,c.name as category')
            ->join('supplier b', 'a.supplierid=b.Id', 'left')
            ->join('product_category c', 'a.categoryid=c.Id', 'left')
            ->where($map)
            ->limit(5)
            ->order('supplierrecommend desc')
            ->select();

        dcache(CACHE_COMMODITY, $key, $products);

        return $products;
    }

    public function getRelateSuppliers($supplierid, $categoryid)
    {

        $name = 'GRS_' . $supplierid . '_' . $categoryid;

        $suppliers = dcache(CACHE_COMMODITY, $name);

        if ($suppliers) {
            return $suppliers;
        }

        $map = ['supplierid' => ['<>', $supplierid], 'categoryid' => $categoryid];

        $list = $this->field('supplierid')
            ->where($map)
            ->group('supplierid')
            ->limit(5)
            ->select();

        $ids = [];
        foreach ($list as $i) {
            array_push($ids, $i['supplierid']);
        }

        $suppliers = Db::name('supplier')->field('name,logopath,id as supplierid')
            ->where('id', 'in', $ids)
            ->where('status', 0)
            ->select();

        dcache(CACHE_COMMODITY, $name, $suppliers);

        return $suppliers;
    }

    public function getProductHomeRecommend()
    {

        $key = 'GPHR';

        $parent = dcache(CACHE_COMMODITY, $key);

        if ($parent) {
            return $parent;
        }

        $list = Db::name('product_home')->order('parentid asc')->select();

        $parent = [];

        foreach ($list as $l) {
            if ($l['parentid'] == 0) {

                $child = [];

                foreach ($list as $p) {
                    if ($p['parentid'] == $l['id']) {
                        array_push($child, $p);
                    }
                }

                $l['child'] = $child;

                array_push($parent, $l);
            }
        }

        dcache(CACHE_COMMODITY, $key, $parent);

        return $parent;
    }
}
