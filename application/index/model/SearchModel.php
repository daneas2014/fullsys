<?php
namespace app\index\model;

use think\Db;
use think\Model;

class SearchModel extends Model
{

    public function product($key)
    {
        $cachekey = 'Pi' . $key;

        $list = dcache(CACHE_SEARCH, $cachekey);

        $map['isdisabled'] = 0;
        $map['ispublish'] = 1;

        $mapor = [];

        if ($key > 0) {
            $map['sn|id'] = $key;
        } else {
            $map['name'] = ['like', '%' . $key . '%'];
            $mapor['title|keywords|summary'] = ['like', '%' . $key . '%'];
        }

        if ($list == null) {

            $query = Db::name('product')->field('id,name,isheadline')
                ->where($map)
                ->order('id,isheadline desc')
                ->limit(5)
                ->select();

            $list = [];
            foreach ($query as $q) {
                $temp['name'] = $q['name'];
                $temp['url'] = $this->initUrl('product', $q['id']);
                array_push($list, $temp);
                unset($temp);
            }

            dcache(CACHE_SEARCH, $cachekey, $list);
        }

        return $list;
    }

    public function article($key)
    {
        $cachekey = 'article.' . $key;

        $list = dcache(CACHE_SEARCH, $cachekey);

        if ($list == null) {

            $query = Db::name('article')->field('id,title,createdtime,isheadline')
                ->where('title', 'like', '%' . $key . '%')
                ->order('id,isheadline desc')
                ->limit(5)
                ->select();

            $list = [];
            foreach ($query as $q) {
                $temp['name'] = $q['title'];
                $temp['url'] = $this->initUrl('article', $q['id'], $q['createdtime']);
                array_push($list, $temp);
                unset($temp);
            }

            dcache(CACHE_SEARCH, $cachekey, $list);
        }

        return $list;
    }

    public function resource($key)
    {
        $cachekey = 'RESOURCE.' . $key;

        $list = dcache(CACHE_SEARCH, $cachekey);

        if ($list == null) {

            $query = Db::name('product_download')->field('id,name,downloadqty')
                ->where('name', 'like', '%' . $key . '%')
                ->order('downloadqty desc')
                ->limit(5)
                ->select();

            $list = [];
            foreach ($query as $q) {
                $temp['name'] = $q['name'];
                $temp['url'] = $this->initUrl('resource', $q['Id']);
                array_push($list, $temp);
                unset($temp);
            }
            dcache(CACHE_SEARCH, $cachekey, $list);
        }

        return $list;
    }

    public function video($key)
    {
        $cachekey = 'VIDEO.' . $key;

        $list = dcache(CACHE_SEARCH, $cachekey);

        if ($list == null) {

            $query = Db::name('video')->field('id,title,isheadline')
                ->where('title', 'like', '%' . $key . '%')
                ->order('id,isheadline desc')
                ->limit(5)
                ->select();

            $list = [];
            foreach ($query as $q) {
                $temp['name'] = $q['title'];
                $temp['url'] = $this->initUrl('video', $q['id']);
                array_push($list, $temp);
                unset($temp);
            }

            dcache(CACHE_SEARCH, $cachekey, $list);
        }

        return $list;
    }

    public function course($key)
    {

        $cachekey = 'COURSE.' . $key;

        $list = dcache(CACHE_SEARCH, $cachekey);

        if ($list == null) {

            $query = Db::name('course_description')->field('id,title')
                ->where('title', 'like', '%' . $key . '%')
                ->order('id desc')
                ->limit(5)
                ->select();

            $list = [];
            foreach ($query as $q) {
                $temp['name'] = $q['title'];
                $temp['url'] = $this->initUrl('edu', $q['id']);
                array_push($list, $temp);
                unset($temp);
            }

            dcache(CACHE_SEARCH, $cachekey, $list);
        }

        return $list;
    }

    private function initUrl($typp, $id, $createdtime = null)
    {
        switch ($typp) {
            case 'article':
                return SITE_DOMAIN . '/article/' . $id;
            case 'commodity':
                return SITE_DOMAIN . '/commodity/' . $id;
            case 'video':
                return SITE_DOMAIN . '/video/' . $id;
            case 'course':
                return SITE_DOMAIN . '/course/play/' . $id;
        }
    }
}
