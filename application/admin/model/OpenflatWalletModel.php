<?php
namespace app\admin\model;

use think\Db;
use think\Model;

class OpenflatWalletModel extends Model
{

    protected $name = 'opf_appwallet';

    /*
     * 创建钱包
     */
    public function createwallet($gh_id)
    {
        $data['gh_id'] = $gh_id;
        return $this->create($data);
    }

    public function getwallet($gh_id)
    {
        return $this->where('gh_id', $gh_id)->find();
    }

    /*
     * 获取所有钱包信息
     */
    public function allwallet()
    {
        $result = Db::name('opf_app')->alias('a')
            ->join('op_appwallet b', 'a.username=b.gh_id')
            ->field('a.name,b.*')
            ->select();

        return $result;
    }

    /*
     * admin给账户充值,记录充值的公众号和金额
     */
    public function chagewallet($adminid, $gh_id, $amount, $remark)
    {
        $wallet = $this->where('gh_id', $gh_id)->find();

        $data['totalamount'] = $wallet['totalamount'] + $amount;
        $data['leftamount'] = $wallet['leftamount'] + $amount;

        if ($this->where('gh_id', $gh_id)->update($data)) {

            $result = Db::name('op_appwallet_log')->insert([
                'gh_id' => $gh_id,
                'admin' => $adminid,
                'amount' => $amount,
                'remark' => $remark,
                'createtime' => time(),
            ]);

            return json(['code' => 1, 'data' => '', 'msg' => '账户充值完毕']);
        }

        return json(['code' => -1, 'data' => '', 'msg' => '账户充值失败']);
    }

    /*
     * 钱包消费
     */
    public function consumewallet($adminid, $gh_id, $amount, $remark)
    {
        $wallet = $this->where('gh_id', $gh_id)->find();
        $data['leftamount'] = $wallet['leftamount'] - $amount;

        if ($this->where('gh_id', $gh_id)->update($data)) {

            Db::name('opf_appwallet_log')->insert([
                'gh_id' => $gh_id,
                'admin' => $adminid,
                'amount' => -$amount,
                'remark' => $remark,
                'createtime' => time(),
            ]);
            return json(['code' => 1, 'data' => '', 'msg' => '账户充值完毕']);
        }

        return json(['code' => -1, 'data' => '', 'msg' => '账户充值失败']);
    }
}
