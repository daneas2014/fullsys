<?php

namespace app\admin\model;
use think\Model;
use think\Db;

class MemberModel extends Model
{
    protected $name = 'customer';
    protected $autoWriteTimestamp = true;   // 开启自动写入时间戳

    /**
     * 根据搜索条件获取用户列表信息
     */
    public function getMemberByWhere($map, $Nowpage, $limits)
    {
        $list= $this->alias('a')
            ->field('a.*,b.emailvalitime,b.mobilevalitime')
            ->join('customer_attribute b','a.id=b.customerid','left')
            ->where($map)
            ->page($Nowpage, $limits)
            ->order('id desc')
            ->select();
        
        $count = $this->alias('a')->where($map)->count();
        
        return ['list'=>$list,'count'=>$count];
    }
    
    public function getCourseMember($map,$Nowpage,$limits){
                
        $list= $this->alias('a')
        ->field('a.nickname,a.mobile,a.id as mid,b.*')
        ->join('series_access b','a.id=b.customerid','right')
        ->where($map)
        ->page($Nowpage, $limits)
        ->order('create_time desc')
        ->select();
        
        $count= $this->alias('a')
        ->join('series_access b','a.id=b.customerid','right')
        ->where($map)
        ->count();
        
        return ['list'=>$list,'count'=>$count];
    }

    /**
     * 插入信息
     */
    public function insertMember($param)
    {
        try{
            $result = $this->allowField(true)->save($param);
            if(false === $result){            
                return ['code' => -1, 'data' => '', 'msg' => $this->getError()];
            }else{
                return ['code' => 1, 'data' => '', 'msg' => '添加成功'];
            }
        }catch( PDOException $e){
            return ['code' => -2, 'data' => '', 'msg' => $e->getMessage()];
        }
    }

    /**
     * 编辑信息
     * @param $param
     */
    public function editMember($param)
    {
        try{
            $param['update_time']=time();
            $result =  $this->allowField(true)->save($param, ['id' => $param['id']]);
            if(false === $result){            
                return ['code' => 0, 'data' => '', 'msg' => $this->getError()];
            }else{
                return ['code' => 1, 'data' => '', 'msg' => '编辑成功'];
            }
        }catch( PDOException $e){
            return ['code' => 0, 'data' => '', 'msg' => $e->getMessage()];
        }
    }


    /**
     * 根据管理员id获取角色信息
     * @param $id
     */
    public function getOneMember($id)
    {
        return $this->where('id', $id)->find();
    }


    /**
     * 删除管理员
     * @param $id
     */
    public function delUser($id)
    {
        try{

            $this->where('id', $id)->delete();
            Db::name('auth_group_access')->where('uid', $id)->delete();
            return ['code' => 1, 'data' => '', 'msg' => '删除成功'];

        }catch( PDOException $e){
            return ['code' => 0, 'data' => '', 'msg' => $e->getMessage()];
        }
    }


    /**删除会员
     * @param $id
     * @return array
     */
    public function delMember($id)
    {
        try{
            $map['delete_time']=time();
            $this->where('id', $id)->update($map);
            return ['code' => 1, 'data' => '', 'msg' => '删除成功'];
        }catch( PDOException $e){
            return ['code' => 0, 'data' => '', 'msg' => $e->getMessage()];
        }
    }

    /**
     * 会员重置密码
     */
    public function resetPwd($id,$password){
        try{
            $data['password']=$password;
            $data['password_update']=time();
            $result =  $this->where('Id',$id)->update($data);
            if(false === $result){
                return ['code' => 0, 'data' => '', 'msg' => $this->getError()];
            }else{
                return ['code' => 1, 'data' => '', 'msg' => '修改密码成功'];
            }
        }catch( PDOException $e){
            return ['code' => 0, 'data' => '', 'msg' => $e->getMessage()];
        }
    }


}