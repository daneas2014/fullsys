<?php
namespace app\admin\model;

use think\Model;
use think\Db;

class ReportModel extends Model
{
    
    protected $name='report_pvcrunms';
    
    /**
     * 获取管理人员时间段内的流量获取情况
     * @param unknown $start
     * @param unknown $end
     * @return unknown
     */
    public function adminPv($start,$end,$type){
        
        $where['a.create_time']=['between',[$start,$end]];
        $where['adminid']=['>',0];
        $where['groupid']=2;
        $where['status']=1;
        if($type&&$type!=''){
            $where['datatype']=$type;
        }
        
        $list = $this->alias('a')
        ->field('adminid,sum(pv) as total,real_name')
        ->join('sys_admin b', 'a.adminid=b.id', 'left')
        ->where($where)
        ->group('adminid,real_name')
        ->order('total desc')
        ->select();
        
        return $list;
    }
    
    /**
     * 供应商的PV占比
     * @param unknown $start
     * @param unknown $end
     * @param unknown $type
     * @return unknown
     */
    public function supplierPv($start,$end,$type='',$supplierid=0){
        
        $where['a.create_time']=['between',[$start,$end]];
        $where['status']=1;
        if($type&&$type!=''){
            $where['datatype']=$type;
        }
        if($supplierid>0){
            $where['supplierid']=$supplierid;
        }
        else{
            $where['supplierid']=['>',0];
        }
        
        $list = $this->alias('a')
        ->field('supplierid,sum(pv) as total,name')
        ->join('supplier b', 'a.supplierid=b.id', 'left')
        ->where($where)
        ->group('supplierid,name')
        ->order('total desc')
        ->limit(30)
        ->select();

        
        if(!$list){
            $list=[['supplierid'=>0,'name'=>'示例','total'=>10000,'']];
        }
        
        $where2['create_time']=['between',[$start,$end]];
        $where2['supplierid']=['in',echartInit($list, 'supplierid','int')];
        if($type&&$type!=''){
            $where2['datatype']=$type;
        }
        if($supplierid>0){
            $where2['supplierid']=$supplierid;
        }
        else{
            $where2['supplierid']=['>',0];
        }
        
        
        $datatypes=$this->field('supplierid,sum(pv) as total,datatype')
        ->where($where2)
        ->group('supplierid,datatype')
        ->order('total desc')
        ->select();
        
        $supplier=[];
        foreach ($list as $l){
            foreach ($datatypes as $t) {
                if ($t['supplierid'] == $l['supplierid']) {
                    $l[$t['datatype']]= $t['total'];
                }
            }
            
            if(!isset($l['download'])){
                $l['download']=0;
            }
            if(!isset($l['demo'])){
                $l['demo']=0;
            }
            if(!isset($l['article'])){
                $l['article']=0;
            }
            if(!isset($l['product'])){
                $l['product']=0;
            }
            if(!isset($l['video'])){
                $l['video']=0;
            }
            if(!isset($l['mall'])){
                $l['mall']=0;
            }
            
            array_push($supplier, $l);
        }
        
        return $supplier;
    }
    
    public function todayWork(){
        
        $startyear=strtotime(date('Y-1-1'));
        $startmonth=strtotime(date('Y-m-1'));
        $start=strtotime('today');
        $end=$start+60*60*24;
        
        $article=Db::name('article')->where('create_time','>',$start)->count();
        $attach=Db::name('product_download')->where('create_time','>',strtotime('-1 day'))->count();
        $member=Db::name('customer')->where('create_time','>',$start)->count();
        $download=Db::name('product_downloadrecord')->where('downloadtime','>',date('Y-m-d 00:00:00'))->count();
        $video=Db::name('video')->where('create_time','>',strtotime('-1 day'))->count();
        $adbits=Db::name('advertisment_bits')->where('create_time','>',$start)->count();
        $incoming=Db::name('orders')->where('pay_time','>',$start)->count();
        $orderprice=Db::name('orders')->where('pay_time','>',$start)->sum('TotalAmount');
        
        $memberm=Db::name('customer')->where('create_time','>',$startmonth)->count();
        $orderm=Db::name('orders')->where('pay_time','>',$startmonth)->sum('TotalAmount');
        $downloadm=Db::name('product_downloadrecord')->where('downloadtime','>',date('Y-m-1 00:00:00'))->count();
        
        return [
            'article' => $article,
            'attach' => $attach,
            'member' => $member,
            'lead' => $download,
            'video' => $video,
            'adbits' => $adbits,
            'incoming' => $incoming,
            'orderprice' => $orderprice,
            'memberm'=>$memberm,
            'orderm'=>$orderm,
            'leadm'=>$downloadm
        ];
    }
    
    /**
     * 本月工作分组
     */
    public function monthWork()
    {
        $users = Db::name('sys_admin')->field('id,real_name')->where('status', 1)->select();
        
        $startdate = date('Y-m-1');
        $starttime = strtotime('month');
        
        $article = Db::name('article')->field('adminid,count(adminid) as a')
        ->where('create_time', '>', $starttime)
        ->group('adminid')
        ->select();
                
        $video=Db::name('video')->field('adminid,count(adminid) as v')
        ->where('create_time', '>', $starttime)
        ->group('adminid')
        ->select();
        
                
        $monthwork = [];
        
        foreach ($users as $u) {
            
            foreach ($article as $a) {
                if ($a['adminid'] == $u['id']) {
                    $u['a']= $a['a'];
                    continue;
                }
            }
            
            foreach ($video as $v) {
                if ($v['adminid'] == $u['id']) {
                    $u['v']= $v['v'];
                    continue;
                }
            }
            
            
            if(!array_key_exists('a', $u)){
                $u['a']=0;
            }
            
            if(!array_key_exists('d', $u)){
                $u['d']=0;
            }
            
            if(!array_key_exists('v', $u)){
                $u['v']=0;
            }
            
            
            $u['t']=$u['a']+$u['d']+$u['v'];
            
            array_push($monthwork, $u);
        }
        
        $count = count($monthwork);
        
        /*
         * 正序
         * for ($i = 0; $i < $count; $i ++) {
         *
         * for ($j = $count - 1; $j > $i; $j --) {
         *
         * if ($monthwork[$j]['t'] < $monthwork[$j - 1]['t']) {
         * $temp = $monthwork[$j];
         * $monthwork[$j] = $monthwork[$j - 1];
         * $monthwork[$j - 1] = $temp;
         * }
         *
         * }
         *
         * }
         */
        for ($i = 1; $i < $count; $i ++) {
            for ($j = 0; $j < ($count - $i); $j ++) {
                if ($monthwork[$j]['t'] < $monthwork[$j + 1]['t']) {
                    $temp = $monthwork[$j + 1];
                    $monthwork[$j + 1] = $monthwork[$j];
                    $monthwork[$j] = $temp;
                }
            }
        }
        
        return $monthwork;
    }
    
}
