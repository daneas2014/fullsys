<?php

namespace app\admin\model;
use think\Model;

class ArticleModel extends Model
{
    protected $name = 'article';
    
    // 开启自动写入时间戳字段
    protected $autoWriteTimestamp = true;


    /**
     * 根据搜索条件获取用户列表信息
     */
    public function getArticleByWhere($map, $Nowpage, $limits)
    {
        $count = $this->alias('a')->where($map)->count();
        
        $list = $this->alias('a')
            ->field('a.id,a.title,a.imagepath,a.ispublish,a.create_time,real_name,c.name,a.summary,b.clickcount')
            ->join('article_attribute b','a.id=b.articleid','left')
            ->join('article_category c', 'c.id=a.categoryid', 'left')
            ->join('sys_admin d', 'd.id=a.adminid', 'left')
            ->where($map)
            ->page($Nowpage, $limits)
            ->order('id desc')
            ->select();
        
        $res['count'] = $count;
        $res['list'] = $list;
        
        return $res;
    }
    
    
    /**
     * [insertArticle 添加文章]
     */
    public function insertArticle($param)
    {
        try{
            $result = $this->allowField(true)->save($param);
            if(false === $result){             
                return ['code' => -1, 'data' => '', 'msg' => $this->getError()];
            }else{
                return ['code' => 1, 'data' => '', 'msg' => '文章添加成功'];
            }
        }catch( PDOException $e){
            return ['code' => -2, 'data' => '', 'msg' => $e->getMessage()];
        }
    }



    /**
     * [updateArticle 编辑文章]
     */
    public function updateArticle($param)
    {
        try{
            $result = $this->allowField(true)->save($param, ['id' => $param['id']]);
            if(false === $result){          
                return ['code' => 0, 'data' => '', 'msg' => $this->getError()];
            }else{
                return ['code' => 1, 'data' => '', 'msg' => '文章编辑成功'];
            }
        }catch( PDOException $e){
            return ['code' => 0, 'data' => '', 'msg' => $e->getMessage()];
        }
    }

}