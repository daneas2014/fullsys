<?php

namespace app\admin\model;
use think\Model;

class AdModel extends Model
{
    protected $name = 'advertisments';

    /**
     * 根据条件获取列表信息
     * @param $where
     * @param $Nowpage
     * @param $limits
     */
    public function getAdAll($map, $Nowpage, $limits)
    {
        $list= $this->alias('a')
            ->field('a.*,b.name,pkey')
            ->join('advertisment_position b', 'a.positionid = b.Id')
            ->where($map)
            ->page($Nowpage,$limits)
            ->order('Id desc')
            ->select();     
        
        $count=$this->alias('a')
            ->join('advertisment_position b', 'a.positionid = b.Id')
            ->where($map)
            ->count();
        
        $res['list']=$list;
        $res['count']=$count;
        
        return $res;
    }

    /**
     * 插入信息
     * @param $param
     */
    public function insertAd($param)
    {
        $param['CreatedTime']=date('Y-m-d h:i:sa');
        try{
            $result = $this->validate('AdValidate')->allowField(true)->save($param);
            if(false === $result){       
                return ['code' => -1, 'data' => '', 'msg' => $this->getError()];
            }else{
                return ['code' => 1, 'data' => '', 'msg' => '添加广告成功'];
            }
        }catch( PDOException $e){
            return ['code' => -2, 'data' => '', 'msg' => $e->getMessage()];
        }
    }

    /**
     * 编辑信息
     * @param $param
     */
    public function editAd($param)
    {
        try{

            $result = $this->validate('AdValidate')->allowField(true)->save($param, ['Id' => $param['Id']]);

            if(false === $result){
                return ['code' => 0, 'data' => '', 'msg' => $this->getError()];
            }else{
                return ['code' => 1, 'data' => '', 'msg' => '编辑广告成功'];
            }
        }catch( PDOException $e){
            return ['code' => 0, 'data' => '', 'msg' => $e->getMessage()];
        }
    }

    /**
     * 根据id获取一条信息
     * @param $id
     */
    public function getOneAd($id)
    {
        return $this->where('id', $id)->find();
    }


    /**
     * 删除信息
     * @param $id
     */
    public function delAd($id)
    {
        try{
            $this->where('Id',$id)->delete();
            return ['code' => 1, 'data' => '', 'msg' => '删除广告成功'];
        }catch( PDOException $e){
            return ['code' => 0, 'data' => '', 'msg' => $e->getMessage()];
        }
    }

}