<?php

namespace app\admin\model;
use think\Db;
use think\Model;

class UserType extends Model {
	protected $name = 'Sys_auth_group';

	// 开启自动写入时间戳字段
	protected $autoWriteTimestamp = true;

	/**
	 * [getRoleByWhere 根据条件获取角色列表信息]
	 */
	public function getRoleByWhere($map, $Nowpage, $limits) {
		return $this->where($map)->page($Nowpage, $limits)->order('id desc')->select();
	}

	/**
	 * [getRoleByWhere 根据条件获取所有的角色数量]
	 */
	public function getAllRole($where) {
		return $this->where($where)->count();
	}

	/**
	 * [insertRole 插入角色信息]
	 */
	public function insertRole($param) {
		try {
			$result = $this->validate('RoleValidate')->save($param);
			if (false === $result) {
				return ['code' => -1, 'data' => '', 'msg' => $this->getError()];
			} else {
				return ['code' => 1, 'data' => '', 'msg' => '添加角色成功'];
			}
		} catch (PDOException $e) {
			return ['code' => -2, 'data' => '', 'msg' => $e->getMessage()];
		}
	}

	/**
	 * [editRole 编辑角色信息]
	 */
	public function editRole($param) {
		try {
			$result = $this->validate('RoleValidate')->save($param, ['id' => $param['id']]);
			if (false === $result) {
				return ['code' => 0, 'data' => '', 'msg' => $this->getError()];
			} else {
				return ['code' => 1, 'data' => '', 'msg' => '编辑角色成功'];
			}
		} catch (PDOException $e) {
			return ['code' => 0, 'data' => '', 'msg' => $e->getMessage()];
		}
	}

	/**
	 * [getOneRole 根据角色id获取角色信息]
	 */
	public function getOneRole($id) {
		return $this->where('id', $id)->find();
	}

	/**
	 * [delRole 删除角色]
	 */
	public function delRole($id) {
		try {
			$this->where('id', $id)->delete();
			return ['code' => 1, 'data' => '', 'msg' => '删除角色成功'];
		} catch (PDOException $e) {
			return ['code' => 0, 'data' => '', 'msg' => $e->getMessage()];
		}
	}

	/**
	 * [getRole 获取所有的角色信息]
	 */
	public function getRole() {
		return $this->where('id', '<>', 1)->select();
	}

	/**
	 * [getRole 获取角色的权限节点]
	 */
	public function getRuleById($id) {
		$res = $this->field('rules')->where('id', $id)->find();
		return $res['rules'];
	}

	/**
	 * 获取管理员的自定义权限
	 *
	 * @param [type] $adminid
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2024-01-04
	 */
	public function getRuleByAdminid($adminid) {
		$res = Db::name('sys_auth_admin')->field('rules')->where('userid', $adminid)->find();
		return phpnullck($res, 'rules', '');
	}

	/**
	 * [editAccess 分配权限]
	 */
	public function editAccess($param) {
		try {
			$this->save($param, ['id' => $param['id']]);
			return ['code' => 1, 'data' => '', 'msg' => '分配权限成功'];

		} catch (PDOException $e) {
			return ['code' => 0, 'data' => '', 'msg' => $e->getMessage()];
		}
	}

	/**
	 * 给具体的人分配权限， sys_auth_admin
	 *
	 * @param [type] $param
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2024-01-04
	 */
	public function editAdminAccess($param) {

		try {
			$db = Db::name('sys_auth_admin');

			if ($param['userid'] <= 0) {
				return ['code' => -1, 'data' => '', 'msg' => '用户标识错误'];
			}

			$obj = $db->where('userid', $param['userid'])->find();

			$res = false;
			if ($obj) {
				$param['update_time'] = time();
				$res = $db->where('userid', $param['id'])->update(['rules' => $param['rules']]);
			} else {
				$param['create_time'] = time();
				$param['update_time'] = 0;
				$res = $db->insert($param);
			}

			return ['code' => 1, 'data' => '', 'msg' => '分配权限成功'];

		} catch (PDOException $e) {
			return ['code' => 0, 'data' => '', 'msg' => $e->getMessage()];
		}
	}

	/**
	 * [getRoleInfo 获取角色信息]
	 */
	public function getRoleInfo($id) {

		$result = Db::name('Sys_auth_group')->where('id', $id)->find();
		if (empty($result['rules'])) {
			$where = '';
		} else {
			$where = 'id in(' . $result['rules'] . ')';
		}
		$res = Db::name('Sys_auth_rule')->field('name')->where($where)->select();

		foreach ($res as $key => $vo) {
			if ('#' != $vo['name']) {
				$result['name'][] = $vo['name'];
			}
		}

		return $result;
	}
}