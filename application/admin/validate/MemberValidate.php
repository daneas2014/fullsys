<?php

namespace app\admin\validate;
use think\Validate;

class MemberValidate extends Validate
{
    protected $rule = [
        ['username', 'unique:user_base', '该会员已经存在'],
        ['mobile', 'unique:user_base', '该手机号已经存在'],
        ['username', 'require', '会员名必填'],
    ];

}