<?php

namespace app\admin\validate;

use think\Validate;

class AdValidate extends Validate
{
    protected $rule = [
       'Title|标题'  => 'require',
       'EndTime|结束时间'  => 'require',
       'ImagePath|广告图链接'  => 'require',
       'txtAdPath|广告链接'  => 'require',
       'Content|广告备注'  => 'require',
    ];

}