<?php

namespace app\admin\validate;
use think\Validate;

class CampaignsValidate extends Validate
{
    protected $rule = [
        ['Title', 'unique:Campaign', '活动已经存在']
    ];

}