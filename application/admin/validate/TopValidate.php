<?php

namespace app\admin\validate;
use think\Validate;

class TopValidate extends Validate
{
    protected $rule = [
        ['Name', 'unique:Top_New', '资源已经存在']
    ];

}