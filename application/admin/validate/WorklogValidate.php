<?php
namespace app\admin\validate;

use think\Validate;

class WorklogValidate extends Validate
{

    protected $rule = [
        'url|落地连接' => 'require',
        'title|主题' => 'require',
        'type|工作类型' => 'require',
        ['url', 'unique:Sys_worklog', '该任务已经存在']
    ];
}