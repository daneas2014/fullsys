<?php

namespace app\admin\controller;
use app\admin\model\MenuModel;
use think\Db;

class Menu extends Base
{
    /**
     * [index 菜单列表]
     * @return [type] [description]
     */
    public function index()
    {
        $nav = new \org\Leftnav;
        $menu = new MenuModel();
        $admin_rule = $menu->getAllMenu();
        
        $vue=childList($admin_rule);
        $this->assign('vuetable',$vue);
        
        return $this->fetch();
    }
    

    /**
     * 新的保存方式
     */
    public function savemenu(){
        $db=Db::name('sys_auth_rule');
    
        $nav = new \org\Leftnav;
        $menu = new MenuModel();
        $admin_rule = $menu->getAllMenu();
    
        $assign=['parent'=>$nav::rule($admin_rule)];
    
        return parent::singleDataSave($db, input('param.'),'菜单',$assign);
    }


    /**
     * [ruleorder 排序]
     * @return [type] [description]
     */
    public function ruleSort()
    {
        if (request()->isAjax()){
            $param = input('post.');
            $auth_rule = Db::name('sys_auth_rule');
            $auth_rule->where(array('id' => $param['id'] ))->setField('sort' , $param['sort']);
            return json(['code' => 1, 'msg' => '排序更新成功']);
        }
    }

    /**
     * [rule_state 菜单状态]
     * @return [type] [description]
     */
    public function state()
    {
        $id = input('param.id');
        $status = Db::name('sys_auth_rule')->where('id',$id)->value('status');//判断当前状态
        if($status==1)
        {
            $flag = Db::name('sys_auth_rule')->where('id',$id)->setField(['status'=>0]);
            return json(['code' => 1, 'data' => $flag['data'], 'msg' => '已禁止']);
        }
        else
        {
            $flag = Db::name('sys_auth_rule')->where('id',$id)->setField(['status'=>1]);
            return json(['code' => 0, 'data' => $flag['data'], 'msg' => '已开启']);
        }
    }
}
