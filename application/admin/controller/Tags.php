<?php
namespace app\admin\controller;

use think\Db;

use app\common\model\TagsBase;
use app\common\model\ArticleBase;
use app\common\model\ProductBase;
use app\common\model\RelationsBase;
use app\common\model\SupplierBase;
use app\common\model\ResourceBase;

class Tags extends Base
{

    /**
     * 所有标签
     */
    public function index()
    {       
        $key = input('key');                
        $page = input('get.page') ? input('get.page'):1;
        $rows = input('get.rows');// 获取总条数

        $md = new TagsBase();
        $res = $md->getTags($key,$page,$rows);
        
        $data['list'] = $res['list'];
        $data['count'] = $res['count'];
        $data['page'] = $page;
        if(input('get.page')){
            return json($data);
        }
        
        $types = config('targtype');
        $this->assign('types' ,$types);
        
        return $this->fetch();
    }

    public function deltag()
    {
        if (request()->isPost()) {
            
            $md = new TagsBase();
            
            $id = input('post.id');
            
            Db::name('Tags')->where('Id', $id)->delete();
            
            $db = Db::name('tags_relation');
            
            if ($db->where('Tid', $id)->count() > 10) {
                return json([
                    'data' => '',
                    'code' => - 1,
                    'msg' => '本标签绑定数据大于10条不允许删除'
                ]);
            }
            
            $db->where('Tid', $id)->delete();
            
            return json([
                'data' => '',
                'code' => 1,
                'msg' => '删除完毕'
            ]);
        }
    }

    /**
     * 保存标签
     */
    public function savetag()
    {
        $db=Db::name('Tags');
        
        if(request()->isPost())
        {
            $data=input('post.');
            
            $id=$data['Id'];
            
            if($id>0)
            {
                unset($data['Id']);
                $db->where('Id',$id)->update($data);
                return json(['code'=>1,'data'=>'','msg'=>'保存完毕']);
            }
            else{
                $db->insert($data);
                return json(['code'=>1,'data'=>'','msg'=>'保存完毕']);                
            }
                        
            return json(['code'=>-1,'data'=>'','msg'=>'保存失败']);
        }
        
        $id=input('get.id');        
        $tag=$db->where('Id',$id)->find();
        $this->assign('tag',$tag);
        return $this->fetch();
    }
    

    /**
     * 标签关联的资源（关联结果）
     */
    public function sources()
    {
            $type = input('get.type');            
            $tid = input('get.tid');            
            $md = new TagsBase();
            $relations=$md->getRelations($tid, $type, 1, 100);
                                    
            switch ($type) {
                case 0:
                    $data=$this->conUser();                    
                    $template='conUser';
                    break;
                case 1:
                    $data = $this->conProduct();
                    $template = 'conProduct';
                    break;
                case 2:
                    $data = $this->conArticle();
                    $template = 'conArticle';
                    break;
                case 3:
                    $data = $this->conSupplier();
                    $template = 'conSupplier';
                    break;
                case 4:
                    $data = $this->conAsk();
                    $template = 'conAsk';
                    break;
                case 5:
                    $data = $this->conVideo();
                    $template = 'conVideo';
                    break;
                case 6:
                    $data = $this->conDemo();
                    $template = 'conDemo';
                    break;
                case 7:
                    $data = $this->conAd();
                    $template = 'conAd';
                    break;
            }
                        
                                    
            $this->assign(['data'=>$data,'tid'=>$tid,'relations'=>$relations]);
            
            return $this->fetch($template);
    }

    /**
     * 标签关联资源（关联动作）
     */
    public function sourcerelation()
    {
        if(request()->isPost()){
            
            $db=Db::name('tag_relation');            
            $act=input('post.act');
            
            if($act=='conn'){
                $type=input('post.type');
                $tid=input('post.tid');
                $id=input('post.id');        
              
                $data=['Oid'=>$id,'Uid'=>0,'Tid'=>$tid,'TargetType'=>$type,'SignTime'=>date('Y-m-d'),'SignType'=>0,'AccountType'=>'system'];
                
                $id= $db->insertGetId($data);
                
                Db::name('Tags')->where('Id')->setInc('Number');
              
                if($id>0){
                    return json(['code'=>1,'data'=>$id,'msg'=>'绑定成功']);
                }
                
                return json(['code'=>-1,'data'=>'','msg'=>'绑定失败']);
            }
            else{
                $id=input('post.id');
                
                $obj=$db->where('Id',$id)->find();
                
                if($db->where('Id',$id)->delete()){
                                        
                    Db::name('Tags')->where('Id',$obj['Tid'])->setDec('Number');
                    
                    return json(['code'=>1,'data'=>$id,'msg'=>'解除绑定成功']);
                }
                
                return json(['code'=>-1,'data'=>'','msg'=>'解除绑定失败']);            
            }
        }
    }
    
    /**
     * 只要是绑定产品，都调用这个
     * @return \think\response\Json|boolean
     */
    public function conProduct()
    {
        $key = input('key');
        $page = input('get.page') ? input('get.page'):1;
        $rows = input('get.rows');// 获取总条数
        
        $pm=new ProductBase();
        $res=$pm->getProducts($key, $page, $rows);
        
        $data['list'] = $res['list'];
        $data['count'] = $res['count'];
        $data['page'] = $page;
        
        if(input('get.page')){
            return json($data);
        }        
    }
    
    
    public function conGroup(){
        
        $key = input('key');
        $page = input('get.page') ? input('get.page'):1;
        $rows = input('get.rows');// 获取总条数
        $groupid=input('get.id');
        
        $map = [];
        if($key&&$key!==""){
            $map['Title'] = ['like',"%" . $key . "%"];
        }
        
        $article = new ArticleBase();
        $res = $article->getArticleByWhere($map, $page, $rows);
        
        $data['list'] = $res['list'];
        $data['count'] = $res['count'];
        $data['page'] = $page;
        
        if(input('get.page')){
            return json($data);
        }
        
        $r=new RelationsBase();
        $this->assign('relations',$r->getArticleGroupRelations($groupid));
    }
    
    public function conSupplier()
    {
        
        $key = input('key');
        $page = input('get.page') ? input('get.page'):1;
        $rows = input('get.rows');// 获取总条数
        $articleid=input('get.id');
        
        $map = [];
        if($key&&$key!==""){
            $map['Name'] = ['like',"%" . $key . "%"];
        }
        
        $sup=new SupplierBase();
        $res = $sup->getSuppliers($map, $page, $rows);
        
        $list=[];
        foreach ($res['list'] as $l){
            $l['Title']=$l['Name'];
            array_push($list, $l);
        }
        
        $data['list'] = $list;
        $data['count'] = $res['count'];
        $data['page'] = $page;
        
        if(input('get.page')){
            return json($data);
        }
        
        $r=new RelationsBase();
        $this->assign('relations',$r->getArticleSupplierRelation($articleid));
    }
    
    
    
    public function conArticle()
    {        
        $key = input('key');
        $page = input('get.page') ? input('get.page'):1;
        $rows = input('get.rows');// 获取总条数
        $key = input('key');
        
        $map = [];
        if($key&&$key!==""){
            $map['Title'] = ['like',"%" . $key . "%"];
        }
        $am=new ArticleBase();
        $res=$am->getArticleByWhere($map, $page, $rows);
        
        $data['list'] = $res['list'];
        $data['count'] = $res['count'];
        $data['page'] = $page;
        
        if(input('get.page')){
            return json($data);
        }      
    }
    
    public function conVideo(){
        
        $key = input('key');
        $page = input('get.page') ? input('get.page') : 1;
        $rows = input('get.rows'); // 获取总条数
        $articleid = input('get.id');
        
        $rm = new ResourceBase();
        $res = $rm->getVideos($key, 0, $page, $rows);
        
        $data['list'] = $res['list'];
        $data['count'] = $res['count'];
        $data['page'] = $page;
        
        if (input('get.page')) {
            return json($data);
        }
        
    }
    
    public function conDemo(){
        
        $key = input('key');
        $type= input('type');
        $page = input('get.page') ? input('get.page') : 1;
        $rows = input('get.rows'); // 获取总条数
                
        $rm = new ResourceBase();
        $res = $rm->getDemos($key, 0, $type, $page, $rows);
        
        $data['list'] = $res['list'];
        $data['count'] = $res['count'];
        $data['page'] = $page;
        
        if (input('get.page')) {
            return json($data);
        }
    }
}