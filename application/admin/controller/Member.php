<?php

namespace app\admin\controller;
use app\admin\model\MemberModel;
use app\admin\model\MemberGroupModel;
use think\Db;

class Member extends Base{
    //*********************************************会员组*********************************************//
    /**
     * [group 会员组]
     * @author
     */
    public function group(){
        $db=Db::name('customer_level');
        $map=[];
        
        return parent::vueQuerySingle($db,$map);
    }

    /**
     * [add_group 添加会员组]
     * @author
     */
    public function savegroup(){
        $db=Db::name('customer_level');
        $data=input('param.');
        
        return parent::singleDataSave($db, $data);
    }


    //*********************************************会员列表*********************************************//
    /**
     * 会员列表
     * @author
     */
    public function index(){
        $key = input('key');
        $start=input('start');
        $end=input('end');
        $level=input('level');
        $map=[];
        
        if($level&&$level>3){
            $map['AccountLevel']=$level;
        }
        
        if(input('selectall')=="false"){
            $map['a.create_time'] = ['between',[strtotime($start),strtotime($end)]];
        }
        
        if($key&&$key!==""){
            if($key>0&&$key<10000){
                $map['id'] = $key ;
            }
            else{
                $map['username|mobile|email|company'] = ['like',"%" . $key . "%"];
            }
        }       
        
        $member = new MemberModel();
        $Nowpage = input('get.page') ? input('get.page'):1;
        $limits = input('get.rows');// 获取总条数
        
        $res = $member->getMemberByWhere($map, $Nowpage, $limits);        
        
        $data['list']=$res['list'];
        $data['count']=$res['count'];
        $data['page']=$Nowpage;
        
        if(input('get.page'))
        {
            return json($data);
        }
        
        return $this->fetch();
    }
    
    
    /**
     * 会员列表
     * @author
     */
    public function course(){
        $key = input('key');
        $map = [];
        if($key&&$key!==""){
            if($key>0&&$key<1000000){
                $map['b.customerid'] = $key ;
            }
            else{
                $map['UserName|Mobile|Email'] = ['like',"%" . $key . "%"];                
            }
        }
        
        $member = new MemberModel();
        $Nowpage = input('get.page') ? input('get.page'):1;
        $limits = config('list_rows');// 获取总条数
        
        $res = $member->getCourseMember($map, $Nowpage, $limits);
        
        $data['list']=$res['list'];
        $data['count']=$res['count'];
        $data['page']=$Nowpage;
        
        if(input('get.page'))
        {
            return json($data);
        }
        
        return $this->fetch();
    }
    
    public function addmembercourse(){
        
        $db = Db::name('course_access');
        
        $data = input('param.');
        $data['adminid']=session('uid');
        if(array_key_exists('deadline', $data))
        {
            $data['deadline']=strtotime($data['deadline']);
        }
        
        return parent::singleDataSave($db, $data, '课程授权');
    }

    public function savemember()
    {
        $member = new MemberModel();
        if (request()->isGet()) {            
            $id = input('param.id');
            $group = new MemberGroupModel();
            $this->assign('data', $member->getOneMember($id));
            $this->assign('groups',$group);
            return $this->fetch();
        }
        if (request()->isPost()) {            
            $param = input('param.');
            $param['ip'] = request()->ip();
            
            if (array_key_exists('password', $param)&&$param['password']!='') {
                $param['password'] = md5(md5($param['password']) . config('auth_key'));
            }
            
            if (array_key_exists('id', $param)) {
                $flag = $member->editMember($param);
            } else {                
                $flag = $member->insertMember($param);
            }            
            return json($flag);
        }
    }

    public function minfo(){
        if(request()->isPost()){
            $uid=input('uid');
            $member = new MemberModel();            
            $one=$member->get(['Id'=>$uid]);            
            return json(['code'=>1,'data'=>$one,'msg'=>'']);
        }
    }

    /**
     * 删除会员
     * @author
     */
    public function del_member(){
        if(request()->isPost()){
            $id = input('param.id');
            $member = new MemberModel();
            $flag = $member->delMember($id);
            return json($flag);
        }
    }
    
    public function resetpwd(){
        
        if(request()->isPost()){
            $id = input('param.id');
            $password_new= md5(md5('123456789') . config('auth_key'));    
            $member = new MemberModel();
            $result=$member->resetPwd($id,$password_new);
            return json($result);
        }
    }

    /**
     * 合并用户信息，用于一个手机号注册了两个账号的老账号
     * @return mixed|string
     */
    public function mergemember(){
        
        if(request()->isPost()){
            $newid=input('newid');
            $oldid=input('oldid');
            
            
        }
        
        return $this->fetch();
    }


    /**
     * 会员状态
     * @author
     */
    public function member_status(){
        $id = input('param.id');
        $status = Db::name('user_base')->where('id',$id)->value('status');//判断当前状态情况
        if($status==1)
        {
            $flag = Db::name('user_base')->where('id',$id)->setField(['status'=>0]);
            return json(['code' => 1, 'data' => $flag['data'], 'msg' => '已禁止']);
        } else {
            $flag = Db::name('user_base')->where('id',$id)->setField(['status'=>1]);
            return json(['code' => 0, 'data' => $flag['data'], 'msg' => '已开启']);
        }

    }
    /**
     * 重置密码
     */
    public function reset_pwd(){
        $member = new MemberModel();
        if(request()->isPost()){
            $param = input('post.');
            unset($param['username']);
            if(strcmp($param['password'],$param['password_ensure'])!=0){
                return json(['code' => - 1, 'data' => '', 'msg' => '两次密码输入不一致']);
            }
            $param['password'] = md5(md5($param['password']) . config('auth_key'));
            $flag = $member->resetPwd($param);
            return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);
        }
        $id = input('param.id');
        $this->assign([
            'member' => $member->getOneMember($id),
        ]);
        return $this->fetch();
    }

}
