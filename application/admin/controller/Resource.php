<?php
namespace app\admin\controller;

use app\common\model\ResourceBase;
use app\common\model\TagsBase;
use think\Db;

class Resource extends Base
{

    public function attachments()
    {
        $key = input('key');
        $type = input('type');
        $page = input('get.page') ? input('get.page') : 1;
        $rows = input('get.rows'); // 获取总条数
        $productid = input('get.productid') ? input('get.productid') : 0;
        $this->assign('productid', $productid);

        $rm = new ResourceBase();
        $res = $rm->getAttchs($key, $type, $productid, $page, $rows);

        $data['list'] = $res['list'];
        $data['count'] = $res['count'];
        $data['page'] = $page;

        if (input('get.page')) {
            return json($data);
        }

        $resourcetype = config('resourcetype');
        $this->assign('resourcetype', $resourcetype);

        return $this->fetch();
    }

    public function saveatta()
    {
        $input = input('param.');
        $productid = input('get.productid') ? input('get.productid') : 0;
        $assign['productid'] = $productid;
        if ($input['id'] > 0) {
            $tm = new TagsBase();
            $tags = $tm->getObjTagsDetail($id, 6);
            $assign['deltags'] = $tags;
        }

        $db = Db::name('product_download');

        
        $tm = new TagsBase();
        $tags = $tm->getObjTagsDetail($productid, 6);
        $this->assign('deltags', $tags);

        return parent::singleDataSave($db, $input, '资源', $assign);
    }
}
