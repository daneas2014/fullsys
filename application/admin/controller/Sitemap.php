<?php
namespace app\admin\controller;

use think\Db;

class Sitemap extends Base
{

    public function index()
    {
        $list = glob('./sitemap/*.txt');
        $this->assign('list', $list);
        return $this->fetch();
    }

    public function monthsitemap()
    {
        $start = input('param.start');

        $firstday = $start . '-01';
        $endday = $start . '-30 23:59:59';

        if (strstr($start, '-02')) {
            $endday = $start . '-28 23:59:59';
        }

        $map['create_time'] = [
            'between',
            [
                strtotime($firstday),
                strtotime($endday),
            ],
        ];

        $urls = array();

        $list = Db::name('article')->where($map)
            ->field('id')
            ->select();
        foreach ($list as $arr) {
            array_push($urls, SITE_DOMAIN . '/article/' . $arr['id']);
        }

        $list3 = Db::name('video')->where($map)
            ->field('id')
            ->select();
        foreach ($list3 as $arr) {
            array_push($urls, SITE_DOMAIN . '/video/' . $arr['id']);
        }

        $list5 = Db::name('product_download')->where($map)
            ->field('id,type')
            ->select();
        foreach ($list5 as $arr) {
            array_push($urls, SITE_DOMAIN . '/package/detail/' . $arr['type'] . '/' . $arr['Id']);
        }

        $sitemap = '';

        foreach ($urls as $uu) {
            $sitemap .= $uu . "\n";
        }

        $address = 'sitemap/' . $firstday . '.txt';

        file_put_contents($address, $sitemap);

        $this->success('地图生成完毕<br/> 地址：' . SITE_DOMAIN . '/' . $address, url('index'), null, 20);
    }
}
