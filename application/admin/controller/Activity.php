<?php
namespace app\admin\controller;

use think\Controller;
use think\Db;

/**
 *
 */
class Activity extends Base
{
    /*
     *营销活动
     */
    public function campaigns()
    {
        $db = Db::name('campaign');
        return parent::vueQuerySingle($db);
    }

    /**
     * 保存营销活动
     */
    public function savecamp()
    {
        $db = Db::name('campaign');
        $data = input('param.');
        return parent::singleDataSave($db, $data);
    }

    /*
     *资源合集
     */
    public function topics()
    {

        $map = [];

        $key = input('key');

        if ($key != '') {
            $map['name'] = ['like', '%' . $key . '%'];
        }

        $db = Db::name('topic');

        return parent::vueQuerySingle($db, $map);
    }

    /**
     * 话题编辑
     */
    public function savetopic()
    {
        $db = Db::name('topic');

        $data = input('param.');

        $cate = Db::name('article_category')->select();

        $assign = [
            'cate' => $cate->getAllCate(),
        ];

        return parent::singleDataSave($db, $data, '', $assign);
    }

    public function partitions()
    {
        $tid = input('tid');

        $map['topicid'] = $tid;

        $db = Db::name('topic_partitionmodule');

        $assign = [
            'tid' => $tid,
        ];

        return parent::vueQuerySingle($db, $map, '', $assign);
    }

    /**
     * 区块编辑，以及展示标签
     */
    public function savepart()
    {
        $db = Db::name('topic_partitionmodule');

        $data = input('param.');

        $assign = [
            'tid' => input('tid'),
        ];

        return parent::singleDataSave($db, $data, '', $assign);
    }

    public function partitiondetail()
    {
        $db = Db::name('topic_partition');

        $data = input('param.');

        $map['PartitionId'] = input('pid');

        $assign = [
            'pid' => input('pid'),
            'tid' => input('tid'),
        ];

        return parent::vueQuerySingle($db, $map, 'oid desc', $assign);
    }

    /**
     * 展示内容，并关联区块与内容的id
     */
    public function savesource()
    {
        $db = Db::name('topic_partition');

        $data = input('param.');

        $assign = [
            'pid' => input('pid'),
            'tid' => input('tid'),
        ];

        return parent::singleDataSave($db, $data, '', $assign);
    }

}
