<?php
namespace app\admin\controller;

use think\Controller;
use think\Db;

class Wechat extends Controller
{

    private function getGhid()
    {
        $key = 'adminghid';
        $ghid = session($key);
        if ($ghid == null) {
            $app = Db::name('opf_app')->where('mid', session('uid'))->find();
            $ghid = $app['username'];
            session($key, $ghid);
        }

        return $ghid;
    }

    public function follow()
    {
        $list = Db::name('wx_keywords')->where('gh_id', self::getGhid())
            ->order('id asc')
            ->select();
        $this->assign('list', $list);
        return $this->fetch();
    }

    public function savef()
    {
        $db = Db::name('wx_keywords');
        if (request()->isPost()) {
            $data = input('post.');
            $data['gh_id'] = self::getGhid();
            $result = false;

            if ($data['id'] < 1) {
                if ($data['carjump'] == 1) {
                    $db->where('gh_id', $data['gh_id'])->update([
                        'carjump' => 0,
                    ]);
                }
                $result = $db->where('gh_id', $data['gh_id'])->insert($data);
            } else {
                $result = $db->where('id', $data['id'])->update($data);
            }

            if ($result) {
                return json(['code' => 1, 'data' => '', 'msg' => '保存完毕']);
            }

            return json(['code' => -1, 'data' => '', 'msg' => '保存失败']);
        }

        $id = input('get.id');

        $data = $db->where('id', $id)->find();
        $this->assign('data', $data);
        return $this->fetch();
    }

    public function delf()
    {
        $id = input('get.id');

        $db = Db::name('wx_keywords');

        if ($db->where('id', $id)->delete()) {
            return json(['code' => 1, 'data' => '', 'msg' => '关键词已删除']);
        }

        return json(['code' => -1, 'data' => '', 'msg' => '关键词删除失败']);
    }
}
