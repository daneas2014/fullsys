<?php
namespace app\admin\controller;

use think\Db;

class Wekeywords extends Base
{

    public function index()
    {
        $db = Db::name('wx_keywords');
        
        return parent::vueQuerySingle($db);
    }

    public function save()
    {
        $db = Db::name('wx_keywords');
        
        $data = input('param.');
        
        return parent::singleDataSave($db, $data, '微信回复');
    }
}