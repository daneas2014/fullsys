<?php
namespace app\admin\controller;

use think\Db;

class Portalhome extends Base
{

    public function index()
    {
        $db = Db::name('product_home');
        
        $toplevel = $db->where('parentid', 0)->select();
        
        $this->assign('parent', $toplevel);
        
        $parentid = input('parentid');
        $key = input('key');
        $map = [];
        if ($parentid > 0) {
            $map['parentid|id'] = $parentid;
        }
        if ($key && $key != '') {
            $map['productname|tags'] = ['like','%' . $key . '%'];
        }
        
        return parent::vueQuerySingle($db, $map);
    }

    public function saveone()
    {
        $db = Db::name('product_home');
        
        $data = input('param.');
        
        $toplevel = $db->where('parentid', 0)->select();
        
        $assign=['parent'=>$toplevel];
        
        return parent::singleDataSave($db, $data,'门户推荐',$assign);
    }
}