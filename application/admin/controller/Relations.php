<?php
namespace app\admin\controller;

use app\common\model\ArticleBase;
use app\common\model\ProductBase;
use app\common\model\RelationsBase;
use app\common\model\SupplierBase;

/**
 * 这个类用于后端全部用于进行关联的操作，用弹窗形式弹出，返回parent：function childcallback(data){}，也就是说调用的页面必须安放这个方法，data为相关的增项数据清单全部
 *
 * @author Administrator
 *        
 */
class Relations extends Base
{

    public function connects()
    {
        $type = input('get.type');
        $id = input('get.id');
        $source = input('get.source');
        
        switch ($type) {
            case 'articlegroup':
                $data=$this->conGroup();
                $template='conGroup';
                break;
            case 'product':
                $data = $this->conProduct();
                $template = 'conProduct';
                break;
            case 'supplier':
                $data = $this->conSupplier();
                $template = 'conSupplier';
                break;
                
                // 下面三个关联逻辑有待商榷，暂时不做
            case 'article':
                $data = $this->conArticle();
                $template = 'conArticle';
                break;
            case 'campaign':
                $data = $this->conCampaign();
                $template = 'conCampaign';
                break;
            case 'topic':
                $data = $this->conTopic();
                $template = 'conTopic';
                break;
        }
        
        
        $sourced['sourceid']=$id;
        $sourced['sourcetype']=$source;
                
        $this->assign('data', $data);        
        $this->assign('source',$sourced);
        
        return $this->fetch($template);
    }


    /**
     * 只要是绑定产品，都调用这个
     * @return \think\response\Json|boolean
     */
    public function conProduct()
    {       
        $key = input('key');
        $page = input('get.page') ? input('get.page'):1;
        $rows = input('get.rows');// 获取总条数
        $articleid=input('get.id');
        
        $pm=new ProductBase();
        $res=$pm->getProducts($key, $page, $rows);
        
        $data['list'] = $res['list'];
        $data['count'] = $res['count'];
        $data['page'] = $page;
        
        if(input('get.page')){
            return json($data);
        }
        
        $r=new RelationsBase();
        $this->assign('relations',$r->getArticleRelations($articleid));
    }
    
    
    public function conGroup(){
        
        $key = input('key');
        $page = input('get.page') ? input('get.page'):1;
        $rows = input('get.rows');// 获取总条数
        $groupid=input('get.id');
        
        $map = [];
        if($key&&$key!==""){
            $map['title'] = ['like',"%" . $key . "%"];
        }
        
        $article = new ArticleBase();
        $res = $article->getArticleByWhere($map, $page, $rows);
        
        $data['list'] = $res['list'];
        $data['count'] = $res['count'];
        $data['page'] = $page;
        
        if(input('get.page')){
            return json($data);
        }
        
        $r=new RelationsBase();
        $this->assign('relations',$r->getArticleGroupRelations($groupid));
    }

    public function conSupplier()
    {
        
        $key = input('key');
        $page = input('get.page') ? input('get.page'):1;
        $rows = input('get.rows');// 获取总条数
        $articleid=input('get.id');
        
        $map = [];
        if($key&&$key!==""){
            $map['Title'] = ['like',"%" . $key . "%"];
        }
        
        $sup=new SupplierBase();
        $res = $sup->getSuppliers($map, $page, $rows);
        
        $data['list'] = $res['list'];
        $data['count'] = $res['count'];
        $data['page'] = $page;
        
        if(input('get.page')){
            return json($data);
        }
        
        $r=new RelationsBase();
        $this->assign('relations',$r->getArticleSupplierRelation($articleid));
    }

    
    
    public function conArticle($id, $source)
    {
    }
    
    public function conCampaign($id, $source)
    {
    }

    public function conTopic($id, $source)
    {
    }
    
    //=====================================================异步关联操作=============================================================
    /**
     * 关联和取消关联操作，包含了文章关联，话题关联，系列关联
     */
    public function connAction(){
        
        if(request()->isPost()){
            $data=input('post.');
            $type=$data['type'];
            $id=$data['id'];
            
            if($type=='add'){
                $res= $this->addType($data['sourcefrom'], $data['sourceid'], $data['id']);
            }
            else if($type=='del'){
                $res=$this->delType($data['id']);
            }
            
            return json($res);
        }
    }
    
    private function addType($type,$sourceid,$id){        
        $r=new RelationsBase();
        switch ($type){
            case "article":
                return $r->saveProductRelation($sourceid, $id);
            case "articlegroup":                
                return $r->saveArticleGroupRelation($sourceid, $id);
            case "articlesupplier":
                return $r->saveSupplierRelation($sourceid, $id);
        }
        
    }
    
    private function delType($id){
        $r=new RelationsBase();
        return $r->delRalation($id);
    }
}