<?php

namespace app\admin\controller;

use app\admin\model\ArticleModel;
use app\common\model\TagsBase;
use think\Db;

class Article extends Base
{

    /**
     * [index 文章列表]
     */
    public function index()
    {
        $key = input('key');
        $category = input('cate');
        $adminid = input('adminid');
        $start = input('start');
        $end = input('end');

        $map = [];
        if ($key && $key !== "") {
            $map['a.title'] = ['like', "%" . $key . "%"];
        }
        if ($category && $category > 0) {
            $map['a.categoryid'] = $category;
        }
        if ($adminid && $adminid > 0) {
            $map['a.adminid'] = $adminid;
        }
        if ($start && $start != '') {
            $map['a.create_time'] = ['between', [strtotime($start), strtotime($end)]];
        }

        $page = input('get.page') ? input('get.page') : 1;
        $rows = input('get.rows'); // 获取总条数

        $article = new ArticleModel();
        $res = $article->getArticleByWhere($map, $page, $rows);

        $data['list'] = $res['list'];
        $data['count'] = $res['count'];
        $data['page'] = $page;
        if (input('get.page')) {
            return json($data);
        }

        $this->assign('cate', Db::name('article_category')->select());
        $admins = Db::name('sys_admin')->where(['status' => 1])->select();
        $this->assign('admins', $admins);

        return $this->fetch();
    }

    /**
     * 系列文摘
     * @return \think\response\Json|mixed|string
     */
    public function serializes()
    {
        $map = [];

        $key = input('key');
        if ($key && $key !== "") {
            $map['name'] = ['like', "%" . $key . "%"];
        }

        $page = input('get.page') ? input('get.page') : 1;
        $limits = input('get.rows') ? input('get.rows') : config('list_rows'); // 获取总条数

        $db = Db::name('article_group');

        $lists = $db->alias('a')
            ->join('(select groupid,count(groupid)as counts,sum(clickcount) as clicks from article c left join article_attribute d on c.id=d.articleid group by groupid) b', 'a.id=b.groupid', 'left')
            ->where($map)
            ->order('clicks desc')
            ->page($page, $limits)
            ->select();

        $count = $db->where($map)->count(); //计算总页面

        $data['list'] = $lists;
        $data['count'] = $count;
        $data['page'] = $page;
        if (input('get.page')) {
            return json($data);
        }
        return $this->fetch();
    }

    /**
     * 新增保存一条龙
     * @return \think\response\Json|mixed|string
     */
    public function serializesave()
    {

        $db = Db::name('article_group');

        $data = input('param.');

        return parent::singleDataSave($db, $data);
    }

    /**
     * 假删除，关闭即可
     * @return \think\response\Json
     */
    public function delserialize()
    {
        $id = input('param.id');
        $flag = Db::name('article_group')->where('id', $id)->update(['status' => 0]);
        if ($flag) {
            $uid = session('uid');
            $username = session('username');
            writelog($uid, $username, '用户【' . $username . '】删除了表【系列文章】记录:' . $id, 1);

            return json(['code' => 1, 'data' => null, 'msg' => '删除系列完毕']);
        }
        return json(['code' => -1, 'data' => null, 'msg' => '删除过程中出现错误']);
    }

    /**
     * [savearticle 编辑文章]
     *
     * @return [type] [description]
     */
    public function savearticle()
    {
        $db = Db::name('article');

        $data = input('param.');

        $tm = new TagsBase();

        $assign = [
            'cate' => Db::name('article_category')->select(),
            'groups' => Db::name('article_group')->where('status', 1)->select(),
            'article' => $db->find(['id' => $data['id']]),
            'deltags' => $tm->getObjTagsDetail($data['id'], 2),
        ];

        return parent::singleDataSave($db, $data, '文章资讯', $assign);
    }

    /**
     * [article_state 文章状态]
     * @return [type] [description]

     */
    public function article_state()
    {
        $id = input('param.id');
        $status = Db::name('article')->where(array('id' => $id))->value('status'); //判断当前状态情况
        if ($status == 1) {
            $flag = Db::name('article')->where(array('id' => $id))->setField(['status' => 0]);
            return json(['code' => 1, 'data' => $flag['data'], 'msg' => '已禁止']);
        } else {
            $flag = Db::name('article')->where(array('id' => $id))->setField(['status' => 1]);
            return json(['code' => 0, 'data' => $flag['data'], 'msg' => '已开启']);
        }
    }

    // *********************************************分类管理*********************************************//

    /**
     * [index_cate 分类列表]
     *
     * @return [type] [description]
     */
    public function index_cate()
    {
        $db = Db::name('article_category');

        return parent::vueQuerySingle($db, [], 'pid asc');
    }

    /**
     * [edit_cate 编辑分类]
     *
     * @return [type] [description]
     *
     */
    public function savecate()
    {
        $db = Db::name('article_category');

        $data = input('param.');

        $assign = [
            'cates' => $db->select(),
        ];

        return parent::singleDataSave($db, $data, '文章分类', $assign);
    }

    /**
     * [cate_state 分类状态]
     * @return [type] [description]

     */
    public function cate_state()
    {
        $id = input('param.id');
        $status = Db::name('article_cate')->where(array('id' => $id))->value('status'); //判断当前状态情况
        if ($status == 1) {
            $flag = Db::name('article_cate')->where(array('id' => $id))->setField(['status' => 0]);
            return json(['code' => 1, 'data' => $flag['data'], 'msg' => '已禁止']);
        } else {
            $flag = Db::name('article_cate')->where(array('id' => $id))->setField(['status' => 1]);
            return json(['code' => 0, 'data' => $flag['data'], 'msg' => '已开启']);
        }
    }

}
