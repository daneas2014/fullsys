<?php
namespace app\admin\controller;

use think\Db;
use app\admin\model\LogModel;
use com\IpLocation;

class Log extends Base
{
    
    /**
     * [index 操作日志]
     *
     * @return [type] [description]
     */
    public function index()
    {
        $key = input('key');
        $start=input('start');
        $end=input('end');
        $map = [];
        
        if ($key && $key !== "") {
            $map['admin_id'] = $key;
        }
        
        if($start&&$start!=''){
            $map['add_time']=['between',[strtotime($start),strtotime($end)]];
        }
        
        $arr = Db::name("sys_admin")->field("id,username,real_name")->order('status,id desc')->select(); // 获取用户列表
        
        $Nowpage = input('get.page') ? input('get.page') : 1;
        $limits = config('list_rows'); // 获取总条数
        
        $count = Db::name('sys_log')->where($map)->count(); // 计算总页面
                
        $allpage = intval(ceil($count / $limits));
        
        $lists = Db::name('sys_log')->where($map)
        ->page($Nowpage, $limits)
        ->order('add_time desc')
        ->select();
        
        $Ip = new IpLocation('UTFWry.dat'); // 实例化类 参数表示IP地址库文件
        foreach ($lists as $k => $v) {
            $lists[$k]['add_time'] = date('Y-m-d H:i:s', $v['add_time']);
            $lists[$k]['ipaddr'] = $Ip->getlocation($lists[$k]['ip']);
        }
                
        $this->assign("search_user", $arr);
        
        if (input('get.page')) {
            $data['list']=$lists;
            $data['count']=$count;
            return json($data);
        }
        return $this->fetch();
    }
    
    /**
     * [del_log 删除日志]
     *
     * @return [type] [description]
     */
    public function del_log()
    {
        $id = input('param.id');
        $log = new LogModel();
        $flag = $log->delLog($id);
        return json([
            'code' => $flag['code'],
            'data' => $flag['data'],
            'msg' => $flag['msg']
        ]);
    }
    
    public function errorlog()
    {
        $path = RUNTIME_PATH . '/log/' . date('Ym') . '/' . date('d') . '.log';
        
        if(request()->isPost()){
            
            unlink($path);
            
            return json(['code'=>1,'msg'=>'日志已删除','data'=>'']);
        }
        
        $txt = file_get_contents($path);
        
        $this->assign('text',$txt);
        
        return $this->fetch();
    }
}