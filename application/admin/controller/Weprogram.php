<?php
namespace app\admin\controller;

use app\wechat\model\ServiceMinprogram;
use think\Db;

class Weprogram extends Base
{

    /**
     * 淘宝客导航
     * @return \think\response\Json|\app\admin\controller\unknown
     */
    public function tknav()
    {
        $db = Db::name('tb_nav');
        $map = [];
        return parent::vueQuerySingle($db, $map);
    }

    /**
     * 保存淘宝客导航
     * @return \think\response\Json
     */
    public function savetknav()
    {
        $db = Db::name('tb_nav');
        $data = input('param.');
        return parent::singleDataSave($db, $data, "淘宝客导航");
    }

    /**
     * 生成小程序码
     * @return \think\response\Json|mixed|string
     */
    public function mincode()
    {
        if (request()->isPost()) {
            $data = input('post.');
            
            $sm = new ServiceMinprogram();
            
            $res=$sm->makeMinCode($data['path'], $data['title']);
            
            if($res===false){
                return json(['code'=>-1,'data'=>'','msg'=>'']);
            }
            
            return json(['code'=>1,'data'=>$res,'msg'=>'小程序码已生成']);
        }
        
        return $this->fetch();
    }

}