<?php
namespace app\admin\controller;

use Exception;
use think\Db;

class Editpage extends Base
{
    /**
     * 关于我们的页面编辑
     *
     * @return void
     * @author dmakecn@163.com
     * @since 2020-12-03
     */
    public function index()
    {
        if (request()->isAjax()) {
            $data = input('param.');

            $map = [];
            if ($data['key'] != '') {
                $map['bakpath'] = ['like', '%' . $data['key'] . '%'];
            }

            $list = Db::name('sys_editpage')->alias('a')->join('sys_admin b', 'a.adminid=b.id', 'left')
                ->where($map)
                ->page($data['page'], $data['rows'])
                ->field('a.*,b.real_name')
                ->order('id desc')
                ->select();

            $count = Db::name('sys_editpage')
                ->where($map)
                ->count();

            $data['list'] = $list;
            $data['count'] = $count;
            $data['page'] = $data['page'];

            return json($data);
        }

        return $this->view->assign('files', list_file(APP_PATH . 'about/view'))->fetch();
    }

    /**
     * 加载view文件夹，罗列文件，编辑文件
     *
     * @return void
     * @author dmakecn@163.com
     * @since 2020-12-03
     */
    public function edit()
    {
        if (request()->isGet()) {
            $path = input('param.path');
            echo file_get_contents($path);
        }

        if (request()->isPost()) {
            $path = input('param.path');
            $dir = input('param.dir');
            $content = input('param.html');
            $bak = file_get_contents($path);
            $bakdir = str_replace('view', 'bakview', $dir);
            $bakpath = str_replace('view', 'bakview', $path);

            if (!is_dir($bakdir)) {
                mkdir($bakdir, 0755, true); //true代表递归创建
            }

            $recordpath = $bakpath . '.' . date('Ymdhms') . '.bak';

            file_put_contents($recordpath, $bak);

            file_put_contents($path, $content);

            $id = Db::name('sys_editpage')->insertGetId(['create_time' => time(), 'adminid' => session('uid'), 'bakpath' => $recordpath, 'note' => input('param.note')]);

            return $id > 0 ? getJsonCode(true, '文件已替换，源文件已备份至' . $recordpath) : getJsonCode(true, '文件已替换，备份记录失败');
        }
    }

    /**
     * 下载文件
     *
     * @return void
     * @author dmakecn@163.com
     * @since 2022-05-17
     */
    public function pro()
    {
        $db = Db::name('sys_editpage');
        $act = input('param.act');
        $id = input('param.id');
        $now = $db->where('id', $id)->find();
        $filename = substr($now['bakpath'], strrpos($now['bakpath'], '/'));

        if (request()->isAjax()) {

            if ($act == 'cover') {
                return getJsonCode(false, '这个功能还没有做');
            }
        }

        if ($act == 'download') {
            header('Content-Type: application/octet-stream');
            try {
                $filesize = filesize($now['bakpath']); //获得文件大小
            } catch (Exception $e) {
                die($now['bakpath']);
            }

            header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
            header('Cache-Control: no-store, no-cache, must-revalidate');
            header('Cache-Control: pre-check=0, post-check=0, max-age=0');
            header('Content-Transfer-Encoding: binary');
            header('Content-Encoding: none');
            header('Content-type: application/force-download');
            header('Content-length: ' . $filesize);
            header("Content-Disposition: attachment; filename=$filename");
            readfile($now['bakpath']);

        }

    }
}
