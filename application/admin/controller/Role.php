<?php

namespace app\admin\controller;

use app\admin\model\Node;
use app\admin\model\UserType;
use think\Db;

class Role extends Base
{

    /**
     * [index 角色列表]
     * @return [type] [description]
     */
    public function index()
    {
        $key = input('key');
        $map = [];
        if ($key && $key !== "") {
            $map['title'] = ['like', "%" . $key . "%"];
        }

        $db = Db::name('sys_auth_group');

        return parent::vueQuerySingle($db, $map);
    }

    /**
     * 修改管理员角色
     *
     * @return void
     * @author dmakecn@163.com
     * @since 2020-02-28
     */
    public function save()
    {
        $db = Db::name('sys_auth_group');

        $data = input('param.'); 

        return parent::singleDataSave($db, $data, '管理员角色');
    }

    /**
     * [role_state 用户状态]
     * @return [type] [description]
     */
    public function state()
    {
        $id = input('param.id');
        $status = Db::name('sys_auth_group')->where('id', $id)->value('status'); //判断当前状态情况

        $flag = Db::name('Sys_admin')->where('id', $id)->setField(['status' => ($status == 1 ? 0 : 1)]);

        return json(['code' => 1, 'data' => $flag['data'], 'msg' => ($status == 1 ? '已禁止' : '已开启')]);

    }

    /**
     * [giveAccess 分配权限]
     * @return [type] [description]
     */
    public function giveAccess()
    {
        $param = input('param.');
        $node = new Node();
        //获取现在的权限
        if ('get' == $param['type']) {
            $nodeStr = $node->getNodeInfo($param['id']);
            return json(['code' => 1, 'data' => $nodeStr, 'msg' => 'success']);
        }
        //分配新权限
        if ('give' == $param['type']) {

            $doparam = [
                'id' => $param['id'],
                'rules' => $param['rule'],
            ];
            $user = new UserType();
            $flag = $user->editAccess($doparam);
            return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);
        }
    }
}
