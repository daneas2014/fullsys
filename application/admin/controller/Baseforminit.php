<?php

namespace app\admin\controller;

use think\Db;

class Baseforminit extends Base {
	/**
	 * 列举出所有数据库清单
	 *
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2022-03-23
	 */
	public function index() {
		if (input('get.r')) {
			$Db = Db::connect();
			$tmp = $Db->query('SHOW TABLE STATUS');
			$data = array_map('array_change_key_case', $tmp);
			$value['list'] = $data;
			return json($value);
		}
		return $this->fetch();
	}

	/**
	 * 加载数据表结构
	 *
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2022-03-23
	 */
	public function TableShow() {
		if (request()->isGet()) {

			$name = input('tablename');
			$sqlTable = "SHOW FULL COLUMNS FROM $name";
			$obj = Db::query($sqlTable);

			return $this->assign(['table' => $obj, 'name' => $name])->fetch();
		}

		if (request()->isPost()) {
			$data = input('post.');
			$name = $data['name'];
			$sqlTable = "SHOW FULL COLUMNS FROM $name";

			$obj = Db::query($sqlTable);

			// 1.生成文件夹app、controller、view??为什么不分别放controller和view，因为这个功能只是针对后端，方便复制
			$dirName = BASEINIT_PATH . '/' . $name;
			if (!file_exists($dirName)) {
				mkdir($dirName, 0777, true);
			}
			// 2.生成controller
			$this->initcontroller($dirName, $name, explode(',', $data['paramsfield']));
			// 3.生成view
			$this->initlist($dirName, $obj, explode(',', $data['listfield']));
			$this->initform($dirName, $obj, $data['formfield'], $name);

			return json(['data' => '', 'code' => 1, 'msg' => '已生成controller和view文件，请访问' . $dirName]);
		}

	}

	/**
	 * 生成简易controller
	 *
	 * @param [type] $dirName
	 * @param [type] $obj
	 * @param [type] $except
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2023-11-14
	 */
	private function initcontroller($dirName, $name, $fields) {

		$params = '';
		if (is_array($fields)) {
			foreach ($fields as $v) {
				$params .= '$p_' . $v . '=input("param.' . $v . '")?input("param.' . $v . '"):"";
                if($p_' . $v . '&&$p_' . $v . '!=""){
                    $map["' . $v . '"]=$p_' . $v . ';
                }
				';
			}
		}

		// 1. 取模板
		$tp = file_get_contents(BASEINIT_PATH . '/php.txt');

		// 2. 填模板
		$tp = str_replace('[table]', $name, $tp); //命名
		$tp = str_replace('[param]', $params, $tp); // 查询参数

		// 3. 存模板
		$filename = $dirName . "/$name.php";
		$handle = fopen($filename, "w");
		$str = fwrite($handle, $tp);
		fclose($handle);
	}

	/**
	 * 生成列表清单
	 *
	 * @param [type] $obj
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2022-03-23
	 */
	private function initlist($dirName, $obj, $except) {

		$list = "";
		foreach ($obj as $o) {
			if ($o['Key'] == 'PRI' || $o['Extra'] == "auto_increment") {
				continue;
			}
			if (strchr($o['Type'], 'varchar') || strchr($o['Type'], 'int')) {
				$list .= '<el-table-column prop="' . $o['Field'] . '" label="' . $this->ckTitle($o) . '" show-overflow-tooltip align="left" ></el-table-column>';
			}

			if (strchr($o['Type'], 'timestamp')) {
				$list .= '<el-table-column prop="' . $o['Field'] . '" label="' . $this->ckTitle($o) . '" :formatter="dateFormat" align="center"></el-table-column>';
			}

			if (strchr($o['Type'], 'tiny')) {
				$list .= '
                <el-table-column prop="' . $o['Field'] . '" label="' . $this->ckTitle($o) . '" align="center">
                    <template scope="scope">
                        <span v-if="scope.row.' . $o['Field'] . '==0" style="color:#FF4949">0</span>
                        <span v-if="scope.row.' . $o['Field'] . '==1" style="color:#07a379">1</span>
                    </template>
                </el-table-column>';
			}
		}

		// 1. 取模板
		$tp = file_get_contents(BASEINIT_PATH . '/tpList.html');

		// 2. 填模板
		$tp = str_replace('[templatestr]', $list, $tp);

		// 3. 存模板
		$filename = $dirName . '/list.html';
		$handle = fopen($filename, "w");
		$str = fwrite($handle, $tp);
		fclose($handle);
	}

	/**
	 * 生成save
	 *
	 * @param [type] $obj
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2022-03-23
	 */
	private function initform($dirName, $obj, $except, $name) {

		$form = "";
		foreach ($obj as $o) {
			if ($o['Key'] == 'PRI' || $o['Extra'] == "auto_increment") {
				continue;
			}
			$form .= '<div class="hr-line-dashed"></div>
            <div class="form-group">';

			if (strchr($o['Type'], 'int')) {
				$form .= '
                    <label class="col-lg-1 col-sm-2 control-label">' . $this->ckTitle($o) . '：</label>
                    <div class="input-group col-sm-4">
                        <input type="text" class="form-control" name="' . $o['Field'] . '" value="' . (!isset($o['Default']) ? "" : $o['Default']) . '">
                    </div>
                ';
			}

			if (strchr($o['Type'], 'varchar')) {
				$form .= '
                    <label class="col-lg-1 col-sm-2 control-label">' . $this->ckTitle($o) . '：</label>
                    <div class="input-group col-sm-4">
                        <textarea class="form-control" name="' . $o['Field'] . '">' . (!isset($o['Default']) ? "" : $o['Default']) . '</textarea>
                    </div>
                ';
			}

			if (strchr($o['Type'], 'text')) {
				$form .= '
                    <label class="col-lg-1 col-sm-2 control-label">' . $this->ckTitle($o) . '：</label>
                    <div class="input-group col-sm-4">
                        <textarea class="form-control" name="' . $o['Field'] . '" id="myEditor">' . (!isset($o['Default']) ? "" : $o['Default']) . ' </textarea>
                    </div>
                ';
			}

			if (strchr($o['Type'], 'tiny')) {
				$form .= '
            <label class="col-lg-1 col-sm-2 control-label">推荐：</label>
            <div class="col-sm-1">
                <div class="radio i-checks">
                    <input type="radio" name="' . $o['Field'] . '" value="1" />是&nbsp;&nbsp;
                    <input type="radio" name="' . $o['Field'] . '" value="0" checked="checked" />否
                </div>
            </div>
            ';
			}

			$form .= "</div>";
		}

		// 1. 取模板
		$tp = file_get_contents(BASEINIT_PATH . '/tpform.html');

		// 2. 填模板
		$tp = str_replace('[templatestr]', $form, $tp);

		// 3. 存模板
		$filename = $dirName . '/save.html';
		$handle = fopen($filename, "w");
		$str = fwrite($handle, $tp);
		fclose($handle);

	}

	private function ckTitle($o) {
		return $o['Comment'] == '' ? $o['Field'] : $o['Comment'];

	}

}
