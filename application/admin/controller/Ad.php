<?php

namespace app\admin\controller;

use app\admin\model\AdModel;
use app\admin\model\AdPositionModel;
use think\Db;

class Ad extends Base {

	public function index() {

		$map = [];

		$key = input('key');
		if ($key && $key !== "") {
			$map['title'] = ['like', "%" . $key . "%"];
		}

		$id = input('positionid') ? input('positionid') : 0;
		if ($id > 0) {
			$map['positionid'] = $id;
		}

		$page = input('get.page') ? input('get.page') : 1;
		$rows = input('get.rows'); // 获取总条数

		$ad = new AdModel();
		$res = $ad->getAdAll($map, $page, $rows);

		$data['list'] = $res['list'];
		$data['count'] = $res['count'];
		$data['page'] = $page;
		if (input('get.page')) {
			return json($data);
		}

		$adp = new AdPositionModel();

		$assign = [
			'positionid' => $id,
			'positions' => $adp->getAllPosition(),
		];

		return $this->view->assign($assign)->fetch();

	}

	/**
	 * [add_ad 添加广告]
	 *
	 * @return [type] [description]
	 *
	 */
	public function savead() {
		$position = new AdPositionModel();

		$db = Db::name('advertisments');

		$param = input('param.');

		$positionid = isset($param['positionid']) ? $param['positionid'] : 0;

		$assign = [
			'positions' => $position->getAllPosition(),
			'positionid' => $positionid,
		];

		return parent::singleDataSave($db, $param, '', $assign);
	}

	/**
	 * [ad_state 广告状态]
	 * @return [type] [description]

	 */
	public function ad_state() {
		$id = input('param.id');
		$status = Db::name('ad')->where(array('id' => $id))->value('status'); //判断当前状态情况
		if ($status == 1) {
			$flag = Db::name('ad')->where(array('id' => $id))->setField(['status' => 0]);
			return json(['code' => 1, 'data' => $flag['data'], 'msg' => '已禁止']);
		} else {
			$flag = Db::name('ad')->where(array('id' => $id))->setField(['status' => 1]);
			return json(['code' => 0, 'data' => $flag['data'], 'msg' => '已开启']);
		}
	}

	// *********************************************广告位*********************************************//

	/**
	 * [index_position 广告位列表]
	 *
	 * @return [type] [description]
	 */
	public function index_position() {
		$db = Db::name('advertisment_position');

		$map = [];

		$key = input('key');

		if ($key && $key != '') {
			$map['name|pkey|description'] = ['like', '%' . $key . '%'];
		}

		return parent::vueQuerySingle($db, $map);
	}

	/**
	 * [add_position 添加广告位]
	 * @return [type] [description]

	 */
	public function saveposition() {
		$db = Db::name('advertisment_position');

		$param = input('param.');

		return parent::singleDataSave($db, $param);
	}

	/**
	 * [position_state 广告位状态]
	 * @return [type] [description]

	 */
	public function position_state() {
		$id = input('param.id');
		$status = Db::name('ad_position')->where(array('id' => $id))->value('status'); //判断当前状态情况
		if ($status == 1) {
			$flag = Db::name('ad_position')->where(array('id' => $id))->setField(['status' => 0]);
			return json(['code' => 1, 'data' => $flag['data'], 'msg' => '已禁止']);
		} else {
			$flag = Db::name('ad_position')->where(array('id' => $id))->setField(['status' => 1]);
			return json(['code' => 0, 'data' => $flag['data'], 'msg' => '已开启']);
		}
	}

	/**
	 * 广告效果
	 * @return mixed|string
	 */
	public function report() {
		$endtime = date('Y-m-d 00:00:00');

		$list = Db::name('advertisment_bits')->alias('a')
			->field('c.name,c.pkey,COUNT(c.pkey) as total')
			->join('advertisments b', 'a.adid=b.Id', 'left')
			->join('advertisment_position c', 'b.positionid=c.Id', 'left')
			->where('b.endtime', '>', $endtime)
			->group('c.name,c.pkey')
			->order('total desc')
			->select();

		$this->assign('list', $list);
		return $this->fetch();
	}
}
