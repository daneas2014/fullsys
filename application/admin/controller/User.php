<?php

namespace app\admin\controller;

use app\admin\model\Node;
use app\admin\model\UserModel;
use app\admin\model\UserType;
use think\Db;

class User extends Base {

	/**
	 * [index 用户列表]
	 */
	public function index() {
		$key = input('key');
		$map = [];
		if ($key && $key !== "") {
			$map['username'] = ['like', "%" . $key . "%"];
		}
		$page = input('get.page') ? input('get.page') : 1;
		$rows = input('get.rows'); // 获取总条数
		$count = Db::name('Sys_admin')->where($map)->count(); //计算总页面
		$user = new UserModel();
		$lists = $user->getUsersByWhere($map, $page, $rows);
		$data['list'] = $lists;
		$data['count'] = $count;
		$data['page'] = $page;
		if (input('get.page')) {
			return json($data);
		}
		return $this->fetch();
	}

	/**
	 * 保存管理员不含删除
	 *
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2020-02-28
	 */
	public function save() {
		$user = new UserModel();
		if (request()->isAjax()) {

			// 测试账号的漏
			if (request()->isAjax() && session('username') == 'test') {
				return json(['code' => -1, 'msg' => '该账号禁止该操作']);
			}

			$param = input('post.');
			if (empty($param['password'])) {
				unset($param['password']);
			} else {
				$param['password'] = md5(md5($param['password']) . config('auth_key'));
			}

			if ($param['id'] > 0) {
				$flag = $user->editUser($param);
				$group_access = Db::name('Sys_auth_group_access')->where('uid', $param['id'])->update(['group_id' => $param['groupid']]);
			} else {

				$flag = $user->insertUser($param);
				$accdata = array(
					'uid' => $user['id'],
					'group_id' => $param['groupid'],
				);
				$group_access = Db::name('Sys_auth_group_access')->insert($accdata);
			}
			return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);
		}

		$id = input('param.id');
		$role = new UserType();
		$this->assign([
			'user' => $user->getOneUser($id),
			'role' => $role->getRole(),
			'suppliers' => Db::name('supplier')->field('id,name')->where('status', 1)->select(),
		]);
		return $this->fetch();
	}

	/**
	 * 删除用户
	 *
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2020-02-28
	 */
	public function del() {
		$id = input('param.id');
		$role = new UserModel();
		$flag = $role->delUser($id);
		return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);
	}

	/**
	 * 用户状态
	 *
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2020-02-28
	 */
	public function state() {
		$id = input('param.id');

		$status = Db::name('Sys_admin')->where('id', $id)->value('status'); //判断当前状态情况

		$flag = Db::name('Sys_admin')->where('id', $id)->setField(['status' => ($status == 1 ? 0 : 1)]);

		return json(['code' => 1, 'data' => $flag['data'], 'msg' => ($status == 1 ? '已禁止' : '已开启')]);
	}

	/**
	 * 甘特图看板，可以看所有人的，可以看某个人的
	 *
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2020-05-11
	 */
	public function gantt() {
		$users = Db::name('sys_admin')->where('id', 'in', function ($query) {
			$query->table('op_gantt')->distinct(true)->field('uid');
		})->field('id,real_name')->select();

		$map = [];
		if (input('param.uid')) {
			$map['uid'] = input('param.uid');
		}
		if (input('param.keyword')) {
			$map['name'] = ['like', '%' . input('param.name') . '%'];
		}

		$data = Db::name('op_gantt')->where($map)->order('id asc')->select();

		$assign = ['data' => $data, 'users' => $users];
		return $this->view->assign($assign)->fetch();
	}

	/**
	 * 保存甘特图，甘特图子项在页面js组装
	 *
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2020-05-11
	 */
	public function savegantt() {
		$db = Db::name('op_gantt');

		$input = input('param.');

		foreach ($input as $k => $v) {
			if (strpos($k, 'customClass') > -1) {
				unset($input[$k]);
			}
		}

		return parent::singleDataSave($db, $input, '甘特图');
	}

	public function authadmin() {

		if (request()->isAjax()) {
			$node = new Node();
			$param = input('param.');
			$userid = $param['userid'];

			$role = new UserModel();
			$groupid = $role->getUserRoleId($userid);
			if ('get' == $param['type']) {
				$nodeStr = $node->getNodeInfo($groupid);
				return json(['code' => 1, 'data' => $nodeStr, 'msg' => 'success']);
			}

		}

		// 给独立admin授权
		if (request()->isPost()) {
			$param = input('param.');

			$doparam = [
				'userid' => $param['userid'],
				'rules' => $param['rule'],
				'adminid' => session('uid', '', 'admin'),
			];

			if ('give' == $param['type']) {
				$user = new UserType();
				$data = $user->editAdminAccess($doparam);
				return json($data);
			}
		}

		$adminid = input('id');

		// 1.获取该成员的组织node

		// 2.偶去该成员的auth_admin node

		return $this->assign('userid', $adminid)->fetch();
	}
}
