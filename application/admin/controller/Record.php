<?php
namespace app\admin\controller;

use think\Controller;
use think\Db;
use app\common\model\SupplierModel;

class Record extends Base
{

    public function index()
    {
        $page = input('get.page') ? input('get.page'):1;
        $rows = input('get.rows');// 获取总条数        
        
        $sqllist = "select customerid,downloadtime,b.productid,b.downloadtype,b.fileid,c.name,d.name as suppliername
                  from product_downloadrecord b
                  left join product c on b.productid=c.id
                  left join supplier d on c.supplierid=d.id
                  where downloadtime between '%s' and '%s'";

        
        
        $sqluser="select b.id as did, a.id,a.realname,a.nickname,a.mobile,a.email
                  from product_downloadrecord b
                  left join customer a on a.id=b.customerid
                  where downloadtime between '%s' and '%s'                  
                  ";
        
        $sqlcount="select count(1) as cc
                  from product_downloadrecord
                  where downloadtime between '%s' and '%s'";
               
        $starttime= input('start')?input('start') :date('Y-m-d 00:00:00');
        $endtime = input('end')?input('end') :date('Y-m-d 23:59:59');
        
        $kw = input('get.key');
        if ($kw && intval($kw)) {
            $sqllist=sprintf($sqllist . " and  productid=%d",$starttime,$endtime,$kw,$kw);
        }
        else{
            $sqllist=sprintf($sqllist,$starttime,$endtime);
        }
        
        $list=Db::query($sqllist);
        
        $kw = input('get.key');
        if ($kw && intval($kw)) {
            $sqlcount=sprintf($sqlcount . " and  productid=%d",$starttime,$endtime,$kw);
        }
        else{
            $sqlcount=sprintf($sqlcount,$starttime,$endtime);
        }
        $count=Db::query($sqlcount);
        
        $kw = input('get.key');
        if ($kw && intval($kw)) {
            $sqluser=sprintf($sqluser . " and  productid=%d group by a.id,a.realname,a.nickname,a.mobile,a.email,b.id",$starttime,$endtime,$kw);
        }
        else{
            $sqluser=sprintf($sqluser.' group by a.id,a.realname,a.nickname,a.mobile,a.email,b.id',$starttime,$endtime);            
        }
        
        $users=Db::query($sqluser.' order by b.id desc');        
                        
        $newlist=[];
        
        foreach ($users as $u){
            $tem='';
            foreach ($list as $l){
                if($u['id']==$l['Userid']){
                    $tem .='下载详情：[供应商：'.$l['suppliername'].'  产品id:'.$l['productid'].' 产品名称'.$l['name'].']'.'<br/>下载类型：'.$l['downloadtype'].':'.$l['fileid'].' 下载时间:'.$l['DownloadTime'].'<br/>';
                }
            }
            
            $u['log']=$tem;
            array_push($newlist, $u);
            unset($u);
        }
        
                
        $data['list'] = $newlist;
        $data['count'] =(int)$count[0]['cc'];
        $data['page'] = $page;
        if(input('get.page')){
            return json($data);
        }
        
        $this->assign('total',count($users));
        return $this->fetch();
    }
    
    
    public function supplier()
    {
        $page = input('get.page') ? input('get.page') : 1;
        $rows = input('get.rows'); // 获取总条数
        $starttime = input('start') ? input('start') : date('Y-m-d 00:00:00');
        $endtime = input('end') ? input('end') : date('Y-m-d 23:59:59');
        $adminid = input('adminid');
        $supplierid = input('supplierid');
        
        
        $map['downloadtime']=['between',[$starttime,$endtime]];
        
        if ($adminid && $adminid > 0) {
            $map['adminid'] = $adminid;
        }
        if ($supplierid && $supplierid > 0) {
            $map['supplierid'] = $supplierid;
        }
        
        $list = Db::name('product_downloadrecord')->alias('a')
            ->field('a.*,product,supplier')
            ->join('(select adminid,product.id as productid,product.name as product,supplierid,supplier.name as supplier from product left join supplier on product.supplierid=supplier.id) b', 'a.productid=b.productid', 'left')
            ->where($map)
            ->page($page, $rows)
            ->select();
        
        $count = Db::name('product_downloadrecord')->alias('a')
            ->join('(select adminid,product.id as productid,supplierid from product left join supplier on product.supplierid=supplier.id) b', 'a.productid=b.productid', 'left')
            ->where($map)
            ->count();
        
        $data['list'] = $list;
        $data['count'] =$count;
        $data['page'] = $page;
        if(input('get.page')){
            return json($data);
        }

        $admins=Db::name('sys_admin')->where(['groupid'=>2,'status'=>1])->select();
        $this->assign('admins', $admins);
        
        $supplier = new SupplierModel();
        $suppliers = $supplier->getSuppliersOnlyname();
        $this->assign('suppliers',$suppliers);
        return $this->fetch();
    }
}