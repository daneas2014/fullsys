<?php
namespace app\admin\controller;

use think\Db;
use app\common\model\SmsModel;
use app\common\model\MailModel;

class Orders extends Base
{

    public function index()
    {
        $db = Db::name('orders');
        
        $key=input('param.key')?input('param.key'):'';
        $status=input('param.status')?input('param.status'):'';
        $paytype=input('param.paytype');
        
        $MAP=[];
        
        if($key&&$key!=''){
            $MAP['orderno']=$key;
        }
        if($status&&$status!=''){
            $MAP['status']=$status;
        }
        if($paytype&&$paytype!=''){
            $MAP['paymenttype']=$paytype;
        }
               
        return parent::vueQuerySingle($db,$MAP);
    }
    
    public function detail(){
        
        $id=input('id');        
        
        $om=new \app\pay\model\OrderModel();
        
        $order=$om->getOrderDetail($id);
                
        $this->assign('order',$order['order']);
        $this->assign('items',$order['items']);
        
        return $this->fetch();        
    }
    
    public function deliver(){
        
        $orderid=input('id');
        
        $order=Db::name('orders')->where('id',$orderid)->find();
        
        $om=new \app\pay\model\OrderModel();
        
        $res=$om->where('id',$orderid)->setField('status','SD');
        
        if($res){            
            
            writelog(session('uid'), session('username'), '用户【' . session('username') . '】给订单【'.$order['orderno'].'】发货', 1);
            
            return json(['code'=>1,'data'=>'','msg'=>'订单发货，状态已转化为完结']);
        }
    }
    
    public function mailagin(){
        if(request()->isPost()){
            
            $orderid=input('id');
            
            $order=Db::name('orders')->where('id',$orderid)->find();
            
            $sms=new SmsModel($order['ShipToCustomerPhone']);
            $sms->sendOrderpaid($order['subject']);
            
            $mm=new MailModel('dmakecn@163.com');
            $mm->sendOrderPaid($order);
            
            writelog(session('uid'), session('username'), '用户【' . session('username') . '】给订单【'.$order['OrderNo'].'】重发通知短信', 1);
            
            return json(['code'=>1,'data'=>'','msg'=>'订单发货，状态已转化为完结']);   
        }
    }
    
    public function saleadjust()
    {
        if (request()->isPost()) {
            $id = input('post.id');
            $amount = input('post.totalamount');
            $reson = input('post.reason');
            $oldprice = input('post.oldprice');
            $invoice=input('post.Invoice');
            $subject = $reson . ' [' . date('Y-m-d H:i:s') . ' ' . session('username') . '改价' . $oldprice . '至' . $amount . ']';
            
            $res=Db::name('orders')->where('id',$id)->update(['totalamount'=>$amount,'subject'=>$subject,'invoice'=>$invoice]);
            
            if($res){
                return json(['code'=>1,'data'=>'','msg'=>'改价成功']);
            }

            return json(['code'=>-1,'data'=>'','msg'=>'改价失败']);
        }
        
        $orderid=input('id');
        
        $order=Db::name('orders')->where('id',$orderid)->find();
        
        $this->assign('data',$order);
        
        return $this->fetch();
    }
}