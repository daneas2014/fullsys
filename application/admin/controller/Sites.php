<?php
namespace app\admin\controller;

use think\Db;

class Sites extends Base
{

    public function index()
    {
        $db = Db::name('sites');
        return parent::vueQuerySingle($db);
    }

    public function savesite()
    {
        $db = Db::name('sites');
        $data = input('param.');
        return parent::singleDataSave($db, $data);
    }

    public function channels()
    {
        $siteid=0;
        
        if(input('param.siteid')){
            $siteid=input('param.siteid');
        }
        
        $this->assign('siteid',$siteid);
        
        $db = Db::name('sites_channel');
        return parent::vueQuerySingle($db);
    }

    public function savechannel()
    {
        $db = Db::name('sites_channel');
        $data = input('param.');
                
        $siteid=0;
        if(array_key_exists('siteid', $data)){
            $siteid=$data['siteid'];
        }
        $this->assign('siteid',$siteid);        
        
        return parent::singleDataSave($db, $data);
    }
}