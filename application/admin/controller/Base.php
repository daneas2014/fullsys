<?php
namespace app\admin\controller;

use app\admin\model\Node;
use com\Fastadmin;
use think\Controller;

class Base extends Controller {

	public function _initialize() {
		$uid = session('uid', '', 'admin');
		$username = session('username', '', 'admin');

		if (!$uid || !$username) {
			$this->redirect('login/index');
		}

		$auth = new \com\Auth();
		$module = strtolower(request()->module());
		$controller = strtolower(request()->controller());
		$action = strtolower(request()->action());
		$url = $module . "/" . $controller . "/" . $action;

		//跳过检测以及主页权限
		if (!in_array($uid, [1, 2])) {
			if (!in_array($url, ['admin/index/index', 'admin/index/indexpage', 'admin/upload/upload', 'admin/index/uploadface'])) {
				if (!$auth->check($url, $uid)) {
					$this->error('抱歉，您没有操作权限');
				}
			}
		}

		$node = new Node();
		$this->assign([
			'username' => session('username', '', 'admin'),
			'portrait' => session('portrait', '', 'admin'),
			'rolename' => session('rolename', '', 'admin'),
			'menu' => $node->getMenu($uid == 1 ? '' : session('rule', '', 'admin')),
		]);

		$config = cache('db_config_data','',null,'admin');

		if (!$config) {
			$config = load_config();
			cache('db_config_data', $config,null,'admin');
		}

		config($config);

		if (config('web_site_close') == 0 && session('uid', '', 'admin') != 1) {
			$this->error('站点已经关闭，请稍后访问~');
		}

		if (config('admin_allow_ip') && session('uid', '', 'admin') != 1) {
			if (in_array(request()->ip(), explode('#', config('admin_allow_ip')))) {
				$this->error('403:禁止访问');
			}
		}
	}

	/**
	 * 适用于单表查询，有id,title,keywords,description的表，前端用VUE展示
	 *
	 * @param unknown $db
	 * @param array $map
	 * @param array $orderby
	 * @param $assign 传入要渲染的参数
	 * @param string $fields 是否需要精准搜索字段
	 * @return \think\response\Json|unknown
	 */
	public function vueQuerySingle($db, $map = [], $orderby = 'id desc', $assign = [], $fields = null) {
		return (new Fastadmin())->vueQuerySingle($db, $map, $orderby, $assign, $fields);
	}

	/**
	 * 适用于单表保存，主键为id的表,有update_time,create_time
	 *
	 * @param unknown $db
	 * @param unknown $data
	 * @param $assign 传入要渲染的参数
	 * @return \think\response\Json
	 */
	public function singleDataSave($db, $data, $dbname = '', $assign = []) {

		if (request()->isAjax() && session('username', '', 'admin') == 'test') {
			return json(['code' => -1, 'msg' => '该账号禁止该操作']);
		}

		return (new Fastadmin())->singleDataSave($db, $data, $dbname, $assign);
	}
}
