<?php
namespace app\admin\controller;
use think\Db;

/**
 * 插件管理
 *
 * @author dmakecn@163.com
 * @since 2022-04-27
 */
class Addons extends Base {

	/**
	 * 罗列出所有插件及状态
	 *
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2022-04-27
	 */
	public function index() {

		$db = Db::name('addons');
		$allplugin = $db->column('name'); //获得一个array

		$arr = array();
		$data = scandir(ADDON_PATH); //获取所有的插件目录，也会把隐藏目录识别出来

		// 所有插件  $arr里面看看

		$addons_standby = array(); // 未安装的

		foreach ($data as $value) {
			if (is_dir(ADDON_PATH . $value) && $value != '.' && $value != '..') {
				$class = "addons\\$value\\Index";
				$a = new $class();
				$info = $a->info;
				$arr[] = $info;

				if ($allplugin && !in_array($info['name'], $allplugin)) {
					$info['ispublish'] = 0;
					$info['manage'] = addon_url($info['manage']);
					$addons_standby[] = $info;
				}
			}
		}

		if (request()->isAjax()) {

			if ($arr == []) {
				return json(['list' => [], 'count' => 0, 'page' => 1]);
			}

			// 在看已安装插件
			$page = input('get.page') ? input('get.page') : 1;
			$rows = input('get.rows'); // 获取总条数

			$res = $db->page($page, $rows)->select();

			// 都没安装
			if (count($res) == 0) {
				return json(['list' => $arr, 'count' => count($arr), 'page' => $page]);
			}

			$showlist = [];
			foreach ($res as $r) {
				for ($i = 0; $i < count($arr); $i++) {

					if ($arr[$i]['name'] == $r['name']) {
						$r['manage'] = addon_url($arr[$i]['manage']);
						$r['ispublish'] = 1;

						array_push($showlist, $r);
					}
				}
			}

			$showlist = array_merge($addons_standby,$showlist);

			return json(['list' => $showlist, 'count' => $db->count(), 'page' => $page]);
		}

		return $this->fetch();
	}

	public function excute() {

		if (request()->isPost()) {

			$data = input('param.');

			if ($data['act'] == 'install') {
				return $this->install($data['name']);
			}

			if ($data['act'] == 'uninstall') {

				if (in_array($data['name'], array('tbk', 'wechat', 'apps', 'openflat', 'doclib'))) {
					return json(['code' => 0, 'msg' => '该插件是系统默认插件，不可卸载']);
				}

				return $this->unstall($data['id'], $data['name']);
			}
			if ($data['act'] == 'stop') {
				$res = Db::name('addons')->where('id', $data['id'])->update(['status' => 0]);
			}
			if ($data['act'] == 'start') {
				$res = Db::name('addons')->where('id', $data['id'])->update(['status' => 1]);
			}
			if ($res == 1) {
				return json(['code' => 1, 'msg' => '操作成功']);
			}
			return json(['code' => 0, 'msg' => '操作失败']);
		}
	}

	/**
	 * 安装插件
	 *
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2022-04-27
	 */
	private function install($name) {
		$class = "addons\\$name\\Index";

		$res = (new $class())->install();

		if ($res) {
			return json(['code' => 1, 'msg' => '安装完毕']);
		}

		return json(['code' => 0, 'msg' => '安装失败']);
	}

	/**
	 * 卸载插件
	 *
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2022-04-27
	 */
	private function unstall($id, $name) {

		$class = "addons\\$name\\Index";

		$res = (new $class())->uninstall();

		if ($res) {
			return json(['code' => 1, 'msg' => '卸载完毕']);
		}

		return json(['code' => 0, 'msg' => '卸载失败']);
	}

	/**
	 * 插件设置
	 *
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2022-04-27
	 */
	public function setting() {

	}
}