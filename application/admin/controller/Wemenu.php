<?php
namespace app\admin\controller;

use think\Db;


class Wemenu extends Base
{

    public function index()
    {
        $list = Db::name('wx_custom_menu')->order('sort asc')->select();
        
        $father = array();
        if ($list != null) {
            foreach ($list as $item) {
                if ($item['pid'] == 0) {
                    array_push($father, $item);
                }
            }
        }
        
        if ($list != null) {
            $list = subTreeLevel($list);
        }
        
        $this->assign('menus', $list);
        $this->assign('father', $father);
        
        return $this->fetch();
    }

    public function savemenu()
    {
        $db = Db::name('wx_custom_menu');
        
        if (request()->isPost()) {
            $id = input('id');
            $param = input('post.');
            $result = false;
            if ($id > 0) {
                unset($param['id']);
                $result = $db->where('id', $id)->update($param);
            } else {
                $count = $db->where('pid', $param['pid'])->count();
                
                if ($count >= 5) {
                    return json([
                        'code' => - 1,
                        'data' => null,
                        'msg' => '微信自定义菜单每组只能有5个子菜单'
                    ]);
                }
                
                $result = $db->insert($param);
            }
            
            if ($result) {
                return json([
                    'code' => 1,
                    'data' => null,
                    'msg' => '保存成功'
                ]);
            } else {
                return json([
                    'code' => - 1,
                    'data' => null,
                    'msg' => '保存失败'
                ]);
            }
        } else {
            $father = $db->where('pid', 0)
                ->order('sort asc')
                ->select();
            
            $data = $db->where('id', input('id'))->find();
            
            $this->assign('father', $father);
            $this->assign('menu', $data);
            
            return $this->fetch();
        }
    }

    public function deletemenu()
    {
        $id = input('id');
        $db = Db::name('wx_custom_menu');
        $item = $db->where('id', $id)->find();
        
        $result = false;
        $result = $db->where('id', $id)->delete();
        
        if ($item['pid'] == 0) {
            $result = $db->where('pid', $id)->delete();
        }
        
        if ($result) {
            return json([
                'code' => 1,
                'data' => null,
                'msg' => '删除菜单完毕'
            ]);
        } else {
            return json([
                'code' => - 1,
                'data' => null,
                'msg' => '失败'
            ]);
        }
    }

    public function ruleorder()
    {
        if (request()->isAjax()) {
            $param = input('post.');
            $auth_rule = Db::name('wx_custom_menu');
            foreach ($param as $id => $sort) {
                $auth_rule->where(array(
                    'id' => $id
                ))->setField('sort', $sort);
            }
            return json([
                'code' => 1,
                'msg' => '排序更新成功'
            ]);
        }
    }

    public function pushmenu()
    {
        $db = Db::name('wx_custom_menu');
        $list = $db->order('sort asc')->select();
        
        $buttons = [];
        foreach ($list as $item) {
            if ($item['pid'] == 0) {
                $array = self::childNode($list, $item['id']);
                if ($array == false) {
                    if ($item['type'] === 'miniprogram') {
                        
                        $buttons[] = [
                            'name' => $item['name'],
                            'type' => $item['type'],
                            'url' => $item['value'],
                            'appid' => $item['appid'],
                            'pagepath' => $item['pagepath']
                        ];
                    } else {
                        $buttons[] = [
                            'name' => $item['name'],
                            'type' => $item['type'],
                            self::menutype($item['type']) => $item['value']
                        ];
                    }
                    continue;
                }
                
                $subbuttons = [];
                foreach ($array as $a) {
                    if ($a['type'] == 'miniprogram') {
                        $subbuttons[] = [
                            'type' => $a['type'],
                            'name' => $a['name'],
                            'url' => $a['value'],
                            'appid' => $a['appid'],
                            'pagepath' => $a['pagepath']
                        ];
                    } else {
                        $subbuttons[] = [
                            'type' => $a['type'],
                            'name' => $a['name'],
                            self::menutype($a['type']) => $a['value']
                        ];
                    }
                }
                
                $buttons[] = [
                    'name' => $item['name'],
                    'sub_button' => $subbuttons
                ];
            }
        }
        
        $wxapp = new \addons\wechat\controller\Base();
        $menu = $wxapp->wxapp()->menu;
        $result = $menu->create($buttons);
        
        return json($result);
    }

    public function childNode($list, $pid)
    {
        $array = array();
        foreach ($list as $item) {
            if ($item['pid'] == $pid) {
                array_push($array, $item);
            }
        }
        
        if (count($array) == 0) {
            return false;
        }
        return $array;
    }

    public function menutype($type)
    {
        switch ($type) {
            case 'click':
                return 'key';
                break;
            case 'view':
                return 'url';
                break;
            case 'miniprogram':
                return 'url';
                break;
        }
    }
}