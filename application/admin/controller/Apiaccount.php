<?php

namespace app\admin\controller;

/**
 * 多个页面账号，mail、pay、qqauth、sinaauth、taokeapi、wxauth
 *
 * @author dmakecn@163.com
 * @since 2022-05-13
 */
class Apiaccount extends Base {
	public function index() {

		$assign = [
			'mail' => config('mail'),
			'pay' => config('pay'),
			'qqauth' => config('qqauth'),
			'sinaauth' => config('sinaauth'),
			'taokeapi' => config('taokeapi'),
			'wxauth' => config('wxauth'),
			'cloud' => config('cloud'),
		];

		return $this->view->assign($assign)->fetch();
	}

	public function save() {
		if (request()->isPost()) {
			$data = input('post.');

			$file = $data['act'];

			unset($data['act']);

			$php = "<?php\r\n";
			$php .= "return [\r\n";

			foreach ($data as $key => $value) {
				$php .= "    '$key' => '$value',\r\n";
			}

			$php .= "];";

			file_put_contents(CONF_PATH . 'extra/' . $file . '.php', $php);

			return getJsonCode(true, '保存成功');
		}

	}
}