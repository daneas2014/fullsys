<?php
namespace app\admin\controller;

use think\Db;

class Comment extends Base
{
    
    public function index()
    {        
        $db=Db::name('comments');
        $map=[];
        $key=input('param.key');
        if($key&&$key!='')
        {
            $map['content']=$key;
        }
        
        $Nowpage = input('get.page') ? input('get.page'):1;
        $limits =  input('get.rows') ? input('get.rows'):config('list_rows');// 获取总条数
        
        $list= $db->alias('a')->field('a.*,b.email,b.mobile')->where($map)
        ->join('customer b','a.Userid=b.id','left')
        ->page($Nowpage, $limits)
        ->order('id desc')
        ->select();
        
        $comments=[];
        foreach ($list as $l){
            $title='';
            $url='';            
            if($l['moduletype']==2){
                $c=Db::name('product')->where('id',$l['targetid'])->find();
                $title=$c['name'];
                $url='/commodity/'.$c['id'];
            }
            
            if($l['moduletype']==1){
                $c=Db::name('video')->where('id',$l['targetid'])->find();
                $title=$c['title'];
                $url='/video/'.$c['id'];
            }
            
            if($l['moduletype']==3){
                $c=Db::name('article')->field('id,title')->where('id',$l['targetid'])->find();
                $title=$c['Title'];
                $url='/article/'.$c['id'];
            }
            
            if($l['moduletype']==4){
                $c=Db::name('course_description')->where('id',$l['targetid'])->find();
                $title=$c['title'];
                $url='/course/'.$c['id'];
            }
            
            $l['title']=$title;
            $l['url']=$url;
            
            array_push($comments, $l);
            unset($l);
        }
        
        $count = $db->where($map)->count();
        
        $data['list']=$comments;
        $data['count']=$count;
        $data['page']=$Nowpage;
        
        if(input('get.page'))
        {
            return json($data);
        }
        
        return $this->fetch();  
    }

    public function savecomment()
    {
        $db=Db::name('comments');
        $data=input('param.');
        return parent::singleDataSave($db, $data);
    }
}