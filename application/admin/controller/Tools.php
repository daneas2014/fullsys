<?php
namespace app\admin\controller;

use app\common\model\LicenseModel;

class Tools extends Base
{

    /**
     * 工具合集
     */
    public function index()
    {}

    /**
     * 上传CRM的授权表格
     */
    public function uploadlicense()
    {
        if (request()->isPost()) {

            $file = request()->file('file');
            $info = $file->validate([
                'size' => 1024 * 512,
                'ext' => 'xlsx,xls,csv',
            ])->move(ROOT_PATH . 'public' . DS . 'excel');

            if ($info) {

                $exclePath = $info->getSaveName(); // 获取文件名
                $file_name = ROOT_PATH . 'public' . DS . 'excel' . DS . $exclePath; // 上传文件的地址
                $excel_array = importExecl($file_name, 0, 20);
                $title = array_shift($excel_array);

                $lm = new LicenseModel();
                $errors = [];
                $j = 0;
                foreach ($excel_array as $d) {

                    $license = $this->initLicense($d);

                    $res = $lm->updateLicense($license['licensekey'], $license['licensename'], $license['productid'], $license['listprice'], $license['remark'], $license['minqty'], $license['atypestr'], $license['active']);

                    $res ? $j++ : array_push($errors, $d);
                }

                if ($errors == [] || count($errors) == 0) {
                    return json(['code' => 1, 'data' => '', 'msg' => $j . '全部导入完毕']);
                }

                return json(['code' => 100, 'data' => $errors, 'msg' => count($errors) . '条导入失败']);
            }
        }

        return $this->fetch();
    }

    /**
     * 组装成保存的格式
     * @param unknown $data
     * @return unknown
     */
    public function initLicense($data)
    {
        $license['licensekey'] = $data['B'];
        $license['licensename'] = $data['D'];
        $license['productid'] = $data['F'];
        $license['listprice'] = $data['C'];
        $license['remark'] = $data['T'];
        $license['minqty'] = $data['E'];
        $license['atypestr'] = $data['G'];

        $active['Supplierprice'] = $data['I'];
        $active['Activityprice'] = $data['J'];
        $active['Activitydeadline'] = $data['K'];
        $active['ActivityType'] = $data['L'];
        $active['Invoice'] = $data['M'];
        $active['newprice'] = $data['N'];
        $active['validity'] = $data['O'];

        $license['active'] = $active;

        return $license;
    }
    
    /**
     * 群发邮件清单
     *
     * @return void
     * @author dmakecn@163.com
     * @since 2020-03-18
     */
    public function mails()
    {

        $db = Db::name('op_mailsender');

        return parent::vueQuerySingle($db);
    }

    /**
     * 只是修改和保存邮件，不提供删除服务，并且注意预设header、footer模板，编辑勾选的情况下给加进去
     *
     * @return void
     * @author dmakecn@163.com
     * @since 2020-03-18
     */
    public function savemail()
    {

        $db = Db::name('op_mailsender');

        $data = input('param.');

        return parent::singleDataSave($db, $data);
    }

    /**
     * 发送测试邮件
     *
     * @return void
     * @author dmakecn@163.com
     * @since 2020-03-18
     */
    public function testmail()
    {

        if (request()->isPost()) {

            $input = input('post.');

            $mail = Db::name('op_mailsender')->find(['id' => $input['id']]);

            if (!$mail || !$mail['testmail']) {
                return json(['code' => -1, 'data' => '', 'msg' => '邮件不存在']);
            }

            $result = sendMail($mail['testmail'], $mail['subject'], $mail['header'] . $mail['body'] . $mail['footer']);

            if ($result) {
                Db::name('op_mailsender')->where('id', $input['id'])->update(['test_time' => time()]);
                return json(['code' => 1, 'data' => '', 'msg' => '已经发送测试']);
            }

            return json(['code' => -1, 'data' => '', 'msg' => '已经发送测试，但是失败了']);
        }

        $mailid = input('get.mailid');

        $this->assign('id', $mailid);

        return $this->fetch();
    }

    /**
     * 经过测试的邮件都可以被发送
     *
     * @return void
     * @author dmakecn@163.com
     * @since 2020-03-18
     */
    public function sendmaillist()
    {

        if (request()->isPost()) {

            $input = input('post.');

            $mail = Db::name('op_mailsender')->find(['id' => $input['id']]);

            /////////////////    排异1       /////////////////
            if (!$mail) {
                return json(['code' => -1, 'data' => '', 'msg' => '邮件不存在']);
            }

            if (!$mail['testmail'] || $mail['test_time'] == 0) {
                return json(['code' => -1, 'data' => '', 'msg' => '邮件还未测试效果，请测试后再尝试发送！']);
            }

            if ($mail['send_time'] > 0) {
                return json(['code' => -1, 'data' => '', 'msg' => '本邮件已经发送过啦！']);
            }

            /////////////////    排异2       /////////////////
            $email = $mail['header'] . $mail['body'] . $mail['footer'];
            $result = sendMail($mail['sendto'], $mail['subject'], $email);

            if ($result) {

                Db::name('op_mailsender')->where('id', $input['id'])->update(['send_time' => time()]);

                return json(['code' => 1, 'data' => '', 'msg' => '已经发送' . $mail['subject']]);
            }

            return json(['code' => -1, 'data' => '', 'msg' => '已经发送邮件，但是失败了']);
        }

    }
}
