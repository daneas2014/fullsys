<?php
namespace app\admin\controller;

use think\Db;

class Msg extends Base
{

    public function codemsg()
    {
        $db = Db::name('Customervalidate');
        
        $key = input('key');
        
        $map = [];
        
        if ($key && $key != '') {
            $map['valiaccount'] = [
                'like',
                '%' . $key . '%'
            ];
        }
        
        return parent::vueQuerySingle($db, $map);
    }
}