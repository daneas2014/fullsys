<?php
namespace app\admin\controller;

use app\common\model\ResourceBase;
use app\common\model\TagsBase;
use think\Db;

/**
 *
 * @author Administrator
 *        
 */
class Video extends Base
{

    /**
     * 所有课程
     * @return unknown
     */
    public function index()
    {
        $courseid=input('param.courseid');
        
        $this->assign('courseid',$courseid?$courseid:0);
        
        $db = Db::name('series_description');
        
        return parent::vueQuerySingle($db);
    }

    public function savecourse()
    {
        $db = Db::name('series_description');
        
        $data = input('param.');
        $data['adminid']=session('uid');
        
        return parent::singleDataSave($db, $data,'课程');
    }
    
    /**
     * 课程章节
     * @return \think\response\Json|\app\admin\controller\unknown
     */
    public function charts(){
        
        $courseid = input('param.courseid') ? input('param.courseid') : 0;        

        $assign=['courseid'=> $courseid];        
        
        $db = Db::name('series_group');
        
        $map['courseid']=$courseid;
        
        return parent::vueQuerySingle($db,$map,'id desc',$assign);
    }

    
    public function savecharts(){
        
        $db = Db::name('series_group');
        
        $data = input('param.');          
        
        $assign=['courseid'=> $data['courseid']];
        
        return parent::singleDataSave($db, $data,'课程章节',$assign);
    }
    
    /**
     * 课程单条
     * @return unknown
     */
    public function items()
    {
        $parentid = input('get.courseid') ? input('get.courseid') : 0;
        
        $this->assign('courseid', $parentid);
        
        $db = Db::name('series_item');
        
        $map=[];
        if($parentid>0)
        {
            $map['courseid']=$parentid;    
        }
        
        return parent::vueQuerySingle($db,$map);
    }

    public function saveitem()
    {
        $db = Db::name('series_item');
        
        $data = input('param.');
        
        $charts=Db::name('series_group')->where('courseid',$data['courseid'])->select();
        
        $this->assign('charts',$charts);
        
        $this->assign('courseid',$data['courseid']);
        
        return parent::singleDataSave($db, $data,'课时');
    }

    

    public function video()
    {

        $key = input('key');
        $productid = input('get.productid') ? input('get.productid') : 0;
        $this->assign('productid', $productid);
        $page = input('get.page') ? input('get.page') : 1;
        $rows = input('get.rows'); // 获取总条数
        $articleid = input('get.id');

        $rm = new ResourceBase();
        $res = $rm->getVideos($key, $productid, $page, $rows);

        $data['list'] = $res['list'];
        $data['count'] = $res['count'];
        $data['page'] = $page;

        if (input('get.page')) {
            return json($data);
        }

        return $this->fetch();
    }

    public function savevideo()
    {
        $db = Db::name('video');

        $data = input('param.');

        $assign = [];

        if (request()->isGet()) {

            $tags = null;
            if ($data['id'] > 0) {
                $tm = new TagsBase();
                $tags = $tm->getObjTagsDetail($data['id'], 5);
            }
            $assign['deltags'] = $tags;
            if (!array_key_exists('productid', $data)) {
                $assign['productid'] = 0;
            } else {
                $assign['productid'] = $data['productid'];
            }
        }

        return parent::singleDataSave($db, $data, '视频', $assign);
    }
}