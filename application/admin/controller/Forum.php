<?php
namespace app\admin\controller;

use app\common\model\SupplierBase;
use think\Db;

class Forum extends Base
{

    /**
     * 各个版块的信息清单
     */
    public function index()
    {
        $key = input('key');

        $map = [];
        if ($key && $key != '') {
            $map['title'] = ['like', '%' . $key . '%'];
        }

        $db = Db::name('bbs_forum');

        return parent::vueQuerySingle($db, $map);
    }

    /**
     * 保存版块 *
     */
    public function saveforum()
    {
        $supplier = new SupplierBase();
        $suppliers = $supplier->getSuppliersOnlyName();
        $this->assign('sus', $suppliers);

        $db = Db::name('bbs_forum');
        return parent::singleDataSave($db, input('param.'), '论坛版块');
    }

    /**
     * 主题帖清单
     */
    public function threads()
    {
        $db = Db::name('bbs_thread');

        $key = input('key');
        $fid = input('get.fid') ? input('get.fid') : 0;
        $this->assign('fid', $fid);

        $map = [];
        if ($key && $key != '') {
            if ($key > 0) {
                $map['id'] = $key;
            } else {
                $map['name|title'] = ['like', '%' . $key . '%'];
            }
        }
        if ($fid > 0) {
            $map['fid'] = $fid;
        }

        return parent::vueQuerySingle($db, $map);
    }

    /**
     * 保存主题帖
     */
    public function savethread()
    {
        $db = Db::name('bbs_thread');

        $forum = Db::name('bbs_forum')->select();
        $this->assign('forum', $forum);

        $data = input('param.');
        if (array_key_exists('topendtime', $data)) {
            $data['topendtime'] = strtotime($data['topendtime']);
        }

        return parent::singleDataSave($db, $data, '论坛主题帖');
    }

    /**
     * 回复审核
     */
    public function posts()
    {
        $tid = input('get.tid') ? input('get.tid') : 0;
        $this->assign('tid', $tid);

        $db = Db::name('bbs_post');

        $key = input('key');
        $map = [];
        if ($key && $key != '') {
            $map['title'] = ['like', '%' . $key . '%'];
        }
        if ($tid > 0) {
            $map['tid'] = $tid;
        }

        return parent::vueQuerySingle($db, $map);
    }

    /**
     * 保存回复
     */
    public function savepost()
    {
        $db = Db::name('bbs_post');
        return parent::singleDataSave($db, input('param.'), '论坛回帖');
    }
}
