<?php
namespace app\admin\controller;

use app\common\model\CouponModel;
use app\common\model\SmsModel;
use think\Db;

class Coupon extends Base
{

    public function index()
    {
        $db = Db::name('Coupon');

        $start = strtotime(input('param.start'));
        $end = strtotime(input('param.end'));
        $type = input('param.type');

        $map['create_time'] = ['between', [$start, $end]];
        $key = input('param.key') ? input('param.key') : '';
        if ($key && $key != '') {
            $map['name|code|amount'] = ['like', '%' . $key . '%'];
        }
        if ($type == "0") {
            $map['pid'] = 0;
        }
        if ($type == "1") {
            $map['pid'] = ['>', 0];
        }

        return parent::vueQuerySingle($db, $map);
    }

    public function savecoupon()
    {
        $db = Db::name('Coupon');

        $data = input('param.');
        $data['adminid'] = session('uid');
        if (array_key_exists('end_time', $data)) {
            $data['end_time'] = strtotime($data['end_time']);
        }
        if (!array_key_exists('totalnum', $data) && array_key_exists('leftnum', $data)) {
            $data['totalnum'] = $data['leftnum'];
        }

        return parent::singleDataSave($db, $data, '优惠券');
    }

    public function membercoupon()
    {

        $db = Db::name('customer_coupon');

        $start = strtotime(input('param.start'));
        $end = strtotime(input('param.end'));

        $map['end_time'] = ['between', [$start, $end]];

        $key = input('param.key') ? input('param.key') : '';
        if ($key && $key != '') {
            $map['c.real_name|b.Mobile|b.Email|code'] = ['like', '%' . $key . '%'];
        }
 
        $page = input('get.page') ? input('get.page') : 1;
        $rows = input('get.rows'); // 获取总条数

        $list = $db->alias('a')
            ->field('a.*,b.Mobile,c.real_name,d.code,d.type,d.end_time,d.status,d.amount')
            ->join('Customer b', 'customerid=b.Id', 'left')
            ->join('Sys_admin c', 'adminid=c.Id', 'left')
            ->join('Coupon d', 'couponid=d.id', 'left')
            ->where($map)
            ->page($page, $rows)
            ->select();

        $count = $db->alias('a')
            ->join('Customer b', 'customerid=b.Id', 'left')
            ->join('Sys_admin c', 'adminid=c.Id', 'left')
            ->join('Coupon d', 'couponid=d.id', 'left')
            ->where($map)
            ->count();

        $data['list'] = $list;
        $data['count'] = $count;
        $data['page'] = $page;
        if (input('get.page')) {
            return json($data);
        }

        return $this->fetch();
    }

    /**
     * 给某个人发券
     * @return \think\response\Json|unknown
     */
    public function sendcoupon()
    {
        if (request()->isPost()) {

            $data = input('param.');

            if (array_key_exists('end_time', $data)) {
                $data['end_time'] = strtotime($data['end_time']);
            }

            $coupon['name'] = $data['name'];
            $coupon['code'] = $data['code'];
            $coupon['amount'] = $data['amount'];
            $coupon['productid'] = $data['productid'];
            $coupon['type'] = $data['type'];
            $coupon['totalnum'] = $data['totalnum'];
            $coupon['leftnum'] = $data['leftnum'];
            $coupon['status'] = 0;
            $coupon['end_time'] = $data['end_time'];
            $coupon['adminid'] = session('uid');

            $member['customerid'] = $data['customerid'];
            $member['adminid'] = $coupon['adminid'];

            if ($data['msgnotice'] == "1") {
                $sms = new SmsModel($data['mobile']);
                $sms->sendCouponMsg($coupon['amount']);
            }

            $cm = new CouponModel();
            return json($cm->sendCustomerCounpon($coupon, $member));

        }

        return $this->fetch();
    }

    public function valicoupon()
    {
        $db = Db::name('Customercoupon');
        $data = input('param.');
        if (array_key_exists('couponid', $data) && $data['couponid'] > 0) {
            Db::name('Coupon')->where('id', $data['couponid'])->setField(['status' => 1, 'used_time' => time(), 'leftnum' => 0]);
        }
        return parent::singleDataSave($db, $data, '优惠券');
    }
}