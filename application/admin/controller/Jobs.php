<?php
namespace app\admin\controller;

use think\Db;
use function Qiniu\json_decode;

class Jobs extends Base{

    public function index()
    {
        $db = Db::name('jobs');
    
        $jobs = $db->where('reserved_at is null')->count();
    
        $this->assign('jobs',$jobs);
    
        return parent::vueQuerySingle($db);
    }
    
    public function license(){

        $db = Db::name('jobs');
        
        $key=input('get.key')?input('get.key'):'';
        
        $map=[];
        
        if($key&&$key!=''){
            $map['payload']=['like','%'.$key.'%'];
        }
                
        $Nowpage = input('get.page') ? input('get.page'):1;
        
        $limits =  input('get.rows') ? input('get.rows'):config('list_rows'); // 获取总条数
        
        $list = $db->where($map)
            ->page($Nowpage, $limits)
            ->order('id desc')
            ->select();
        
        $count = $db->where($map)->count();
        
        $newlist=[];
        
        foreach ($list as $l){
            $n=json_decode($l['payload'],true)['data'];
            $n['id']=$l['id'];
            $n['created_at']=$l['created_at'];
            $n['reserved_at']=$l['reserved_at'];
            array_push($newlist, $n);
        }
        
        $data['list']=$newlist;
        $data['count']=$count;
        $data['page']=$Nowpage;
        
        if(input('get.page'))
        {
            return json($data);
        }

        $jobs = $db->where('reserved_at is null')->count();
        
        $this->assign('jobs',$jobs);
        
        return $this->fetch();
    }
}