<?php
namespace app\admin\controller;

use think\Db;

class Sysfaq extends Base
{

    public function index()
    {
        $db = Db::name('Sys_faq');
        return parent::vueQuerySingle($db);
    }

    public function save()
    {
        $db = Db::name('Sys_faq');
        
        $data = input('param.');
        $username=session('username');
        
        if(array_key_exists('id', $data)&& $data['id']==''){
            $data['createdby']=session('username');
            $data['create_time']=time();
        }
        else if(array_key_exists('id', $data)&& $data['id']>0){
            $data['solutedby']=session('username');
            $data['update_time']=time();
        }
        
        return parent::singleDataSave($db, $data);
    }

    public function detail()
    {
        $id = input('id');
        
        $db = Db::name('Sys_faq');
        
        $data = $db->where('id', $id)->find();
        
        $this->assign('data' , $data);
        
        return $this->fetch();
    }
}

?>