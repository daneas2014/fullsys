<?php
namespace app\admin\controller;

use app\admin\model\ReportModel;
use app\common\model\SupplierBase;
use think\Db;

class Report extends Base
{

    public function editors()
    {
        $key = input('key');
        $map = [];
        if ($key && $key !== "") {
            $map['title'] = ['like', "%" . $key . "%"];
        }

        $db = Db::name('report_worklog');

        $page = input('get.page') ? input('get.page') : 1;
        $rows = input('get.rows'); // 获取总条数
        $count = $db->where($map)->count(); // 计算总页面
        $lists = $db->field('a.*,b.real_name')->alias('a')
            ->join('sys_admin b', 'a.adminid=b.id')
            ->where($map)
            ->page($page, $rows)
            ->order('id desc')
            ->select();

        $data['list'] = $lists;
        $data['count'] = $count;
        $data['page'] = $page;
        if (input('get.page') > 0) {
            return json($data);
        }

        $typs = config('worklog_type');
        $this->assign('types', json_encode($typs));
        return $this->fetch();
    }

    /**
     * 将report_pvsource和worklog相结合，做个报表出来，post
     */
    public function pv()
    {

        $start = input('start') ? strtotime(input('start')) : strtotime('-2 month');
        $end = input('start') ? strtotime(input('end')) + 3600 * 24 : time();
        $type = input('type');
        $supplierid = input('supplierid');
        $lastcreatetime = file_get_contents('./initpvcrunms.txt');

        $im = new ReportModel();

        // 工作量
        $name = 'workanly-' . $start . '-' . $end . '-' . $type . '-' . $supplierid;
        $data = dcache(CACHE_ADMIN, $name);
        if ($data == null) {
            $data['pv'] = $im->adminPv($start, $end, $type);
            $data['su'] = $im->supplierPv($start, $end, $type, $supplierid);
            $data['w'] = $im->todayWork();
            $data['m'] = $im->monthWork();

            $supplierspv = $im->alias('a')
                ->join('supplier b', 'a.supplierid=b.id', 'right')
                ->group('supplierid,name,website')
                ->order('pv desc')
                ->field('supplierid,SUM(pv)as pv,name,website')
                ->select();

            $data['spv'] = $supplierspv;

            dcache(CACHE_ADMIN, $name, $data);
        }

        $this->assign('pv', $data['pv']);
        $this->assign('supplier', $data['su']);
        $this->assign('w', $data['w']);
        $this->assign('m', $data['m']);
        $this->assign('spv', $data['spv']);
        $this->assign('lastdate', $lastcreatetime);

        return $this->fetch();
    }

    /**
     *  当日的工作结果
     * @return \think\response\Json|unknown
     */
    public function pvsource()
    {
        $key = input('key');
        $starttime = strtotime(input('start'));
        $endtime = strtotime(input('end'));
        $map['create_time'] = ['between', [$starttime, $endtime]];

        if ($key && $key !== "") {
            if ($key > 0) {
                $map['dataid'] = $key;
            } else {
                $map['datacode'] = ['like', $key . '%'];
            }
        }

        $db = Db::name('report_pvsource');

        return parent::vueQuerySingle($db, $map);
    }

    /**
     * 工作分析
     */
    public function workanly()
    {
        $starttime = input('start') ? input('start') : date('Y-m-d 00:00:00');
        $endtime = input('end') ? input('end') : date('Y-m-d 23:59:59');
        $adminid = input('adminid') ? input('adminid') : 0;

        if (input(('start'))) {

            $conda['a.status'] = 1;
            $conda['b.create_time'] = ['between', [strtotime($starttime), strtotime($endtime)]];
            if ($adminid > 0) {
                $conda['adminid'] = $adminid;
            }

            $db = Db::name('sys_admin');

            $article = $db->alias('a')
                ->field("b.id,b.title,b.create_time,'文章' as tt,adminid,real_name")
                ->join('article b', 'a.id=b.adminid', 'left')
                ->where($conda)
                ->select();

            $download = $db->alias('a')
                ->field("b.id,b.name as title,b.create_time,'试用' as tt,adminid,real_name")
                ->join('product_download b', 'a.id=b.adminid', 'left')
                ->where($conda)
                ->select();

            $video = $db->alias('a')
                ->field("b.id,b.title,'视频' as tt,b.create_time,adminid,real_name")
                ->join('video b', 'a.id=b.adminid', 'left')
                ->where($conda)
                ->select();

            $list = array_merge($article, $download, $video);

            $data['list'] = $list;
            $data['count'] = count($list);

            return json($data);
        }

        $admins = Db::name('sys_admin')->where(['groupid' => 2, 'status' => 1])->select();
        $this->assign('admins', $admins);
        $this->assign('adminid', $adminid);
        return $this->fetch();
    }

    /**
     * 生成时间周期内的报表
     */
    public function initpvcrunms()
    {
        if (request()->isPost()) {

            $lastcreatetime = file_get_contents('./initpvcrunms.txt');
            $now = time();

            if ($now < $lastcreatetime + 3600 * 24 * 5) {
                return json(['code' => 1, 'data' => '', 'msg' => '本次刷新失败，5天为最小统计周期']);
            }

            // -- 1. 产品详情页统计
            $sql1 = "insert into report_pvcrunms(adminid,supplierid,productid,datatype,dataid,create_time,pv)
select adminid,supplierid,dataid,'product',dataid,$now as tag,pv from (select a.adminid,supplierid,b.id from supplier a right join product b on a.id=b.supplierid where a.adminid>0) c
inner join (select dataid,SUM(contribut)as pv from report_pvsource where type='product' and create_time between $now and $lastcreatetime group by dataid )d on c.id=d.dataid";

            // --2. 商城关注度
            $sql2 = "insert into report_pvcrunms(adminid,supplierid,productid,datatype,dataid,create_time,pv)
select adminid,supplierid,dataid,'mall',dataid,$now as tag,pv from (select a.adminid,supplierid,b.id from supplier a right join product b on a.id=b.supplierid where a.adminid>0) c
inner join (select dataid,SUM(contribut)as pv from report_pvsource where type='mall' and create_time between $now and $lastcreatetime group by dataid )d on c.id=d.dataid";

            // --3. 文章和供应商对应
            $sql3 = "insert into report_pvcrunms(adminid,supplierid,productid,datatype,dataid,create_time,pv)
select adminid,supplierid,0,'article',dataid,$now as tag,pv from article a
inner join (select dataid,SUM(contribut)as pv from report_pvsource where type='article' and create_time between $now and $lastcreatetime group by dataid )b on a.id=b.dataid
left join(select supplierid,articleid from article_relations group by articleid,supplierid)c on c.articleid=b.dataid";

            // --4. 下载报表
            $sql4 = "insert into report_pvcrunms(adminid,supplierid,productid,datatype,dataid,create_time,pv)
select a.adminid ,supplierid,productid,'download',dataid,$now as tag,pv from product_download a
inner join (select dataid,SUM(contribut)as pv from report_pvsource where type='download' and create_time between $now and $lastcreatetime group by dataid )b on a.id=b.dataid
left join product c on c.id=a.productid";


            // --6. 视频报表
            $sql6 = "insert into report_pvcrunms(adminid,supplierid,productid,datatype,dataid,create_time,pv)
select a.adminid,supplierid,productid,'video',dataid,$now as tag,pv from video a
inner join (select dataid,SUM(contribut)as pv from report_pvsource where type='video' and create_time between $now and $lastcreatetime group by dataid )b on a.id=b.dataid
left join product c on c.id=a.productid";

            // 7.解决人员归属的问题
            $sql7 = "update report_pvcrunms inner join supplier on report_pvcrunms.supplierid=supplier.id set report_pvcrunms.adminid = supplier.adminid";

            // 8.解决数据表中 有文章id，没有supplierid和productid的情况下，通过文章和产品的关联，解决汇总表产品id的问题
            $sql8 = " update report_pvcrunms inner join
                    (select a.productid from report_pvcrunms a
                        inner join article_relations b on a.supplierid=b.supplierid
                        where a.productid=0
						and a.datatype='article'
						and a.dataid=b.articleid
						and b.productid>0
						and a.create_time > $now) c
                    set report_pvcrunms.productid=c.productid";

            Db::startTrans();

            try {
                Db::execute($sql1);
                Db::execute($sql2);
                Db::execute($sql3);
                Db::execute($sql4);
                Db::execute($sql6);
                Db::execute($sql7);
                Db::execute($sql8);
                // 提交事务
                Db::commit();

                file_put_contents('./initpvcrunms.txt', $now);

                return json(['code' => 1, 'data' => '', 'msg' => '数据已更新至' . date('Y-m-d H:i:s', $now)]);

            } catch (\Exception $e) {
                // 回滚事务
                Db::rollback();

                return json(['code' => -1, 'data' => '', 'msg' => '数据已更新失败：' . $e->getMessage()]);
            }
        }
    }

    public function videohistory()
    {
        $key = input('key');
        $starttime = strtotime(input('start'));
        $endtime = strtotime(input('end'));
        $map['a.create_time'] = ['between', [$starttime, $endtime]];

        if ($key && $key > 0) {
            $map['a.supplierid'] = $key;
        }

        $page = input('get.page') ? input('get.page') : 1;
        $rows = input('get.rows'); // 获取总条数

        $list = Db::name('video_history')->alias('a')
            ->join('product b', 'a.productid=b.id', 'left')
            ->join('supplier c', 'a.supplierid=c.id', 'left')
            ->join('customer d', 'a.customerid=d.id', 'left')
            ->field('a.*,b.name as product,c.name as supplier,d.nickname,d.mobile,d.email')
            ->where($map)
            ->page($page, $rows)
            ->select();

        $count = Db::name('video_history')->alias('a')
            ->where($map)
            ->count();

        $data['list'] = $list;
        $data['count'] = $count;
        $data['page'] = $page;
        if (input('get.page')) {
            return json($data);
        }

        $supplier = new SupplierBase();
        $suppliers = $supplier->getSuppliersOnlyName();
        $this->assign('sup', $suppliers);

        return $this->fetch();
    }

    public function pvproduct()
    {
        $start = input('start') ? strtotime(input('start')) : strtotime('-1 month');
        $end = input('start') ? strtotime(input('end')) + 3600 * 24 : time();
        $type = input('type');
        $supplierid = input('supplierid') ? input('supplierid') : 146;

        $db = Db::name('report_pvcrunms');

        // 3. 供应商清单
        $supplier = new SupplierBase();
        $suppliers = $supplier->getSuppliersOnlyName();

        $productlist = $db->alias('a')
            ->field('productid,sum(pv) as total,name')
            ->join('product b', 'a.productid=b.id', 'right')
            ->where('a.supplierid', $supplierid)
            ->group('productid,name')
            ->limit(10)
            ->order('total desc')
            ->select();

        $name = 'PVPRODUCT' . $start . $end . $type . $supplierid;

        $report = dcache(CACHE_ADMIN, $name);

        if (!$report) {

            // 1. 饼状图
            $cicle = $db->alias('a')
                ->field('sum(pv) as total,datatype')
                ->where(['supplierid' => $supplierid])
                ->group('datatype')
                ->order('total desc')
                ->select();

            // 2. 产品堆叠图
            $productpvs = Db::query('select datatype,sum(pv) total,productid from report_pvcrunms where supplierid=' . $supplierid . ' and productid>0 group by datatype,productid');
            $products = [];
            foreach ($productlist as $l) {
                foreach ($productpvs as $pv) {
                    if ($pv['productid'] == $l['productid']) {
                        $l[$pv['datatype']] = $pv['total'];
                    }
                }

                if (!isset($l['download'])) {
                    $l['download'] = 0;
                }
                if (!isset($l['demo'])) {
                    $l['demo'] = 0;
                }
                if (!isset($l['article'])) {
                    $l['article'] = 0;
                }
                if (!isset($l['product'])) {
                    $l['product'] = 0;
                }
                if (!isset($l['video'])) {
                    $l['video'] = 0;
                }
                if (!isset($l['mall'])) {
                    $l['mall'] = 0;
                }

                array_push($products, $l);
            }

            $report['suppliers'] = $suppliers;
            $report['productlist'] = $productlist;
            $report['products'] = $products;
            $report['cicle'] = $cicle;
            dcache(CACHE_ADMIN, $name, $report);
        }

        $this->assign(['report' => $report,
            'supplierid' => $supplierid,
            'suppliers' => $report['suppliers'],
            'products' => $report['productlist'],
            'cicle' => $report['cicle'],
            'diejia' => $report['products'],
        ]);
        return $this->fetch();
    }

    public function productrank()
    {

        if (input('productid') && input('productid') > 0) {
            $cond['a.productid'] = input('productid');
        } else {
            $cond['a.supplierid'] = input('supplierid');
        }

        $start = strtotime(input('start'));
        $end = strtotime(input('end'));
        $cond['create_time'] = ['between', [$start, $end]];

        $page = input('currentPage');
        $pagesize = input('pageSize');

        $articles = $this->rankType($cond, 'article', $page, $pagesize);
        $article = $articles['list'];
        $video = $this->rankType($cond, 'video', $page, $pagesize);
        $download = $this->rankType($cond, 'download', $page, $pagesize);
        $demo = $this->rankType($cond, 'demo', $page, $pagesize);

        $datalist = [];
        for ($i = 0; $i < $pagesize; $i++) {
            $data['rowid'] = $i + 1;
            if ($article && count($article) > $i) {
                $data['article'] = "<span class='el-button el-button--primary el-button--small'>" . $article[$i]['pv'] . "</span>&nbsp;<a target='_blank' href='/article/" . $article[$i]['id'] . "'>" . $article[$i]['title'] . "</a>";
            }
            if ($video && count($video) > $i) {
                $data['video'] = "<span class='el-button el-button--primary el-button--small'>" . $video[$i]['pv'] . "</span>&nbsp;<a target='_blank' href='/video/" . $video[$i]['id'] . "'>" . $video[$i]['title'] . "</a>";
            }
            if ($download && count($download) > $i) {
                $data['download'] = "<span class='el-button el-button--primary el-button--small'>" . $download[$i]['pv'] . "</span>&nbsp;<a target='_blank' href='/download/" . $download[$i]['id'] . "'>" . $download[$i]['name'] . "</a>";
            }
            if ($demo && count($demo) > $i) {
                $data['demo'] = "<span class='el-button el-button--primary el-button--small'>" . $demo[$i]['pv'] . "</span>&nbsp;<a target='_blank' href='/demo/" . $demo[$i]['id'] . "'>" . $demo[$i]['name'] . "</a>";
            }
            if (count($data) == 1) {
                continue;
            }
            array_push($datalist, $data);
            unset($data);
        }

        $data['list'] = $datalist;
        $data['count'] = $articles['count'];
        if (input('productid')) {
            return json($data);
        }
    }

    /**
     * 类型排序
     **/
    private function rankType($cond, $table, $page = 1, $pagesize = 10)
    {
        $db = Db::name('report_pvcrunms');
        switch ($table) {
            case 'article':
                $cond['datatype'] = 'article';
                $cond['dataid'] = ['>', 0];
                $data['list'] = $db->alias('a')
                    ->field('b.create_time,b.title,b.id,sum(pv) as pv')
                    ->join('article b', 'a.dataid=b.id', 'right')
                    ->where($cond)
                    ->group('b.title,b.id,b.create_time')
                    ->order('pv desc')
                    ->page($page, $pagesize)
                    ->select();

                $data['count'] = $db->alias('a')
                    ->where($cond)
                    ->count();
                break;
            case 'video':
                $cond['datatype'] = 'video';
                $data = $db->alias('a')
                    ->field('b.id,b.title,sum(pv) as pv')
                    ->join('video b', 'a.dataid=b.id', 'right')
                    ->where($cond)
                    ->group('b.id,b.title')
                    ->order('pv desc')
                    ->page($page, $pagesize)
                    ->select();
                break;
            case 'download':
                $cond['datatype'] = 'download';
                $data = $db->alias('a')
                    ->field('b.name,b.id,sum(pv) as pv')
                    ->join('product_download b', 'a.dataid=b.id', 'right')
                    ->where($cond)
                    ->group('b.id,b.name')
                    ->order('pv desc')
                    ->page($page, $pagesize)
                    ->select();
                break;
            case 'demo':
                $cond['datatype'] = 'demo';
                $data = $db->alias('a')
                    ->field('b.name,b.id,sum(pv) as pv')
                    ->join('product_resource b', 'a.dataid=b.id', 'right')
                    ->where($cond)
                    ->group('b.id,b.name')
                    ->order('pv desc')
                    ->page($page, $pagesize)
                    ->select();
                break;
        }
        return $data;
    }

    /**
     * 流量趋势【总的，单个的，连续12个月的】
     */
    public function pvtrend()
    {

        $rows = input('get.rows') ? input('get.rows') : 30;

        $name = 'ADTREND' . $rows;

        $data = dcache(CACHE_ADMIN, $name);

        if (!$data) {

            $t = strtotime(date('Y-1-1'));

            $top30 = Db::query("select name,supplierid,sum(pv) as pv from report_pvcrunms a inner join supplier b on a.supplierid=b.id where create_time >$t group by supplierid,Name limit $rows order by pv desc");

            $supplierids = [];

            $data = [];

            $db = Db::name('report_pvcrunms');

            foreach ($top30 as $tp) {

                $mdata = [];

                for ($i = 12; $i >= 0; $i--) {
                    if ($i == 0) {
                        $last_mont_first_date = date('Y-m-1');
                        $last_mont_end_date = date('Y-m-d');
                    } else {
                        $start = '-' . $i . ' month';
                        $end = '-' . ($i - 1) . ' month';
                        $last_mont_first_date = date('Y-m-1', strtotime($start));
                        $last_mont_end_date = date('Y-m-1', strtotime($end));
                    }

                    $tp['time' . $i] = $last_mont_first_date . '~' . $last_mont_end_date;
                    $month['start'] = strtotime($last_mont_first_date);
                    $month['end'] = strtotime($last_mont_end_date);
                    $map['supplierid'] = $tp['supplierid'];
                    $map['create_time'] = ['between', [$month['start'], $month['end']]];

                    $pv = $db->where($map)->sum('pv');

                    array_push($mdata, $pv);

                    if ($i == 0) {
                        $days = getDaysByMonth(date('Y'), date('m'));
                        $pv = $days * ($pv / date('d'));
                        array_push($mdata, $pv);
                    }
                }

                $tp['tpv'] = $mdata;

                array_push($data, $tp);
            }

            dcache(CACHE_ADMIN, $name, $data, 3600 * 24);
        }

        $this->assign('supplier', $data);
        $this->assign('rows', $rows);

        return $this->fetch();
    }
}
