<?php
namespace app\admin\controller;

use think\Cache;
use think\Db;

class Cachemanage extends Base
{

    public function index()
    {
        if (request()->isPost()) {
            $type = input('t');
            switch ($type) {
                case 'admin':
                    Cache::clear(CACHE_ADMIN);
                    $msg = '后台';
                    break;
                case "seo":
                    Cache::clear('PAGESEO');
                case 'API':
                    Cache::clear(CACHE_API);
                    $msg = 'API';
                    break;
                case 'sites':
                    Cache::clear(CACHE_SITES);
                    $msg = '站群';
                    break;
                case 'mall':
                    Cache::clear(CACHE_MALL);
                    $msg = '商城';
                    break;
                case 'article':
                    Cache::clear(CACHE_ARTICLE);
                    $msg = '文章';
                    break;
                case 'commodity':
                    Cache::clear(CACHE_COMMODITY);
                    $msg = '供应商';
                    break;
                case 'ad':
                    Cache::clear(CACHE_AD);
                    $msg = '广告';
                    break;
                case 'package':
                    Cache::clear(CACHE_PACKAGE);
                    $msg = '下载';
                    break;
                case 'lincence':
                    Cache::clear(CACHE_LICENCE);
                    $msg = '授权';
                    break;
                case 'sample':
                    Cache::clear(CACHE_SAMPLE);
                    $msg = '示例';
                    break;
                case 'video':
                    Cache::clear(CACHE_VIDEO);
                    $msg = '视频';
                    break;
                case 'supplier':
                    Cache::clear(CACHE_SUPLERS);
                    $msg = '供应商';
                    break;
                case 'temp':
                    $dir = RUNTIME_PATH . '/temp/';
                    delete_dir_file($dir);
                    $msg = '页面缓存';
                    break;
                case 'log':
                    $dir = RUNTIME_PATH . '/log/';
                    delete_dir_file($dir);
                    $msg = '系统日志';
                    break;
                case 'taoke':
                    Db::query("TRUNCATE TABLE `tk_urls`");
                    $msg="淘宝客";
                    break;
                default:
                    Cache::clear();
                    $msg = '全部';
                    break;
            }

            return json(['code' => 1, 'data' => null, 'msg' => $msg . '缓存已删除']);
        }

        return $this->fetch();
    }
}
