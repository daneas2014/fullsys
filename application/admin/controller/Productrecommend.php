<?php
namespace app\admin\controller;

use think\Db;

class Productrecommend extends Base
{

    public function index()
    {
        $db = Db::name('product_recommend');

        $catalog = Db::name('product_category')->select();

        $this->assign('cates', $catalog);

        return parent::vueQuerySingle($db);
    }

    public function savec()
    {

        $db = Db::name('product_recommend');

        if (request()->isPost()) {

            $data = input('post.');

            if (array_key_exists('file', $data)) {
                unset($data['file']);
            }

            $data['create_time'] = time();
            $data['update_time'] = 0;

            $db->insertGetId($data);

            return json(['code' => 1, 'data' => '', 'msg' => '保存完毕']);
        }

        $id = input('param.id');

        if ($id > 0) {
            $db->where('id', $id)->delete();

            return json(['code' => 1, 'data' => '', 'msg' => '']);
        }

        $catalog = Db::name('product_category')->select();

        $this->assign('cates', $catalog);

        return $this->fetch();
    }

    public function graps()
    {
        if (request()->isPost()) {
            $id = input('param.dataid');
            $type = input('param.datatype');

            if ($type == 'article') {
                $item = Db::name('article')->where('Id', $id)->find();
                $data = ['title' => $item['title'], 'keywords' => $item['Keywords'], 'description' => $item['Summary'], 'imagepath' => $item['ImagePath'], 'url' => '/article/' . $id];
            }
            if ($type == 'product') {
                $item = Db::name('product')->where('Id', $id)->find();
                $data = ['title' => $item['name'], 'keywords' => $item['Name'], 'description' => $item['DescriptionWord'], 'imagepath' => $item['ImagePath'], 'url' => "/product/" . $id];
            }
            if ($type == 'supplier') {
                $item = Db::name('supplier')->where('Id', $id)->find();
                $data = ['title' => $item['name'], 'keywords' => $item['Name'], 'description' => $item['SEOTitle'], 'imagepath' => $item['LogoPath'], 'url' => '/supplier/' . $id];
            }
            if ($type == 'demo') {
                $item = Db::name('product_download')->where('Id', $id)->find();
                $data = ['title' => $item['name'], 'keywords' => $item['keywords'], 'description' => $item['description'], 'imagepath' => $item['imagepath'], 'url' => "/resorce/detail-demo-" . $id];
            }
            if ($type == 'video') {
                $item = Db::name('video')->where('Id', $id)->find();
                $data = ['title' => $item['title'], 'keywords' => $item['Title'], 'description' => $item['Title'], 'imagepath' => $item['Image'], 'url' => "/video/" . $id];
            }

            return json(['code' => 1, 'data' => $data, 'msg' => '']);
        }
    }
}
