<?php

namespace app\admin\controller;

use app\admin\model\ReportModel;
use think\Cache;
use think\Config;
use think\Db;

class Index extends Base
{

    public function index()
    {
        return $this->fetch('/index');
    }

    /**
     * [indexPage 后台首页]
     * @return [type] [description]
     */
    public function indexPage()
    {
        $name = 'workanly';
        $assign = dcache(CACHE_ADMIN, $name);

        if ($assign == null) {

            $im = new ReportModel();

            $as = Db::name('article')->order('id desc')->limit(8)->field('id,title')->select();
            $bbs = Db::name('bbs_thread')->order('id desc')->limit(8)->field('id,title')->select();

            $info = array(
                'web_server' => $_SERVER['SERVER_SOFTWARE'],
                'onload' => ini_get('upload_max_filesize'),
                'think_v' => THINK_VERSION,
                'phpversion' => phpversion(),
            );

            $assign = ['w' => $im->todayWork(), 'm' => $im->monthWork(), 'info' => $info, 'as' => $as, 'bbs' => $bbs];

            dcache(CACHE_ADMIN, $name, $assign);
        }

        return $this->view->assign($assign)->fetch('index');
    }

    /**
     * [userEdit 修改密码]
     * @return [type] [description]
     */
    public function editpwd()
    {
        if (request()->isAjax()) {
            $param = input('post.');
            $user = Db::name('sys_admin')->where('id=' . session('uid'))->find();
            if (md5(md5($param['old_password']) . config('auth_key')) != $user['password']) {
                return json(['code' => -1, 'url' => '', 'msg' => '旧密码错误']);
            } else {
                $pwd['password'] = md5(md5($param['password']) . config('auth_key'));
                Db::name('sys_admin')->where('id=' . $user['id'])->update($pwd);
                session(null);
                cache('db_config_data', null); //清除缓存中网站配置信息
                return json(['code' => 1, 'url' => 'index/index', 'msg' => '密码修改成功']);
            }
        }
        return $this->fetch();
    }

    /**
     * 清除缓存
     */
    public function clear()
    {
        Cache::clear();
        return json(['code' => 1, 'msg' => '清除缓存成功']);

        if (delete_dir_file(CACHE_PATH) && delete_dir_file(TEMP_PATH)) {
            return json(['code' => 1, 'msg' => '清除缓存成功']);
        } else {
            return json(['code' => 0, 'msg' => '清除缓存失败']);
        }
    }

    public function mailtest()
    {

        if (request()->isPost()) {
            $sendto = input('sendto');
            $body = input('body');

            $result = sendMail($sendto, '系统邮件测试', $body);

            if ($result) {
                return json(['code' => 1, 'data' => '', 'msg' => '已经发送测试']);
            }

            return json(['code' => -1, 'data' => '', 'msg' => '已经发送测试，但是失败了']);
        }

        return $this->fetch();
    }

    public function smstest()
    {
        $sms = new SmsModel('18996069665');
        return json($sms->sendOrderpaid('producttes'));
    }

    // 推送当天链接给百度
    public function pushtobaidu()
    {
        if (request()->isPost()) {
            $urls = input('urls');

            $res = $this->pushurls($urls);

            return json($res);
        }

        return $this->fetch();
    }

    public function pushurls($urls = null)
    {

        $array = array();

        $map['create_time'] = ['>', strtotime(date('Y-m-d 00:00:00'))];

        // 文章链接
        $db = Db::name('article');
        $list = $db->field('id')
            ->where($map)
            ->select();

        foreach ($list as $arr) {
            array_push($array, SITE_DOMAIN . '/article/' . $arr['id']);
        }

        // 文章分类链接
        $db2 = Db::name('video');
        $list2 = $db2->field('id')
            ->where($map)
            ->select();
        foreach ($list2 as $arr) {
            array_push($array, SITE_DOMAIN . '/video/' . $arr['id']);
        }

        // 便民服务推荐
        $db3 = Db::name('product_download');
        $list3 = $db3->field('id')
            ->where($map)
            ->select();
        foreach ($list3 as $arr) {
            array_push($array, SITE_DOMAIN . '/download/' . $arr['id']);
        }

        return pushbaidu($array);
    }
}
