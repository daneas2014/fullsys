<?php
namespace app\admin\controller;

use think\Db;

class Sysseo extends Base
{

    public function index()
    {
        $db = Db::name('sys_seo');
        
        return parent::vueQuerySingle($db);
    }

    public function saveseo()
    {
        $db = Db::name('sys_seo');
        $data = input('param.');
        return parent::singleDataSave($db, $data);
    }
}