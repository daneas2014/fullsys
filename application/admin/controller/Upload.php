<?php
namespace app\admin\controller;

use think\Controller;
use think\File;
use think\Request;
use app\api\controller\UploadApi;

class Upload extends UploadApi
{

    // 图片上传
    public function upload()
    {
        $act = input('param.act');
        
        $file = request()->file('file');
                
        $dir = ROOT_PATH . 'web' . DS . 'attachment/images'; // 上传到目录        
        
        $info = $file->move($dir);
        
        if ($info) {
            
            $filename = $info->getSaveName();
            
            unset($info);
            
            switch ($act) {
                case 'cloud':
                    $filepath = '/attachment/images/' . $filename;
                    $res = parent::uploadtoqiniu($filepath, str_replace("\\", "/", $filename));
                    echo $res;
                    break;                   
                default:
                    echo $filename;
                    break;
            }
        } else {
            echo $file->getError();
        }
    }

    // 会员头像上传
    public function uploadface()
    {
        $file = request()->file('file');
        $info = $file->move(ROOT_PATH . 'public' . DS . 'Content/face');
        if ($info) {
            echo $info->getSaveName();
        } else {
            echo $file->getError();
        }
    }
}
