<?php
namespace app\admin\controller;

use app\common\model\SupplierBase;
use app\common\model\TagsBase;
use think\Db;

class Product extends Base {

	/**
	 * 产品清单
	 */
	public function products() {
		$db = Db::name('product');
		$dbc = Db::name('product_category');

		$key = input('key');
		$type = input('type');
		$supplierid = input('supplierid');

		$map = [];
		if ($key && $key !== "") {
			if ($key > 0) {
				$map['id'] = $key;
			} else {
				$map['name'] = ['like', "%" . trim($key) . "%"];
			}
		}
		if ($type && $type !== "") {
			$map['categoryid'] = $type;
		}

		// 注入：如果登录的账号权限是8，带supplierid，那么就要强绑定supplierid
		if ($supplierid && $supplierid != '') {
			$map['supplierid'] = $supplierid;
		}
		if (session('groupid', '', 'admin') == 8) {
			$map['supplierid'] = session('supplierid', '', 'admin');
		}

		$cates = $dbc->select();
		$assign = ['cates' => $cates, 'catestr' => json_encode($cates), 'supplierid' => $supplierid, 'key' => $key];

		return parent::vueQuerySingle($db, $map, 'id desc', $assign, 'id,name,categoryid,status,status,headlinetime,imagepath,create_time');
	}

	/**
	 * 保存产品
	 */
	public function saveproduct() {
		$db = Db::name('product');
		$dbc = Db::name('product_category');
		$id = input('id');
		$supplier = new SupplierBase();
		$tm = new TagsBase();
		$assign = ['sus' => $supplier->getSuppliersOnlyName(), 'cates' => $dbc->select(), 'deltags' => $tm->getObjTagsDetail($id, 1)];

		return parent::singleDataSave($db, input('param.'), '产品', $assign);
	}

	/**
	 * 分类清单
	 */
	public function catagories() {
		$db = Db::name('product_category');

		return parent::vueQuerySingle($db);
	}

	/**
	 * 保存分类
	 */
	public function savecatagory() {
		$db = Db::name('product_category');

		$data = input('param.');

		$parents = $db->where('parentid', 0)->select();

		return parent::singleDataSave($db, $data, '分类', ['parents' => $parents]);
	}

	/**
	 * 供应商
	 */
	public function suppliers() {
		$db = Db::name('supplier');

		$key = input('key');

		$enableadd = true;

		$map = [];
		if ($key && $key !== "") {
			$map['name|title|description'] = ['like', "%" . $key . "%"];
		}
		$adminid = input('adminid');
		if ($adminid && $adminid > 0) {
			$map['adminid'] = $adminid;
		}

		// 注入：如果登录的账号权限是8，带supplierid，那么就要强绑定supplierid
		if (session('groupid', '', 'admin') == 8) {
			$map['id'] = session('supplierid', '', 'admin');
			$enableadd = false;
		}

		$assign = [
			'admins' => Db::name('sys_admin')->where('status', 1)->select(),
			'adminid' => session('uid', '', 'admin'),
			'enableadd' => $enableadd,
		];

		return parent::vueQuerySingle($db, $map, 'id desc', $assign, 'id,name,logopath,status,description,website,adminid');
	}

	/**
	 * 保存供应商
	 */
	public function savesupplier() {
		$db = Db::name('supplier');

		$id = input('get.id');

		$tm = new TagsBase();

		$users = Db::name('sys_admin')->field('id,real_name')->where('status', 1)->select();

		$assign = ['admins' => $users, 'deltags' => $tm->getObjTagsDetail($id, 3)];

		return parent::singleDataSave($db, input('param.'), '合作伙伴', $assign);
	}

	/**
	 * 产品价格，admin可以全局查询，groupid=8的只能查询自己的，所以在query的时候需要区别
	 *
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2024-02-01
	 */
	public function pricelist() {

		// 终端用户查询的时候需要聚合
		if (session('groupid', '', 'admin') == 8) {

			$db = Db::name('product_price');

			$map = [];
			$id = input('param.productid') ? input('param.productid') : 0;
			if ($id > 0) {
				$map['productid'] = $id;
			}

			$key = input('param.key');
			if ($key && $key != '') {
				$map['name|description'] = ['like', '%' . $key . '%'];
			}

			$parentid = input('get.parentid') ? input('get.parentid') : 0;
			$this->assign('parentid', $parentid);

			$Nowpage = input('get.page') ? input('get.page') : 1;
			$limits = input('get.rows') ? input('get.rows') : config('list_rows'); // 获取总条数

			$list = $db->where($map)
				->where('productid', 'IN', function ($query) {
					$query->table('product')->where('supplierid', session('supplierid', '', 'admin'))->field('id');
				})
				->page($Nowpage, $limits)
				->order('id desc')
				->paginate($limits);

			$data['list'] = $list->items();
			$data['count'] = $list->total();
			$data['page'] = $Nowpage;

			if (input('get.page')) {
				return json($data);
			}

			return $this->view->assign('productid', $id)->fetch('mypricelist');

		} else {
			$db = Db::name('product_price');

			$map = [];

			$id = input('param.productid');
			if ($id && $id > 0) {
				$map['productid'] = $id;
			} else {
				$id = 0;
			}

			$key = input('param.key');
			if ($key && $key != '') {
				$map['name|description'] = ['like', '%' . $key . '%'];
			}

			return parent::vueQuerySingle($db, $map, 'id desc', ['productid' => $id]);
		}
	}

	public function saveprice() {

		$db = Db::name('product_price');

		$data = input('param.');

		$data['adminid'] = session('uid');

		$this->assign('productid', isset($data['productid']) ? $data['productid'] : '');

		return parent::singleDataSave($db, $data, '自定义产品授权');
	}

	/**
	 * 预览图
	 *
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2020-02-28
	 */
	public function productatlas() {
		$db = Db::name('product_images');

		$map['productid'] = input('productid');

		return parent::vueQuerySingle($db, $map, 'id desc', ['productid' => $map['productid']]);
	}

	public function atlassave() {

		$db = Db::name('product_images');

		$data = input('param.');

		return parent::singleDataSave($db, $data);
	}
}
