<?php
namespace app\admin\controller;

use think\Db;
use app\wechat\model\ServiceMinprogram;

/**
 * 小程序订阅者消息推送
 *
 * @author dmakecn@163.com
 * @since 2022-05-17
 */
class Subscribe extends Base
{

    /**
     * 推送主题们
     */
    public function index()
    {
        $db=Db::name('op_subscribe_subject');
        
        $map=[];
        
        if(input('param.key')){
            $map['title|description']=['like','%'.input('param.key').'%'];
        }
        
        return parent::vueQuerySingle($db,$map);
    }

    /**
     * 推送到个人的信息清单
     */
    public function msgs()
    {
        $db=Db::name('op_subscribe_msg');
        
        $map['subjectid']=input('param.subjectid');
        
        $key=input('param.key')?input('param.key'):'';
        
        if($key && $key!=''){
            $map['title|description']=['like','%'.$key.'%'];
        }
        
        return parent::vueQuerySingle($db,$map);
    }

    /**
     * 触发推动到微信通知
     */
    public function sendmsgs()
    {
        $db=Db::name('op_subscribe_msg');
        $map=['issend'=>0,'isview'=>0,'subjectid'=>input('param.subjectid')];
        
        $list = $db->alias('a')
            ->join('Customer b', 'a.customerid=b.Id', 'inner')
            ->where($map)
            ->limit(500)
            ->field('a.*,b.Nickname')
            ->select();
        
        if(count($list)==0){
            return json(['code'=>1,'data'=>'0','msg'=>'无可推送消息']);
        }
        
        $sendlist=[];
        foreach ($list as $l){
            $temp['id']=$l['id'];
            $temp['openid']=$l['openid'];
            $temp['datatype']=$l['datatype'];
            $temp['dataid']=$l['dataid'];
            $temp['username']=$l['Nickname'];
            $temp['title']=$l['title'];
            $temp['description']=$l['description'];
            $temp['remark']='您的订阅已更新，请尽快查看！';
            
            array_push($sendlist, $temp);
            unset($temp);
        }
        
        $mm=new ServiceMinprogram();
        
        $i=$mm->sendSubscribe($sendlist);        

        return json(['code'=>1,'data'=>'0','msg'=>'推送消息共'.$i.'条']);
    }

    /**
     * 把个人额推送主题相绑定
     */
    public function msgtomembers()
    {
        if (request()->isPost()) {
            $data = input('param.');
            
            $datatype = $data['datatype'];
            $dataid = $data['dataid'];
            
            $msgdatatype = $data['msgdatatype'];
            $msgdataid = $data['msgdataid'];
            unset($data['msgdatatype']);
            unset($data['msgdataid']);
            
            // 1. 找到需要推送的人
            $users=Db::name('op_subscribe')->alias('a')
                ->join('Customeropenidoauth b','a.customerid=b.CustomerId','inner')
                ->where(['datatype'=>$datatype,'dataid'=>$dataid,'OAuthType'=>2])
                ->field('a.customerid,b.OAuthOpenId as openid,a.form_id')
                ->select();
            
                
            $data['members']=count($users);
                
                // 2. 把信息存入他们的个人信息库里面去
            $subjectid = Db::name('op_subscribe_subject')->insertGetId($data);
            $list = [];
            foreach ($users as $u) {
                $u['subjectid'] = $subjectid;
                $u['datatype'] = $msgdatatype;
                $u['dataid'] = $msgdataid;
                $u['title'] = $data['title'];
                $u['description'] = $data['description'];
                $u['detail'] = $data['detail'];
                $u['create_time'] = time();
                unset($u['ROW_NUMBER']);
                array_push($list, $u);
            }            
            
            $res=Db::name('op_subscribe_msg')->insertAll($list);
            
            return json(['code'=>1,'data'=>'','msg'=>'订阅主题和消息队列已完成']);
        }
        
        return $this->fetch();
    }
    
    /**
     * 主题要删，消息也要删
     * @return \think\response\Json
     */
    public function delsubject(){

        if (request()->isPost()) {            
            $id=input('param.id');
            Db::name('op_subscribe_subject')->where('id', $id)->delete();
            Db::name('op_subscribe_msg')->where('subjectid', $id)->delete();
            return json(['code'=>1,'data'=>'','msg'=>'推送主题和消息已经删除完毕']);
        }
    }
}