<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: yunwuxin <448901948@qq.com>
// +----------------------------------------------------------------------

// +----------------------------------------------------------------------
// | Addons插件配置 定义了钩子名，通过xxxx插件来实现xxxxxxhook钩子
// +----------------------------------------------------------------------
return [
	'temphook' => 'temp',
	'tbkhook' => 'tbk',
	'testhook' => 'test',
	'doclibhook' => 'doclib',
	'appshook' => 'apps',
	'wehook' => 'wechat',
	'openflathook' => 'openflat',
];
