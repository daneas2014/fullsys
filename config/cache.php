<?php
return [    
    // 驱动方式
    'type'   => 'memcache',
    // 缓存保存目录
    'path'   => CACHE_PATH,
    // 缓存前缀
    'prefix' => '',
    // 缓存有效期 0表示永久缓存
    'expire' => 3200,
    'host' => '127.0.0.1',
    'port' => 11211
];