<?php

return [
    '__pattern__' => ['name' => '\w+'],

    // +----------------------------------------------------------------------
    // | index模块,主要用于导航展示与全站资源导航用
    // +----------------------------------------------------------------------
    'sitemap' => 'index/index/sitemap',
    'about/:type' => ['about/index/index', ['method' => 'get'], ['type' => '\w+']],
    'sinaauth' => 'index/thirdauth/sinaauth',
    'sinacallback' => 'index/thirdauth/sinacallback',
    'qqauth' => 'index/thirdauth/qqauth',
    'qqcallback' => 'index/thirdauth/qqcallback',
    'search' => 'index/search/index',
    'ad' => 'index/ad/ad',


    // +----------------------------------------------------------------------
    // | 文章路由，用于展现所有文章，包含教程等类容
    // +----------------------------------------------------------------------
    'article/:id' => ['archive/article/detail', ['method' => 'get'], ['id' => '\d+']],
    '[articles]' => [
        ':id' => ['archive/article/list', ['method' => 'get'], ['id' => '\d+']],
        ':kw' => ['archive/article/list', ['method' => 'get'], ['kw' => '\w+']],
        '' => 'archive/article/index',
    ],

    // +----------------------------------------------------------------------
    // | 活动通告，每次活动都方里面去，方便
    // +----------------------------------------------------------------------
    '[campaign]' => [
        ':id' => ['index/index/campaign', ['method' => 'get'], ['id' => '\d+']],
        '' => 'index/index/campaigns',
    ],
 
   

    // +----------------------------------------------------------------------
    // | 聚合页
    // +----------------------------------------------------------------------
    '[polymerization]' => [
        '' => 'achive/polymerization/index',
        ':id' => ['achive/polymerization/detail', ['method' => 'get'], ['id' => '\d+']],
    ],

    // +----------------------------------------------------------------------
    // | 厂商路由，用于展现所有产品资料，切忌是资料
    // +----------------------------------------------------------------------
    '[commodity]' => [
        'c/:id' => ['commodity/index/category', ['method' => 'get'], ['id' => '\d+']],
        'v/:id' => ['commodity/index/vendor', ['method' => 'get'], ['id' => '\d+']],
        ':id' => ['commodity/index/detail', ['method' => 'get'], ['id' => '\d+']],
        'list' => 'commodity/index/search',
        '' => 'commodity/index/index',
    ],

    // +----------------------------------------------------------------------
    // | 资源路由，用于资源的导航、分类和细节展示
    // +----------------------------------------------------------------------
    '[package]' => [
        'detail/:type/:id' => ['package/index/detail', ['method' => 'get'], ['type' => '\w+', 'id' => '\d+']],
        'downloadfile' => 'package/index/downloadfile',
        'checkout/vip' => 'package/checkout/vip',
        '' => 'package/index/index',
    ],

    // +----------------------------------------------------------------------
    // | 优酷那种单集普通视频
    // +----------------------------------------------------------------------
    '[video]' => [
        ':type/:id' => ['media/video/list', ['method' => 'get'], ['type' => '\w+', 'id' => '\d+']],
        ':id' => ['media/video/detail', ['method' => 'get'], ['id' => '\d+']], //301跳转
        'list' => 'media/video/list',
        '' => 'media/video/index',
    ],

    // +----------------------------------------------------------------------
    // | 在线课程，慕课网那种收付费也可以
    // +----------------------------------------------------------------------
    '[series]' => [
        'list/:id' => ['media/series/list', ['method' => 'get'], ['id' => '\d+']],
        'play/:id' => ['media/series/video', ['method' => 'get'], ['id' => '\d+']],
        'checkout/vippay' => 'media/checkout/vippay',
        '' => 'media/series/index',
    ],

    // +----------------------------------------------------------------------
    // | 商城
    // +----------------------------------------------------------------------
    '[mall]' => [
        'list/:cateid' => ['mall/index/list', ['method' => 'get'], ['cateid' => '\d+']],
        'list/:cateid?key=:kw' => ['mall/index/list', ['method' => 'get'], ['cateid' => '\d+', 'kw' => '\w+']],
        'list' => 'mall/index/list',
        'd/:id' => ['mall/index/detail', ['method' => 'get'], ['id' => '\d+']], //有规则的列表
        'checkout/index' => 'mall/checkout/index', //产品支付
        '' => 'mall/index/index',
    ],


    // +----------------------------------------------------------------------
    // | 统一支付接口
    // +----------------------------------------------------------------------
    '[pay]' => [
        'payorder' => 'pay/index/payorder',
        'wenotify' => 'pay/index/wenotify',
        'alinotify' => 'pay/index/alinotify',
        'alireturn' => 'pay/index/alireturn',
    ],
 
    // +----------------------------------------------------------------------
    // | 论坛
    // +----------------------------------------------------------------------
    '[forum]' => [
        'u/:uid' => ['forum/user/detail', ['method' => 'get'], ['uid' => '\d+']],
        'thread/:tid' => ['forum/index/thread', ['method' => 'get'], ['tid' => '\d+']],
        'index/newthread' => ['forum/index/newthread'],
        'threadpost' => 'forum/index/threadpost',
        'newthread' => 'forum/index/newthread',
        'threads' => 'forum/index/threads',
        'groups' => 'forum/index/groups',
        'upload/canvas' => 'forum/upload/canvas',
        'upload/thread' => 'forum/upload/thread',
        ':fid' => ['forum/index/forum', ['method' => 'get'], ['fid' => '\d+']],
        '' => 'forum/index/index',
    ],
];
