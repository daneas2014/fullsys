<?php

return [            
    //模板参数替换
    'view_replace_str' => array(
        '__CSS__' => '/static/admin/css',
        '__JS__'  => '/static/admin/js',
        '__IMG__' => '/static/admin/images',
        '__ECHARTS__' => '/static/admin/echarts-2.2.7',	
    ),
];
