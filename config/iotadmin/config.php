<?php
return
	[
	// 应用调试模式
	'app_debug' 	=> true,
	// 应用Trace
	'app_trace' 	=> false,
	// 应用模式状态
	'project_id' 	=> '0',
	'project_name' 	=> '安全生产风险预警平台',
	'project_mult' 	=> true, // 是否启用多项目联网，false代表局域网部署，隐藏所有项目选项
	'project_map' 	=> true, //是否启用地图，false代表不启用，直接隐蔽
	//模板参数替换
	'view_replace_str' 	=> array(
		'__CSS__'		=> '/static/admin/css',
		'__JS__'		=> '/static/admin/js',
		'__IMG__'		=> '/static/admin/images',
		'__ECHARTS__'	=> '/static/admin/echarts-2.2.7',
	),
	'database' => [
		// 数据库类型
		'type' 		=> 'mysql',
		// 服务器地址
		'hostname' 	=> '124.220.210.81',
		// 数据库名
		'database' 	=> 'iot_v21',
		// 用户名
		'username' 	=> 'root',
		// 密码
		'password' 	=> 'Zhangxiaojun2023',
		// 端口
		'hostport'	=> '3306',
		// 数据库调试模式
		'debug'           => true,
	],

	'tdengine'=>[
		'host' 		=> '124.220.210.81',
		// 'hostName'        => 'localhost',
		'port' 		=> 6041,
		'user' 		=> 'root',
		'password' 	=> 'Zhangxiaojun2023',
		'db' 		=> 'iot_v21',
		// 'ssl'             => false,
		// 'timestampFormat' => \Yurun\TDEngine\Constants\TimeStampFormat::LOCAL_STRING, // 此配置 TDengine 3.0 下失效，统一返回格式为：2022-11-25T05:41:04.690Z
		// 'keepAlive'       => true,
		// 'version'         => '3', // TDengine 版本
		// 'timezone'        => 'Asia/Shanghai', // TDengine >= 3.0 时支持，可选参数，指定返回时间的时区 Asia/Shanghai  UTC-8 GMT-8
	]
];
