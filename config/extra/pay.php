<?php

return [
    'ali' => [
        'app_id' => "2018021202184246",
        'notify_url' => SITE_DOMAIN . "/pay/alinotify", // 异步通知地址
        'return_url' => SITE_DOMAIN . "/pay/alireturn", // 同步跳转
        'charset' => "UTF-8", // 编码格式
        'sign_type' => "RSA2", // 签名方式 "MD5 RSA RSA2 DSA"
        'gatewayUrl' => "https://openapi.alipay.com/gateway.do", // 支付宝网关
        'merchant_private_key' => "", // 商户私钥
        'alipay_public_key' => "", // 支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm 对应APPID下的支付宝公钥。
    ],
    'alisandbox' => [
        'app_id' => "2016092900622155",
        'notify_url' => SITE_DOMAIN . "/pay/alinotify1", // 异步通知地址
        'return_url' => SITE_DOMAIN . "/pay/alireturn1", // 同步跳转
        'charset' => "UTF-8", // 编码格式
        'sign_type' => "RSA", // 签名方式 "MD5 RSA RSA2 DSA"
        'gatewayUrl' => "https://openapi.alipaydev.com/gateway.do", // 支付宝网关
        'merchant_private_key' => "MIICXAIBAAKBgQDyN5PIvuAPfY4eK7hT5q/Am/84zhGtMxmqccrEubWkmH5CrYTD+cH3gzhV07yYhRjGOyAxNyZYhstk0C2lFt4rSCfcYNIkokcugOCbJN3IdBVWn0XrRpEadvAbAYTlVbYPavS0mDrNhmsFiDGcmWvan4jjoBLQ4+YcttmBY3uO0wIDAQABAoGAciHwGBqg94uhCORvAdHkpOyc0YaAW82FXYQrGJGtXragYAjlU3b/iAKj7AVOi8vDhbzciXRENFinxIQ3zd+9pVq2CUo5/NPpYKTXD5iGMxR43LMzPb+Y9/mAePNGniRew2ulV5O2XGBjott9mz/MWmfrXkJPectfGnYBSyvzDEECQQD7ojSs9Z5hSyfc58DXQouQNlvRbOfeZaBRkH9s6H60Bm1iPVtzL5cYHfhAPJs1NOFEdvXE/bCP++algFgrP5MhAkEA9muKvExYpMsC8yBZtr+s2QdAI+ORFzivvHgzOKFDe4lrFdUwoa7JtgiEcFmOPdgaVsT7HS4T9eZusfEnlaSXcwJAA/JzaRMhPN107p4kBDxi/AwePJa8vFxTu3PMy+SJuEExjgzwVko00IgH0NoihR8jOXO0PzbLgjatU4ND3snMgQJAI5qEMBKj2COQ6InrDHGDStQ2WzkdFIrXMVb4p5z9QBMpyrzDywiTEl8Gq6j02VCo9ZJ+acfnkGbb5Y/fVUqzqwJBAIsbE/rsyeyQwOV9aK2wZ/HRpEmvcVBr8pDI58adATA/LH/fleelanPpBQvNh44cM4bNGaXk/wl/0qZ5Nd/4ps8=", // 商户私钥
        'alipay_public_key' => "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCnxj/9qwVfgoUh/y2W89L6BkRAFljhNhgPdyPuBV64bfQNN1PjbCzkIM6qRdKBoLPXmKKMiFYnkd6rAoprih3/PrQEB/VsW8OoM8fxn67UDYuyBTqA23MML9q1+ilIZwBC2AQ2UBVOrFXfFl75p6/B5KsiNG9zpgmLCUYuLkxpLQIDAQAB", // 支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm 对应APPID下的支付宝公钥。
    ],
    'wechat' => [
        'app_id' => '',
        'mch_id' => '',
        'key' => '', // API 密钥
        'notify_url' => SITE_DOMAIN . '/pay/wenotify', // 你也可以在下单时单独设置来想覆盖它
        //'cert_path' => 'path/to/your/cert.pem', // XXX: 绝对路径！！！！对外支付使用
        //'key_path' => 'path/to/your/key' // XXX: 绝对路径！！！！对外支付使用
    ],
    'wechatsandbox' => [
        'app_id' => '',
        'mch_id' => '',
        'key' => '', // API 密钥
        'notify_url' => SITE_DOMAIN . '/pay/wenotify', // 你也可以在下单时单独设置来想覆盖它
        //'cert_path' => 'path/to/your/cert.pem', // XXX: 绝对路径！！！！对外支付使用
        //'key_path' => 'path/to/your/key' // XXX: 绝对路径！！！！对外支付使用
    ],
    'paypal' => [
        'account' => '',
        'ClientID' => 'AYSq3RDGsmBLJE-otTkBtM-jBRd1TCQwFf9RGfwddNXWz0uFU9ztymylOhRS',
        'ClientSecret' => 'EGnHDxD_qRPdaLdZz8iCr8N7_MzF-YHPTkjs6NKYQvQSBngp4PTTVWkPZRbL',
        'returnurl' => SITE_DOMAIN . "/pay/paypalreturn",
        'cancelurl' => SITE_DOMAIN . "/pay/paypalcancel",
    ],
    // 服务器部署到paypal时区的要更好一些，省去登录等等麻烦
    'paypalsandbox' => [
        'account' => 'sb-gk7gt4953779@personal.example.com',
        'pwd' => '/.9,Fn^N',
        'testx'=>'sb-fa6eg4955145@business.example.com',
        'test'=>'#h%r)3fX',
        'ClientID' => 'AWkkf7YAwSWRQHPdGFtSa42tVcLe7v-PRR10X14hl8YMYHE1MuVEwDDTg_9v3OitCgqMRly8qfQxLLnJ',
        'ClientSecret' => 'EEK-1klvzLHfUtuNLtOSdXsbZwAF7OCTLIYgUYS4IbDX1bN8XnUkqsg749GKF6vtPN-spQLO3mZX-Yat',
        'returnurl' => SITE_DOMAIN . "/pay/paypalreturn",
        'cancelurl' => SITE_DOMAIN . "/pay/paypalcancel",
    ],
];
