<?php
return [
    
    // +----------------------------------------------------------------------
    // | 微信一系列api和secret
    // +----------------------------------------------------------------------
    
    // 登录授权信息
    'wxas' => [
        0 => [
            'appname' => '暂保密',
            'accountid' => 'gh_6c48739f46f6',
            'app_id'=>'wx0094c411f57f1d2e',
            'appid' => 'wx0094c411f57f1d2e',
            'secret' => '7dcfb99aa1b43903d848c613adbca1ff',
            'url'=>'https://www.dmake.cn/a/wechat-api-index',
            'token' => '4iKtNvp8mqKJloCf294v',
            'aes_key' => 'Wvn42DB8znIvJ4MG0Qse85M1po9OuTj20nQUGvzTFnM'
        ],
        1 => [
            'appname' => '铜梁生活馆',
            'appid' => 'wx17d9f462f641b50c',
            'secret' => 'd9cc72cae64f50671443cbe141efd812'
        ],
        2 => [
            'appname' => '龙康贝贝佳',
            'appid' => 'wx9916a5a3d3eb602c',
            'secret' => '4d2ef15a227fc9f1547e667ed5b779b7'
        ],
        10000 => [
            'appname' => '铜梁生活通',
            'appid' => 'wx64943c1af286a89f',
            'secret' => '6d75ac6225b6e052ef512e1d16f4de28'
        ]
    ],
    
    // 小程序配置
    'minprogram'=>[
        'app_id'=>'wxc7063c34c0d24d7f',
        'secret'=>'aaa0a805d4172cc27595b2b6fbcef044',
        'response_type' => 'array'
    ],
    
    // 微信支付登陆授权，微信支付页面授权
    'authwx' => [
        'appname' => '暂保密',
        'accountid' => 'gh_6c48739f46f6',
        'gh_id' => 'gh_6c48739f46f6',
        'app_id'=>'wx0094c411f57f1d2e',
        'appid' => 'wx0094c411f57f1d2e',
        'secret' => '7dcfb99aa1b43903d848c613adbca1ff',
        'url'=>'https://www.dmake.cn/a/wechat-api-index',
        'token' => '4iKtNvp8mqKJloCf294v',
        'aes_key' => 'Wvn42DB8znIvJ4MG0Qse85M1po9OuTj20nQUGvzTFnM'
    ],
    
    // 微信支付商户授权
    'authwechatpay' => [
        'app_id' => 'wx17d9f462f641b50c',
        'mch_id' => '1356899102',
        'key' => '177d8bbdec664a3ae381ffcf8fe08d22'
    ],
    
    // 第三方平台的配置信息
    'openflat' => [
        'app_id' => 'wx42d16b2f6bcf552d',
        'secret' => '217c2a33fd2331a9792bca3570a75e78',
        'token' => 'serviceforourlife',
        'aes_key' => 'ocjdtPdmEnWMzpIPeuQwYsXnIjSoIiKsPYxflQaFPue'
    ],
    
    // mp账号
    'mpaccount'=>[
        '铜梁包打听'=>'daneas714101@gmail.com',
        '铜梁视窗'=>'2594827648@qq.com',
        '同城购'=>'daneas3@qq.com',
        '随风来'=>'heyuemax@163.com'
    ]
    
];