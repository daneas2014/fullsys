<?php
return [
    'apshow' => 0, // 文章是否展示产品
    'asshow' => 0, // 文章是否展示厂商
    'avshow' => 0, // 文章是否展示视频
    'acshow' => 0, // 文章是否展示课程
    'arshow' => 0, // 文章是否展示资源
    'atshow' => 0, // 文章是否展示标签
    'amshow' => 0, // 文章是否展示商城
];