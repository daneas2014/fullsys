<?php

/**
 * 物联网数据库结构，tdengine参考这里：https://docs.taosdata.com/taos-sql/table/
 * 在TDengine中建表是个难事儿，要么在代码中做，要么在cmd里面taos ；taos -p；use db;写命令建表修改表例如
 * 新建表：CREATE STABLE iot_data_harmfulgas(ts timestamp,batch_id bigint,dev_id int,gateway_id int,h1 float,h2 float,h3 float,h4 float,h5 float,h6 float,h7 float,date_time varchar) TAGS(project_id int);这里用了varchar就报错，用binary(4n)才行！！！
 * 删除表：drop stable iot_data_harmfulgas;
 * 新增列： alter stable iot_data_harmfulgas add column date_time binary(12);
 * 修改表和列很麻烦，因此一次性设计好很重要，tags那个属于外键的样子
 */
return [
	'sensor' => [
		// 土壤墒情
		'soil' => [
			'relations' => [
				['label' => '时间戳', 'prop' => 'ts'],
				['label' => 'ID', 'prop' => 'data_id'],
				['label' => '批次ID', 'prop' => 'batch_id'],
				['label' => 'pH', 'prop' => 'j1'],//4.5-8.5
				['label' => '温度(℃)', 'prop' => 'j2'],//0-60
				['label' => '湿度(%)', 'prop' => 'j3'], // 0~100
				['label' => '容量（g/cm3）', 'prop' => 'j4'],//20-80
				['label' => '有机质（g/kg）', 'prop' => 'j5'],//0.10-10.00
				['label' => '有效磷（mg/kg）', 'prop' => 'j6'],//5-20
				['label' => '含氧量（mL/m³）', 'prop' => 'j7'],//10.00-20.10

				['label' => '设备', 'prop' => 'dev_id'],
				['label' => '网关', 'prop' => 'gateway_id'],
				['label' => '项目', 'prop' => 'project_id'],
			],
			'length' => 7,
		],
		// 水体监测
		'water' => [
			'relations' => [
				['label' => '时间戳', 'prop' => 'ts'],
				['label' => 'ID', 'prop' => 'data_id'],
				['label' => '批次ID', 'prop' => 'batch_id'],
				['label' => '温度（℃）', 'prop' => 'i1'],//0-30
				['label' => '氨氮(mg/L)', 'prop' => 'i2'],//0.02-150
				['label' => '溶解氧(mg/L)', 'prop' => 'i3'],//2-6
				['label' => '亚硝酸盐(mg/L)', 'prop' => 'i4'],//0.1-1.0
				['label' => '硫化物(mg/L)', 'prop' => 'i5'],//0-1
				['label' => 'pH', 'prop' => 'i6'],//6.5-8.5
				['label' => '余氯(mg/L)', 'prop' => 'i7'],//0-0.5
				['label' => '设备', 'prop' => 'dev_id'],
				['label' => '网关', 'prop' => 'gateway_id'],
				['label' => '项目', 'prop' => 'project_id'],
			],
			'length' => 7,
		],
		// 物流追踪
		'transport' => [
			'relations' => [
				['label' => '时间戳', 'prop' => 'ts'],
				['label' => 'ID', 'prop' => 'data_id'],
				['label' => '批次ID', 'prop' => 'batch_id'],
				['label' => '车厢温度(℃)', 'prop' => 't1'],//0-100
				['label' => '车厢湿度（%）', 'prop' => 't2'],//0-2
				['label' => '待1', 'prop' => 't3'],
				['label' => '待2', 'prop' => 't4'],
				['label' => '待3', 'prop' => 't5'],
				['label' => '设备', 'prop' => 'dev_id'],
				['label' => '网关', 'prop' => 'gateway_id'],
				['label' => '项目', 'prop' => 'project_id'],
			],
			'length' => 2,
		],
		// 火灾监测
		'smoke' => [
			'relations' => [
				['label' => '时间戳', 'prop' => 'ts'],
				['label' => 'ID', 'prop' => 'data_id'],
				['label' => '批次ID', 'prop' => 'batch_id'],
				['label' => '温度(℃)', 'prop' => 'a1'],//0-100
				['label' => '烟雾浓度（dB/m）', 'prop' => 'a2'],//0-2
				['label' => '待1', 'prop' => 'a3'],
				['label' => '待2', 'prop' => 'a4'],
				['label' => '待3', 'prop' => 'a5'],
				['label' => '设备', 'prop' => 'dev_id'],
				['label' => '网关', 'prop' => 'gateway_id'],
				['label' => '项目', 'prop' => 'project_id'],
			],
			'length' => 2,
		],
		// 电气设备
		'electronic' => [
			'relations' => [
				['label' => '时间戳', 'prop' => 'ts'],
				['label' => 'ID', 'prop' => 'data_id'],
				['label' => '批次ID', 'prop' => 'batch_id'],
				['label' => '电流（mA）', 'prop' => 'b1'],//0-1000
				['label' => '线路温度（℃）', 'prop' => 'b2'],//0-100
				['label' => '备用', 'prop' => 'b3'],
				['label' => '备用', 'prop' => 'b4'],
				['label' => '备用', 'prop' => 'b5'],
				['label' => '备用', 'prop' => 'b6'],
				['label' => '备用', 'prop' => 'b7'],
				['label' => '设备', 'prop' => 'dev_id'],
				['label' => '网关', 'prop' => 'gateway_id'],
				['label' => '项目', 'prop' => 'project_id'],
			],
			'length' => 7,
		],
		// 有害气体
		'harmfulgas' => [
			'relations' => [
				['label' => '时间戳', 'prop' => 'ts'],
				['label' => 'ID', 'prop' => 'data_id'],
				['label' => '批次ID', 'prop' => 'batch_id'],
				['label' => '甲烷（mg/m³）', 'prop' => 'h1'],//0-5
				['label' => '氨气（mg/m³）', 'prop' => 'h2'],
				['label' => '硫化氢（mg/m³）', 'prop' => 'h3'],//0-10
				['label' => '二氧化碳（mg/m³）', 'prop' => 'h4'],//1000-20000
				['label' => '一氧化碳（mg/m³）', 'prop' => 'h5'],//0-50
				['label' => '气压（kPa）', 'prop' => 'h6'],//100-250
				['label' => '温度（℃）', 'prop' => 'h7'],//0-100
				['label' => '设备', 'prop' => 'dev_id'],
				['label' => '网关', 'prop' => 'gateway_id'],
			],
			'length' => 7,
		],

	],
];