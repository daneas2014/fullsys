<?php
namespace addons\doclib\model;

use think\Db;
use think\Model;

class ChmModel extends Model
{

    protected $name = 'chm_subject';

    protected $autoWriteTimestamp = true;

    /**
     * 保存这篇文档
     *
     * @param [type] $data
     * @return void
     * @author dmakecn@163.com
     * @since 2020-11-05
     */
    public function saveitem($data)
    {
        $db = Db::name('chm_item');

        if (isset($data['id']) && $data['id'] > 0) {
            $id = $data['id'];
            unset($data['id']);
            return $db->where('id', $id)->update($data);
        }

        return $db->insertGetId($data);
    }

    /**
     * 单个文档的评论
     *
     * @param [type] $coment
     * @return void
     * @author dmakecn@163.com
     * @since 2020-11-05
     */
    public function savecomment($coment)
    {
        return Db::name('chm_comment')->insertGetId($coment);
    }

    /**
     * 保存邀约
     *
     * @param [type] $data
     * @return void
     * @author dmakecn@163.com
     * @since 2020-11-05
     */
    public function saveinvite($data)
    {

        $db = Db::name('chm_invite');

        if (isset($data['id'])) {
            return $db->where('id', $data['id'])->update($data);
        }

        return $db->insertGetId($data);
    }

    /**
     * 获得目录章节
     *
     * @param [type] $subid
     * @return void
     * @author dmakecn@163.com
     * @since 2020-11-05
     */
    public function getChmlist($subid)
    {
        $original = Db::name('chm_item')
            ->where('subid', $subid)
            ->order('parentid asc')
            ->field('id,title,parentid')
            ->select();

        $menu = subTreeLevel($original, 'title', 'parentid');

        return $menu;
        // 这里需要整理出一个菜单来，满足树形结构的菜单
        //  Chart
        //  --ChartChild
        //  ----doc
        //  Chart
        //  --doc
    }

    /**
     * 获得目录章节UL形式展现
     *
     * @param [type] $subid
     * @return void
     * @author dmakecn@163.com
     * @since 2020-11-05
     */
    public function getChmlistUl($subid, $hrefTmp, $title = 'title')
    {
        $original = Db::name('chm_item')
            ->where('subid', $subid)
            ->order('parentid asc')
            ->field('id,title,seotitle,parentid')
            ->select();

        $menu = subTreeUlLevel($original, $title, 'parentid', 'id', 0, $hrefTmp);

        return $menu;
    }

    /**
     * 获取某一篇内容
     *
     * @param [type] $id
     * @return void
     * @author dmakecn@163.com
     * @since 2020-11-05
     */
    public function getChart($id)
    {
        if ($id == 0) {
            return null;
        }
        return Db::name('chm_item')->where('id', $id)->find();
    }
}
