<?php
namespace addons\doclib\controller;

use addons\Base;
use think\Db;

class Admin extends Base {

	public function index() {

		$db = Db::name('chm_subject');

		return $this->vueQuerySingle($db);

	}

}