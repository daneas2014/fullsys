<?php
namespace addons\doclib\controller;

use addons\doclib\model\ChmModel; 
use think\addons\Controller;
use think\Db;

class Index extends Controller
{

    private $cm;

    public function _initialize()
    {
        parent::_initialize();

        $this->cm = new ChmModel();
    }

    public function index()
    {
        $list = $this->cm->select();

         $this->view->assign(['title' => '文档中心', 'keywords' => '', 'description' => '', 'list' => $list]);

         return $this->fetch();
    }

    public function doc()
    {
        $subid = input('param.id') ? input('param.id') : 1;

        $itemid = input('param.chart') ? input('param.chart') : 0;

        $menu = $this->cm->getChmlistUl($subid, ' href="/a/doclib-index-doc/id/' . $subid . '/chart/%u" ');

        $doc = $this->cm::get($subid);
        
        $doc['seotitle'] = $doc['title'];

        $item = $this->cm->getChart($itemid);

        $body = $item == null ? $doc : $item;

        $assign = ['title' => $body['seotitle'] . '-' . $doc['title'], 'keywords' => '', 'description' => $body['description'], 'menu' => $menu, 'doc' => $doc, 'page' => $body];

         $this->view->assign($assign);
         return $this->fetch();
    }

    public function savecomments()
    {
        if (request()->isPost()) {

            return $this->cm->savecomment(input('post.')) ? getJsonCode(true) : getJsonCode(false);
        }
    }

    public function getitem()
    {
        return $this->view->assign('html', $this->cm->getpage(input('get.id')))->fetch();
    }

    public function getcomments()
    {}

    /**
     * 关键词搜索
     *
     * @return void
     * @author dmakecn@163.com
     * @since 2020-11-11
     */
    public function search()
    {
        $key = input('param.key');
        $subid = input('param.subid');
        $list = Db::name('chm_item')
            ->where('seotitle|title|description', 'like', "%$key%")
            ->where('subid', $subid)
            ->field('id,title')
            ->select();

        return json($list);
    }
}
