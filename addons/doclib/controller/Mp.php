<?php
namespace addons\doclib\controller;

use addons\doclib\model\ChmModel;
use think\addons\Controller;
use think\Db;

class Mp extends Controller {
	public function index() {
        
		$u = getUserInfo();

		$this->assign(['title' => '文档中心', 'keywords' => '', 'description' => '','u'=>$u]);

		return $this->fetch();
	}

	public function docs() {
		$u = getUserInfo();

		$list = Db::name('chm_subject')
			->where('authorid', $u->id)
			->whereOr('authorid', 0)
			->paginate(10);

		$this->assign('page_method', $list->render());
		$this->assign('list', $list);

		$this->view->assign(['title' => '我参与的文档', 'keywords' => '', 'description' => '','u'=>$u]);
		return $this->fetch();
	}

	/**
	 * 节点页面梭哈
	 *
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2020-11-11
	 */
	public function pages() {
		$subid = input('param.subid');
		$cm = new ChmModel();

		$menu = $cm->getChmlistUl($subid, ' href="savepage?subid=' . $subid . '&id=%u" ', 'seotitle');

		$this->view->assign(['title' => '文档节点', 'keywords' => '', 'description' => '', 'list' => $menu, 'subid' => $subid]);
		return $this->fetch();
	}

	public function savepage() {

		$cm = new ChmModel();

		if (request()->isPost()) {
			$data = input('post.');

			return getJsonCode($cm->saveitem($data), '保存完毕');
		}

		$subid = input('param.subid');

		$charts = $cm->getChmlist($subid);

		$page = $cm->getChart(input('param.id'));
		if ($page) {

			$html = str_replace('&nbsp;', '&amp;nbsp;', $page['detail']);
			$html = str_replace('&gt;', '&amp;gt;', $html);
			$html = str_replace('&lt;', '&amp;lt;', $html);
			$page['detial'] = $html;
		}

		$menu = $cm->getChmlistUl($subid, ' href="javascript:ckp(%u);" ');

		$assign = ['title' => '编辑节点', 'keywords' => '', 'description' => '', 'subid' => $subid, 'charts' => $charts, 'page' => $page, 'menu' => $menu];

		return $this->view->assign($assign)->fetch($page == null ? 'newpage' : 'savepage');
	}

	public function savedoc() {

		$cm = new ChmModel();

		if (request()->isPost()) {
			$data = input('post.');

			if (!isset($data['id']) || $data['id'] == 0) {
				return json($cm->save($data));
			}
			return getJsonCode($cm->save($data, ['id' => $data['id']]), '保存完毕');
		}

		$id = input('param.id');

		$doc = null;

		if ($id > 0) {
			$doc = $cm::get($id);
			$html = str_replace('&nbsp;', '&amp;nbsp;', $doc['detail']);
			$html = str_replace('&gt;', '&amp;gt;', $html);
			$html = str_replace('&lt;', '&amp;lt;', $html);
			$doc['detial'] = $html;
		}

		return $this->view->assign(['title' => '编辑文档', 'keywords' => '', 'description' => '', 'doc' => $doc])->fetch($doc == null ? 'newdoc' : 'savedoc');
	}

	public function deldoc() {
		$id = input('param.subid');
		Db::name('chm_subject')->where('id', $id)->delete();
		Db::name('chm_item')->where('subid', $id)->delete();

		return redirect('/doclib/mp/docs');
	}

}
