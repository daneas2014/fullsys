<?php
// +----------------------------------------------------------------------
// | thinkphp5 Addons [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2016 http://www.zzstudio.net All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: Byron Sampson <xiaobo.sun@qq.com>
// +----------------------------------------------------------------------
namespace addons\doclib;

use think\Addons;
use think\Db;

/**
 * 插件测试
 * @author byron sampson
 */
class Index extends Addons {
	public $info = [
		'name' => 'doclib',
		'title' => '文档中心',
		'description' => '连载文档、云文档',
		'status' => 0,
		'author' => 'daneas',
		'version' => '0.1',
		'manage' => 'doclib://admin/index',
	];

	/**
	 * 插件安装方法
	 * @return bool
	 */
	public function install() {
		$res = Db::execute(initAddon($this->info));

		if (!$res) {
			return false;
		}

		$sql = file_get_contents(__DIR__ . "/data.sql");

		$a = explode(";\r\n", $sql); //根据";\r\n"条件对数据库中分条执行
		$total = count($a);
		for ($i = 0; $i < $total; $i++) {
			$res = Db::query($a[$i]);
		}

		return $res > 0 ? true : false;
	}

	/**
	 * 插件卸载方法
	 * @return bool
	 */
	public function uninstall() {
		$sql = "DROP TABLE IF EXISTS chm_item;
        DROP TABLE IF EXISTS chm_subject;
        delete from addons where name='doclib';";

		$a = explode(";\r\n", $sql); //根据";\r\n"条件对数据库中分条执行
		$total = count($a);
		for ($i = 0; $i < $total; $i++) {
			$res = Db::query($a[$i]);
		}
		return $res > 0 ? true : false;
	}

	/**
	 * 实现的testHook钩子方法
	 * @return mixed
	 */
	public function doclibhook($param) {
		echo '<p><font color="red">开始处理钩子啦</font></p>';
		echo '<p><font color="green">打印传给钩子的参数：</font></p>';
		dump($param);
		echo '<p><font color="green">打印插件配置：</font></p>';
		dump($this->getConfig());

		// 这里可以通过钩子来调用钩子模板
		return $this->fetch('info');
	}

}
