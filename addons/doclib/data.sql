-- ----------------------------
-- Table structure for chm_item
-- ----------------------------
DROP TABLE IF EXISTS `chm_item`;
CREATE TABLE `chm_item`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `seotitle` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `description` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `authorid` int(11) NOT NULL,
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  `parentid` int(11) NOT NULL,
  `subid` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for chm_subject
-- ----------------------------
DROP TABLE IF EXISTS `chm_subject`;
CREATE TABLE `chm_subject`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `description` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `authorid` int(11) NOT NULL,
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  `pic_url` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

INSERT INTO `chm_subject` (`id`, `title`, `description`, `detail`, `authorid`, `create_time`, `update_time`, `pic_url`) VALUES
(1, '板砖博客系统说明', '板砖博客是基于php和mysql的轻量级平台型系统，您可以通过后端配置形成完整的CMS+微信公众号管理工具+B2C商城+文档中心+资源下载平台+淘宝客平台，板砖博客还带有微信小程序源码。', '<p>\n	DMAKE轻量级PHP平台建站系统（板砖博客）是基于前后端分离的理念开发而来，经过门户网站、微信公众平台、资讯网站、资源网站和企业站群等多种运营模式和建站模式产品化，方便更多的个人站长和企业能够快速建立互联网品牌。\n</p>\n<p>\n	您可以通过后端配置形成完整的CMS+微信公众号管理工具+B2C商城+文档中心+资源下载平台+淘宝客平台，板砖博客还带有微信小程序源码。\n</p>\n<p>\n	前端采用了Bootstrap模块化布局，业务模块无缝组合；后端采用了CRUD标准化设计，新建业务模块从设计到上线不超过30分钟。\n</p>\n<p>\n	各版块介绍请看文档详情。\n</p>\n<p>\n<pre class=\"prettyprint lang-js\">本文档中心是参考仿制的看云在线文档。\n每天都会用到https://www.kancloud.cn/manual/thinkphp5/118003的教程，因此如何把1个功能多样化运用起来是这个板块创作的中心。\n\n一是可以做成类似的连载教程\n二是可以在此基础上拓展付费阅读的（小说连载）的模式。如何做销售连载的权限，也已提上了开发日程。</pre>\n</p>', 1, 1605237603, 1611551165, '');

INSERT INTO `chm_item` (`id`, `title`, `seotitle`, `description`, `detail`, `authorid`, `create_time`, `update_time`, `parentid`, `subid`, `status`) VALUES
(1, '系统安装', '板砖博客系统安装', '本篇为您讲述板砖博客的详细安装软硬件具体配置。', '<p>\n	板砖博客安装推荐硬件配置：1G内存以上，10G硬盘空间，1M宽带以上\n</p>\n<p>\n	板砖博客安装推荐系统配置：centos8.x ，php7.x，mysql5.6以上，推荐mysql8性能翻倍，系统配置memcache或者redis缓存\n</p>\n<p>\n	板砖博客存储推荐：系统自带七牛云存储方案\n</p>\n<p>\n	<br />\n</p>', 1, 0, 0, 0, 1, 1),
(2, 'CMS模块', 'CMS模块说明', 'CMS模块包含文章的增、删、改、差、系列文章绑定功能。', 'CMS模块包含文章的增、删、改、差、系列文章绑定功能。', 1, 0, 0, 0, 1, 1),
(3, '供应商模块', '供应商模块说明', '供应商模块包含了整套供应商、产品、产品附录等功能。', '供应商模块包含了整套供应商、产品、产品附录等功能。', 1, 0, 0, 0, 1, 1);