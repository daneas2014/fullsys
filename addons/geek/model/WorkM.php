<?php
namespace addons\geek\model;

use think\Model;

/**
 * 所有下拉选项、导航都用这个，和每一个信息关联
 *
 * @author dmakecn@163.com
 * @since 2023-03-07
 */
class WorkM extends Model {

	protected $name = 'geek_works';

	// 开启自动写入时间戳
	protected $autoWriteTimestamp = true;

	public function saveWork($data){
		if($data['id']){
			$id=$data['id'];
			unset($data['id']);
			return $this->valudate('WorkValidate')->where('id',$id)->save($data);

		}
		else{
			return $this->valudate('WorkValidate')->save($data);
		}
	}
}