<?php
namespace addons\geek\model;

use think\Model;

/**
 * 所有下拉选项、导航都用这个，和每一个信息关联
 *
 * @author dmakecn@163.com
 * @since 2023-03-07
 */
class OptionsM extends Model {

	protected $name = 'geek_options';

	// 开启自动写入时间戳
	protected $autoWriteTimestamp = true;

	/**
	 * 根据类型返回选项
	 *
	 * @param [type] $type
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2023-03-07
	 */
	public function getOptions($type) {
		return $this->where('type', $type)->select();
	}

	/**
	 * 统一保存，避免增项的时候没得改
	 *
	 * @param [type] $name
	 * @param [type] $desc
	 * @param [type] $type
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2023-03-07
	 */
	public function saveOption($name, $desc, $type) {
		$data['name'] = $name;
		$data['description'] = $desc;
		$data['type'] = $type;

		return $this->validate('OptionsValidate')->save($data);
	}
}