CREATE TABLE `geek_options` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL COMMENT '角色名',
  `description` varchar(500) NOT NULL COMMENT '角色描述',
  `type` varchar(10) NOT NULL,
  `create_time` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='极客角色';
INSERT INTO `geek_options` (`id`, `name`, `description`, `type`, `create_time`) VALUES
(11, '喜马拉雅', '喜马拉雅', 'platform', 0),
(2, '创建者', '作品创建者', 'role', 0),
(3, 'CV', 'CV', 'role', 0),
(4, '导演', '导演', 'role', 0),
(5, '编剧', '编剧', 'role', 0),
(6, '后期', '后期', 'role', 0),
(7, '美工', '美工', 'role', 0),
(8, '歌手', '歌手', 'role', 0),
(9, '作曲', '作曲', 'role', 0),
(10, '词作', '词作', 'role', 0),
(12, '蜻蜓FM', '蜻蜓FM', 'platform', 0),
(13, '游戏配音', '游戏配音', 'workfor', 0),
(14, '广告配音', '广告配音', 'workfor', 0),
(15, '书籍配音', '书籍配音', 'workfor', 0),
(16, '视频配音', '视频配音', 'workfor', 0),
(17, '批发/零售/食品/服务', '批发/零售/食品/服务', 'industry', 0),
(18, '汽车/机械/自动化', '汽车/机械/自动化', 'industry', 0);
ALTER TABLE `geek_options`
  ADD PRIMARY KEY (`id`);
ALTER TABLE `geek_options`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
COMMIT;


CREATE TABLE `geek_group` (
  `id` int(11) NOT NULL,
  `wid` int(11) NOT NULL COMMENT '作品id',
  `uid` int(11) NOT NULL DEFAULT '0' COMMENT '默认0，注册用户填用户id',
  `name` varchar(120) NOT NULL COMMENT '参与人',
  `roleid` int(11) NOT NULL COMMENT '用户角色'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='作品参与团队';
ALTER TABLE `geek_group`
  ADD PRIMARY KEY (`id`);
ALTER TABLE `geek_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;


CREATE TABLE `geek_works` (
  `id` int(11) NOT NULL,
  `userid` int(11) NOT NULL COMMENT '发起用户',
  `title` varchar(150) NOT NULL COMMENT '项目名称',
  `description` varchar(200) NOT NULL COMMENT '项目描述',
  `showpic` varchar(200) NOT NULL COMMENT '项目封面',
  `detail` text NOT NULL COMMENT '项目说明',
  `link` varchar(500) NOT NULL COMMENT '平台的链接',
  `mywork` varchar(50) NOT NULL COMMENT '团队角色',
  `publish_time` int(11) NOT NULL COMMENT '发布时间',
  `roleid` int(11) NOT NULL COMMENT '',
  `workfor` int(11) NOT NULL COMMENT '',
  `industry` int(11) NOT NULL COMMENT '',
  `platform` int(11) NOT NULL COMMENT '',
  `check_time` int(11) NOT NULL COMMENT '审核时间',
  `create_time` int(11) NOT NULL COMMENT '创建时间'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='极客的作品';
ALTER TABLE `geek_works`
  ADD PRIMARY KEY (`id`);
ALTER TABLE `geek_works`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;


CREATE TABLE `geek_tags` (
  `id` int(11) NOT NULL,
  `datatype` varchar(10) NOT NULL COMMENT '对象类型：user、works、actions、recruit（人、作品、动态、招募）',
  `dataid` int(11) NOT NULL COMMENT '对象id',
  `name` int(11) NOT NULL COMMENT '标签名'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='极客标签';


ALTER TABLE `geek_tags`
  ADD PRIMARY KEY (`id`);
ALTER TABLE `geek_tags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;


CREATE TABLE `geek_recruit` (
  `id` int(11) NOT NULL,
  `title` varchar(120) NOT NULL,
  `description` varchar(200) NOT NULL,
  `idtype` int(1) NOT NULL COMMENT '0是个人，1是团队，-1是不限',
  `start_time` int(11) NOT NULL,
  `select_time` int(11) NOT NULL,
  `end_time` int(11) NOT NULL,
  `selectee` int(11) NOT NULL COMMENT '中选人',
  `detail` text NOT NULL COMMENT '项目说明',
  `showpic` varchar(200) NOT NULL,
  `create_time` int(11) NOT NULL,
  `adminid` int(11) NOT NULL COMMENT '创建人id',
  `roleid` int(11) NOT NULL COMMENT '用户角色'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='招募广场项目';
ALTER TABLE `geek_recruit`
  ADD PRIMARY KEY (`id`);
ALTER TABLE `geek_recruit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;


CREATE TABLE `geek_recruitpost` (
  `id` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `create_time` int(11) NOT NULL,
  `post` varchar(1000) NOT NULL COMMENT '应标内容',
  `wids` varchar(100) NOT NULL COMMENT '作品集',
  `result` int(11) NOT NULL COMMENT '默认0,1 是被选中',
  `check_time` int(11) NOT NULL COMMENT '中选时间'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='应标者';
ALTER TABLE `geek_recruitpost`
  ADD PRIMARY KEY (`id`);
ALTER TABLE `geek_recruitpost`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;