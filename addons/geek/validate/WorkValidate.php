<?php
namespace addons\geek\validate;

use think\Validate;

class WorkValidate extends Validate {

	protected $rule = [
		'title' => 'require|max:25',
		'description' => 'require|length:1,100',
		'link' => 'require',
	];

	protected $message = [
		'title.require' => '名称必须',
		'title.max' => '名称最多不能超过25个字符',
		'description.require' => '描述必须填写',
		'description.length' => '描述长度必须在100个字符内',
		'link.require' => '选项类型必须填',
	];

}