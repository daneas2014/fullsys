<?php
namespace addons\geek\validate;

use think\Validate;

class OptionsValidate extends Validate {

	protected $rule = [
		'name' => 'require|max:25',
		'description' => 'require|length:1,100',
		'type' => 'require|in:role,platform,workfor,industry',
	];

	protected $message = [
		'name.require' => '名称必须',
		'name.max' => '名称最多不能超过25个字符',
		'description.require' => '描述必须填写',
		'description.length' => '描述长度必须在100个字符内',
		'type.require' => '选项类型必须填',
		'type,in' => '类型必须是role,platform,workfor,industry之一',
	];

}