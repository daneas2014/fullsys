<?php
namespace addons\geek\controller;

use think\addons\Controller;

class Index extends Controller {

	/**
	 * 极客中心，包含个人、团队、最新任务、最新作品
	 *
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2023-02-27
	 */
	public function index() {
		$this->assign(['title' => '声音号配音平台 威客平台', 'keywords' => '配音社区', 'description' => '配音平台']);
		return $this->fetch();
	}

	/**
	 * 最新发布的任务
	 *
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2023-02-27
	 */
	public function tasks() {}

	/**
	 * 任务详情
	 *
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2023-02-27
	 */
	public function task() {}

	/**
	 * 个人作品清单
	 *
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2023-02-27
	 */
	public function ulist() {

	}

	/**
	 * 团队作品清单
	 *
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2023-02-27
	 */
	public function glist() {}

	/**
	 * 作品
	 *
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2023-02-27
	 */
	public function detail() {}

}