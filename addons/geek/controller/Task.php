<?php
namespace addons\geek\controller;

use think\addons\Controller;

class Task extends Controller {

	/**
	 * 我发布的/参与的任务，清单上打标签区别，赋予不同的管理按钮
	 *
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2023-02-27
	 */
	public function my() {}

	/**
	 * 编辑任务
	 *
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2023-02-27
	 */
	public function edit() {}

    /**
     * 上传声音作品（owner）
     *
     * @return void
     * @author dmakecn@163.com
     * @since 2023-02-27
     */
    public function uploadvioce(){}

}