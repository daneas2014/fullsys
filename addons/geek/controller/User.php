<?php
namespace addons\geek\controller;

use addons\Base;
use addons\geek\model\WorkM;
use think\Db;

/**
 * 个人作品信息
 *
 * @author dmakecn@163.com
 * @since 2023-02-27
 */
class User extends Base {

	/**
	 * 个人中心
	 *
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2023-02-27
	 */
	public function index() {

		$this->assign(['title' => '我的作品', 'keywords' => '', 'description' => '']);

		return $this->fetch();

	}

	/**
	 * 个人标签等
	 *
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2023-02-27
	 */
	public function info() {
		return $this->fetch();
	}

	/**
	 * 个人作品清单
	 *
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2023-03-07
	 */
	public function mine() {
		return $this->fetch();
	}

	/**
	 * 保存个人作品
	 *
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2023-03-07
	 */
	public function savework() {

		$op = Db::name('geek_options');

		$this->assign(['title' => '发布作品', 'keywords' => '', 'description' => '', 'options' => $op->select()]);

		$m = new WorkM();
 

		if (request()->isPost) {
			$data = input('param.');
			$data['check_time'] = 0;

			$res = $m->saveWork($data);

			if ($res) {
				return json(['code' => 1, 'msg' => '保存成功']);
			}

			return json(['code' => 0, 'msg' => $res]);
		}
		else{

			$res = $m->find(input('param.id'));

			if (!$res) {
				$this->assign('doc', null);
			} else {
				$this->assign('doc', $res);
			}
		}

		return $this->fetch();
	}
}