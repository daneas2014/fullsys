<?php
namespace addons;

use think\addons\Controller;

class Base extends Controller {

	public function _initialize() {
		$uid = session('mid');
		$userinfo=session('memberinfo');
		$username = session('username');

		if (!$uid || !$userinfo) {
			$this->redirect('/ucenter/uauth/login');
		}

		/**
		 * 未完工 
		 */
		$this->assign([
			'username' => session('username'),
			'portrait' => session('portrait'),
			'rolename' => session('rolename'),
		]);
	}

	/**
	 * 适用于单表查询，有id,title,keywords,description的表，前端用VUE展示
	 *
	 * @param unknown $db
	 * @param array $map
	 * @param array $orderby
	 * @param $assign 传入要渲染的参数
	 * @param string $fields 是否需要精准搜索字段
	 * @return \think\response\Json|unknown
	 */
	public function vueQuerySingle($db, $map = [], $orderby = 'id desc', $assign = [], $fields = null) {
		if ($orderby == '' || $orderby == null) {
			$orderby = 'id desc';
		}

		$parentid = input('get.parentid') ? input('get.parentid') : 0;
		$this->assign('parentid', $parentid);

		if ($map == []) {
			$key = input('key');
			if ($key && $key !== "") {
				$map['title|keywords|description'] = ['like', "%" . $key . "%"];
			}
		}

		$Nowpage = input('get.page') ? input('get.page') : 1;
		$limits = input('get.rows') ? input('get.rows') : config('list_rows'); // 获取总条数

		if ($fields !== null) {
			$list = $db->where($map)
				->page($Nowpage, $limits)
				->order($orderby)
				->field($fields)
				->paginate($limits);
		} else {
			$list = $db->where($map)
				->page($Nowpage, $limits)
				->order($orderby)
				->paginate($limits);
		}

		$data['list'] = $list->items();
		$data['count'] = $list->total();
		$data['page'] = $Nowpage;

		if (input('get.page')) {
			return json($data);
		}

		$this->view->assign($assign);
		return $this->fetch();
	}

	/**
	 * 适用于单表保存，主键为id的表,有update_time,create_time
	 *
	 * @param unknown $db
	 * @param unknown $data
	 * @param $assign 传入要渲染的参数
	 * @return \think\response\Json
	 */
	public function singleDataSave($db, $data, $dbname = '', $assign = []) {

		if (request()->isPost()) {

			$uid = session('uid');
			$username = session('username');

			// 测试账号的漏
			if ($username == 'test') {
				return json(['code' => -1, 'msg' => '该账号禁止该操作']);
			}

			$act = $data['act'];
			$id = $data['id'];

			if (array_key_exists('act', $data)) {
				unset($data['act']);
			}
			if (array_key_exists('id', $data)) {
				unset($data['id']);
			}
			if (array_key_exists('file', $data)) {
				unset($data['file']);
			}
			if (array_key_exists('route', $data)) {
				unset($data['route']);
			}

			if ($act == 'del') {
				$result = $db->where('id', $id)->delete();
			}

			if ($act == 'edit' && $id && $id > 0) {
				$result = $db->where('id', $id)->update($data);
			}

			if (($act == 'edit' || $act == 'new') && ($id == 0 || $id == null)) {
				$data['create_time'] = time();
				$result = $db->insertGetId($data);
			}

			if ($result) {
				return json(['code' => 1, 'data' => $result, 'msg' => '操作成功']);
			}

			return json(['code' => -1, 'data' => $result, 'msg' => '操作失败']);
		}

		$id = input('param.id');

		$assign['data'] = $id > 0 ? $db->find(['id' => $id]) : null;

		// 转义html代码，不然后端显示不出来
		if ($assign['data'] && isset($assign['data']['detail'])) {
			$assign['data']['detail'] = $this->foramp($assign['data']['detail']);
		} else if ($assign['data'] && isset($assign['data']['content'])) {
			$assign['data']['content'] = $this->foramp($assign['data']['content']);
		} else if ($assign['data'] && isset($assign['data']['summary'])) {
			$assign['data']['summary'] = $this->foramp($assign['data']['summary']);
		}

		$this->view->assign($assign);
		return $this->fetch();
	}

	/**
     * html转义
     *
     * @param string $html
     * @return void
     * @author dmakecn@163.com
     * @since 2021-01-21
     */
    private function foramp($html = '')
    {

        if ($html == null || $html == "") {
            return "";
        }

        $html = str_replace('&nbsp;', '&amp;nbsp;', $html);
        $html = str_replace('&gt;', '&amp;gt;', $html);
        $html = str_replace('&lt;', '&amp;lt;', $html);

        return $html;
    }
}
