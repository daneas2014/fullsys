<?php
namespace addons\wechat\model;

use EasyWeChat\Kernel\Messages\Text;
use think\Db;
use think\Model;

class ServiceLuckmoney extends Model {

	protected $name = 'wx_luckcode';

	/*
	 * 发口令红包，要是没有获得过现金红包就可以一直发，当次活动获得过之后就只能领券了
	 */
	public function ProcessCode($gh_id, $code, $openid) {
		//人工淘汰
		if (rand(1, 10) == 8) {
			return new Text('现在抽奖人数太多，请稍后再试！');
		}

		$tao = ''; //营销平台的广告语

		$one = $this->where('code', $code)
			->where('gh_id', $gh_id)
			->find(); // 还剩余多少现金红包

		if ($one == null) {
			return new Text('您的口令错误【' . $code . '】！请在本公众号文章中找寻正确口令');
		}

		if ($one['starttime'] > time() || $one['endtime'] < time()) {
			return new Text('口令使用时间为' . date('Y-m-d H:i:s', $one['starttime']) . '至' . date('Y-m-d H:i:s', $one['endtime']));
		}

		if ($one['totalnum'] == 0 && $one['leftnum'] == 0) {
			return new Text('现在的口令是:' . $one['othercode']);
		}

		//如果没有红包了就啥也没有
		if ($one['leftnum'] <= 0) {

			$othercode = $tao;
			if (strlen($one['othercode']) > 1) {
				$othercode = '今天的红包早早被领完，要不咱们再加一个？试试 ' . $one['othercode'];
			}

			return new Text('今日口令"' . $code . '"红包' . $one['totalnum'] . '个已领完。请关注明日微信公众号推文，寻找当天口令！也可以点击=><a href="' . $one['jumpurl'] . '">#这里#</a>了解他们的活动详情加客服daneas2014详询活动日程和诸多好物推荐！' . $othercode);
		}

		$db = Db::name('wx_luckcodelog');

		$logs = $db->where('code', $code)
			->where('openid', $openid)
			->where('gh_id', $gh_id)
			->select();

		//个人参与次数已完的情况
		if (count($logs) >= $one['maxnum']) {
			return new Text('您今天的口令次数已耗尽（共' . $one['maxnum'] . '次），还剩余' . $one['leftnum'] . '个现金红包哟，赶紧邀约你的好友参与吧' . $tao);
		}

		//是否存在领取过现金红包的状态，为什么不直接find？减少数据库负载
		$ifmoneycheck = false;
		foreach ($logs as $l) {
			if ($l['money'] == 1) {
				$ifmoneycheck = true;
				continue;
			}
		}

		// 没有领过的的才生成现金红包
		if ($ifmoneycheck == false) {

			// 这里是在算比率，rate指的一轮几个，如果刚好在领红包位上就可以领取
			if ($one != null && $one['leftnum'] > 0 && ($db->where('code', $code)->count()) % $one['rate'] == 0) {

				$type = $one['leftnum'] > 3 ? rand(1, 2) : 1;

				if ($type == 2 && rand(1, 4) == 4) {
					$type = 2;
				} else {
					$type = 1;
				}

				if ($type == 1) {

					$this->where('code', $code)->setDec('leftnum', 1);
				} else {

					$this->where('code', $code)->setDec('leftnum', 3);
				}

				$db->insert([
					'openid' => $openid,
					'code' => $code,
					'gh_id' => $gh_id,
					'money' => 1,
					'createtime' => time(),
				]);

				//生成裂变红包？？？？根据随机来看是单个红包还是裂变红包
				return new Text(self::CreateLuckM($gh_id, $openid, $type));
			}
		}

		// 生成不要钱的券包
		$db->insert([
			'openid' => $openid,
			'code' => $code,
			'gh_id' => $gh_id,
			'money' => 0,
			'createtime' => time(),
		]);

		$random = rand(0, 1);

		if ($random == 0) {
			return new Text('(⊙o⊙)… 这次你啥也没中，继续关注本活动总会有搞着的<a href="' . $one['jumpurl'] . '">' . $one['jumpword'] . '</a>');
		}

		return new Text(self::CreateLuckC($gh_id, $openid, $one['jumpurl'], $one['jumpword']));
	}

	//生成不要钱的全包，totalfee就代表支出金额
	public function CreateLuckC($gh_id, $openid, $jumpurl, $jumpword) {
		$order['out_trade_no'] = 'LUCKEY' . time() . rand(0, 9);
		$order['trade_type'] = 'API';
		$order['type'] = 1;
		$order['tag'] = '口令红包';
		$order['body'] = '口令红包';
		$order['detail'] = '优惠券';
		$order['total_fee'] = 0;
		$order['scopenid'] = $openid;
		$order['overjump'] = $jumpurl;
		$order['gh_id'] = $gh_id;
		$order['createtime'] = time();

		$m = new OrderModel();
		$orderid = $m->createOrder($order, null);
		$split = WX_AUTHSPLIT;
		$url = wx_get_code('pay' . $split . 'luckmoney' . $split . '1' . $split . $orderid, addon_url('wechat://auth/index'), config('authwx'));

		return '<a href="' . $url . '">#' . $jumpword . '#</a>点击这里参与抽奖哟！';
	}

	/*
	 * 生成普通红包和裂变红包，type=1是普通红包，2是裂变红包
	 */
	public function CreateLuckM($gh_id, $openid, $type) {
		$order['out_trade_no'] = 'LUCKEY' . time() . rand(0, 9);
		$order['trade_type'] = 'API';

		$order['type'] = $type;
		$order['tag'] = '铜梁视窗生活服务平台';
		$order['body'] = '铜梁视窗生活服务平台';
		$order['detail'] = '关注铜梁视窗公众号免费发布信息，还有阅读奖励等你来拿！';
		$order['total_fee'] = 100;

		if ($order['type'] == 2) {
			$order['tag'] = '铜梁视窗生活服务平台';
			$order['body'] = '铜梁视窗生活服务平台';
			$order['detail'] = '关注铜梁视窗公众号免费发布信息，还有阅读奖励等你来拿！';
			$order['total_fee'] = 300;
		}

		$order['scopenid'] = $openid;
		$order['gh_id'] = $gh_id;
		$order['overjump'] = 'https://www.dmake.cn';
		$order['createtime'] = time();

		$m = new OrderModel();
		$orderid = $m->createOrder($order, null);
		$split = WX_AUTHSPLIT;
		$url = wx_get_code('pay' . $split . 'luckmoney' . $split . '1' . $split . $orderid, addon_url('wechat://auth/index'), config('authwx'));

		$notice = '#注意：当日红包在1个小时内不领取将会被系统收回';

		if ($type == 1) {
			return '<a href="' . $url . '">#关注“铜梁生活通”公众号后点此链接领红包#</a>每天共3次抽奖机会哟！么么哒' . $notice;
		}

		return '<a href="' . $url . '">#关注“铜梁生活通”公众号后点此链接领取裂变红包，自己另一个还能分享给另外两个朋友各1个#</a>每天共3次抽奖机会哟！么么哒' . $notice;
	}
}