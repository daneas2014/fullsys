<?php
namespace addons\wechat\model;

use app\common\model\TaobaoBase;
use EasyWeChat\Kernel\Messages\News;
use EasyWeChat\Kernel\Messages\NewsItem;
use EasyWeChat\Kernel\Messages\Text;
use think\Db;
use think\Model;

class ServiceProcess extends Model {

	/**
	 * 提交内容给微信会员
	 *
	 * @param unknown $array
	 */
	public function processPutTxt($wxid, $openid, $word) {
		preg_match('/@([^@]*)@/i', $word, $matchs);
		if (count($matchs) == 2) {
			$code = str_replace(' ', '', $matchs[1]);
			$group = new ServiceLuckmoney();
			return $group->ProcessCode($wxid, $code, $openid);
		}

		if (preg_match('/^\d*$/', $word)) {
			// 调用投票解析 :直接投投票项目id
			$toupiao = new ServiceToupiao();
			return $toupiao->fast($openid, $word, $word);
		}

		// 先查询关键词
		$keywords = Db::name('wx_keywords')->where('keyword', 'like', '%' . $word . '%')
			->where('gh_id', $wxid)
			->order('sort asc')
			->find();

		if ($keywords != null) {
			if ($keywords['wordorpic'] == 0) {
				return new Text($keywords['description']);
			} else {
				$toupiaos = array();
				$nitem = new NewsItem([
					'title' => $keywords['title'],
					'description' => $keywords['description'],
					'image' => $keywords['showpic'],
					'url' => $keywords['url'] . "?openid=$openid", //关键词要带上openid，因为订阅号获取不到呀
				]);
				array_push($toupiaos, $nitem);

				return new News($toupiaos);
			}
		}

		if (strstr($word, '签到')) {
			$signin = new ServiceSignin();
			return $signin->index($wxid, $openid);
		}

		if (strstr($word, '投票')) {
			// 调用投票解析 :投票+11+1
			$toupiao = new ServiceToupiao();
			return $toupiao->fast($openid, $word);
		}

		if (strstr($word, '搜')) {
			// 调用淘宝客
			$taobao = new TaobaoBase();
			return $taobao->search($openid, str_replace('搜', '', $word));
		}

		if (strstr($word, '答题')) {
			// 调用投票解析 :投票+11+1
			$exam = new ServiceExam();
			return $exam->load($word);
		}

		if (strstr($word, '朋友是个圈')) {
			$group = new ServiceGroups();
			return $group->identify($word, $openid);
		}

		if (strstr($word, '随手拍')) {
			return self::pai($word, $openid);
		}

		if (strstr($word, '笑话') || strstr($word, '天气') || strstr($word, '段子')) {
			return self::robot($word, $openid);
		}

		if (strstr($word, '商务合作')) {
			return new Text('加微信个人号：daneas2014 或者拨打电话 18996069665');
		}

		if (strstr($word, WX_AUTH_DOMAIN)) {
			$url = $word . '?openid=' . $openid;
			if (strstr($word, '&')) {
				$url = $word . '&openid=' . $openid;
			}
			return new Text('应相关政策要求，网民发布信息必须通过微信实名认证        <a href="' . $url . '">点击进入（无需认证）</a>');
		}

		// 此处应该加上关键词识别

		$news = self::xinwen($word);
		if ($news == false) {
			return new Text('因相关原因关闭机器人智能回答，如需帮助请联系客服');
			//return self::robot($word, $openid);
		}
		return $news;
	}

	public function xinwen($word) {
		$list = null;
		$list = Db::name('article')->field('id,title,summary,imagepath')
			->where('title', 'like', '%' . $word . '%')
			->limit(6)
			->order('id desc')
			->select();

		if ($list == null) {
			return false;
		}

		$articles = array();
		foreach ($list as $item) {

			$nitem = new NewsItem([
				'title' => $item['title'],
				'description' => $item['summary'],
				'image' => makeimg($item['imagepath']),
				'url' => makeurl($item['id'], 'news'),
			]);

			array_push($articles, $nitem);
		}

		return new News($articles);
	}

	public function pai($word, $openid) {
		$pai = array();
		$pailist = Db::name('wx_pai_topic')->field('id,title,showpic,description,starttime,endtime')
			->order('id desc')
			->limit(8)
			->select();

		$pai1 = new NewsItem([
			'title' => '随手拍铜梁分享铜梁生活，参与就有红包奖励！',
			'description' => '本活动由铜梁视窗发起，任何铜梁视窗粉丝通过随手拍铜梁分享铜梁生活事儿，加客服好友即送微信红包，分享朋友圈获取100好评即可获得5元现金红包，限量放送100人先到先得送完为止。',
			'image' => 'http://www.5atl.com/static/images/ssp.png',
			'url' => 'http://www.5atl.com/wechat-pai-apply.html?openid=' . $openid,
		]);

		array_push($pai, $pai1);

		foreach ($pailist as $item) {
			$status = '【进行中点击进入】';

			if ($item['starttime'] > time()) {
				$status = '【未开始】';
			}

			if ($item['endtime'] < time()) {
				$status = '【已完结】';
			}

			$nitem = new NewsItem([
				'title' => $item['title'] . $status,
				'description' => $item['description'],
				'image' => makeimg($item['showpic']),
				'url' => 'http://www.5atl.com/wechat-pai-topic.html?tid=' . $item['id'] . '&openid=' . $openid,
			]);

			array_push($pai, $nitem);
		}

		return new News($pai);
	}

	/**
	 * 图灵机器人回复
	 *
	 * @param $word 关键词
	 * @param $uid 客户id
	 */
	public function robot($word, $uid) {
		$result = tulingchat($word, $uid);
		$json = json_decode($result, true);
		$txt = '';

		if ($json['code'] == 40001 || $json['code'] == 40002 || $json['code'] == 40004 || $json['code'] == 40007) {
			$txt = $json['text'];
		}
		if ($json['code'] == 100000) {
			$txt = $json['text'];
		}
		if ($json['code'] == 200000) {
			$txt = $json['text'] . '-><a href="' . $json['url'] . '">点击阅读全文</a>';
		}
		if ($json['code'] == 302000) {
			$txt = $json['text'] . ':\n';
			foreach ($json['list'] as $item) {
				$txt .= '<a href="' . $item['detailurl'] . '">' . $item['article'] . '</a>\n';
			}
		}
		if ($json['code'] == 308000) {
			$txt = $json['text'] . ':\n';
			foreach ($json['list'] as $item) {
				$txt .= '<a href="' . $item['detailurl'] . '">' . $item['name'] . '</a>\n';
			}
		}

		return new Text($txt);
	}
}