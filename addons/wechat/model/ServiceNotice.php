<?php
namespace addons\wechat\model;

use think\Db;
use think\Model;

/*
 * 这个类做以下工作
 * 1. 发送公众号推送消息
 * 2. 协助公众号获得消息模板并绑定到数据库中
 * 3. 协助处理公众号消息模板数据
 */
class ServiceNotice extends Model
{

    /*
     * 这个是push调用的
     */
    public function sendMsg($app, $msg)
    {
        $data = $msg['data'];
        
        $data['first'] = [
            'value' => $data['first'],
            'color' => '#ff0000'
        ];
        
        $data['remark'] = [
            'value' => $data['remark'],
            'color' => '#07519a'
        ];
        
        $tem = self::getMsgFormat($msg['type']);
        
        if (array_key_exists('miniprogram', $msg)) {
            $app->template_message->send([
                'touser' => $msg['openid'],
                'template_id' => $tem['id'],
                'url' => 'http://weixin.qq.com/download',
                'miniprogram' => [
                    'appid' => $msg['minappid'],
                    'pagepath' => $msg['url']
                ],
                'data' => $data
            ]);
        } else {
            $app->template_message->send([
                'touser' => $msg['openid'],
                'template_id' => $tem['id'],
                'url' => $msg['url'],
                'data' => $data
            ]);
        }
        return true;
    }

    public function sendSubMsg($app, $msg)
    {
        $tem = self::getMsgFormat($msg['type']);
        
        $data = json_decode($msg['data'], true);
        $app->template_message->sendSubscription([
            'touser' => $msg['openid'],
            'template_id' => $tem['id'],
            'url' => $msg['url'],
            'scene' => 1000,
            'data' => $data
        ]);
        
        return true;
    }

    /**
     * 创建模板消息，并给公众号所有人发信息
     *
     * @param
     *            appid:公众号appid
     * @param
     *            type:是什么类型：config('msgtemplate')
     * @param
     *            value:array数据，键和值对应
     */
    public function createMsg($data)
    {
        // 1. 先建立模板消息
        $template = $this->getMsgFormat($data['type']);
        $msg['title'] = $data['title'];
        $msg['gh_id'] = $data['gh_id'];
        $msg['appid'] = $data['appid'];
        $msg['type'] = $data['type'];
        $msg['templateid'] = $template['id'];
        $msg['url'] = $data['url'];
        $msg['createtime'] = time();
        $msg['isurl'] = 1;
        
        if ($data['type'] == 'gift') {
            $msg['isurl'] = 0;
            $msg['minappid'] = 'wxc3a4d33951b1f98c';
            $msg['miniprogram'] = 'miniprogram';
            $msg['url'] = '/pages/tk/index';
        }
        
        $tid = Db::name('op_msgtemplate')->insertGetId($msg);
        
        // 2. 把所有相关人等都搞进MSG里面去        
        // $sql = 'insert into think_op_msg (tid,openid,sendtime,status)
        // select ' . $tid . ',openid,0,0 from think_op_appuser where gh_id="' . $data['gh_id'] . '"';
        
        // return Db::query($sql);
    }

    /**
     * 返回这个模板的模板格式到前端，并以此做提交的信息
     *
     * @param unknown $type            
     * @return string
     */
    public function getMsgFormat($type)
    {
        foreach ($this->MSG() as $key => $value) {
            if ($key == $type) {
                return $value;
            }
        }
    }

    public function getMsgType()
    {
        $types = [];
        foreach ($this->MSG() as $key => $value) {
            array_push($typs, $key);
        }
        return $types;
    }

    /*
     * 微信模板消息类型和格式
     */
    private function MSG()
    {
        return [
            'newuser' => [
                'id' => '0Qv2H2OPBFkVZxvFnSnpfpiJAkPHtjUJKwWbVIrnapI',
                'fields' => 'first,cardBumber,address,VIPName,VIPPhone,expDate,remark',
                'title' => '标题,地址,姓名,电话,有效期,备注'
            ], // 填写了手机号注册会员通知
            'signup' => [
                'id' => 'ija2zuu9FqwsiBQsXV4BbroIYT18Bs5lqMjs8jN-SRQ',
                'fields' => 'first,keyword1,keyword2,keyword3,remark',
                'title' => '标题,签到时间,获得积分,累计积分,备注'
            ], // 签到得金币通知:
            'cointotick' => [
                'id' => 'RMhwHxH_pVqSB5zFIypruXaf30iLYoTH-ul72LN9vXs',
                'fields' => 'first,keyword1,keyword2,keyword3',
                'title' => '标题,券名称,券编码,兑换时间,备注'
            ], // 金币换券通知
            'tickdie' => [
                'id' => '39JnvWWIQkhTygqwHkidZjgGs7VL9i6hySKKwJfyS9c',
                'fields' => 'first,keyword1,keyword2,keyword3,remark',
                'title' => '标题,退款金额,过期票券,票券张数,备注'
            ], // 券到期了通知
            'tickused' => [
                'id' => 'InIHRfhSjT232TZbw18abiXTfnpmamxB5SolmclIcHI',
                'fields' => 'first,keyword1,keyword2,keyword3,keyword4,remark',
                'title' => '标题,券号,消费门店,消费项目,消费时间,备注'
            ], // 券被用了通知
            'tickborth' => [
                'id' => 'gMHGTCjJ9wt0-TC7aaQYQIjUImA5U4Gf3ADG2wVw4A0',
                'fields' => 'first,keyword1,keyword2,keyword3,keyword4,remark',
                'title' => '标题,商户名称,卡券名称,有效期,办理时间,备注'
            ], // 收到一张券通知
            'notice' => [
                'id' => 'xRFOHnnn_eKYKBXhXZ3xKvSMX2m0hZHpKRDbSxxsoNM',
                'fields' => 'first,tradeDateTime,orderType,customerInfo,orderItemName,orderItemData,remark',
                'title' => '标题,提交时间,订单类型,客户信息,订单名称,订单内容,备注'
            ], // 普通通知
            'gift' => [
                'id' => '_N7p4QVxXeKYJQ17mBmh2GU0JXS_HZOEUNbTfjCGl5w',
                'fields' => 'first,keyword1,keyword2,keyword3,keyword4,remark',
                'title' => '标题,订单编号,下单时间,赠送状态,接收人,备注'
            ], // 赠送通知
            'bbjhd' => [
                'id' => '1x3CPCxFQnPajPJtEN8u2wgMrIzBLcmVHYVLbkbiL3g',
                'fields' => 'first,keyword1,keyword2,remark',
                'title' => '标题,交易内容,交易时间,备注'
            ],
            'other' => ''
        ];
    }
}