<?php
namespace addons\wechat\model;

use think\Model;
use think\Db;
use EasyWeChat\Kernel\Messages\News;
use EasyWeChat\Kernel\Messages\NewsItem;
use EasyWeChat\Kernel\Messages\Text;
use EasyWeChat\Kernel\Messages\Image;

class ServiceEvent extends Model
{

    public function processEvent($wxid, $openid, $message, $cusapp = null)
    {
        switch ($message['Event']) {
            case 'subscribe':
                
                // 1.登记用户
                $user = $cusapp->user->get($openid);
                $user['gh_id'] = $wxid;
                $user['data'] = json_encode($user);
                
                unset($user['subscribe']);
                unset($user['language']);
                unset($user['country']);
                unset($user['remark']);
                unset($user['groupid']);
                unset($user['tagid_list']);
                if (array_key_exists('subscribe_scene', $user)) {
                    unset($user['subscribe_scene']);
                }
                
                Db::name('op_appuser')->insert($user);
                
                if ($message['EventKey']) {
                    $key = str_replace('qrscene_', '', $message['EventKey']);
                    $tiket = $message->Ticket;
                    return $key;
                }
                
                $twk = Db::name('wx_keywords')->where('carjump', 1)
                    ->where('gh_id', $wxid)
                    ->find();
                
                if ($twk == null) {
                    return new Text('感谢您关注我公众号，本公众号每天为您分享产品相关资讯，您可以通过下方菜单体验我们提供的相关服务');
                }
                
                // 先推一个小程序
                // $temp = $cusapp->media->uploadImage('./public_html/minapp.jpg');
                // $res = json_encode($temp);
                // $mediaId = $res['media_id'];
                // $im = new Image($mediaId);
                
                // if ($cusapp != null) {
                // $cusapp->staff->message($im)
                // ->to($openid)
                // ->send();
                // }
                
                // if ($twk['wordorpic'] == 0) {
                // return new Text($twk['description']);
                // }
                
                // 4. 如果是图片就回复
                $nitem = [
                    new NewsItem([
                        'title' => $twk['title'],
                        'description' => $twk['description'],
                        'image' => $twk['showpic'],
                        'url' => $twk['url']
                    ])
                ];
                
                return new News($nitem);
                
                break;
            case 'unsubscribe':
                // 直接删除会员资料
                Db::name('op_appuser')->where('openid', $openid)->delete();
                return 'success';
                break;
            case 'LOCATION': // 服务号功能
                return $message['Latitude'] . ',' . $message['Longitude'] . ',' . $message['Precision'];
                break;
            case 'CLICK':
                $keywords = $message['EventKey'];
                $txt = new ServiceProcess();
                return $txt->processPutTxt($wxid, $openid, $keywords);
                break;
            case 'VIEW':
                $url = $message['EventKey'];
                // return '如果当你看到的信息头像是个小人儿的时候，恭喜你遇到了我们公众号聊天机器人啦！你可以和他随便聊聊，尽量保持情绪克制！如果您有急事儿请拨打我们公众号客服电话或联系客服人员';
                break;
            case 'SCAN': // 服务号功能
                $tiket = $message['Ticket'];
                return $tiket;
                break;
            default:
                // return '如果当你看到的信息头像是个小人儿的时候，恭喜你遇到了我们公众号聊天机器人啦！你可以和他随便聊聊，尽量保持情绪克制！如果您有急事儿请拨打我们公众号客服电话或联系客服人员';
                break;
        }
    }
}