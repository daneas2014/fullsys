<?php
namespace addons\wechat\model;

use think\Db;
use EasyWeChat\Kernel\Messages\Text;
use EasyWeChat\Kernel\Messages\News;
use EasyWeChat\Kernel\Messages\NewsItem;
use think\Model;

class ServiceExam extends Model
{

    /**
     * 加载调查问券或者是答题
     *
     * @param unknown $word            
     * @return boolean|multitype:
     */
    public function load($word)
    {
        if ($word == '答题兑奖') {
            return new Text('点击验证答题兑奖有效性 =》》<a> href="https://www.dmake.cn/a/wechat-exam-valiexam.html">兑奖</a>');
        }
        
        // 新闻解析
        $list = Db::name('wx_exam')->field('id,name,reply_content,showpic')
            ->limit(6)
            ->order('id desc')
            ->select();
        
        if ($list == null) {
            return false;
        }
        
        $exam = array();
        foreach ($list as $item) {
            $nitem = new NewsItem([
                'title' => $item['name'],
                'description' => $item['reply_content'],
                'image' => makeimg($item['showpic']),
                'url' => makeurl($item['id'], 'exam')
            ]);
            
            array_push($exam, $nitem);
        }
        
        return new News($exam);
    }
}