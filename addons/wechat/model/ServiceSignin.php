<?php
namespace addons\wechat\model;

use think\Db;
use think\Model;
use EasyWeChat\Kernel\Messages\News;
use EasyWeChat\Kernel\Messages\NewsItem;

class ServiceSignin extends Model
{

    protected $name = 'op_signin';

    public function index($gh_id, $openid)
    {
        // 1. 是否存在任务，或者任务是否有效
        $signin = $this->where('gh_id', $gh_id)
            ->where('leftnum', '>', 0)
            ->find();
        
        if ($signin == null) {
            return '签到任务已结束';
        }
        
        // 2. 是否已经签到过了
        $logs = Db::name('op_signinlog')->where('openid', $openid)
            ->where('gh_id', $gh_id)
            ->where('signintime', 'between', [
            strtotime(date('Y-m-d 0:0:0')),
            time()
        ])
            ->count();
        
        if ($logs > 0) {
            return '今天您已签到';
        }
        
        // 3.弹出签到链接，这个是为了让签到者再看看广告
        return new News([
            new NewsItem([
                'title' => $signin['name'],
                'description' => $signin['description'],
                'image' => makeimg($signin['showpic']),
                'url' => makeurl($signin['id'] . '&gh_openid=' . $openid, 'signin')
            ])
        ]);
    }

    public function signin($gh_id, $gh_openid, $openid, $nickname, $headimg, $lonlat)
    {
        
        // 1. 是否存在任务，或者任务是否有效
        $signin = $this->where('gh_id', $gh_id)
            ->where('leftnum', '>', 100)
            ->find();
        
        if ($signin == null) {
            return getJsonCode(false, '签到任务已结束，请静待下期签到任务！');
        }
        
        $prise = rand($signin['prisemin'], $signin['prisemax']);
        if ($prise > $signin['leftnum']) {
            return getJsonCode(false, '签到红包已经领取完啦！');
        }
        
        // 2. 是否已经签到过了
        $logs = Db::name('op_signinlog')->where('openid', $openid)
            ->where('gh_id', $gh_id)
            ->where('signintime', 'between', [
            strtotime(date('Y-m-d 0:0:0')),
            time()
        ])
            ->count();
        
        if ($logs > 0) {
            return getJsonCode(false, '今天您已签到');
        }
        
        // 指定位置定位，这个已经是大范围了
        if ($signin['locationlimit']) {
            $signlonlat = explode(',', $signin['locationlimit']);
            $userlonlat = explode(',', $lonlat);
            
            if (substr($signlonlat[0], 0, 7) != substr($userlonlat[0], 0, 7) || substr($signlonlat[1], 0, 7) != substr($userlonlat[1], 0, 7)) {
                return getJsonCode(false, '您不在定位区域');
            }
        }
        
        // 3. 写入签到
        $log['gh_id'] = $gh_id;
        $log['gh_openid'] = $gh_openid;
        $log['openid'] = $openid;
        $log['nickname'] = $nickname;
        $log['headimg'] = $headimg;
        $log['signinid'] = $signin['id'];
        $log['signintime'] = time();
        $log['location'] = $lonlat;
        $log['prised'] = 0;
        
        $start = strtotime(- $signin['limittime'] . 'day');
        
        // 已经连续签到时间
        $sdays = Db::name('op_signinlog')->where('openid', $openid)
            ->where('gh_id', $gh_id)
            ->where('prised', 0)
            ->where('signintime', 'between', [
            strtotime(date("Y-m-d", $start)),
            time()
        ])
            ->count();
        
        $update = null;
        $update['signinnum'] = $signin['signinnum'] + 1;
        
        if (($sdays + 1) == $signin['limittime']) {
            
            $log['prised'] = 1;
            $update['leftnum'] = $signin['leftnum'] - $prise;
            $update['prisednum'] = $signin['prisednum'] + $prise;
        }
        
        $updateresult = $this->where('id', $signin['id'])->update($update);
        
        if ($updateresult) {
            $url = $signin['jumpurl'];
            
            if ($log['prised'] == 1) {
                $url = self::CreateLuckM($gh_id, $openid, $prise, $signin['description']);
                $log['prised'] = $prise;
            }
            
            if (Db::name('op_signinlog')->insert($log)) {
                return getJsonCode($url, '签到成功，看看今天的精彩内容吧！');
            }
            
            return getJsonCode(false, '签到错误，请联系管理员 微信：daneas2014');
        }
        
        return getJsonCode(false, '签到错误，请联系管理员 微信：daneas2014');
    }

    /*
     * 生成普通红包和裂变红包，type=1是普通红包，2是裂变红包
     */
    public function CreateLuckM($gh_id, $openid, $totalfee, $description)
    {
        $order['out_trade_no'] = 'SIGNIN' . time() . rand(0, 9);
        $order['trade_type'] = 'API';
        
        $order['type'] = 1;
        $order['tag'] = '签到红包';
        $order['body'] = '签到红包';
        $order['detail'] = $description;
        $order['total_fee'] = $totalfee;
        
        $order['scopenid'] = $openid;
        $order['gh_id'] = $gh_id;
        $order['overjump'] = 'http://www.tlrbbs.com';
        $order['createtime'] = time();
        
        $m = new OrderModel();
        $orderid = $m->createOrder($order, null);
        $split = WX_AUTHSPLIT;
        $url = wx_get_code('pay' . $split . 'luckmoney' . $split . '1' . $split . $orderid, url('wechat/auth/index'), config('wxas')[WX_APPID]);
        
        $notice = '#注意：当日红包在1个小时内不领取将会被系统收回';
        
        return $url;
    }
}