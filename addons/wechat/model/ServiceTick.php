<?php
namespace addons\wechat\model;

use think\Model;
use think\Db;

class ServiceTick extends Model
{

    /**
     * 创建特惠券项目，写入ticks表
     */
    public function createTicks($data)
    {
        if (Db::name('op_tickts')->insert($data)) {
            
            return getJsonCode(true, '特惠券新增上线');
        }
    }

    /**
     * 创建特惠券项目，写入ticks表
     */
    public function saveTicks($data)
    {
        if (Db::name('op_tickts')->where('id', $data['id'])->update($data)) {
            
            return getJsonCode(true, '特惠券已保存');
        }
    }

    /**
     * 分票出来，这里暂时减少ticks数量，增加tick项目，[openid是分享者的]
     */
    public function createTick($openid, $tickid)
    {
        $tickts = Db::name('op_tickts')->where('id', $tickid)
            ->where('surplus>0')
            ->where('deadtime', '>', time())
            ->find();
        
        if ($tickts == null) {
            return getJsonCode(false, '特惠券失效');
        }
        
        $shop = Db::name('mall')->field('verifyopenid')
            ->where('shopid', $tickts['shopid'])
            ->find();
        
        $tickt['ticksid'] = $tickid;
        $tickt['shareopenid'] = $openid;
        $tickt['checkshopid'] = $tickts['shopid'];
        $tickt['checkopenid'] = $shop['verifyopenid'];
        $tickt['createtime'] = time();
        $tickt['usedtime'] = 0;
        $tickt['code'] = md5(json_encode($tickt));
        $tickt['commission'] = $tickts['worth'] * 0.1;
        
        if (! Db::name('op_tickt')->insert($tickt)) {
            return getJsonCode(false, '分享特惠券失败');
        }
        
        return getJsonCode($tickt['code']);
    }

    /*
     * 接收分享的票据【openid是接受者的】
     */
    public function getTick($openid, $code)
    {
        $db = Db::name('op_tickt');
        
        $tickt = $db->alias('a')
            ->join('think_op_tickts b', 'a.ticksid=b.id')
            ->field('a.*,b.deadtime,b.shopname,b.title')
            ->where('code', $code)
            ->where('b.deadtime', '>', time())
            ->find();
        
        if ($tickt == null) {
            return getJsonCode(false, '特惠券已失效');
        }
        
        if ($tickt['shareopenid'] == $openid) {
            return getJsonCode(false, '不能自己分享券给自己');
        }
        
        if ($db->where('id', $tickt['id'])->update([
            'openid' => $openid
        ])) {
            // 1. 分享券被收到的时候才减少库存
            Db::name('op_tickts')->where('id', $tickt['ticksid'])->setDec('surplus');
            
            // 2. 顾客要接到一条推送
            self::sendReciveMsg($tickt['shopname'], $tickt['title'], $openid, $code, $tickt['deadtime']);
            
            return getJsonCode(true, '您已接受好友赠送的特惠券，VVVIP优惠独享，请速度到店兑换');
        }
        
        return getJsonCode(false, '特惠券领取失败');
    }

    /**
     * 积分兑券
     */
    public function coinTick($openid, $tickid)
    {
        $tickts = Db::name('op_tickts')->where('id', $tickid)
            ->where('surplus>0')
            ->where('deadtime', '>', time())
            ->find();
        
        $coin = Db::name('op_signrank')->where('openid', $openid)->find();
        
        if ($tickts['coin'] > $coin['surplus']) {
            return getJsonCode(false, '您的积分不足哟，签到才能赢积分！');
        }
        
        if ($tickts == null) {
            return getJsonCode(false, '特惠券失效');
        }
        
        $shop = Db::name('mall')->field('openid,verifyopenid')
            ->where('shopid', $tickts['shopid'])
            ->find();
        
        $tickt['ticksid'] = $tickid;
        $tickt['shareopenid'] = '铜梁视窗';
        $tickt['checkshopid'] = $tickts['shopid'];
        $tickt['checkopenid'] = $shop['verifyopenid'];
        $tickt['createtime'] = time();
        $tickt['usedtime'] = 0;
        $tickt['code'] = md5(json_encode($tickt));
        $tickt['commission'] = 0; // 积分换的没有佣金
        
        if (! Db::name('op_tickt')->insert($tickt)) {
            return getJsonCode(false, '分享特惠券失败');
        }
        
        if (Db::name('op_tickts')->where('id', $tickid)->setDec('surplus')) {
            
            // 1. 减少券库存货
            $u['surplus'] = $coin['surplus'] - $tickts['coin'];
            $u['used'] = $coin['used'] + $tickts['coin'];
            Db::name('op_signrank')->where('openid', $openid)->update($u);
            
            // 2. 顾客要接到一条推送，
            self::sendReciveMsg($tickts['shopname'], $tickts['title'], $openid, $tickt['code'], $tickts['deadtime']);
            
            return getJsonCode($tickt['code']);
        }
    }

    /**
     * 使用个人票，将扫码人的openid写上去【openid是扫码者的】
     */
    public function useTick($openid, $code)
    {
        $db = Db::name('op_tickt');
        
        $tickt = $db->alias('a')
            ->join('think_op_tickts b', 'a.ticksid=b.id')
            ->field('a.*')
            ->where('code', $code)
            ->where('b.deadtime', '>', time())
            ->find();
        
        if ($tickt == null) {
            return getJsonCode(false, '特惠券已失效');
        }
        
        if ($tickt['shareopenid'] == $openid) {
            return getJsonCode(false, '不能自己核销自己的券');
        }
        
        $result = $db->where('id', $tickt['id'])->update([
            'usedopenid' => $openid,
            'usedtime' => time()
        ]);
        
        if ($result) {
            return getJsonCode(true, '您已核销特惠券，推荐更多人使用VVVIP优惠独享，获海量推荐佣金');
        }
        
        return getJsonCode(false, '特惠券领取失败');
    }

    /*
     * 获得店铺相关的数据汇总，发了多少券用了多少张券
     */
    public function monthReport($shopid)
    {}

    /*
     * 获得个人当月分红报表,数据要写入另一张表的
     */
    public function returnReport($openid)
    {}

    /*
     * 支付某人的红利
     */
    public function returnPay($returnid)
    {}

    /**
     * 生成券推送消息
     *
     * @param unknown $shopname            
     * @param unknown $ticktitle            
     * @param unknown $openid            
     * @param unknown $code            
     * @param unknown $deadtime            
     */
    private function sendReciveMsg($shopname, $ticktitle, $openid, $code, $deadtime)
    {
        $msg['tid'] = 1;
        $msg['code'] = $code;
        $msg['openid'] = $openid;
        $msg['data'] = '{"first":"尊贵的铜梁生活馆会员，您获得了一张VVVIP优惠券","keyword1":"' . $shopname . '","keyword2":"' . $ticktitle . '","keyword3":"' . date('Y-m-d H:i:s', $deadtime) . '","keyword4":"' . date('Y-m-d H:i:s') . '","remark":"本优惠券不找零、不提现、过期作废！请尽快前往店铺消费"}';
        $msg['createtime'] = time();
        $msg['deadtime'] = $deadtime;
        $msg['sendtime'] = 0;
        $msg['status'] = 0;
        return Db::name('op_msg')->insert($msg);
    }
}