<?php
namespace addons\wechat\model;

use think\Model;
use think\Db;

class ShopModel extends Model
{

    public function saveShop($pai, $pai_pics)
    {
        if (Db::name('mall')->where('tel', 'like', '%' . $pai['tel'] . '%')->count() > 0) {
            return json([
                'code' => - 1,
                'msg' => '本店铺已经被网友抢先拍啦！'
            ]);
        }
        
        $id = Db::name('mall')->insertGetId($pai);
        $DBP = Db::name('mall_pic');
        
        $list = [];
        if (count($pai_pics) > 1) {
            foreach ($pai_pics as $item) {
                $temp['shopid'] = $id;
                $temp['url'] = $item;
                $list[] = $temp;
            }
        } else {
            $temp['shopid'] = $id;
            $temp['url'] = $pai['shopphoto'];
            $list[] = $temp;
        }
        
        $result = $DBP->insertAll($list);
        
        if ($result > 0) {
            
            if (empty(session('SCTEL'))) {
                Db::name('wx_member')->where('openid_d', $pai['openid'])->update([
                    'tel' => $pai['tel']
                ]);
            }
            
            return json([
                'code' => 1,
                'data' => $id,
                'msg' => '感谢您的参与，您的随拍店铺将在内容审核通过后公开公布，审核时间为9：00-18：00'
            ]);
        } else {
            return json([
                'code' => - 1,
                'data' => '',
                'msg' => '网络连通故障中···'
            ]);
        }
    }
}