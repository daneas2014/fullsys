<?php
namespace addons\wechat\model;

use think\Model;
use think\Db;

class PaiModel extends Model
{

    public function savepai($pai, $pai_pics)
    {        
        // 0. 判断是否存在tid就是是否有上级专属的随手拍，如果没有就是0,0就是无主题随手拍
        // 1. 插入wx_pai
        // 2. 插入wx_pai_pic
        
        $id = Db::name('wx_pai')->insertGetId($pai);
        $DBP = Db::name('wx_pai_pic');
        
        $list = [];
        if (count($pai_pics)>1) {
            foreach ($pai_pics as $item) {
                $temp['pid'] = $id;
                $temp['url'] = $item;
                $list[] = $temp;
            }
        } else {
            $temp['pid'] = $id;
            $temp['url'] = $pai['showpic'];
            $list[] = $temp;
        }
        
        $result = $DBP->insertAll($list);
        
        if ($result > 0) {
            
            if(empty(session('SCTEL')))
            {
                Db::name('wx_member')->where('openid_d',$pai['openid'])->update(['tel'=>$pai['tel']]);
            }
            
            return json([
                'code' => 1,
                'data' => $id,
                'msg' => '感谢您的参与，您的随手拍将在内容审核通过后公开公布，审核时间为9：00-18：00'
            ]);
        } else {
            return json([
                'code' => - 1,
                'data' => '',
                'msg' => '网络连通故障中···'
            ]);
        }
    }
}