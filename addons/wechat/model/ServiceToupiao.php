<?php
namespace addons\wechat\model;

use think\Db;
use think\Model;
use app\wechat\model\OrderModel;
use EasyWeChat\Kernel\Messages\Text;
use EasyWeChat\Kernel\Messages\News;
use EasyWeChat\Kernel\Messages\NewsItem;

class ServiceToupiao extends Model
{

    /**
     * 公众号回复快捷投票方法
     *
     * @param unknown $openid            
     * @param unknown $word            
     */
    public function fast($openid, $word, $isid = 0)
    {
        if (rand(1, 10) < 3) {
            return new Text('现在投票人数较多，请稍后再试！');
        }
        
        // 直接回复id投票的方式
        if ($isid !== 0) {
            $idvote = Db::name('wx_voteimg_item')->field('vote_id,id,itemnum')
                ->where('id=' . $isid)
                ->find();
            
            if ($idvote == null) {
                return new Text('没有找到“' . $isid . '”相关投票');
            }
            
            $result = self::vote($idvote['vote_id'], $idvote['itemnum'], $openid, 'fast');
            
            return new Text($result['msg']);
        }
        
        // 调用投票解析的方式 :投票+11+1
        $db = Db::name('wx_voteimg');
        $array = explode('+', $word);
        
        $arraycount = count($array);
        
        // 输出所有投票
        if ($arraycount == 1) {
            
            $list = $db->field('id,action_name,reply_pic,reply_content')
                ->limit(6)
                ->order('id desc')
                ->select();
            
            if ($list == null) {
                return new Text('没有投票活动');
            }
            
            $toupiaos = array();
            foreach ($list as $item) {
                $nitem = new NewsItem([
                    'title' => $item['action_name'],
                    'description' => $item['reply_content'],
                    'image' => makeimg($item['reply_pic']),
                    'url' => makeurl($item['id'], 'vote')
                ]);
                
                array_push($toupiaos, $nitem);
            }
            
            return new News($toupiaos);
        }
        
        // 输出单个投票
        if ($arraycount == 2) {
            
            $item = $db->field('id,action_name,reply_pic,reply_content')
                ->where('id', $array[1])
                ->limit(6)
                ->order('id desc')
                ->find();
            
            if ($item == null) {
                return new Text('没有找到相关投票');
            }
            
            $nitem = new NewsItem([
                'title' => $item['action_name'],
                'description' => $item['reply_content'],
                'image' => makeimg($item['reply_pic']),
                'url' => makeurl($item['id'], 'vote')
            ]);
            
            return new News([
                $nitem
            ]);
        }
        
        // 没有投票
        if ($arraycount > 3) {
            return new Text('没有找到“' . $word . '”相关投票');
        }
        
        // 给某个选项投票
        $voteid = $array[1];
        $voteitemid = $array[2];
        
        // ******************************************************************
        // 这个节点是考量是否关闭微信快速投票，开启的话有点不方便，不开启的话有些哈儿快捷投票投了搞忘网页投票
        // ******************************************************************
        if (1 != 1) {
            $voteitem = Db::name('wx_voteimg_item')->field('vote_title,manifesto,vote_img,vote_id,id')
                ->where([
                'itemnum' => $voteitemid,
                'vote_id' => $voteid
            ])
                ->find();
            
            $nitem = new NewsItem([
                'title' => '我是' . $voteitem['vote_title'] . '，赶紧帮我投票吧',
                'description' => $voteitem['manifesto'],
                'image' => makeimg($voteitem['vote_img']),
                'url' => makeurl('voteid=' . $voteitem['vote_id'] . '&voteitemid=' . $voteitem['id'], 'voteitem')
            ]);
            
            return new News([
                $nitem
            ]);
        }
        
        $result = self::vote($voteid, $voteitemid, $openid, 'fast');
        
        return new Text($result['msg']);
    }

    /**
     * 网页投票
     *
     * @param unknown $voteid            
     * @param unknown $voteitemid            
     * @param unknown $openid            
     * @param unknown $nickname            
     * @param unknown $headimg            
     */
    public function wap($voteid, $voteitemid, $openid, $nickname, $headimg)
    {
        return json(self::vote($voteid, $voteitemid, $openid, 'wap', $nickname, $headimg));
    }

    /**
     * 投票实施逻辑，要改就改这一个方法（单次投票最大sql执行次数7次，平均执行次数6次，最小sql执行次数1次）
     *
     * @param unknown $voteid            
     * @param unknown $voteitemid            
     * @param unknown $openid            
     * @param string $nickname            
     * @param string $headimg            
     * @return multitype:number string
     */
    public function vote($voteid, $voteitemid, $openid, $votetype, $nickname = '', $headimg = '')
    {
        // 1.1 判断是否存在活动
        $voteimg = Db::name('wx_voteimg')->field('id,action_name,start_time,end_time,limit_vote_day,reply_content,vote_type,opentool,luckm')
            ->where('id', $voteid)
            ->find();
        if ($voteimg == null) {
            return [
                'code' => - 1,
                'msg' => "活动不存在，请确认活动编号是否正确！"
            ];
        }
        
        $votestr = "投票";
        if ($voteimg['vote_type'] == 1) {
            $votestr = "";
        }
        
        // 1.2 判断是否存在选项
        $voteitem = null;
        if ($votetype == 'fast') {
            $voteitem = Db::name('wx_voteimg_item')->where([
                'itemnum' => $voteitemid,
                'vote_id' => $voteid
            ])->find();
        } else {
            $voteitem = Db::name('wx_voteimg_item')->where('id', $voteitemid)->find();
        }
        
        if ($voteitem == null) {
            return [
                'code' => - 1,
                'msg' => $votestr . "对象不存在，请确认" . $votestr . "对象是否正确！\n\n感谢您参与活动：" . $voteimg['action_name']
            ];
        }
        
        // 1.3 判断活动是否已经开始
        if ($voteimg['start_time'] > time()) {
            return [
                'code' => - 1,
                'msg' => $votestr . "活动【 " . $voteimg['action_name'] . " 】还未开始， " . $votestr . "开始时间是" . date('Y-m-d', $voteimg['start_time'])
            ];
        }
        
        // 1.4 判断活动是否已经结束
        if ($voteimg['end_time'] < time()) {
            return [
                'code' => - 1,
                'msg' => $votestr . "活动【" . $voteimg['action_name'] . " 】已结束， " . $votestr . "结束时间是" . date('Y-m-d', $voteimg['end_time'])
            ];
        }
        
        // 2. 检查$to是否可以投票
        $records = Db::name('wx_voteimg_record')->alias('a')
            ->join('think_wx_voteimg_users b', 'a.user_id=b.id', 'LEFT')
            ->where('a.vote_id', $voteid)
            ->where('a.vote_day', date('Y-m-d'))
            ->where('b.wecha_id', $openid)
            ->count();
        
        if ($records >= $voteimg['limit_vote_day']) {
            
            if ($voteimg['opentool'] != 1) {
                return [
                    'code' => - 1,
                    'msg' => "今天您已" . $votestr . $voteimg['limit_vote_day'] . "次，感谢您参与 " . $voteimg['action_name']
                ];
            }
            
            // 如果投票的人有额外的票则继续投，否则在这里就应该终止。
            $exnum = Db::name('wx_voteimg_extention')->where('openid_f', $openid)
                ->where('voteid', $voteid)
                ->find();
            
            if ($exnum == null || $exnum['exnum'] < 1) {
                return [
                    'code' => - 1,
                    'msg' => "今天您已" . $votestr . $voteimg['limit_vote_day'] . "次（不包含使用道具投票），感谢您参与 " . $voteimg['action_name']
                ];
            }
            
            Db::name('wx_voteimg_extention')->where('openid_f', $openid)
                ->where('voteid', $voteid)
                ->setDec('exnum', 1);
            
            $voteimg['limit_vote_day'] += $exnum['exnum'];
        }
        
        // 3. 投票人第一次投票要做资料
        $voteuser = Db::name('wx_voteimg_users')->where('wecha_id', $openid)->find();
        if ($voteuser == null) {
            $voteuser['vote_id'] = $voteid;
            $voteuser['wecha_id'] = $openid;
            $voteuser['nick_name'] = $nickname;
            $voteuser['wecha_pic'] = $headimg;
            $voteuser['phone'] = '';
            $voteuser['client_ip'] = '';
            $voteuserid = Db::name('wx_voteimg_users')->insertGetId($voteuser);
            if ($voteuserid < 1) {
                return [
                    'code' => - 1,
                    'msg' => "感谢您的" . $votestr . "，回复您的电话有可能获得神秘大奖01"
                ];
            }
            $voteuser['id'] = $voteuserid;
        }
        
        // 4. 新增投票记录
        $record['vote_id'] = $voteid;
        $record['item_id'] = $voteitemid;
        $record['user_id'] = $voteuser['id'];
        $record['vote_time'] = time();
        $record['vote_day'] = date('Y-m-d');
        $record['vote_type'] = 'fast';
        $recordid = Db::name('wx_voteimg_record')->insertGetId($record);
        if ($recordid < 1) {
            return [
                'code' => - 1,
                'msg' => '感谢您的' . $votestr . '，回复您的电话有可能获得神秘大礼包02'
            ];
        }
        
        // 5. 修改项目票数
        if ($votetype == 'wap') {
            $result = Db::name('wx_voteimg_item')->where('id', $voteitemid)->setInc('vote_count');
        } else {
            $result = Db::name('wx_voteimg_item')->where([
                'itemnum' => $voteitemid,
                'vote_id' => $voteid
            ])->setInc('vote_count');
        }
        
        if (! $result) {
            return [
                'code' => - 1,
                'msg' => $votestr . "异常，请联系电话<a href='tel:18996069665'>18996069665</a>迅速解决"
            ];
        }
        
        $msg = '';
        $vtype = $voteimg['vote_type'] == 1 ? '助力1次' : '投1票';
        $votedcount = $records + 1;
        
        if ($votetype == 'wap') {
            $towx = sprintf('TIPS:关注“铜梁视窗”公众号回复“%s”还能再为好友投%s票哦', $voteitem['id'], $voteimg['limit_vote_day']);
            $msg = sprintf("您已为%s号【%s】%s<br/><br/>今日您共投出%s票<br/><br/><p style='text-align:left;'>%s</p><br/>", $voteitem['itemnum'], $vtype, $voteitem['vote_title'], $votedcount, $voteimg['reply_content']);
            
            if ($votedcount == $voteimg['limit_vote_day']) {
                $priseurl = '<br/><a href="https://www.dmake.cn/a/wechat-Luck-index"><b style="color:RED;font-size:1.2em;">点击此链接抽奖</b></a>';
                $msg = sprintf("您已为%s号【%s】%s<br/><br/>今日您共投出%s票<br/>%s", $voteitem['itemnum'], $vtype, $voteitem['vote_title'], $votedcount, $priseurl);
            } else {
                $msg = $msg . $towx;
            }
        } else {
            
            $luckm = '';
            
            if ($voteimg['luckm'] === 1 && $votedcount == $voteimg['limit_vote_day']) {
                $luckm = '铜梁视窗公众号发送@铜梁最美老板娘豪礼相送@抽取现金红包概率30%'; // self::sendLucky($openid, $voteimg['action_name'], $voteid);
            }
            
            $leftcount = $voteimg['limit_vote_day'] - $votedcount;
            $towapurl = sprintf('/玫瑰 想为ta送出祝福吗？赶紧<a href="https://www.dmake.cn/wechat-vote-item.html?voteid=%s&voteitemid=%s">点击详情</a>，送道具、送祝福、还能再投%s票哦', $voteid, $voteitem['id'], $voteimg['limit_vote_day']);
            $msg = sprintf("%s \n\n/太阳  您已为【%s】活动%s号【%s】%s么么哒\n\n/太阳   今日您共投出%s票，还可以回复关键词投%s票\n\n/太阳  %s\n\n", $luckm, $voteimg['action_name'], $voteitem['itemnum'], $voteitem['vote_title'], $vtype, $votedcount, $leftcount, $voteimg['reply_content']);
            
            $msg = $msg . $towapurl;
        }
        
        return [
            'code' => 1,
            'msg' => $msg
        ];
    }

    /**
     * 给投票者加额外的票数
     *
     * @param unknown $voteid            
     * @param unknown $openid            
     * @param unknown $exnum            
     */
    public function addexnum($voteid, $openid, $exnum)
    {
        $data['openid_f'] = $openid;
        $data['exnum'] = $exnum;
        $data['voteid'] = $voteid;
        
        return Db::name('wx_vote_extention')->insert($data);
    }

    /*
     * 投票完了就已经决定了她有没有奖，其他抽奖只是个噱头罢了
     */
    public function sendLucky($openid, $votename, $voteid)
    {
        $fee = rand(0, 120);
        $order['out_trade_no'] = 'LUCKEY' . time() . $voteid;
        $order['trade_type'] = 'API';
        $order['type'] = 1;
        $order['tag'] = $votename;
        $order['body'] = $votename . '投票抽奖';
        $order['detail'] = '您在投票活动中意外中了' . ($fee / 100) . '元';
        $order['total_fee'] = $fee;
        $order['scopenid'] = session('SCOPENID');
        $order['overjump'] = url('index') . '?voteid=' . $voteid;
        $order['createtime'] = time();
        $order['gh_id'] = 'gh_7bf948cbfdb0'; // 这个活动只能从铜梁视窗出
        
        if ($fee < 100) {
            $order['total_fee'] = 0;
        }
        
        $m = new OrderModel();
        $orderid = $m->createOrder($order, null);
        $split = WX_AUTHSPLIT;
        $url = wx_get_code('pay' . $split . 'luckmoney' . $split . '1' . $split . $orderid, addon_url('wechat://auth/index'), config('wxauth')['authwx']);
        return '<a href="' . $url . '">#关注“铜梁生活馆”公众号后点此链接领红包#</a>';
    }
}