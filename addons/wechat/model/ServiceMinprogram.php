<?php
namespace addons\wechat\model;

use EasyWeChat\Factory;
use think\Db;
use function GuzzleHttp\json_encode;

/**
 * 专门用来小程序发消息
 *
 * @author Daneas
 *        
 */
class ServiceMinprogram
{

    public function sendAd()
    {
        $list = Db::name('Customeropenidoauth')->alias('a')
            ->join('Customer b', 'a.CustomerId=b.Id', 'inner')
            ->where('OAuthType', 2)
            ->field('a.OAuthOpenId as openid,b.UserName')
            ->select();
        
        foreach ($list as $l) {
            $data['page'] = "/pages/subscribe/msg?dataid=" . $l['dataid'].'&datatype='.$l['datatype'];
            $data['openid'] = $l['openid'];
            $data['formid'] = '';
            $data['data'] = [
                'keyword1' => $l['title'],
                'keyword2' => $l['description'],
                'keyword3' => $l['username'],
                'keyword4' => $l['remark']
            ];
            
            $this->sendTemplatemsg('3715UdGnfzKgpgPtgBFgFQtQLfeD3yBoLNCMG3Smlmc', $data);
        }
    }

    /**
     * 订阅通知
     */
    public function sendSubscribe($list)
    {
        $i=0;
        
        foreach ($list as $l) {
            $data['page'] = "/pages/subscribe/msg?dataid=" . $l['dataid'].'&datatype='.$l['datatype'].'&id='.$l['id'];
            $data['openid'] = $l['openid'];
            $data['formid'] = $l['datatype'].'subscribe';
            $data['data'] = [
                'keyword1' => $l['title'],
                'keyword2' => $l['description'],
                'keyword3' => $l['username'],
                'keyword4' => $l['remark']
            ];
            
            $this->sendTemplatemsg('3715UdGnfzKgpgPtgBFgFQtQLfeD3yBoLNCMG3Smlmc', $data);
            
            $i++;
        }
        
        return $i;
    }

    /**
     * 生成小程序码
     * 
     * @param unknown $path 保存文件夹
     * @param unknown $name  保存文件名
     */
    public function makeMinCode($path, $name, $width = 250)
    {
        $minprogram = config('wxauth')['minprogram'];
        
        $config = [
            'app_id' => $minprogram['app_id'],
            'secret' => $minprogram['secret'],
            'response_type' => 'array',
        ];
        
        $app = Factory::miniProgram($config);
        
        $response = $app->app_code->get($path, [
            'width' => $width,
            'line_color' => [
                'r' => 0,
                'g' => 0,
                'b' => 0
            ]
        ]);
        
        if ($response instanceof \EasyWeChat\Kernel\Http\StreamResponse) {
            $dir = '/attachment/mincode/' . date('y');
            $filename = $response->saveAs('.' . $dir, $name);
            return $dir .'/'. $filename;
        }
        
        return false;
    }

    /**
     * 发送模板消息，注意data里面要带的6个值
     *
     * @param unknown $templateid            
     * @param unknown $data            
     * @return boolean
     */
    public function sendTemplatemsg($templateid, $data)
    {
        $minprogram = config('wxauth')['minprogram'];
        
        $config = [
            'app_id' => $minprogram['app_id'],
            'secret' => $minprogram['secret'],
            'response_type' => 'array',
            'log' => [
                'level' => 'debug',
                'file' => __DIR__ . '/wechat.log'
            ]
        ];
        
        $app = Factory::miniProgram($config);
        
        $res = $app->template_message->send([
            'touser' => $data['openid'],
            'template_id' => $templateid,
            'page' => $data['page'],
            'form_id' => $data['formid'],
            'data' => $data['data']
        ]);
        
        debuggerfile(json_encode($res),'minprogram');
        
        if ($res['errCode'] == 0) {
            return true;
        }
        
        return false;
    }
}