<?php
namespace addons\wechat\model;

use think\Model;
use think\Db;

class OrderModel extends Model
{
    // id out_trade_no 给微信的订单号 trade_type body 订单名称 detail 订单详情 total_fee 分 openid createtime return_code prepay_id 微信返回的订单号 paid_at支付时间
    // id orderid name num 数量 saleprice 分 realprice 分 total_fee 分 detail 其他描述
    public function saveOrder($order)
    {
        return Db::name('wx_order')->where('id', $order['id'])->update($order);
    }

    public function getOrder($orderid, $outtradeno = null)
    {
        if ($orderid > 0) {
            return Db::name('wx_order')->where('id', $orderid)->find();
        } else {
            return Db::name('wx_order')->where('out_trade_no', $outtradeno)->find();
        }
    }

    public function getOrderdetail($orderid)
    {
        return Db::name('wx_order_detail')->where('orderid', $orderid)->select();
    }

    public function createOrder($order, $items)
    {
        $orderid = Db::name('wx_order')->insertGetId($order);
        
        if (count($items) > 0) {
            
            $saveitems = [];
            foreach ($items as $item) {
                $item['orderid'] = $orderid;
                $saveitems[] = $item;
            }
            
            Db::name('wx_order_detail')->insertAll($saveitems);
        }
        
        return $orderid;
    }

    public function processCallback($order)
    {
        // 如果是投票道具订单，那么我们就要找出要给那一个人在额外票仓里面准备翻倍票数
        if ($order['type'] == 0) {
            
            $gift = Db::name('wx_voteimg_gift')->alias('a')
                ->join('think_wx_voteimg b', 'a.voteid=b.id')
                ->field('a.*,b.limit_vote_day')
                ->where('a.orderid', $order['id'])
                ->find();
            
            $exten['openid_f'] = $gift['openid'];
            $exten['voteid'] = $gift['voteid'];
            $exten['exnum'] = $gift['giftid'] * $gift['limit_vote_day'];
            
            $dbextention = Db::name('wx_voteimg_extention');
            
            $obj = $dbextention->where([
                'openid_f' => $gift['openid'],
                'voteid' => $gift['voteid']
            ])->find();
            
            if ($obj == null) {
                return $dbextention->insert($exten);
            } else {
                $exten['exnum'] = $exten['exnum'] + $obj['exnum'];
                return $dbextention->where('id', $obj['id'])->update($exten);
            }
        }
    }

    public function getLucklog()
    {
        return Db::name('wx_order')->where('type', '>', 0)
            ->where('total_fee', '>', 0)
            ->where('paid_at', '>', 0)
            ->where('nickname', '<>', '')
            ->limit(20)
            ->order('id desc')
            ->select();
    }
}