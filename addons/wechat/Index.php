<?php
namespace addons\wechat; // 注意命名空间规范

use think\Addons;
use think\Db;

/**
 * 淘宝客插件
 * @author byron sampson
 */
class Index extends Addons// 需继承think\addons\Addons类

{
	// 该插件的基础信息
	public $info = [
		'name' => 'wechat', // 插件标识
		'title' => '微信拓展', // 插件名称
		'description' => '微信拓展插件', // 插件简介
		'status' => 0, // 状态
		'author' => 'daneas',
		'version' => 'v5',
		'manage' => 'wechat://admin/index',
	];

	/**
	 * 插件安装方法
	 * @return bool
	 */
	public function install() {
		$res = Db::execute(initAddon($this->info));

		if (!$res) {
			return false;
		}

		// 公共部分
		$sql = file_get_contents(__DIR__ . "/data.sql");
		$a = explode(";\r\n", $sql); //根据";\r\n"条件对数据库中分条执行
		$total = count($a);
		for ($i = 0; $i < $total; $i++) {
			$res = Db::query($a[$i]);
		}

		if ($res <= 0) {
			return false;
		}

		//随手拍
		$sql = file_get_contents(__DIR__ . "/data_pai.sql");
		$a = explode(";\r\n", $sql); //根据";\r\n"条件对数据库中分条执行
		$total = count($a);
		for ($i = 0; $i < $total; $i++) {
			$res = Db::query($a[$i]);
		}

		if ($res <= 0) {
			return false;
		}

		//投票
		$sql = file_get_contents(__DIR__ . "/data_vote.sql");
		$a = explode(";\r\n", $sql); //根据";\r\n"条件对数据库中分条执行
		$total = count($a);
		for ($i = 0; $i < $total; $i++) {
			$res = Db::query($a[$i]);
		}
		return $res > 0 ? true : false;
	}

	/**
	 * 插件卸载方法
	 * @return bool
	 */
	public function uninstall() {
		$sql = "DROP TABLE IF EXISTS wx_attribute;
        DROP TABLE IF EXISTS wx_custom_menu;
        DROP TABLE IF EXISTS wx_exam;
        DROP TABLE IF EXISTS wx_exam_item;
        DROP TABLE IF EXISTS wx_exam_result;
        DROP TABLE IF EXISTS wx_keywords;
        DROP TABLE IF EXISTS wx_luckcode;
        DROP TABLE IF EXISTS wx_luckcodelog;
        DROP TABLE IF EXISTS wx_member;
        DROP TABLE IF EXISTS wx_menu;
        DROP TABLE IF EXISTS wx_order;
        DROP TABLE IF EXISTS wx_order_detail;
        DROP TABLE IF EXISTS wx_pai;
        DROP TABLE IF EXISTS wx_pai_comment;
        DROP TABLE IF EXISTS wx_pai_pic;
        DROP TABLE IF EXISTS wx_pai_prize;
        DROP TABLE IF EXISTS wx_pai_prizelog;
        DROP TABLE IF EXISTS wx_pai_recode;
        DROP TABLE IF EXISTS wx_pai_topic;
        DROP TABLE IF EXISTS wx_pai_type;
        DROP TABLE IF EXISTS wx_voteimg;
        DROP TABLE IF EXISTS wx_voteimg_banner;
        DROP TABLE IF EXISTS wx_voteimg_bottom;
        DROP TABLE IF EXISTS wx_voteimg_comment;
        DROP TABLE IF EXISTS wx_voteimg_extention;
        DROP TABLE IF EXISTS wx_voteimg_gift;
        DROP TABLE IF EXISTS wx_voteimg_item;
        DROP TABLE IF EXISTS wx_voteimg_menus;
        DROP TABLE IF EXISTS wx_voteimg_prize;
        DROP TABLE IF EXISTS wx_voteimg_record;
        DROP TABLE IF EXISTS wx_voteimg_sponor;
        DROP TABLE IF EXISTS wx_voteimg_users;
        delete from addons where name='wechat';";

		$a = explode(";\r\n", $sql); //根据";\r\n"条件对数据库中分条执行
		$total = count($a);
		for ($i = 0; $i < $total; $i++) {
			$res = Db::query($a[$i]);
		}
		return $res > 0 ? true : false;
	}

	/**
	 * 实现的testhook钩子方法
	 * @return mixed
	 */
	public function wehook($param) {
		// 调用钩子时候的参数信息
		print_r($param);
		// 当前插件的配置信息，配置信息存在当前目录的config.php文件中，见下方
		print_r($this->getConfig());
		// 可以返回模板，模板文件默认读取的为插件目录中的文件。模板名不能为空！
		return $this->fetch('info');
	}

}
