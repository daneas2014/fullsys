-- ----------------------------
-- Table structure for wx_attribute
-- ----------------------------
DROP TABLE IF EXISTS `wx_attribute`;
CREATE TABLE `wx_attribute`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '字段名',
  `title` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '字段注释',
  `field` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '字段定义',
  `type` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '数据类型',
  `value` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '字段默认值',
  `remark` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '备注',
  `is_show` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '是否显示',
  `extra` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '参数',
  `model_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '模型id',
  `model_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `is_must` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否必填',
  `status` tinyint(2) NOT NULL DEFAULT 0 COMMENT '状态',
  `update_time` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '更新时间',
  `create_time` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
  `validate_rule` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `validate_time` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `error_info` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `validate_type` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `auto_rule` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `auto_time` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `auto_type` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `model_id`(`model_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 11395 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '模型属性表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wx_attribute
-- ----------------------------
INSERT INTO `wx_attribute` VALUES (11371, 'sort', '排序号', 'tinyint(4) NULL ', 'num', '0', '数值越小越靠前', 1, '', 0, 'custom_menu', 0, 1, 1394523288, 1394519175, '', 3, '', 'regex', '', 3, 'function');
INSERT INTO `wx_attribute` VALUES (11372, 'pid', '一级菜单', 'int(10) NULL', 'select', '0', '如果是一级菜单，选择“无”即可', 1, '0:无', 0, 'custom_menu', 0, 1, 1416810279, 1394518930, '', 3, '', 'regex', '', 3, 'function');
INSERT INTO `wx_attribute` VALUES (11373, 'title', '菜单名', 'varchar(50) NOT NULL', 'string', '', '可创建最多 3 个一级菜单，每个一级菜单下可创建最多 5 个二级菜单。编辑中的菜单不会马上被用户看到，请放心调试。', 1, '', 0, 'custom_menu', 1, 1, 1408951570, 1394518988, '', 3, '', 'regex', '', 3, 'function');
INSERT INTO `wx_attribute` VALUES (11374, 'keyword', '关联关键词', 'varchar(100) NULL', 'string', '', '', 1, '', 0, 'custom_menu', 0, 1, 1464331802, 1394519054, '', 3, '', 'regex', '', 3, 'function');
INSERT INTO `wx_attribute` VALUES (11375, 'url', '关联URL', 'varchar(255) NULL ', 'string', '', '', 1, '', 0, 'custom_menu', 0, 1, 1394519090, 1394519090, '', 3, '', 'regex', '', 3, 'function');
INSERT INTO `wx_attribute` VALUES (11376, 'token', 'Token', 'varchar(255) NULL', 'string', '', '', 0, '', 0, 'custom_menu', 0, 1, 1394526820, 1394526820, '', 3, '', 'regex', 'get_token', 1, 'function');
INSERT INTO `wx_attribute` VALUES (11377, 'type', '类型', 'varchar(30) NULL', 'bool', 'click', '', 1, 'click:点击推事件|keyword@show,url@hide\r\nview:跳转URL|keyword@hide,url@show\r\nscancode_push:扫码推事件|keyword@show,url@hide\r\nscancode_waitmsg:扫码带提示|keyword@show,url@hide\r\npic_sysphoto:弹出系统拍照发图|keyword@show,url@hide\r\npic_photo_or_album:弹出拍照或者相册发图|keyword@show,url@hide\r\npic_weixin:弹出微信相册发图器|keyword@show,url@hide\r\nlocation_select:弹出地理位置选择器|keyword@show,url@hide\r\nnone:无事件的一级菜单|keyword@hide,url@hide', 0, 'custom_menu', 0, 1, 1416812039, 1416810588, '', 3, '', 'regex', '', 3, 'function');
INSERT INTO `wx_attribute` VALUES (11378, 'from_type', '配置动作', 'char(50) NULL', 'select', '-1', '', 1, '0:站内信息|keyword@hide,url@hide,type@hide,sucai_type@hide,addon@show,jump_type@show\r\n1:站内素材|keyword@hide,url@hide,type@hide,sucai_type@show,addon@hide,jump_type@hide\r\n9:自定义|keyword@show,url@hide,type@show,addon@hide,sucai_type@hide,jump_type@hide\r\n-1:请选择|keyword@hide,url@hide,type@hide,addon@hide,sucai_type@hide,jump_type@hide', 0, 'custom_menu', 0, 1, 1447318552, 1447208677, '', 3, '', 'regex', '', 3, 'function');
INSERT INTO `wx_attribute` VALUES (11379, 'addon', '选择插件', 'char(50) NULL', 'select', '0', '', 1, '0:请选择', 0, 'custom_menu', 0, 1, 1447208750, 1447208750, '', 3, '', 'regex', '', 3, 'function');
INSERT INTO `wx_attribute` VALUES (11380, 'target_id', '选择内容', 'int(10) NULL', 'num', '', '', 4, '0:请选择', 0, 'custom_menu', 0, 1, 1447208825, 1447208825, '', 3, '', 'regex', '', 3, 'function');
INSERT INTO `wx_attribute` VALUES (11381, 'sucai_type', '素材类型', 'varchar(50) NULL', 'material', '', '', 1, '', 0, 'custom_menu', 0, 1, 1464357378, 1447208890, '', 3, '', 'regex', '', 3, 'function');
INSERT INTO `wx_attribute` VALUES (11382, 'jump_type', '推送类型', 'char(10) NULL', 'radio', '0', '', 1, '1:URL|keyword@hide,url@show\r\n0:关键词|keyword@show,url@hide', 0, 'custom_menu', 0, 1, 1447208981, 1447208981, '', 3, '', 'regex', '', 3, 'function');

-- ----------------------------
-- Table structure for wx_custom_menu
-- ----------------------------
DROP TABLE IF EXISTS `wx_custom_menu`;
CREATE TABLE `wx_custom_menu`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NULL DEFAULT NULL,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `value` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `appid` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `pagepath` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` int(11) NULL DEFAULT NULL,
  `sort` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 40 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wx_custom_menu
-- ----------------------------
INSERT INTO `wx_custom_menu` VALUES (7, 32, '附近的店', 'miniprogram', 'http://mp.weixin.qq.com', 'wx9553756d97d8f461', 'page/message/index', 1, 0);
INSERT INTO `wx_custom_menu` VALUES (13, 0, '互动馆', 'miniprogram', 'http://mp.weixin.qq.com', 'wx9553756d97d8f461', 'page/discover/discover', 1, 1);
INSERT INTO `wx_custom_menu` VALUES (34, 32, '超级折扣', 'miniprogram', 'http://mp.weixin.qq.com', 'wx6c309364fa3bc0b8', 'pages/tk/index', 1, 50);
INSERT INTO `wx_custom_menu` VALUES (17, 0, '车生活', 'miniprogram', 'http://mp.weixin.qq.com', 'wx735e6051a9c5a8f7', 'page/message/index', 1, 3);
INSERT INTO `wx_custom_menu` VALUES (39, 32, '年货节', 'view', 'http://www.maimaitl.com/m', '', '', 1, 50);
INSERT INTO `wx_custom_menu` VALUES (32, 0, '生活馆', 'view', '铜梁生活', '', '', 1, 0);

-- ----------------------------
-- Table structure for wx_exam
-- ----------------------------
DROP TABLE IF EXISTS `wx_exam`;
CREATE TABLE `wx_exam`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `keywords` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `description` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `createtime` int(11) NOT NULL,
  `endtime` int(11) NOT NULL,
  `sendmsg` int(11) NOT NULL DEFAULT 0,
  `showpic` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `examor` int(11) NOT NULL DEFAULT 0 COMMENT '是考试还是调查问卷',
  `mark` int(11) NOT NULL COMMENT '及格线',
  `havegit` int(11) NOT NULL COMMENT '是否有礼品',
  `reply_content` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '考试，选择题' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wx_exam
-- ----------------------------
INSERT INTO `wx_exam` VALUES (1, '铜梁视窗微信调查问卷模块测试', '调查问卷', '  \r\n									    \r\n									    \r\n									    \r\n									    \r\n									    \r\n									  <p>  \r\n									    \r\n									    \r\n									  铜梁视窗微信调查问卷模块测试，旨在打造通用的网上答题、调查问卷，主办方可以设置问卷后可以发送领取礼品的短信。</p>																																																						', 1494987270, 1514736000, 1, '', 0, 60, 1, '铜梁视窗微信调查问卷模块测试，旨在打造通用的网上答题、调查问卷，主办方可以设置问卷后可以发送领取礼品');

-- ----------------------------
-- Table structure for wx_exam_item
-- ----------------------------
DROP TABLE IF EXISTS `wx_exam_item`;
CREATE TABLE `wx_exam_item`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `examid` int(11) NOT NULL,
  `title` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `choose1` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `choose2` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `choose3` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `choose4` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `rightanswer` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '选择题' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wx_exam_item
-- ----------------------------
INSERT INTO `wx_exam_item` VALUES (1, 1, '你能不能坦然接受别人说你是“二货”', '是', '不是', '不确定', '肯定是', 'A');
INSERT INTO `wx_exam_item` VALUES (2, 1, '静夜思是谁写的', '王维', '孟浩然', '李清照', '李白', 'D');
INSERT INTO `wx_exam_item` VALUES (3, 1, '鹿晗是个什么东西', '名词', '名字<img src=\"http://img1.imgtn.bdimg.com/it/u=3783329168,412735847&fm=26&gp=0.jpg\" width=\"200\" />', '名号', '动物', 'B');
INSERT INTO `wx_exam_item` VALUES (4, 1, '迪丽热巴是哪儿的人', '蒙古人', '山西人', '新疆人', '重庆人', 'C');
INSERT INTO `wx_exam_item` VALUES (5, 1, '圆饼红绿灯怎么转', '看情况右转', '直接忽略', '红灯停绿灯行', '减速慢行', 'A');

-- ----------------------------
-- Table structure for wx_exam_result
-- ----------------------------
DROP TABLE IF EXISTS `wx_exam_result`;
CREATE TABLE `wx_exam_result`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `examid` int(11) NOT NULL,
  `openid` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '答题者的微信id',
  `username` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `phone` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `mid` int(11) NOT NULL,
  `score` int(11) NOT NULL COMMENT '分数',
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '考卷详情:json',
  `createtime` int(11) NOT NULL,
  `code` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '领奖代码',
  `isvaliable` int(11) NOT NULL COMMENT '是否兑了奖',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 18 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '考试人员和结果' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wx_exam_result
-- ----------------------------
INSERT INTO `wx_exam_result` VALUES (9, 1, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', '何跃', '13983975714', 0, 100, '[{\"itemid\":1,\"selected\":\"A\"},{\"itemid\":2,\"selected\":\"D\"},{\"itemid\":3,\"selected\":\"B\"},{\"itemid\":4,\"selected\":\"C\"},{\"itemid\":5,\"selected\":\"A\"}]', 1494987409, '1494987409', 1);
INSERT INTO `wx_exam_result` VALUES (10, 1, 'o73cGwSTPa07-i_C6ngzEcZ8kpEA', '呵呵', '18996069665', 0, 100, '[{\"itemid\":1,\"selected\":\"A\"},{\"itemid\":2,\"selected\":\"D\"},{\"itemid\":3,\"selected\":\"B\"},{\"itemid\":4,\"selected\":\"C\"},{\"itemid\":5,\"selected\":\"A\"}]', 1494987984, '1494987984', 1);
INSERT INTO `wx_exam_result` VALUES (11, 1, 'o73cGwZeKw-lCCv9c9YT2updN9cY', '测试', '测试', 0, 40, '[{\"itemid\":1,\"selected\":\"C\"},{\"itemid\":2,\"selected\":\"C\"},{\"itemid\":3,\"selected\":\"B\"},{\"itemid\":4,\"selected\":\"A\"},{\"itemid\":5,\"selected\":\"A\"}]', 1494988659, '', 0);
INSERT INTO `wx_exam_result` VALUES (12, 1, 'o73cGwflBGZyE_FYzV2Tn9lizSBM', '周周', '18696785292', 0, 100, '[{\"itemid\":1,\"selected\":\"A\"},{\"itemid\":2,\"selected\":\"D\"},{\"itemid\":3,\"selected\":\"B\"},{\"itemid\":4,\"selected\":\"C\"},{\"itemid\":5,\"selected\":\"A\"}]', 1494991685, '1494991685', 1);
INSERT INTO `wx_exam_result` VALUES (13, 1, 'o73cGwWq-1GoNrvAGzyiWR_buz8Q', 'wwws', '15829475652', 0, 20, '[{\"itemid\":1,\"selected\":\"B\"},{\"itemid\":2,\"selected\":\"C\"},{\"itemid\":3,\"selected\":\"C\"},{\"itemid\":4,\"selected\":\"C\"},{\"itemid\":5,\"selected\":\"C\"}]', 1495610843, '', 0);
INSERT INTO `wx_exam_result` VALUES (14, 1, 'o73cGwfamMlraReT6U1vu0BOH__U', '谢真全', '15923163679', 0, 20, '[{\"itemid\":1,\"selected\":\"B\"},{\"itemid\":2,\"selected\":\"A\"},{\"itemid\":3,\"selected\":\"B\"},{\"itemid\":4,\"selected\":\"B\"},{\"itemid\":5,\"selected\":\"C\"}]', 1495773349, '', 0);
INSERT INTO `wx_exam_result` VALUES (15, 1, 'oYCDIt2DvUavx2N8nsGeyTB8sTc4', 'asdf', '13555555555', 0, 100, '[{\"itemid\":1,\"selected\":\"A\"},{\"itemid\":2,\"selected\":\"D\"},{\"itemid\":3,\"selected\":\"B\"},{\"itemid\":4,\"selected\":\"C\"},{\"itemid\":5,\"selected\":\"A\"}]', 1499411282, '1499411282', 0);
INSERT INTO `wx_exam_result` VALUES (16, 1, 'oYCDItw53NmEuoCuUUuO2D6mVM3k', '哈哈哈', '18996069665', 0, 100, '[{\"itemid\":1,\"selected\":\"A\"},{\"itemid\":2,\"selected\":\"D\"},{\"itemid\":3,\"selected\":\"B\"},{\"itemid\":4,\"selected\":\"C\"},{\"itemid\":5,\"selected\":\"A\"}]', 1499411422, '1499411422', 1);
INSERT INTO `wx_exam_result` VALUES (17, 1, 'oYCDIt_uD8NjNmlsOCoYMeVjOqvo', 'tfff', '13777777777', 0, 20, '[{\"itemid\":1,\"selected\":\"A\"},{\"itemid\":2,\"selected\":\"C\"},{\"itemid\":3,\"selected\":\"A\"},{\"itemid\":4,\"selected\":\"B\"},{\"itemid\":5,\"selected\":\"C\"}]', 1499412694, '', 0);

-- ----------------------------
-- Table structure for wx_keywords
-- ----------------------------
DROP TABLE IF EXISTS `wx_keywords`;
CREATE TABLE `wx_keywords`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gh_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `keyword` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `wordorpic` int(11) NULL DEFAULT NULL,
  `title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `showpic` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `carjump` int(11) NULL DEFAULT NULL,
  `sort` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wx_keywords
-- ----------------------------
INSERT INTO `wx_keywords` VALUES (1, 'gh_7bf948cbfdb0', '', 0, '公众号关注回复', '', '回复关键词将为您推荐相关信息。\r\n                    \r\n1.推广店铺点击菜单“互动馆=》联盟”\r\n2.找工作点击菜单“互动馆=》招聘”\r\n3.租房子请点击菜单“互动馆=》房产”\r\n4.发布个人需求请点击“生活馆=》小程序”\r\n\r\n订阅、参加铜梁优惠折扣信息请关注公众号“铜梁生活馆”\r\n                    \r\n商务合作请拨打电话18996069665（微信同号）', 1, 0);
INSERT INTO `wx_keywords` VALUES (2, 'gh_7bf948cbfdb0', '', 1, '因为有你“柚”惑我心 龙韵果乡形象大使选拔', 'http://source.5atl.com/attachment/images/201710/09/1507539652.jpg', '因为有你“柚”惑我心，上传你和柚子的合影参与选拔，有机会当免费农场主送柚子树三棵！', 0, 0);
INSERT INTO `wx_keywords` VALUES (4, 'gh_7bf948cbfdb0', '阅读', 0, '阅读是个好习惯', '', '阅读是个好习惯，访问http://www.5atl.com/wechat-read-index.html即可体验', 0, 0);
INSERT INTO `wx_keywords` VALUES (5, 'gh_7bf948cbfdb0', '魅力老板娘', 1, '魅力老板娘投票抢千元现金大奖+10万推广基金', 'http://source.5atl.com/attachment/images/20180906/42a2d0ad463353c2292a432721369b03.jpg', '报名即送美容大礼包，前三名现金大奖，前50都能获得定制推广！', 0, 1);

-- ----------------------------
-- Table structure for wx_luckcode
-- ----------------------------
DROP TABLE IF EXISTS `wx_luckcode`;
CREATE TABLE `wx_luckcode`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gh_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `code` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '红包口令',
  `leftnum` int(11) NOT NULL DEFAULT 0 COMMENT '剩余红包数量',
  `createtime` int(11) NOT NULL,
  `starttime` int(11) NOT NULL COMMENT '口令开始时间',
  `endtime` int(11) NOT NULL COMMENT '口令失效时间',
  `totalnum` int(11) NOT NULL DEFAULT 0 COMMENT '总计',
  `maxnum` int(11) NOT NULL DEFAULT 3 COMMENT '最多可以玩几次',
  `rate` int(11) NOT NULL COMMENT '整数0-100',
  `jumpurl` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '非现金红包跳转链接',
  `jumpword` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '跳转词',
  `othercode` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '如果这次没有发购，那么再发一个',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 44 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '口令红包' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wx_luckcode
-- ----------------------------
INSERT INTO `wx_luckcode` VALUES (42, 'gh_7bf948cbfdb0', '铜梁视窗再来一波红包', 0, 1538396575, 0, 1577836800, 20, 1, 20, 'https://mp.weixin.qq.com/s/9XRIAjWUl3ysNGkA_OuvQw', '10月4日土桥原生露营节报名啦', '');
INSERT INTO `wx_luckcode` VALUES (41, 'gh_7bf948cbfdb0', '10月4日土桥原生露营节报名啦', 4, 1538367401, 0, 1577836800, 30, 1, 20, 'https://mp.weixin.qq.com/s/D-QuqccM2TAfHHasnhcn9A', '10月4日土桥原生露营节报名啦', '铜梁视窗再来一波红包');
INSERT INTO `wx_luckcode` VALUES (43, 'gh_6c48739f46f6', '重启', 1, 1652251765, 1577836800, 1893456000, 1000, 10, 10, 'https://www.dmake.cn/a/taoke/index/index', '我胡寒山又回来了', '');

-- ----------------------------
-- Table structure for wx_luckcodelog
-- ----------------------------
DROP TABLE IF EXISTS `wx_luckcodelog`;
CREATE TABLE `wx_luckcodelog`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gh_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `openid` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `code` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `createtime` int(11) NOT NULL,
  `money` tinyint(4) NOT NULL DEFAULT 0 COMMENT '幸运红包是钱还是其他',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wx_luckcodelog
-- ----------------------------
INSERT INTO `wx_luckcodelog` VALUES (1, 'gh_6c48739f46f6', 'oIYiD1njS6X6qJWB7ouwvYx0NZ3c', '重启', 1652251925, 1);
INSERT INTO `wx_luckcodelog` VALUES (2, 'gh_6c48739f46f6', 'oIYiD1njS6X6qJWB7ouwvYx0NZ3c', '重启', 1652251929, 0);
INSERT INTO `wx_luckcodelog` VALUES (3, 'gh_6c48739f46f6', 'oIYiD1njS6X6qJWB7ouwvYx0NZ3c', '重启', 1652251931, 0);
INSERT INTO `wx_luckcodelog` VALUES (4, 'gh_6c48739f46f6', 'oIYiD1njS6X6qJWB7ouwvYx0NZ3c', '重启', 1652251935, 0);
INSERT INTO `wx_luckcodelog` VALUES (5, 'gh_6c48739f46f6', 'oIYiD1njS6X6qJWB7ouwvYx0NZ3c', '重启', 1652251939, 0);
INSERT INTO `wx_luckcodelog` VALUES (6, 'gh_6c48739f46f6', 'oIYiD1njS6X6qJWB7ouwvYx0NZ3c', '重启', 1652251941, 0);
INSERT INTO `wx_luckcodelog` VALUES (7, 'gh_6c48739f46f6', 'oIYiD1njS6X6qJWB7ouwvYx0NZ3c', '重启', 1652251943, 0);
INSERT INTO `wx_luckcodelog` VALUES (8, 'gh_6c48739f46f6', 'oIYiD1njS6X6qJWB7ouwvYx0NZ3c', '重启', 1652251960, 0);
INSERT INTO `wx_luckcodelog` VALUES (9, 'gh_6c48739f46f6', 'oIYiD1njS6X6qJWB7ouwvYx0NZ3c', '重启', 1652251962, 0);
INSERT INTO `wx_luckcodelog` VALUES (10, 'gh_6c48739f46f6', 'oIYiD1njS6X6qJWB7ouwvYx0NZ3c', '重启', 1652251964, 0);

-- ----------------------------
-- Table structure for wx_member
-- ----------------------------
DROP TABLE IF EXISTS `wx_member`;
CREATE TABLE `wx_member`  (
  `mid` int(11) NOT NULL AUTO_INCREMENT,
  `subscribe` int(11) NOT NULL COMMENT '是否关注',
  `openid_d` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '订阅号的id',
  `openid_f` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '服务号的id',
  `nickname` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `sex` int(11) NOT NULL,
  `language` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `city` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `province` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `country` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `headimgurl` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `subscribe_time` int(11) NOT NULL,
  `unionid` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `remark` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `groupid` int(11) NOT NULL,
  `tagid_list` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `lastlogintime` int(11) NOT NULL COMMENT '最后登录时间',
  `tel` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`mid`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '铜梁视窗微信应用用户信息表专用' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for wx_menu
-- ----------------------------
DROP TABLE IF EXISTS `wx_menu`;
CREATE TABLE `wx_menu`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `url` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `appid` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '小程序id',
  `pagepath` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '小程序路径',
  `keywords` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '关键词',
  `media_id` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '素材id',
  `sort` int(11) NOT NULL COMMENT '由小到大',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for wx_order
-- ----------------------------
DROP TABLE IF EXISTS `wx_order`;
CREATE TABLE `wx_order`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gh_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `tag` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '15字备注',
  `type` tinyint(1) NOT NULL DEFAULT 0 COMMENT '订单类型，0是进账1是出账',
  `out_trade_no` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '给微信的订单号',
  `trade_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `body` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '订单名称',
  `detail` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '订单详情',
  `total_fee` int(11) NOT NULL COMMENT '分',
  `openid` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '这个是服务号的id',
  `nickname` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT ' ' COMMENT '用户名',
  `scopenid` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT ' ' COMMENT '铜梁视窗的openid',
  `createtime` int(11) NOT NULL,
  `return_code` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `prepay_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '微信返回的订单号',
  `paid_at` int(11) NOT NULL DEFAULT 0 COMMENT '支付时间',
  `status` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `overjump` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '订单支付了就跳转的页面',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '订单' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wx_order
-- ----------------------------
INSERT INTO `wx_order` VALUES (1, 'gh_6c48739f46f6', '铜梁视窗生活服务平台', 1, 'LUCKEY16522519251', 'API', '铜梁视窗生活服务平台', '关注铜梁视窗公众号免费发布信息，还有阅读奖励等你来拿！', 100, '', ' ', 'oIYiD1njS6X6qJWB7ouwvYx0NZ3c', 1652251925, NULL, NULL, 0, NULL, 'https://www.dmake.cn');
INSERT INTO `wx_order` VALUES (2, 'gh_6c48739f46f6', '口令红包', 1, 'LUCKEY16522519314', 'API', '口令红包', '优惠券', 0, '', ' ', 'oIYiD1njS6X6qJWB7ouwvYx0NZ3c', 1652251931, NULL, NULL, 0, NULL, 'https://www.dmake.cn/a/taoke/index/index');
INSERT INTO `wx_order` VALUES (3, 'gh_6c48739f46f6', '口令红包', 1, 'LUCKEY16522519390', 'API', '口令红包', '优惠券', 0, '', ' ', 'oIYiD1njS6X6qJWB7ouwvYx0NZ3c', 1652251939, NULL, NULL, 0, NULL, 'https://www.dmake.cn/a/taoke/index/index');
INSERT INTO `wx_order` VALUES (4, 'gh_6c48739f46f6', '口令红包', 1, 'LUCKEY16522519432', 'API', '口令红包', '优惠券', 0, '', ' ', 'oIYiD1njS6X6qJWB7ouwvYx0NZ3c', 1652251943, NULL, NULL, 0, NULL, 'https://www.dmake.cn/a/taoke/index/index');
INSERT INTO `wx_order` VALUES (5, 'gh_6c48739f46f6', '口令红包', 1, 'LUCKEY16522519607', 'API', '口令红包', '优惠券', 0, '', ' ', 'oIYiD1njS6X6qJWB7ouwvYx0NZ3c', 1652251960, NULL, NULL, 0, NULL, 'https://www.dmake.cn/a/taoke/index/index');
INSERT INTO `wx_order` VALUES (6, 'gh_6c48739f46f6', '口令红包', 1, 'LUCKEY16522519629', 'API', '口令红包', '优惠券', 0, '', ' ', 'oIYiD1njS6X6qJWB7ouwvYx0NZ3c', 1652251962, NULL, NULL, 0, NULL, 'https://www.dmake.cn/a/taoke/index/index');
INSERT INTO `wx_order` VALUES (7, 'gh_6c48739f46f6', '口令红包', 1, 'LUCKEY16522519642', 'API', '口令红包', '优惠券', 0, '', ' ', 'oIYiD1njS6X6qJWB7ouwvYx0NZ3c', 1652251964, NULL, NULL, 0, NULL, 'https://www.dmake.cn/a/taoke/index/index');

-- ----------------------------
-- Table structure for wx_order_detail
-- ----------------------------
DROP TABLE IF EXISTS `wx_order_detail`;
CREATE TABLE `wx_order_detail`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `orderid` int(11) NOT NULL,
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `num` int(11) NOT NULL COMMENT '数量',
  `saleprice` int(11) NOT NULL COMMENT '分',
  `realprice` int(11) NOT NULL COMMENT '分',
  `total_fee` int(11) NOT NULL COMMENT '分',
  `detail` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '其他描述',
  `data` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '传入的参数',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '微信订单的详情' ROW_FORMAT = Dynamic;