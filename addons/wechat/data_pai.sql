-- ----------------------------
-- Table structure for wx_pai
-- ----------------------------
DROP TABLE IF EXISTS `wx_pai`;
CREATE TABLE `wx_pai`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tid` int(11) NOT NULL COMMENT '上级主题id',
  `title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `showpic` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `keywords` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `description` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ptype` int(11) NOT NULL COMMENT '类型的id',
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `createtime` int(11) NOT NULL,
  `audittime` int(11) NOT NULL DEFAULT 0,
  `bits` int(11) NOT NULL COMMENT '阅读量',
  `comments` int(11) NOT NULL COMMENT '评论数量',
  `likes` int(11) NOT NULL DEFAULT 0,
  `score` int(11) NOT NULL COMMENT '价值分为阅读为1分评论6分汇总',
  `auther` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `tel` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `openid` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `prizeid` int(11) NOT NULL DEFAULT 0,
  `code` int(11) NOT NULL COMMENT '兑奖码',
  `address` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `lnglat` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `id`(`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '随手拍主表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for wx_pai_comment
-- ----------------------------
DROP TABLE IF EXISTS `wx_pai_comment`;
CREATE TABLE `wx_pai_comment`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL COMMENT '随手拍的id',
  `detail` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '评论',
  `openid` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `nick_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `headimg` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '随手拍评论' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for wx_pai_pic
-- ----------------------------
DROP TABLE IF EXISTS `wx_pai_pic`;
CREATE TABLE `wx_pai_pic`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL,
  `url` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for wx_pai_prize
-- ----------------------------
DROP TABLE IF EXISTS `wx_pai_prize`;
CREATE TABLE `wx_pai_prize`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tid` int(11) NOT NULL COMMENT '随手拍话题',
  `title` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `detail` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `totalnum` int(11) NOT NULL,
  `leftnum` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '随手拍奖品' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wx_pai_prize
-- ----------------------------
INSERT INTO `wx_pai_prize` VALUES (1, 1, '5元微信红包1个', '加铜梁视窗微信号客服微信回复姓名+兑奖码即可兑换', 200, 200);

-- ----------------------------
-- Table structure for wx_pai_prizelog
-- ----------------------------
DROP TABLE IF EXISTS `wx_pai_prizelog`;
CREATE TABLE `wx_pai_prizelog`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tid` int(11) NOT NULL,
  `pid` int(11) NOT NULL,
  `prizeid` int(11) NOT NULL,
  `auther` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `openid` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `code` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `tel` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `createtime` int(11) NOT NULL,
  `audittime` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '随手拍发放奖品记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for wx_pai_recode
-- ----------------------------
DROP TABLE IF EXISTS `wx_pai_recode`;
CREATE TABLE `wx_pai_recode`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tid` int(11) NOT NULL,
  `pid` int(11) NOT NULL,
  `tag` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '计量标识',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 47 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wx_pai_recode
-- ----------------------------
INSERT INTO `wx_pai_recode` VALUES (1, 1, 5, 'oYCDIt2DvUavx2N8nsGeyTB8sTc4');
INSERT INTO `wx_pai_recode` VALUES (2, 1, 5, 'oYCDItw53NmEuoCuUUuO2D6mVM3k');
INSERT INTO `wx_pai_recode` VALUES (3, 1, 5, 'oN5FQuH12hi5_lc4RTRDXrF-ZeKg');
INSERT INTO `wx_pai_recode` VALUES (4, 1, 8, 'oYCDIt2DvUavx2N8nsGeyTB8sTc4');
INSERT INTO `wx_pai_recode` VALUES (5, 1, 9, 'oN5FQuMzpVHGddAnvO-hWs9uBqRk');
INSERT INTO `wx_pai_recode` VALUES (6, 1, 5, 'oN5FQuK--0S-1iH2aURKoELl1uEU');
INSERT INTO `wx_pai_recode` VALUES (7, 1, 10, 'oN5FQuK--0S-1iH2aURKoELl1uEU');
INSERT INTO `wx_pai_recode` VALUES (8, 1, 11, 'oN5FQuK--0S-1iH2aURKoELl1uEU');
INSERT INTO `wx_pai_recode` VALUES (9, 1, 11, 'oYCDIt4liUJMYMpnNnWvnHBiPZ9g');
INSERT INTO `wx_pai_recode` VALUES (10, 1, 12, 'oN5FQuK--0S-1iH2aURKoELl1uEU');
INSERT INTO `wx_pai_recode` VALUES (11, 1, 12, 'oN5FQuH12hi5_lc4RTRDXrF-ZeKg');
INSERT INTO `wx_pai_recode` VALUES (12, 1, 10, 'oN5FQuH12hi5_lc4RTRDXrF-ZeKg');
INSERT INTO `wx_pai_recode` VALUES (13, 1, 12, 'oN5FQuMzpVHGddAnvO-hWs9uBqRk');
INSERT INTO `wx_pai_recode` VALUES (14, 1, 5, 'oN5FQuMzpVHGddAnvO-hWs9uBqRk');
INSERT INTO `wx_pai_recode` VALUES (15, 1, 8, 'oN5FQuMzpVHGddAnvO-hWs9uBqRk');
INSERT INTO `wx_pai_recode` VALUES (16, 1, 11, 'oN5FQuMzpVHGddAnvO-hWs9uBqRk');
INSERT INTO `wx_pai_recode` VALUES (17, 1, 11, 'oN5FQuH12hi5_lc4RTRDXrF-ZeKg');
INSERT INTO `wx_pai_recode` VALUES (18, 1, 12, 'oYCDIt3pct6UNyLhTjYCPeXv90IQ');
INSERT INTO `wx_pai_recode` VALUES (19, 1, 11, 'oYCDIt3pct6UNyLhTjYCPeXv90IQ');
INSERT INTO `wx_pai_recode` VALUES (20, 1, 13, 'oN5FQuH12hi5_lc4RTRDXrF-ZeKg');
INSERT INTO `wx_pai_recode` VALUES (21, 1, 14, 'oYCDIt2DvUavx2N8nsGeyTB8sTc4');
INSERT INTO `wx_pai_recode` VALUES (22, 1, 15, 'oYCDIt2DvUavx2N8nsGeyTB8sTc4');
INSERT INTO `wx_pai_recode` VALUES (23, 1, 15, 'oN5FQuI62F-LkGYHghBO-jHbIvhw');
INSERT INTO `wx_pai_recode` VALUES (24, 1, 11, 'oN5FQuI62F-LkGYHghBO-jHbIvhw');
INSERT INTO `wx_pai_recode` VALUES (25, 1, 9, 'oN5FQuI62F-LkGYHghBO-jHbIvhw');
INSERT INTO `wx_pai_recode` VALUES (26, 1, 5, 'oN5FQuI62F-LkGYHghBO-jHbIvhw');
INSERT INTO `wx_pai_recode` VALUES (27, 1, 14, 'oN5FQuI62F-LkGYHghBO-jHbIvhw');
INSERT INTO `wx_pai_recode` VALUES (28, 1, 11, 'oYCDIt2DvUavx2N8nsGeyTB8sTc4');
INSERT INTO `wx_pai_recode` VALUES (29, 1, 16, 'oN5FQuH12hi5_lc4RTRDXrF-ZeKg');
INSERT INTO `wx_pai_recode` VALUES (30, 1, 17, 'oN5FQuI62F-LkGYHghBO-jHbIvhw');
INSERT INTO `wx_pai_recode` VALUES (31, 1, 17, 'oYCDIt2DvUavx2N8nsGeyTB8sTc4');
INSERT INTO `wx_pai_recode` VALUES (32, 2, 19, 'oN5FQuH12hi5_lc4RTRDXrF-ZeKg');
INSERT INTO `wx_pai_recode` VALUES (33, 1, 20, 'oYCDIt84LkfEYHFGPN2hs0pFS7qs');
INSERT INTO `wx_pai_recode` VALUES (34, 1, 21, 'oN5FQuH12hi5_lc4RTRDXrF-ZeKg');
INSERT INTO `wx_pai_recode` VALUES (35, 1, 21, 'oYCDIt2DvUavx2N8nsGeyTB8sTc4');
INSERT INTO `wx_pai_recode` VALUES (36, 1, 22, 'oYCDIt2DvUavx2N8nsGeyTB8sTc4');
INSERT INTO `wx_pai_recode` VALUES (37, 1, 16, 'oYCDIt73jic7oQF6MwWv2xC3yZWw');
INSERT INTO `wx_pai_recode` VALUES (38, 1, 5, 'oYCDIt73jic7oQF6MwWv2xC3yZWw');
INSERT INTO `wx_pai_recode` VALUES (39, 1, 25, 'oN5FQuH12hi5_lc4RTRDXrF-ZeKg');
INSERT INTO `wx_pai_recode` VALUES (40, 2, 10, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI');
INSERT INTO `wx_pai_recode` VALUES (41, 1, 26, 'oN5FQuNmN3qs9L1pRcYZBj2Aq2cQ');
INSERT INTO `wx_pai_recode` VALUES (42, 1, 25, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI');
INSERT INTO `wx_pai_recode` VALUES (43, 1, 26, 'oN5FQuI62F-LkGYHghBO-jHbIvhw');
INSERT INTO `wx_pai_recode` VALUES (44, 1, 28, 'oN5FQuJo2ifQDmwV1gcYueVopXGo');
INSERT INTO `wx_pai_recode` VALUES (45, 2, 29, 'o73cGwY-QQZYuUY8bVxRWrEufaPo');
INSERT INTO `wx_pai_recode` VALUES (46, 1, 140, 'oN5FQuGQBeMuKIE8Rz3BjfbLQ_1w');

-- ----------------------------
-- Table structure for wx_pai_topic
-- ----------------------------
DROP TABLE IF EXISTS `wx_pai_topic`;
CREATE TABLE `wx_pai_topic`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '话题主题',
  `keywords` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `description` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '简述',
  `showpic` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '随手拍详情',
  `viewlimit` int(11) NOT NULL,
  `commentlimit` int(11) NOT NULL,
  `starttime` int(11) NOT NULL,
  `endtime` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '随手拍的主题' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wx_pai_topic
-- ----------------------------
INSERT INTO `wx_pai_topic` VALUES (1, '随手拍铜梁，用手机记录真实的铜梁生活', '铜梁随手拍', '随手拍铜梁，拍摄铜梁生活，展览铜梁点滴。每个人都有机会拿现金红包！', 'http://source.5atl.com/attachment/images/201706/17/1497672240.jpg', '<p>铜梁视窗随手拍，拍摄铜梁生活，展览铜梁点滴。<br/>本次活动将提供1000元现金给前200位完成下列任务的网友：\r\n随手拍转发朋友圈收货50个红心者可获得5元现金红包。<br/>参与方法：关注“铜梁视窗”微信公众号，进入菜单“随手拍”即可参与</p>', 50, 0, 1496246400, 1504195200);
INSERT INTO `wx_pai_topic` VALUES (2, '铜梁户外随手拍，分享你爱运动的故事', '户外拍', '本活动旨在分享爱运动的铜梁网友们的故事，号召更多的人加入你们的团体。', 'http://source.5atl.com/attachment/images/201706/17/1497671696.jpg', '<p><br/>本活动旨在分享爱运动的铜梁网友们的故事，号召更多的人加入你们的团体。欢迎铜梁各户外团体加入这个招募爱好者的大家庭来。健康铜梁人，从加入运动开始！</p>', 0, 0, 1496246400, 1527782400);

-- ----------------------------
-- Table structure for wx_pai_type
-- ----------------------------
DROP TABLE IF EXISTS `wx_pai_type`;
CREATE TABLE `wx_pai_type`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` int(11) NOT NULL,
  `description` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '这个类型是拿来做什么的',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '随手拍类型' ROW_FORMAT = Dynamic;