<?php
namespace addons\wechat\controller;

use think\Db;

/**
 * 铜梁视窗专属应用
 *
 * @author Daneas
 *        
 */
class Pai extends Base
{

    /**
     * 首页:集合了额所有随手拍话题，图片列表方式加载
     */
    public function index()
    {
        $topics = Db::name('wx_pai_topic')->field('id,title,description,showpic')
            ->limit(10)
            ->order('id desc')
            ->select();
        
        $sql = 'SELECT a1.id,a1.title,a1.tid,a1.bits,a1.comments,a1.likes,a1.showpic,a1.openid,a1.auther FROM think_wx_pai a1 INNER JOIN (' . 'SELECT a.tid,a.id FROM think_wx_pai a LEFT JOIN think_wx_pai b ' . 'ON a.tid = b.tid AND a.id <= b.id ' . 'GROUP BY a.id HAVING COUNT(b.id) <=5) b1 ON a1.id = b1.id ORDER BY a1.id desc';
        
        $pai = Db::query($sql);
        
        $this->assign('topics', $topics);
        $this->assign('pai', $pai);
        
        $data['title'] = '首页';
        $data['keywords'] = '铜梁随手拍';
        $data['description'] = '铜梁随手拍是铜梁视窗旗下的微信应用，旨在铜梁群众积极参与健康铜梁生活的分享中来。';
        $this->assign('openid', session('SCOPENID'));
        
        $this->assign('data', $data);
        
        return $this->fetch();
    }

    /**
     * 单个随手拍话题的集合，图片，标题，进度，作者
     */
    public function topic()
    {
        $topic = Db::name('wx_pai_topic')->where('id', input('tid'))->find();
        
        $pai = Db::name('wx_pai')->field('id,tid,title,description,showpic,bits,comments,likes,openid,auther')
            ->where('tid', input('tid'))
            ->order('id desc')
            ->select();
        
        $openid = input('openid');
        if (empty($openid)) {
            $openid = session('SCOPENID');
        }
        
        $this->assign('topic', $topic);
        $this->assign('pai', $pai);
        $this->assign('openid', $openid);
        
        $base = new Base();
        $app = $base->wxapp();
        $js = $app->jssdk;
        $this->assign('js', $js);
        
        return $this->fetch();
    }

    /**
     * 单个随手拍话题的详情、活动简介、任务进度【如果不是微信打开的，不显示留言框和点赞按钮，加载个扫描二维码进入随手拍】
     */
    public function item()
    {
        $id = input('id');
        
        $dbpai = Db::name('wx_pai');
        $item = $dbpai->where('id', $id)->find();
        
        $dbpai->where('id', $id)->setInc('bits');
        
        $itempics = Db::name('wx_pai_pic')->field('url')
            ->where('pid', $id)
            ->select();
        
        $topic = Db::name('wx_pai_topic')->where('id', $item['tid'])->find();
        
        // id pid 随手拍的id detail 评论 openid nick_name headimg
        $comments = Db::name('wx_pai_comment')->where('pid', $id)->select();
        
        // 任务进度
        $jindu = 100;
        
        if ($topic['viewlimit'] > 0 && $topic['commentlimit'] > 0) {
            $rateview = $topic['viewlimit'] / ($topic['viewlimit'] + $topic['commentlimit']);
            $jindu = ($item['likes'] / ($topic['viewlimit'])) * 100 * $rateview + ($item['comments'] / $topic['commentlimit']) * 100 * (100 - $rateview);
        } else 
            if ($topic['viewlimit'] > 0 && $topic['commentlimit'] == 0) {
                $jindu = ($item['likes'] / ($topic['viewlimit'])) * 100;
            } else 
                if ($topic['viewlimit'] == 0 && $topic['commentlimit'] > 0) {
                    $jindu = ($item['comments'] / ($topic['commentlimit'])) * 100;
                }
        
        $base = new Base();
        $app = $base->wxapp();
        $js = $app->jssdk;
        $this->assign('js', $js);
        
        $gift = Db::name('wx_pai_prize')->where('tid', $item['tid'])->find();
        
        $this->assign('data', $item);
        $this->assign('pics', $itempics);
        $this->assign('topic', $topic);
        $this->assign('comment', $comments);
        $this->assign('openid', session('SCOPENID'));
        $this->assign('jindu', $jindu);
        $this->assign('gift', $gift);
        
        return $this->fetch();
    }

    public function comment()
    {
        if (request()->isPost()) {
            $id = input('id');
            $openid = session('SCOPENID');
            $nickname = session('SCNICKNAME');
            $headimg = session('SCHEADIMG');
            
            // 获取网络授权
            if ($openid == null || $nickname == null || $headimg == null) {
                
                $split = WX_AUTHSPLIT;
                $state = 'pai' . $split . 'item' . $split . '0' . $split . $id;
                $jumpto = url('auth/index');
                $url = wx_get_code($state, $jumpto, config('wxas')[WX_APPID]);
                return json([
                    'code' => 0,
                    'data' => $url,
                    'msg' => 'jump'
                ]);
            }
            
            // $pai=Db::name('wx_pai')->where('id',$id)->find();
            $result = Db::name('wx_pai_comment')->insertGetId([
                'pid' => $id,
                'openid' => $openid,
                'nick_name' => $nickname,
                'headimg' => $headimg,
                'detail' => input('detail')
            ]);
            
            if ($result > 0) {
                
                Db::name('wx_pai')->where('id', $id)->setInc('comments');
                
                // 要不要计算一下是否自动发奖？？？？这个是个重要节点
                self::chackprize($id);
                
                $str = '<div class="item">
                		<div class="left"><img src="%s" alt="%s"></div>
                		<div class="right">
                		<p>%s</p>
                		<label>微信网友：%s</label>
                		</div>
                	</div>';
                
                return json([
                    'code' => 1,
                    'data' => '',
                    'msg' => sprintf($str, $headimg, $nickname, input('detail'), $nickname)
                ]);
            }
            
            return json([
                'code' => - 1,
                'data' => '',
                'msg' => '很遗憾的告知您：因网络原因您的评价为能成功'
            ]);
        }
    }
    
    // 点赞，获取openid
    public function likeit()
    {
        
        // think_wx_pai_recode ：tid pid tag
        // 1. 查询某个人阅读过几次，如果没有阅读过就insert个记录进去
        // 2. 检查这个随手拍是否达到了领奖标准/是否有奖项
        // 3. 检查是否奖池中海油奖品
        // 4. 发布奖品通知（短信发给作者【您参与铜梁随手拍活动取得优异成绩，赶快加铜梁视窗客服好友领取活动奖励：10元现金红包/大地电影院门票1张/尚莲蝶姿红包1个】）
        if (request()->isPost()) {
            $id = input('post.id');
            $tid = input('post.tid');
            $openid = session('SCOPENID');
            $nickname = session('SCNICKNAME');
            $headimg = session('SCHEADIMG');
            
            // 获取网络授权
            if ($openid == null || $nickname == null || $headimg == null) {
                $split = WX_AUTHSPLIT;
                $state = 'pai' . $split . 'item' . $split . '0' . $split . $id;
                $jumpto = url('auth/index');
                $url = wx_get_code($state, $jumpto, config('wxas')[WX_APPID]);
                return json([
                    'code' => 0,
                    'data' => $url,
                    'msg' => 'jump'
                ]);
            }
            
            $db = Db::name('wx_pai_recode');
            
            $cond['pid'] = $id;
            $cond['tag'] = $openid;
            $cond['tid'] = $tid;
            
            $recode = $db->where($cond)->find();
            
            if ($recode) {
                return json([
                    'code' => - 1,
                    'data' => '',
                    'msg' => '您已赞过本篇随手拍'
                ]);
            }
            
            $result = $db->insertGetId($cond);
            
            if ($result > 0) {
                
                Db::name('wx_pai')->where('id', $id)->setInc('likes');
                
                // 要不要计算一下是否自动发奖？？？？这个是个重要节点
                self::chackprize($id);
                
                return json([
                    'code' => 1,
                    'data' => '',
                    'msg' => '感谢您对本随手拍的支持'
                ]);
            }
            
            return json([
                'code' => - 1,
                'data' => '',
                'msg' => '/(ㄒoㄒ)/~~'
            ]);
        }
    }

    /*
     * 单个随手拍话题的排序、作者昵称、奖品、进度
     */
    public function paixu()
    {}

    /**
     * 拍照（从公众号接口带入openid，通过接口获取详细信息，并填写电话号码，姓名）
     */
    public function apply()
    {
        if (request()->isPost()) {
            $data = input('post.');
            $topic_img = $data['topic_img'];
            $topic_imgs = explode(',', $data['topic_imgs']);
            
            unset($data['topic_img']);
            unset($data['topic_imgs']);
            
            $data['showpic'] = $topic_img;
            $data['createtime'] = time();
            if ($data['tid'] == 1) {
                $data['audittime'] = 0;
            } else {
                $data['audittime'] = time();
            }
            $data['bits'] = 0;
            $data['comments'] = 0;
            $data['score'] = 0;
            $data['likes'] = 0;
            $data['auther'] = session('SCNICKNAME') == null ? '微信网友' : session('SCNICKNAME');
            $data['prizeid'] = 0;
            
            $description = mb_substr(strip_tags($data['detail']), 0, 60, 'utf-8');
            
            $data['description'] = str_replace(PHP_EOL, '', $description);
            
            $pai = new \app\wechat\model\PaiModel();
            return $pai->savepai($data, $topic_imgs);
        }
        
        $openid = input('get.openid');
        if (empty($openid) || strstr($openid, 'oN5FQu') == false) {
            $this->error('请关注公众号“铜梁视窗”后从菜单中打开“红包任务”', url('index'), null, 10);
        }
        
        $wxapp = $this->wxapp()->user;
        $userjson = $wxapp->get($openid);
        
        if ($userjson == null || key_exists('errcode', $userjson)) {
            $this->error('请关注公众号“铜梁视窗”后从菜单中打开“红包任务”', url('index'), null, 10);
        }
        
        $user = $userjson;
        if ($user == null || key_exists('errcode', $user)) {
            $this->error('请关注公众号“铜梁视窗”后从菜单中打开“红包任务”', url('index'), null, 10);
        }
        
        $userauth = new Auth();
        $result = $userauth->wxlogin($user, false, 1);
        
        $this->assign('openid', $user['openid']);
        $this->assign('nickname', $user['nickname']);
        $this->assign('headimgurl', $user['headimgurl']);
        $this->assign('tel', session('SCTEL'));
        
        $tid = input('tid');
        $tid = $tid > 0 ? $tid : 1;
        $topic = Db::name('wx_pai_topic')->where('id', $tid)->find();
        $this->assign('topic', $topic);
        
        return $this->fetch();
    }

    /**
     * 检查是否达到兑奖标准，达到了就兑奖，减库存，发短信
     */
    private function chackprize($pid)
    {
        $pai = Db::name('wx_pai')->field('a.auther,a.openid,a.tel,a.id,a.tid,a.likes,a.comments,b.viewlimit,b.commentlimit')
            ->alias('a')
            ->join('think_wx_pai_topic b', 'a.tid=b.id')
            ->where('a.id', $pid)
            ->find();
        
        if ($pai['viewlimit'] < 1 && $pai['commentlimit'] < 1) {
            return true;
        }
        
        if ($pai['likes'] >= $pai['viewlimit'] && $pai['comments'] >= $pai['commentlimit']) {
            
            // 1. 检查还有木有礼品可以发
            $prizes = Db::name('wx_pai_prize')->where('tid', $pai['tid'])->select();
            
            foreach ($prizes as $prize) {
                
                if ($prize['leftnum'] == 0) {
                    continue;
                }
                
                $code = rand(100000, 999999);
                
                $result = Db::name('wx_pai_prize')->where('id', $prize['id'])->setDec('leftnum');
                $log['pid'] = $pid;
                $log['tid'] = $pai['tid'];
                $log['prizeid'] = $prize['id'];
                $log['auther'] = $pai['auther'];
                $log['openid'] = $pai['openid'];
                $log['code'] = $code;
                $log['tel'] = $pai['tel'];
                $log['createtime'] = time();
                $log['audittime'] = 0;
                
                Db::name('wx_pai_prizelog')->insert($log);
                
                if ($result) {
                    
                    $result = Db::name('wx_pai')->where('id', $pid)->update([
                        'prizeid' => $prize['id'],
                        'code' => $code
                    ]);
                    
                    if ($result) {
                        // 2. 生成兑奖码和礼品到prizeid上去，写入顾客的兑奖日志
                        sendsmsmsg('微信会员' . $pai['auther'] . '您发布的随手拍已达到提现标准，请关注铜梁视窗微信公众号联系客服提现，验证码：' . $code, $pai['tel']);
                    }
                }
                
                return true;
            }
        }
    }

    public function author()
    {
        $openid = input('openid');
        
        $pai = Db::name('wx_pai')->field('id,tid,title,description,showpic,bits,comments,likes,openid,auther')
            ->where('openid', $openid)
            ->order('id desc')
            ->select();
        
        $user = Db::name('wx_member')->where('openid_d', $openid)->find();
        
        $this->assign('pai', $pai);
        $this->assign('user', $user);
        
        return $this->fetch();
    }
}