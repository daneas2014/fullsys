<?php
namespace addons\wechat\controller;

use think\Db;
use addons\wechat\model\ServiceSignin;

class Signin extends Base
{

    private $configkey = CKEY_WX_CONFIG;

    /*
     * 获取跳转的连接，还有APP secreate等要缓存
     */
    private function getauth($appid, $itemid, $type, $action)
    {
        $key = $this->configkey . $appid;
        
        $config = cache($key);
        
        if ($config == null) {
            $app = Db::name('op_app')->where('username', $appid)->find();
            
            if ($app == null || $app['type'] == 0) {
                $config = config('wxas')[WX_APPID];
            } else {
                $config = $app;
            }
            
            cache($key, $config);
        }
        
        $split = WX_AUTHSPLIT;
        return wx_get_code('signin' . $split . $action . $split . $type . $split . $itemid . $split . $config['appid'], url('auth/index'), $config);
    }

    /*
     * 进入签到页，把定位搞上去
     */
    public function index()
    {
        $id = input('get.id');
        $gh_openid = input('get.gh_openid');
        
        // 1. 是否存在任务，或者任务是否有效
        $signin = Db::name('op_signin')->where('id', $id)->find();
        if ($signin == null) {
            echo '签到任务已结束';
            die();
        }
        
        if (session('SCOPENID') == NULL) {
            $url = $this->getauth($signin['gh_id'], $gh_openid . '.' . $id, 1, 'index');
            return redirect($url);
        }
        
        $signuser['openid'] = session('SCOPENID');
        $signuser['gh_openid'] = $gh_openid;
        $signuser['signid'] = $id;
        $signuser['nickname'] = session('SCNICKNAME');
        $signuser['headimg'] = session('SCHEADIMG');
        
        $logdb = Db::name('op_signinlog');
        
        $start = strtotime(date('Y-m-1 0:0:0'));
        
        $logs = $logdb->field('signintime')
            ->where('gh_id', $signin['gh_id'])
            ->where('openid', $signuser['openid'])
            ->where('signintime', 'between', [
            $start,
            time()
        ])
            ->select();
        
        $countdays = getDaysByMonth(intval(date('Y')), intval(date('m')));
        $rows = ($countdays % 7 == 0) ? ($countdays / 7) : ($countdays / 7 + 1);
        
        // 生成签到明细
        $table = '<table class="signtable">';
        for ($i = 0; $i < $rows - 1; $i ++) {
            $table .= '<tr>';
            for ($j = 1; $j < 8; $j ++) {
                $day = $i * 7 + $j;
                if ($day > $countdays) {
                    break;
                }
                $status = $this->checktime(date('Y-m-' . $day), $logs);
                $table .= '<td><span class="' . ($status == true ? "in" : "notin") . '">' . $day . '</span></td>';
            }
            $table .= '</tr>';
        }
        $table .= '</table>';
        
        $todaystatus = $this->checktime(date('Y-m-d'), $logs) == true ? 1 : 0;
        
        $signers = $logdb->field('nickname,headimg,count(gh_openid) as stime')
            ->where('gh_id', $signin['gh_id'])
            ->group('gh_openid')
            ->order('stime desc')
            ->select();
        
        $prise = $logdb->where('gh_id', $signin['gh_id'])
            ->where('gh_openid', $signuser['gh_openid'])
            ->sum('prised');
        
        $signuser['prised'] = $prise/100;
        
        $this->assign([
            'signer' => $signuser,
            'signin' => $signin,
            'todaystatus' => $todaystatus,
            'signers' => $signers,
            'logs' => $logs,
            'token' => md5($gh_openid . '&' . $signuser['openid']),
            'signtable' => $table
        ]);
        
        return $this->fetch();
    }

    private function checktime($day, $times)
    {
        $start = strtotime($day . ' 0:0:0');
        $end = strtotime($day . ' 23:59:59');
        foreach ($times as $time) {
            if ($start < $time['signintime'] && $time['signintime'] < $end) {
                return true;
            }
        }
        
        return false;
    }

    /*
     * 签到
     */
    public function signup()
    {
        if (request()->isPost()) {
            $signuser = input('post.');
            
            if ($signuser['token'] != md5($signuser['gh_openid'] . '&' . $signuser['openid'])) {
                return json([
                    'code' => - 1,
                    'data' => '',
                    'msg' => 'TOKEN ERROR'
                ]);
            }
            
            $sm = new ServiceSignin();
            return $sm->signin($signuser['gh_id'], $signuser['gh_openid'], $signuser['openid'], $signuser['nickname'], $signuser['headimg'], $signuser['lonlat']);
        }
    }
}