<?php
namespace addons\wechat\controller;
use addons\Base;
use think\Db;

class LuckyA extends Base {

	/*
	 * 口令管理
	 */
	public function index() {

		$config = config('wxauth')['authwx'];

		$cond['gh_id'] = $config['gh_id'];

		$kw = input('get.keyword') ? input('get.keyword') : null;
		$start = input('get.start') ? input('get.start') : date('Y-m-01');
		$end = input('get.end') ? input('get.end') : date('Y-m-t');

		if ($kw != null) {
			$cond['code'] = $kw;
		}
		$cond['starttime|endtime'] = ['between', [strtotime($start), strtotime($end)]];

		$db = Db::name('wx_luckcode');

		return parent::vueQuerySingle($db, $cond, 'id desc');
	}

	/*
	 * 保存口令
	 */
	public function lucksave() {
		$config = config('wxauth')['authwx'];

		if (request()->isPost()) {
			$data = input('post.');

			$data['starttime'] = strtotime($data['starttime']);
			$data['endtime'] = strtotime($data['endtime']);
			if ($data['id'] < 1) {
				$data['leftnum'] = 0;
				$data['gh_id'] = $config['gh_id'];
				$data['createtime'] = time();
				$result = Db::name('wx_luckcode')->insert($data);
			} else {
				$result = Db::name('wx_luckcode')->where('id', $data['id'])->update($data);
			}

			if ($result) {
				return json(['code' => 1, 'data' => '', 'msg' => '保存完毕']);
			}

			return json(['code' => -1, 'data' => '', 'msg' => '保存失败']);
		}

		$id = input('id');
		$menu = $id ? Db::name('wx_luckcode')->where('id', $id)->find() : null;

		$this->assign('menu', $menu);
		return $this->fetch();
	}

}