<?php
namespace addons\wechat\controller;

use EasyWeChat;
use think\addons\Controller;
use think\Db;

/**
 * 这里SDK里面修改了【overtrue\wechat\src\core\AccessToken.php】的缓存方式，从官方方式调整到daneas方式
 *
 * @author Daneas
 *
 */
class Base extends Controller {

	private $configkey = CKEY_WX_CONFIG;

	private $key_refreshkey = CKEY_WX_REFRESHTOKEN;

	/**
	 * 返回EasyWechat的app
	 *
	 * @return multitype:boolean multitype:string string
	 */
	public function wxapp($gh_id = null) {
		// 这个是DMAKE.CN自用的
		if ($gh_id == null) {
			$wechatconfig = config('wxauth')['authwx'];

			$options = [
				'debug' => false,
				'app_id' => $wechatconfig['appid'],
				'secret' => $wechatconfig['secret'],
				'token' => $wechatconfig['token'],
				'aes_key' => $wechatconfig['aes_key'],
				'response_type' => 'array',
				'log' => [
					'level' => 'error',
					'file' => './easywechat.log',
				],
			];

			return EasyWeChat\Factory::officialAccount($options);
		}

		// 这个是DMAKE.CN合作平台授权的

		$key = $this->configkey . $gh_id;

		$config = cache($key);

		if ($config == null) {
			$app = Db::name('op_app')->where('username', $gh_id)->find();

			if ($app == null || $app['type'] == 0) {
				die('该公众号未登记，请联系系统管理员');
			}

			$config = $app;

			cache($key, $config);
		}

		$openPlatform = EasyWeChat\Factory::openPlatform(config('wxauth')['openflat']);

		$authappid = $config['appid'];
		$authtoken = cache($this->key_refreshkey . $authappid);

		if ($authtoken == null || strlen($authtoken) < 10) {
			$authinfo = $openPlatform->getAuthorizer($authappid)['authorization_info'];
			if ($authinfo['authorizer_refresh_token'] != null && $authinfo['authorizer_refresh_token'] != '') {
				$authappid = $authinfo['authorizer_appid'];
				$authtoken = $authinfo['authorizer_refresh_token'];

				Db::name('op_app')->where('appid', $authappid)->update([
					'refresh_token' => $authtoken,
				]);
			}
			cache($this->key_refreshkey . $authappid, $authtoken);
		}

		return $openPlatform->officialAccount($authappid, $authtoken);
	}
}