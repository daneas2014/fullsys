<?php
namespace addons\wechat\controller;

use addons\wechat\model\ServiceToupiao;
use think\Db;

class Vote extends VoteBase {

	private function getoauth($voteid, $voteitemid = 0) {
		$split = WX_AUTHSPLIT;
		if ($voteitemid === 0) {
			return wx_get_code('vote' . $split . 'index' . $split . '0' . $split . $voteid, url('auth/index'), config('wxauth')['authwx']);
		} else {
			return wx_get_code('vote' . $split . 'item' . $split . '0' . $split . $voteid . '.' . $voteitemid, url('auth/index'), config('wxauth')['authwx']);
		}
	}

	/**
	 * 活动首页
	 */
	public function index() {
		$key = input('key');
		$map['vote_id'] = input('voteid');
		if ($key && $key !== "") {
			$map['itemnum'] = $key;
		}

		$db = Db::name('wx_voteimg_item');

		$Nowpage = input('get.page') ? input('get.page') : 1;
		$limits = 10; // 获取总条数
		$count = $db->where($map)->count(); // 总数据
		$allpage = intval(ceil($count / $limits));
		$lists = $db->where($map)
			->page($Nowpage, $limits)
			->order('vote_count desc')
			->select();

		$this->assign('Nowpage', $Nowpage); // 当前页
		$this->assign('allpage', $allpage); // 总页数
		$this->assign('val', $key);

		if (input('get.page')) {
			return json($lists);
		}
		return $this->fetch("/vote/1/index");
	}

	public function descr() {
		return $this->fetch("/vote/1/descr");
	}

	/**
	 * 排序:共多少票，多少人多少次投票结果
	 */
	public function paixu() {
		$db = Db::name('wx_voteimg_item');
		$map['vote_id'] = input('voteid');
		$Nowpage = input('get.page') ? input('get.page') : 1;
		$limits = 50; // 获取总条数
		$count = $db->where($map)->count(); // 总数据
		$allpage = intval(ceil($count / $limits));
		$lists = $db->where($map)
			->page($Nowpage, $limits)
			->order('vote_count desc')
			->select();

		$this->assign('Nowpage', $Nowpage); // 当前页
		$this->assign('allpage', $allpage); // 总页数
		$this->assign('pageindex', $Nowpage);

		if (input('get.page')) {
			return json($lists);
		}
		return $this->fetch("/vote/1/paixu");
	}

	public function comment() {
		if (request()->isAjax()) {
			$voteid = input('post.voteid');
			$voteitemid = input('post.voteitemid');
			$msg = input('post.msg');

			if ($voteid > 0 && $voteitemid > 0) {

				$openid = session('SCOPENID');

				if ($openid == null) {
					return json([
						'code' => -1,
						'data' => $this->getoauth($voteid, $voteitemid),
						'msg' => '您还未授权本次投票，即将为您网络授权',
					]);
				}

				$nickname = session('SCNICKNAME');
				$headimg = session('SCHEADIMG');

				$param['vote_id'] = $voteid;
				$param['voteitem_id'] = $voteitemid;
				$param['msg'] = $msg;
				$param['nickname'] = $nickname == null ? '微信网友' : $nickname;
				$param['headimg'] = $headimg == null ? '/static/wechat/voteimg/headimg.gif' : $headimg;
				$param['createtime'] = time();

				$result = Db::name('wx_voteimg_comment')->insert($param);

				if ($result) {
					return [
						'code' => 1,
						'data' => '',
						'msg' => '您的评论已提交',
					];
				} else {
					return [
						'code' => -1,
						'data' => '',
						'msg' => '评论失败',
					];
				}
			}
		}
	}

	/**
	 * 活动单项，判断是否具有投票资格
	 */
	public function item() {
		$voteitemid = input("voteitemid");
		$voteitem = Db::name('wx_voteimg_item')->where('id', $voteitemid)->find();
		$this->assign('voteitem', $voteitem);

		$base = new Base();
		$app = $base->wxapp();
		$js = $app->jssdk;
		$this->assign('js', $js);

		$comments = Db::name('wx_voteimg_comment')->where('voteitem_id', $voteitemid)
			->order('id desc')
			->limit(20)
			->select();
		$this->assign('comments', $comments);

		// 投票道具
		$gifts = Db::name('wx_voteimg_gift')->alias('a')
			->join('think_wx_order b', 'a.orderid=b.id')
			->join('think_wx_voteimg_item c', 'a.voteitemid=c.id')
			->field('a.*,c.vote_title,c.itemnum')
			->where('b.status', 'paid')
			->where('a.voteid', $voteitem['vote_id'])
			->select();

		$this->assign('gifts', $gifts);

		if (session('SCOPENID') != null) {
			$this->assign('islogin', 1);
			$this->assign('SCOPENID', session('SCOPENID'));
		} else {
			$this->assign('islogin', 0);
		}

		return $this->fetch("/vote/1/item");
	}

	public function share() {
		$voteitemid = input("voteitemid");
		$voteitem = Db::name('wx_voteimg_item')->where('id', $voteitemid)->find();
		$this->assign('voteitem', $voteitem);

		// 投票道具
		$gifts = Db::name('wx_voteimg_gift')->alias('a')
			->join('think_wx_order b', 'a.orderid=b.id')
			->join('think_wx_voteimg_item c', 'a.voteitemid=c.id')
			->field('a.*,c.vote_title,c.itemnum')
			->where('b.status', 'paid')
			->where('a.voteid', $voteitem['vote_id'])
			->select();

		$this->assign('gifts', $gifts);

		$base = new Base();
		$app = $base->wxapp();
		$js = $app->jssdk;
		$this->assign('js', $js);

		return $this->fetch("/vote/1/share");
	}

	public function sendgift() {
		$g = input('g');
		$i = input('i');
		$v = input('v');

		if (!$g || !$i || !$v) {
			return false;
		}

		$openid = session('SCOPENID');

		if ($openid == null) {
			return json([
				'code' => -1,
				'data' => $this->getoauth($v, $i),
				'msg' => '您还未授权本次投票，即将为您网络授权',
			]);
		}

		$gs = '';
		$fee = 0;
		switch ($g) {
		case 1:
			$gs = '1支鲜花';
			$fee = 188;
			break;
		case 2:
			$gs = '2支鲜花';
			$fee = 366;
			break;
		case 3:
			$gs = '1捧鲜花';
			$fee = 520;
			break;
		case 4:
			$gs = '1个福袋';
			$fee = 666;
			break;
		}

		$order['out_trade_no'] = 'VOTE' . time() . $i;
		$order['trade_type'] = 'JSAPI';
		$order['tag'] = '';
		$order['body'] = '投票道具-' . $gs;
		$order['detail'] = '您在投票活动中购买了' . $gs;
		$order['total_fee'] = $fee;
		$order['openid'] = session('SCOPENID');
		$order['overjump'] = url('item') . '?voteid=' . $v . '&voteitemid=' . $i;
		$order['createtime'] = time();

		$item['name'] = $order['body'];
		$item['num'] = 1;
		$item['saleprice'] = $fee;
		$item['realprice'] = $fee;
		$item['total_fee'] = $fee;
		$item['detail'] = $g;
		$item['orderid'] = 0;
		$item['data'] = '{"voteid":' . $v . ',"voteitemid":' . $i . '}';

		$items = [];
		$items[] = $item;

		$orderm = new \addons\wechat\model\OrderModel();
		$orderid = $orderm->createOrder($order, $items);

		if ($orderid > 0) {

			$log['voteitemid'] = $i;
			$log['voteid'] = $v;
			$log['nickname'] = session('SCNICKNAME');
			$log['headimg'] = session('SCHEADIMG');
			$log['openid'] = session('SCOPENID');
			$log['giftid'] = $g;
			$log['createtime'] = time();
			$log['orderid'] = $orderid;

			if (Db::name('wx_voteimg_gift')->insert($log)) {
				$url = url('pay/index') . '?id=' . $orderid;
				return redirect($url);
			}
		}

		return false;
	}

	/**
	 * 报名
	 */
	public function apply() {
		if (request()->isAjax()) {

			$voteid = input('vote_id');
			$voteimgaction = Db::name('wx_voteimg')->field('id,action_name,watermark,firstwidth,firstheight,apply_end_time,vote_type')
				->where('id', $voteid)
				->find();

			$db = Db::name('wx_voteimg_item');
			$uploader = new \app\api\controller\UploadApi();

			$param = input('post.');
			$param['itemnum'] = 1;

			$maxitemnum = $db->field('itemnum')
				->where('vote_id', $param['vote_id'])
				->order('itemnum desc')
				->find();

			if ($maxitemnum != null) {
				$param['itemnum'] = $maxitemnum['itemnum'] + 1;
			}

			$voteimg = $param['vote_img'];
			$param['vote_imgs'] = str_replace('\\', '/', $param['vote_imgs']);

			// 1. 是否需要合成海报,用添加水印的方法
			if (array_key_exists('vote_type', $param) && $param['vote_type'] == 1) {

				unset($param['vote_type']);

				$voteitemid = $db->insertGetId($param);

				// 0. 确定海报路径
				$voteimg_n = str_replace('.', '_bh.', $voteimg);

				// 1. 合并横幅【这里打个标记：如果是自定义了水印图片，那么需要修改位置起点】
				$image = \think\Image::open('.' . $voteimg);

				// 右上方水印

				if ($voteid == 16) { // 江小白
					$image->water('./static/wechat/image/watermark.png', \think\Image::WATER_NORTHEAST)->save('.' . $voteimg_n);
				}

				if ($voteimgaction['vote_type'] == 1 && isset($voteimgaction['watermark']) && $voteimgaction['firstheight'] > 0 && $voteimgaction['firstwidth'] > 0) {
					// 图文海报
					$image->water('.' . $voteimgaction['watermark'], 1, 100)->save('.' . $voteimg_n);
				} else {
					// 图文投票
					$image->water('./static/wechat/image/bottom.png', 8, 70)->save('.' . $voteimg_n);
				}

				// 2. 生成口号
				$image = \think\Image::open('.' . $voteimg_n);

				if ($voteimgaction['vote_type'] == 1 && isset($voteimgaction['watermark']) && $voteimgaction['firstheight'] > 0 && $voteimgaction['firstwidth'] > 0) {
					// 图文海报
					$newstr = '        我是「' . $param['vote_title'] . '」
            ' . $param['manifesto'] . '
            微信识别二维码关注铜梁视窗，关注铜梁生活';
					$image->text('“' . $newstr, './static/fonts/STHeiti-Medium.ttc', 24, '#ffffff', 7, -60, 0)->save('.' . $voteimg_n);
				} else {
					// 图文投票
					$newstr = '        我是' . $param['itemnum'] . '号选手「' . $param['vote_title'] . '」
           ' . $param['manifesto'] . '
           微信识别二维码回复关键词“' . $voteitemid . '”投我一票吧';
					$image->text('“' . $newstr, './static/fonts/msyh.ttc', 14, '#0A8BD6', 7, -60, 0)->save('.' . $voteimg_n);
				}

				// 3. 将海报赋值到第一个图片
				$voteimg_n = $uploader->uploadtoqiniu($voteimg_n);

				if (strstr($param['vote_imgs'], ',')) {
					$param['vote_imgs'] = $voteimg_n . ',' . $param['vote_imgs'];
				} else {
					$param['vote_imgs'] = $voteimg_n;
				}
			}

			// 2. 将第一张图切了。
			self::cutpic($voteimg);
			$voteimg = $uploader->uploadtoqiniu($voteimg);
			$param['vote_img'] = $voteimg;

			$result = $db->where('id', $voteitemid)->update($param);
			if ($result) {
				return json([
					'code' => 1,
					'data' => '',
					'msg' => '您已成功报名，由活动主办方审核无误后即可投票',
				]);
			} else {
				return json([
					'code' => -1,
					'data' => '',
					'msg' => '申请失败，请刷新后重试',
				]);
			}
		}

		$session_openid = session('SCOPENID');
		$get_openid = input("get.openid");
		$openid = $session_openid == null ? $get_openid : $session_openid;

		$voteid = input('voteid');
		$voteimgaction = Db::name('wx_voteimg')->field('id,action_name,apply_end_time,vote_type,need_check')
			->where('id', $voteid)
			->find();

		if ($voteimgaction['apply_end_time'] < time()) {
			$this->error('本次活动报名时间已过，当个观众也挺好的(*^__^*)');
		}

		// 1个微信是否只能报名1次

		if ($openid == null) {
			$this->error('您还未授权本次投票，即将为您网络授权', $this->getoauth($voteid), 10);
		}

		// 判断是否需要关注了公众号后，这个东西就尴尬了，逻辑太复杂，不用这个了
		if ($voteimgaction['need_check'] == 1) {
			$checkmark = self::checkmark($openid);
			if ($checkmark === false) {
				$descurl = url('descr') . '?voteid=' . $voteid;
				$this->error('您还未关注活动公众号，请关注二维码后参加活动吧', $descurl);
			}
		}

		$this->assign('openid', $openid);

		return $this->fetch('/vote/2/apply');
	}

	/**
	 * 跳转投票,投票后跳转到item页面
	 */
	public function recode() {
		if (request()->isAjax()) {
			$voteid = input('post.voteid');
			$voteitemid = input('post.voteitemid');

			if ($voteid > 0 && $voteitemid > 0) {

				$openid = session('SCOPENID');

				if ($openid == null) {
					return json([
						'code' => -1,
						'data' => $this->getoauth($voteid, $voteitemid),
						'msg' => '您还未授权本次投票，即将为您网络授权',
					]);
				}

				$checkmark = self::checkmark($openid);
				if ($checkmark === false) {
					return json([
						'code' => -1,
						'data' => '',
						'msg' => '您还未关注活动公众号，请点击下方“简介”关注二维码参加活动吧',
					]);
				}

				$nickname = session('SCNICKNAME');
				$headimg = session('SCHEADIMG');

				$vote = new ServiceToupiao();
				$json = $vote->wap($voteid, $voteitemid, $openid, $nickname, $headimg);

				return $json;
			}
		}
	}

	public function voteupload() {
		$base64 = input('img');
		$saveimg = input('saveimg');
		$voteid = input('voteid');

		$base64_image = str_replace(' ', '+', $base64);
		// post的数据里面，加号会被替换为空格，需要重新替换回来，如果不是post的数据，则注释掉这一行
		if (preg_match('/^(data:\s*image\/(\w+);base64,)/', $base64_image, $result)) {
			// 匹配成功
			if ($result[2] == 'jpeg') {
				$image_name = uniqid() . '.jpg';
				// 纯粹是看jpeg不爽才替换的
			} else {
				$image_name = uniqid() . '.' . $result[2];
			}

			$dir = "/attachment/voteimg/" . date('Ym') . '/' . date('d');
			$image_file = $dir . '/' . time() . '.jpg';

			if (!is_dir(ROOT_PATH . 'public_html' . $dir)) {
				mkdir(ROOT_PATH . 'public_html' . $dir, 0755, true);
			}

			// 服务器文件存储路径
			if (file_put_contents('.' . $image_file, base64_decode(str_replace($result[1], '', $base64_image)))) {

				$path = '';

				if ($saveimg == 0) { // 处理普通图片,并上传云盘
					self::cutpic($image_file, 480);
					$uploader = new \app\api\controller\UploadApi();
					$path = $uploader->uploadtoqiniu($image_file);
				} else { // 剪切海报，在点击提交的时候给打水印
					$path = $image_file;
					$voteimg = Db::name('wx_voteimg')->where('id', $voteid)->find();

					// 剪切图片
					if ($voteimg['firstwidth'] > 0 && $voteimg['firstheight'] > 0) {
						self::cutpic($image_file, $voteimg['firstwidth'], $voteimg['firstheight']);
					} else {
						self::cutpic($image_file, 720);
					}
				}

				return json([
					'code' => 1,
					'data' => $path,
				]);
			} else {
				return json([
					'code' => -1,
					'data' => '',
				]);
			}
		} else {
			return json([
				'code' => -1,
				'data' => '',
			]);
		}
	}

	/**
	 * 检查投票人是否关注微信号
	 *
	 * @param unknown $openid
	 * @return boolean
	 */
	private function checkmark($openid) {
		return true;

		$base = new Base();
		$user = $base->wxapp()->user;
		try {
			$markuser = $user->get($openid);

			if ($markuser == null || $markuser['subscribe'] != 1) {
				return false;
			}

			return true;
		} catch (\Exception $e) {
			return false;
		}
	}

	/**
	 * 剪切缩略图，以覆盖的方式剪切
	 *
	 * @param unknown $src
	 */
	private function cutpic($src, $savewidth = 300, $saveheight = 300) {
		$src = "." . $src;

		$image = \think\Image::open($src);
		// 返回图片的宽度
		$width = $image->width();
		// 返回图片的高度
		$height = $image->height();
		// 返回图片的类型
		$type = $image->type();
		// 返回图片的mime类型
		$mime = $image->mime();
		// 返回图片的尺寸数组 0 图片宽度 1 图片高度
		$size = $image->size();

		if ($savewidth != 300 && $saveheight = 300) {
			// 按照原图的比例生成一个最大为150*150的缩略图并保存为thumb.png
			$saveheight = $savewidth * ($height / $width);
		}

		$image->thumb($savewidth, intval($saveheight), \think\Image::THUMB_CENTER)->save($src);
	}
}