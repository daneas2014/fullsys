<?php
namespace addons\wechat\controller;

use think\Db;
use addons\Base;

class PaiA extends Base
{

    public function index()
    {
        $db = Db::name('wx_pai_topic');
        
        $cond = null;
        
        if (input('kw') != null) {
            $cond['title'] = [
                'like',
                '%' . input('kw') . '%'
            ];
        }
        
        $count = $db->where($cond)->count();
        
        $list = $db->where($cond)
            ->field('id,title,description,showpic,starttime,endtime,viewlimit,commentlimit')
            ->order('id desc')
            ->paginate(20);
        
        $this->assign('page_method', $list->render());
        $this->assign('news_count', $count);
        $this->assign('list', $list);
        $this->assign('pageindex', input('page'));
        
        return $this->fetch();
    }

    public function savepai()
    {
        $db = Db::name('wx_pai_topic');
        if (request()->isPost()) {
            $param = input('post.');
            unset($param['file']);
            $param['starttime'] = strtotime($param['starttime']);
            $param['endtime'] = strtotime($param['endtime']);
            
            $result = true;
            if ($param['id'] == 0) {
                $result = $db->insert($param);
            } else {
                $result = $db->where('id', $param['id'])->update($param);
            }
            
            if ($result) {
                $this->success('保存完毕', url('index'));
            }
            
            $this->error('保存失败');
        }
        
        $id = input('id');
        $data = $db->where('id', $id)->find();
        $this->assign('model', $data);
        
        return $this->fetch();
    }

    public function prizelist()
    {
        $db = Db::name('wx_pai_prize');
        
        $cond['tid'] = input('tid');
        
        $count = $db->where($cond)->count();
        
        $list = $db->where($cond)
            ->order('id desc')
            ->paginate(20);
        
        $this->assign('page_method', $list->render());
        $this->assign('news_count', $count);
        $this->assign('list', $list);
        $this->assign('pageindex', input('page'));
        $this->assign('tid', input('tid'));
        
        return $this->fetch();
    }

    public function saveprize()
    {
        $db = Db::name('wx_pai_prize');
        if (request()->isPost()) {
            $param = input('post.');
            
            $result = true;
            if ($param['id'] == 0) {
                $result = $db->insert($param);
            } else {
                $result = $db->where('id', $param['id'])->update($param);
            }
            
            if ($result) {
                $this->success('保存完毕', url('prizelist') . '?tid=' . $param['tid']);
            }
            
            $this->error('保存失败');
        }
        
        $id = input('id');
        $data = $db->where('id', $id)->find();
        $this->assign('model', $data);
        
        $tid = input('tid');
        if ($data) {
            $tid = $data['tid'];
        }
        $this->assign('tid', $tid);
        
        return $this->fetch();
    }

    public function audititem()
    {
        $db = Db::name('wx_pai');
        
        if (request()->isPost()) {
            $id = input('post.id');
            $type = input('post.type');
            
            $result = true;
            
            if ($type == 'a') {
                $result = $db->where('id', $id)->update([
                    'audittime' => time()
                ]);
            }
            if ($type == 'd') {
                $result = $db->where('id', $id)->delete();
            }
            
            if ($result) {
                return json(['code'=>1,'data'=>'','msg'=>'操作成功']);
            }

            return json(['code'=>-1,'data'=>'','msg'=>'操作失败']);
        }
        
        $tid = input('tid');
        
        if ($tid > 0) {
            $cond['tid'] = $tid;
        }
        if (input('kw') != null) {
            $cond['title'] = [
                'like',
                '%' . input('kw') . '%'
            ];
        }
        
        $count = $db->where($cond)->count();
        
        $list = $db->where($cond)
            ->field('id,title,showpic,tid,auther,bits,likes,comments,tel,audittime')
            ->order('id desc')
            ->paginate(20);
        
        $this->assign('page_method', $list->render());
        $this->assign('news_count', $count);
        $this->assign('list', $list);
        $this->assign('pageindex', input('page'));
        
        return $this->fetch();
    }

    public function prizelog()
    {
        $db = Db::name('wx_pai_prizelog');
        
        $cond = null;
        if (input('all') == 0) {
            $cond['audittime'] = 0;
        }
        
        if (input('kw')) {
            $cond['tel'] = input('kw');
        }
        
        $count = $db->where($cond)->count();
        
        $list = $db->where($cond)
            ->order('id desc')
            ->paginate(20);
        
        $this->assign('page_method', $list->render());
        $this->assign('news_count', $count);
        $this->assign('list', $list);
        $this->assign('pageindex', input('page'));
        
        return $this->fetch();
    }

    public function auditprize()
    {
        $result = Db::name('wx_pai_prizelog')->where('id', input('id'))->update([
            'audittime' => time()
        ]);
        
        if ($result) {
            $this->success('已登记为兑换');
        }
        
        $this->error('未能兑换成功');
    }
}