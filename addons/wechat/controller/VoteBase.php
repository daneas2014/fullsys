<?php
namespace addons\wechat\controller;

use think\Db;
use think\addons\Controller;

/**
 * 投票页面都会用到的数据
 *
 * @author Daneas
 *        
 */
class VoteBase extends Controller
{

    public function _initialize()
    {
        $voteid = input('get.voteid');
        if ($voteid > 0) {
            $result = self::getInitData($voteid);
            $this->assign([
                'voteid' => $voteid,
                'voteimg' => $result['voteimg'],
                'bannar' => $result['bannar'],
                'bottom' => $result['bottom'],
                'prize' => $result['prize'],
                'sponsor' => $result['sponsor'],
                'tongji1' => $result['tongji1'],
                'tongji2' => $result['tongji2'],
                'tongji3' => $result['tongji3']
            ]);
        }
    }

    /**
     * 把基础资料这些都弄上来
     *
     * @param unknown $voteid            
     */
    public function getInitData($voteid)
    {
        $key = 'WECHATVOTE' . $voteid;
        
        $result = cache($key);
        
        $cond['vote_id'] = $voteid;
        
        if ($result == null) {
            $voteimg = Db::name('wx_voteimg')->where('id', $voteid)->find();
            $bottom = Db::name('wx_voteimg_bottom')->where($cond)
                ->order('bottom_rank asc')
                ->select();
            $prize = Db::name('wx_voteimg_prize')->where($cond)->select();
            $sponsor = Db::name('wx_voteimg_sponor')->where($cond)->select();
            $bannar = Db::name('wx_voteimg_banner')->where($cond)
                ->order('banner_rank asc')
                ->select();
            
            $tonji_item = Db::name('wx_voteimg_item')->where($cond)->count();
            $tonji_piaoshu = Db::name('wx_voteimg_record')->where($cond)->count();
            $tonji_renshu = Db::name('wx_voteimg_users')->where($cond)->count();
            
            $result = array(
                'voteimg' => $voteimg,
                'bannar' => $bannar,
                'bottom' => $bottom,
                'prize' => $prize,
                'sponsor' => $sponsor,
                'tongji1' => $tonji_item,
                'tongji2' => $voteimg['fvote'] + $tonji_piaoshu,
                'tongji3' => $voteimg['fman'] + $tonji_renshu
            );
            
            cache($key, $result);
        }
        
        return $result;
    }
}