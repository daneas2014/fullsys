<?php
namespace addons\wechat\controller;

use think\Db;

class Read extends Base
{

    private $configkey = CKEY_WX_CONFIG;

    /*
     * 获取跳转的连接，还有APP secreate等要缓存
     */
    private function getauth($appid, $itemid, $type, $action)
    {
        $key = $this->configkey . $appid;
        
        $config = cache($key);
        
        if ($config == null) {
            $app = Db::name('op_app')->where('username', $appid)->find();
            
            if ($app == null || $app['type'] == 0) {
                $config = config('wxas')[WX_APPID];
            } else {
                $config = $app;
            }
            
            cache($key, $config);
        }
        
        $split = WX_AUTHSPLIT;
        return wx_get_code('read' . $split . $action . $split . $type . $split . $itemid, url('auth/index'), $config);
    }

    /*
     * 所有的阅读任务
     */
    public function index()
    {
        $data['title'] = '本地好文好看更好玩，阅读就能产生收益！';
        $data['keywords'] = '本地好文好看更好玩，阅读就能产生收益！';
        $data['description'] = '本地好文好看更好玩，阅读就能产生收益！';
        
        $list = Db::name('op_read')->field('id,title,showpic,description,totalamount,leftamount,starttime,endtime')
            ->order('id desc')
            ->select();
        
        $this->assign('data', $data);
        $this->assign('list', $list);
        
        return $this->fetch();
    }

    /*
     * 获取阅读任务，显示总量剩余量和参与人数等,页面上面做分享按钮，那个时候判断是否有session,如果notice为空，authurl才会出来
     */
    public function item()
    {
        $id = input('get.id');
        
        $item = Db::name('op_read')->alias('a')
            ->join('think_op_app b', 'a.gh_id=b.username')
            ->field('a.*,b.name,b.id as aid,b.wximg,b.description as bd')
            ->where('a.id', $id)
            ->find();
        
        if (session('SCOPENID') == NULL) {
            $url = $this->getauth($item['gh_id'], $id, 0, 'item');
            return redirect($url);
        }
        
        $notice = '';
        // 检查是否关注了公众号的
        if (Db::name('op_appuser')->where('openid', session('SCOPENID'))->count() == 0) {
            $app = Db::name('op_app')->alias('a')
                ->join('think_op_read b', 'a.username=b.gh_id')
                ->where('b.id', $id)
                ->find();
            $notice = '亲，你还未关注公众号“' . $app['name'] . '”，不能参加此公众号发起的任务。扫描下方二维码即可参与任务<br/><img src="' . $app['wximg'] . '" width="100"/>';
        }
        
        $js = $this->wxapp()->jssdk;
        $this->assign('js', $js);
        
        $mission = Db::name('op_readuser')->where('openid', session('SCOPENID'))
            ->where('readid', $id)
            ->find();
        if ($mission == null) {
            $this->assign('authurl', $this->getauth($item['gh_id'], $id, 0, 'signup'));
        } else {
            $this->assign('authurl', url('share') . '?id=' . $mission['id'] . '&openid=' . $mission['openid']);
        }
        
        $this->assign('notice', $notice);
        $this->assign('data', $item);
        $this->assign('jindu', $item['leftamount'] / $item['totalamount']);
        return $this->fetch();
    }

    /*
     * 点击item页面我要参加任务的时候跳转到这个页面处理,这个时候网友一方面报了名而方便跳转到任务分享页面
     */
    public function signup()
    {
        $id = input('get.id');
        
        $item = Db::name('op_read')->where('id', $id)->find();
        
        $data['readid'] = $id;
        $data['openid'] = session('SCOPENID');
        $data['createtime'] = time();
        $data['status'] = 'in';
        $data['nickname'] = session('SCNICKNAME');
        $data['headimgurl'] = session('SCHEADIMG');
        $data['viewnum'] = 0;
        $data['gh_id'] = $item['gh_id'];
        
        $userdb = Db::name('op_readuser');
        $cound['openid'] = $data['openid'];
        $cound['readid'] = $id;
        $missionid = 0;
        if ($userdb->where($cound)->count() < 1) {
            $missionid = $userdb->insertGetId($data);
        } else {
            $missionid = $userdb->where($cound)->find()['id'];
        }
        
        return redirect(url('wechat/read/share') . '?id=' . $missionid . '&openid=' . $data['openid']);
    }

    /*
     * 朋友圈阅读，需要获取auth，获取后就出现了助力按钮，点击的时候就跳转mpurl，搞错了搞错了，这里share的应该是user任务id
     */
    public function share()
    {
        $id = input('get.id');
        $openid = input('get.openid');
        
        $userdb = Db::name('op_readuser');
        
        $user = $userdb->where('id', $id)->find();
        
        if ($user == null) {
            return $this->error('参数错误1');
        }
        
        if (session('SCOPENID') == NULL) {
            $url = $this->getauth($user['gh_id'], $id . '&' . $openid, 1, 'share');
            return redirect($url);
        }
        
        $item = Db::name('op_readuser')->alias('a')
            ->join('think_op_app b', 'a.gh_id=b.username')
            ->join('think_op_read c', 'a.readid=c.id')
            ->field('a.*,b.name,b.id as aid,b.wximg,b.description as bd,c.title,c.showpic,c.description,c.detail,c.leftamount,c.totalamount,c.mpurl,c.limitnum')
            ->where('a.id', $id)
            ->find();
        
        if ($item == null) {
            return $this->error('参数错误2');
        }
        
        // 2. 分享页面就可以操作了
        if ($item != null && $user != null) {
            
            $this->assign('data', $item);
            $this->assign('user', $user);
            
            $js = $this->wxapp()->jssdk;
            $this->assign('js', $js);
            $this->assign('jindu', $item['leftamount'] / $item['totalamount']);
            
            $loger = Db::name('op_readlog')->where('openid', session('SCOPENID'))
                ->where('readid', $item['readid'])
                ->where('readuserid', $item['id'])
                ->find();
            
            if ($item['viewnum'] <= $item['limitnum']) {
                $this->assign('withdrow', '<a href="#" class="sharedbt">等待提现</a>');
            } else {
                if ($item['status'] == 'in') {
                    $this->assign('withdrow', '<a href="' . url('missionend') . '?readid=' . $user['readid'] . '&readuserid=' . $user['id'] . '" class="sharebt">速度提现</a>');
                } else {
                    $this->assign('withdrow', '<a href="#" class="sharedbt">您已提现</a>');
                }
            }
            
            $this->assign('loger', $loger);
            
            $logs = Db::name('op_readlog')->where('readid', $item['readid'])
                ->where('readuserid', $item['id'])
                ->select();
            $this->assign('logs', $logs);
            
            $this->assign('token', md5(session('SCOPENID')));
            return $this->fetch();
        }
    }

    /*
     * 把好友帮扶的记录写入，还要验证有效性
     */
    public function log()
    {
        $data = input('post.'); // 任务readid，发起人readuseri
        $data['openid'] = session('SCOPENID');
        $data['nickname'] = session('SCNICKNAME');
        $data['headimgurl'] = session('SCHEADIMG');
        $data['createtime'] = time();
        
        // 1.验证好友是否登录
        if ($data['openid'] == NULL || $data['token'] != md5($data['openid'])) {
            echo '非法操作';
            die();
        }
        
        // 2. 验证是否是本人
        
        $user = Db::name('op_readuser')->where('id', $data['readuserid'])->find();
        if ($data['openid'] == $user['openid']) {
            return json([
                'code' => - 1,
                'data' => null,
                'msg' => '本人不参与阅读哟'
            ]);
        }
        
        // 3. 验证是否阅读过了
        $logdb = Db::name('op_readlog');
        if ($logdb->where('openid', $data['openid'])
            ->where('readid', $data['readid'])
            ->where('readuserid', $user['id'])
            ->count() > 0) {
            return json([
                'code' => - 1,
                'data' => null,
                'msg' => '您已经帮助阅读过一次啦'
            ]);
        }
        
        // 4. 记录一下
        if ($logdb->insert($data)) {
            Db::name('op_readuser')->where('id', $data['readuserid'])->setInc('viewnum');
            return json([
                'code' => 1,
                'data' => null,
                'msg' => '今天的文章真真真好，要是你觉得还可以的话赶快分享给你的朋友们吧！'
            ]);
        }
    }

    public function missionend()
    {
        $data = input('get.');
        
        $mission = Db::name('op_read')->where('id', $data['readid'])->find();
        
        if ($mission == null) {
            return json([
                'code' => - 1,
                'data' => '',
                'msg' => '任务不存在'
            ]);
        }
        
        if ($mission != null && $mission['leftamount'] <= 0) {
            
            return $this->error('任务红包已经被抢光啦，下次早点来提现哟');
            
            // return json([
            // 'code' => - 1,
            // 'data' => '',
            // 'msg' => '任务红包已经被抢光啦，下次早点来提现哟'
            // ]);
        }
        
        $user = Db::name('op_readlog')->alias('a')
            ->join('think_op_readuser b', 'a.readuserid=b.id')
            ->field('a.*,b.status,b.viewnum')
            ->where('a.readid', $data['readid'])
            ->where('a.readuserid', $data['readuserid'])
            ->select();
        
        if (count($user) > $mission['limitnum'] && $user[0]['status'] == 'in') {
            
            $uRL = $this->CreateLuckRM($mission['gh_id'], $user[0]['openid'], 100);
            
            Db::name('op_readuser')->where('id', $data['readuserid'])->update([
                'status' => 'end'
            ]);
            
            Db::name('op_read')->where('id', $data['readid'])->setDec('leftamount');
            
            return $this->success('红包已发放，正在跳转领取', $uRL);
            
            // return json([
            // 'code' => 1,
            // 'data' => $uRL,
            // 'msg' => '红包已发放，正在跳转领取'
            // ]);
        }
        
        return json([
            'code' => - 1,
            'data' => '',
            'msg' => 'ERROR'
        ]);
    }

    /*
     * 生成普通红包和裂变红包，type=1是普通红包，2是裂变红包
     */
    public function CreateLuckRM($gh_id, $openid, $totalfee = 100)
    {
        $order['out_trade_no'] = 'READ' . time() . rand(0, 9);
        $order['trade_type'] = 'API';
        
        $order['type'] = 1;
        $order['tag'] = '铜梁视窗阅读红包';
        $order['body'] = '铜梁视窗阅读红包';
        $order['detail'] = '恭喜你获得阅读红包一个，关注铜梁视窗公众号阅读文章即可拥有';
        $order['total_fee'] = $totalfee;
        
        $order['scopenid'] = $openid;
        $order['gh_id'] = $gh_id;
        $order['overjump'] = 'http://www.maimaitl.com/m';
        $order['createtime'] = time();
        
        $m = new \app\wechat\model\OrderModel();
        $orderid = $m->createOrder($order, null);
        $split = WX_AUTHSPLIT;
        $url = wx_get_code('pay' . $split . 'luckmoney' . $split . '1' . $split . $orderid, url('wechat/auth/index'), config('wxas')[WX_APPID]);
        
        return $url;
    }
}