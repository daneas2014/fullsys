<?php
namespace addons\wechat\controller;

use think\addons\Controller;
use think\Db;

class Exam extends Controller {

	/*
	 * 考试封面，简述，礼品等
	 */
	public function index() {

        if(!input('id')){
            die('参数不对');
        }

		if (session('SCOPENID') == null && client_get_browser() == 'Wechat') {
			$split = WX_AUTHSPLIT;
			$state = 'exam' . $split . 'index' . $split . '0' . $split . input('id');
			$jumpto = addon_url('wechat://auth/index');
            
			$url = wx_get_code($state, $jumpto, config('wxauth')['authwx']);

			return redirect($url);
		}

		$data = Db::name('wx_exam')->where('id', input('id'))->find();
		$items = Db::name('wx_exam_item')->where('examid', input('id'))->select();
		$sessionmark = 1;
		if (session('exam_name') == null) {
			$sessionmark = 0;
		}

		$this->assign([
			'exam' => $data,
			'items' => $items,
			'sessionmark' => $sessionmark,
		]);

		return $this->fetch();
	}

	public function auth() {
		if (request()->isPost()) {
			session('exam_name', input('examname'));
			session('exam_phone', input('examphone'));
			$url = addon_url('wechat://exam/index') . '?id=' . input('examid');
			return redirect($url);
		}
	}

	/**
	 * 考试答案
	 */
	public function answer() {
		$data = Db::name('wx_exam_result')->where('id', input('id'))->find();

		$exam = Db::name('wx_exam')->where('id', $data['examid'])->find();

		$items = Db::name('wx_exam_item')->where('examid', $data['examid'])->select();

		$result = json_decode($data['detail'], true);

		$list = [];

		foreach ($items as $item) {
			foreach ($result as $child) {
				if ($item['id'] == $child['itemid']) {
					$item['selected'] = $child['selected'];
					$list[] = $item;
				}
			}
		}

		$this->assign([
			'exam' => $exam,
			'list' => $list,
			'data' => $data,
		]);

		return $this->fetch();
	}

	/*
	 * post检查答案正确与否，先检查是否登录，答案正确写入wx_exame_result
	 */
	public function check() {
		$vali1 = Db::name('wx_exam_result')->alias('a')
			->join('think_wx_exam b', 'a.examid=b.id')
			->where('a.openid', session('SCOPENID'))
			->where('b.id', input('examid'))
			->find();

		if ($vali1 != null) {
			return json([
				'code' => -1,
				'msg' => '您已经参与过答题了',
			]);
		}

		// 1. 传入答题和答案的序列
		$result = json_decode(input('data'), true);

		// 2. 从数据库中加载正确的答题列表和答案
		$exam = Db::name('wx_exam')->field('id,name,endtime,sendmsg,examor,mark,havegit')
			->where('id', input('examid'))
			->find();

		if (time() > $exam['endtime']) {
			return json([
				'code' => -1,
				'msg' => '本次答题活动已结束',
			]);
		}

		$items = Db::name('wx_exam_item')->where('examid', input('examid'))->select();
		if ($items == null) {
			return json([
				'code' => -1,
				'msg' => '考题都还没出呢',
			]);
		}

		// 3. 对比两个序列，把不匹配的项目列出来
		$list = [];
		foreach ($items as $item) {
			foreach ($result as $child) {
				if ($item['id'] == $child['itemid']) {
					if ($item['rightanswer'] == $child['selected']) {
						$item['selected'] = $child['selected'];
						$list[] = $item;
					}
				}
			}
		}

		// 4. 计算分值 正确数量占比
		$score = count($list) * 100 / count($items);

		// 5. 分值和错误答案的正确选择，并返回奖励序列号，写入result

		$msg = '';
		$insert['openid'] = session('SCOPENID');
		$insert['username'] = session('exam_name');
		$insert['phone'] = session('exam_phone');
		$insert['examid'] = input('examid');
		$insert['mid'] = 0;
		$insert['score'] = $score;
		$insert['createtime'] = time();
		if ($exam['examor'] == 0 && $exam['mark'] <= $score) {
			$insert['code'] = $insert['createtime'];
			$insert['isvaliable'] = 0;
			$msg = '真厉害，共您答对' . count($list) . '道题，得分' . $score . '分！';
			if ($exam['havegit'] === 1) {
				sendsmsmsg('感谢您参与' . $exam['name'] . ',凭此电子券【' . $insert['code'] . '.' . $exam['id'] . '】可前往活动指定地点领取礼品一份', $insert['phone']);
			}
		}
		if ($exam['examor'] == 0 && $exam['mark'] > $score) {

			$insert['code'] = '';
			$insert['isvaliable'] = 0;
			$msg = '真抱歉，共您答对' . count($list) . '道题，得分' . $score . '分！';
		}

		if ($exam['examor'] == 1) {
			$insert['code'] = $insert['createtime'];
			$insert['isvaliable'] = 0;
			$msg = '感謝您参与本次调查活动';
			if ($exam['havegit'] === 1) {
				sendsmsmsg('感谢您参与' . $exam['name'] . ',凭此电子券可千万活动指定地点领取礼品一份，【' . $insert['code'] . '】', $insert['phone']);
			}
		}

		$insert['detail'] = input('data');

		$id = Db::name('wx_exam_result')->insertGetId($insert);
		if ($id > 0) {
			return json([
				'code' => 1,
				'data' => url('answer') . '?id=' . $id,
				'msg' => $msg,
			]);
		}

		return json([
			'code' => -1,
			'msg' => $msg,
		]);
	}

	/*
	 * 前台兑奖
	 */
	public function valiexam() {
		// id openid 答题者的微信id username phone mid score 分数 createtime code 领奖代码 isvaliable 是否兑了奖
		if (request()->isPost()) {
			$string = input('code');

			if (!$string) {
				$this->assign('result', '兑奖码不正确');
				return $this->fetch();
			}

			$array = explode('.', $string);
			if (count($array) != 2) {
				$this->assign('result', '兑奖码不正确');
				return $this->fetch();
			}

			$code = $array[0];
			$examid = $array[1];

			$data = Db::name('wx_exam_result')->alias('a')
				->join('think_wx_exam b', 'a.examid=b.id')
				->field('a.id,a.username,a.phone,a.isvaliable,b.name,b.description')
				->where('a.code', $code)
				->where('b.id', $examid)
				->find();

			if ($data == null || $data['isvaliable'] == 1) {
				$this->assign('result', '兑奖码不正确或已使用');
				return $this->fetch();
			}

			$result = Db::name('wx_exam_result')->where('id', $data['id'])->update([
				'isvaliable' => 1,
			]);

			if ($result) {

				$this->assign('result', '网友：' . $data['username'] . '<br/>电话：' . $data['phone'] . '<br/>参加活动【' . $data['name'] . '】经验证【有效】<br/><h3>活动简介</h3><br/>' . $data['description']);
				return $this->fetch();
			}

			$this->assign('result', '网络异常，请刷新本页面再试！');
			return $this->fetch();
		}

		$this->assign('result', '');
		return $this->fetch();
	}
}