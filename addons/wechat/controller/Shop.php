<?php
namespace addons\wechat\controller;

use think\Db;

class Shop extends Base
{

    public function index()
    {}

    public function apply()
    {
        if (request()->isPost()) {
            $data = input('post.');
            $topic_img = $data['shopphoto'];
            $topic_imgs = explode(',', $data['shopphotos']);
            
            unset($data['shopphotos']);
            
            $imgstr = '';
            if (count($topic_imgs) > 1) {
                foreach ($topic_imgs as $item) {
                    $imgstr .= '<img src="' . $item . '" alt="' . $data['shopname'] . '"/><br/>';
                }
            }
            
            $description = mb_substr(strip_tags($data['shopdetail']), 0, 60, 'utf-8');
            $data['description'] = str_replace(PHP_EOL, '', $description);
            $data['shopdetail'] = $imgstr . $data['shopdetail'];
            $data['createdtime'] = date('Y-m-d');
            
            $data['address'] = str_replace('铜梁县', '铜梁区', $data['address']);
            $data['otheraddress'] = str_replace('铜梁县', '铜梁区', $data['otheraddress']);
            
            $shop = new \app\wechat\model\ShopModel();
            $json = $shop->saveShop($data, $topic_imgs);
            
            if ($json['code'] == 1) {
                $shopid = $json['data'];
            }
            
            $this->success('发布成功');
        }
        
        $openid = input('get.openid');
        if (empty($openid)) {
            $this->error('请关注公众号“铜梁视窗”回复“拍店铺””', url('index'));
        }
        
        $wxapp = $this->wxapp()->user;
        $userjson = $wxapp->get($openid);
        
        if ($userjson == null || key_exists('errcode', $userjson)) {
            $this->error('请关注公众号“铜梁视窗”回复“拍店铺”', url('index'));
        }
        
        $user = $userjson;
        if ($user == null || key_exists('errcode', $user)) {
            $this->error('请关注公众号“铜梁视窗”后回复“拍店铺”', url('index'));
        }
        
        $userauth = new Auth();
        $result = $userauth->wxlogin($user);
        
        $this->assign('openid', $user['openid']);
        $this->assign('nickname', $user['nickname']);
        $this->assign('headimgurl', $user['headimgurl']);
        
        $shops = Db::name('cms_type')->where('channel', 'shop')->select();
        $this->assign('types', $shops);
        
        return $this->fetch();
    }
}