<?php
namespace addons\wechat\controller;

use think\Db;
use addons\Base;

class Admin extends Base
{
    /**
     * 微信应用入口
     *
     * @return void
     * @author dmakecn@163.com
     * @since 2022-04-27
     */
    public function index(){
        return $this->fetch();
    }    
}
