<?php
namespace addons\wechat\controller;

use think\addons\Controller;
use think\Db;

class Auth extends Controller {

	/*
	 * 约定state这个词搞4个参数128个字节用_来拼接：controller_action_type_id
	 *
	 */
	public function index() {
		$state = input('state');
		$array = explode(WX_AUTHSPLIT, $state);
		$controller = $array[0];
		$action = $array[1];
		$type = $array[2];
		$dataid = $array[3];

		// 1. 拿到token，如果最终还加了个appid参数，那就只指定公众号授权，取值
		$config = null;
		if (count($array) == 5) {
			$config = Db::name('op_app')->where('appid', $array[4])->find();
		} else {
			$config = config('wxauth')['authwx'];
		}

		// 支付功能必须全部走支付账号配置
		if ($controller == 'pay') {
			$config = config('wxauth')['authwechatpay'];
		}

		$token = wx_get_access_token($config);

		// file_put_contents('/tok.txt', $token);

		// 2. 拿详细信息了
		$userinfo = wx_get_userinfo($token);

		// file_put_contents('/inf.txt', $userinfo);

		// 3. 检查代码
		if (array_key_exists('errcode', $userinfo)) {
			$this->error(json_encode($userinfo), null, null, 10);
		}

		$url = '';
		switch ($controller) {
		case "pai": // 随手拍应用
			if ($action == 'item') {
				$url = addon_url('wechat://pai/item') . '?id=' . $dataid;
			}

			self::checksub($url, $userinfo);

			break;
		case 'exam': //在线考试应用
			if ($action == 'index') {
				$url = addon_url('wechat://exam/index') . '?id=' . $dataid;
			}

			self::checksub($url, $userinfo);
			break;
		case "vote": //投票应用
			if ($action == 'item') {
				$data = explode('.', $dataid);
				$url = addon_url('wechat://vote/item') . '?voteid=' . $data[0] . '&voteitemid=' . $data[1];
			}
			if ($action == "index") {
				$url = addon_url('wechat://vote/index') . '?voteid=' . $dataid;
			}

			self::checksub($url, $userinfo);
			break;
		case "pay": //支付订单
			if ($action == 'luckmoney' && $type == '1') {
				$url = addon_url('wechat://pay/luckmoney') . '?id=' . $dataid;
			}
			break;
		case "read": //阅读有奖
			if ($type == 0) {
				$url = addon_url('wechat://read/' . $action) . '?id=' . $dataid . '&openid=' . $userinfo['openid'];
			}
			if ($type == 1) {
				$ids = explode('&', $dataid);
				$url = addon_url('wechat://read/' . $action) . '?id=' . $ids[0] . '&openid=' . $ids[1] . '&logid=' . $ids[1];
			}
			break;
		case "signin": //登录系统，用于绑定了个人微信的用户
			if ($type == 1) {
				$data = explode('.', $dataid);
				$url = addon_url('wechat://signin/' . $action) . '?openid=' . $userinfo['openid'] . '&gh_openid=' . $data[0] . '&id=' . $data[1];
			} else {
				$url = addon_url('wechat://signin/' . $action) . '?openid=' . $userinfo['openid'] . '&id=' . $dataid;
			}
			break;
		case "tickt": //优惠券，用于哪儿
			if ($action == "item") {
				$url = addon_url('wechat://tickt/' . $action) . '?tid=' . $dataid;
			} else {
				$url = addon_url('wechat://tickt/' . $action) . '?code=' . $dataid;
			}
			break;
		default:
			$url = 'https://www.dmake.cn';
			break;
		}

		$result = self::wxlogin($userinfo, true);

		// 4.绑定信息获登录
		if ($result) {
			return redirect($url);
		}

		return $this->error('微信登录失败');
	}

	public function checksub($url, $userinfo) {
		if (!key_exists('subscribe', $userinfo) || $userinfo['subscribe'] === 0) {
			session('SCJUMPTO', $url);
		}
	}

	public function wxlogin($user, $needback = false, $tlsc = 0) {
		$openid = $user['openid'];

		$user['openid_d'] = '';
		$user['openid_f'] = '';

		unset($user['openid']);
		unset($user['privilege']);
		if (array_key_exists('subscribe_scene', $user)) {
			unset($user['subscribe_scene']);
		}

		if (array_key_exists('qr_scene', $user)) {
			unset($user['qr_scene']);
		}

		unset($user['qr_scene_str']);

		$db = Db::name('wx_member');
		$member = null;

		// 再三判断是不是铜梁视窗的id
		if (strstr($openid, 'oN5FQu')) {
			$tlsc = 1;
		}

		if ($tlsc == 1) {
			$user['openid_d'] = $openid;
			$member = $db->where('openid_d', $openid)->find();
		} else {
			$user['openid_f'] = $openid;
			$member = $db->where('openid_f', $openid)->find();
		}

		$result = true;

		$user['lastlogintime'] = time();
		if ($member == null) {
			$user['tel'] = '';
			$retuslt = $db->insert($user);
		} else {
			$user['tel'] = $member['tel'];
			if ($tlsc == 1) {
				$result = $db->where('openid_d', $openid)->update($user);
			} else {
				$result = $db->where('openid_f', $openid)->update($user);
			}
		}

		if ($result) {
			session('SCOPENID', $openid);
			session('SCNICKNAME', $user['nickname']);
			session('SCHEADIMG', $user['headimgurl']);
			session('SCTEL', $user['tel']);
		}

		if ($needback == true) {
			return true;
		}

		return true;
	}
}