<?php
namespace addons\wechat\controller;

use addons\wechat\model\OrderModel;
use EasyWeChat\Factory;
use think\addons\Controller;

/**
 * 这个类就用来收钱，铜梁生活馆这个配置
 *
 * @author Daneas
 *
 */
class Pay extends Controller {

	public function index() {
		$orderid = input('get.id');

		if (strlen($orderid) > 5) {
			echo 'BAD REQUEST';
			exit();
		}

		$orderm = new OrderModel();
		$realorder = $orderm->getOrder($orderid);

		if (!$realorder) {
			return false;
		}

		$attributes = [
			'trade_type' => 'JSAPI', // JSAPI，NATIVE，APP...
			'body' => $realorder['body'],
			'detail' => $realorder['detail'],
			'out_trade_no' => $realorder['out_trade_no'],
			'total_fee' => $realorder['total_fee'], // 单位：分
			'notify_url' => url('callback'), // 支付结果通知网址，如果不设置则会使用配置里的默认地址
			'openid' => $realorder['openid'],
		];

		$payment = self::payparam();

		$result = $payment->order->unify($attributes);

		if ($result['return_code'] == 'SUCCESS' && $result['result_code'] == 'SUCCESS') {
			$prepayId = $result['prepay_id'];
			$realorder['prepay_id'] = $prepayId;
			$realorder['return_code'] = 'SUCCESS';
			$orderm->saveOrder($realorder);

			$js = self::wxapp()->jssdk;
			$this->assign('js', $js);

			$config = $payment->jssdk->sdkConfig($prepayId);
			$this->assign('config', $config);
			$this->assign('order', $realorder);
			$this->assign('items', $orderm->getOrderdetail($orderid));
			return $this->fetch();
		} else {
			return dump($result);
		}
	}

	public function callback() {
		$response = self::payparam()->handlePaidNotify(function ($notify, $successful) {

			$orderm = new OrderModel();

			// 使用通知里的 "微信支付订单号" 或者 "商户订单号" 去自己的数据库找到订单
			$order = $orderm->getOrder(0, $notify['out_trade_no']);

			if (!$order) { // 如果订单不存在
				return 'Order not exist.'; // 告诉微信，我已经处理完了，订单没找到，别再通知我了
			}

			// 假设订单字段“支付时间”不为空代表已经支付,则不更新了
			if ($order['paid_at'] != null && $order['paid_at'] > 0) {
				return true;
			}

			// 用户是否支付成功
			if (!$successful) {
				// 用户支付失败
				$order['status'] = 'paid_fail';
			} else {
				// 这里处理订单后的问题，如果失败的话微信会自动重新触发这个事件

				if (!$orderm->processCallback($order)) {
					return 'PROCESS FAILD';
				}

				// 不是已经支付状态则修改为已经支付状态
				$order['paid_at'] = time();
				$order['status'] = 'paid';
			}

			return $orderm->saveOrder($order);
		});

		$response->send();
	}

	public function luckmoney() {
		$orderid = input('get.id');

		if (strlen($orderid) > 5) {
			echo 'BAD REQUEST';
			exit();
		}

		$luckey = new OrderModel();
		$order = $luckey->getOrder($orderid);

		if ($order == null || $order['paid_at'] > 0) {
			echo '红包已失效';
			exit();
		}

		$order['openid'] = strlen($order['openid']) > 10 ? $order['openid'] : session('SCOPENID');
		$order['nickname'] = session('SCNICKNAME');

		if (input('get.act') === 'draw' && intval(input('get.token')) > time()) { // && strlen(input('get.kas') == 32)

			if (md5($order['openid']) != input('get.kas')) {
				return json([
					'code' => -1,
					'data' => '',
					'msg' => '身份校验无效',
				]);
			}

			if ($order['paid_at'] > 0) {
				return json([
					'code' => -1,
					'data' => '',
					'msg' => '这个红包已经被领过了哟，重领无效!',
				]);
			}

			if ($order['total_fee'] == 0) {
				return json([
					'code' => -1,
					'data' => '',
					'msg' => '本次抽奖您未获奖，不要灰心明天再来!',
				]);
			}

			$result = $this->wepay($order);
			$tradno = $result['mch_billno'];
			$openid = $result['re_openid'];

			if ($result['result_code'] == 'SUCCESS') {
				$order['paid_at'] = time();
				$order['status'] = 'paid';
				$luckey->saveOrder($order);

				return json([
					'code' => 1,
					'data' => '',
					'msg' => $result['return_msg'] . '，感谢您参与本次活动',
				]);
			} else {
				return json([
					'code' => -1,
					'data' => $result['err_code'],
					'msg' => $result['return_msg'] . '[code:' . $result['err_code'] . ']',
				]);
			}
		} else {
			if ($order['total_fee'] == 0 && $order['paid_at'] < 1) {
				$order['paid_at'] = time();
				$order['status'] = 'paid';
				$luckey->saveOrder($order);
			}
			$this->assign('order', $order);
			$this->assign('token', time() + 120);
			$this->assign('kas', md5($order['openid']));
			$this->assign('list', $luckey->getLucklog());
			return $this->fetch();
		}
	}

	/*
	 * 发送普通红包或者裂变红包
	 */
	private function wepay($order) {
		$payment = self::payparam();
		$redpack = $payment->redpack;
		$result = null;

		if ($order['type'] == 1) {
			$redpackData = [
				'mch_billno' => $order['out_trade_no'],
				'send_name' => $order['body'],
				're_openid' => $order['openid'],
				'total_num' => 1, // 固定为1，可不传
				'total_amount' => $order['total_fee'], // 单位为分，不小于100
				'wishing' => $order['detail'],
				'client_ip' => '', // 可不传，不传则由 SDK 取当前客户端 IP
				'act_name' => $order['tag'],
				'remark' => '关注铜梁视窗公众号，每天都能在文章中找到红包口令哟',
				'scene_id' => 'PRODUCT_2',
			];

			$result = $redpack->sendNormal($redpackData);
		}

		if ($order['type'] == 2) {
			$redpackData = [
				'mch_billno' => $order['out_trade_no'],
				'send_name' => $order['body'],
				're_openid' => $order['openid'],
				'total_num' => 3, // 不小于3
				'total_amount' => $order['total_fee'], // 单位为分，不小于300
				'wishing' => $order['detail'],
				'act_name' => $order['tag'],
				'remark' => '关注铜梁视窗公众号，每天都能在文章中找到红包口令哟',
				'amt_type' => 'ALL_RAND',
				'scene_id' => 'PRODUCT_2',
			];

			$result = $redpack->sendGroup($redpackData);
		}

		return $result;
	}

	private function wxapp() {
		$authwx = config('authwx');
		$options = [
			'debug' => false,
			'app_id' => $authwx['app_id'],
			'secret' => $authwx['secret'],
			'token' => $authwx['token'],
			'aes_key' => $authwx['aes_key'],
			'log' => [
				'level' => 'debug',
				'file' => './easywechat.log',
			],
		];

		$app = Factory::officialAccount($options);
		return $app;
	}

	private function payparam() {
		$authwechatpay = config('authwechatpay');
		$config = [
			// 前面的appid什么的也得保留哦
			'app_id' => $authwechatpay['app_id'],
			'mch_id' => $authwechatpay['mch_id'],
			'key' => $authwechatpay['key'],
			'cert_path' => WXCERT_PATH . 'apiclient_cert.pem', // XXX: 绝对路径！！！！  拿来做退款用的
			'key_path' => WXCERT_PATH . 'apiclient_key.pem', // XXX: 绝对路径！！！！   拿来做退款用的
			'notify_url' => url('callback'),
		]; // 你也可以在下单时单独设置来想覆盖它

		$payment = Factory::payment($config);
		return $payment;
	}
}