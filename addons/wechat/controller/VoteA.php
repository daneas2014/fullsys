<?php
namespace addons\wechat\controller;

use addons\Base;
use think\Db;

class VoteA extends Base {

	/**
	 * 活动列表
	 */
	public function index() {
		$cond = [];
		if (input('kw')) {
			$cond['action_name'] = ['like', "%" . input('kw') . "%"];
		}
		$db = Db::name('wx_voteimg');

		return $this->vueQuerySingle($db, $cond,'id desc',['domain'=>WX_AUTH_DOMAIN]);
	}

	/**
	 * 保存活动
	 */
	public function save() {
		if (request()->isPost()) {
			$type = input('stype');
			$id = input('id');
			$param = input('post.');
			unset($param['stype']);
			unset($param['file']);
			$result = false;

			switch ($type) {
			case "1":
				$db = Db::name('wx_voteimg');
				$param['apply_start_time'] = strtotime($param['apply_start_time']);
				$param['apply_end_time'] = strtotime($param['apply_end_time']);
				$param['start_time'] = strtotime($param['start_time']);
				$param['end_time'] = strtotime($param['end_time']);
				$result = false;

				if ($id > 0) {
					$result = $db->where('id', $id)->update($param);
				} else {
					$result = $db->insert($param);
				}

				return self::returncode($result);
			case "2":
				$db = Db::name('wx_voteimg');
				$result = $db->where('id', $id)->update($param);

				return self::returncode($result);
			case "3":
				$db = Db::name('wx_voteimg_prize');
				if ($id > 0) {
					$result = $db->where('id', $id)->update($param);
				} else {
					$result = $db->insertGetId($param);
				}
				return self::returncode($result);
			case "4":
				$db = Db::name('wx_voteimg_sponor');
				if ($id > 0) {
					$result = $db->update($param);
				} else {
					$result = $db->insertGetId($param);
				}
				return self::returncode($result);
			case "5":
				$db = Db::name('wx_voteimg_banner');
				if ($id > 0) {
					$result = $db->where('id', $id)->update($param);
				} else {
					$result = $db->insertGetId($param);
				}
				return self::returncode($result);
			case "6":
				$db = Db::name('wx_voteimg_bottom');
				if ($id > 0) {
					$result = $db->where('id', $id)->update($param);
				} else {
					$result = $db->insertGetId($param);
				}
				return self::returncode($result);
			}
		}

		$id = input('id');

		$vote = Db::name('wx_voteimg')->where('id', $id)->find();
		$prize = Db::name('wx_voteimg_prize')->where('vote_id', $id)->select();
		$sponor = Db::name('wx_voteimg_sponor')->where('vote_id', $id)->select();
		$banner = Db::name('wx_voteimg_banner')->where('vote_id', $id)->select();
		$menu = Db::name('wx_voteimg_bottom')->where('vote_id', $id)->select();

		$this->assign([
			'vote' => $vote,
			'prize' => $prize,
			'sponor' => $sponor,
			'banner' => $banner,
			'menu' => $menu,
			'other' => (5 - count($menu)),
		]);
		return $this->fetch();
	}

	/**
	 * 删除活动
	 */
	public function delete() {}

	public function returncode($data) {
		if ($data == null) {
			return json(['code' => -1, 'data' => null, 'msg' => '保存失败']);
		}

		return json(['code' => 1, 'data' => $data, 'msg' => '保存完毕']);
	}

	/**
	 * 投票选项
	 */
	public function apply() {
		$voteid = input('voteid');
		$db = Db::name('wx_voteimg_item');
		$cond['vote_id'] = $voteid;

		if (input('kw')) {
			$cond['vote_title'] = ['like', "%" . input('kw') . "%"];
		}

		return $this->vueQuerySingle($db, $cond, 'id desc', ['voteid' => $voteid]);
	}

	/**
	 * 查看投票记录
	 */
	public function recode() {}

	/**
	 * 修改投票选项
	 */
	public function saveitem() {
		if (request()->isAjax()) {
			$param = input('post.');
			if (array_key_exists('admin', $param)) {
				$param['wecha_id'] = 'admin';
				unset($param['admin']);
			}
			if (array_key_exists('file', $param)) {
				unset($param['file']);
			}

			$db = Db::name('wx_voteimg_item');

			$result = false;

			if ($param['id'] > 0) {
				$result = $db->where('id', $param['id'])->update($param);
			} else {
				$result = $db->insert($param);
			}

			if ($result) {
				return json([
					'code' => 1,
					'data' => '',
					'msg' => '保存投票选项完毕',
				]);
			} else {
				return json([
					'code' => -1,
					'data' => '',
					'msg' => '保存投票选项失败',
				]);
			}
		}

		$id = input('id');
		$voteid = input('voteid');
		$item = Db::name('wx_voteimg_item')->where('id', $id)->find();
		$this->assign('data', $item);
		$this->assign('voteid', $voteid);
		return $this->fetch();
	}

	/**
	 * 删除投票选项:要删除投票item和recodes
	 */
	public function deletitem() {
		$id = input('id');

		$result = Db::name('wx_voteimg_item')->where('id', $id)->delete();

		if ($result) {
			$this->success('选项已删除');
		} else {

			$this->error('选项删除失败');
		}
	}

	/**
	 * 奖品列表和
	 */
	public function prize() {}

	/**
	 * 赞助商
	 */
	public function sponor() {}
}
