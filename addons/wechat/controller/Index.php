<?php
namespace addons\wechat\controller;

use EasyWeChat\Factory;
use EasyWeChat\Kernel\Messages\Image;
use EasyWeChat\Kernel\Messages\Video;
use think\Db;

class Index extends Base {

	/**
	 * 意思就是1个平台对应1个公众号，如果是开放平台的话，在app\openflat里面的，和这个完全分离
	 * wechat和openflat就是为了解决个体和群体的问题，dmake是平台类型，所以群体问题还是放在主要应用里面
	 *
	 * @var array
	 * @author dmakecn@163.com
	 * @since 2022-05-07
	 */
	protected $config = [];

	public function _initialize() {
		$this->config = config('wxauth')['authwx'];
	}

	/**
	 * 文本回复
	 */
	public function index() {
		$echoStr = input("get.echostr") ? input('get.echostr') : false;

		if ($echoStr && $echoStr != '') {
			if ($this->checkSignature() == true) {
				ob_clean();
				die($echoStr);
			} else {
				die('fail');
			}
		}

		$config = $this->config;

		$options = [
			'debug' => false,
			'app_id' => $config['app_id'],
			'secret' => $config['secret'],
			'token' => $config['token'],
			'aes_key' => $config['aes_key'],
			'response_type' => 'array',
			'log' => [
				'level' => 'error',
				'file' => './easywechat.log',
			],
		];

		$app = Factory::officialAccount($options);

		$app->server->push(function ($message) {

			$wxid = $message['ToUserName'];
			$openid = $message['FromUserName'];

			switch ($message['MsgType']) {
			case 'event':
				$event = new \addons\wechat\model\ServiceEvent();
				return $event->processEvent($wxid, $openid, $message, $this->wxapp());
				break;
			case 'text':
				$text = new \addons\wechat\model\ServiceProcess();
				return $text->processPutTxt($wxid, $openid, $message['Content']);
				break;
			case 'image':
				$mediaId = $message->MediaId;
				return new Image($mediaId);
				break;
			case 'voice':

				if ($message['Recognition']) {
					$regstr = '/[[:punct:]]|[[:lower:]]|。|，|,/i';
					$word = preg_replace($regstr, '', $message['Recognition']);
					$one = Db::name('wx_luckcode')->where('code', $word)
						->where('gh_id', $wxid)
						->find(); // 还剩余多少现金红包

					if ($one != null && $one['leftnum'] > 0) {
						$group = new \addons\wechat\model\ServiceLuckmoney();
						return $group->ProcessCode($wxid, $word, $openid);
					}

					if (strstr($word, '天王盖地虎') || strstr($word, '口令')) {
						$word = str_replace('天王盖地虎', '', $word);
						$word = str_replace('口令', '', $word);
						$word = str_replace(' ', '', $word);
						$group = new \addons\wechat\model\ServiceLuckmoney();
						return $group->ProcessCode($wxid, $word, $openid);
					} else {
						$text = new \addons\wechat\model\ServiceProcess();
						return $text->processPutTxt($wxid, $openid, $word);
					}
				} else {
					return '亲，我只是个机器人啊，听不懂你说什么啊！';
				}

				break;
			case 'video':
				$mediaId = $message->MediaId;
				return new Video($mediaId);
				break;
			case 'location':
				$map = new \addons\wechat\model\ServiceMap();
				return '收到坐标消息';
				break;
			case 'link':
				return '收到链接消息';
				break;
			default:
				return '收到其它消息';
				break;
			}
		});

		$response = $app->server->serve();

		$response->send();
	}

	private function checkSignature() {
		$signature = $_GET["signature"];
		$timestamp = $_GET["timestamp"];
		$nonce = $_GET["nonce"];

		$config = $this->config;

		$token = $config['token'];

		$tmpArr = array(
			$token,
			$timestamp,
			$nonce,
		);

		sort($tmpArr, SORT_STRING);
		$tmpStr = implode($tmpArr);
		$tmpStr = sha1($tmpStr);

		if ($tmpStr == $signature) {
			return true;
		} else {
			return false;
		}
	}
}