<?php
namespace addons\wechat\controller;

use addons\Base;

class Member extends Base
{

    /**
     * 个人中心，展示内容轨迹：随手拍留言和打分轨迹，考试轨迹，随手拍轨迹
     */
    public function index()
    {
        $openid = session('SCOPENID');
        if (! $openid) {
            return redirect(url(''));
        }
    }
}