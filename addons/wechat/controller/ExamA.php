<?php
namespace addons\wechat\controller;

use addons\Base;
use think\Db;

class ExamA extends Base {

	/*
	 * 考试列表
	 */
	public function index() {
		// id name keywords description createtime endtime sendmsg showpic
		$kw = input('kw');

		$cond = null;

		if ($kw != null) {
			$cond['name'] = $kw;
		}

		$db = Db::name('wx_exam');

		return $this->vueQuerySingle($db, $cond);
	}

	/*
	 * 新增考试、编辑考试
	 */
	public function saveexam() {
		$db = Db::name('wx_exam');
		if (request()->isPost()) {
			$param = input('post.');

			unset($param['file']);

			$result = false;
			$param['endtime'] = strtotime($param['endtime']);

			if ($param['id'] > 0) {
				$result = $db->where('id', $param['id'])->update($param);
			} else {
				$param['createtime'] = time();
				$result = $db->insert($param);
			}

			if ($result) {
				return json(['code' => 1, 'msg' => '考试|调查问卷保存完毕']);
			}

			return json(['code' => -1, 'msg' => '保存失败']);
		}

		$id = input('get.id');
		$item = null;
		if ($id > 0) {
			$item = $db->where('id', $id)->find();
		}

		$this->assign('exam', $item);
		return $this->fetch();
	}

	/*
	 * 删除考试
	 */
	public function delexam() {
		$id = input('id');

		Db::name('wx_exam')->where('id', $id)->delete();

		Db::name('wx_exam_item')->where('examid', $id)->delete();

		return json([
			'code' => 1,
			'msg' => '考试和试题内容已经删除完毕',
		]);
	}

	/*
	 * 考试题目
	 */
	public function items() {
		// id examid title choose1 choose2 choose3 choose4 rightanswer
		$cond['examid'] = input('examid');

		$db = Db::name('wx_exam_item');

		return $this->vueQuerySingle($db, $cond, 'id desc', ['examid' => $cond['examid']]);
	}

	/*
	 * 新增、编辑考试选项
	 */
	public function saveitem() {
		$db = Db::name('wx_exam_item');
		if (request()->isPost()) {
			$param = input('post.');

			$result = false;

			if ($param['id'] > 0) {
				$result = $db->where('id', $param['id'])->update($param);
			} else {
				$result = $db->insert($param);
			}

			if ($result) {
				return json(['code' => 1, 'msg' => '选项已保存']);
			}

			return json(['code' => -1, 'msg' => '保存失败']);
		}

		$examid = input('examid');
		$id = input('id');
		$item = null;
		if ($id > 0) {
			$item = $db->where('id', $id)->find();
		}

		$this->assign([
			'item' => $item,
			'examid' => $examid,
		]);
		return $this->fetch();
	}

	/*
	 * 删除选项
	 */
	public function delitem() {
		$id = input('id');

		Db::name('wx_exam_item')->where('id', $id)->delete();

		return json([
			'code' => 1,
			'msg' => '试题内容已经删除完毕',
		]);
	}

	public function valiexam() {
		// id openid 答题者的微信id username phone mid score 分数 createtime code 领奖代码 isvaliable 是否兑了奖 ，detail
		$cond['examid'] = input('examid');

		$kw = input('kw');
		if ($kw != null) {
			$cond['username'] = $kw;
		}

		$db = Db::name('wx_exam_result');

		return $this->vueQuerySingle($db, $cond, 'id desc', ['examid' => $cond['examid']]);
	}

	/*
	 * 考卷详情
	 */
	public function validetail() {
		$data = Db::name('wx_exam_result')->where('id', input('id'))->find();

		$exam = Db::name('wx_exam')->where('id', $data['examid'])->find();

		$items = Db::name('wx_exam_item')->where('examid', $data['examid'])->select();

		$result = json_decode($data['detail'], true);

		$list = [];

		foreach ($items as $item) {
			foreach ($result as $child) {
				if ($item['id'] == $child['itemid']) {
					$item['selected'] = $child['selected'];
					$list[] = $item;
				}
			}
		}

		$this->assign([
			'exam' => $exam,
			'list' => $list,
			'data' => $data,
		]);

		return $this->fetch();
	}
}