<?php
namespace addons\wechat\controller;

use addons\Base;
use think\Db;
use addons\wechat\model\ServiceTick;

class Tickt extends Base
{

    private function getoauth($type, $code)
    {
        $split = WX_AUTHSPLIT;
        $url = '';
        
        switch ($type) {
            case "item":
                $url = wx_get_code('tickt' . $split . 'item' . $split . '0' . $split . $code, url('auth/index'), config('wxas')[WX_APPID]);
                break;
            case "recive":
                $url = wx_get_code('tickt' . $split . 'recive' . $split . '0' . $split . $code, url('auth/index'), config('wxas')[WX_APPID]);
                break;
            case "cost":
                $url = wx_get_code('tickt' . $split . 'cost' . $split . '0' . $split . $code, url('auth/index'), config('wxas')[WX_APPID]);
                break;
            case "mytickts":
                $url = wx_get_code('tickt' . $split . 'mytickts' . $split . '0' . $split . $code, url('auth/index'), config('wxas')[WX_APPID]);
                break;
            case "mycommission":
                $url = wx_get_code('tickt' . $split . 'mycommission' . $split . '0' . $split . $code, url('auth/index'), config('wxas')[WX_APPID]);
                break;
        }
        
        return $url;
    }

    /*
     * 列举出所有券
     */
    public function index()
    {
        // session('SCOPENID', 'o73cGwZLQw0XSG_7qJ9abHuH_sEI');
        $tickts = Db::name('op_tickts')->alias('a')
            ->join('think_mall b', 'a.shopid=b.shopid')
            ->field('a.*,b.address,b.lnglat,b.bits,b.shopphoto')
            ->order('a.deadtime desc')
            ->select();
        
        $base = new Base();
        $app = $base->wxapp();
        $js = $app->jssdk;
        $this->assign('js', $js);
        
        $this->assign('tickts', $tickts);
        return $this->fetch();
    }

    /*
     * 分享页面票票
     */
    public function item()
    {
        $id = input('get.tid');
        
        $openid = session('SCOPENID');
        
        if ($openid == null) {
            return redirect($this->getoauth('item', $id));
        }
        
        // 1.检查权限，看有资格分享券木有
        
        $res = Db::name('mall')->where('verifyopenid', 'like', '%' . $openid . '%')->count();
        
        $tickts = Db::name('op_tickts')->where('id', $id)->find();
        
        $shop = Db::name('mall')->where('shopid', $tickts['shopid'])->find();
        
        $this->assign([
            'tickts' => $tickts,
            'shop' => $shop,
            'usertype' => ($res > 0 ? 'share' : 'recive')
        ]);
        
        $base = new Base();
        $app = $base->wxapp();
        $js = $app->jssdk;
        $this->assign('js', $js);
        
        return $this->fetch();
    }

    /*
     * 分享post
     */
    public function share()
    {
        if (request()->isPost()) {
            $openid = session('SCOPENID');
            $id = input('post.id');
            
            if ($openid == null) {
                return redirect($this->getoauth('item', $id));
            }
            $service = new ServiceTick();
            $openid = session('SCOPENID');
            return $service->createTick($openid, $id);
        }
    }

    /*
     * 积分换券
     */
    public function cointo()
    {
        if (request()->isPost()) {
            $openid = session('SCOPENID');
            $id = input('post.id');
            
            if ($openid == null) {
                return redirect($this->getoauth('item', $id));
            }
            $service = new ServiceTick();
            $openid = session('SCOPENID');
            return $service->coinTick($openid, $id);
        }
    }

    /*
     * 会员接受
     */
    public function recive()
    {
        $code = input('get.code');
        
        $openid = session('SCOPENID');
        
        if ($openid == null) {
            return redirect($this->getoauth('recive', $code));
        }
        
        // 1. 是否关注平台
        $ife = Db::name('op_appuser')->where('openid', $openid)->count();
        if ($ife < 1) {
            return $this->error("<div>您还未关注铜梁生活馆公众号，识别下方二维码即可关注使用本特惠券！</div><div><img src='http://www.5atl.com/weixin.jpg'/></div>");
        }
        
        // 2. 接收券
        $service = new ServiceTick();
        $json = $service->getTick($openid, $code);
        
        if ($json) {
            return redirect(url('info') . '?code=' . $code);
        }
        
        return $this->error("<div>接收特惠券失败，请刷新本页面重试！</div>");
    }

    /*
     * 会员打开的核销页面
     */
    public function info()
    {
        $code = input('get.code');
        
        $openid = session('SCOPENID');
        
        if ($openid == null) {
            return redirect($this->getoauth('cost', $code));
        }
        
        // 1. 去除券
        $db = Db::name('op_tickt');
        
        $tickt = $db->alias('a')
            ->join('think_op_tickts b', 'a.ticksid=b.id')
            ->field('a.*,b.title,b.adimg,b.adurl,b.summry,b.surplus,b.total,b.shopname,b.deadtime,shopid')
            ->where('code', $code)
            ->where('b.deadtime', '>', time())
            ->find();
        
        if ($tickt == null) {
            return $this->error('特惠券已失效', url('index'), null, 10);
        }
        
        if ($tickt['usedtime'] > 0) {
            return $this->success(date('Y-m-d H:i:s', $tickt['usedtime']) . '券已核销', url('index'), null, 10);
        }
        
        $shop = Db::name('mall')->where('shopid', $tickt['shopid'])->find();
        
        // 2.验证归属
        
        if (! strstr($tickt['openid'], $openid)) {
            return $this->error('<div>您不是这张券的主人，您得想办法找到铜梁视窗联盟商家送您一张</div>', url('index'));
        }
        
        $this->assign('tickt', $tickt);
        $this->assign('shop', $shop);
        
        return $this->fetch();
    }

    public function checkstatus()
    {
        if (request()->isPost()) {
            $code = input('post.code');
            $time = input('post.time');
            
            $db = Db::name('op_tickt');
            $condition['code'] = $code;
            $condition['usedtime'] = [
                '>',
                0
            ];
            
            for ($i = 0; $i < $time; $i ++) {
                
                sleep(2);
                
                if ($db->where($condition)->count() > 0) {
                    return getJsonCode(true);
                }
            }
            
            return getJsonCode(false);
        }
    }

    /*
     * 店员扫码之后
     */
    public function cost()
    {
        $code = input('get.code');
        
        $openid = session('SCOPENID');
        
        if ($openid == null) {
            return redirect($this->getoauth('cost', $code));
        }
        
        // 1. 获得券
        $db = Db::name('op_tickt');
        
        $tickt = $db->alias('a')
            ->join('think_op_tickts b', 'a.ticksid=b.id')
            ->field('a.*,b.title,b.adimg,b.summry,b.surplus,b.total')
            ->where('code', $code)
            ->where('b.deadtime', '>', time())
            ->find();
        
        if ($tickt == null) {
            return $this->error('特惠券已失效', url('index'));
        }
        
        // 2. 验证身份有效性
        if (! strstr($tickt['checkopenid'], $openid)) {
            return $this->error('您不具备该券的核销权限', url('index'));
        }
        
        // 3. 核销
        $result = $db->where('code', $code)->update([
            'usedtime' => time(),
            'usedopenid' => $openid
        ]);
        if ($result) {
            
            // 清算佣金share,use,check
            $comissions = [];
            
            $comission['openid'] = $tickt['openid'];
            $comission['ticktsid'] = $tickt['ticksid'];
            $comission['ticktid'] = $tickt['id'];
            $comission['createtime'] = time();
            $comission['commission'] = $tickt['commission'] * 0.4;
            $comission['type'] = 'share';
            array_push($comissions, $comission);
            
            $comission['commission'] = $tickt['commission'] * 0.3;
            $comission['type'] = 'check';
            array_push($comissions, $comission);
            
            $comission['commission'] = $tickt['commission'] * 0.3;
            $comission['type'] = 'use';
            array_push($comissions, $comission);
            
            Db::name('op_ticktcommission')->insertAll($comissions);
            
            return $this->success('优惠券核销完成，加顾客微信好友分享其他优惠券给他，您还可以获得分享佣金哟！', url('index'));
        }
    }

    /**
     * 个人券包
     *
     * @return \think\response\Redirect|mixed|string
     */
    public function mytickts()
    {
        $openid = session('SCOPENID');
        
        if ($openid == null) {
            return redirect($this->getoauth('mytickts', 1));
        }
        
        $tickts = Db::name('op_tickt')->alias('a')
            ->join('think_op_tickts b', 'a.ticksid=b.id')
            ->join('think_mall c', 'c.shopid=b.shopid')
            ->field('a.*,c.address,c.lnglat,c.bits,c.shopphoto,b.title,b.condi,b.worth,b.surplus,b.total,b.shopname')
            ->where('a.openid', $openid)
            ->order('usedtime asc')
            ->select();
        
        $this->assign('tickts', $tickts);
        
        return $this->fetch();
    }

    /**
     * 个人佣金
     *
     * @return \think\response\Redirect|mixed|string
     */
    public function mycommission()
    {
        $openid = session('SCOPENID');
        
        if ($openid == null) {
            return redirect($this->getoauth('mycommission', 1));
        }
        
        $tickts = Db::name('op_ticktcommission')->alias('a')
            ->join('think_op_tickts b', 'a.ticktsid=b.id')
            ->where('a.openid', $openid)
            ->select();
        
        $this->assign('tickts', $tickts);
        
        return $this->fetch();
    }
}