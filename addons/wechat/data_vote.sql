-- ----------------------------
-- Table structure for wx_voteimg
-- ----------------------------
DROP TABLE IF EXISTS `wx_voteimg`;
CREATE TABLE `wx_voteimg`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '活动名称',
  `vote_type` int(11) NOT NULL DEFAULT 0 COMMENT '0是普通投票，1是海报投票',
  `action_desc` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '活动简介',
  `award_desc` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '参赛说明',
  `flow_desc` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '流程介绍',
  `join_desc` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '报名简介',
  `keyword` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '回复关键词',
  `reply_title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '回复标题',
  `reply_content` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '回复内容',
  `reply_pic` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '回复图片',
  `start_time` int(11) NOT NULL COMMENT '开始时间',
  `apply_start_time` int(11) NOT NULL COMMENT '报名开始时间',
  `end_time` int(11) NOT NULL COMMENT '结束时间',
  `apply_end_time` int(11) NOT NULL COMMENT '报名结束时间',
  `fman` int(11) NOT NULL DEFAULT 0 COMMENT '假人数',
  `fvote` int(11) NOT NULL DEFAULT 0 COMMENT '假票数',
  `is_follow` tinyint(1) NOT NULL DEFAULT 1 COMMENT '是否需要关注',
  `is_numtime` tinyint(1) NOT NULL DEFAULT 1 COMMENT '限制报名次数',
  `limit_vote` int(11) NOT NULL COMMENT '限制投票数',
  `limit_vote_day` int(11) NOT NULL COMMENT '限制每天投票数',
  `limit_vote_item` int(11) NOT NULL COMMENT '限制投票数',
  `phone` char(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '电话号码',
  `page_type` enum('waterfall','page') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'waterfall',
  `opentool` int(11) NOT NULL DEFAULT 1,
  `display` tinyint(1) NOT NULL COMMENT '分页数',
  `default_skin` tinyint(1) NOT NULL,
  `follow_msg` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `follow_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `self_status` tinyint(1) NOT NULL,
  `follow_btn_msg` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `register_msg` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `custom_sharetitle` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `custom_sharedsc` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `img_count` tinyint(1) NOT NULL COMMENT '上传图片数量',
  `need_check` tinyint(1) NOT NULL COMMENT '报名审核',
  `firstwidth` int(11) NOT NULL COMMENT '海报图宽',
  `firstheight` int(11) NOT NULL COMMENT '海报图宽',
  `watermark` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '海报水印',
  `color` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '配色',
  `luckm` int(11) NOT NULL DEFAULT 0 COMMENT '是否投票后送抽奖红包',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 18 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wx_voteimg
-- ----------------------------
INSERT INTO `wx_voteimg` VALUES (1, '微信投票测试', 0, '  \r\n									  <p>  \r\n									  本次活动是由铜梁视窗发起的投票系统测试活动，本系统旨在提高微信投票活动体验进行的技术改良测试。</p>									', '本次活动是由铜梁视窗发起的投票系统测试活动，本系统旨在提高微信投票活动体验进行的技术改良测试。', '  \r\n									  <p>  \r\n									  本次活动是由铜梁视窗发起的投票系统测试活动，本系统旨在提高微信投票活动体验进行的技术改良测试。</p>									', '  \r\n									  <p>关注微信公众号“铜梁视窗”即可参与</p>									', '系统测试', '铜梁视窗研发微信投票系统公测', '本次活动是由铜梁视窗发起的投票系统测试活动，本系统提高了投票过程中抗压能力，投票体验更好。欢迎广大商家和技术爱好者试用，如有意向请拨打底部菜单电话。', '/attachment/images/201704/10/1491800677.jpg', 1514736000, 1491062400, 1517500800, 1505836800, 0, 0, 1, 1, 0, 10, 0, '023-45655113', 'page', 1, 0, 0, '', '', 0, '', '', '', '', 10, 0, 0, 0, '', 'fen', 1);
INSERT INTO `wx_voteimg` VALUES (12, '铜梁首届大众小吃评选', 0, '<p class=\"MsoNormal\"><span>本次活动旨在将铜梁本地特色小吃一网搜集，</span><span>通过同本地的网友的亲身体验和评价为铜梁人民提供一个大众小吃的精准地图。</span></p>  \r\n									  									', '', '<p class=\"MsoNormal\"><span>1. 5月1日前邀约铜梁特色小吃报名，并主动搜罗未报名的但是在群众中有口碑的特色小吃。</span><span><o:p></o:p></span></p><p class=\"MsoNormal\"><span>2. 5月1日通过铜梁视窗微信公众号、微信群、qq群等方式邀约铜梁居民投票</span><span><o:p></o:p></span></p><p class=\"MsoNormal\"><span>3. 6月1日起铜梁视窗将为排名前20的铜梁本地大众小吃分2期制作推广专题，让更多的铜梁好吃嘴们一饱口福。</span></p>  \r\n									  									', '<p class=\"MsoNormal\"><!--[if !supportLists]--><span>1.&nbsp;</span><!--[endif]--><span><font face=\"宋体\">商家和网友可以通过铜梁视窗微信公众号报名参与，上传</font>1-5张特色小吃招聘和简介即可</span><span><o:p></o:p></span></p><p class=\"MsoNormal\"><!--[if !supportLists]--><span>2.&nbsp;</span><!--[endif]--><span>铜梁视窗活动组同时会将一些有口碑但是没有主动参与的铜梁美食邀约参与活动评比</span><span><o:p></o:p></span></p><p class=\"MsoNormal\"><span>3. 投票期间网友可以通过铜梁视窗微信公众号、朋友圈投票分享参与投票</span></p>  \r\n									  									', '大众小吃', '', '本次活动旨在将铜梁本地特色小吃一网搜集，通过同本地的网友的亲身体验和评价为铜梁人民提供一个大众小吃的精准地图。', 'http://images2015.cnblogs.com/blog/817457/201704/817457-20170415165209751-919065927.png', 1492099200, 1490976000, 1686758400, 1683734400, 0, 0, 1, 1, 0, 5, 0, '023-45655113', 'waterfall', 1, 0, 0, '', '', 0, '', '', '', '', 0, 0, 0, 0, '', 'red', 0);
INSERT INTO `wx_voteimg` VALUES (13, '爱要大声说出来', 1, '<p class=\"MsoNormal\"><font face=\"宋体\">本次活动是分享“我”最爱的那个人，爱是社会正能量，爱要大声说出来，爱要让全世界听到。网友通过参加本次活动直击心中深藏的爱，大胆的表达内心的爱意，拉进和最爱的人的距离。</font></p>  \r\n									  									', '', '<p class=\"MsoNormal\" align=\"justify\" style=\"text-align: justify;\">1. 5月1日起通过活动主办方、承办方、协办方的自媒体平台宣传本次活动，并在“铜梁视窗”微信公众号开始报名。</p><p class=\"MsoNormal\" align=\"justify\" style=\"text-align: justify;\">2. 5月15日起开始活动投票，同时也接受活动报名。&nbsp;</p><p class=\"MsoNormal\" align=\"justify\" style=\"text-align: justify;\">3. 5月20日23点59分停止活动投票，揭晓活动排名。&nbsp;</p><p class=\"MsoNormal\" align=\"justify\" style=\"text-align: justify;\">4. 5月21日至5月31日参与网友前往指定地点领取活动礼品。</p>  \r\n									  									', '<p>网友通过“铜梁视窗”微信公众号报名后，活动主办方生成个人海报，网友再通过分享海报至其朋友圈邀请点赞助力的方式，点赞助力的人根据排名将获得主办方不同的情谊礼包一份。</p>  \r\n									  									', '爱要大声说出来', '', '爱要大声说出来，对你的恩师、你的亲属、你的至尊闺蜜、你的铁干兄弟。', 'http://source.5atl.com/attachment/images/201705/06/1494057181.png', 1493654400, 1493654400, 1503849600, 1503849600, 10, 221, 1, 1, 0, 20, 0, '023-45655113', 'page', 1, 0, 0, '', '', 0, '', '', '', '', 0, 0, 720, 960, '/static/wechat/image/lovewater.png', 'red', 0);
INSERT INTO `wx_voteimg` VALUES (14, '万花筒2017“手拉手 共成长”文艺汇演投票活动', 0, '<p><span>投票方式：</span><br><span>⑴关注【万花筒艺术学校】微信公众号。</span><br><span>⑵打开铜梁区万花筒艺术学校公众号，点击“网络投票”即可参与2017“手拉手 共成长”文艺汇演投票页面投票。</span><br><span>⑶每个微信号每天可投3票，可对不同节目或同一节目进行投票。</span><br><span>⑷此次活动节目获奖由最终投票票数决定。</span><br><span>本次活动最终解释权归铜梁区万花筒艺术学校所有<br>奖品图片以实物为准，一下图片仅供参考</span></p>  \r\n									  									', '', '  \r\n									  									', '  \r\n									  									', '万花筒', '', '万花筒2017“手拉手 共成长”文艺汇演投票活动由“买车修车到昊川名车”的昊川名车赞助。', 'http://source.5atl.com/attachment/upload/201705/31/1496211447477.jpg', 1496246400, 1496246400, 1497528000, 1496246400, 782, 2348, 1, 1, 0, 3, 0, '023-45655113', 'waterfall', 1, 0, 0, '', '', 0, '', '', '', '', 0, 0, 0, 0, '', 'red', 0);
INSERT INTO `wx_voteimg` VALUES (15, '2017铜梁嘉和大润发广场首届汉唐婚博会“幸福新娘”评比活动', 0, '  \r\n									    \r\n									    \r\n									    \r\n									    \r\n									    \r\n									    \r\n									    \r\n									    \r\n									  <p>2017年9月2日至9月3日铜梁首届唐制婚博会暨铜梁嘉和大润发广场首届婚博会即将启程，报名参与最和谐伴侣评比赢婚庆大礼包！婚博会现场更有报名者福利环节哦！</p><p>本活动不限婚姻状态、照片形式，但必须满足是情侣或配偶合照。</p><p>感谢以下单位的参与</p><p><br></p><table class=\"layui-table\" style=\"font-size:.8em;\"><tbody><tr><td width=\"109\" valign=\"center\"><p><span>站台编号</span></p></td><td width=\"333\" valign=\"center\"><p><span>参展单位</span></p></td><td width=\"221\" valign=\"center\"><p><span>电话</span></p></td></tr><tr><td width=\"109\" valign=\"center\"><p><span>1号</span></p></td><td width=\"333\" valign=\"center\"><p><span>囍多多婚庆</span></p></td><td width=\"221\" valign=\"center\"><p><span>15334527889</span></p></td></tr><tr><td width=\"109\" valign=\"center\"><p><span>2号</span></p></td><td width=\"333\" valign=\"center\"><p><span>艺纱工作室</span></p></td><td width=\"221\" valign=\"center\"><p><span>18996309850</span></p></td></tr><tr><td width=\"109\" valign=\"center\"><p><span>3号</span></p></td><td width=\"333\" valign=\"center\"><p><span>新巴黎婚庆</span></p></td><td width=\"221\" valign=\"center\"><p><span>17784382470</span></p></td></tr><tr><td width=\"109\" valign=\"center\"><p><span>4号</span></p></td><td width=\"333\" valign=\"center\"><p><span>幸福约定主题婚礼</span></p></td><td width=\"221\" valign=\"center\"><p><span>13883756421</span></p></td></tr><tr><td width=\"109\" valign=\"center\"><p><span>5号</span></p></td><td width=\"333\" valign=\"center\"><p><span>千尚化妆摄影工作室</span></p></td><td width=\"221\" valign=\"center\"><p><span>15102396523</span></p></td></tr><tr><td width=\"109\" valign=\"center\"><p><span>6号</span></p></td><td width=\"333\" valign=\"center\"><p><span>维纳斯婚尚摄影</span></p></td><td width=\"221\" valign=\"center\"><p><span>18723129025</span></p></td></tr><tr><td width=\"109\" valign=\"center\"><p><span>7号</span></p></td><td width=\"333\" valign=\"center\"><p><span>丽雅婚庆馆</span></p></td><td width=\"221\" valign=\"center\"><p><span>17783994341</span></p></td></tr><tr><td width=\"109\" valign=\"center\"><p><span>8号</span></p></td><td width=\"333\" valign=\"center\"><p><span>童话镇婚礼工作室</span></p></td><td width=\"221\" valign=\"center\"><p><span>15702301988</span></p></td></tr><tr><td width=\"109\" valign=\"center\"><p><span>9号</span></p></td><td width=\"333\" valign=\"center\"><p><span>尚莲国际</span></p></td><td width=\"221\" valign=\"center\"><p><span>023-45589888</span></p></td></tr><tr><td width=\"109\" valign=\"center\"><p><span>10号</span></p></td><td width=\"333\" valign=\"center\"><p><span>爱恋婚纱·婚庆</span></p></td><td width=\"221\" valign=\"center\"><p><span>15223340518</span></p></td></tr><tr><td width=\"109\" valign=\"center\"><p><span>11号</span></p></td><td width=\"333\" valign=\"center\"><p><span>爱上爱婚礼定制</span></p></td><td width=\"221\" valign=\"center\"><p><span>18502349335</span></p></td></tr><tr><td width=\"109\" valign=\"center\"><p><span>12号</span></p></td><td width=\"333\" valign=\"center\"><p><span>博洋家纺</span></p></td><td width=\"221\" valign=\"center\"><p><span>18983906017</span></p></td></tr><tr><td width=\"109\" valign=\"center\"><p><span>13号</span></p></td><td width=\"333\" valign=\"center\"><p><span>U遇见婚庆</span></p></td><td width=\"221\" valign=\"center\"><p><span>15923340401</span></p></td></tr><tr><td width=\"109\" valign=\"center\"><p><span>14号</span></p></td><td width=\"333\" valign=\"center\"><p><span>爱意婚典</span></p></td><td width=\"221\" valign=\"center\"><p><span>15826184645</span></p></td></tr><tr><td width=\"109\" valign=\"center\"><p><span>15号</span></p></td><td width=\"333\" valign=\"center\"><p>同心赋婚庆用酒</p></td><td width=\"221\" valign=\"center\"><p><span>15680915529</span></p></td></tr></tbody></table><p><br></p><p><span>​</span></p>  \r\n									  																																																																																										', '', '  \r\n									    \r\n									    \r\n									    \r\n									    \r\n									    \r\n									    \r\n									    \r\n									    \r\n									  <p>1. 本次投票报名从即日起至2017年8月27日23点59分截至；</p><p>2. 本次投票从2017年8月23日0点至9月6日23点59分截至；</p><p>3. 凡是报名参与本次最美新娘投票的网友都可以于2017年9月2日晚7点起前往铜梁嘉和大润发广场婚博会现场领取来自14家婚庆、摄影、酒水商家提供的大礼包一份</p>																																																																																	', '  \r\n									    \r\n									    \r\n									    \r\n									    \r\n									    \r\n									    \r\n									    \r\n									  <p>1. 关注“铜梁视窗”微信公众号点击菜单=》婚博专题=》网上投票</p><p>2. 上传情侣或配偶合照。</p>																																																																								', '和谐伴侣', '', '3999元现金+18888元豪礼等你来拿！9月2/3日晚邀您亲至嘉和·大润发广场一起观赏唐朝婚礼盛宴......', 'http://source.5atl.com/attachment/images/201708/18/1503033672.png', 1503417600, 1502380800, 1504713599, 1503849599, 0, 0, 1, 1, 0, 3, 0, '023-45655113', 'page', 1, 0, 0, '', '', 0, '', '', '', '', 0, 0, 0, 0, '', 'fen', 0);
INSERT INTO `wx_voteimg` VALUES (16, '因为有你“柚”惑我心 龙韵果乡形象大使选拔', 0, '  \r\n									  <p>因为有你“柚”惑我心，上传你和柚子的合影参与选拔，有机会当免费农场主送柚子树三棵！</p>  \r\n									  																		', '', '  \r\n									  <p>2017年10月20日至2017年11月12日您可以关注“龙韵果乡”、“铜梁视窗”微信公众号报名参与本次活动；</p><p>2017年10月26日至2017年11月12日您可以在“龙韵果乡”、“铜梁视窗”微信公众号参与每日1次投票；</p><p>2017年11月14日获奖者一同驱车前往龙韵果乡挑选您的战利品：柚子树N棵。</p>									', '<p>  \r\n									  1. 报名期间您可以在公众号上传您与柚子的合照，获得您的活动参与海报</p><p>2. 投票期间在“龙韵果乡”、“铜梁视窗”微信公众号将您的投票页面或参与活动海报分享朋友圈即可</p>', '柚子', '', '因为有你“柚”惑我心，上传你和柚子的合影参与选拔，有机会当免费农场主送柚子树三棵！', 'http://source.5atl.com/attachment/images/201710/09/1507539652.jpg', 1508428800, 1508083200, 1636646400, 1636646400, 0, 0, 1, 1, 0, 1, 0, '13452803380', 'page', 1, 0, 0, '', '', 0, '', '', '', '', 0, 0, 0, 0, '', 'green', 0);
INSERT INTO `wx_voteimg` VALUES (17, '寻找魅力老板娘暨蝶姿杯魅力老板娘评选', 0, '  \r\n									  <p class=\"MsoNormal\"><span>寻找</span><span>铜梁</span><span><font face=\"宋体\">魅力女老板主题活动，并形成一档具有社会和媒体影响力的活动。这个明明可以靠脸吃饭的群体，但偏偏固执的非要靠才华和努力的女老板怎么样了呢？创业不易，市场激烈，在</font>2018年里又有多少女性朋友踏上创业之路呢？</span><span><o:p></o:p></span></p><p class=\"MsoNormal\"><span>她们靠智慧、汗水和女性独有的魅力，打拼出了自己的一方天地，无论她们有着怎样的收获，都是我们值得学习、钦佩的。基于此，我们计划在往届活动的成熟经验，特举办第魅力女老板评选活动，其旨在展示</span><span>铜梁</span><span>魅力女老板们的风采！</span><span><o:p></o:p></span></p>									', '', '  \r\n									  <p class=\"MsoNormal\">1、线上报名时间：9月8日—9月16日</p><p class=\"MsoNormal\">2、报名及投票时间：9月16日——9月26日</p><p class=\"MsoNormal\">3、老板娘展示时间：9月8日-9月底（待定）</p>									', '  \r\n									  <p>1、通过铜梁视窗活动报名链接，填写您的姓名、电话、自我形象、企业形象照，书写评比宣言即完成报名。</p><p>2、分享您的活动评比海报至好友，邀约其为您助力。</p>									', '老板娘投票', '', '铜梁女老板都是个中能手，我们一起来为铜梁的女中豪杰们投上一票吧！', '\r\nhttp://source.5atl.com/attachment/images/20180906/42a2d0ad463353c2292a432721369b03.jpg', 1536854400, 1536105600, 1537977599, 1537977599, 100, 0, 1, 1, 0, 4, 0, '023-45655113', 'page', 0, 0, 0, '', '', 0, '', '', '', '', 0, 0, 0, 0, '', 'fen', 1);

-- ----------------------------
-- Table structure for wx_voteimg_banner
-- ----------------------------
DROP TABLE IF EXISTS `wx_voteimg_banner`;
CREATE TABLE `wx_voteimg_banner`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vote_id` int(11) NOT NULL COMMENT '活动id',
  `title` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `img_url` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `external_links` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `banner_rank` int(11) NOT NULL DEFAULT 1 COMMENT '播放顺序',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `vote_id`(`vote_id`) USING BTREE,
  INDEX `banner_index`(`vote_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 13 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wx_voteimg_banner
-- ----------------------------
INSERT INTO `wx_voteimg_banner` VALUES (6, 1, 'undefined', 'http://www.internetke.com/jsEffects/2014052304/images/1.jpg', '#', 0);
INSERT INTO `wx_voteimg_banner` VALUES (7, 1, 'undefined', 'http://www.internetke.com/jsEffects/2014052304/images/2.jpg', '#', 0);
INSERT INTO `wx_voteimg_banner` VALUES (8, 12, 'undefined', 'http://www.5atl.com/attachment/images/201704/14/1492141421.png', '#', 0);
INSERT INTO `wx_voteimg_banner` VALUES (9, 15, 'undefined', 'http://source.5atl.com/attachment/upload/201708/15/15027600388837.png', 'http://www.5atl.com/index-index-show-aid-121879.html', 0);
INSERT INTO `wx_voteimg_banner` VALUES (10, 15, 'undefined', 'http://source.5atl.com/attachment/upload/201708/15/15027599112629.png', 'http://www.5atl.com/index-index-show-aid-121879.html', 0);
INSERT INTO `wx_voteimg_banner` VALUES (11, 15, 'undefined', 'http://source.5atl.com/attachment/upload/201708/15/15027599112629.png', 'http://www.5atl.com/index-index-show-aid-121879.html', 0);
INSERT INTO `wx_voteimg_banner` VALUES (12, 16, 'undefined', 'http://source.5atl.com/attachment/images/201710/09/1507529516.jpg', 'https://z.m.jd.com/project/details/90544.html', 0);

-- ----------------------------
-- Table structure for wx_voteimg_bottom
-- ----------------------------
DROP TABLE IF EXISTS `wx_voteimg_bottom`;
CREATE TABLE `wx_voteimg_bottom`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vote_id` int(11) NOT NULL COMMENT '活动id',
  `bottom_name` char(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '导航名称',
  `bottom_link` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `bottom_icon` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '导航图标',
  `bottom_rank` int(11) NOT NULL,
  `hide` tinyint(1) NOT NULL DEFAULT 1 COMMENT '是否隐藏',
  `otherlink` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '跳转链接',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `vote_id`(`vote_id`) USING BTREE,
  INDEX `bottom_index`(`vote_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 87 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wx_voteimg_bottom
-- ----------------------------
INSERT INTO `wx_voteimg_bottom` VALUES (52, 1, '首页', 'index', 'index', 1, 1, '');
INSERT INTO `wx_voteimg_bottom` VALUES (53, 1, '简介', 'descr', 'descr', 2, 1, '');
INSERT INTO `wx_voteimg_bottom` VALUES (54, 1, '排名', 'paixu', 'paixu', 3, 1, '');
INSERT INTO `wx_voteimg_bottom` VALUES (55, 1, '报名', 'apply', 'apply', 4, 1, '');
INSERT INTO `wx_voteimg_bottom` VALUES (56, 1, '电话', 'other', 'other', 5, 1, 'tel:023-45655113');
INSERT INTO `wx_voteimg_bottom` VALUES (57, 12, '首页', 'index', 'index', 0, 1, '');
INSERT INTO `wx_voteimg_bottom` VALUES (58, 12, '简介', 'descr', 'descr', 0, 1, '');
INSERT INTO `wx_voteimg_bottom` VALUES (59, 12, '排名', 'paixu', 'paixu', 0, 1, '');
INSERT INTO `wx_voteimg_bottom` VALUES (60, 12, '报名', 'apply', 'apply', 0, 1, '');
INSERT INTO `wx_voteimg_bottom` VALUES (61, 12, '咨询', 'other', 'other', 0, 1, 'tel:023-45655113');
INSERT INTO `wx_voteimg_bottom` VALUES (62, 13, '首页', 'index', 'index', 0, 1, '');
INSERT INTO `wx_voteimg_bottom` VALUES (63, 13, '简介', 'descr', 'descr', 0, 1, '');
INSERT INTO `wx_voteimg_bottom` VALUES (64, 13, '排名', 'paixu', 'paixu', 0, 1, '');
INSERT INTO `wx_voteimg_bottom` VALUES (65, 13, '报名', 'apply', 'apply', 0, 1, '');
INSERT INTO `wx_voteimg_bottom` VALUES (66, 13, '其他', 'other', 'other', 0, 1, 'tel:023-45655113');
INSERT INTO `wx_voteimg_bottom` VALUES (67, 14, '首页', 'index', 'index', 0, 1, '');
INSERT INTO `wx_voteimg_bottom` VALUES (68, 14, '简介', 'descr', 'descr', 0, 1, '');
INSERT INTO `wx_voteimg_bottom` VALUES (69, 14, '排名', 'paixu', 'paixu', 0, 1, '');
INSERT INTO `wx_voteimg_bottom` VALUES (70, 14, '报名', 'apply', 'apply', 0, 1, '');
INSERT INTO `wx_voteimg_bottom` VALUES (71, 14, '电话', 'other', 'other', 0, 1, 'tel:45210878');
INSERT INTO `wx_voteimg_bottom` VALUES (72, 15, '首页', 'index', 'index', 0, 1, '');
INSERT INTO `wx_voteimg_bottom` VALUES (73, 15, '简介', 'descr', 'descr', 0, 1, '');
INSERT INTO `wx_voteimg_bottom` VALUES (74, 15, '报名', 'apply', 'apply', 0, 1, '');
INSERT INTO `wx_voteimg_bottom` VALUES (75, 15, '排名', 'paixu', 'paixu', 0, 1, '');
INSERT INTO `wx_voteimg_bottom` VALUES (76, 15, '其他', 'other', 'other', 0, 1, 'tel:023-45655113');
INSERT INTO `wx_voteimg_bottom` VALUES (77, 16, '首页', 'index', 'index', 0, 1, '');
INSERT INTO `wx_voteimg_bottom` VALUES (78, 16, '简介', 'descr', 'descr', 0, 1, '');
INSERT INTO `wx_voteimg_bottom` VALUES (79, 16, '报名', 'apply', 'apply', 0, 1, '');
INSERT INTO `wx_voteimg_bottom` VALUES (80, 16, '排名', 'paixu', 'paixu', 0, 1, '');
INSERT INTO `wx_voteimg_bottom` VALUES (81, 16, '其他', 'other', 'other', 0, 1, 'tel:13452803380');
INSERT INTO `wx_voteimg_bottom` VALUES (82, 17, '首页', 'index', 'index', 0, 1, '');
INSERT INTO `wx_voteimg_bottom` VALUES (83, 17, '简介', 'descr', 'descr', 0, 1, '');
INSERT INTO `wx_voteimg_bottom` VALUES (84, 17, '排名', 'paixu', 'paixu', 0, 1, '');
INSERT INTO `wx_voteimg_bottom` VALUES (85, 17, '报名', 'apply', 'apply', 0, 1, '');
INSERT INTO `wx_voteimg_bottom` VALUES (86, 17, '电话', 'other', 'other', 0, 1, 'tel:15215161525');

-- ----------------------------
-- Table structure for wx_voteimg_comment
-- ----------------------------
DROP TABLE IF EXISTS `wx_voteimg_comment`;
CREATE TABLE `wx_voteimg_comment`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vote_id` int(11) NOT NULL,
  `voteitem_id` int(11) NOT NULL,
  `wecha_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `nickname` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `headimg` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `msg` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `createtime` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '评论' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for wx_voteimg_extention
-- ----------------------------
DROP TABLE IF EXISTS `wx_voteimg_extention`;
CREATE TABLE `wx_voteimg_extention`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `openid_f` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '服务号的openid',
  `exnum` int(11) NOT NULL COMMENT '额外的票数',
  `voteid` int(11) NOT NULL COMMENT '投票活动id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 75 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '投票道具加票仓库' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wx_voteimg_extention
-- ----------------------------
INSERT INTO `wx_voteimg_extention` VALUES (1, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 0, 1);
INSERT INTO `wx_voteimg_extention` VALUES (2, 'o73cGwflBGZyE_FYzV2Tn9lizSBM', 10, 1);
INSERT INTO `wx_voteimg_extention` VALUES (3, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 0, 15);
INSERT INTO `wx_voteimg_extention` VALUES (4, 'o73cGwb8UbrCmHEMf0oz-f2bOwko', 6, 15);
INSERT INTO `wx_voteimg_extention` VALUES (5, 'o73cGwb8UbrCmHEMf0oz-f2bOwko', 6, 15);
INSERT INTO `wx_voteimg_extention` VALUES (6, 'o73cGwbv0CwQdUQeeQ4R7rcY9Wtc', 0, 15);
INSERT INTO `wx_voteimg_extention` VALUES (7, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', -10, 15);
INSERT INTO `wx_voteimg_extention` VALUES (8, 'o73cGwYwf_IpNqlhmhUbnnPD9zRY', 6, 15);
INSERT INTO `wx_voteimg_extention` VALUES (9, 'o73cGwZqpd43SHNgQ0DLHKqERWjc', 0, 15);
INSERT INTO `wx_voteimg_extention` VALUES (10, 'o73cGwY0T-68x_LigknfWX2HvTYE', 0, 15);
INSERT INTO `wx_voteimg_extention` VALUES (11, 'o73cGwUoVNG3eR2mqquGo4_AN6eI', 0, 15);
INSERT INTO `wx_voteimg_extention` VALUES (12, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', -9, 15);
INSERT INTO `wx_voteimg_extention` VALUES (13, 'o73cGwSTPa07-i_C6ngzEcZ8kpEA', 3, 15);
INSERT INTO `wx_voteimg_extention` VALUES (14, 'o73cGwV1X9_aESVKl2XXaRg4IB-g', 0, 15);
INSERT INTO `wx_voteimg_extention` VALUES (15, 'o73cGwZ4TPEqDWX4FmCJKC936mtE', 3, 15);
INSERT INTO `wx_voteimg_extention` VALUES (16, 'o73cGwZYeM_Vv4CE5NLF3hPN0bSY', 0, 15);
INSERT INTO `wx_voteimg_extention` VALUES (17, 'o73cGwZn9cI2_ezrVu62V1qjt1nY', 0, 15);
INSERT INTO `wx_voteimg_extention` VALUES (18, 'o73cGwYSRhH3NuCnWYNdfcluIFg4', 6, 15);
INSERT INTO `wx_voteimg_extention` VALUES (19, 'o73cGwWAGatclXaxqo7WfcznQZgk', 12, 15);
INSERT INTO `wx_voteimg_extention` VALUES (20, 'o73cGwU6gD5TG8Wr7c6ID8CQtR3s', 12, 15);
INSERT INTO `wx_voteimg_extention` VALUES (21, 'o73cGwQJVieizzZDBdJSDDIIgWpQ', 0, 15);
INSERT INTO `wx_voteimg_extention` VALUES (22, 'o73cGwbmsjkwDHWVuRE9hD_3JhCE', 2, 15);
INSERT INTO `wx_voteimg_extention` VALUES (23, 'o73cGwesg87u9ifia2tf8nAGlMjc', 0, 15);
INSERT INTO `wx_voteimg_extention` VALUES (24, 'o73cGwZ1Tcp4RK2s-yCdypO-4DoE', 0, 15);
INSERT INTO `wx_voteimg_extention` VALUES (25, 'o73cGwZqrXUSXClFAqwFDVUxUvQs', 0, 15);
INSERT INTO `wx_voteimg_extention` VALUES (26, 'o73cGwVE49LF4iXeJrArOlrjsApQ', 9, 15);
INSERT INTO `wx_voteimg_extention` VALUES (27, 'o73cGwW9EF97rB6tX0v3Ftf736Z8', 0, 15);
INSERT INTO `wx_voteimg_extention` VALUES (28, 'o73cGwcmniVeTribf9grwoKXWgao', 0, 15);
INSERT INTO `wx_voteimg_extention` VALUES (29, 'o73cGwTe4ZKgHe_2pjKtvknauNaM', 0, 15);
INSERT INTO `wx_voteimg_extention` VALUES (30, 'o73cGwbfsREdqzO4rI6nsgmIWtzM', 0, 15);
INSERT INTO `wx_voteimg_extention` VALUES (31, 'o73cGwQX8_c1m3ftzh5DY0WXsFks', 0, 15);
INSERT INTO `wx_voteimg_extention` VALUES (32, 'o73cGwQWPS3ScgfWtKjV5A-qIiz4', 0, 15);
INSERT INTO `wx_voteimg_extention` VALUES (33, 'o73cGwU8RghQlz1L2H1859kSNmiY', 3, 15);
INSERT INTO `wx_voteimg_extention` VALUES (34, 'o73cGwX7gROu8HPsZuagWwzeZqX0', 0, 15);
INSERT INTO `wx_voteimg_extention` VALUES (35, 'o73cGwSvkw5G-tf-K6uxHQy8km_U', 12, 15);
INSERT INTO `wx_voteimg_extention` VALUES (36, 'o73cGwb5gDz-NUbT7EJO_h_eCDsI', 0, 15);
INSERT INTO `wx_voteimg_extention` VALUES (37, 'o73cGwbYox2Oc3ypbuMMSFFLvl4I', 0, 15);
INSERT INTO `wx_voteimg_extention` VALUES (38, 'o73cGweB7djARDADY6J6tYuelG5s', 0, 15);
INSERT INTO `wx_voteimg_extention` VALUES (39, 'o73cGwctijuWmAa7HeVXLbV_VTfM', 0, 15);
INSERT INTO `wx_voteimg_extention` VALUES (40, 'o73cGwcAvgRbujYWTSzoxgZmw1r4', 0, 15);
INSERT INTO `wx_voteimg_extention` VALUES (41, 'o73cGwebaXRP4bhYUHKRgxK35elw', 0, 15);
INSERT INTO `wx_voteimg_extention` VALUES (42, 'o73cGwQHUsDywbQyeHSif6gcxi2E', 3, 15);
INSERT INTO `wx_voteimg_extention` VALUES (43, 'o73cGwc2Xmm126aPY175XxHvVwtU', 0, 15);
INSERT INTO `wx_voteimg_extention` VALUES (44, 'o73cGwS9zhLblQuVVXQyMFjFkPGw', 0, 15);
INSERT INTO `wx_voteimg_extention` VALUES (45, 'o73cGweMHgus1QO2dmQNS3HD4xEQ', 0, 15);
INSERT INTO `wx_voteimg_extention` VALUES (46, 'o73cGwSdxBv31tgzGqpjEHpqIcs4', 0, 15);
INSERT INTO `wx_voteimg_extention` VALUES (47, 'o73cGwcs83n-MjNmZkPcm-EFrkRE', 0, 15);
INSERT INTO `wx_voteimg_extention` VALUES (48, 'o73cGwVpdysPUL0rk7fgaVeAu2ik', 0, 15);
INSERT INTO `wx_voteimg_extention` VALUES (49, 'o73cGwZjlZHEnHPpziQZ3K__lyNw', 0, 15);
INSERT INTO `wx_voteimg_extention` VALUES (50, 'o73cGwV77CpnPWex_2vlWQq6GOX4', 0, 15);
INSERT INTO `wx_voteimg_extention` VALUES (51, 'o73cGwf5O9r4HALiUoqDXKi14fk0', 0, 15);
INSERT INTO `wx_voteimg_extention` VALUES (52, 'o73cGwWd9jhuPiyM9OVxb09Rf8iY', 0, 15);
INSERT INTO `wx_voteimg_extention` VALUES (53, 'o73cGwVdhgAX20fqMeLI1UFrFy38', 0, 15);
INSERT INTO `wx_voteimg_extention` VALUES (54, 'o73cGwbx2vbWLx8DLjAvgb6riJbQ', 0, 15);
INSERT INTO `wx_voteimg_extention` VALUES (55, 'o73cGwYIT02jJ11Bq6XkGsIvbCCE', 0, 15);
INSERT INTO `wx_voteimg_extention` VALUES (56, 'o73cGwQcy61ov2HRUmfCPILLf0vU', 0, 15);
INSERT INTO `wx_voteimg_extention` VALUES (57, 'o73cGwQGwvvb_uqbpZJIozDyGrnk', 0, 15);
INSERT INTO `wx_voteimg_extention` VALUES (58, 'o73cGwWMW3K69TrlD13x6zBB2s7E', 0, 15);
INSERT INTO `wx_voteimg_extention` VALUES (59, 'o73cGwQtPerlJd34xTTKQnlHaceQ', 0, 15);
INSERT INTO `wx_voteimg_extention` VALUES (60, 'o73cGwe1z9Wfh1vdsTJEGb8dov_4', 0, 15);
INSERT INTO `wx_voteimg_extention` VALUES (61, 'o73cGwYNnU35mocJNcfjFVOXULbU', 0, 15);
INSERT INTO `wx_voteimg_extention` VALUES (62, 'o73cGwYeZ5Jl1YFseAhOyQW1EpKw', 0, 15);
INSERT INTO `wx_voteimg_extention` VALUES (63, 'o73cGwegvDbDhPq6mQjHE85evbvQ', 0, 15);
INSERT INTO `wx_voteimg_extention` VALUES (64, 'o73cGweUDc6cYdwXqUB5R97kk7A8', 3, 15);
INSERT INTO `wx_voteimg_extention` VALUES (65, 'o73cGwT9nbkWSa2sP6f8tPWdGTTk', 0, 15);
INSERT INTO `wx_voteimg_extention` VALUES (66, 'o73cGwecxQKcYoYyEjQ39CFtIO4E', 0, 15);
INSERT INTO `wx_voteimg_extention` VALUES (67, 'o73cGwexzBqc04_d24lK9xDd0u28', 0, 15);
INSERT INTO `wx_voteimg_extention` VALUES (68, 'o73cGwQThljSqo9NLohtyVXpDVoA', 0, 15);
INSERT INTO `wx_voteimg_extention` VALUES (69, 'o73cGwfgtUWGpZoX7e1vNio0Xmr0', 27, 15);
INSERT INTO `wx_voteimg_extention` VALUES (70, 'o73cGwZ2KXlGqe1fBm5ohHhOqhvk', 3, 15);
INSERT INTO `wx_voteimg_extention` VALUES (71, 'o73cGwVKvNaCUzq3LZn0bs3QfgE8', 0, 15);
INSERT INTO `wx_voteimg_extention` VALUES (72, 'o73cGwQ7pMinXP5p2ZlvmZtEz78g', 0, 15);
INSERT INTO `wx_voteimg_extention` VALUES (73, 'o73cGwVqskKrHsptmgv9LaE7OwRU', 0, 15);
INSERT INTO `wx_voteimg_extention` VALUES (74, 'oUn5Ew_1uVGMM2Cu-RIsPhNS4ygk', 2, 16);

-- ----------------------------
-- Table structure for wx_voteimg_gift
-- ----------------------------
DROP TABLE IF EXISTS `wx_voteimg_gift`;
CREATE TABLE `wx_voteimg_gift`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `voteid` int(11) NOT NULL,
  `voteitemid` int(11) NOT NULL,
  `giftid` int(11) NOT NULL,
  `orderid` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '支付订单id',
  `createtime` int(11) NOT NULL,
  `headimg` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `nickname` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `openid` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 344 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '道具投票' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wx_voteimg_gift
-- ----------------------------
INSERT INTO `wx_voteimg_gift` VALUES (1, 1, 985, 1, '12', 1500337896, 'http://wx.qlogo.cn/mmhead/AbruuZ3ILCkZ0Cb693G9icnHJhtKBE40ZfFK02z02JAE9SM7RdRByhA/0', '大娃-何跃', '');
INSERT INTO `wx_voteimg_gift` VALUES (2, 1, 995, 1, '13', 1500340038, 'http://wx.qlogo.cn/mmhead/AbruuZ3ILCkZ0Cb693G9icnHJhtKBE40ZfFK02z02JAE9SM7RdRByhA/0', '大娃-何跃', '');
INSERT INTO `wx_voteimg_gift` VALUES (3, 1, 993, 1, '14', 1500340082, 'http://wx.qlogo.cn/mmhead/AbruuZ3ILCkZ0Cb693G9icnHJhtKBE40ZfFK02z02JAE9SM7RdRByhA/0', '大娃-何跃', '');
INSERT INTO `wx_voteimg_gift` VALUES (4, 1, 1002, 1, '15', 1500340115, 'http://wx.qlogo.cn/mmhead/AbruuZ3ILCkZ0Cb693G9icnHJhtKBE40ZfFK02z02JAE9SM7RdRByhA/0', '大娃-何跃', '');
INSERT INTO `wx_voteimg_gift` VALUES (5, 1, 997, 1, '16', 1500341735, 'http://wx.qlogo.cn/mmhead/AbruuZ3ILCkZ0Cb693G9icnHJhtKBE40ZfFK02z02JAE9SM7RdRByhA/0', '大娃-何跃', '');
INSERT INTO `wx_voteimg_gift` VALUES (6, 1, 991, 1, '17', 1500341971, 'http://wx.qlogo.cn/mmhead/lHTeHNjM60V1U5mbAvia60agcfmibGpLRXKqtyYqfYat8/0', '龙康.贝贝佳', '');
INSERT INTO `wx_voteimg_gift` VALUES (7, 1, 1031, 1, '18', 1500342197, 'http://wx.qlogo.cn/mmhead/lHTeHNjM60V1U5mbAvia60agcfmibGpLRXKqtyYqfYat8/0', '龙康.贝贝佳', '');
INSERT INTO `wx_voteimg_gift` VALUES (8, 1, 1031, 2, '19', 1500342211, 'http://wx.qlogo.cn/mmhead/lHTeHNjM60V1U5mbAvia60agcfmibGpLRXKqtyYqfYat8/0', '龙康.贝贝佳', '');
INSERT INTO `wx_voteimg_gift` VALUES (9, 1, 1031, 1, '20', 1500342309, 'http://wx.qlogo.cn/mmhead/lHTeHNjM60V1U5mbAvia60agcfmibGpLRXKqtyYqfYat8/0', '龙康.贝贝佳', '');
INSERT INTO `wx_voteimg_gift` VALUES (10, 1, 985, 1, '21', 1500352831, 'http://wx.qlogo.cn/mmhead/AbruuZ3ILCkZ0Cb693G9icnHJhtKBE40ZfFK02z02JAE9SM7RdRByhA/0', 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', '');
INSERT INTO `wx_voteimg_gift` VALUES (11, 1, 985, 1, '22', 1500356339, 'http://wx.qlogo.cn/mmhead/AbruuZ3ILCkZ0Cb693G9icnHJhtKBE40ZfFK02z02JAE9SM7RdRByhA/0', 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', '');
INSERT INTO `wx_voteimg_gift` VALUES (12, 1, 985, 1, '23', 1500360243, 'http://wx.qlogo.cn/mmhead/AbruuZ3ILCkZ0Cb693G9icnHJhtKBE40ZfFK02z02JAE9SM7RdRByhA/0', 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', '');
INSERT INTO `wx_voteimg_gift` VALUES (13, 13, 1004, 1, '24', 1500362547, 'http://wx.qlogo.cn/mmhead/AbruuZ3ILCkZ0Cb693G9icnHJhtKBE40ZfFK02z02JAE9SM7RdRByhA/0', 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', '');
INSERT INTO `wx_voteimg_gift` VALUES (14, 1, 993, 1, '25', 1500362570, 'http://wx.qlogo.cn/mmhead/AbruuZ3ILCkZ0Cb693G9icnHJhtKBE40ZfFK02z02JAE9SM7RdRByhA/0', 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', '');
INSERT INTO `wx_voteimg_gift` VALUES (15, 1, 987, 1, '26', 1500372437, 'http://wx.qlogo.cn/mmopen/kvEYORDAicI0bvkb3gXMScez1AibiaVwcl7dxrQWpwfrRG0Ck7dKkrAWiaMD3nC6P0YjibzyTZvJRCsNRkHsN84N7iba3VQ6VDj1icF/0', 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', '');
INSERT INTO `wx_voteimg_gift` VALUES (16, 1, 993, 1, '27', 1500373645, 'http://wx.qlogo.cn/mmopen/kvEYORDAicI0bvkb3gXMScez1AibiaVwcl7dxrQWpwfrRG0Ck7dKkrAWiaMD3nC6P0YjibzyTZvJRCsNRkHsN84N7iba3VQ6VDj1icF/0', 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', '');
INSERT INTO `wx_voteimg_gift` VALUES (17, 1, 993, 1, '28', 1500373779, 'http://wx.qlogo.cn/mmopen/kvEYORDAicI0bvkb3gXMScez1AibiaVwcl7dxrQWpwfrRG0Ck7dKkrAWiaMD3nC6P0YjibzyTZvJRCsNRkHsN84N7iba3VQ6VDj1icF/0', 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', '');
INSERT INTO `wx_voteimg_gift` VALUES (18, 1, 993, 1, '29', 1500373896, 'http://wx.qlogo.cn/mmopen/kvEYORDAicI0bvkb3gXMScez1AibiaVwcl7dxrQWpwfrRG0Ck7dKkrAWiaMD3nC6P0YjibzyTZvJRCsNRkHsN84N7iba3VQ6VDj1icF/0', 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', '');
INSERT INTO `wx_voteimg_gift` VALUES (19, 1, 985, 4, '30', 1500373965, 'http://wx.qlogo.cn/mmopen/kvEYORDAicI0bvkb3gXMScez1AibiaVwcl7dxrQWpwfrRG0Ck7dKkrAWiaMD3nC6P0YjibzyTZvJRCsNRkHsN84N7iba3VQ6VDj1icF/0', 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', '');
INSERT INTO `wx_voteimg_gift` VALUES (20, 1, 987, 1, '31', 1500386844, 'http://wx.qlogo.cn/mmhead/AbruuZ3ILCkZ0Cb693G9icnHJhtKBE40ZfFK02z02JAE9SM7RdRByhA/0', 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', '');
INSERT INTO `wx_voteimg_gift` VALUES (21, 1, 987, 1, '32', 1500426687, 'http://wx.qlogo.cn/mmopen/kvEYORDAicI0bvkb3gXMScez1AibiaVwcl7dxrQWpwfrRG0Ck7dKkrAWiaMD3nC6P0YjibzyTZvJRCsNRkHsN84N7iba3VQ6VDj1icF/0', '大娃-何跃', 'o73cGwZLQw0XSG_7qJ9abHuH_sEI');
INSERT INTO `wx_voteimg_gift` VALUES (22, 1, 989, 1, '33', 1500601695, 'http://wx.qlogo.cn/mmhead/AbruuZ3ILCkZ0Cb693G9icnHJhtKBE40ZfFK02z02JAE9SM7RdRByhA/0', '大娃-何跃', 'o73cGwZLQw0XSG_7qJ9abHuH_sEI');
INSERT INTO `wx_voteimg_gift` VALUES (23, 1, 989, 1, '34', 1500601719, 'http://wx.qlogo.cn/mmhead/AbruuZ3ILCkZ0Cb693G9icnHJhtKBE40ZfFK02z02JAE9SM7RdRByhA/0', '大娃-何跃', 'o73cGwZLQw0XSG_7qJ9abHuH_sEI');
INSERT INTO `wx_voteimg_gift` VALUES (24, 1, 987, 1, '35', 1500705624, 'http://wx.qlogo.cn/mmhead/AbruuZ3ILCkZ0Cb693G9icnHJhtKBE40ZfFK02z02JAE9SM7RdRByhA/0', '大娃-何跃', 'o73cGwZLQw0XSG_7qJ9abHuH_sEI');
INSERT INTO `wx_voteimg_gift` VALUES (25, 1, 987, 1, '36', 1500705814, 'http://wx.qlogo.cn/mmhead/AbruuZ3ILCkZ0Cb693G9icnHJhtKBE40ZfFK02z02JAE9SM7RdRByhA/0', '大娃-何跃', 'o73cGwZLQw0XSG_7qJ9abHuH_sEI');
INSERT INTO `wx_voteimg_gift` VALUES (26, 1, 985, 1, '37', 1500864683, 'http://wx.qlogo.cn/mmhead/AbruuZ3ILCkZ0Cb693G9icnHJhtKBE40ZfFK02z02JAE9SM7RdRByhA/0', '大娃-何跃', 'o73cGwZLQw0XSG_7qJ9abHuH_sEI');
INSERT INTO `wx_voteimg_gift` VALUES (27, 1, 984, 1, '38', 1500964933, 'http://wx.qlogo.cn/mmopen/kvEYORDAicI2Jx4XEKV9Gy0Ql7zsUY3BR0KstuALpC3cx0iaqDsjTPcmssAPmKNP2bbFcyu8GWvZ1ic9es7Q380mJCgv7QlCwOZ/0', '秋', 'o73cGwflBGZyE_FYzV2Tn9lizSBM');
INSERT INTO `wx_voteimg_gift` VALUES (28, 1, 1032, 1, '39', 1502271852, 'http://wx.qlogo.cn/mmopen/kvEYORDAicI0bvkb3gXMScez1AibiaVwcl7dxrQWpwfrRG0Ck7dKkrAWiaMD3nC6P0YjibzyTZvJRCsNRkHsN84N7iba3VQ6VDj1icF/0', '大娃-何跃', 'o73cGwZLQw0XSG_7qJ9abHuH_sEI');
INSERT INTO `wx_voteimg_gift` VALUES (29, 15, 1053, 2, '40', 1503444954, 'http://wx.qlogo.cn/mmopen/FcqfTESP3ZeJIyEyYqIuLuApOmsEPQP3CehH9vGic5kibQME6WP5uKwB0azMIbibvLgCTmUXPaoHIGEsnqIdHFuI1J0sfjXk2Gr/0', '喻芬', 'o73cGwb8UbrCmHEMf0oz-f2bOwko');
INSERT INTO `wx_voteimg_gift` VALUES (30, 15, 1083, 1, '41', 1503468010, 'http://wx.qlogo.cn/mmopen/4YYHFun9lU1KvkbRTX9PoDWQSKmtQ3iaVlQlxWpVwvm0aSZWdj9wSjpwWUa1REZW5L2wUu85OYZgnkdXgYeIFJx3yeOibXnLb1/0', '釹      荏    ざ    九', 'o73cGwbv0CwQdUQeeQ4R7rcY9Wtc');
INSERT INTO `wx_voteimg_gift` VALUES (31, 15, 1033, 1, '42', 1503468029, 'http://wx.qlogo.cn/mmopen/kvEYORDAicI0bvkb3gXMScez1AibiaVwcl7dxrQWpwfrRG0Ck7dKkrAWiaMD3nC6P0YjibzyTZvJRCsNRkHsN84N7iba3VQ6VDj1icF/0', '大娃-何跃', 'o73cGwZLQw0XSG_7qJ9abHuH_sEI');
INSERT INTO `wx_voteimg_gift` VALUES (32, 15, 1033, 1, '43', 1503468670, 'http://wx.qlogo.cn/mmopen/kvEYORDAicI0bvkb3gXMScez1AibiaVwcl7dxrQWpwfrRG0Ck7dKkrAWiaMD3nC6P0YjibzyTZvJRCsNRkHsN84N7iba3VQ6VDj1icF/0', '大娃-何跃', 'o73cGwZLQw0XSG_7qJ9abHuH_sEI');
INSERT INTO `wx_voteimg_gift` VALUES (33, 15, 1033, 1, '44', 1503469592, 'http://wx.qlogo.cn/mmopen/kvEYORDAicI0bvkb3gXMScez1AibiaVwcl7dxrQWpwfrRG0Ck7dKkrAWiaMD3nC6P0YjibzyTZvJRCsNRkHsN84N7iba3VQ6VDj1icF/0', '大娃-何跃', 'o73cGwZLQw0XSG_7qJ9abHuH_sEI');
INSERT INTO `wx_voteimg_gift` VALUES (34, 15, 1037, 1, '45', 1503469669, 'http://wx.qlogo.cn/mmopen/kvEYORDAicI0bvkb3gXMScez1AibiaVwcl7dxrQWpwfrRG0Ck7dKkrAWiaMD3nC6P0YjibzyTZvJRCsNRkHsN84N7iba3VQ6VDj1icF/0', '大娃-何跃', 'o73cGwZLQw0XSG_7qJ9abHuH_sEI');
INSERT INTO `wx_voteimg_gift` VALUES (35, 15, 1082, 1, '46', 1503469679, 'http://wx.qlogo.cn/mmopen/kvEYORDAicI3y7zFLkO1mJg4G5DsIK9kOX8IAUX1IIweXwM8hj2SHyDJjJmjZohqCISklqfKMat9EdZeibUQpKZF7icDZicHXdhL/0', '老男孩', 'o73cGwYwf_IpNqlhmhUbnnPD9zRY');
INSERT INTO `wx_voteimg_gift` VALUES (36, 15, 1082, 1, '47', 1503469721, 'http://wx.qlogo.cn/mmopen/kvEYORDAicI3y7zFLkO1mJg4G5DsIK9kOX8IAUX1IIweXwM8hj2SHyDJjJmjZohqCISklqfKMat9EdZeibUQpKZF7icDZicHXdhL/0', '老男孩', 'o73cGwYwf_IpNqlhmhUbnnPD9zRY');
INSERT INTO `wx_voteimg_gift` VALUES (37, 15, 1037, 1, '48', 1503470605, 'http://wx.qlogo.cn/mmopen/kvEYORDAicI0bvkb3gXMScez1AibiaVwcl7dxrQWpwfrRG0Ck7dKkrAWiaMD3nC6P0YjibzyTZvJRCsNRkHsN84N7iba3VQ6VDj1icF/0', '大娃-何跃', 'o73cGwZLQw0XSG_7qJ9abHuH_sEI');
INSERT INTO `wx_voteimg_gift` VALUES (38, 15, 1037, 1, '49', 1503470695, 'http://wx.qlogo.cn/mmopen/kvEYORDAicI0bvkb3gXMScez1AibiaVwcl7dxrQWpwfrRG0Ck7dKkrAWiaMD3nC6P0YjibzyTZvJRCsNRkHsN84N7iba3VQ6VDj1icF/0', '大娃-何跃', 'o73cGwZLQw0XSG_7qJ9abHuH_sEI');
INSERT INTO `wx_voteimg_gift` VALUES (39, 15, 1037, 1, '50', 1503470757, 'http://wx.qlogo.cn/mmopen/kvEYORDAicI0bvkb3gXMScez1AibiaVwcl7dxrQWpwfrRG0Ck7dKkrAWiaMD3nC6P0YjibzyTZvJRCsNRkHsN84N7iba3VQ6VDj1icF/0', '大娃-何跃', 'o73cGwZLQw0XSG_7qJ9abHuH_sEI');
INSERT INTO `wx_voteimg_gift` VALUES (40, 15, 1037, 1, '51', 1503470877, 'http://wx.qlogo.cn/mmopen/kvEYORDAicI0bvkb3gXMScez1AibiaVwcl7dxrQWpwfrRG0Ck7dKkrAWiaMD3nC6P0YjibzyTZvJRCsNRkHsN84N7iba3VQ6VDj1icF/0', '大娃-何跃', 'o73cGwZLQw0XSG_7qJ9abHuH_sEI');
INSERT INTO `wx_voteimg_gift` VALUES (41, 15, 1037, 1, '52', 1503471051, 'http://wx.qlogo.cn/mmopen/kvEYORDAicI0bvkb3gXMScez1AibiaVwcl7dxrQWpwfrRG0Ck7dKkrAWiaMD3nC6P0YjibzyTZvJRCsNRkHsN84N7iba3VQ6VDj1icF/0', '大娃-何跃', 'o73cGwZLQw0XSG_7qJ9abHuH_sEI');
INSERT INTO `wx_voteimg_gift` VALUES (42, 15, 1099, 4, '53', 1503471213, 'http://wx.qlogo.cn/mmopen/Q3auHgzwzM7Kibm09zgn2Fl7HGSNPWr8Jt3vgNTCkBPO0yIe4icPiaVhbDBkZVhjvQMWr6bbnZ97fqjfhWRSJg9dg/0', '唐', 'o73cGwZqpd43SHNgQ0DLHKqERWjc');
INSERT INTO `wx_voteimg_gift` VALUES (43, 15, 1037, 1, '54', 1503471223, 'http://wx.qlogo.cn/mmopen/kvEYORDAicI0bvkb3gXMScez1AibiaVwcl7dxrQWpwfrRG0Ck7dKkrAWiaMD3nC6P0YjibzyTZvJRCsNRkHsN84N7iba3VQ6VDj1icF/0', '大娃-何跃', 'o73cGwZLQw0XSG_7qJ9abHuH_sEI');
INSERT INTO `wx_voteimg_gift` VALUES (44, 15, 1037, 1, '55', 1503471433, 'http://wx.qlogo.cn/mmopen/kvEYORDAicI0bvkb3gXMScez1AibiaVwcl7dxrQWpwfrRG0Ck7dKkrAWiaMD3nC6P0YjibzyTZvJRCsNRkHsN84N7iba3VQ6VDj1icF/0', '大娃-何跃', 'o73cGwZLQw0XSG_7qJ9abHuH_sEI');
INSERT INTO `wx_voteimg_gift` VALUES (45, 15, 1083, 4, '56', 1503471594, 'http://wx.qlogo.cn/mmopen/FcqfTESP3ZccNy7Z5KCFNwGicxD0WLX7z9fgTCDFiaRZIZrVUcXnLr950bcsbBXC0WqVtpnUJkwHWqx8t0BwApFHfAFAgicnYEg/0', '西永乘风通讯', 'o73cGwUoVNG3eR2mqquGo4_AN6eI');
INSERT INTO `wx_voteimg_gift` VALUES (46, 15, 1037, 1, '57', 1503471833, 'http://wx.qlogo.cn/mmopen/kvEYORDAicI0bvkb3gXMScez1AibiaVwcl7dxrQWpwfrRG0Ck7dKkrAWiaMD3nC6P0YjibzyTZvJRCsNRkHsN84N7iba3VQ6VDj1icF/0', '大娃-何跃', 'o73cGwZLQw0XSG_7qJ9abHuH_sEI');
INSERT INTO `wx_voteimg_gift` VALUES (47, 15, 1033, 1, '58', 1503471983, 'http://wx.qlogo.cn/mmopen/kvEYORDAicI1kpDVv31hRMECk4dgXMt21Y5aVWle4bRH5dRbP41CcczMibYX7HOj8zcKgjr4ibpzneZ4DoBA1SXGicl1oT6Gibcv2/0', 'ConceNtricity \'\'', 'o73cGwY0T-68x_LigknfWX2HvTYE');
INSERT INTO `wx_voteimg_gift` VALUES (48, 15, 1033, 1, '59', 1503473089, 'http://wx.qlogo.cn/mmopen/4YYHFun9lU0FLmQsyPDBB8kFfEI9YfdgNn6sNiaibNSGx3skvowdKAKw7QT05Cd7SLgSy95UPiblLOmGhAv0OdVsDIveA26AXHd/0', '锅烟煤', 'o73cGwSTPa07-i_C6ngzEcZ8kpEA');
INSERT INTO `wx_voteimg_gift` VALUES (49, 15, 1104, 1, '60', 1503479791, 'http://wx.qlogo.cn/mmopen/kvEYORDAicI2VLrKwjyuApBibibibf6GobNtXOsnnCKAf8fCWPZzbqqqvIhpY58kEeDtkWYG08jtibFsSJGZ9Ekd4Zib2X4BxQQqI9/0', '荣耀(乐陶陶)', 'o73cGwV1X9_aESVKl2XXaRg4IB-g');
INSERT INTO `wx_voteimg_gift` VALUES (50, 15, 1104, 3, '61', 1503479960, 'http://wx.qlogo.cn/mmopen/kvEYORDAicI2VLrKwjyuApBibibibf6GobNtXOsnnCKAf8fCWPZzbqqqvIhpY58kEeDtkWYG08jtibFsSJGZ9Ekd4Zib2X4BxQQqI9/0', '荣耀(乐陶陶)', 'o73cGwV1X9_aESVKl2XXaRg4IB-g');
INSERT INTO `wx_voteimg_gift` VALUES (51, 15, 1053, 1, '62', 1503485816, 'http://wx.qlogo.cn/mmopen/gicOicRnq2ibicHB6mm8RcFNZcy6u3D5fGypLkTicqYtoUiaugpWgRbn4T8fOTib2vnTrjG1KN86ENWsMyrtNwNtjCwXPB6pMfnB9Af/0', '艾雪', 'o73cGwX3JqNrI3NsRWhRw5e6rHbM');
INSERT INTO `wx_voteimg_gift` VALUES (52, 15, 1094, 4, '63', 1503489258, 'http://wx.qlogo.cn/mmopen/FMajU52WvbFkia9HUSIB7q2jYJrbbh3Yn7I7D2GRicOawq8KMo7BppH07jJCBECl4uW9uFsJm3L0uuemDexfIFia8guGQ30xrtN/0', '邹永伦', 'o73cGwegvDbDhPq6mQjHE85evbvQ');
INSERT INTO `wx_voteimg_gift` VALUES (53, 15, 1041, 1, '64', 1503490013, 'http://wx.qlogo.cn/mmopen/kvEYORDAicI1icHLCh2O5diaJSRSrRicOND2M8D98RXsDCcXxd2qyCU0BVDicKWGvlkMOiaMegrddkTOx52iaLFrZTCB9JjyvEIV06S/0', '田锋', 'o73cGwZ4TPEqDWX4FmCJKC936mtE');
INSERT INTO `wx_voteimg_gift` VALUES (54, 15, 1127, 1, '65', 1503491831, 'http://wx.qlogo.cn/mmopen/ajNVdqHZLLCSK6ULgsmia22ZM0eISgl6JWwEU8BFZZeGQ95tBlatWwH7q9NBibiccgOvxfudNnlu48YtMiaeU3kG0A/0', 'wo', 'o73cGwf8apXUtOihkgBK_S1iPucM');
INSERT INTO `wx_voteimg_gift` VALUES (55, 15, 1094, 3, '66', 1503492365, 'http://wx.qlogo.cn/mmopen/kvEYORDAicI17AN9mgBXalR4pueOjABibwQ80OUZ2zL9PIRXBWLIqrvXge1xAwvdyKIDPLiazDe3yzt9IHpBG0dRYOFnSsamjqB/0', '娇', 'o73cGwbkCzuNS6Q24CWewL2JDo7w');
INSERT INTO `wx_voteimg_gift` VALUES (56, 15, 1071, 4, '67', 1503493994, 'http://wx.qlogo.cn/mmopen/FcqfTESP3ZeKaJcIuLdEPXuL9SV3Sic79M2uBRgFaJX9gOZXTdF97wZ11iaz1948QbnS3tea9gy5aA5J8aatiagHJYiae5vpJsoib/0', '周四', 'o73cGwZYeM_Vv4CE5NLF3hPN0bSY');
INSERT INTO `wx_voteimg_gift` VALUES (57, 15, 1071, 4, '68', 1503494144, 'http://wx.qlogo.cn/mmopen/FcqfTESP3ZeKaJcIuLdEPXuL9SV3Sic79M2uBRgFaJX9gOZXTdF97wZ11iaz1948QbnS3tea9gy5aA5J8aatiagHJYiae5vpJsoib/0', '周四', 'o73cGwZYeM_Vv4CE5NLF3hPN0bSY');
INSERT INTO `wx_voteimg_gift` VALUES (58, 15, 1071, 1, '69', 1503494267, 'http://wx.qlogo.cn/mmopen/FcqfTESP3ZfoH5swWfV0YhAESib76kVYTnvibxvNMJkPJEmHvDlCwiceC8xqlgNicgDfiaQkNkrke1TrFBr94rXbK0Xq4HAbxA0Ck/0', '', 'o73cGwZn9cI2_ezrVu62V1qjt1nY');
INSERT INTO `wx_voteimg_gift` VALUES (59, 15, 1088, 1, '70', 1503495433, 'http://wx.qlogo.cn/mmopen/4YYHFun9lU16FwntjWngtaJrv54WvyFLNnqia9Lvw0sFknXAP7GJGNIfg7W8yoxUicrSjsk74kic47mYYiabZxzDoH9AgSxtgibbo/0', 'A Hello future ', 'o73cGwTUoKF80DEQkfOrfumlaDkY');
INSERT INTO `wx_voteimg_gift` VALUES (60, 15, 1071, 1, '71', 1503498455, 'http://wx.qlogo.cn/mmopen/FcqfTESP3ZeJIyEyYqIuLtf7ZmOT0cduUwhMpQqspr72zTBNSPgvfVl2IqKyY7lia0ic5wHSlWZOMHATKwJ2dz1Nj0xicHFwAr7/0', '雨后有彩虹', 'o73cGwfZFL8kpRyUAocxsd3pGIRs');
INSERT INTO `wx_voteimg_gift` VALUES (61, 15, 1053, 2, '72', 1503499552, 'http://wx.qlogo.cn/mmopen/FcqfTESP3ZdzmEFg2yw14ICFy6nyVkTN9Dj2zZBrSOlLR2uOFgz9WPJUpr7sAvU6IME1z5rUzaVWiaRBVRGnXfRicjRSjibl90A/0', '田老大', 'o73cGwYSRhH3NuCnWYNdfcluIFg4');
INSERT INTO `wx_voteimg_gift` VALUES (62, 15, 1131, 4, '73', 1503499740, 'http://wx.qlogo.cn/mmopen/FMajU52WvbECsgL6zj7LEyGgHfqw9M986BpjZugd0rkHnss3UpqWiaVhWsvRTz7p29ax9hjuE4WyWc0HJbXkZHQ/0', '高雨', 'o73cGwWAGatclXaxqo7WfcznQZgk');
INSERT INTO `wx_voteimg_gift` VALUES (63, 15, 1131, 4, '74', 1503502376, 'http://wx.qlogo.cn/mmopen/Kx8BLmG9Mia3YQahMglKLP5FKf33NA5Lt2P3m57Lc0ibhQLeJ75nCdbv92ReVfA7jib7xApibHkks60gPgMEejYCBEUt5vJRgiaW4/0', '江小美', 'o73cGwXmUHFwcD_19XtcAXVJt6pc');
INSERT INTO `wx_voteimg_gift` VALUES (64, 15, 1071, 4, '75', 1503502509, 'http://wx.qlogo.cn/mmopen/Q3auHgzwzM6wQZxkq7d9yIPHFnlSllnM1dd6lZ99Pp7Ysa0zPC7t3SKh29lG0e0xZbteMAvXYiciaccSqRHPvLr42M9xNtPYmD4iadFMWn4Zbc/0', 'Renconte_', 'o73cGwf58HX6dCLn13_7DYrNUO60');
INSERT INTO `wx_voteimg_gift` VALUES (65, 15, 1071, 1, '76', 1503502545, 'http://wx.qlogo.cn/mmopen/Q3auHgzwzM6wQZxkq7d9yIPHFnlSllnM1dd6lZ99Pp7Ysa0zPC7t3SKh29lG0e0xZbteMAvXYiciaccSqRHPvLr42M9xNtPYmD4iadFMWn4Zbc/0', 'Renconte_', 'o73cGwf58HX6dCLn13_7DYrNUO60');
INSERT INTO `wx_voteimg_gift` VALUES (66, 15, 1060, 4, '77', 1503532426, 'http://wx.qlogo.cn/mmopen/4YYHFun9lU0FLmQsyPDBB9nqh7djyvzv7LoS2sma7O0syWCAVhNzlU67aQXESH5p7VpGsfl8GVicImLPoNoyZ4VANO8jjmMPh/0', '【倩女幽魂】七夜圣君', 'o73cGwU6gD5TG8Wr7c6ID8CQtR3s');
INSERT INTO `wx_voteimg_gift` VALUES (67, 15, 1060, 4, '78', 1503532461, 'http://wx.qlogo.cn/mmopen/4YYHFun9lU0FLmQsyPDBB9nqh7djyvzv7LoS2sma7O0syWCAVhNzlU67aQXESH5p7VpGsfl8GVicImLPoNoyZ4VANO8jjmMPh/0', '【倩女幽魂】七夜圣君', 'o73cGwU6gD5TG8Wr7c6ID8CQtR3s');
INSERT INTO `wx_voteimg_gift` VALUES (68, 15, 1060, 4, '79', 1503532511, 'http://wx.qlogo.cn/mmopen/4YYHFun9lU0FLmQsyPDBB9nqh7djyvzv7LoS2sma7O0syWCAVhNzlU67aQXESH5p7VpGsfl8GVicImLPoNoyZ4VANO8jjmMPh/0', '【倩女幽魂】七夜圣君', 'o73cGwU6gD5TG8Wr7c6ID8CQtR3s');
INSERT INTO `wx_voteimg_gift` VALUES (69, 15, 1127, 4, '80', 1503532592, 'http://wx.qlogo.cn/mmopen/ajNVdqHZLLCSK6ULgsmia22ZM0eISgl6JWwEU8BFZZeGQ95tBlatWwH7q9NBibiccgOvxfudNnlu48YtMiaeU3kG0A/0', 'wo', 'o73cGwf8apXUtOihkgBK_S1iPucM');
INSERT INTO `wx_voteimg_gift` VALUES (70, 15, 1071, 3, '81', 1503537392, 'http://wx.qlogo.cn/mmopen/Q3auHgzwzM5Q878Yhc8xtlJJyVX7Sw2HUnRxaojGPnoZ5aIevpJajl7hLzTynWu6mCa7MJVLQO2NVLQu0D4shTZoPhE1ZtZw74ln0BcWLbo/0', '蜘蛛', 'o73cGwQJVieizzZDBdJSDDIIgWpQ');
INSERT INTO `wx_voteimg_gift` VALUES (71, 15, 1071, 4, '82', 1503537408, 'http://wx.qlogo.cn/mmopen/Q3auHgzwzM5Q878Yhc8xtlJJyVX7Sw2HUnRxaojGPnoZ5aIevpJajl7hLzTynWu6mCa7MJVLQO2NVLQu0D4shTZoPhE1ZtZw74ln0BcWLbo/0', '蜘蛛', 'o73cGwQJVieizzZDBdJSDDIIgWpQ');
INSERT INTO `wx_voteimg_gift` VALUES (72, 15, 1083, 1, '83', 1503537888, 'http://wx.qlogo.cn/mmopen/kvEYORDAicI3y7zFLkO1mJohicGnk3IgcDElbRmjKGX6NGY7cEsdxU8wicMNd5RJfYGyKWqBxnNIsAWeceMr7mYBjCLxGtibWicpa/0', '晴天', 'o73cGwQcy61ov2HRUmfCPILLf0vU');
INSERT INTO `wx_voteimg_gift` VALUES (73, 15, 1083, 1, '84', 1503541990, 'http://wx.qlogo.cn/mmopen/kvEYORDAicI1MUPN80l83OfQSbEARKkmdtSJSAb1aEibKAX9wrm6HX4hmJc9vxruAeuma9AGutNaVWgvicialzkHZIECHLRT6yK1/0', '@（苏苏)导师兰子', 'o73cGwbmsjkwDHWVuRE9hD_3JhCE');
INSERT INTO `wx_voteimg_gift` VALUES (74, 15, 1071, 1, '85', 1503542543, 'http://wx.qlogo.cn/mmopen/Q3auHgzwzM6wQZxkq7d9yIPHFnlSllnM1dd6lZ99Pp7Ysa0zPC7t3SKh29lG0e0xZbteMAvXYiciaccSqRHPvLr42M9xNtPYmD4iadFMWn4Zbc/0', 'Renconte_', 'o73cGwf58HX6dCLn13_7DYrNUO60');
INSERT INTO `wx_voteimg_gift` VALUES (75, 15, 1132, 1, '86', 1503546980, 'http://wx.qlogo.cn/mmopen/Q3auHgzwzM7tbP7LtjIFBEZXap1eg9nHvVdq2tsroctQ3hQ2NmtXIWeoptXLWYEFibgf6UIGQ9pArGicOXvOfibBVKzOBkrMTZNA62mRlBGowQ/0', '志龙', 'o73cGwekbw1B1LY9ivjEdgn5F-2s');
INSERT INTO `wx_voteimg_gift` VALUES (76, 15, 1090, 1, '87', 1503552520, 'http://wx.qlogo.cn/mmopen/4YYHFun9lU1k3wwCjKBQHTSfFWvRQ40iaM194IW1Wnxyaaibf1kPxRGJGDc58edxCSqHvHyHmicJJibHPovqjUH5FANibJUwaWUTh/0', '潇潇', 'o73cGwShwlX9X7-KTqONntYojdnY');
INSERT INTO `wx_voteimg_gift` VALUES (77, 15, 1132, 1, '88', 1503555085, 'http://wx.qlogo.cn/mmopen/Q3auHgzwzM51l3MmmIibx4ibZ51ibS3oiafsMOyTRNndhoLiao3iaDIXeduiaIW6jaf5H8D2uAdZ56OEZPVwoG9H4VebQ/0', '', 'o73cGwesg87u9ifia2tf8nAGlMjc');
INSERT INTO `wx_voteimg_gift` VALUES (78, 15, 1041, 2, '89', 1503563241, 'http://wx.qlogo.cn/mmopen/4YYHFun9lU0CDHtZRia6rMP6wgyXAxh2cH0DPrMSUpruTsz8axDibXM4JLDfBruSVxxNCylrQkJnvQb54icH1A8InKlibfbhlb0Z/0', '〉微笑', 'o73cGwZ1Tcp4RK2s-yCdypO-4DoE');
INSERT INTO `wx_voteimg_gift` VALUES (79, 15, 1127, 4, '90', 1503567784, 'http://wx.qlogo.cn/mmopen/4YYHFun9lU0OqYcOysGgWLCweyjSQiaxcyIFuWTVVp5DUZwdqSxE5KTfWafyxekjPf6oXt9GF29QwboiacMYWoGj2yia4ycdnibv/0', '', 'o73cGwZqrXUSXClFAqwFDVUxUvQs');
INSERT INTO `wx_voteimg_gift` VALUES (80, 15, 1071, 1, '91', 1503571369, 'http://wx.qlogo.cn/mmopen/kvEYORDAicI0skuMUoWibGWE5Y0TzekQib0Yicexcg3qWhHSP6C9DKw02yiakXwkcHPn57DBo8A38XXftKPMUu3e3Dic0MCQwiclHwm/0', '绍瑜', 'o73cGwUhrZiJljM5IzT4r903rBnQ');
INSERT INTO `wx_voteimg_gift` VALUES (81, 15, 1041, 3, '92', 1503579087, 'http://wx.qlogo.cn/mmopen/kvEYORDAicI1kpDVv31hRMILMItHDM8NhhlosRxSdiaSo83tSgfyNpCpYuDCicZGQicVgZc19cGy8m1NYk6HaeFBtWX5TeE7ILYM/0', 'Coco', 'o73cGwVE49LF4iXeJrArOlrjsApQ');
INSERT INTO `wx_voteimg_gift` VALUES (82, 15, 1094, 2, '93', 1503582529, 'http://wx.qlogo.cn/mmopen/4YYHFun9lU2ZbgP36FrFU0W9feuS4odmoAg2TFxhkwXic2FYZWnrUL5U4FV9I4jglYwL7iafJZGDic2ic3OgsZAibJQ74mCsQsGpia/0', '凤凰', 'o73cGwZxm1uZK5M-piOb0hXPDLCE');
INSERT INTO `wx_voteimg_gift` VALUES (83, 15, 1129, 4, '94', 1503584986, 'http://wx.qlogo.cn/mmopen/kvEYORDAicI1kpDVv31hRMAqj5KVPq48gviaYFX1zAhPgQ2XmuddVN7DiaxBBtZ2ht0C3HjfA6QUZ7F22wHibgaAJfSGJWQMNpec/0', 'ナ臉猫', 'o73cGwX7gROu8HPsZuagWwzeZqX0');
INSERT INTO `wx_voteimg_gift` VALUES (84, 15, 1132, 4, '95', 1503617616, 'http://wx.qlogo.cn/mmopen/FcqfTESP3ZeYnGw1Wib24TLUouf2eJTuR6TYiaTxWeqdibbnFKmJaRyJ5icYthxQkPibB5FbM38hsZGPm1BvaSelxBy5c7a3PIJlia/0', '张俊波', 'o73cGwW9EF97rB6tX0v3Ftf736Z8');
INSERT INTO `wx_voteimg_gift` VALUES (85, 15, 1071, 4, '96', 1503621324, 'http://wx.qlogo.cn/mmopen/iaibOdkD8oPtnMCkiaTbU3m1Pu1AWricatqYFnXvFW2uPZp3qewXeY72oD74zBGUKHLyiarEH4RKmzI8hXbBQd3leGdFmUN5OJX1m/0', '*~开心就好~*', 'o73cGwZ21dngZQv1CfmggUuSrkCA');
INSERT INTO `wx_voteimg_gift` VALUES (86, 15, 1132, 1, '97', 1503631794, 'http://wx.qlogo.cn/mmopen/4YYHFun9lU38FHXkDBXYAEweMia6kvU8FiceFBZOhSt0GuIVKeQwL48AmEyIlp8VL8TUUoBJicibtqMFicz08pT5aNnKsvTCQLaib9/0', '杜益平', 'o73cGwcmniVeTribf9grwoKXWgao');
INSERT INTO `wx_voteimg_gift` VALUES (87, 15, 1135, 1, '98', 1503638736, 'http://wx.qlogo.cn/mmopen/ajNVdqHZLLC8oxVDHuB2zIBZCN7xVarnoTxWIsMUSXCZCRZYw8j5tZPakaFnzQH47Z2QRlO5gfUO2p44emhuxtU7hLadicPxMk5lpms2tL44/0', '　　　　　　　　', 'o73cGwTe4ZKgHe_2pjKtvknauNaM');
INSERT INTO `wx_voteimg_gift` VALUES (88, 15, 1071, 4, '99', 1503645558, 'http://wx.qlogo.cn/mmopen/kvEYORDAicI3y7zFLkO1mJtuhLttpKflicBagjs1JZ86WChYjEib59K7uqzKPXUxpNq87eNBqicibNVicjHoWRZhfA4N7nwQROOzfb/0', 'Nehzx', 'o73cGwbfsREdqzO4rI6nsgmIWtzM');
INSERT INTO `wx_voteimg_gift` VALUES (89, 15, 1083, 1, '100', 1503651363, '/0', '艺豪雕塑2', 'o73cGwZfLRYq2s3JcuVj64GwdCDg');
INSERT INTO `wx_voteimg_gift` VALUES (90, 15, 1083, 4, '101', 1503659459, 'http://wx.qlogo.cn/mmopen/kvEYORDAicI1kpDVv31hRMCl25JcDclqxNJXq5Zib9qCicOCH6ib54PE4fU24T1TEcbN108SPVsT4KaRr27PzEib0R6gj41CFcxUZ/0', '空白格', 'o73cGwQX8_c1m3ftzh5DY0WXsFks');
INSERT INTO `wx_voteimg_gift` VALUES (91, 15, 1083, 4, '102', 1503659479, 'http://wx.qlogo.cn/mmopen/kvEYORDAicI1kpDVv31hRMCl25JcDclqxNJXq5Zib9qCicOCH6ib54PE4fU24T1TEcbN108SPVsT4KaRr27PzEib0R6gj41CFcxUZ/0', '空白格', 'o73cGwQX8_c1m3ftzh5DY0WXsFks');
INSERT INTO `wx_voteimg_gift` VALUES (92, 15, 1041, 1, '103', 1503663154, 'http://wx.qlogo.cn/mmopen/kvEYORDAicI1kpDVv31hRMAqj5KVPq48gviaYFX1zAhPgQ2XmuddVN7DiaxBBtZ2ht0C3HjfA6QUZ7F22wHibgaAJfSGJWQMNpec/0', 'ナ臉猫', 'o73cGwX7gROu8HPsZuagWwzeZqX0');
INSERT INTO `wx_voteimg_gift` VALUES (93, 15, 1129, 3, '104', 1503663202, 'http://wx.qlogo.cn/mmopen/kvEYORDAicI1kpDVv31hRMAqj5KVPq48gviaYFX1zAhPgQ2XmuddVN7DiaxBBtZ2ht0C3HjfA6QUZ7F22wHibgaAJfSGJWQMNpec/0', 'ナ臉猫', 'o73cGwX7gROu8HPsZuagWwzeZqX0');
INSERT INTO `wx_voteimg_gift` VALUES (94, 15, 1129, 4, '105', 1503663332, 'http://wx.qlogo.cn/mmopen/kvEYORDAicI1kpDVv31hRMAqj5KVPq48gviaYFX1zAhPgQ2XmuddVN7DiaxBBtZ2ht0C3HjfA6QUZ7F22wHibgaAJfSGJWQMNpec/0', 'ナ臉猫', 'o73cGwX7gROu8HPsZuagWwzeZqX0');
INSERT INTO `wx_voteimg_gift` VALUES (95, 15, 1129, 1, '106', 1503663333, 'http://wx.qlogo.cn/mmopen/FcqfTESP3ZccNy7Z5KCFN4zxSTjBnqCBTeVpxtxrQib2vc8Gicb4rFZ8kvWmm6WMFOaYApQN1M8cV8pGO6tuPLib9pMBk2R00xB/0', '涵涵', 'o73cGwQWPS3ScgfWtKjV5A-qIiz4');
INSERT INTO `wx_voteimg_gift` VALUES (96, 15, 1129, 4, '107', 1503663491, 'http://wx.qlogo.cn/mmopen/FcqfTESP3ZccNy7Z5KCFN4zxSTjBnqCBTeVpxtxrQib2vc8Gicb4rFZ8kvWmm6WMFOaYApQN1M8cV8pGO6tuPLib9pMBk2R00xB/0', '涵涵', 'o73cGwQWPS3ScgfWtKjV5A-qIiz4');
INSERT INTO `wx_voteimg_gift` VALUES (97, 15, 1041, 1, '108', 1503665287, 'http://wx.qlogo.cn/mmopen/ajNVdqHZLLCvWnqSGq8tmlqG35RyB18Qqxhl7ibMgaia8HgOmPHOOI1eLFpTSosqtGp5F1KlZW0FLtK1Fcb01hiaw/0', '笑话', 'o73cGwU8RghQlz1L2H1859kSNmiY');
INSERT INTO `wx_voteimg_gift` VALUES (98, 15, 1094, 1, '109', 1503665696, 'http://wx.qlogo.cn/mmopen/FMajU52WvbEjxyf6tibMC9hbCKbhvCcmGdxYVlrjZ9iaRlRksJ6ZmIgwR6pojLC7u4GMeaZePPaK8EEicL3Cia8a0A/0', '老大', 'o73cGwZCDW503XjcPuANQ8Ibyfcs');
INSERT INTO `wx_voteimg_gift` VALUES (99, 15, 1041, 1, '110', 1503669877, 'http://wx.qlogo.cn/mmopen/Q3auHgzwzM41VrNdicfribwhlIKFDODyymRCmXCEib7B0Tk3U7FVR02lTuL4rV3ic1spq7hMicmSo6A2d1cKx5OI40aWKrS7jRawtpeJUogiaA9fE/0', '威韡', 'o73cGwUeImWT_CY-bV0na3aiGoUo');
INSERT INTO `wx_voteimg_gift` VALUES (100, 15, 1127, 1, '111', 1503700196, 'http://wx.qlogo.cn/mmopen/PiajxSqBRaEJB9AM7mNy5femQeULlTGWrxlwuc1rV6Bnk4icfRlgHTPOhZKTuFtuYYRJV0iajSeXUBnOMcE3zaYLA/0', 'hai', 'o73cGwXO2PNVjJVhnuapC5JnNdpA');
INSERT INTO `wx_voteimg_gift` VALUES (101, 15, 1129, 2, '112', 1503707846, 'http://wx.qlogo.cn/mmopen/kvEYORDAicI1kpDVv31hRMAqj5KVPq48gviaYFX1zAhPgQ2XmuddVN7DiaxBBtZ2ht0C3HjfA6QUZ7F22wHibgaAJfSGJWQMNpec/0', 'ナ臉猫', 'o73cGwX7gROu8HPsZuagWwzeZqX0');
INSERT INTO `wx_voteimg_gift` VALUES (102, 15, 1129, 3, '113', 1503708203, 'http://wx.qlogo.cn/mmopen/kvEYORDAicI1kpDVv31hRMAqj5KVPq48gviaYFX1zAhPgQ2XmuddVN7DiaxBBtZ2ht0C3HjfA6QUZ7F22wHibgaAJfSGJWQMNpec/0', 'ナ臉猫', 'o73cGwX7gROu8HPsZuagWwzeZqX0');
INSERT INTO `wx_voteimg_gift` VALUES (103, 15, 1129, 4, '114', 1503708775, 'http://wx.qlogo.cn/mmopen/kvEYORDAicI1kpDVv31hRMAqj5KVPq48gviaYFX1zAhPgQ2XmuddVN7DiaxBBtZ2ht0C3HjfA6QUZ7F22wHibgaAJfSGJWQMNpec/0', 'ナ臉猫', 'o73cGwX7gROu8HPsZuagWwzeZqX0');
INSERT INTO `wx_voteimg_gift` VALUES (104, 15, 1053, 4, '115', 1503717416, 'http://wx.qlogo.cn/mmopen/DfadJfheePwB74YNPYBicDUdAHUSlD01BXOiaXvrZfSRPuISkBe7wiaJGwmuw9nHIdLLiaeQOFgia7XtkxQQdV0jnrWPkQTof1opK/0', '', 'o73cGwSvkw5G-tf-K6uxHQy8km_U');
INSERT INTO `wx_voteimg_gift` VALUES (105, 15, 1094, 1, '116', 1503717840, 'http://wx.qlogo.cn/mmopen/FcqfTESP3ZefwdDQicqENuBDyyrPq0K4JE3d5ia6Wc5trwoA3Zrkh6cToHY4LnXJknMzkwqqibtrSicYUFbmAtj38ZP86F4TgJIf/0', 'LDL', 'o73cGwV77CpnPWex_2vlWQq6GOX4');
INSERT INTO `wx_voteimg_gift` VALUES (106, 15, 1071, 1, '117', 1503733191, 'http://wx.qlogo.cn/mmopen/kvEYORDAicI1kpDVv31hRMFynMOgAhIxvnOwx5JKuD248kgx0kUiaMJMRhN54y7zTt3okKEfECBIPbibSRKtia8hKOLWhGEC7b5F/0', '张兰', 'o73cGwXchFr3BlWtUTnoawY7UckY');
INSERT INTO `wx_voteimg_gift` VALUES (107, 15, 1053, 4, '118', 1503734270, 'http://wx.qlogo.cn/mmopen/DfadJfheePwB74YNPYBicDUdAHUSlD01BXOiaXvrZfSRPuISkBe7wiaJGwmuw9nHIdLLiaeQOFgia7XtkxQQdV0jnrWPkQTof1opK/0', '', 'o73cGwSvkw5G-tf-K6uxHQy8km_U');
INSERT INTO `wx_voteimg_gift` VALUES (108, 15, 1053, 4, '119', 1503734290, 'http://wx.qlogo.cn/mmopen/DfadJfheePwB74YNPYBicDUdAHUSlD01BXOiaXvrZfSRPuISkBe7wiaJGwmuw9nHIdLLiaeQOFgia7XtkxQQdV0jnrWPkQTof1opK/0', '', 'o73cGwSvkw5G-tf-K6uxHQy8km_U');
INSERT INTO `wx_voteimg_gift` VALUES (109, 15, 1132, 1, '120', 1503752963, 'http://wx.qlogo.cn/mmopen/4YYHFun9lU3pPpSEVSrgZdRaF6HPqiaMnAaz8qxnRiaIIDZbSWWYzRbJSkcwzhKxgfWlnzKBs7icQYdALib5IvFQvuwPvCtzqvO2/0', '美丽的夕阳', 'o73cGwb5gDz-NUbT7EJO_h_eCDsI');
INSERT INTO `wx_voteimg_gift` VALUES (110, 15, 1041, 4, '121', 1503757110, 'http://wx.qlogo.cn/mmopen/4YYHFun9lU12GibGHkDjia7xqjz5Mx7Y7unaSgMoyicPibAg4zlfqmXiasM826N2T947pUWiblARuSsnYmySCv6L7Dc7jUxfcNgxU5/0', '飞哥', 'o73cGwel30qLW71ZbknYHF0BLvQo');
INSERT INTO `wx_voteimg_gift` VALUES (111, 15, 1041, 1, '122', 1503763639, 'http://wx.qlogo.cn/mmopen/Q3auHgzwzM79cglYLjuRbicZkKaIs2EzuglXdcqKYTFiaapUY3KYHFdoQSkhF5lLdRsbxPccAuY6ianeQAsw1as6w/0', '爱上你哟', 'o73cGwWi_x-01BS8TvPU16SB3sGU');
INSERT INTO `wx_voteimg_gift` VALUES (112, 15, 1090, 4, '123', 1503787968, 'http://wx.qlogo.cn/mmopen/kvEYORDAicI1zXAJ75DoJNcDhHqaoQgofKfiakhVZhyLCO1jynpZqXjhOubv4YJ9aUDYSzCdEqP1qjEUMgRj75wE3OLibWp3pFX/0', '笑对人生$', 'o73cGwXTfcTHWeyfFT7rjf57WGbI');
INSERT INTO `wx_voteimg_gift` VALUES (113, 15, 1041, 3, '124', 1503789727, 'http://wx.qlogo.cn/mmopen/Q3auHgzwzM41VrNdicfribwhlIKFDODyymRCmXCEib7B0Tk3U7FVR02lTuL4rV3ic1spq7hMicmSo6A2d1cKx5OI40aWKrS7jRawtpeJUogiaA9fE/0', '威韡', 'o73cGwUeImWT_CY-bV0na3aiGoUo');
INSERT INTO `wx_voteimg_gift` VALUES (114, 15, 1041, 1, '125', 1503791448, 'http://wx.qlogo.cn/mmopen/4YYHFun9lU0FLmQsyPDBB9liaooqrO3GK0tXPWzM6YVEvjCFUaN0Nkd9NJsVheJClicjiaweUlCInToyibkUibS8lpSBgCRiavsSeO/0', '梅花香自苦寒来', 'o73cGwUmKgeQ_oRuZxev456AQaWA');
INSERT INTO `wx_voteimg_gift` VALUES (115, 15, 1132, 1, '126', 1503792061, 'http://wx.qlogo.cn/mmopen/kvEYORDAicI3wc57lqF28OlFUNxmIXoyKSbAkawgD5FbhCh4TXMjbkCJNqDkkX3iaA1IPV6YW8kQsMwlHxW4K3xsob0mNzA9Dx/0', '', 'o73cGwZjlZHEnHPpziQZ3K__lyNw');
INSERT INTO `wx_voteimg_gift` VALUES (116, 15, 1132, 2, '127', 1503792709, 'http://wx.qlogo.cn/mmopen/4YYHFun9lU38FHXkDBXYAEweMia6kvU8FiceFBZOhSt0GuIVKeQwL48AmEyIlp8VL8TUUoBJicibtqMFicz08pT5aNnKsvTCQLaib9/0', '杜益平', 'o73cGwcmniVeTribf9grwoKXWgao');
INSERT INTO `wx_voteimg_gift` VALUES (117, 15, 1071, 4, '128', 1503796933, 'http://wx.qlogo.cn/mmopen/PiajxSqBRaEKD0m1lKH3JuYfbVLWsribZFGhK5BhJD8WhtBRibtadhMQqjn10NYWSn9K7GiaCU5m4GJcEasGgMnDicuU82twR1Otp0T2SJpHPS8o/0', '曾曾同学', 'o73cGwbYox2Oc3ypbuMMSFFLvl4I');
INSERT INTO `wx_voteimg_gift` VALUES (118, 15, 1094, 4, '129', 1503802258, 'http://wx.qlogo.cn/mmopen/4YYHFun9lU3X7QwG0rN0qeyJcbbSveeUCDXaANCozJ2HfeFtErtP5LZZ52f1tiavib1IAVpcnHcePQxvnIkrauKaE6uMgzFOMQ/0', '', 'o73cGweB7djARDADY6J6tYuelG5s');
INSERT INTO `wx_voteimg_gift` VALUES (119, 15, 1094, 4, '130', 1503802296, 'http://wx.qlogo.cn/mmopen/4YYHFun9lU3X7QwG0rN0qeyJcbbSveeUCDXaANCozJ2HfeFtErtP5LZZ52f1tiavib1IAVpcnHcePQxvnIkrauKaE6uMgzFOMQ/0', '', 'o73cGweB7djARDADY6J6tYuelG5s');
INSERT INTO `wx_voteimg_gift` VALUES (120, 15, 1094, 4, '131', 1503802350, 'http://wx.qlogo.cn/mmopen/4YYHFun9lU3X7QwG0rN0qeyJcbbSveeUCDXaANCozJ2HfeFtErtP5LZZ52f1tiavib1IAVpcnHcePQxvnIkrauKaE6uMgzFOMQ/0', '', 'o73cGweB7djARDADY6J6tYuelG5s');
INSERT INTO `wx_voteimg_gift` VALUES (121, 15, 1094, 1, '132', 1503802628, 'http://wx.qlogo.cn/mmopen/4YYHFun9lU3X7QwG0rN0qeyJcbbSveeUCDXaANCozJ2HfeFtErtP5LZZ52f1tiavib1IAVpcnHcePQxvnIkrauKaE6uMgzFOMQ/0', '', 'o73cGweB7djARDADY6J6tYuelG5s');
INSERT INTO `wx_voteimg_gift` VALUES (122, 15, 1132, 4, '133', 1503811206, 'http://wx.qlogo.cn/mmopen/FcqfTESP3ZeYnGw1Wib24TLUouf2eJTuR6TYiaTxWeqdibbnFKmJaRyJ5icYthxQkPibB5FbM38hsZGPm1BvaSelxBy5c7a3PIJlia/0', '张俊波', 'o73cGwW9EF97rB6tX0v3Ftf736Z8');
INSERT INTO `wx_voteimg_gift` VALUES (123, 15, 1094, 3, '134', 1503812043, 'http://wx.qlogo.cn/mmopen/FcqfTESP3ZeJIyEyYqIuLrlF7Q5AlO0qy4gdnJmW92DicDat0Kjrm1CngtMicf6ia03NdGvEYEdFbiacNFZRUzKGnMic628BxhHtz/0', '何代琴', 'o73cGwZKt5NfjumYtA2c4dUvnIkc');
INSERT INTO `wx_voteimg_gift` VALUES (124, 15, 1094, 1, '135', 1503812068, 'http://wx.qlogo.cn/mmopen/FcqfTESP3ZeJIyEyYqIuLrlF7Q5AlO0qy4gdnJmW92DicDat0Kjrm1CngtMicf6ia03NdGvEYEdFbiacNFZRUzKGnMic628BxhHtz/0', '何代琴', 'o73cGwZKt5NfjumYtA2c4dUvnIkc');
INSERT INTO `wx_voteimg_gift` VALUES (125, 15, 1094, 4, '136', 1503822005, 'http://wx.qlogo.cn/mmopen/DfadJfheePwB74YNPYBicDaqE13mtHQzyIw37XecLsYJuQOibBaA2NicGQ6HibmhEbvPpPVSmQ8Vg3Vj5bQ5bFrLy6cEnAODTT3D/0', '凡人', 'o73cGwctijuWmAa7HeVXLbV_VTfM');
INSERT INTO `wx_voteimg_gift` VALUES (126, 15, 1094, 4, '137', 1503824434, 'http://wx.qlogo.cn/mmopen/FcqfTESP3Zcm20iclkm68iafqBV3bhFkzJ6bLlDMDIAcSPUAkk2tLm6zf5qWq6tQAWjAbjLEQQVv8HRCeLaNCmZYUUhCnoemBu/0', 'HDy', 'o73cGwcAvgRbujYWTSzoxgZmw1r4');
INSERT INTO `wx_voteimg_gift` VALUES (127, 15, 1094, 4, '138', 1503824658, 'http://wx.qlogo.cn/mmopen/FcqfTESP3Zcm20iclkm68iafqBV3bhFkzJ6bLlDMDIAcSPUAkk2tLm6zf5qWq6tQAWjAbjLEQQVv8HRCeLaNCmZYUUhCnoemBu/0', 'HDy', 'o73cGwcAvgRbujYWTSzoxgZmw1r4');
INSERT INTO `wx_voteimg_gift` VALUES (128, 15, 1094, 4, '139', 1503826439, 'http://wx.qlogo.cn/mmopen/FcqfTESP3Zcm20iclkm68iafqBV3bhFkzJ6bLlDMDIAcSPUAkk2tLm6zf5qWq6tQAWjAbjLEQQVv8HRCeLaNCmZYUUhCnoemBu/0', 'HDy', 'o73cGwcAvgRbujYWTSzoxgZmw1r4');
INSERT INTO `wx_voteimg_gift` VALUES (129, 15, 1038, 3, '140', 1503842651, 'http://wx.qlogo.cn/mmopen/kvEYORDAicI2t5yWvWOmib2Du0SE20wibOicS9K8ym2IKNT6aickXgicBzUNQ88WBtRXBzsMj5L7Kbzo5ZBIk5DVO2yib19CwE5NJWR/0', '果果*', 'o73cGwfpjYGJYZUZlVxJjREC_5XU');
INSERT INTO `wx_voteimg_gift` VALUES (130, 15, 1094, 2, '141', 1503843969, 'http://wx.qlogo.cn/mmopen/kvEYORDAicI1icHLCh2O5diaJSRSrRicOND2M8D98RXsDCcXxd2qyCU0BVDicKWGvlkMOiaMegrddkTOx52iaLFrZTCB9JjyvEIV06S/0', '田锋', 'o73cGwZ4TPEqDWX4FmCJKC936mtE');
INSERT INTO `wx_voteimg_gift` VALUES (131, 15, 1071, 4, '142', 1503859917, 'http://wx.qlogo.cn/mmopen/FcqfTESP3ZeJIyEyYqIuLia00j8sLLMkhsTa0TK4FVWhaLIZy5t0FEoTnOZgaYf35icx8l2PvGDpw4lNHJWGRictib1nIyjv4miaB/0', '', 'o73cGwV8u_sC4XwdDva2zz_Qq9qo');
INSERT INTO `wx_voteimg_gift` VALUES (132, 15, 1083, 1, '143', 1503874143, 'http://wx.qlogo.cn/mmopen/ajNVdqHZLLDMWeLP8GaWIL0XzVV89HPoacteOLFA7D6JHcrMMrpTtR9dstiaib7XQSicDSpu0XSo4tAt47cm2d0hw/0', '风往尘香花已尽', 'o73cGwaBA0t6FJYbfZ3CT1f--8y8');
INSERT INTO `wx_voteimg_gift` VALUES (133, 15, 1132, 4, '144', 1503876681, 'http://wx.qlogo.cn/mmopen/4YYHFun9lU38FHXkDBXYAEweMia6kvU8FiceFBZOhSt0GuIVKeQwL48AmEyIlp8VL8TUUoBJicibtqMFicz08pT5aNnKsvTCQLaib9/0', '杜益平', 'o73cGwcmniVeTribf9grwoKXWgao');
INSERT INTO `wx_voteimg_gift` VALUES (134, 15, 1132, 4, '145', 1503877369, 'http://wx.qlogo.cn/mmopen/FcqfTESP3ZeYnGw1Wib24TLUouf2eJTuR6TYiaTxWeqdibbnFKmJaRyJ5icYthxQkPibB5FbM38hsZGPm1BvaSelxBy5c7a3PIJlia/0', '张俊波', 'o73cGwW9EF97rB6tX0v3Ftf736Z8');
INSERT INTO `wx_voteimg_gift` VALUES (135, 15, 1094, 4, '146', 1503877975, 'http://wx.qlogo.cn/mmopen/4YYHFun9lU3X7QwG0rN0qeyJcbbSveeUCDXaANCozJ2HfeFtErtP5LZZ52f1tiavib1IAVpcnHcePQxvnIkrauKaE6uMgzFOMQ/0', '', 'o73cGweB7djARDADY6J6tYuelG5s');
INSERT INTO `wx_voteimg_gift` VALUES (136, 15, 1041, 1, '147', 1503885121, 'http://wx.qlogo.cn/mmopen/FcqfTESP3ZdBWNiae5IjFY8iaMbZ410p8EDIZ70kqPJyHHE3PdDz5ynKlT1v6aGrj78ltzlKh53Xd9gSkPFcBCnxdvxzI5r8f4/0', '无忧无虑', 'o73cGwebaXRP4bhYUHKRgxK35elw');
INSERT INTO `wx_voteimg_gift` VALUES (137, 15, 1071, 1, '148', 1503886337, 'http://wx.qlogo.cn/mmopen/FMajU52WvbFa7DwCVhcI9pwX3wztib8geDr66mTqSjLa0Zeup7kMABib4icQQORRJWBMYuoxfNLzeXfT77K3M7BBHL1kSmjml7f/0', '刁仙', 'o73cGwQHUsDywbQyeHSif6gcxi2E');
INSERT INTO `wx_voteimg_gift` VALUES (138, 15, 1132, 1, '149', 1503889754, 'http://wx.qlogo.cn/mmopen/4YYHFun9lU2yKQzYh0p4EoTWX1gkaCUN4z5twHJE26jxCibDRStFyg6Pk3vxtrDicxqcvy7HDk9lMaQ2ibZsCN1hyMAdNs6PDHia/0', '廖菊兰', 'o73cGwfR8LWruMO9XlzhBxfErnTk');
INSERT INTO `wx_voteimg_gift` VALUES (139, 15, 1132, 1, '150', 1503894742, 'http://wx.qlogo.cn/mmopen/FMajU52WvbFkiaf3icANk5hZgkVpAtNU23vlSib8nmHhAwyR7MGIrYqMHib95wSx8whYQiaxFjLFdfo6smXo98WJl8rT6E5kXS3ibt/0', '李骥', 'o73cGwaTgSNFrqBR5lpGhqX7lY1o');
INSERT INTO `wx_voteimg_gift` VALUES (140, 15, 1094, 1, '151', 1503909541, 'http://wx.qlogo.cn/mmopen/DfadJfheePwB74YNPYBicDaqE13mtHQzyIw37XecLsYJuQOibBaA2NicGQ6HibmhEbvPpPVSmQ8Vg3Vj5bQ5bFrLy6cEnAODTT3D/0', '凡人', 'o73cGwctijuWmAa7HeVXLbV_VTfM');
INSERT INTO `wx_voteimg_gift` VALUES (141, 15, 1080, 4, '152', 1503930368, 'http://wx.qlogo.cn/mmopen/FcqfTESP3ZeJIyEyYqIuLqgEwPaJWeAfmHcx8ogkjicdHIEWQcfmicg90D1hPOq4ibRu93icZTNmXxWPkPfoKvZNjteshyDKyLFY/0', '菊', 'o73cGwc2Xmm126aPY175XxHvVwtU');
INSERT INTO `wx_voteimg_gift` VALUES (142, 15, 1132, 1, '153', 1503932091, 'http://wx.qlogo.cn/mmopen/kvEYORDAicI1fEwPWCvsvk8N7Eh4H9NxA3pwGezY0Ze1m48G55GAwNHAM8LOhFuvaZp5iboUSPrtiaERfsHTsPwy39Rq1PBibtlo/0', '天天妈', 'o73cGwSFJMuvLwXzvXJBlzKVDt8w');
INSERT INTO `wx_voteimg_gift` VALUES (143, 15, 1094, 4, '154', 1503961757, 'http://wx.qlogo.cn/mmopen/FcqfTESP3ZeVgCtMgO92P8Gicp8f250IljjrfXrr7x1K4X1A2naia846qicX7TxkvjHjpuKGjia70jPib5fa1KMTJM2c2QK4d5etW/0', '睿禾玥', 'o73cGwS9zhLblQuVVXQyMFjFkPGw');
INSERT INTO `wx_voteimg_gift` VALUES (144, 15, 1132, 4, '155', 1503961790, 'http://wx.qlogo.cn/mmopen/4YYHFun9lU38FHXkDBXYAEweMia6kvU8FiceFBZOhSt0GuIVKeQwL48AmEyIlp8VL8TUUoBJicibtqMFicz08pT5aNnKsvTCQLaib9/0', '杜益平', 'o73cGwcmniVeTribf9grwoKXWgao');
INSERT INTO `wx_voteimg_gift` VALUES (145, 15, 1132, 4, '156', 1503961831, 'http://wx.qlogo.cn/mmopen/4YYHFun9lU38FHXkDBXYAEweMia6kvU8FiceFBZOhSt0GuIVKeQwL48AmEyIlp8VL8TUUoBJicibtqMFicz08pT5aNnKsvTCQLaib9/0', '杜益平', 'o73cGwcmniVeTribf9grwoKXWgao');
INSERT INTO `wx_voteimg_gift` VALUES (146, 15, 1138, 1, '157', 1503970015, 'http://wx.qlogo.cn/mmopen/kvEYORDAicI1oDT1pyhsMMdcbnulfTx3qiaR812xRgoQ61fy2t3WIUFicrwvRxZKtiathJs7qdaVMth0VY25oegeWpH1Erw06XJB/0', '龙的传人', 'o73cGwcpb73IJmAsEX7t7GzZbQ20');
INSERT INTO `wx_voteimg_gift` VALUES (147, 15, 1094, 4, '158', 1503972638, 'http://wx.qlogo.cn/mmopen/kvEYORDAicI3kwH7wOb0VSZ6SITgbzDDV56Cvte1XaEXia7Vt597fv5odeo1dutU0kZ8nAcHKtq4C0vZpjQSnH2SE13B2qWhJC/0', '快乐时光', 'o73cGwcs83n-MjNmZkPcm-EFrkRE');
INSERT INTO `wx_voteimg_gift` VALUES (148, 15, 1094, 1, '159', 1503973342, 'http://wx.qlogo.cn/mmopen/FcqfTESP3ZccNy7Z5KCFNicqUxk7P703J7NHn3micdPMHYngUJkiapm5ALLggdcC7SzYUdreSvBia0jxoLu7ib2ECTW7AEhDozAEL/0', '', 'o73cGweMHgus1QO2dmQNS3HD4xEQ');
INSERT INTO `wx_voteimg_gift` VALUES (149, 15, 1094, 4, '160', 1503973374, 'http://wx.qlogo.cn/mmopen/FcqfTESP3ZccNy7Z5KCFNicqUxk7P703J7NHn3micdPMHYngUJkiapm5ALLggdcC7SzYUdreSvBia0jxoLu7ib2ECTW7AEhDozAEL/0', '', 'o73cGweMHgus1QO2dmQNS3HD4xEQ');
INSERT INTO `wx_voteimg_gift` VALUES (150, 15, 1094, 4, '161', 1503973963, 'http://wx.qlogo.cn/mmopen/FcqfTESP3ZccNy7Z5KCFNicqUxk7P703J7NHn3micdPMHYngUJkiapm5ALLggdcC7SzYUdreSvBia0jxoLu7ib2ECTW7AEhDozAEL/0', '', 'o73cGweMHgus1QO2dmQNS3HD4xEQ');
INSERT INTO `wx_voteimg_gift` VALUES (151, 15, 1129, 2, '162', 1503974021, 'http://wx.qlogo.cn/mmopen/4YYHFun9lU3a4E7CWVknW2ia9jzjgYHRsg3eoC88qQ2TYlgPnPgaVBQ0FjPqzMCt7ecuHz9T1ZjXqPCh9ftMpSMwNc42TOJYU/0', '平', 'o73cGwQ63CPLpare-XfG79y4OJfw');
INSERT INTO `wx_voteimg_gift` VALUES (152, 15, 1132, 1, '163', 1503975746, 'http://wx.qlogo.cn/mmopen/FcqfTESP3ZccNy7Z5KCFN7vNl9ZvahRu2zaI6F9Pib8n0qOaruwG9vpBpotlFtA1EPI81DLPPI7kWN94BPZiaRicZKxliaDzUTac/0', '刘炼 ', 'o73cGwSdxBv31tgzGqpjEHpqIcs4');
INSERT INTO `wx_voteimg_gift` VALUES (153, 15, 1094, 4, '164', 1503976172, 'http://wx.qlogo.cn/mmopen/kvEYORDAicI3kwH7wOb0VSZ6SITgbzDDV56Cvte1XaEXia7Vt597fv5odeo1dutU0kZ8nAcHKtq4C0vZpjQSnH2SE13B2qWhJC/0', '快乐时光', 'o73cGwcs83n-MjNmZkPcm-EFrkRE');
INSERT INTO `wx_voteimg_gift` VALUES (154, 15, 1132, 1, '165', 1503981600, 'http://wx.qlogo.cn/mmopen/MKcCI6ibkzYYbM64M5YtTUgh3twHc1ZhVr3WZ62x8sjcUmckhliclfeykGjnjSjt7XAgDed3pvaCiamUn8tibNfsRMoFjPBibKMUN/0', '令', 'o73cGwVpdysPUL0rk7fgaVeAu2ik');
INSERT INTO `wx_voteimg_gift` VALUES (155, 15, 1132, 4, '166', 1503983237, 'http://wx.qlogo.cn/mmopen/vi_32/n1liaTpt4LfcpSdl1eIeOQMiaqNnbwZNwwxT8jA8zqKUoghhRLIbta7x0dtrTZdbmVmZpaMsFDm9KZI51S6jIQsw/0', '张俊波', 'o73cGwW9EF97rB6tX0v3Ftf736Z8');
INSERT INTO `wx_voteimg_gift` VALUES (156, 15, 1132, 4, '167', 1503985191, 'http://wx.qlogo.cn/mmopen/vi_32/KldS4l1azvaqDBoe9bdskxhVgZBny7b0Dr68kerfpdficlvhmttFMjNh5bZ36jJQw9UibmllxAN8Ia7MAF1PjQWA/0', '', 'o73cGwZjlZHEnHPpziQZ3K__lyNw');
INSERT INTO `wx_voteimg_gift` VALUES (157, 15, 1132, 2, '168', 1503985204, 'http://wx.qlogo.cn/mmopen/vi_32/KldS4l1azvaqDBoe9bdskxhVgZBny7b0Dr68kerfpdficlvhmttFMjNh5bZ36jJQw9UibmllxAN8Ia7MAF1PjQWA/0', '', 'o73cGwZjlZHEnHPpziQZ3K__lyNw');
INSERT INTO `wx_voteimg_gift` VALUES (158, 15, 1094, 2, '169', 1503995602, 'http://wx.qlogo.cn/mmopen/FcqfTESP3ZefwdDQicqENuBDyyrPq0K4JE3d5ia6Wc5trwoA3Zrkh6cToHY4LnXJknMzkwqqibtrSicYUFbmAtj38ZP86F4TgJIf/0', 'LDL', 'o73cGwV77CpnPWex_2vlWQq6GOX4');
INSERT INTO `wx_voteimg_gift` VALUES (159, 15, 1094, 1, '170', 1504014305, 'http://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLa6vzvRvAvAc9WW6HKnaU2Ef71pYtq0Q5NF13eBwz61beyLeKnJTWKPqll8Jw2FSxNW0WNfXG15w/0', '好人一生平安', 'o73cGwdkHUlG5zgqTMZ-fMXC5rwM');
INSERT INTO `wx_voteimg_gift` VALUES (160, 15, 1083, 3, '171', 1504045184, 'http://wx.qlogo.cn/mmopen/vi_32/tQqGWy0HiaxibMjIUPmwXB5bU8ibNOxHP2wg5YeH9ia2qv1PfXqyFETVuUShvN5DXWl1MXuicUqBp9SxIOYnPXibdqVg/0', '一帘幽梦', 'o73cGwT00GaxAxMzZMg0mxkB_Rfw');
INSERT INTO `wx_voteimg_gift` VALUES (161, 15, 1132, 4, '172', 1504048340, 'http://wx.qlogo.cn/mmopen/vi_32/n1liaTpt4LfcpSdl1eIeOQMiaqNnbwZNwwxT8jA8zqKUoghhRLIbta7x0dtrTZdbmVmZpaMsFDm9KZI51S6jIQsw/0', '张俊波', 'o73cGwW9EF97rB6tX0v3Ftf736Z8');
INSERT INTO `wx_voteimg_gift` VALUES (162, 15, 1129, 4, '173', 1504050117, 'http://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIxEuD0ytJhQT1R4OE2s5jljuZYl6oiaNG017nfibWwtaEeNxHVSuPMQMoR2N5tCNNoDVzr2qRojw3w/0', 'ナ臉猫', 'o73cGwX7gROu8HPsZuagWwzeZqX0');
INSERT INTO `wx_voteimg_gift` VALUES (163, 15, 1132, 4, '174', 1504056318, 'http://wx.qlogo.cn/mmopen/4YYHFun9lU2ZbgP36FrFU3mmnyRLxTe1khyia7pQUmPXflhXfQibibInWGNTsgaZnLiaNU6Cj9K1UKcIEAFAF6Y2pGC1wQFt7d9L/0', '凤', 'o73cGwf5O9r4HALiUoqDXKi14fk0');
INSERT INTO `wx_voteimg_gift` VALUES (164, 15, 1132, 4, '175', 1504056339, 'http://wx.qlogo.cn/mmopen/4YYHFun9lU2ZbgP36FrFU3mmnyRLxTe1khyia7pQUmPXflhXfQibibInWGNTsgaZnLiaNU6Cj9K1UKcIEAFAF6Y2pGC1wQFt7d9L/0', '凤', 'o73cGwf5O9r4HALiUoqDXKi14fk0');
INSERT INTO `wx_voteimg_gift` VALUES (165, 15, 1132, 3, '176', 1504058821, 'http://wx.qlogo.cn/mmopen/4YYHFun9lU38FHXkDBXYAEweMia6kvU8FiceFBZOhSt0GuIVKeQwL48AmEyIlp8VL8TUUoBJicibtqMFicz08pT5aNnKsvTCQLaib9/0', '杜益平', 'o73cGwcmniVeTribf9grwoKXWgao');
INSERT INTO `wx_voteimg_gift` VALUES (166, 15, 1094, 4, '177', 1504059512, 'http://wx.qlogo.cn/mmopen/4YYHFun9lU3X7QwG0rN0qeyJcbbSveeUCDXaANCozJ2HfeFtErtP5LZZ52f1tiavib1IAVpcnHcePQxvnIkrauKaE6uMgzFOMQ/0', '', 'o73cGweB7djARDADY6J6tYuelG5s');
INSERT INTO `wx_voteimg_gift` VALUES (167, 15, 1094, 3, '178', 1504059698, 'http://wx.qlogo.cn/mmopen/4YYHFun9lU2MGnF3vjEtYgGwftRA19ywaYicIe4pxWCk4PFw6TzcxGwhff6eLqes5pXibwfvrcQGEhLpjeonPpkldq2Hm36YnE/0', '飞龙在天', 'o73cGwVwezO2TkQw7tHt-RsCThDU');
INSERT INTO `wx_voteimg_gift` VALUES (168, 15, 1094, 1, '179', 1504059762, 'http://wx.qlogo.cn/mmopen/4YYHFun9lU2MGnF3vjEtYgGwftRA19ywaYicIe4pxWCk4PFw6TzcxGwhff6eLqes5pXibwfvrcQGEhLpjeonPpkldq2Hm36YnE/0', '飞龙在天', 'o73cGwVwezO2TkQw7tHt-RsCThDU');
INSERT INTO `wx_voteimg_gift` VALUES (169, 15, 1094, 4, '180', 1504061473, 'http://wx.qlogo.cn/mmopen/vi_32/tANLzkWibMmjOn4iciaoR9Ze5vf0cI4elWynmMUKjlp0b3vVQiaZX4I4HtpB9jiae8FAEhiaMLhxgRkY5CpYgR8nNYOQ/0', '橘子菌', 'o73cGwWd9jhuPiyM9OVxb09Rf8iY');
INSERT INTO `wx_voteimg_gift` VALUES (170, 15, 1094, 4, '181', 1504061532, 'http://wx.qlogo.cn/mmopen/vi_32/tANLzkWibMmjOn4iciaoR9Ze5vf0cI4elWynmMUKjlp0b3vVQiaZX4I4HtpB9jiae8FAEhiaMLhxgRkY5CpYgR8nNYOQ/0', '橘子菌', 'o73cGwWd9jhuPiyM9OVxb09Rf8iY');
INSERT INTO `wx_voteimg_gift` VALUES (171, 15, 1094, 4, '182', 1504061564, 'http://wx.qlogo.cn/mmopen/vi_32/tANLzkWibMmjOn4iciaoR9Ze5vf0cI4elWynmMUKjlp0b3vVQiaZX4I4HtpB9jiae8FAEhiaMLhxgRkY5CpYgR8nNYOQ/0', '橘子菌', 'o73cGwWd9jhuPiyM9OVxb09Rf8iY');
INSERT INTO `wx_voteimg_gift` VALUES (172, 15, 1094, 4, '183', 1504061588, 'http://wx.qlogo.cn/mmopen/vi_32/tANLzkWibMmjOn4iciaoR9Ze5vf0cI4elWynmMUKjlp0b3vVQiaZX4I4HtpB9jiae8FAEhiaMLhxgRkY5CpYgR8nNYOQ/0', '橘子菌', 'o73cGwWd9jhuPiyM9OVxb09Rf8iY');
INSERT INTO `wx_voteimg_gift` VALUES (173, 15, 1094, 4, '184', 1504061608, 'http://wx.qlogo.cn/mmopen/vi_32/tANLzkWibMmjOn4iciaoR9Ze5vf0cI4elWynmMUKjlp0b3vVQiaZX4I4HtpB9jiae8FAEhiaMLhxgRkY5CpYgR8nNYOQ/0', '橘子菌', 'o73cGwWd9jhuPiyM9OVxb09Rf8iY');
INSERT INTO `wx_voteimg_gift` VALUES (174, 15, 1094, 4, '185', 1504061632, 'http://wx.qlogo.cn/mmopen/vi_32/tANLzkWibMmjOn4iciaoR9Ze5vf0cI4elWynmMUKjlp0b3vVQiaZX4I4HtpB9jiae8FAEhiaMLhxgRkY5CpYgR8nNYOQ/0', '橘子菌', 'o73cGwWd9jhuPiyM9OVxb09Rf8iY');
INSERT INTO `wx_voteimg_gift` VALUES (175, 15, 1094, 4, '186', 1504063527, 'http://wx.qlogo.cn/mmopen/FcqfTESP3ZeVqZ4W6Vcmt2cjnDKjHfqQfKMK7VkICFN3YYjBVM9wB8YjdgjKUJsbesHibONVeiax2CVk0JmRN9Q672B6H1RfZia/0', '啊姜', 'o73cGwVdhgAX20fqMeLI1UFrFy38');
INSERT INTO `wx_voteimg_gift` VALUES (176, 15, 1094, 4, '187', 1504063554, 'http://wx.qlogo.cn/mmopen/FcqfTESP3ZeVqZ4W6Vcmt2cjnDKjHfqQfKMK7VkICFN3YYjBVM9wB8YjdgjKUJsbesHibONVeiax2CVk0JmRN9Q672B6H1RfZia/0', '啊姜', 'o73cGwVdhgAX20fqMeLI1UFrFy38');
INSERT INTO `wx_voteimg_gift` VALUES (177, 15, 1094, 4, '188', 1504063568, 'http://wx.qlogo.cn/mmopen/FcqfTESP3ZeVqZ4W6Vcmt2cjnDKjHfqQfKMK7VkICFN3YYjBVM9wB8YjdgjKUJsbesHibONVeiax2CVk0JmRN9Q672B6H1RfZia/0', '啊姜', 'o73cGwVdhgAX20fqMeLI1UFrFy38');
INSERT INTO `wx_voteimg_gift` VALUES (178, 15, 1094, 4, '189', 1504063584, 'http://wx.qlogo.cn/mmopen/FcqfTESP3ZeVqZ4W6Vcmt2cjnDKjHfqQfKMK7VkICFN3YYjBVM9wB8YjdgjKUJsbesHibONVeiax2CVk0JmRN9Q672B6H1RfZia/0', '啊姜', 'o73cGwVdhgAX20fqMeLI1UFrFy38');
INSERT INTO `wx_voteimg_gift` VALUES (179, 15, 1094, 4, '190', 1504063600, 'http://wx.qlogo.cn/mmopen/FcqfTESP3ZeVqZ4W6Vcmt2cjnDKjHfqQfKMK7VkICFN3YYjBVM9wB8YjdgjKUJsbesHibONVeiax2CVk0JmRN9Q672B6H1RfZia/0', '啊姜', 'o73cGwVdhgAX20fqMeLI1UFrFy38');
INSERT INTO `wx_voteimg_gift` VALUES (180, 15, 1094, 4, '191', 1504064651, 'http://wx.qlogo.cn/mmopen/FcqfTESP3ZeVqZ4W6Vcmt2cjnDKjHfqQfKMK7VkICFN3YYjBVM9wB8YjdgjKUJsbesHibONVeiax2CVk0JmRN9Q672B6H1RfZia/0', '啊姜', 'o73cGwVdhgAX20fqMeLI1UFrFy38');
INSERT INTO `wx_voteimg_gift` VALUES (181, 15, 1094, 4, '192', 1504064674, 'http://wx.qlogo.cn/mmopen/FcqfTESP3ZeVqZ4W6Vcmt2cjnDKjHfqQfKMK7VkICFN3YYjBVM9wB8YjdgjKUJsbesHibONVeiax2CVk0JmRN9Q672B6H1RfZia/0', '啊姜', 'o73cGwVdhgAX20fqMeLI1UFrFy38');
INSERT INTO `wx_voteimg_gift` VALUES (182, 15, 1094, 4, '193', 1504064704, 'http://wx.qlogo.cn/mmopen/FcqfTESP3ZeVqZ4W6Vcmt2cjnDKjHfqQfKMK7VkICFN3YYjBVM9wB8YjdgjKUJsbesHibONVeiax2CVk0JmRN9Q672B6H1RfZia/0', '啊姜', 'o73cGwVdhgAX20fqMeLI1UFrFy38');
INSERT INTO `wx_voteimg_gift` VALUES (183, 15, 1094, 4, '194', 1504064727, 'http://wx.qlogo.cn/mmopen/FcqfTESP3ZeVqZ4W6Vcmt2cjnDKjHfqQfKMK7VkICFN3YYjBVM9wB8YjdgjKUJsbesHibONVeiax2CVk0JmRN9Q672B6H1RfZia/0', '啊姜', 'o73cGwVdhgAX20fqMeLI1UFrFy38');
INSERT INTO `wx_voteimg_gift` VALUES (184, 15, 1094, 4, '195', 1504064743, 'http://wx.qlogo.cn/mmopen/FcqfTESP3ZeVqZ4W6Vcmt2cjnDKjHfqQfKMK7VkICFN3YYjBVM9wB8YjdgjKUJsbesHibONVeiax2CVk0JmRN9Q672B6H1RfZia/0', '啊姜', 'o73cGwVdhgAX20fqMeLI1UFrFy38');
INSERT INTO `wx_voteimg_gift` VALUES (185, 15, 1094, 4, '196', 1504064758, 'http://wx.qlogo.cn/mmopen/FcqfTESP3ZeVqZ4W6Vcmt2cjnDKjHfqQfKMK7VkICFN3YYjBVM9wB8YjdgjKUJsbesHibONVeiax2CVk0JmRN9Q672B6H1RfZia/0', '啊姜', 'o73cGwVdhgAX20fqMeLI1UFrFy38');
INSERT INTO `wx_voteimg_gift` VALUES (186, 15, 1094, 3, '197', 1504065502, 'http://wx.qlogo.cn/mmopen/4YYHFun9lU3X7QwG0rN0qeyJcbbSveeUCDXaANCozJ2HfeFtErtP5LZZ52f1tiavib1IAVpcnHcePQxvnIkrauKaE6uMgzFOMQ/0', '', 'o73cGweB7djARDADY6J6tYuelG5s');
INSERT INTO `wx_voteimg_gift` VALUES (187, 15, 1071, 1, '198', 1504080300, 'http://wx.qlogo.cn/mmopen/vi_32/5DE6B2AkLKBg4iaB6usbIwXicwsDdiaQkCiaiarSBZR8H3tR9nqp5Sytib4gxXoPTQOjJIyhbbo9UNS83ajIhnBaacFQ/0', '童话', 'o73cGwbx2vbWLx8DLjAvgb6riJbQ');
INSERT INTO `wx_voteimg_gift` VALUES (188, 15, 1094, 4, '199', 1504133578, 'http://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTK5X6ZVA5XWBAcAUr221qwl9NDpnkrltmF9oxGOQYGtdkf9NqvwtcqDDBDoyRakJKxr2HDVZj32tw/0', '何永胜', 'o73cGwYIT02jJ11Bq6XkGsIvbCCE');
INSERT INTO `wx_voteimg_gift` VALUES (189, 15, 1071, 4, '200', 1504141176, 'http://wx.qlogo.cn/mmopen/vi_32/6iaaCMXJVLEFgzevPQERaQdxlnccU4ic8chciaSxBFvhSNib8yxPt8VHpvjUficwsTqY691GOx5tNIM83CwyY5qqvNw/0', '蜘蛛', 'o73cGwQJVieizzZDBdJSDDIIgWpQ');
INSERT INTO `wx_voteimg_gift` VALUES (190, 15, 1083, 1, '201', 1504141260, 'http://wx.qlogo.cn/mmopen/vi_32/dQQR8nfd3k8zO9Z7TjOxSomn9ko0LfdTia12mrDNJsY6ibpbO03ybcyPzRSF4QIQS8Nc4VaicuhBK4KicWfxn0tPQA/0', '晴天', 'o73cGwQcy61ov2HRUmfCPILLf0vU');
INSERT INTO `wx_voteimg_gift` VALUES (191, 15, 1071, 4, '202', 1504142292, 'http://wx.qlogo.cn/mmopen/vi_32/6iaaCMXJVLEFgzevPQERaQdxlnccU4ic8chciaSxBFvhSNib8yxPt8VHpvjUficwsTqY691GOx5tNIM83CwyY5qqvNw/0', '蜘蛛', 'o73cGwQJVieizzZDBdJSDDIIgWpQ');
INSERT INTO `wx_voteimg_gift` VALUES (192, 15, 1132, 4, '203', 1504142305, 'http://wx.qlogo.cn/mmopen/vi_32/n1liaTpt4LfcpSdl1eIeOQMiaqNnbwZNwwxT8jA8zqKUoghhRLIbta7x0dtrTZdbmVmZpaMsFDm9KZI51S6jIQsw/0', '张俊波', 'o73cGwW9EF97rB6tX0v3Ftf736Z8');
INSERT INTO `wx_voteimg_gift` VALUES (193, 15, 1071, 4, '204', 1504142484, 'http://wx.qlogo.cn/mmopen/vi_32/6iaaCMXJVLEFgzevPQERaQdxlnccU4ic8chciaSxBFvhSNib8yxPt8VHpvjUficwsTqY691GOx5tNIM83CwyY5qqvNw/0', '蜘蛛', 'o73cGwQJVieizzZDBdJSDDIIgWpQ');
INSERT INTO `wx_voteimg_gift` VALUES (194, 15, 1132, 4, '205', 1504142595, 'http://wx.qlogo.cn/mmopen/vi_32/IujJKkcVyOKwa7WHY7INuvQaTwqiaxzoRBgRx99OAd2GERrAaRsngVvwTNRkL4N7qnbFRribjdrCpAHXy6vxoWXQ/0', '杜益平', 'o73cGwcmniVeTribf9grwoKXWgao');
INSERT INTO `wx_voteimg_gift` VALUES (195, 15, 1071, 4, '206', 1504142608, 'http://wx.qlogo.cn/mmopen/vi_32/6iaaCMXJVLEFgzevPQERaQdxlnccU4ic8chciaSxBFvhSNib8yxPt8VHpvjUficwsTqY691GOx5tNIM83CwyY5qqvNw/0', '蜘蛛', 'o73cGwQJVieizzZDBdJSDDIIgWpQ');
INSERT INTO `wx_voteimg_gift` VALUES (196, 15, 1071, 4, '207', 1504142702, 'http://wx.qlogo.cn/mmopen/vi_32/6iaaCMXJVLEFgzevPQERaQdxlnccU4ic8chciaSxBFvhSNib8yxPt8VHpvjUficwsTqY691GOx5tNIM83CwyY5qqvNw/0', '蜘蛛', 'o73cGwQJVieizzZDBdJSDDIIgWpQ');
INSERT INTO `wx_voteimg_gift` VALUES (197, 15, 1071, 4, '208', 1504142797, 'http://wx.qlogo.cn/mmopen/vi_32/6iaaCMXJVLEFgzevPQERaQdxlnccU4ic8chciaSxBFvhSNib8yxPt8VHpvjUficwsTqY691GOx5tNIM83CwyY5qqvNw/0', '蜘蛛', 'o73cGwQJVieizzZDBdJSDDIIgWpQ');
INSERT INTO `wx_voteimg_gift` VALUES (198, 15, 1071, 4, '209', 1504142929, 'http://wx.qlogo.cn/mmopen/vi_32/6iaaCMXJVLEFgzevPQERaQdxlnccU4ic8chciaSxBFvhSNib8yxPt8VHpvjUficwsTqY691GOx5tNIM83CwyY5qqvNw/0', '蜘蛛', 'o73cGwQJVieizzZDBdJSDDIIgWpQ');
INSERT INTO `wx_voteimg_gift` VALUES (199, 15, 1094, 4, '210', 1504143286, 'http://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJicnNKApqG9UicCnnJvfR0KD7eLQut7YsO3fOQ7QxXdGHFt9PsXmYKL2ffmibA1zsf7kmvKdWAUHtng/0', '', 'o73cGweMHgus1QO2dmQNS3HD4xEQ');
INSERT INTO `wx_voteimg_gift` VALUES (200, 15, 1094, 4, '211', 1504143389, 'http://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJicnNKApqG9UicCnnJvfR0KD7eLQut7YsO3fOQ7QxXdGHFt9PsXmYKL2ffmibA1zsf7kmvKdWAUHtng/0', '', 'o73cGweMHgus1QO2dmQNS3HD4xEQ');
INSERT INTO `wx_voteimg_gift` VALUES (201, 15, 1094, 4, '212', 1504143907, 'http://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJicnNKApqG9UicCnnJvfR0KD7eLQut7YsO3fOQ7QxXdGHFt9PsXmYKL2ffmibA1zsf7kmvKdWAUHtng/0', '', 'o73cGweMHgus1QO2dmQNS3HD4xEQ');
INSERT INTO `wx_voteimg_gift` VALUES (202, 15, 1094, 4, '213', 1504146541, 'http://wx.qlogo.cn/mmopen/vi_32/zia53RUE441naJ2libQuWl2FzqRe5Bb1ST37oQKSwwDfwaWzal44Sa2ibfyw578vp2KGR2oE5ZWXyDlsNiaic3jjJDw/0', '', 'o73cGweB7djARDADY6J6tYuelG5s');
INSERT INTO `wx_voteimg_gift` VALUES (203, 15, 1094, 4, '214', 1504146633, 'http://wx.qlogo.cn/mmopen/vi_32/zia53RUE441naJ2libQuWl2FzqRe5Bb1ST37oQKSwwDfwaWzal44Sa2ibfyw578vp2KGR2oE5ZWXyDlsNiaic3jjJDw/0', '', 'o73cGweB7djARDADY6J6tYuelG5s');
INSERT INTO `wx_voteimg_gift` VALUES (204, 15, 1094, 4, '215', 1504152373, 'http://wx.qlogo.cn/mmopen/vi_32/BAuQlia9Dc04zZywpetYibC9ibesPygAKEsbxxib0ic3ezaibrliaOXm5ia2jnX0TVDlibfDpydyFNUwCbJduBKm2ibVF3zA/0', '谢强', 'o73cGwQky3zqZ4ym5oIXoHg884Ak');
INSERT INTO `wx_voteimg_gift` VALUES (205, 15, 1094, 4, '216', 1504153144, 'http://wx.qlogo.cn/mmopen/vi_32/zia53RUE441naJ2libQuWl2FzqRe5Bb1ST37oQKSwwDfwaWzal44Sa2ibfyw578vp2KGR2oE5ZWXyDlsNiaic3jjJDw/0', '', 'o73cGweB7djARDADY6J6tYuelG5s');
INSERT INTO `wx_voteimg_gift` VALUES (206, 15, 1094, 4, '217', 1504153848, 'http://wx.qlogo.cn/mmopen/vi_32/aTDKUevBoRxzwFoicQ32wJosKh61bFqal4qLYWWp5jVuD1e70iaGOfqjR6mEibkLjh0uXjHULeWjAofVIE1g7Zh5g/0', '不用谢#^_^#', 'o73cGwe1z9Wfh1vdsTJEGb8dov_4');
INSERT INTO `wx_voteimg_gift` VALUES (207, 15, 1094, 4, '218', 1504160358, 'http://wx.qlogo.cn/mmopen/vi_32/Js7KJfJj4jH5GwCSlDP9gVynyFoYaQsJfTEUSVEIckykAGbLqAUkY7msQXY4mm5u6RlprEqhjicdx4yBUVIjtoQ/0', '回忆', 'o73cGwQGwvvb_uqbpZJIozDyGrnk');
INSERT INTO `wx_voteimg_gift` VALUES (208, 15, 1071, 4, '219', 1504164214, 'http://wx.qlogo.cn/mmopen/vi_32/4ygzJtuheda6eicx1yVRuoOCXDWNXugAeymM0zLJf3RibYXVaU7fCJ1zKeFtjd2D6EI8vPLyiaX7KJE0w9fvg0mAg/0', 'Lai guo hui', 'o73cGwbyYt_3ARRA0dazOPkIi88s');
INSERT INTO `wx_voteimg_gift` VALUES (209, 15, 1132, 4, '220', 1504168464, 'http://wx.qlogo.cn/mmopen/vi_32/T0gZ8xrNwMpibJibpU92ZxMWnwolIqgaCnPzKfDTeBQSicsjl9uXzicbRex4faiaibicMyIzV8w89gSfoB2kdCGKDVB9g/0', '凤', 'o73cGwf5O9r4HALiUoqDXKi14fk0');
INSERT INTO `wx_voteimg_gift` VALUES (210, 15, 1094, 4, '221', 1504168524, 'http://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTI9Wf73uXyCyLBSFdIRmbyCFxqNHN48fBq8BzuKMIbRice1aWDqDtshG1oXPkUgBCP9MJHbFKOkMEA/0', '凡人', 'o73cGwctijuWmAa7HeVXLbV_VTfM');
INSERT INTO `wx_voteimg_gift` VALUES (211, 15, 1080, 4, '222', 1504172635, 'http://wx.qlogo.cn/mmopen/vi_32/J09GRz0ljica0s2XY7upic4iaE62AwcWpbmcCuggZUyIf4gMBg0hK9ZdeicXs60x1u81O4DCGNaibEcDK49EfZDMgUA/0', '菊', 'o73cGwc2Xmm126aPY175XxHvVwtU');
INSERT INTO `wx_voteimg_gift` VALUES (212, 15, 1094, 4, '223', 1504175271, 'http://wx.qlogo.cn/mmopen/vi_32/DYAIOgq83eqseBe7MHdmiamlf0Yug5fI6Ojl7U3HkxeicacG7bahiagTZoq0GCpdKIhI0RXK1Pibp02Zdww9gfY7ZA/0', '冷血', 'o73cGwWMW3K69TrlD13x6zBB2s7E');
INSERT INTO `wx_voteimg_gift` VALUES (213, 15, 1053, 1, '224', 1504181402, 'http://wx.qlogo.cn/mmopen/vi_32/DYAIOgq83eoc8sReia18fc9zBMicCGQncZrh6Yt3cicYHtzMxxZPHOaVH9JnsrohhCWHlgwdBZwn29vMmdkkh1rLg/0', '彤彤', 'o73cGwQtPerlJd34xTTKQnlHaceQ');
INSERT INTO `wx_voteimg_gift` VALUES (214, 15, 1132, 4, '225', 1504221745, 'http://wx.qlogo.cn/mmopen/vi_32/n1liaTpt4LfcpSdl1eIeOQMiaqNnbwZNwwxT8jA8zqKUoghhRLIbta7x0dtrTZdbmVmZpaMsFDm9KZI51S6jIQsw/0', '张俊波', 'o73cGwW9EF97rB6tX0v3Ftf736Z8');
INSERT INTO `wx_voteimg_gift` VALUES (215, 15, 1094, 4, '226', 1504221826, 'http://wx.qlogo.cn/mmopen/vi_32/aTDKUevBoRxzwFoicQ32wJosKh61bFqal4qLYWWp5jVuD1e70iaGOfqjR6mEibkLjh0uXjHULeWjAofVIE1g7Zh5g/0', '不用谢#^_^#', 'o73cGwe1z9Wfh1vdsTJEGb8dov_4');
INSERT INTO `wx_voteimg_gift` VALUES (216, 15, 1132, 1, '227', 1504221856, 'http://wx.qlogo.cn/mmopen/vi_32/n1liaTpt4LfcpSdl1eIeOQMiaqNnbwZNwwxT8jA8zqKUoghhRLIbta7x0dtrTZdbmVmZpaMsFDm9KZI51S6jIQsw/0', '张俊波', 'o73cGwW9EF97rB6tX0v3Ftf736Z8');
INSERT INTO `wx_voteimg_gift` VALUES (217, 15, 1094, 1, '228', 1504225353, 'http://wx.qlogo.cn/mmopen/vi_32/DGsVMibdPu1KQ7ha5vsBkBjxib95HbnpmkRzYGvH7AjWxsOCXQK4oAF4xPJJjia5BibaxVnJGkEBoHdZ5omKUHSE4g/0', '婷儿', 'o73cGwYNnU35mocJNcfjFVOXULbU');
INSERT INTO `wx_voteimg_gift` VALUES (218, 15, 1132, 1, '229', 1504225501, 'http://wx.qlogo.cn/mmopen/vi_32/DGsVMibdPu1KQ7ha5vsBkBjxib95HbnpmkRzYGvH7AjWxsOCXQK4oAF4xPJJjia5BibaxVnJGkEBoHdZ5omKUHSE4g/0', '婷儿', 'o73cGwYNnU35mocJNcfjFVOXULbU');
INSERT INTO `wx_voteimg_gift` VALUES (219, 15, 1094, 1, '230', 1504225625, 'http://wx.qlogo.cn/mmopen/vi_32/UlGaUTKWY6LRtTBsQJvicGxqSz0r1icRu6N7TIkiau6y9sKF9IemdxaNstmWNYsNluE5zb1mdPSJSHU8y3lCbibKdg/0', '想你，可不在回来', 'o73cGwYExM1XL6vLAYJvGgOtMiT0');
INSERT INTO `wx_voteimg_gift` VALUES (220, 15, 1132, 3, '231', 1504227201, 'http://wx.qlogo.cn/mmopen/vi_32/IujJKkcVyOKwa7WHY7INuvQaTwqiaxzoRBgRx99OAd2GERrAaRsngVvwTNRkL4N7qnbFRribjdrCpAHXy6vxoWXQ/0', '杜益平', 'o73cGwcmniVeTribf9grwoKXWgao');
INSERT INTO `wx_voteimg_gift` VALUES (221, 15, 1138, 4, '232', 1504228617, 'http://wx.qlogo.cn/mmopen/vi_32/dQQR8nfd3k8zO9Z7TjOxSomn9ko0LfdTia12mrDNJsY6ibpbO03ybcyPzRSF4QIQS8Nc4VaicuhBK4KicWfxn0tPQA/0', '晴天', 'o73cGwQcy61ov2HRUmfCPILLf0vU');
INSERT INTO `wx_voteimg_gift` VALUES (222, 15, 1094, 1, '233', 1504236013, 'http://wx.qlogo.cn/mmopen/vi_32/DYAIOgq83eqXmCwa043xJ0wnd4tqBjG3AP5bOAlvUiasrKLhS1iaAnZBVAmVoyoaibjmd6oZEB3licnsdN4wiaSHdYQ/0', '刘久玲', 'o73cGwcoqoOIIb5HVJQemWDOV9wk');
INSERT INTO `wx_voteimg_gift` VALUES (223, 15, 1071, 2, '234', 1504262404, 'http://wx.qlogo.cn/mmopen/vi_32/Br2eWib8ysIdUXBHOw7yJuO1FTXCibLhl5NIhGfYiaZdJu6meQWic5Lyu8KTicxZChDSUJQ8G9O7V1FS9w3SCC2yrFQ/0', '一个人要成实做事', 'o73cGwd0GBdV6ZdWX-RBDwFIra-M');
INSERT INTO `wx_voteimg_gift` VALUES (224, 15, 1094, 4, '235', 1504265321, 'http://wx.qlogo.cn/mmopen/vi_32/jib4NtzQJTqccmMUv7o9oMGpNz7IEQsAycwTRaibqIj7c4yFDczibLMeJibyqmDOfZBH9wTDtib0H2H8NmkfuAENvkA/0', '老魏', 'o73cGwVhb3rN8B5QdVedzEAK0ZB0');
INSERT INTO `wx_voteimg_gift` VALUES (225, 15, 1094, 4, '236', 1504269309, 'http://wx.qlogo.cn/mmopen/vi_32/aTDKUevBoRxzwFoicQ32wJosKh61bFqal4qLYWWp5jVuD1e70iaGOfqjR6mEibkLjh0uXjHULeWjAofVIE1g7Zh5g/0', '不用谢#^_^#', 'o73cGwe1z9Wfh1vdsTJEGb8dov_4');
INSERT INTO `wx_voteimg_gift` VALUES (226, 15, 1094, 4, '237', 1504319577, 'http://wx.qlogo.cn/mmopen/vi_32/aiaO77mTsCalcia49ElevPn2k09frPEFxic1ajmjGxjUFdicgBmSRLhdwibT9OvFsxOLoK7oMt9cW4ykAtGzYwnlO0Q/0', '何代刚', 'o73cGwdNb2AEaIew-eCOw5vp_0UM');
INSERT INTO `wx_voteimg_gift` VALUES (227, 15, 1094, 4, '238', 1504323965, 'http://wx.qlogo.cn/mmopen/vi_32/OSFvibYvaBKhUf8548aOxvic2KXxQhSh0tGCEbric1rPlNla8UwbBSwfZ3IgcFC6raiaG8erKmc7CG3gtt6WQnsKvQ/0', '梅', 'o73cGwcN_6QMZ1yYSf37olBJYluY');
INSERT INTO `wx_voteimg_gift` VALUES (228, 15, 1094, 1, '239', 1504329949, 'http://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTI9Wf73uXyCyLBSFdIRmbyCFxqNHN48fBq8BzuKMIbRice1aWDqDtshG1oXPkUgBCP9MJHbFKOkMEA/0', '凡人', 'o73cGwctijuWmAa7HeVXLbV_VTfM');
INSERT INTO `wx_voteimg_gift` VALUES (229, 15, 1094, 3, '240', 1504350108, 'http://wx.qlogo.cn/mmopen/vi_32/aTDKUevBoRxzwFoicQ32wJosKh61bFqal4qLYWWp5jVuD1e70iaGOfqjR6mEibkLjh0uXjHULeWjAofVIE1g7Zh5g/0', '不用谢#^_^#', 'o73cGwe1z9Wfh1vdsTJEGb8dov_4');
INSERT INTO `wx_voteimg_gift` VALUES (230, 15, 1094, 4, '241', 1504372458, 'http://wx.qlogo.cn/mmopen/vi_32/lgFibq2ekzhogM3I1pGhmUK6yU9ibj1KxcjAVoV8rHdPRo07Kgbju9VPekvvtI6HmNtfycmGIy27hzGPd16l319Q/0', 'HDy', 'o73cGwcAvgRbujYWTSzoxgZmw1r4');
INSERT INTO `wx_voteimg_gift` VALUES (231, 15, 1094, 4, '242', 1504373124, 'http://wx.qlogo.cn/mmopen/vi_32/lgFibq2ekzhogM3I1pGhmUK6yU9ibj1KxcjAVoV8rHdPRo07Kgbju9VPekvvtI6HmNtfycmGIy27hzGPd16l319Q/0', 'HDy', 'o73cGwcAvgRbujYWTSzoxgZmw1r4');
INSERT INTO `wx_voteimg_gift` VALUES (232, 15, 1094, 4, '243', 1504392218, 'http://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJicnNKApqG9UicCnnJvfR0KD7eLQut7YsO3fOQ7QxXdGHFt9PsXmYKL2ffmibA1zsf7kmvKdWAUHtng/0', '', 'o73cGweMHgus1QO2dmQNS3HD4xEQ');
INSERT INTO `wx_voteimg_gift` VALUES (233, 15, 1094, 4, '244', 1504392341, 'http://wx.qlogo.cn/mmopen/vi_32/DYAIOgq83eoVjaVYC3zqDuVUibGOw3mS7M4mqdltKS0QeD9njllt1rAkRg8SibA5KC1DroywVEyiaibUTsUrj3O4yw/0', '荷塘燕子飞', 'o73cGwXDxsEnM7tUfO1FyBzlUb0g');
INSERT INTO `wx_voteimg_gift` VALUES (234, 15, 1071, 1, '245', 1504395946, 'http://wx.qlogo.cn/mmopen/vi_32/DYAIOgq83eo6uMpeHVAfIdQID8sQFvhb5xicJMDtWJicPdia5Vok6A3kkeunD0FP9HzTqicpNjooSc1ye9CFRuHYIg/0', '满江红', 'o73cGwdDkXXgbe-EH_RmltwWpFY8');

-- ----------------------------
-- Table structure for wx_voteimg_item
-- ----------------------------
DROP TABLE IF EXISTS `wx_voteimg_item`;
CREATE TABLE `wx_voteimg_item`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `itemnum` int(11) NOT NULL COMMENT '选项id',
  `vote_id` int(11) NOT NULL COMMENT '图片投票id',
  `vote_title` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '图片描述',
  `vote_img` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '投票页面展示图300*300',
  `introduction` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '自我介绍',
  `manifesto` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '拉票宣言',
  `vote_imgs` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '所有图片地址',
  `jump_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `contact` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '手机号',
  `vote_count` int(11) NOT NULL COMMENT '获得票数',
  `upload_time` int(11) NOT NULL COMMENT '上传时间',
  `check_pass` tinyint(1) NOT NULL COMMENT '审核',
  `wecha_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `upload_type` tinyint(1) NOT NULL COMMENT '区分管理上传还是报名',
  `video_path` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ifhongbao` tinyint(1) NOT NULL,
  `record_time` int(11) NOT NULL,
  `upload_voice` varchar(800) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `alert_state` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `vote_id`(`vote_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1199 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wx_voteimg_item
-- ----------------------------
INSERT INTO `wx_voteimg_item` VALUES (986, 3, 1, 'Una', '/attachment/voteimg/201704/11/1491872113.jpg', '推女神巨乳美女于姬Una、极品性感女神，一组私房照和浴室湿身写真分享给大家，在这个阳光明媚的早晨，微暖的微光照射在私房里的于姬Una，顶级的美妙身材和后期充满朦胧感的处理，显得既有意境又充满诱惑力。', '推女神巨乳美女于姬Una、极品性感女神', '/attachment/voteimg/201704/11/1491872113.jpg,/attachment/voteimg/201704/11/1491872118.jpg', '', '33333333', 52, 0, 0, 'oYCDIt2DvUavx2N8nsGeyTB8sTc4', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (984, 1, 1, '慕羽茜', '/attachment/voteimg/201704/11/1491871893.jpg', '美艳尤物慕羽茜，尤果网平面模特儿，天生的漂亮脸蛋和恰到好处的完美身材，美腿，美胸让人难忘。穿着各种服饰的慕羽茜性感写真，展现出女人的万种风情。内衣、紧身皮衣、连衣裙、超短裙、每一种，每一件服饰都演绎的是性感而又美妙绝伦。', '内衣、紧身皮衣、连衣裙、超短裙、每一种，每一件服饰都演绎的是性感而又美妙绝伦。', '/attachment/voteimg/201704/11/1491871893.jpg,/attachment/voteimg/201704/11/1491871896.jpg,/attachment/voteimg/201704/11/1491871900.jpg', '', '11111111', 38, 0, 0, 'oYCDIt2DvUavx2N8nsGeyTB8sTc4', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (985, 2, 1, 'Jessica', '/attachment/voteimg/201704/11/1491872011.jpg', '短发美女赵欢颜Jessica，MiStar魅妍社新晋嫩模，最新私房写真系列第（一）篇和大家分享，穿着白色内衣的赵欢颜，火辣美艳身材一极棒，诱惑的身形以及性感的肢体语言，绝对让你大饱眼福！ 喜欢的请继续关注赵欢颜后续内容哦。。。', '短发美女赵欢颜Jessica，MiStar魅妍社新晋嫩模', '/attachment/voteimg/201704/11/1491872011.jpg,/attachment/voteimg/201704/11/1491872015.jpg,/attachment/voteimg/201704/11/1491872017.jpg', '', '22222222', 51, 0, 0, 'oYCDIt2DvUavx2N8nsGeyTB8sTc4', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (987, 4, 1, '喵喵', '/attachment/voteimg/201704/11/1491872195.jpg', '性感美女尤物喵喵健身房写真分享，穿着两种不同风格的喵喵，性感美臀巨乳身材惹人注目。白色紧身内衣的呼之欲出，黑丝连体丝袜内衣的女性魅力，都一一的展现在我们面前。', '白色紧身内衣的呼之欲出，黑丝连体丝袜内衣的女性魅力', '/attachment/voteimg/201704/11/1491872195.jpg,/attachment/voteimg/201704/11/1491872199.jpg,/attachment/voteimg/201704/11/1491872203.jpg', '', '444444444', 118, 0, 0, 'oYCDIt2DvUavx2N8nsGeyTB8sTc4', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (988, 5, 1, '的', '/attachment/voteimg/201704/12/1491975510.jpg', '试试', '试试', '/attachment/voteimg/201704/12/1491975510.jpg', '', '18457451110', 3, 0, 0, '', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (989, 6, 1, '网友测试', '/attachment/voteimg/201704/12/1491975510.jpg', '试试阿斯蒂芬是的2', '试试阿斯蒂芬2', '/attachment/voteimg/201704/12/1491975510.jpg,/attachment/voteimg/201704/12/1491977577.png', '', '18457451110', 53, 0, 0, 'admin', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (990, 1, 12, '铜梁杨记凤爪', '/attachment/voteimg/201704/14/1492140683.jpg', '杨记凤爪位于铜梁区明月广场拱桥旁，欢迎品尝。', '不一样的火辣辣，正宗铜梁味！', '/attachment/voteimg/201704/14/1492140683.jpg', '', '#', 8, 0, 0, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (991, 7, 1, '考虑考虑考虑', '/attachment/voteimg/201704/20/1492657301.PNG', '考虑考虑考虑了', '考虑考虑了', '/attachment/voteimg/201704/20/1492657301.PNG', '', '45655113', 1, 0, 0, 'o73cGwSj1eIiS-gGp8MYKAzDt2Jg', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1002, 18, 1, '罗熙桐', 'http://source.5atl.com/attachment/voteimg/201705/06/1494055708.jpg', '', '请忘记我的名字，请叫我健身女神', 'http://source.5atl.com/attachment/voteimg/201705/06/1494055708_bh.jpg', '', '0000-1111', 19, 0, 0, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (993, 9, 1, '海报测试', '/attachment/voteimg/201704/26/1493188379.jpg', '', '海报测试，更方便的投票方式', '/attachment/voteimg/201704/26/1493188379.jpg', '', '45655113', 1, 0, 0, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (994, 10, 1, '刘飞儿', 'http://source.5atl.com/attachment/voteimg/201704/26/1493191730.jpg', '', '我是大奶牛刘飞儿，为我加油吧', '/attachment/voteimg/201704/26/1493191730.jpg,http://source.5atl.com/attachment/voteimg/201704/26/1493191751.jpg', '', '0000-0000', 0, 0, 0, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (995, 11, 1, '张优', 'http://source.5atl.com/attachment/voteimg/201704/26/1493192068.jpg', '', '我是张优，身材全优', 'http://source.5atl.com/attachment/voteimg/201704/26/1493192068_bh.jpg,/attachment/voteimg/201704/26/1493192068.jpg,http://source.5atl.com/attachment/voteimg/201704/26/1493192072.jpg', '', '0000-2222', 5, 0, 0, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (996, 12, 1, '陈潇', 'http://source.5atl.com/attachment/voteimg/201704/26/1493192693.jpg', '', '我是陈潇，我比她们都漂亮。', 'http://source.5atl.com/attachment/voteimg/201704/26/1493192693_bh.jpg,/attachment/voteimg/201704/26/1493192693.jpg,http://source.5atl.com/attachment/voteimg/201704/26/1493192696.jpg', '', '0000-4444', 9, 0, 0, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (997, 13, 1, '夏小秋秋', 'http://source.5atl.com/attachment/voteimg/201704/26/1493192922.jpg', '', '清纯数我，谁人能敌', 'http://source.5atl.com/attachment/voteimg/201704/26/1493192922_bh.jpg,/attachment/voteimg/201704/26/1493192922.jpg,http://source.5atl.com/attachment/voteimg/201704/26/1493192925.jpg', '', '0000-5555', 4, 0, 0, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (998, 14, 1, '花瓣', 'http://source.5atl.com/attachment/voteimg/201704/26/1493193075.jpg', '', '我是花瓣女神，要出名上花瓣', 'http://source.5atl.com/attachment/voteimg/201704/26/1493193075_bh.jpg,/attachment/voteimg/201704/26/1493193075.jpg', '', '0000-5555', 5, 0, 0, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (999, 15, 1, '清新空气', 'http://source.5atl.com/attachment/voteimg/201704/26/1493193161.jpg', '', '跟我一起到处呼吸清新空气吧', 'http://source.5atl.com/attachment/voteimg/201704/26/1493193161_bh.jpg,/attachment/voteimg/201704/26/1493193161.jpg', '', '0000-6666', 3, 0, 0, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1000, 16, 1, '花儿朵朵', 'http://source.5atl.com/attachment/voteimg/201704/27/1493278299.jpg', '', '小婊砸，庆幸有你一路随行', 'http://source.5atl.com/attachment/voteimg/201704/27/1493278299_bh.jpg,/attachment/voteimg/201704/27/1493278299.jpg', '', '0000-9999', 4, 0, 0, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1001, 17, 1, '美仑美艳大表姐', 'http://source.5atl.com/attachment/voteimg/201704/30/1493513763.jpg', '', '美仑美艳的大表姐，太空旅客好霸气，随时随地干一逼', 'http://source.5atl.com/attachment/voteimg/201704/30/1493513763_bh.jpg,/attachment/voteimg/201704/30/1493513763.jpg', '', '0000-2222', 18, 0, 0, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1003, 1, 13, '那个她', 'http://source.5atl.com/attachment/voteimg/201705/06/1494058175.jpg', '', '十年前邻居的她，你还好吗？', 'http://source.5atl.com/attachment/voteimg/201705/06/1494058175_bh.jpg', '', '0000-1111', 2, 0, 0, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1004, 2, 13, '爱上爱吃我的菜的你', 'http://source.5atl.com/attachment/voteimg/201705/06/1494058974.jpg', '', '因为你的喜欢，我的菜有了意义', 'http://source.5atl.com/attachment/voteimg/201705/06/1494058974_bh.jpg', '', '0000-2222', 13, 0, 0, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1005, 3, 13, '我叫杰克，她叫肉丝', 'http://source.5atl.com/attachment/voteimg/201705/06/1494063438.jpg', '', '我们的爱情，起源于那一艘沉没的轮船', 'http://source.5atl.com/attachment/voteimg/201705/06/1494063438_bh.jpg', '', '0000-3333', 8, 0, 0, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1006, 4, 13, '西敏', 'http://source.5atl.com/attachment/voteimg/201705/08/1494212531.jpg', '', '一次离别，是无助的也是无奈的', 'http://source.5atl.com/attachment/voteimg/201705/08/1494212531_bh.jpg', '', '0000-3333', 0, 0, 0, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1007, 5, 13, '格利高里·派克', 'http://source.5atl.com/attachment/voteimg/201705/08/1494214374.jpg', '', '那年那时，你还是公主，而我骑着自行车', 'http://source.5atl.com/attachment/voteimg/201705/08/1494214374_bh.jpg', '', '0000-55555', 0, 0, 0, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1008, 6, 13, '赫本', 'http://source.5atl.com/attachment/voteimg/201705/08/1494214796.jpg', '', '那一年我走迷了路，你把我拐上了路', 'http://source.5atl.com/attachment/voteimg/201705/08/1494214796_bh.jpg', '', '0000--55555', 1, 0, 0, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1009, 7, 13, '梁山伯', 'http://source.5atl.com/attachment/voteimg/201705/08/1494214955.jpg', '', '一段爱情，总起源于一个人耍流氓', 'http://source.5atl.com/attachment/voteimg/201705/08/1494214955_bh.jpg', '', '0000-5555', 2, 0, 0, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1010, 8, 13, '黑寡妇', 'http://source.5atl.com/attachment/voteimg/201705/08/1494215093.jpg', '', '我喜欢的那个人，他从来没有注意到', 'http://source.5atl.com/attachment/voteimg/201705/08/1494215093_bh.jpg', '', '22222222', 0, 0, 0, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1011, 9, 13, '神奇女侠', 'http://source.5atl.com/attachment/voteimg/201705/08/1494215313.jpg', '', '我就是我，不惧人间烟火', 'http://source.5atl.com/attachment/voteimg/201705/08/1494215313_bh.jpg', '', '22222222', 2, 0, 0, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1012, 10, 13, '热巴', 'http://source.5atl.com/attachment/voteimg/201705/08/1494215467.jpg', '', '最爱热巴，有风险才有力量', 'http://source.5atl.com/attachment/voteimg/201705/08/1494215467_bh.jpg', '', '3333333', 0, 0, 0, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1013, 11, 13, '张萌', 'http://source.5atl.com/attachment/voteimg/201705/08/1494215727.jpg', '', '有颜值有魅力，就差关注我的你', 'http://source.5atl.com/attachment/voteimg/201705/08/1494215727_bh.jpg', '', '6666666', 1, 0, 0, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1014, 12, 13, '巴扎黑', 'http://source.5atl.com/attachment/voteimg/201705/08/1494226294.jpg', '', '我是小金鱼，关注我爱护我', 'http://source.5atl.com/attachment/voteimg/201705/08/1494226294_bh.jpg', '', '555555', 1, 0, 0, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1015, 1, 14, '红领巾唱响中国', 'http://source.5atl.com/attachment/upload/201705/31/14961950908624.jpg', ' \n							<p>铜梁区万花筒艺术学校2017文艺汇演——红领巾唱响中国</p><p>表演者：拉丁舞参赛4班</p><p></p><p><img src=\"http://source.5atl.com/attachment/upload/201705/31/14961950908624.jpg\"></p><p></p><p><img src=\"http://source.5atl.com/attachment/upload/201705/31/14961950932821.jpg\"></p><p></p><p><img src=\"http://source.5atl.com/attachment/upload/201705/31/14961950976258.jpg\"></p><p><img src=\"http://source.5atl.com/attachment/upload/201705/31/14961951066308.jpg\"></p> \n																			', '表演者：拉丁舞参赛4班', '', '', '0', 13319, 0, 0, 'admin', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1016, 2, 14, '傣族舞《雨林》', 'http://source.5atl.com/attachment/images/201705/31/1496195801.JPG', '<p>铜梁区万花筒艺术学校2017文艺汇演——傣族舞《雨林》</p><p>表演者：中国舞艺术团</p><p></p><p><img src=\"http://source.5atl.com/attachment/images/201705/31/1496195801.JPG\"></p><p></p><p><img src=\"http://source.5atl.com/attachment/upload/201705/31/14961958204514.jpg\"></p><p></p><p><img src=\"http://source.5atl.com/attachment/upload/201705/31/14961958247024.jpg\"></p><p><img src=\"http://source.5atl.com/attachment/upload/201705/31/1496195828212.jpg\"></p><div><br></div> \n													', '表演者：中国舞艺术团', '', '', '0', 151, 0, 0, 'admin', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1017, 3, 14, '拉丁舞《极限特工》', 'http://source.5atl.com/attachment/images/201705/31/1496196096.JPG', '<p>铜梁区万花筒艺术学校2017文艺汇演——拉丁舞《极限特工》</p><p>表演者：拉丁舞艺术团</p><p><img src=\"http://source.5atl.com/attachment/images/201705/31/1496196096.JPG\"></p><p><img src=\"http://source.5atl.com/attachment/upload/201705/31/14961961155343.jpg\"></p><p><img src=\"http://source.5atl.com/attachment/upload/201705/31/14961961187537.jpg\"></p><p><img src=\"http://source.5atl.com/attachment/upload/201705/31/14961961215458.jpg\"></p><p><br></p> \n													', '表演者：拉丁舞艺术团', '', '', '0', 626, 0, 0, 'admin', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1034, 2, 15, '姜胜艳', 'http://source.5atl.com/attachment/voteimg/201708/13/1502594420.jpg', '', '执子之手，与子偕老', 'http://source.5atl.com/attachment/voteimg/201708/13/1502594420_bh.jpg', '', '17783994341', 977, 0, 0, 'o73cGwftOrO7wsJbU29Kt-qeP88k', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1018, 4, 14, '苗族舞咿呀咿呀小嗲嗲', 'http://source.5atl.com/attachment/images/201705/31/1496196490.JPG', '<p>铜梁区万花筒艺术学校2017文艺汇演——苗族舞《咿呀咿呀 小嗲嗲》</p><p>表演者：中国舞二级班</p><p><img src=\"http://source.5atl.com/attachment/images/201705/31/1496196490.JPG\"></p><p><img src=\"http://source.5atl.com/attachment/upload/201705/31/14961965017486.jpg\"></p><p><img src=\"http://source.5atl.com/attachment/upload/201705/31/14961965046983.jpg\"></p><p><img src=\"http://source.5atl.com/attachment/upload/201705/31/14961965093975.jpg\"></p><p><br></p> \n													', '表演者：中国舞二级班', '', '', '0', 7617, 0, 0, 'admin', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1019, 5, 14, '街舞爵士舞串烧《功夫》', 'http://source.5atl.com/attachment/images/201705/31/1496196809.JPG', '<p>铜梁区万花筒艺术学校2017文艺汇演——街舞爵士舞串烧《功夫》</p><p>表演者：街舞 爵士舞班</p><p></p><p><img src=\"http://source.5atl.com/attachment/images/201705/31/1496196809.JPG\"></p><p></p><p><img src=\"http://source.5atl.com/attachment/upload/201705/31/14961968209238.jpg\"></p><p></p><p><img src=\"http://source.5atl.com/attachment/upload/201705/31/14961968232826.jpg\"></p><p><img src=\"http://source.5atl.com/attachment/upload/201705/31/14961968273244.jpg\"></p> \n													', '表演者：街舞 爵士舞班', '', '', '0', 11779, 0, 0, 'admin', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1020, 6, 14, '拉丁舞《热情时代》', 'http://source.5atl.com/attachment/images/201705/31/1496197225.JPG', '<p>铜梁区万花筒艺术学校2017文艺汇演——拉丁舞《热情时代》</p><p>表演者：拉丁舞高级1班</p><p></p><p><img src=\"http://source.5atl.com/attachment/images/201705/31/1496197225.JPG\"></p><p></p><p><img src=\"http://source.5atl.com/attachment/upload/201705/31/14961972567582.jpg\"></p><p></p><p><img src=\"http://source.5atl.com/attachment/upload/201705/31/14961972609402.jpg\"></p><p><img src=\"http://source.5atl.com/attachment/upload/201705/31/14961972633275.jpg\"></p> \n													', '表演者：拉丁舞高级1班', '', '', '0', 1473, 0, 0, 'admin', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1021, 7, 14, '舞蹈《舞蹈那些事儿》', 'http://source.5atl.com/attachment/images/201705/31/1496197801.JPG', '<p>铜梁区万花筒艺术学校2017文艺汇演——舞蹈《舞蹈那些事儿》</p><p>表演者：中国舞六级班 九级班</p><p></p><p><img src=\"http://source.5atl.com/attachment/images/201705/31/1496197801.JPG\"></p><p></p><p><img src=\"http://source.5atl.com/attachment/upload/201705/31/14961978141917.jpg\"></p><p></p><p><img src=\"http://source.5atl.com/attachment/upload/201705/31/14961978174389.jpg\"></p><p><img src=\"http://source.5atl.com/attachment/upload/201705/31/14961978211794.jpg\"></p> \n													', '表演者：中国舞六级班 九级班', '', '', '0', 4160, 0, 0, 'admin', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1022, 8, 14, '亲子秀《相亲相爱》', 'http://source.5atl.com/attachment/images/201705/31/1496198035.JPG', '<p>铜梁区万花筒艺术学校2017文艺汇演——亲子秀《相亲相爱》</p><p>表演者：中国舞一二级3班</p><p></p><p><img src=\"http://source.5atl.com/attachment/images/201705/31/1496198035.JPG\"></p><p></p><p><img src=\"http://source.5atl.com/attachment/upload/201705/31/14961980477188.jpg\"></p><p></p><p><img src=\"http://source.5atl.com/attachment/upload/201705/31/14961980505181.jpg\"></p><p><img src=\"http://source.5atl.com/attachment/upload/201705/31/14961980547892.jpg\"></p> \n													', '表演者：中国舞一二级3班', '', '', '0', 2883, 0, 0, 'admin', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1023, 9, 14, '舞蹈《可爱的舞娃》', 'http://source.5atl.com/attachment/images/201705/31/1496198390.JPG', '<p>铜梁区万花筒艺术学校2017文艺汇演——舞蹈《可爱的舞娃》</p><p>表演者：中国舞三级1班 四级班</p><p></p><p><img src=\"http://source.5atl.com/attachment/images/201705/31/1496198390.JPG\"></p><p></p><p><img src=\"http://source.5atl.com/attachment/upload/201705/31/14961984044606.jpg\"></p><p></p><p><img src=\"http://source.5atl.com/attachment/upload/201705/31/14961984072761.jpg\"></p><p><img src=\"http://source.5atl.com/attachment/upload/201705/31/14961984106916.jpg\"></p> \n													', '表演者：中国舞三级1班 四级班', '', '', '0', 10099, 0, 0, 'admin', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1024, 10, 14, '拉丁《狂舞纷飞》', 'http://source.5atl.com/attachment/images/201705/31/1496199022.JPG', '<p>铜梁区万花筒艺术学校2017文艺汇演——拉丁《狂舞纷飞》</p><p>表演者：拉丁舞参赛2班</p><p><img src=\"http://source.5atl.com/attachment/images/201705/31/1496199022.JPG\"></p><p><img src=\"http://source.5atl.com/attachment/upload/201705/31/14961990326571.jpg\"></p><p><img src=\"http://source.5atl.com/attachment/upload/201705/31/14961990364965.jpg\"></p><p><img src=\"http://source.5atl.com/attachment/upload/201705/31/14961990397245.jpg\"></p><p><br></p> \n													', '表演者：拉丁舞参赛2班', '', '', '0', 5642, 0, 0, 'admin', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1025, 11, 14, '维族女孩的幸福鼓', 'http://source.5atl.com/attachment/images/201705/31/1496199512.JPG', '<p>铜梁区万花筒艺术学校2017文艺汇演——舞蹈《维族女孩的幸福鼓》</p><p>表演者：中国舞五级班</p><p><img src=\"http://source.5atl.com/attachment/images/201705/31/1496199512.JPG\"></p><p><img src=\"http://source.5atl.com/attachment/upload/201705/31/14961995311791.jpg\"></p><p><img src=\"http://source.5atl.com/attachment/upload/201705/31/14961995349086.jpg\"></p><p><img src=\"http://source.5atl.com/attachment/upload/201705/31/14961995404055.jpg\"></p><p><img src=\"http://source.5atl.com/attachment/upload/201705/31/14961995433372.jpg\"></p><p><br></p> \n													', '表演者：中国舞五级班', '', '', '0', 157, 0, 0, 'admin', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1026, 12, 14, '成人拉丁舞《辣妈风采》', 'http://source.5atl.com/attachment/images/201705/31/1496199910.JPG', '<p>铜梁区万花筒艺术学校2017文艺汇演——成人拉丁舞《辣妈风采》</p><p>表演者：拉丁舞成人班</p><p></p><p><img src=\"http://source.5atl.com/attachment/images/201705/31/1496199910.JPG\"></p><p></p><p><img src=\"http://source.5atl.com/attachment/upload/201705/31/14961999228353.jpg\"></p><p></p><p><img src=\"http://source.5atl.com/attachment/upload/201705/31/14961999244492.jpg\"></p><p></p><p><img src=\"http://source.5atl.com/attachment/upload/201705/31/14961999279995.jpg\"></p><p><img src=\"http://source.5atl.com/attachment/upload/201705/31/14961999317131.jpg\"></p> \n													', '表演者：拉丁舞成人班', '', '', '0', 602, 0, 0, 'admin', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1033, 1, 15, '曹大爷和老何', 'http://source.5atl.com/attachment/voteimg/201708/12/1502506688.jpg', '', '时隔3年重温旧梦，14年摄于龙摄影。', 'http://source.5atl.com/attachment/voteimg/201708/12/1502506688_bh.jpg,/attachment/voteimg/201708/12/1502506688.jpg,http://source.5atl.com/attachment/voteimg/201708/12/1502506709.jpg', '', '13983972714', 1026, 0, 0, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1027, 13, 14, '舞蹈《剪窗花》', 'http://source.5atl.com/attachment/images/201705/31/1496200580.JPG', '<p>铜梁区万花筒艺术学校2017文艺汇演——舞蹈《剪窗花》</p><p>表演者：中国舞三级2班</p><p></p><p><img src=\"http://source.5atl.com/attachment/images/201705/31/1496200580.JPG\"></p><p></p><p><img src=\"http://source.5atl.com/attachment/upload/201705/31/14962005914689.jpg\"></p><p></p><p><img src=\"http://source.5atl.com/attachment/upload/201705/31/14962005945727.jpg\"></p><p></p><p><img src=\"http://source.5atl.com/attachment/upload/201705/31/14962005982386.jpg\"></p><p><img src=\"http://source.5atl.com/attachment/upload/201705/31/14962006012457.jpg\"></p><div><br></div> \n													', '表演者：中国舞三级2班', '', '', '0', 1664, 0, 0, 'admin', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1028, 14, 14, '钢琴独奏《星空》', 'http://source.5atl.com/attachment/images/201705/31/1496200833.JPG', '<p>铜梁区万花筒艺术学校2017文艺汇演——钢琴独奏《星空》</p><p>表演者：丁俊轩</p><p></p><p><strong><img src=\"http://source.5atl.com/attachment/images/201705/31/1496200833.JPG\"></strong><br></p><p><strong><img src=\"http://source.5atl.com/attachment/upload/201705/31/14962008742884.jpg\"></strong></p> \n													', '表演者：丁俊轩', '', '', '0', 22, 0, 0, 'admin', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1032, 20, 1, '呵呵呵', 'http://source.5atl.com/attachment/voteimg/201708/09/1502271107.jpg', '', '啦啦啦啦啦', 'http://source.5atl.com/attachment/voteimg/201708/09/1502271107_bh.jpg,/attachment/voteimg/201708/09/1502271107.jpg,http://source.5atl.com/attachment/voteimg/201708/09/1502271123.jpg', '', '13562315862', 23, 0, 0, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1029, 15, 14, '古典舞读唐诗的小姑娘', 'http://source.5atl.com/attachment/images/201705/31/1496201123.JPG', '<p>铜梁区万花筒艺术学校2017文艺汇演——古典舞《读唐诗的小姑娘》</p><p>表演者：中国舞艺术团</p><p></p><p><img src=\"http://source.5atl.com/attachment/images/201705/31/1496201123.JPG\"></p><p></p><p><img src=\"http://source.5atl.com/attachment/upload/201705/31/14962011362136.jpg\"></p><p></p><p><img src=\"http://source.5atl.com/attachment/upload/201705/31/14962011406112.jpg\"></p><p><img src=\"http://source.5atl.com/attachment/upload/201705/31/14962011458293.jpg\"></p> \n													', '表演者：中国舞艺术团', '', '', '0', 1052, 0, 0, 'admin', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1030, 16, 14, '拉丁舞《一起摇摆》', 'http://source.5atl.com/attachment/images/201705/31/1496201380.JPG', '<p>铜梁区万花筒艺术学校2017文艺汇演——拉丁舞《一起摇摆》</p><p>表演者：拉丁舞高级2班 尖子班</p><p></p><p><img src=\"http://source.5atl.com/attachment/images/201705/31/1496201380.JPG\"></p><p></p><p><img src=\"http://source.5atl.com/attachment/upload/201705/31/14962013921767.jpg\"></p><p></p><p><img src=\"http://source.5atl.com/attachment/upload/201705/31/14962013964118.jpg\"></p><p></p><p><img src=\"http://source.5atl.com/attachment/upload/201705/31/14962013997951.jpg\"></p><p><img src=\"http://source.5atl.com/attachment/upload/201705/31/1496201402374.jpg\"></p> \n													', '表演者：拉丁舞高级2班 尖子班', '', '', '0', 2014, 0, 0, 'admin', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1031, 19, 1, '我是宋祖儿', 'http://source.5atl.com/attachment/voteimg/201706/21/1498010484.jpg', '', '你还记得人见人爱的哪吒吗？', 'http://source.5atl.com/attachment/voteimg/201706/21/1498010484_bh.jpg,/attachment/voteimg/201706/21/1498010484.jpg,http://source.5atl.com/attachment/voteimg/201706/21/1498010499.jpg', '', '000000', 2, 0, 0, 'oYCDIt2DvUavx2N8nsGeyTB8sTc4', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1035, 3, 15, '周杨', 'http://source.5atl.com/attachment/voteimg/201708/13/1502595018.jpg', '', '带你吃遍全世界', 'http://source.5atl.com/attachment/voteimg/201708/13/1502595018_bh.jpg', '', '13883755155', 21, 0, 0, 'o73cGwWq84_1-wfJmFT6aLnNpgsk', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1041, 9, 15, '刘威', 'http://source.5atl.com/attachment/voteimg/201708/14/1502687682.jpg', '', '你若不离，我便不弃。', 'http://source.5atl.com/attachment/voteimg/201708/14/1502687682_bh.jpg,/attachment/voteimg/201708/14/1502687682.jpg,http://source.5atl.com/attachment/voteimg/201708/14/1502687705.jpg', '', '13983956518', 3198, 0, 0, 'o73cGwUeImWT_CY-bV0na3aiGoUo', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1037, 5, 15, '李太平', 'http://source.5atl.com/attachment/voteimg/201708/13/1502598748.jpg', '', '懂得是一种难言的柔情。', 'http://source.5atl.com/attachment/voteimg/201708/13/1502598748_bh.jpg', '', '15680915529', 15, 0, 0, 'o73cGwf4WAJUQbqyens4Ku3J3R4g', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1038, 6, 15, '周小宇', 'http://source.5atl.com/attachment/voteimg/201708/13/1502622931.jpg', '', '珍惜，包容。', 'http://source.5atl.com/attachment/voteimg/201708/13/1502622931_bh.jpg', '', '15223369420', 1847, 0, 0, 'o73cGwfpjYGJYZUZlVxJjREC_5XU', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1039, 7, 15, '大卫', 'http://source.5atl.com/attachment/voteimg/201708/13/1502626903.jpg', '', '将爱进行到底', 'http://source.5atl.com/attachment/voteimg/201708/13/1502626903_bh.jpg,/attachment/voteimg/201708/13/1502626903.jpg,http://source.5atl.com/attachment/voteimg/201708/13/1502626920.jpg,http://source.5atl.com/attachment/voteimg/201708/13/1502626944.jpg,http://source.5atl.com/attachment/voteimg/201708/13/1502626969.jpg,http://source.5atl.com/attachment/voteimg/201708/13/1502626993.jpg', '', '15334527889', 1, 0, 0, 'o73cGwfLI6MoNoReoBXxk92mkS_I', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1040, 8, 15, '陈清', 'http://source.5atl.com/attachment/voteimg/201708/14/1502641265.jpg', '', '认识我是你的福气ヾ(๑❛ ▿ ◠๑ )', 'http://source.5atl.com/attachment/voteimg/201708/14/1502641265_bh.jpg,/attachment/voteimg/201708/14/1502641265.jpg,http://source.5atl.com/attachment/voteimg/201708/14/1502641279.jpg', '', '13102308610', 14, 0, 0, 'o73cGwbxDctpDLKS1hyyzUNL_a2A', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1044, 10, 15, '林征', 'http://source.5atl.com/attachment/voteimg/201708/14/1502711730.jpg', '', '爱情需要坚持到底！', 'http://source.5atl.com/attachment/voteimg/201708/14/1502711730_bh.jpg,/attachment/voteimg/201708/14/1502711730.jpg,http://source.5atl.com/attachment/voteimg/201708/14/1502711737.jpg,http://source.5atl.com/attachment/voteimg/201708/14/1502711742.jpg', '', '18599927415', 23757, 0, 0, 'o73cGwflBGZyE_FYzV2Tn9lizSBM', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1045, 11, 15, '骆涵', 'http://source.5atl.com/attachment/voteimg/201708/14/1502716914.jpg', '', '将单车爱情进行到底。', 'http://source.5atl.com/attachment/voteimg/201708/14/1502716914_bh.jpg,/attachment/voteimg/201708/14/1502716914.jpg,http://source.5atl.com/attachment/voteimg/201708/14/1502716922.jpg', '', '15730139134', 2, 0, 0, 'o73cGwWlwzjHQPUOvmAwijiGyMnw', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1046, 12, 15, '骆涵', 'http://source.5atl.com/attachment/voteimg/201708/14/1502717455.jpg', '', '将单车爱情进行到底。', 'http://source.5atl.com/attachment/voteimg/201708/14/1502717455_bh.jpg', '', '15730139134', 3, 0, 0, 'o73cGwWlwzjHQPUOvmAwijiGyMnw', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1047, 13, 15, '曾佑梅', 'http://source.5atl.com/attachment/voteimg/201708/14/1502718016.jpg', '', '我爱你一生一世', 'http://source.5atl.com/attachment/voteimg/201708/14/1502718016_bh.jpg', '', '17782119315', 0, 0, 0, 'o73cGwdFco-J_1gPHhgaUE55E9ds', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1048, 14, 15, '胡婷', 'http://source.5atl.com/attachment/voteimg/201708/15/1502756039.jpg', '', '以后的路一起走 ”', 'http://source.5atl.com/attachment/voteimg/201708/15/1502756039_bh.jpg,/attachment/voteimg/201708/15/1502756039.jpg,http://source.5atl.com/attachment/voteimg/201708/15/1502756063.jpg', '', '15823991676', 983, 0, 0, 'o73cGwcnOSzn6C4YslRRG2_oFHls', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1049, 15, 15, '张策', 'http://source.5atl.com/attachment/voteimg/201708/15/1502760045.jpg', '', '忠贞不渝', 'http://source.5atl.com/attachment/voteimg/201708/15/1502760045_bh.jpg', '', '15712555638', 78, 0, 0, 'o73cGwY0T-68x_LigknfWX2HvTYE', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1050, 16, 15, '刘天星', 'http://source.5atl.com/attachment/voteimg/201708/15/1502760130.jpg', '', '梦回西安，唯你不忘', 'http://source.5atl.com/attachment/voteimg/201708/15/1502760130_bh.jpg', '', '18223311569', 0, 0, 0, 'o73cGwY0T-68x_LigknfWX2HvTYE', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1051, 17, 15, '余蕊', 'http://source.5atl.com/attachment/voteimg/201708/15/1502764747.jpg', '', '你是我最好的礼物', 'http://source.5atl.com/attachment/voteimg/201708/15/1502764747_bh.jpg', '', '15826018726', 2, 0, 0, 'o73cGwRxJnPXFe0ZJrsXdSg1w34Q', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1052, 18, 15, '王洁', 'http://source.5atl.com/attachment/voteimg/201708/15/1502768022.jpg', '', '理解与信任', 'http://source.5atl.com/attachment/voteimg/201708/15/1502768022_bh.jpg,/attachment/voteimg/201708/15/1502768022.jpg,http://source.5atl.com/attachment/voteimg/201708/15/1502768040.jpg,http://source.5atl.com/attachment/voteimg/201708/15/1502768061.jpg,http://source.5atl.com/attachment/voteimg/201708/15/1502768090.jpg', '', '13650527231', 462, 0, 0, 'o73cGwbo9z0QgYitAfqwaAcWPVuI', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1053, 19, 15, '秦童', 'http://source.5atl.com/attachment/voteimg/201708/15/1502768258.jpg', '', '爱一个人就要包容、尊重，相互理解，相互信任～', 'http://source.5atl.com/attachment/voteimg/201708/15/1502768258_bh.jpg,/attachment/voteimg/201708/15/1502768258.jpg,http://source.5atl.com/attachment/voteimg/201708/15/1502768283.jpg', '', '13220222609', 1616, 0, 0, 'o73cGwQtPerlJd34xTTKQnlHaceQ', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1055, 20, 15, '肖飞', 'http://source.5atl.com/attachment/voteimg/201708/15/1502774169.jpg', '', '你若不离，我便不弃变，你若要离，我还不弃，当爱情变', 'http://source.5atl.com/attachment/voteimg/201708/15/1502774169_bh.jpg,/attachment/voteimg/201708/15/1502774169.jpg,http://source.5atl.com/attachment/voteimg/201708/15/1502774183.jpg', '', '15803026202', 1, 0, 0, 'o73cGwbh2nVvY5kcTLsK_aEJtwi8', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1056, 21, 15, '范婉秋', 'http://source.5atl.com/attachment/voteimg/201708/15/1502782169.jpg', '', '执子之手与之偕老', 'http://source.5atl.com/attachment/voteimg/201708/15/1502782169_bh.jpg,/attachment/voteimg/201708/15/1502782169.jpg,http://source.5atl.com/attachment/voteimg/201708/15/1502782199.jpg', '', '18716450261', 2, 0, 0, 'o73cGwaJ4OAVm_Fe6fgE7UNJkakM', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1057, 22, 15, '王昌燕', 'http://source.5atl.com/attachment/voteimg/201708/15/1502782871.jpg', '', '爱是一辈子的陪伴', 'http://source.5atl.com/attachment/voteimg/201708/15/1502782871_bh.jpg,/attachment/voteimg/201708/15/1502782871.jpg,http://source.5atl.com/attachment/voteimg/201708/15/1502782882.jpg,http://source.5atl.com/attachment/voteimg/201708/15/1502782891.jpg,http://source.5atl.com/attachment/voteimg/201708/15/1502782899.jpg,http://source.5atl.com/attachment/voteimg/201708/15/1502782908.jpg', '', '18875136603', 94, 0, 0, 'o73cGwVI6o6PFSohyBwxJUd8jH7o', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1068, 32, 15, '陈然', 'http://source.5atl.com/attachment/voteimg/201708/15/1502795243.jpg', '', '你是我的优乐美！', 'http://source.5atl.com/attachment/voteimg/201708/15/1502795243_bh.jpg', '', '18996053693', 7, 0, 0, 'o73cGwXLfMcdCIdwOy4VKLCnxss4', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1069, 33, 15, '刘洋', 'http://source.5atl.com/attachment/voteimg/201708/15/1502800883.jpg', '', '婚姻需要相互理解和扶持，细水长流。', 'http://source.5atl.com/attachment/voteimg/201708/15/1502800883_bh.jpg', '', '18983695556', 737, 0, 0, 'o73cGwTTKhILq5pJ8VJ2bdeok0gY', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1059, 24, 15, '詹楠', 'http://source.5atl.com/attachment/voteimg/201708/15/1502794150.jpg', '', '最浪漫的事就是陪伴', 'http://source.5atl.com/attachment/voteimg/201708/15/1502794150_bh.jpg,/attachment/voteimg/201708/15/1502794150.jpg,http://source.5atl.com/attachment/voteimg/201708/15/1502794189.jpg', '', '13452001483', 153, 0, 0, 'o73cGwb7BD151e74d4bRMESnstzs', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1060, 25, 15, '汪道萍赵坤', 'http://source.5atl.com/attachment/voteimg/201708/15/1502794223.jpg', '', '我们很幸福', 'http://source.5atl.com/attachment/voteimg/201708/15/1502794223_bh.jpg', '', '15923192131', 6, 0, 0, 'o73cGwU6gD5TG8Wr7c6ID8CQtR3s', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1089, 50, 15, '洪仕玲', 'http://source.5atl.com/attachment/voteimg/201708/17/1502963412.jpg', '', '执子之手，与子偕老', 'http://source.5atl.com/attachment/voteimg/201708/17/1502963412_bh.jpg,/attachment/voteimg/201708/17/1502963412.jpg,http://source.5atl.com/attachment/voteimg/201708/17/1502963436.jpg,http://source.5atl.com/attachment/voteimg/201708/17/1502963474.jpg', '', '13983166673', 23, 0, 0, 'o73cGwXFbHvX_thKfpsjljqP7cK8', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1092, 53, 15, '陈雷', 'http://source.5atl.com/attachment/voteimg/201708/18/1503040023.jpg', '', '一起慢慢变老', 'http://source.5atl.com/attachment/voteimg/201708/18/1503040023_bh.jpg,/attachment/voteimg/201708/18/1503040023.jpg,http://source.5atl.com/attachment/voteimg/201708/18/1503040072.jpg', '', '18983909935', 417, 0, 0, 'o73cGwUEcv8PvaHlf1TYD6aWrObY', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1088, 49, 15, '曹婷', 'http://source.5atl.com/attachment/voteimg/201708/17/1502963136.jpg', '', '跟他在一起我最幸福了', 'http://source.5atl.com/attachment/voteimg/201708/17/1502963136_bh.jpg', '', '15736137173', 562, 0, 0, 'o73cGwZYgc0FWAY20q1Q2aBgMVlE', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1066, 31, 15, '詹楠', 'http://source.5atl.com/attachment/voteimg/201708/15/1502794385.jpg', '', '最浪漫的事就是陪你慢慢变老', 'http://source.5atl.com/1502794385_bh.jpg,http://source.5atl.com/attachment/voteimg/201708/15/1502794421.jpg', '', '13452001483', 0, 0, 0, 'o73cGwb7BD151e74d4bRMESnstzs', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1071, 34, 15, '张强❤️陈蕗尧', 'http://source.5atl.com/attachment/voteimg/201708/15/1502809541.jpg', '', '心有灵犀，一点就通。我～越来越喜欢你了', 'http://source.5atl.com/attachment/voteimg/201708/15/1502809541_bh.jpg,/attachment/voteimg/201708/15/1502809541.jpg,/attachment/voteimg/201708/15/1502809542.jpg,http://source.5atl.com/attachment/voteimg/201708/15/1502809555.jpg,http://source.5atl.com/attachment/voteimg/201708/15/1502809560.jpg', '', '17782299322', 13297, 0, 0, 'o73cGwYJg9fWXXCmUVjXOZWoAdz4', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1082, 45, 15, '罗露', 'http://source.5atl.com/attachment/voteimg/201708/16/1502875119.jpg', '', '打是亲骂是爱，一天不打不相爱', 'http://source.5atl.com/attachment/voteimg/201708/16/1502875119_bh.jpg', '', '18983901415', 131, 0, 0, 'o73cGwYFl1ViQksR8UufGeI49tP4', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1083, 46, 15, '陈波', 'http://source.5atl.com/attachment/voteimg/201708/16/1502877951.jpg', '', '今生共相伴', 'http://source.5atl.com/attachment/voteimg/201708/16/1502877951_bh.jpg,/attachment/voteimg/201708/16/1502877951.jpg,http://source.5atl.com/attachment/voteimg/201708/16/1502877966.jpg,http://source.5atl.com/attachment/voteimg/201708/16/1502877985.jpg,http://source.5atl.com/attachment/voteimg/201708/16/1502878008.jpg,http://source.5atl.com/attachment/voteimg/201708/16/1502878029.jpg', '', '15340586601', 3126, 0, 0, 'o73cGwVJtgxHCAuhoqvhGEvqyxlc', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1078, 41, 15, '刘星', 'http://source.5atl.com/attachment/upload/201708/16/15028662198066.jpg', '', '不求大富大贵，只想和你平平凡凡健健康康度过一生。', 'http://source.5atl.com/attachment/upload/201708/16/15028667274558.png,http://source.5atl.com/attachment/voteimg/201708/16/1502852758.jpg', '', '15812824736', 4, 0, 0, 'o73cGwZ1DPB3nqYkUo8wfWBTjjek', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1086, 47, 15, '小熊', 'http://source.5atl.com/attachment/voteimg/201708/17/1502962235.jpg', '', '执子之手与子皆老，今生能遇到你…真好！', 'http://source.5atl.com/attachment/voteimg/201708/17/1502962235_bh.jpg', '', '15808068377', 3, 0, 0, 'o73cGwSW9tATh3ki4KJgHruJbRPE', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1080, 43, 15, '段辉婷', 'http://source.5atl.com/attachment/voteimg/201708/16/1502855489.jpg', '', '一心一意是这世界上最温暖的力量！', 'http://source.5atl.com/attachment/voteimg/201708/16/1502855489_bh.jpg', '', '18580717128', 68, 0, 0, 'o73cGwXGO5if45VhA3rxo3VC6IVE', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1090, 51, 15, '康玉莲', 'http://source.5atl.com/attachment/voteimg/201708/17/1502974121.jpg', '', '在一起，不分离！', 'http://source.5atl.com/attachment/voteimg/201708/17/1502974121_bh.jpg', '', '13896073416', 74, 0, 0, 'o73cGweZjKhd0sZo64Y5RaWCr5Bk', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1091, 52, 15, '李勇', 'http://source.5atl.com/attachment/voteimg/201708/17/1502978854.jpg', '', '愿得一人心，白首不相离。', 'http://source.5atl.com/attachment/voteimg/201708/17/1502978854_bh.jpg', '', '180-8993-93', 5, 0, 0, 'o73cGwT2lSNOQL9JwN8KCdeJo3mg', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1093, 54, 15, '雷震', 'http://source.5atl.com/attachment/voteimg/201708/19/1503104578.jpg', '', '爱春哥，爱生活', 'http://source.5atl.com/attachment/voteimg/201708/19/1503104578_bh.jpg', '', '18716397457', 4, 0, 0, 'o73cGwYix2scTeUlfj9QP6K07NtA', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1094, 55, 15, '谢欢', 'http://source.5atl.com/attachment/voteimg/201708/19/1503106020.jpg', '', '春风十里，不及你。', 'http://source.5atl.com/attachment/voteimg/201708/19/1503106020_bh.jpg,/attachment/voteimg/201708/19/1503106020.jpg,http://source.5atl.com/attachment/voteimg/201708/19/1503106027.jpg,http://source.5atl.com/attachment/voteimg/201708/19/1503106033.jpg,http://source.5atl.com/attachment/voteimg/201708/19/1503106041.jpg', '', '18725964449', 20285, 0, 0, 'o73cGwe1z9Wfh1vdsTJEGb8dov_4', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1095, 56, 15, '黄珠香', 'http://source.5atl.com/attachment/voteimg/201708/19/1503126451.jpg', '', '虽然没有华丽的婚礼，没有富裕的生活，只要你对我好就', 'http://source.5atl.com/attachment/voteimg/201708/19/1503126451_bh.jpg', '', '15923054570', 4, 0, 0, 'o73cGwQU9hEVEmErCgpR7aFHIy1o', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1096, 57, 15, '陈&芯', 'http://source.5atl.com/attachment/voteimg/201708/19/1503139871.jpg', '', '一个不会游泳的我，却坠入了爱河', 'http://source.5atl.com/attachment/voteimg/201708/19/1503139871_bh.jpg,/attachment/voteimg/201708/19/1503139871.jpg,/attachment/voteimg/201708/19/1503139872.jpg,http://source.5atl.com/attachment/voteimg/201708/19/1503139986.jpg', '', '15310370553', 5, 0, 0, 'o73cGwdhvZClflO9douRaaoeHD6E', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1099, 59, 15, '王宇', 'http://source.5atl.com/attachment/voteimg/201708/23/1503445214.jpg', '', '不离不弃一辈子', 'http://source.5atl.com/attachment/voteimg/201708/23/1503445214_bh.jpg,/attachment/voteimg/201708/23/1503445214.jpg,http://source.5atl.com/attachment/voteimg/201708/23/1503445228.jpg', '', '18375841780', 972, 0, 0, 'o73cGwexCfuOgyp4yN3f3K9_5-iM', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1098, 58, 15, '刘芳', 'http://source.5atl.com/attachment/voteimg/201708/22/1503402095.jpg', '', '不离不弃', 'http://source.5atl.com/attachment/voteimg/201708/22/1503402095_bh.jpg,/attachment/voteimg/201708/22/1503402095.jpg,http://source.5atl.com/attachment/voteimg/201708/22/1503402107.jpg,http://source.5atl.com/attachment/voteimg/201708/22/1503402137.jpg', '', '18782572455', 6, 0, 0, 'o73cGwaKVuuKfAS8UtHSLuteYqoY', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1100, 60, 15, '林凤', 'http://source.5atl.com/attachment/voteimg/201708/23/1503470831.jpg', '', '爱☞永不放弃', 'http://source.5atl.com/attachment/voteimg/201708/23/1503470831_bh.jpg', '', '17783939212', 52, 0, 0, 'o73cGwT0nc7Kaos3N_gvtuEyTOPc', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1105, 62, 15, '练燕', 'http://source.5atl.com/attachment/voteimg/201708/23/1503478513.jpg', '', '平平淡淡才是福', 'http://source.5atl.com/attachment/voteimg/201708/23/1503478513_bh.jpg', '', '15123290325', 1326, 0, 0, 'o73cGwRr4quklEkd1fR3VcIZWqnw', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1106, 63, 15, '小雨中', 'http://source.5atl.com/attachment/voteimg/201708/23/1503479716.jpg', '', '珍惜我们的六年长跑！', 'http://source.5atl.com/attachment/voteimg/201708/23/1503479716_bh.jpg', '', '13983542615', 971, 0, 0, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1104, 61, 15, '唐思雨鲜奶配送', 'http://source.5atl.com/attachment/voteimg/201708/23/1503476585.jpg', '', '就像歌里唱的那样，我们一起变老', 'http://source.5atl.com/attachment/voteimg/201708/23/1503476585_bh.jpg,/attachment/voteimg/201708/23/1503476585.jpg,http://source.5atl.com/attachment/voteimg/201708/23/1503476591.jpg,http://source.5atl.com/attachment/voteimg/201708/23/1503476617.jpg', '', '13647663939', 93, 0, 0, 'o73cGwfX6N0lNrgkygDLQEyBNu6Y', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1107, 64, 15, '赵飞', 'http://source.5atl.com/attachment/voteimg/201708/23/1503480154.jpg', '', '遇见你，爱上你。', 'http://source.5atl.com/attachment/voteimg/201708/23/1503480154_bh.jpg', '', '18223311534', 962, 0, 0, 'o73cGwY0T-68x_LigknfWX2HvTYE', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1108, 65, 15, '陈晓', 'http://source.5atl.com/attachment/voteimg/201708/23/1503480206.jpg', '', '喜欢你的简单，愿我们白头到老！', 'http://source.5atl.com/attachment/voteimg/201708/23/1503480206_bh.jpg', '', '13635471175', 980, 0, 0, 'o73cGwY0T-68x_LigknfWX2HvTYE', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1111, 68, 15, '小米一枝花', 'http://source.5atl.com/attachment/voteimg/201708/23/1503480351.jpg', '', '我在兰花湖畔终于等到了陪我到老的你', 'http://source.5atl.com/attachment/voteimg/201708/23/1503480351_bh.jpg', '', '15365894562', 956, 0, 0, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1110, 67, 15, '沈力', 'http://source.5atl.com/attachment/voteimg/201708/23/1503480190.jpg', '', '感谢你陪我走过那么多个春夏秋冬，我爱你老婆！', 'http://source.5atl.com/attachment/voteimg/201708/23/1503480190_bh.jpg', '', '13596547852', 1008, 0, 0, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1112, 69, 15, '梁琪', 'http://source.5atl.com/attachment/voteimg/201708/23/1503480627.jpg', '', '尽管还不曾离开，我已对你朝思暮想!', 'http://source.5atl.com/attachment/voteimg/201708/23/1503480627_bh.jpg', '', '15823155641', 987, 0, 0, 'o73cGwY0T-68x_LigknfWX2HvTYE', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1113, 70, 15, '何长印', 'http://source.5atl.com/attachment/voteimg/201708/23/1503480678.jpg', '', '打结婚以来老公颜值简直就是逆袭呀', 'http://source.5atl.com/attachment/voteimg/201708/23/1503480678_bh.jpg', '', '13546854795', 1008, 0, 0, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1114, 71, 15, '何东', 'http://source.5atl.com/attachment/voteimg/201708/23/1503480690.jpg', '', '你的笑温暖我的心，你的美灿烂一个春，你的快乐是我心愿，你的幸福是我春天，拥有你我春心荡漾，离开你我心从春出！', 'http://source.5atl.com/attachment/voteimg/201708/23/1503480690_bh.jpg', '', '18225478534', 930, 0, 0, 'o73cGwY0T-68x_LigknfWX2HvTYE', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1115, 72, 15, '赵鹏', 'http://source.5atl.com/attachment/voteimg/201708/23/1503480914.jpg', '', '只要你一直在我身边，其他东西不再重要。', 'http://source.5atl.com/attachment/voteimg/201708/23/1503480914_bh.jpg', '', '13635410185', 961, 0, 0, 'o73cGwY0T-68x_LigknfWX2HvTYE', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1116, 73, 15, '陈佳', 'http://source.5atl.com/attachment/voteimg/201708/23/1503480883.jpg', '', '婚前小鲜肉已经变成矮肥圆，爱你的心也变大变园了', 'http://source.5atl.com/attachment/voteimg/201708/23/1503480883_bh.jpg', '', '15478963256', 976, 0, 0, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1117, 74, 15, '张路平', 'http://source.5atl.com/attachment/voteimg/201708/23/1503481021.jpg', '', '世界上最温暖的两个字是从你口中说出的晚安。', 'http://source.5atl.com/attachment/voteimg/201708/23/1503481021_bh.jpg', '', '18259871432', 994, 0, 0, 'o73cGwY0T-68x_LigknfWX2HvTYE', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1118, 75, 15, '谢小波', 'http://source.5atl.com/attachment/voteimg/201708/23/1503481136.jpg', '', '在认识你之后，我才发现自己可以这样情愿的付出。', 'http://source.5atl.com/attachment/voteimg/201708/23/1503481136_bh.jpg', '', '13996351438', 976, 0, 0, 'o73cGwY0T-68x_LigknfWX2HvTYE', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1119, 76, 15, '张春', 'http://source.5atl.com/attachment/voteimg/201708/23/1503481184.jpg', '', '爱上你，不是因为你给了我需要的东西，而是因为你给了我从未有过的感觉。', 'http://source.5atl.com/attachment/voteimg/201708/23/1503481184_bh.jpg', '', '13996542372', 974, 0, 0, 'o73cGwY0T-68x_LigknfWX2HvTYE', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1121, 78, 15, '陈峰', 'http://source.5atl.com/attachment/voteimg/201708/23/1503481224.jpg', '', '世上最浪漫和最自私的话就是：你是我一个人的。', 'http://source.5atl.com/attachment/voteimg/201708/23/1503481224_bh.jpg', '', '15223311531', 931, 0, 0, 'o73cGwY0T-68x_LigknfWX2HvTYE', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1122, 79, 15, '王明', 'http://source.5atl.com/attachment/voteimg/201708/23/1503481361.jpg', '', '我们彼此相爱着就是幸福，如此简单，如此难。', 'http://source.5atl.com/attachment/voteimg/201708/23/1503481361_bh.jpg', '', '15845312568', 966, 0, 0, 'o73cGwY0T-68x_LigknfWX2HvTYE', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1123, 80, 15, '高杰', 'http://source.5atl.com/attachment/voteimg/201708/23/1503481519.jpg', '', '如果有人问我为什么爱你，我觉得我只能如此回答：因为是你，因为是我。', 'http://source.5atl.com/attachment/voteimg/201708/23/1503481519_bh.jpg', '', '15936512753', 1006, 0, 0, 'o73cGwY0T-68x_LigknfWX2HvTYE', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1124, 81, 15, '罗非明', 'http://source.5atl.com/attachment/voteimg/201708/23/1503481775.jpg', '', '我很傻，我很笨，我有爱我的你。', 'http://source.5atl.com/attachment/voteimg/201708/23/1503481775_bh.jpg', '', '13883942166', 991, 0, 0, 'o73cGwY0T-68x_LigknfWX2HvTYE', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1125, 82, 15, '信兵哲', 'http://source.5atl.com/attachment/voteimg/201708/23/1503482314.jpg', '', '还好在错的时间遇到了对的你，周年快乐', 'http://source.5atl.com/attachment/voteimg/201708/23/1503482314_bh.jpg', '', '15247896512', 971, 0, 0, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1126, 83, 15, '流氓又', 'http://source.5atl.com/attachment/voteimg/201708/23/1503484147.jpg', '', '你在何处漂流 你在和谁厮守 我的天涯和梦要你挽救', 'http://source.5atl.com/attachment/voteimg/201708/23/1503484147_bh.jpg,/attachment/voteimg/201708/23/1503484147.jpg,http://source.5atl.com/attachment/voteimg/201708/23/1503484160.jpg', '', '18672351015', 945, 0, 0, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1127, 84, 15, '严天琴', 'http://source.5atl.com/attachment/voteimg/201708/23/1503491752.jpg', '', '最好的礼物是陪伴', 'http://source.5atl.com/attachment/voteimg/201708/23/1503491752_bh.jpg,/attachment/voteimg/201708/23/1503491752.jpg,http://source.5atl.com/attachment/voteimg/201708/23/1503491779.jpg', '', '13638332635', 706, 0, 0, 'o73cGwf8apXUtOihkgBK_S1iPucM', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1128, 85, 15, '何博', 'http://source.5atl.com/attachment/voteimg/201708/23/1503494704.jpg', '', '宠你，爱你！', 'http://source.5atl.com/attachment/voteimg/201708/23/1503494704_bh.jpg,/attachment/voteimg/201708/23/1503494704.jpg,http://source.5atl.com/attachment/voteimg/201708/23/1503494711.jpg,http://source.5atl.com/attachment/voteimg/201708/23/1503494720.jpg', '', '17623170518', 700, 0, 0, 'o73cGwbSpebwfvfTzXuABUS46cWM', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1129, 86, 15, '李纯燕', 'http://source.5atl.com/attachment/voteimg/201708/23/1503498905.jpg', '', '陪你痴狂千世，陪我万世轮回。', 'http://source.5atl.com/attachment/voteimg/201708/23/1503498905_bh.jpg,/attachment/voteimg/201708/23/1503498905.jpg,http://source.5atl.com/attachment/voteimg/201708/23/1503499056.jpg', '', '15803035052', 1976, 0, 0, 'o73cGwX7gROu8HPsZuagWwzeZqX0', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1137, 93, 15, '秦陈成', 'http://source.5atl.com/attachment/voteimg/201708/25/1503652767.jpg', '', '我中有你，你中有我。', 'http://source.5atl.com/attachment/voteimg/201708/25/1503652767_bh.jpg', '', '18502329268', 385, 0, 0, 'o73cGwfRzY6Ld8jOu0V9x9HwdfjA', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1131, 88, 15, '蒲易♡周江', 'http://source.5atl.com/attachment/voteimg/201708/23/1503499122.jpg', '', '7年没有痒组合', 'http://source.5atl.com/attachment/voteimg/201708/23/1503499122_bh.jpg,/attachment/voteimg/201708/23/1503499122.jpg,http://source.5atl.com/attachment/voteimg/201708/23/1503499186.jpg,http://source.5atl.com/attachment/voteimg/201708/23/1503499195.jpg,http://source.5atl.com/attachment/voteimg/201708/23/1503499204.jpg,http://source.5atl.com/attachment/voteimg/201708/23/1503499214.jpg', '', '17749932100', 468, 0, 0, 'o73cGwXmUHFwcD_19XtcAXVJt6pc', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1132, 89, 15, '夏鑫', 'http://source.5atl.com/attachment/voteimg/201708/24/1503545238.jpg', '', '醉过才知酒浓，爱过才知情重，有你宠溺，我很幸运。', 'http://source.5atl.com/attachment/voteimg/201708/24/1503545238_bh.jpg,/attachment/voteimg/201708/24/1503545238.jpg,http://source.5atl.com/attachment/voteimg/201708/24/1503545252.jpg,http://source.5atl.com/attachment/voteimg/201708/24/1503545268.jpg,http://source.5atl.com/attachment/voteimg/201708/24/1503545286.jpg,http://source.5atl.com/attachment/voteimg/201708/24/1503545329.jpg', '', '17782075682', 4291, 0, 0, 'o73cGwZjlZHEnHPpziQZ3K__lyNw', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1133, 90, 15, '王健', 'http://source.5atl.com/attachment/voteimg/201708/25/1503618246.jpg', '', '爱情甜蜜，婚姻更幸福！', 'http://source.5atl.com/attachment/voteimg/201708/25/1503618246_bh.jpg,/attachment/voteimg/201708/25/1503618246.jpg,http://source.5atl.com/attachment/voteimg/201708/25/1503618351.jpg', '', '18223528728', 529, 0, 0, 'o73cGwVwufYtfOEx-mjRhAr6oY8E', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1134, 91, 15, '杨欢', 'http://source.5atl.com/attachment/voteimg/201708/25/1503622085.jpg', '', '吵闹着才更幸福', 'http://source.5atl.com/attachment/voteimg/201708/25/1503622085_bh.jpg,/attachment/voteimg/201708/25/1503622085.jpg,http://source.5atl.com/attachment/voteimg/201708/25/1503622097.jpg,http://source.5atl.com/attachment/voteimg/201708/25/1503622126.jpg', '', '18502302198', 3, 0, 0, 'o73cGwVuAoF40Hk6stCS29XcYGNk', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1135, 92, 15, '周飞', 'http://source.5atl.com/attachment/voteimg/201708/25/1503638416.jpg', '', '一切尽在不言中(ﾟOﾟ)', 'http://source.5atl.com/attachment/voteimg/201708/25/1503638416_bh.jpg', '', '15736136105', 471, 0, 0, 'o73cGwTe4ZKgHe_2pjKtvknauNaM', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1138, 94, 15, '陈梅', 'http://source.5atl.com/attachment/voteimg/201708/26/1503727457.jpg', '', '  相爱一生', 'http://source.5atl.com/attachment/voteimg/201708/26/1503727457_bh.jpg', '', '18223131855', 402, 0, 0, 'o73cGwXxFcQcDh0yqbjVuQP3_5Hg', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1139, 95, 15, '李纯婷', 'http://source.5atl.com/attachment/voteimg/201708/26/1503753105.jpg', '', '我是豆浆，你是油条，简单爱...', 'http://source.5atl.com/attachment/voteimg/201708/26/1503753105_bh.jpg,/attachment/voteimg/201708/26/1503753105.jpg,http://source.5atl.com/attachment/voteimg/201708/26/1503753113.jpg,http://source.5atl.com/attachment/voteimg/201708/26/1503753122.jpg', '', '13635430176', 505, 0, 0, 'o73cGwVm-oH4XxTWiSTTIjHg-PsY', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1140, 96, 15, '邹利', 'http://source.5atl.com/attachment/voteimg/201708/27/1503791738.jpg', '', '因为爱所以爱', 'http://source.5atl.com/attachment/voteimg/201708/27/1503791738_bh.jpg', '', '15111841175', 399, 0, 0, 'o73cGwT00GaxAxMzZMg0mxkB_Rfw', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1141, 97, 15, '周玲', 'http://source.5atl.com/attachment/voteimg/201708/27/1503842191.jpg', '', '只需要一眼就知道你是我要的幸福', 'http://source.5atl.com/attachment/voteimg/201708/27/1503842191_bh.jpg', '', '15086781893', 194, 0, 0, 'o73cGwV0dK18-rZOOIZVmI_Nwurc', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1158, 6, 16, '魏丽丽', 'http://source.5atl.com/attachment/voteimg/201710/11/1507691740.jpg', '', '支持我，投我一票，么么哒', 'http://source.5atl.com/attachment/voteimg/201710/11/1507691740_bh.jpg', '', '17620122653', 4, 0, 0, 'ols_MwNg1j42b4WEmd_3nwDwj548', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1151, 1, 16, '邹迪', 'http://source.5atl.com/attachment/voteimg/201710/11/1507687346.jpg', '', '我在龙韵果乡当农场主，支持我一次爱you一辈子', 'http://source.5atl.com/attachment/voteimg/201710/11/1507687346_bh.jpg', '', '13452803380', 2266, 0, 0, 'ols_MwJyy3SasMp7PAikj-cgB3fk', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1154, 2, 16, '邱', 'http://source.5atl.com/attachment/voteimg/201710/11/1507690886.jpg', '', '12345', 'http://source.5atl.com/attachment/voteimg/201710/11/1507690886_bh.jpg', '', '15823903518', 10, 0, 0, 'ols_MwG5n5dG4fNI6zFchFQHFaOI', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1155, 3, 16, '王山', 'http://source.5atl.com/attachment/voteimg/201710/11/1507690903.jpg', '', '我要三根柚子树的柚子', 'http://source.5atl.com/attachment/voteimg/201710/11/1507690903_bh.jpg', '', '13883749181', 0, 0, 0, 'ols_MwAsi8shOyNohAoqTNrXWYmo', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1156, 4, 16, '雷波', 'http://source.5atl.com/attachment/voteimg/201710/11/1507691096.jpg', '', '我不要柚子。', 'http://source.5atl.com/attachment/voteimg/201710/11/1507691096_bh.jpg', '', '15923341748', 1, 0, 0, 'ols_MwAckrRSx-XR9P2NaK5OYPSE', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1157, 5, 16, '刘晋宁', 'http://source.5atl.com/attachment/voteimg/201710/11/1507691318.jpg', '', '浓情蜜意   l love 柚', 'http://source.5atl.com/attachment/voteimg/201710/11/1507691318_bh.jpg', '', '13996496015', 1372, 0, 0, 'ols_MwNbXfcdwGi2gUF0Fk5-955Q', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1159, 7, 16, '龙金山', 'http://source.5atl.com/attachment/voteimg/201710/11/1507691887.jpg', '', '柚你就有我。', 'http://source.5atl.com/attachment/voteimg/201710/11/1507691887_bh.jpg', '', '17338629018', 218, 0, 0, 'ols_MwEZ0yafVshEbRcy6s2x08O0', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1160, 8, 16, '刘洪', 'http://source.5atl.com/attachment/voteimg/201710/11/1507693119.jpg', '', '爱你爱你爱你', 'http://source.5atl.com/attachment/voteimg/201710/11/1507693119_bh.jpg', '', '18996069907', 1, 0, 0, 'ols_MwBwvvEwVMtdMvEZjj-EYuFw', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1161, 9, 16, '张远陈', 'http://source.5atl.com/attachment/voteimg/201710/11/1507694230.jpg', '', '与you一起过冬', 'http://source.5atl.com/attachment/voteimg/201710/11/1507694230_bh.jpg', '', '18875012685', 62, 0, 0, 'ols_MwN65TTvpHKaVDlPKDLbtoJc', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1162, 10, 16, '杨涛', 'http://source.5atl.com/attachment/voteimg/201710/11/1507695118.jpg', '', '谢谢一路柚你', 'http://source.5atl.com/attachment/voteimg/201710/11/1507695118_bh.jpg', '', '13953223536', 2767, 0, 0, 'ols_MwK4PW52klA5xsDJf-kMywk4', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1163, 11, 16, '跑者永恒', 'http://source.5atl.com/attachment/voteimg/201710/11/1507695126.jpg', '', '“柚”回故乡，“柚”你同行!', 'http://source.5atl.com/attachment/voteimg/201710/11/1507695126_bh.jpg', '', '15086619002', 3, 0, 0, 'ols_MwG350ZUNoAylrlIMyS39s8c', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1164, 12, 16, '彭明秀', 'http://source.5atl.com/attachment/voteimg/201710/11/1507696860.jpg', '', '因为“柚”你，所以甜蜜……', 'http://source.5atl.com/attachment/voteimg/201710/11/1507696860_bh.jpg', '', '13883661446', 1, 0, 0, 'ols_MwGi5YHbbKv2oVIhXVJotn8w', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1165, 13, 16, '朱莉', 'http://source.5atl.com/attachment/voteimg/201710/11/1507697566.jpg', '', '18523096328', 'http://source.5atl.com/attachment/voteimg/201710/11/1507697566_bh.jpg', '', '18523096328', 2, 0, 0, 'ols_MwPd-ICNRkL_sPnXd0UN4Q10', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1166, 14, 16, '田泾嘉', 'http://source.5atl.com/attachment/voteimg/201710/11/1507701560.jpg', '', '\"柚\"我陪你一起长大!', 'http://source.5atl.com/attachment/voteimg/201710/11/1507701560_bh.jpg', '', '18996066255', 501, 0, 0, 'ols_MwLfsVEEPNBwIOoInIq3O9Rs', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1167, 15, 16, '谷天欣', 'http://source.5atl.com/attachment/voteimg/201710/11/1507704950.jpg', '', '因为“柚”你，每天心情美美哒！', 'http://source.5atl.com/attachment/voteimg/201710/11/1507704950_bh.jpg,/attachment/voteimg/201710/11/1507704950.jpg,http://source.5atl.com/attachment/voteimg/201710/11/1507704961.jpg,http://source.5atl.com/attachment/voteimg/201710/11/1507704977.jpg', '', '18523037379', 3, 0, 0, 'ols_MwHqDnzwaq1HR1v3EuBClUFU', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1169, 16, 16, '苏闲云', 'http://source.5atl.com/attachment/voteimg/201710/15/1508077265.jpg', '', '投进第一名  柚子一人一个 ', 'http://source.5atl.com/attachment/voteimg/201710/15/1508077265_bh.jpg,/attachment/voteimg/201710/15/1508077265.jpg,http://source.5atl.com/attachment/voteimg/201710/15/1508077326.jpg', '', '18323156831', 711, 0, 0, 'ols_MwAEwBQiauCUV2yOjr857KmU', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1170, 17, 16, '蒙清连', 'http://source.5atl.com/attachment/voteimg/201710/20/1508488180.jpg', '', '有你很快乐', 'http://source.5atl.com/attachment/voteimg/201710/20/1508488180_bh.jpg,/attachment/voteimg/201710/20/1508488180.jpg,http://source.5atl.com/attachment/voteimg/201710/20/1508488218.jpg', '', '15310444186', 1, 0, 0, 'ols_MwIF29bo2HhthKZDm1RErdHE', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1171, 18, 16, '余诗蔓', 'http://source.5atl.com/attachment/voteimg/201710/21/1508547724.jpg', '', '我爱“you”,我爱吃“you”！！！', 'http://source.5atl.com/attachment/voteimg/201710/21/1508547724_bh.jpg', '', '18716409400', 31, 0, 0, 'ols_MwP-MCgi97kGYvMGv-Dr73j8', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1172, 19, 16, '苏闲云', 'http://source.5atl.com/attachment/voteimg/201710/21/1508594516.jpg', '', '柚你 柚我 柚家', 'http://source.5atl.com/attachment/voteimg/201710/21/1508594516_bh.jpg,/attachment/voteimg/201710/21/1508594516.jpg,http://source.5atl.com/attachment/voteimg/201710/21/1508594528.jpg', '', '18323156831', 1, 0, 0, 'ols_MwAEwBQiauCUV2yOjr857KmU', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1173, 20, 16, '刘洋', 'http://source.5atl.com/attachment/voteimg/201710/23/1508740861.jpg', '', '柚', 'http://source.5atl.com/attachment/voteimg/201710/23/1508740861_bh.jpg', '', '15213159590', 3, 0, 0, 'ols_MwLJ9RFYobj0HPqnvK57-BAM', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1174, 21, 16, '诗和远方', 'http://source.5atl.com/attachment/voteimg/201710/25/1508892554.jpg', '', '柚衷想你', 'http://source.5atl.com/attachment/voteimg/201710/25/1508892554_bh.jpg', '', '13594159573', 222, 0, 0, 'ols_MwEj6op5n9WcKtmQF9gIs3bM', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1175, 22, 16, '哈哈哈', 'http://source.5atl.com/attachment/voteimg/201710/26/1509004800.jpg', '', '好尴尬', 'http://source.5atl.com/attachment/voteimg/201710/26/1509004800_bh.jpg,/attachment/voteimg/201710/26/1509004800.jpg,http://source.5atl.com/attachment/voteimg/201710/26/1509004831.jpg,http://source.5atl.com/attachment/voteimg/201710/26/1509004839.jpg', '', '庄东林', 0, 0, 0, 'ols_MwMT95-rHswu3JG8h8CSdS88', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1176, 23, 16, '李垅基', 'http://source.5atl.com/attachment/voteimg/201710/26/1509026153.jpg', '', '希望白头偕老', 'http://source.5atl.com/attachment/voteimg/201710/26/1509026153_bh.jpg', '', '15129820219', 0, 0, 0, 'ols_MwG37Pjdzh0pqXiUNEDDwSaQ', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1177, 24, 16, '李垅基', '/attachment/voteimg/201710/26/1509026153.jpg', '', '希望白头偕老', '/attachment/voteimg/201710/26/1509026153.jpg', '', '15129820219', 0, 0, 0, 'ols_MwG37Pjdzh0pqXiUNEDDwSaQ', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1187, 10, 17, '李燕', 'http://source.5atl.com/attachment/images/20180910/1fd92580f68bde5d24008869873a7375.jpg', '', '我努力 我坚持 我一定能成功！', 'http://source.5atl.com/attachment/images/20180910/1928af386a30515274937434854a4cf1.jpg,/attachment/voteimg/201809/09/1536499290.jpg,/attachment/voteimg/201809/09/1536499290.jpg,http://source.5atl.com/attachment/voteimg/201809/09/1536499308.jpg,http://source.5atl.com/attachment/voteimg/201809/09/1536499319.jpg', '', '17783189907', 183, 0, 0, 'oUn5Ew3tU58ktAJzu4lHLI1KaeiE', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1179, 2, 17, '张秀兰', 'http://source.5atl.com/attachment/voteimg/201809/06/1536227651.jpg', '', '美丽，是一场长跑 它不属于某个年龄阶段； 而是，你', 'http://source.5atl.com/attachment/images/20180910/3a521e6f1071c44fca8ea2ba98ae1322.jpg', '', '15310231638', 6, 0, 0, 'oUn5Ew63iS6Fz388yUNbotF6Ien4', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1180, 3, 17, '陈圆圆', 'http://source.5atl.com/attachment/voteimg/201809/06/1536228531.jpg', '', '只有你想不到，没有你做不到的！', 'http://source.5atl.com/attachment/images/20180910/0f7a9b5696176082a06127171c71a8c5.jpg,/attachment/voteimg/201809/06/1536228531.jpg,/attachment/voteimg/201809/06/1536228531.jpg,http://source.5atl.com/attachment/voteimg/201809/06/1536228558.jpg', '', '13888710782', 200, 0, 0, 'oUn5Eww5Bjg1ygF41Cd9dL-F_CcE', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1182, 5, 17, '刘婷', 'http://source.5atl.com/attachment/voteimg/201809/09/1536477610.jpg', '', '把自己的生活工作当做一份爱好、每天重复的事情反复做', 'http://source.5atl.com/attachment/images/20180910/2dd7efa21414dc3dc5da0eeef87fb902.jpg,/attachment/voteimg/201809/09/1536477610.jpg,http://source.5atl.com/attachment/voteimg/201809/09/1536477622.jpg,http://source.5atl.com/attachment/voteimg/201809/09/1536477630.jpg,http://source.5atl.com/attachment/voteimg/201809/09/1536477649.jpg,http://source.5atl.com/attachment/voteimg/201809/09/1536477662.jpg', '', '18580265483', 12825, 0, 0, 'oUn5EwxAIEgAtDP_0Glm70YnEihw', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1185, 8, 17, '覃利华', 'http://source.5atl.com/attachment/voteimg/201809/09/1536489720.jpg', '', '今天没所“谓”，明天无所“为”', 'http://source.5atl.com/attachment/voteimg/201809/09/1536489720_bh.jpg,/attachment/voteimg/201809/09/1536489720.jpg,/attachment/voteimg/201809/09/1536489721.jpg,http://source.5atl.com/attachment/voteimg/201809/09/1536489732.jpg', '', '17782357002', 2, 0, 0, 'oUn5Ew6r9hAWUcWmD4mAqBPcqV84', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1186, 9, 17, '柏欢', 'http://source.5atl.com/attachment/voteimg/201809/09/1536491026.jpg', '', '哈哈您的评价是我前进最大的动力', 'http://source.5atl.com/attachment/voteimg/201809/09/1536491026_bh.jpg,/attachment/voteimg/201809/09/1536491026.jpg,http://source.5atl.com/attachment/voteimg/201809/09/1536491048.jpg,http://source.5atl.com/attachment/voteimg/201809/09/1536491080.jpg', '', '15111860381', 38, 0, 0, 'oUn5Ew9rLVx1Pe4_t9AgDA0ojvdE', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1184, 7, 17, '周婷婷', 'http://source.5atl.com/attachment/voteimg/201809/09/1536486643.jpg', '', '爱贝比爱生活 魅力老板娘应该拥有自信 自立 自强不', 'http://source.5atl.com/attachment/images/20180910/65a7637608e5f5735944825bca384b7c.jpg,/attachment/voteimg/201809/09/1536486643.jpg,http://source.5atl.com/attachment/voteimg/201809/09/1536486656.jpg,http://source.5atl.com/attachment/voteimg/201809/09/1536486667.jpg,http://source.5atl.com/attachment/voteimg/201809/09/1536486678.jpg', '', '13368287640', 61, 0, 0, 'oUn5Ew8ksnX_vQ-Oi5HR6AzBIs10', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1188, 11, 17, '裴若言', 'http://source.5atl.com/attachment/voteimg/201809/09/1536504525.jpg', '', '就是想变美啊~', 'http://source.5atl.com/attachment/images/20180910/7a1b7f9d911de0e1d879f81738d72b1b.jpg,/attachment/voteimg/201809/09/1536504525.jpg,http://source.5atl.com/attachment/voteimg/201809/09/1536504805.jpg,http://source.5atl.com/attachment/voteimg/201809/09/1536504806.jpg', '', '18983904121', 43, 0, 0, 'oUn5Ew2sLo6RofztVWLhvOlW5unI', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1189, 12, 17, '卢红梅', 'http://source.5atl.com/attachment/voteimg/201809/10/1536550724.jpg', '', '人生不长要活得漂亮！', 'http://source.5atl.com/attachment/images/20180910/5be262ff465776162db3ea4a82b5364e.jpg,/attachment/voteimg/201809/10/1536550724.jpg,http://source.5atl.com/attachment/voteimg/201809/10/1536550747.jpg', '', '18523415604', 19, 0, 0, 'oUn5EwzNaS6YxDvG484h64UpG1uw', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1190, 13, 17, '赵凌云', 'http://source.5atl.com/attachment/voteimg/201809/10/1536557574.jpg', '', '越努力越幸运', 'http://source.5atl.com/attachment/images/20180910/b2b0f4d1fd2133300bdb82bbbe18a137.jpg,/attachment/voteimg/201809/10/1536557574.jpg,/attachment/voteimg/201809/10/1536557575.jpg,/attachment/voteimg/201809/10/1536557576.jpg,/attachment/voteimg/201809/10/1536557575.jpg,/attachment/voteimg/201809/10/1536557576.jpg,/attachment/voteimg/201809/10/1536557576.jpg', '', '15111860003', 3, 0, 0, 'oUn5Ew990iYdflo7iYxIvOFOMNrI', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1191, 14, 17, '何叶', 'http://source.5atl.com/attachment/voteimg/201809/10/1536569013.jpg', '', '我是智慧与美貌并存的何叶，在铜梁营销策划找我哟', 'http://source.5atl.com/attachment/images/20180910/6e9caae7a6285ba08724bcfc119f151b.jpg\r\n', '', '18184064446', 1, 0, 0, 'oUn5Ew16NYjUWY-UqycDUeg5Jv8E', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1192, 15, 17, '彭霞', 'http://source.5atl.com/attachment/voteimg/201809/11/1536629025.jpg', '', '正因为我不甘平凡，所以我的青春我做主，我为自己代言', 'http://source.5atl.com/attachment/voteimg/201809/11/1536629025_bh.jpg,/attachment/voteimg/201809/11/1536629025.jpg,http://source.5atl.com/attachment/voteimg/201809/11/1536629031.jpg,http://source.5atl.com/attachment/voteimg/201809/11/1536629040.jpg,http://source.5atl.com/attachment/voteimg/201809/11/1536629048.jpg', '', '15213462922', 33, 0, 0, 'oUn5Ew3r_ErraAzyYCspJAA-aKPU', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1193, 16, 17, '龚红', 'http://source.5atl.com/attachment/voteimg/201809/11/1536644390.jpg', '', '我的小鱼馆，好吃不贵家里味', 'http://source.5atl.com/attachment/voteimg/201809/11/1536644390_bh.jpg', '', '15310195333', 3, 0, 0, 'oUn5Ew0JsoB4EFF2SYPyJmteH164', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1194, 17, 17, '傅月', 'http://source.5atl.com/attachment/voteimg/201809/14/1536893951.jpg', '', '容颜易老，唯有优雅的努力，才能光彩照人', 'http://source.5atl.com/attachment/voteimg/201809/14/1536893951_bh.jpg,/attachment/voteimg/201809/14/1536893951.jpg,http://source.5atl.com/attachment/voteimg/201809/14/1536893993.jpg', '', '13896195696', 1, 0, 0, 'oUn5Ew8KwOU8Bj8Qd1wv3bCF-z7w', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1195, 18, 17, '刘俊廷', 'http://source.5atl.com/attachment/voteimg/201809/14/1536920634.jpg', '', '追逐心之所往。', 'http://source.5atl.com/attachment/voteimg/201809/14/1536920634_bh.jpg,/attachment/voteimg/201809/14/1536920634.jpg,http://source.5atl.com/attachment/voteimg/201809/14/1536920659.jpg,http://source.5atl.com/attachment/voteimg/201809/14/1536920720.jpg', '', '15334526667', 14131, 0, 0, 'oUn5Ew8z11pCOEXWQOchy-2ka1iY', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1196, 19, 17, '龚娅', 'http://source.5atl.com/attachment/voteimg/201809/15/1536998259.jpg', '', '做您身边最贴心的管家', 'http://source.5atl.com/attachment/voteimg/201809/15/1536998259_bh.jpg,/attachment/voteimg/201809/15/1536998259.jpg,/attachment/voteimg/201809/15/1536998260.jpg,/attachment/voteimg/201809/15/1536998261.jpg', '', '17780479691', 3612, 0, 0, 'oUn5Ew8WyxceZxwB--kourtYUWOg', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1197, 20, 17, '姚茂欢', 'http://source.5atl.com/attachment/voteimg/201809/17/1537170006.jpg', '', '網癮少年', 'http://source.5atl.com/attachment/voteimg/201809/17/1537170006_bh.jpg,/attachment/voteimg/201809/17/1537170006.jpg,http://source.5atl.com/attachment/voteimg/201809/17/1537170023.jpg', '', '18848516207', 1, 0, 0, 'oUn5Ew2mau5ezCo4U5JPgHwLBnVY', 0, '', 0, 0, '', 0);
INSERT INTO `wx_voteimg_item` VALUES (1198, 21, 17, '邓小凤', 'http://source.5atl.com/attachment/voteimg/201809/17/1537184684.jpg', '', '越努力越幸运！做一个积极乐观的人。', 'http://source.5atl.com/attachment/voteimg/201809/17/1537184684_bh.jpg,/attachment/voteimg/201809/17/1537184684.jpg,http://source.5atl.com/attachment/voteimg/201809/17/1537184724.jpg,http://source.5atl.com/attachment/voteimg/201809/17/1537184769.jpg,http://source.5atl.com/attachment/voteimg/201809/17/1537184817.jpg', '', '15223464059', 1154, 0, 0, 'oUn5Ew9LtkJp_dERHI2gVN6aoM1E', 0, '', 0, 0, '', 0);

-- ----------------------------
-- Table structure for wx_voteimg_menus
-- ----------------------------
DROP TABLE IF EXISTS `wx_voteimg_menus`;
CREATE TABLE `wx_voteimg_menus`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vote_id` int(11) NOT NULL COMMENT '活动id',
  `token` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'token',
  `menu_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `menu_icon` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '菜单图标',
  `menu_link` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '菜单链接',
  `hide` tinyint(1) NOT NULL DEFAULT 1 COMMENT '是否隐藏',
  `type` tinyint(4) NOT NULL DEFAULT 1 COMMENT '是否是内置菜单',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `vote_id`(`vote_id`) USING BTREE,
  INDEX `menus_index`(`vote_id`, `token`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 56 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for wx_voteimg_prize
-- ----------------------------
DROP TABLE IF EXISTS `wx_voteimg_prize`;
CREATE TABLE `wx_voteimg_prize`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vote_id` int(11) NOT NULL,
  `prizegrade` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `prizename` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `prizesponsor` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `sponsorurl` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `prizepic` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `prizenum` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 66 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wx_voteimg_prize
-- ----------------------------
INSERT INTO `wx_voteimg_prize` VALUES (47, 1, '一等奖', '巴拉23', '铜梁视窗', '#', '#', 2);
INSERT INTO `wx_voteimg_prize` VALUES (48, 1, '二等奖', '巴拉拉2', '铜梁视窗', '#', '#', 2);
INSERT INTO `wx_voteimg_prize` VALUES (50, 1, '一等奖', '巴拉拉', '铜梁视窗', '#', '#', 1);
INSERT INTO `wx_voteimg_prize` VALUES (49, 1, '三等奖', '巴拉拉', '铜梁视窗', '#', '#', 1);
INSERT INTO `wx_voteimg_prize` VALUES (51, 1, '二等奖', '哈哈哈', '铜梁视窗', '#', '#', 2);
INSERT INTO `wx_voteimg_prize` VALUES (52, 1, '三等奖', '大大大', '铜梁视窗', '#', '#', 3);
INSERT INTO `wx_voteimg_prize` VALUES (53, 12, '前20名', '铜梁视窗微信推广1周', '铜梁视窗', 'http://', 'http://', 1);
INSERT INTO `wx_voteimg_prize` VALUES (54, 14, '一等奖', '128元拉杆箱', '', 'http://', 'http://wht.5atl.com/uploads/o/owqurf1449772009/d/e/0/9/thumb_592e7f3e833f8.jpg', 1);
INSERT INTO `wx_voteimg_prize` VALUES (55, 14, '二等奖', '88元护眼折叠台灯', '', 'http://', 'http://wht.5atl.com/uploads/o/owqurf1449772009/8/a/2/5/thumb_592e7f7d1863e.jpg', 2);
INSERT INTO `wx_voteimg_prize` VALUES (56, 14, '三等奖', '68元十万个为什么（套）', '', 'http://', 'http://wht.5atl.com/uploads/o/owqurf1449772009/6/d/0/d/thumb_592e7fc616660.jpg', 3);
INSERT INTO `wx_voteimg_prize` VALUES (57, 14, '四等奖', '38元儿童套碗', '', 'http://', 'http://wht.5atl.com/uploads/o/owqurf1449772009/9/d/e/9/thumb_592e800137ffe.jpg', 4);
INSERT INTO `wx_voteimg_prize` VALUES (58, 15, '一等奖', '1.现金奖3999元 \n2.幸福约定赞助拍摄微电影一套(价值1500元,拍摄恋爱记录，宝宝成长记录，全家幸福记录等) \n3.尚莲蝶姿整形美容提供自体脂肪填充代金券2000元+韩国小气泡1次580元+唇毛或腋毛包干1年598元 \n4.囍多多婚礼提供2999元婚庆现场布置套餐 \n5. 同心赋酒3瓶 \n6. 博洋家纺冬被一床', '', 'http://mp.weixin.qq.com/mp/homepage?__biz=MzA5MjI4Nzc5OQ==&hid=4&sn=213da1177fb27face3e7d51fead32afd#wechat_redirect', 'http://source.5atl.com/attachment/images/201708/23/1503487887.jpg', 1);
INSERT INTO `wx_voteimg_prize` VALUES (59, 15, '二等奖', '1.囍多多婚礼提供1000元婚纱照现金券 \n2.尚莲蝶姿整形美容提供自体脂肪填充代金券1000元+韩国小气泡1次580元+唇毛或腋毛包干1年598元 \n3.爱恋婚纱婚庆提供1000元婚庆现场布置套餐 \n4. 同心赋酒2瓶 \n5. U遇见婚庆提供价值388元真皮女包1个', '', 'http://mp.weixin.qq.com/mp/homepage?__biz=MzA5MjI4Nzc5OQ==&hid=4&sn=213da1177fb27face3e7d51fead32afd#wechat_redirect', 'http://source.5atl.com/attachment/images/201708/23/1503487901.jpg', 2);
INSERT INTO `wx_voteimg_prize` VALUES (60, 15, '三等奖', '1. 尚莲蝶姿整形美容提供自体脂肪填充代金券1000元+唇毛或腋毛包干1年598元\n2. 丽雅婚庆提供婚纱照现金500元现金券\n3.  同心赋酒1瓶', '', 'http://mp.weixin.qq.com/mp/homepage?__biz=MzA5MjI4Nzc5OQ==&hid=4&sn=213da1177fb27face3e7d51fead32afd#wechat_redirect', 'http://source.5atl.com/attachment/images/201708/23/1503487915.jpg', 3);
INSERT INTO `wx_voteimg_prize` VALUES (61, 15, '参与奖', '1. 价值500元的婚庆抵用券（婚博会现场婚庆企业都提供）;\n2. 维纳斯婚尚摄影提供价值100元的摄影抵用券\n3. 千尚提供化妆 美甲 美睫任选1样单次和VIP会员卡1张', '', 'http://mp.weixin.qq.com/mp/homepage?__biz=MzA5MjI4Nzc5OQ==&hid=4&sn=213da1177fb27face3e7d51fead32afd#wechat_redirect', 'http://source.5atl.com/attachment/images/201708/23/1503487943.png', 200);
INSERT INTO `wx_voteimg_prize` VALUES (62, 16, '一等奖', '黄心柚子树3棵1年收益权', '', 'http://', 'http://', 1);
INSERT INTO `wx_voteimg_prize` VALUES (63, 16, '二等奖', '黄心柚子树2棵1年收益权', '', 'http://', 'http://', 2);
INSERT INTO `wx_voteimg_prize` VALUES (64, 16, '三等奖', '黄心柚子树1棵1年收益权', '', 'http://', 'http://', 3);
INSERT INTO `wx_voteimg_prize` VALUES (65, 17, '奖品清单', '全部奖项', '尚莲蝶姿|铜梁视窗|维纳斯婚尚摄影', '#', 'http://source.5atl.com/attachment/images/20180908/55594d8762f6d19c63961960226bf04d.png', 1);

-- ----------------------------
-- Table structure for wx_voteimg_record
-- ----------------------------
DROP TABLE IF EXISTS `wx_voteimg_record`;
CREATE TABLE `wx_voteimg_record`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vote_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `vote_time` int(11) NOT NULL,
  `vote_day` date NOT NULL COMMENT '投票日',
  `vote_type` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `userID`(`user_id`) USING BTREE,
  INDEX `itemId`(`item_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for wx_voteimg_sponor
-- ----------------------------
DROP TABLE IF EXISTS `wx_voteimg_sponor`;
CREATE TABLE `wx_voteimg_sponor`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vote_id` int(11) NOT NULL,
  `sponsorname` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `sponsorurl` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `sponsorpic` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 19 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wx_voteimg_sponor
-- ----------------------------
INSERT INTO `wx_voteimg_sponor` VALUES (12, 0, '铜梁视窗', 'http://www.5atl.com', '1');
INSERT INTO `wx_voteimg_sponor` VALUES (13, 0, 'adfadf', 'addsf', 'asdfasddf');
INSERT INTO `wx_voteimg_sponor` VALUES (14, 1, '一等奖', '#', 'https://img.alicdn.com/simba/img/TB19te0PVXXXXayXXXXSutbFXXX.jpg');
INSERT INTO `wx_voteimg_sponor` VALUES (15, 14, '活动赞助商', '', 'http://source.5atl.com/attachment/upload/201705/31/14962122161672.jpg');
INSERT INTO `wx_voteimg_sponor` VALUES (16, 15, '活动赞助', '#', 'http://source.5atl.com/attachment/images/201708/29/1503965340.png');
INSERT INTO `wx_voteimg_sponor` VALUES (17, 16, '龙韵果乡众筹啦', 'https://z.jd.com/project/details/90544.html', 'http://img30.360buyimg.com/cf/jfs/t8263/287/2474590858/159858/7e1cb69a/59cf14c9N7b12cbcc.jpg');
INSERT INTO `wx_voteimg_sponor` VALUES (18, 17, '活动单位合集', '#', 'http://source.5atl.com/attachment/images/20180903/9c084d4936512cddfa31de2ac96b8a73.jpg');

-- ----------------------------
-- Table structure for wx_voteimg_users
-- ----------------------------
DROP TABLE IF EXISTS `wx_voteimg_users`;
CREATE TABLE `wx_voteimg_users`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vote_id` int(11) NOT NULL COMMENT '活动id',
  `wecha_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'openid',
  `wecha_pic` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `nick_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '微信昵称',
  `phone` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `client_ip` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `vote_id`(`vote_id`) USING BTREE,
  INDEX `IDX_WE_TO_VO`(`wecha_id`, `vote_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;