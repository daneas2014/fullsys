<?php
namespace addons\maps\controller;

use think\addons\Controller;
use think\Db;

class Index extends Controller {
	public function index() {}

	/**
	 * 打开地址页面，其中要包含地址详细信息、图库、论坛交流
	 *
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2022-06-01
	 */
	public function show() {
		$id = input('param.id');

		$data = Db::name('vendor_map')->find(['id' => $id]);

		$this->assign('data', $data);

		return $this->fetch();
	}

	public function map() {
		$db = Db::name('vendor_map');

		$list = $db->field('id,location,name,province,city,street,address,points,thumbnail,description,mobel,tel')->order('id desc')->select();

		$district = $db->field('center,street')->distinct(true)->select();
		$districtstr = '';
		foreach ($district as $d) {
			$districtstr .= '"' . $d['street'] . '":{"center":"' . $d['center'] . '"},';
		}

		$points = "";
		foreach ($list as $l) {
			$points .= "{ 'lnglat': [" . $l['location'] . "], 'city':'" . $l['province'] . "', 'district':'" . $l['city'] . "', 'area': '" . $l['street'] . "' },";
		}

		$assign = [
			'map' => $list,
			'points' => $points,
			'district' => "{" . $districtstr . "}",
		];

		$this->assign($assign);

		return $this->fetch();
	}
}