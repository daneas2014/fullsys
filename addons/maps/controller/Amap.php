<?php

namespace addons\maps\controller;

class Amap {

	private $key = 'f60573c08411405eeed753bcf88a5cb7';

    /**
     * 通过地图定位返回 josn位置
     *
     * @param [type] $location
     * @return void
     * @author dmakecn@163.com
     * @since 2022-06-22
     */
	function getPointAddress($location) {

		/**
		 * url:https://restapi.amap.com/v3/geocode/geo?address=北京市朝阳区阜通东大街6号&output=XML&key=<用户的key>
		 * output（XML/JSON）用于指定返回数据的格式
		 */
		$url = "https://restapi.amap.com/v3/geocode/regeo?output=JSON&location={$location}&key={$this->key}&radius=1000&extensions=base";

        
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_URL, $url);
		$data = curl_exec($ch);
		curl_close($ch);
		$result = json_decode($data, true); //本来返回就是json
		echo $data;
	}

    /**
     * 获得地址的坐标
     *
     * @param [type] $address
     * @param [type] $city
     * @return void
     * @author dmakecn@163.com
     * @since 2022-06-22
     */
	function getAddressLocation($address, $city) {

		$url = "https://restapi.amap.com/v3/geocode/geo?output=JSON&key={$this->key}&address={$address}&city={$city}";
		if ($city == '') {
			$url = "https://restapi.amap.com/v3/geocode/geo?output=JSON&key={$this->key}&address={$address}";
		}
        
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_URL, $url);
		$data = curl_exec($ch);
		curl_close($ch);
		return json_decode($data, true);
	}
}