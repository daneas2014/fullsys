<?php
namespace addons\maps\controller;
use addons\Base;
use think\Db;

class Admin extends Base {

	/**
	 * 所有地点
	 *
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2022-05-30
	 */
	public function index() {
		$db = Db::name('vendor_map');

		$key = input('param.key');
		$map = [];

		if ($key) {
			$map['name|address|points'] = ['like', "%$key%"];
		}

		return $this->vueQuerySingle($db, $map);
	}

	/**
	 * 保存地点
	 *
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2022-05-30
	 */
	public function save() {

		$db = Db::name('vendor_map');

		$data = input('param.');

		//需要web服务的权限
		if (request()->isPost()) {
			$am=new Amap();
			$center=$am->getAddressLocation($data['street'],$data['city']);
			
			if($center['info']='OK'){
				$data['center']=$center['geocodes'][0]['location'];			
			}
		}

		return $this->singleDataSave($db, $data);
	}

	/**
	 * 某地的图片
	 *
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2022-05-30
	 */
	public function images() {
		$db = Db::name('vendor_images');
		return $this->vueQuerySingle($db);
	}

	/**
	 * 保存图片
	 *
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2022-05-30
	 */
	public function saveimages() {
		$db = Db::name('vendor_images');
		return $this->singleDataSave($db, input('param.'));
	}

}