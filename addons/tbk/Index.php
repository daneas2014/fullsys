<?php
namespace addons\tbk; // 注意命名空间规范

use think\Addons;
use think\Db;

/**
 * 淘宝客插件
 * @author byron sampson
 */
class Index extends Addons// 需继承think\addons\Addons类

{
	// 该插件的基础信息
	public $info = [
		'name' => 'tbk', // 插件标识
		'title' => '免费淘宝客网站插件', // 插件名称
		'description' => '免费淘宝客网站插件，可集成于任意Thinkphp系统，无需人工干预即可自动采集，良好的SEO基础有助于做行业、地区淘宝客分销，有微信小程序版本。', // 插件简介
		'status' => 0, // 状态
		'author' => 'daneas',
		'version' => 'v2',
        'manage'=> 'tbk://admin/items'
	];

	/**
	 * 插件安装方法
	 * @return bool
	 */
	public function install() {
		$res = Db::execute(initAddon($this->info));

		if (!$res) {
			return false;
		}

		$sql = file_get_contents(__DIR__ . "/data.sql");

		$a = explode(";\r\n", $sql); //根据";\r\n"条件对数据库中分条执行
		$total = count($a);
		for ($i = 0; $i < $total; $i++) {
			$res = Db::query($a[$i]);
		}

		return $res > 0 ? true : false;
	}

	/**
	 * 插件卸载方法
	 * @return bool
	 */
	public function uninstall() {
		$sql = "DROP TABLE IF EXISTS tb_item;
        DROP TABLE IF EXISTS tb_nav;
        DROP TABLE IF EXISTS tk_urls;
        delete from addons where name='tbk';";

		$a = explode(";\r\n", $sql); //根据";\r\n"条件对数据库中分条执行
		$total = count($a);
		for ($i = 0; $i < $total; $i++) {
			$res = Db::query($a[$i]);
		}
		return $res > 0 ? true : false;
	}

	/**
	 * 实现的testhook钩子方法
	 * @return mixed
	 */
	public function tbkhook($param) {
		// 调用钩子时候的参数信息
		print_r($param);
		// 当前插件的配置信息，配置信息存在当前目录的config.php文件中，见下方
		print_r($this->getConfig());
		// 可以返回模板，模板文件默认读取的为插件目录中的文件。模板名不能为空！
		return $this->fetch('info');
	}

}
