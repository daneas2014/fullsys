<?php

/**
 * 正确的对象
 */
function ckr($obj,$bak)
{
    if (!isset($obj)) {
        return $bak;
    }

    return $obj ? $obj : $bak;
}

/**
 * 检查对象，提取第N个，否则替换
 *
 * @param [type] $obj
 * @param [type] $index
 * @param [type] $bak
 * @return void
 * @author dmakecn@163.com
 * @since 2022-04-26
 */
function cko($obj, $index, $bak)
{

    if (!isset($obj[$index])) {
        return $obj[$bak];
    }

    return $obj[$index] ? $obj[$index] : $obj[$bak];
}