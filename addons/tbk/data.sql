-- ----------------------------
-- 商品基础资料
-- ----------------------------
DROP TABLE IF EXISTS `tb_item`;
CREATE TABLE `tb_item`  (
`id` bigint(20) NOT NULL AUTO_INCREMENT,
`num_iid` bigint(20) NOT NULL,
`goodstype` int(11) NOT NULL COMMENT '0是券，1是淘抢购，2是普通',
`create_time` int(11) NOT NULL,
`category` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
`category_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
`user_type` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
`volume` int(11) NOT NULL,
`item_url` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
`pict_url` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
`pic_url` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
`reserve_price` double NOT NULL,
`zk_final_price` double NOT NULL,
`click_url` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
`small_images` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
`title` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
`short_title` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
`shop_title` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
`coupon_share_url` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
`commission_rate` double NOT NULL,
`coupon_end_time` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
`coupon_start_time` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
`coupon_remain_count` int(11) NOT NULL,
`coupon_total_count` int(11) NOT NULL,
`coupon_info` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
`coupon_amount` double NOT NULL DEFAULT 0 COMMENT '券价格',
`coupon_start_fee` double NOT NULL,
`item_description` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
`nick` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
`total_amount` int(11) NOT NULL,
`sold_num` int(11) NOT NULL,
`start_time` datetime(0) NOT NULL,
`end_time` datetime(0) NOT NULL,
`seller_id` int(11) NOT NULL,
`nickbreak` int(11) NOT NULL,
`provcity` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
`category_id` int(11) NOT NULL,
`url` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
`commission_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
`coupon_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
`include_dxjh` tinyint(1) NOT NULL,
`include_mkt` tinyint(1) NOT NULL,
`info_dxjh` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
`item_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
`level_one_category_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
`level_one_category_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
`real_post_fee` double NOT NULL,
`shop_dsr` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
`tk_total_commi` double NOT NULL,
`tk_total_sales` double NOT NULL,
`white_image` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
`x_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
`update_time` int(11) NOT NULL,
PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- 商品的推广链接
-- ----------------------------
DROP TABLE IF EXISTS `tk_urls`;
CREATE TABLE `tk_urls`  (
`id` int(11) NOT NULL AUTO_INCREMENT,
`num_iid` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
`item_url` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
`click_url` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
`coupon_url` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
`create_time` int(11) NOT NULL,
PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 31 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;


-- ----------------------------
-- 小程序导航条
-- ----------------------------
DROP TABLE IF EXISTS `tb_nav`;
CREATE TABLE `tb_nav`  (
`id` int(11) NOT NULL AUTO_INCREMENT,
`position` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
`status` int(11) NOT NULL DEFAULT 1,
`url` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
`appid` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
`iconpath` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
`name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
`description` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
`sort` int(11) NOT NULL,
`adminid` int(11) NOT NULL,
`create_time` int(11) NOT NULL,
`update_time` int(11) NOT NULL,
PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 18 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- 小程序导航明细
-- ----------------------------
INSERT INTO `tb_nav` (`id`, `position`, `status`, `url`, `appid`, `iconpath`, `name`, `description`, `sort`, `adminid`, `create_time`, `update_time`) VALUES
(1, 'index', 1, 'goods?id=17', '', 'https://www.dmake.cn/static/min/tk/mac.png', '数码', '', 0, 1, 0, 1576205117),
(2, 'index', 1, 'goods?id=10', '', 'https://www.dmake.cn/static/min/tk/nav_7.png', '美食', '', 0, 1, 0, 0),
(3, 'index', 1, 'goods?id=1', '', 'https://www.dmake.cn/static/min/tk/nav_1.png', '女装', '', 0, 1, 0, 0),
(4, 'index', 1, 'goods?id=2', '', 'https://www.dmake.cn/static/min/tk/t.png', '男装', '', 0, 1, 0, 0),
(5, 'index', 1, 'goods?id=3', '', 'https://www.dmake.cn/static/min/tk/nav_3.png', '童装', '', 0, 1, 0, 0),
(6, 'index', 1, 'goods?id=4', '', 'https://www.dmake.cn/static/min/tk/nav_4.png', '内衣', '', 0, 1, 0, 0),
(7, 'index', 1, 'goods?id=18', '', 'https://www.dmake.cn/static/min/tk/nav_5.png', '箱包', '', 0, 1, 0, 0),
(8, 'index', 1, 'goods?id=8', '', 'https://www.dmake.cn/static/min/tk/nav_6.png', '母婴', '', 0, 1, 0, 0),
(9, 'index', 1, 'goods?id=13', '', 'https://www.dmake.cn/static/min/tk/nav_8.png', '百货', '', 0, 1, 0, 0),
(10, 'index', 1, 'goods?id=19', '', 'https://www.dmake.cn/static/min/tk/nav_10.png', '户外', '', 0, 1, 0, 0),
(11, 'apps', 1, '/pages/tk/index', '', 'https://www.dmake.cn/static/min/img/calculator.png', '找同款就要折上折', '用于获得最新的购物优惠券和折扣活动提醒！', 0, 1, 1575618844, 1620719990),
(12, 'apps', 1, '/pages/apps/huangli', '', 'https://www.dmake.cn/static/min/img/syncadapters.png', '程序员老黄历', '程序员每日娱乐起点，先观天象后不加班！', 0, 1, 1575618897, 0),
(13, 'apps', 0, '/pages/video/list', '', 'http://cdn.dmake.cn/attachment/images/20191212/15761136570.png', '列表视频', '单个视频清单', 0, 1, 1576070683, 1576214662),
(14, 'apps', 0, '/pages/seriesvideo/list', '', 'http://cdn.dmake.cn/attachment/images/20191212/15761136335.png', '系列视频', '个人视频集锦', 0, 1, 1576070808, 1576214611),
(15, 'indexsub', 0, 'sales', '', 'http://cdn.dmake.cn/attachment/images/20191213/15762038842.png', '淘抢购', '', 0, 1, 1576162537, 1620720740),
(16, 'indexsub', 0, 'news', '', 'http://cdn.dmake.cn/attachment/images/20191213/15762039147.png', '套内容', '', 0, 1, 1576162574, 1620720728),
(17, 'apps', 1, '/pages/map/findme', '', 'https://www.dmake.cn/static/min/img/apps_maps.png', '找奇葩', '看看你身边的都是些什么人啊？', 1, 1, 1586420433, 1586422212);