<?php
namespace addons\tbk\controller;

use think\Db;
use think\Loader;
use addons\Base;

/**
 * 管理淘宝客的东西？？？
 * 不对呀，2020年已经实现了自动采集了呀，考虑考虑情况
 *
 * @author dmakecn@163.com
 * @since 2022-04-28
 */
class Admin extends Base
{

    public function index(){

        return $this->fetch();
    }

    /**
     * 入库商品的条数
     *
     * @return void
     * @author dmakecn@163.com
     * @since 2022-04-28
     */
    public function items()
    {
        $db = Db::name('tb_item');
        
        $key = input('key');
        $map = [];
        if ($key && $key !== "") {
            $map['title'] = [
                'like',
                "%" . $key . "%"
            ];
        }
        $page = input('get.page') ? input('get.page') : 1;
        $rows = input('get.rows'); // 获取总条数
        $count = $db->where($map)->count(); // 计算总页面
        
        $lists = $db->where($map)
            ->page($page, $rows)
            ->order('id desc')
            ->select();
        
        $data['list'] = $lists;
        $data['count'] = $count;
        $data['page'] = $page;
        if (input('get.page')) {
            return json($data);
        }
        return $this->fetch();
    }

    /**
     * 查询优惠券
     *
     * @return void
     * @author dmakecn@163.com
     * @since 2022-04-28
     */
    public function quan()
    {
        $db = Db::name('tb_item');
    
        $key = input('key');
        $map['goodstype'] = 0;
        if ($key && $key !== "") {
            $map['title'] = [
                'like',
                "%" . $key . "%"
            ];
        }
        $page = input('get.page') ? input('get.page') : 1;
        $rows = input('get.rows'); // 获取总条数
        $count = $db->where($map)->count(); // 计算总页面
    
        $lists = $db->where($map)
        ->page($page, $rows)
        ->order('id desc')
        ->select();
    
        $data['list'] = $lists;
        $data['count'] = $count;
        $data['page'] = $page;
        if (input('get.page')) {
            return json($data);
        }
        return $this->fetch();
    }
    

    /**
     * 查询淘抢购
     *
     * @return void
     * @author dmakecn@163.com
     * @since 2022-04-28
     */
    public function qianggou()
    {
        $db = Db::name('tb_item');
    
        $key = input('key');
        $map['goodstype'] = 1;
        if ($key && $key !== "") {
            $map['title'] = [
                'like',
                "%" . $key . "%"
            ];
        }
        $page = input('get.page') ? input('get.page') : 1;
        $rows = input('get.rows'); // 获取总条数
        $count = $db->where($map)->count(); // 计算总页面
    
        $lists = $db->where($map)
        ->page($page, $rows)
        ->order('id desc')
        ->select();
    
        $data['list'] = $lists;
        $data['count'] = $count;
        $data['page'] = $page;
        if (input('get.page')) {
            return json($data);
        }
        return $this->fetch();
    }


    /**
     * 查询限时优惠
     *
     * @return void
     * @author dmakecn@163.com
     * @since 2022-04-28
     */
    public function goods()
    {
        $db = Db::name('tb_item');
    
        $key = input('key');
        $map['goodstype'] = 2;
        if ($key && $key !== "") {
            $map['title'] = [
                'like',
                "%" . $key . "%"
            ];
        }
        $page = input('get.page') ? input('get.page') : 1;
        $rows = input('get.rows'); // 获取总条数
        $count = $db->where($map)->count(); // 计算总页面
    
        $lists = $db->where($map)
        ->page($page, $rows)
        ->order('id desc')
        ->select();
    
        $data['list'] = $lists;
        $data['count'] = $count;
        $data['page'] = $page;
        if (input('get.page')) {
            return json($data);
        }
        return $this->fetch();
    }
    
    /**
     * 编辑商品，觉着没得用
     *
     * @return void
     * @author dmakecn@163.com
     * @since 2022-04-28
     */
    public function editGood(){
        if(request()->isPost())
        {
            $data=input('post.');
            
            if(Db::name('tb_item')->where('id',$data['id'])->update())
            {
                return json(['code'=>1,'data'=>null,'msg'=>'保存完毕']);
            }

            return json(['code'=>-1,'data'=>null,'msg'=>'保存失败']);
        }
        
        $id=input('param.id');
        
        $item=Db::name('tb_item')->where('id',$id)->find();
        $this->assign('item',$item);
        return $this->fetch();
    }
    
    /**
     * 获得选品库
     *
     * @return Ambigous <void, string>
     */
    public function mychosens()
    {
        $db = Db::name('tao_favorites');
        
        $count = $db->count();
        
        $list = $db->order('favorites_id desc')->paginate(10);
        
        $this->assign('page_method', $list->render());
        $this->assign('news_count', $count);
        $this->assign('list', $list);
        $this->assign('pageindex', input('page'));
        
        return $this->fetch();
    }

    /**
     * 同步选品库
     *
     * @return void
     * @author dmakecn@163.com
     * @since 2022-04-28
     */
    public function proc_chosens()
    {
        Loader::import('taobaoke/TopSdk', EXTEND_PATH);
        $c = new \TopClient();
        
        $req = new \TbkUatmFavoritesGetRequest();
        $req->setPageNo("1");
        $req->setPageSize("100");
        $req->setFields("favorites_title,favorites_id,type");
        $req->setType("-1");
        $resp = $c->execute($req);
        
        $jsonstr = json_encode($resp);
        $json = json_decode($jsonstr, true);
        
        $favters = $json['results']['tbk_favorites'];
        
        $db = Db::name('tao_favorites');
        
        foreach ($favters as $fa) {
            
            if (! $db->where('favorites_id', $fa['favorites_id'])->find()) {
                $db->insert($fa);
            }
        }
        
        $this->success('同步选品库完毕', url('mychosens'));
    }

    /**
     * 获得选品库中的商品清单
     *
     * @return Ambigous <void, string>
     */
    public function mychosenitems()
    {
        $favorites_id = input('favorites_id');
        
        $conod = '';
        
        if ($favorites_id) {
            $conod['favorites_id'] = $favorites_id;
        }
        
        $db = Db::name('tao_favorites_items');
        
        $count = $db->where($conod)->count();
        
        $list = $db->where($conod)
            ->order('num_iid desc')
            ->paginate(20);
        
        $this->assign('page_method', $list->render());
        $this->assign('news_count', $count);
        $this->assign('list', $list);
        $this->assign('pageindex', input('page'));
        
        $this->assign('fa', Db::name('tao_favorites')->where('favorites_id', $favorites_id)
            ->find());
        
        return $this->fetch();
    }

    public function delitem()
    {
        $id = input('id');
        $favorites_id = input('favorites_id');
        
        $db = Db::name('tao_favorites_items');
        
        if ($db->where('id', $id)->delete()) {
            $this->success('删除产品完毕', url('mychosenitems', array(
                'favorites_id' => $favorites_id
            )));
        } else {
            $this->error('删除产品失败');
        }
    }

    public function proce_chosenitems()
    {
        $favorites_id = input('favorites_id');
        
        Loader::import('taobaoke/TopSdk', EXTEND_PATH);
        $c = new \TopClient();
        $req = new \TbkUatmFavoritesItemGetRequest();
        $req->setPlatform("1");
        $req->setPageSize("100");
        $req->setAdzoneId('65100878'); // 橱窗推广专用id
        $req->setFavoritesId($favorites_id);
        $req->setPageNo("1");
        $req->setFields("num_iid,title,pict_url,small_images,reserve_price,zk_final_price,user_type,provcity,item_url,click_url,seller_id,volume,nick,shop_title,zk_final_price_wap,event_start_time,event_end_time,tk_rate,status,type");
        $resp = $c->execute($req);
        
        $jsonstr = json_encode($resp);
        $json = json_decode($jsonstr, true);
        
        if ($json['total_results'] == 0) {
            $this->error('选品库还没有任何商品哦！');
        }
        
        $favters = $json['results']['uatm_tbk_item'];
        
        $db = Db::name('tao_favorites_items');
        
        foreach ($favters as $fa) {
            
            if (! $db->where('num_iid', $fa['num_iid'])->find()) {
                $fa['favorites_id'] = $favorites_id;
                $db->insert($fa);
            }
        }
        
        $this->success('同步选品库完毕', url('mychosenitems', array(
            'favorites_id' => $favorites_id
        )));
    }

    public function mychosenitemdetail()
    {}

    public function editmychosen()
    {
        $fid = input('fid');
        
        $db = Db::name('tao_favorites');
        
        $model = $db->where('fid', $fid)->find();
        
        if (request()->isPost()) {
            $model['pic_url'] = input('pic_url');
            $model['favorites_title'] = input('favorites_title');
            $model['intro'] = input('intro');
            
            if ($db->where('fid', $fid)->update($model)) {
                $this->success('修改完毕', url('mychosenitems'));
            }
        }
        
        $this->assign('model', $model);
        
        return $this->fetch();
    }

    public function itemget()
    {
        Loader::import('taobaoke/TopSdk', EXTEND_PATH);
        $c = new \TopClient();
        $req = new \TbkItemGetRequest();
        $req->setFields("num_iid,title,pict_url,small_images,reserve_price,zk_final_price,user_type,provcity,item_url,seller_id,volume,nick");
        $req->setQ("女装");
        $req->setCat("16,18");
        $req->setItemloc("杭州");
        $req->setSort("tk_rate_des");
        $req->setIsTmall("false");
        $req->setIsOverseas("false");
        $req->setStartPrice("10");
        $req->setEndPrice("10");
        $req->setStartTkRate("123");
        $req->setEndTkRate("123");
        $req->setPlatform("1");
        $req->setPageNo("123");
        $req->setPageSize("20");
        $resp = $c->execute($req);
        
        return dump($resp);
    }

    public function process_brand()
    {
        $sql = 'insert into tlsc.think_tao_brand select * from tltao.ftxia_brand where tltao.ftxia_brand.id>(select max(id) from tlsc.think_tao_brand) and tltao.ftxia_brand.sellerId>0;';
        $sql2 = 'insert into tlsc.think_tao_brand_items select * from tltao.ftxia_brand_items where tltao.ftxia_brand_items.id>(select max(id) from tlsc.think_tao_brand_items) and tltao.ftxia_brand_items.sellerId>0;';
        
        Db::execute($sql);
        Db::execute($sql2);
        
        $this->success('同步品牌团完毕', url('mychosens'));
    }

    public function myqg()
    {
        $db = Db::name('tao_tqg');
        
        $count = $db->count();
        
        $list = $db->order('start_time desc')->paginate(20);
        
        $this->assign('page_method', $list->render());
        $this->assign('news_count', $count);
        $this->assign('list', $list);
        $this->assign('pageindex', input('page'));
        
        return $this->fetch();
    }

    public function taoqianggou()
    {
        $page = input('p');
        if ($page == null || $page < 1) {
            $page = 1;
        }
        
        Loader::import('taobaoke/TopSdk', EXTEND_PATH);
        $c = new \TopClient();
        $req = new \TbkJuTqgGetRequest();
        $req->setAdzoneId("65100878");
        $req->setFields("click_url,pic_url,reserve_price,zk_final_price,total_amount,sold_num,title,category_name,start_time,end_time");
        $req->setStartTime(date('Y-m-d h:i:s', time()));
        $req->setEndTime(date('Y-m-d H:i:s', strtotime('1 day')));
        $req->setPageNo($page);
        $req->setPageSize("96");
        $resp = $c->execute($req);
        
        $jsonstr = json_encode($resp);
        $json = json_decode($jsonstr, true);
        
        if ($json['total_results'] == 0) {
            $this->error('木有找到淘抢购商品');
        }
        
        $favters = $json['results']['results'];
        
        $db = Db::name('tao_tqg');
        
        foreach ($favters as $fa) {
            
            if (! $db->where('num_iid', $fa['num_iid'])->find()) {
                $db->insert($fa);
            }
        }
        
        $this->success('同步选品库完毕', url('myqg'));
    }
}
