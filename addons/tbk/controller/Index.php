<?php
namespace addons\tbk\controller;

use app\common\model\TaobaoBase;
use think\addons\Controller;
use think\Db;

//https://www.iconfont.cn/illustrations/detail?spm=a313x.7781069.1998910419.d9df05512&cid=24652 插图素材

class Index extends Controller {
	/**
	 * 首页
	 *
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2021-01-22
	 */
	public function index() {

		$cates = $this->tkNav();

		$tb = new TaobaoBase();

		$db = Db::name('tb_item');

		$start = date('Y-m-d H:00:00');
		$end = date('Y-m-d H:00:00', strtotime("+1 hour"));

		$scroll = $this->cleandt($tb->getMaterialitems(0, 0, 0, 1, 28026, 8));
		for ($i = 0; $i < count($scroll); $i++) {
			if (!key_exists('small_images', $scroll[$i])) {
				unset($scroll[$i]);
			}
		}

		$data = [
			'scroll' => $scroll, //滚动的
			'goodcoupon' => $this->cleandt($tb->getMaterialitems(0, 0, 0, 1, 3756, 10)), //好物推荐
			'bigcoupon' => $this->cleandt($tb->getMaterialitems(0, 0, 0, 1, 27446, 10)), //大额券
			'recomend' => $this->cleandt($tb->getMaterialitems(0, 0, 0, 1, 31362, 10)), //其他商品
			'brandsales' => $this->cleandt($tb->getMaterialitems(0, 0, 0, 1, 3786, 10)), //品牌热销
			'xuanpin' => $this->cleandt($tb->Xuanpin()['list']), //人工选品
			'nav' => $cates,
			'title' => '找同款网，找同款好物券多多',
			'description' => '找同款网同款好物比价网站，能够非常方便的寻找到您所喜欢的商品同款并且提供大额优惠券，不积分不返利只为无套路帮你淘同款。',
		];

		$this->view->assign($data);
		return $this->fetch();
	}

	/**
	 * 普通清单
	 *
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2021-01-22
	 */
	function list() {

		$param = input('param.');

		if (!isset($param['t'])) {
			$param['t'] = 's';
		}

		if (!isset($param['index'])) {
			$param['index'] = '0';
		}

		if (!isset($param['name'])) {
			$param['name'] = '全网商品';
		}

		if (!isset($param['page'])) {
			$param['page'] = 1;
		}

		if (!isset($param['kw'])) {
			$param['kw'] = '推荐';
		}

		$tb = new TaobaoBase();

		if ($param['t'] == 'm') {
			$list = $tb->getMaterialitems(0, 0, 0, $param['page'], $param['material_id'], 30);
		} else {
			$list = $tb->getDG($param['kw'], $param['page'], 30);
		}

		if (request()->isPost()) {
			return getJsonCode($list);
		}

		$cates = $this->tkNav();

		$data = ['list' => $this->cleandt($list), 'param' => $param, 'nav' => $cates, 'title' => ($param['kw'] == '推荐' ? $param['name'] : $param['kw']) . '同款比价领大额券超划算', 'description' => ($param['kw'] == '推荐' ? $param['name'] : $param['kw']) . '同款优惠，找同款网大额券送给你！'];

		$this->view->assign($data);
		return $this->fetch();
	}

	/**
	 * 普通详情
	 *
	 * @return void
	 * @author dmakecn@163.com
	 * @since 2021-01-22
	 */
	public function detail() {
		$tb = new TaobaoBase();

		$num_iid = input('param.id');

		$detail = $tb->InfoGet($num_iid, 1);

		$item = $detail['item'];

		if (!array_key_exists('small_images', $item)) {
			return $this->error('该产品图片缺失，即将为您跳转');
		}

		$url = Db::name('tk_urls')->where('num_iid', $num_iid)->find();

		if (!$url) {
			$url['click_url'] = $item['item_url'] . '&ali_trackid=2:mm_23250501_15176373_109838350085:1617180379_244_498738618&union_lens=recoveryid:1617180379_244_498738618';
			$url['coupon_url'] = '';
		}

		$cates = $this->tkNav();

		$data = [
			'item' => $item,
			'items' => $detail['items'],
			'title' => $item['title'],
			'description' => $item['title'] . '同款优惠，找同款网大额券送给你，一淘同源、券多多券妈妈同源！',
			'url' => $url,
			'nav' => $cates,
		];

		$this->view->assign($data);
		return $this->fetch();
	}

	public function getcode() {
		$url = input('param.longurl');
		$tb = new TaobaoBase();
		return $tb->long2short($url);
	}

	private function cleandt($list) {
		if (!$list) {
			return false;
		}
		$xlist = [];
		foreach ($list as $obj) {
			if (!$obj) {
				continue;
			}

			if (isset($obj['short_title']) && is_array($obj['short_title'])) {
				continue;
			}

			array_push($xlist, $obj);
		}
		return $xlist;
	}

/**
 * 所有淘宝客分类
 *
 * @return void
 * @description
 * @example
 * @author daneas
 * @since 2021-03-31
 */
	private function tkNav() {
		$json = file_get_contents("./static/taoke.json");

		return json_decode($json, true);
	}

}
