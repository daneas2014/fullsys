<?php
namespace addons\openflat\controller;

use think\addons\Controller;
use think\Db;
use EasyWeChat\Factory;
use EasyWeChat\OpenPlatform\Server\Guard;
use EasyWeChat\Kernel\Messages\Video;
use EasyWeChat\Kernel\Messages\Voice;
use EasyWeChat\Kernel\Messages\Image;

/*
 * 做第三方的想法是从铜梁视窗口令红包获取到的灵感
 * 一个前瞻性的营销模式可以在短时间内获取粉丝，那么同样可以让合作企业使用这个服务
 * 做第三方平台不是铜梁视窗的发展根本，让普通企业加入铜梁视窗，通过给商家付费获取授权的方式，我们开放部分功能给商家，帮助商家增加粉丝
 * 授权事件接收URLhttp://open.dmake.cn/index/callback.html
 * 授权后实现业务
 * 消息校验Tokenserviceforourlife
 * 消息加解密KeyocjdtPdmEnWMzpIPeuQwYsXnIjSoIiKsPYxflQaFPue
 * 消息与事件接收URLhttp://open.dmake.cn/index/process/no/$APPID$.html
 */
class Index extends Controller
{

    /*
     * 第三方app的refreshtoken建名
     */
    private $key_refreshkey = CKEY_WX_REFRESHTOKEN;

    public function callback()
    {
        $openPlatform = Factory::openPlatform(config('openflat'));
        
        if (input('get.auth_code')) {
            $result = $openPlatform->handleAuthorize(input('get.auth_code'));
            // file_put_contents('/www/web/tlsc/web/public_html/inf.txt', json_encode($result));
            $info = $result['authorization_info'];
            $data = [
                'access_token' => $info['authorizer_access_token'],
                'refresh_token' => $info['authorizer_refresh_token'],
                'authorization_info' => json_encode($result)
            ];
            
            cache($this->key_refreshkey . $info['authorizer_appid'], $info['authorizer_refresh_token']);
            
            Db::name('op_app')->where('appid', $info['authorizer_appid'])->update($data);
            
            return redirect(url('admin/openflat/index'));
        }
        
        $server = $openPlatform->server;
        $message = $server->getMessage();
        
        switch ($message['InfoType']) {
            
            case Guard::EVENT_AUTHORIZED:
                // 处理授权成功事件
                $server->push(function ($message) {
                    // $message 为微信推送的通知内容，不同事件不同内容，详看微信官方文档
                    // 获取授权公众号 AppId： $message['AuthorizerAppid']
                    // 获取 AuthCode：$message['AuthorizationCode']
                    // 然后进行业务处理，如存数据库等...
                    
                    $appid = $message['AuthorizerAppid'];
                    $code = $message['AuthorizationCode'];
                    
                    Db::name('op_app')->where('appid', $appid)
                        ->update([
                        'authcode' => $code,
                        'status' => 'active'
                    ]);
                }, Guard::EVENT_AUTHORIZED);
                break;
            
            case Guard::EVENT_UPDATE_AUTHORIZED:
                // 处理授权更新事件
                $server->push(function ($message) {
                    
                    $appid = $message['AuthorizerAppid'];
                    $code = $message['AuthorizationCode'];
                    
                    Db::name('op_app')->where('appid', $appid)
                        ->update([
                        'authcode' => $code
                    ]);
                }, Guard::EVENT_UPDATE_AUTHORIZED);
                break;
            
            case Guard::EVENT_UNAUTHORIZED:
                // 处理授权取消事件
                $server->push(function ($message) {
                    
                    $appid = $message['AuthorizerAppid'];
                    
                    Db::name('op_app')->where('appid', $appid)
                        ->update([
                        'status' => 'cancel'
                    ]);
                }, Guard::EVENT_UNAUTHORIZED);
                break;
        }
        
        $server->serve()->send();
    }

    /*
     * 处理已经授权的公众号的那些事儿
     * 网友的实例：https://www.easywechat.com/discussions/153
     */
    public function process()
    {
        $appid = input('no');
        
        // 1.准备所有需要配置的东西
        $openPlatform = Factory::openPlatform(config('openflat'));
        $server = $openPlatform->server;
        $msg = $server->getMessage();
        
        // 2.获取公众号信息所需要的信息
        $gh_id = $msg['ToUserName'];
        $openid = $msg['FromUserName'];
        $appid = $appid ? $appid : (Db::name('op_app')->where('username', $gh_id)->find())['appid'];
        
        // 3.通过接口把公众号的所有信息搞下来 {"authorizer_info":{"nick_name":"mpqinyetest001","head_img":"http:\/\/wx.qlogo.cn\/mmopen\/lI3oesUNbM71vzTbK0bib4t8DFkBhO2qsnDAhls01yQkX66xFSnrhQMQHeUwpsPOZyoJhj0f6glWZIJibicueVXmYIFG39fu5fs\/0","service_type_info":{"id":2},"verify_type_info":{"id":0},"user_name":"gh_3c884a361561","alias":"mpqinyetest001","qrcode_url":"http:\/\/mmbiz.qpic.cn\/mmbiz\/ZF2Nhic2stBHM6J1XlNd0j0XIxvM3DxLyCSic8r5796cNAUAtQ7NNiaS27GUhwy054WsH3yQiaS7wsDgHVIEf6cmwQ\/0","business_info":{"open_pay":0,"open_shake":0,"open_scan":0,"open_card":0,"open_store":0},"idc":3,"principal_name":"3072995023","signature":"\u6b64\u8d26\u53f7\u7528\u4e8e\u6388\u6743\u7ec4\u4ef6\u5165\u573a\u68c0\u6d4b\u7528\u9014"},"authorization_info":{"authorizer_appid":"wx570bc396a51b8ff8","authorizer_refresh_token":"refreshtoken@@@MDzOwhEVO0gcyaTV88bEVsqioiwYF9Tl6Sd5xyZ4spQ","func_info":[{"funcscope_category":{"id":1}},{"funcscope_category":{"id":15}},{"funcscope_category":{"id":4}},{"funcscope_category":{"id":7}},{"funcscope_category":{"id":2}},{"funcscope_category":{"id":3}},{"funcscope_category":{"id":11}},{"funcscope_category":{"id":6}},{"funcscope_category":{"id":5}},{"funcscope_category":{"id":8}},{"funcscope_category":{"id":13}},{"funcscope_category":{"id":9}},{"funcscope_category":{"id":10}},{"funcscope_category":{"id":12}},{"funcscope_category":{"id":22}},{"funcscope_category":{"id":23}},{"funcscope_category":{"id":24},"confirm_info":{"need_confirm":0,"already_confirm":0,"can_confirm":0}},{"funcscope_category":{"id":26}},{"funcscope_category":{"id":33},"confirm_info":{"need_confirm":0,"already_confirm":0,"can_confirm":0}}]}}
        
        $authappid = $appid;
        $authtoken = cache($this->key_refreshkey . $authappid);
        
        if ($authtoken == null || strlen($authtoken) < 10) {
            $authinfo = $openPlatform->getAuthorizer($appid)['authorization_info'];
            if ($authinfo['authorizer_refresh_token'] != null && $authinfo['authorizer_refresh_token'] != '') {
                $authappid = $authinfo['authorizer_appid'];
                $authtoken = $authinfo['authorizer_refresh_token'];
                
                Db::name('op_app')->where('appid', $appid)->update([
                    'refresh_token' => $authtoken
                ]);
            }
            cache($this->key_refreshkey . $authappid, $authtoken);
        }
        
        // 4. 处理信息
        if ($gh_id != 'gh_3c884a361561') {
            $cusapp = $openPlatform->officialAccount($authappid, $authtoken);
            
            $return = null;
            
            switch ($msg['MsgType']) {
                case 'event':
                    $event = new \app\wechat\model\ServiceEvent();
                    $return = $event->processEvent($gh_id, $openid, $msg, $cusapp);
                    break;
                
                case 'text':
                    $text = new \app\wechat\model\ServiceProcess();
                    $return = $text->processPutTxt($gh_id, $openid, $msg['Content']);
                    break;
                case 'image':
                    $mediaId = $msg->MediaId;
                    $return = new Image([
                        'media_id' => $mediaId
                    ]);
                    break;
                case 'voice':
                    if ($msg['Recognition']) {
                        $regstr = '/[[:punct:]]|[[:lower:]]|。|，|,/i';
                        $word = preg_replace($regstr, '', $msg['Recognition']);
                        
                        $one = Db::name('wx_luckcode')->where('code', $word)
                            ->where('gh_id', $gh_id)
                            ->find(); // 还剩余多少现金红包
                        
                        if ($one != null && $one['leftnum'] > 0) {
                            $group = new \app\wechat\model\ServiceLuckmoney();
                            $return = $group->ProcessCode($gh_id, $word, $openid);
                            break;
                        }
                        
                        if (strstr($word, '天王盖地虎') || strstr($word, '口令')) {
                            $word = str_replace('天王盖地虎', '', $word);
                            $word = str_replace('口令', '', $word);
                            $word = str_replace(' ', '', $word);
                            
                            $group = new \app\wechat\model\ServiceLuckmoney();
                            $return = $group->ProcessCode($gh_id, $word, $openid);
                            break;
                        }
                        
                        $text = new \app\wechat\model\ServiceProcess();
                        $return = $text->processPutTxt($gh_id, $openid, $word);
                    } else {
                        $return = '亲，我只是个机器人啊，听不懂你说什么啊！';
                    }
                    
                    break;
                case 'video':
                    $mediaId = $msg->MediaId;
                    $return = new Video([
                        'media_id' => $mediaId
                    ]);
                    break;
                case 'location':
                    $return = '收到坐标消息';
                    break;
                case 'link':
                    $return = '收到链接消息';
                    break;
                default:
                    $return = '收到其它消息';
                    break;
            }
            
            if ($return == null || $return == false) {
                die();
            }
            
            $cusapp->customer_service->message($return)
                ->from($gh_id)
                ->to($openid)
                ->send();
            die();
        }
        
        // 第三方测试用的
        switch ($msg['MsgType']) {
            case 'event':
                
                $cuservice = $openPlatform->officialAccount($authappid, $authtoken)->customer_service;
                $cuservice->message($msg['Event'] . 'from_callback')
                    ->from($gh_id)
                    ->to($openid)
                    ->send();
            
            case 'text':
                $cuservice = $openPlatform->officialAccount($authappid, $authtoken)->customer_service;
                $word = $msg['Content'];
                
                if ($word == 'TESTCOMPONENT_MSG_TYPE_TEXT') {
                    $cuservice->message($word . '_callback')
                        ->from($gh_id)
                        ->to($openid)
                        ->send();
                    die();
                }
                
                if (strpos($word, 'QUERY_AUTH_CODE') == 0) {
                    echo '';
                    $code = substr($word, 16);
                    $authorizerInfo = $openPlatform->handleAuthorize($code)['authorization_info'];
                    $cuservice = $openPlatform->officialAccount($authorizerInfo['authorizer_appid'], $authorizerInfo['authorizer_refresh_token']);
                    $cuservice->customer_service->message($code . "_from_api")
                        ->from($gh_id)
                        ->to($openid)
                        ->send();
                }
        }
    }
}