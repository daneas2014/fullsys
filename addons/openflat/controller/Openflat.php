<?php
namespace addons\admin\controller;

use app\admin\model\OpenflatWalletModel;
use EasyWeChat\Factory; 
use think\Db;
use addons\Base;

class Openflat extends Base
{

    public function openflatid()
    {
        $openflatid = session('openflatid');
        if (strlen($openflatid) < 10) {
            return $this->error('你还未绑定微信公众号', url('index'));
        }

        return $openflatid;
    }

    /*
     * 第三方填写appid什么的，带意见授权按钮
     */
    public function index()
    {
        $db = Db::name('opf_app');
        if (request()->isPost()) {
            $data = input('post.');
            $result = false;
            $data['overdate'] = strtotime($data['overdate']);
            if ($data['id'] == null || $data['id'] == '') {
                // 创建第三方公众号账号
                $data['createtime'] = time();
                $result = $db->insert($data);
                // 创建第三方公众号账号钱包
                $wallet = new OpenflatWalletModel();
                $wallet->createwallet($data['username']);
            } else {

                $result = $db->where('id', $data['id'])->update($data);
            }

            session('openflatid', $data['username']);
            return $result ? $this->success('账号信息保存成功') : $this->error('保存失败');
        }

        $app = $db->where('mid', session('uid'))->find();

        if ($app != null && $app['appid'] != null) {
            session('openflatid', $app['username']);
        }

        $this->assign('app', $app);

        return $this->fetch();
    }

    /*
     * 查看自己的资金进度
     */
    public function fundm()
    {
        $openflatid = $this->openflatid();

        $cond = 'select sum(total_fee) as a,count(1) as b,%s from wx_order where  gh_id="' . $openflatid . '" and total_fee>0 ';

        $sql = sprintf($cond, 1) . ' and paid_at>0 and type=0 '; // 获取的是 一共收了多少钱
        $sql .= ' union (' . sprintf($cond, 2) . '  and paid_at>0 and type>0) '; // 获取一共付出了多少钱
        $sql .= ' union (' . sprintf($cond, 3) . '  and (paid_at between ' . strtotime(date('Y-m-01')) . ' and ' . strtotime(date('Y-m-t')) . ' ) and type=0) '; // 获取当月的收入
        $sql .= ' union (' . sprintf($cond, 4) . '  and (paid_at between ' . strtotime(date('Y-m-01')) . ' and ' . strtotime(date('Y-m-t')) . ' ) and type>0) '; // 获取当月的收入

        $result = Db::query($sql);

        for ($i = 0; $i < 4; $i++) {
            $result[$i]['a'] = $result[$i]['a'] ? $result[$i]['a'] : 0;
        }

        $this->assign('data', $result);

        return $this->fetch();
    }

    /*
     * 查看资金日志
     */
    public function fundlog()
    {
        $openflatid = $this->openflatid();

        $cond['gh_id'] = $openflatid;

        $kw = input('get.keyword') ? input('get.keyword') : null;
        $start = input('get.start') ? input('get.start') : date('Y-m-01');
        $end = input('get.end') ? input('get.end') : date('Y-m-t');

        if ($kw != null) {
            $cond['out_trade_no|openid'] = $kw;
        }
        $cond['paid_at'] = [
            'between',
            [
                strtotime($start),
                strtotime($end),
            ],
        ];

        $db = Db::name('wx_order');

        $count = $db->where($cond)->count();

        $list = $db->where($cond)
            ->order('id desc')
            ->paginate(10);

        $this->assign('keyword', $kw);
        $this->assign('start', $start);
        $this->assign('end', $end);
        $this->assign('page_method', $list->render());
        $this->assign('news_count', $count);
        $this->assign('list', $list);
        $this->assign('pageindex', input('p'));

        return $this->fetch();
    }

    /*
     * 保存口令
     */
    public function lucksave()
    {
        $openflatid = $this->openflatid();

        if (request()->isPost()) {
            $data = input('post.');

            $data['starttime'] = strtotime($data['starttime']);
            $data['endtime'] = strtotime($data['endtime']);
            if ($data['id'] < 1) {
                $data['leftnum'] = 0;
                $data['gh_id'] = $openflatid;
                $data['createtime'] = time();
                $result = Db::name('wx_luckcode')->insert($data);
            } else {
                $result = Db::name('wx_luckcode')->where('id', $data['id'])->update($data);
            }

            if ($result) {
                return json(['code' => 1, 'data' => '', 'msg' => '保存完毕']);
            }

            return json(['code' => -1, 'data' => '', 'msg' => '保存失败']);
        }

        $id = input('id');
        $menu = $id ? Db::name('wx_luckcode')->where('id', $id)->find() : null;

        $assign = ['apps' => Db::name('opf_app')->select(), 'menu' => $menu, 'gh_id' => $openflatid];
        return $this->view->assign($assign)->fetch();
    }

    /*
     * 口令管理
     */
    public function luckm()
    {
        $openflatid = $this->openflatid();

        $cond['gh_id'] = $openflatid;

        $kw = input('get.keyword') ? input('get.keyword') : null;
        $start = input('get.start') ? input('get.start') : date('Y-m-01');
        $end = input('get.end') ? input('get.end') : date('Y-m-t');

        if ($kw != null) {
            $cond['code'] = $kw;
        }
        $cond['createtime'] = [
            'between',
            [
                strtotime($start),
                strtotime($end),
            ],
        ];

        $assign = ['apps' => Db::name('opf_app')->select()];

        $db = Db::name('wx_luckcode');

        return parent::vueQuerySingle($db, $cond, 'id desc', $assign);
    }

    /*
     * 阅读任务列表
     */
    public function readlist()
    {
        $openflatid = $this->openflatid();

        $cond['gh_id'] = $openflatid;

        $kw = input('get.keyword') ? input('get.keyword') : null;
        $start = input('get.start') ? input('get.start') : date('Y-m-01');
        $end = input('get.end') ? input('get.end') : date('Y-m-t');

        if ($kw != null) {
            $cond['title'] = $kw;
        }
        $cond['createtime'] = [
            'between',
            [
                strtotime($start),
                strtotime($end),
            ],
        ];

        $db = Db::name('op_read');

        $count = $db->where($cond)->count();

        $list = $db->where($cond)
            ->order('id desc')
            ->paginate(10);

        $this->assign('keyword', $kw);
        $this->assign('start', $start);
        $this->assign('end', $end);
        $this->assign('page_method', $list->render());
        $this->assign('news_count', $count);
        $this->assign('list', $list);
        $this->assign('pageindex', input('p'));

        return $this->fetch();
    }

    /*
     * 保存阅读任务
     */
    public function readsave()
    {
        $openflatid = $this->openflatid();

        if (request()->isPost()) {
            $data = input('post.');

            if (array_key_exists('file', $data)) {
                unset($data['file']);
            }

            $data['starttime'] = strtotime($data['starttime']);
            $data['endtime'] = strtotime($data['endtime']);
            if ($data['id'] < 1) {
                $data['leftamount'] = 0;
                $data['gh_id'] = $openflatid;
                $data['createtime'] = time();
                $result = Db::name('op_read')->insert($data);
            } else {
                $result = Db::name('op_read')->where('id', $data['id'])->update($data);
            }

            if ($result) {
                return json([
                    'code' => 1,
                    'data' => '',
                    'msg' => '保存完毕',
                ]);
            }

            return json([
                'code' => -1,
                'data' => '',
                'msg' => '保存失败',
            ]);
        }

        $id = input('id');
        $menu = null;

        if ($id) {
            $menu = Db::name('op_read')->where('id', $id)->find();
        }
        $this->assign('menu', $menu);
        $this->assign('gh_id', $openflatid);

        return $this->fetch();
    }

    /*
     * 阅读任务日志
     */
    public function readlog()
    {
        $type = input('get.type');
        $dataid = input('get.dataid');
        $readid = input('get.readid');
        $count = 0;
        $list = null;

        if ($type == 'user') {
            $cond['readid'] = $readid;
            $cond['openid'] = $dataid;
            $cond['gh_id'] = $this->openflatid();
            $db1 = Db::name('op_readuser')->where();
            $count = $db1->where($cond)->count();
            $list = $db1->where($cond)
                ->order('id desc')
                ->paginate(10);
        } else {
            $cond['readid'] = $readid;
            $cond['readuserid'] = $dataid;
            $db1 = Db::name('op_readlog')->where();
            $count = $db1->where($cond)->count();
            $list = $db1->where($cond)
                ->order('id desc')
                ->paginate(10);
        }

        $this->assign('page_method', $list->render());
        $this->assign('news_count', $count);
        $this->assign('list', $list);
        $this->assign('pageindex', input('p'));

        return $this->fetch();
    }

    /*
     * 给某些群众发消息
     */
    public function sendmsg()
    {
        $openflatid = session('openflatid');

        if (request()->isPost()) {
            $app = Db::name('op_app')->where('username', $openflatid)->find();
            $open = Factory::openPlatform(config('openflat'))->officialAccount($app['appid'], $app['refresh_token'])->broadcasting;
            $open->sendMessage(input('post.msg'), explode(',', input('post.openid')));
        }
    }
}
