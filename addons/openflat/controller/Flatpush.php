<?php
namespace addons\openflat\controller;

use think\Db;
use think\addons\Controller;
use EasyWeChat\Factory;

class Flatpush extends Controller
{

    /*
     * 第三方app的refreshtoken建名
     */
    private $key_refreshkey = CKEY_WX_REFRESHTOKEN;

    public function initofficialAccount($appid)
    {
        $openPlatform = Factory::openPlatform(config('openflat'));
        $server = $openPlatform->server;
        $msg = $server->getMessage();
        
        $authappid = $appid;
        $authtoken = cache($this->key_refreshkey . $authappid);
        
        if ($authtoken == null || strlen($authtoken) < 10) {
            $authinfo = $openPlatform->getAuthorizer($appid)['authorization_info'];
            if ($authinfo['authorizer_refresh_token'] != null && $authinfo['authorizer_refresh_token'] != '') {
                $authappid = $authinfo['authorizer_appid'];
                $authtoken = $authinfo['authorizer_refresh_token'];
                
                Db::name('op_app')->where('appid', $appid)->update([
                    'refresh_token' => $authtoken
                ]);
            }
            cache($this->key_refreshkey . $authappid, $authtoken);
        }
        
        $cusapp = $openPlatform->officialAccount($authappid, $authtoken);
        
        return $cusapp;
    }

    /**
     * 自动任务：轮训数据库中的优惠券并发送出去
     */
    public function sendTicksTo()
    {
        $ticks = Db::name('op_msg')->alias('a')
            ->join('think_op_msgtemplate b', 'a.tid=b.id')
            ->field('b.appid,b.type,b.url,b.isurl,b.miniprogram,b.minappid,a.code,a.openid,a.data,a.id as aid')
            ->where('a.status=0')
            ->limit(100)
            ->select();
        
        set_time_limit(120);
        $db = Db::name('op_msg');
        
        foreach ($ticks as $t) {
            
            $app = self::initofficialAccount($t['appid']);
            $service = new \app\wechat\model\ServiceNotice();
            
            $msg['openid'] = $t['openid'];
            $msg['url'] = $t['url'];
            $msg['data'] = json_decode($t['data'], true);
            $msg['type'] = $t['type'];
            
            if ($t['isurl'] == '0') {
                $msg['miniprogram'] = 'miniprogram';
                $msg['appid'] = $t['minappid'];
                $msg['pagepath'] = $t['url'];
            }
            
            $result = false;
            try {
                if ($t['type'] == 'once') {
                    $result = $service->sendSubMsg($app, $msg);
                } else {
                    $result = $service->sendMsg($app, $msg);
                }
            } catch (PDOException $e) {
                continue;
            }
            
            if ($result) {
                $db->where('id', $t['aid'])->update([
                    'sendtime' => time(),
                    'status' => '1'
                ]);
            }
        }
        
        echo date('Y-m-d H:i:s') . '搞定了';
    }
}