<?php
namespace addons\admin\controller;

use addons\Base;
use app\admin\model\OpenflatWalletModel;
use baiduspeak\AipSpeech;
use EasyWeChat\Factory;
use think\Db;

class Openservice extends Base {

	/*
	 * 第三方app的refreshtoken建名
	 */
	private $key_refreshkey = CKEY_WX_REFRESHTOKEN;

	/*
	 * 处理微信第三方一键授权
	 */
	public function wxauth() {
		$openPlatform = Factory::openPlatform(config('openflat'));

		$url = $openPlatform->getPreAuthorizationUrl('https://www.dmake.cn/openflat/index/callback'); // 传入回调URI即可

		return redirect($url);
	}

	public function walletprocess() {
		$wallet = new OpenflatWalletModel();

		if (request()->isPost()) {
			$data = input('post.');

			if ($data['type'] == 1) {
				return $wallet->chagewallet(session('uid'), $data['gh_id'], $data['amount'], $data['remark']);
			} else {
				return $wallet->consumewallet(session('uid'), $data['gh_id'], $data['amount'], $data['remark']);
			}
		}

		// 1. 把所有的wallet带出来
		$this->assign('wallets', $wallet->allwallet());
		return $this->fetch();
	}

	public function walletlog() {
		$kw = input('get.key') ? input('get.key') : null;
		$start = input('get.start') ? input('get.start') : date('Y-m-01');
		$end = input('get.end') ? input('get.end') : date('Y-m-t');

		if ($kw != null) {
			$cond['remark'] = $kw;
		}
		$cond['a.createtime'] = [
			'between',
			[
				strtotime($start),
				strtotime($end),
			],
		];

		if (request()->isAjax()) {
			$db = Db::name('opf_appwallet_log');
			$count = $db->alias('a')->where($cond)->count();

			$list = $db->alias('a')
				->join('opf_app b', 'a.gh_id=b.username')
				->field('b.name,a.*')
				->where($cond)
				->order('id desc')
				->select(10);

			$data['list'] = $list;
			$data['count'] = $count;

			return json($data);
		}

		return $this->fetch();
	}

	/*
	 * 管理所有APP
	 */
	public function appm() {
		$keywords = input('get.kw');
		$cond = null;
		if ($keywords != null) {
			$cond['name'] = [
				'like',
				'%' . $keywords . '%',
			];
		}

		$db = Db::name('opf_app');

		return parent::vueQuerySingle($db, $cond);

	}

	/*
	 * 手动添加app
	 */
	public function apps() {
		$db = Db::name('opf_app');

		if (request()->isPost()) {
			$data = input('post.');
			$data['overdate'] = strtotime($data['overdate']);
			$result = false;
			if (!$data['id']) {
				$data['createtime'] = time();
				$data['status'] = $data['createtime'] < $data['overdate'] ? 'active' : 'overdate';
				$result = $db->insert($data);
			} else {
				$data['status'] = time() < $data['overdate'] ? 'active' : 'overdate';
				$result = $db->where('id', $data['id'])->update($data);
			}

			return $result ? getJsonCode(true, '账号信息保存成功') : getJsonCode(false, '保存失败');
		}

		$app = null;
		$appid = '';
		if (input('get.id')) {
			$app = $db->where('id', input('get.id'))->find();
			$appid = $app['appid'];
		}

		$this->assign('app', $app);
		$this->assign('appid', $appid);

		return $this->fetch();
	}

	/*
	 * 这个功能就是想拿来统计现在公众号的各项参数：现在有多少人，关注人数的变化，消息发送的变化
	 */
	public function appc() {
		$appid = input('get.appid');
	}

	public function appd() {
		$id = input('get.id');
		$url = url('appm');
		$result1 = Db::name('opf_app')->where('id', $id)->delete();
		return $result1 ? $this->success('公众号已删除', $url) : $this->error('公众号未能删除');
	}

	/*
	 * 阅读任务列表
	 */
	public function readlist() {
		$kw = input('get.keyword') ? input('get.keyword') : null;
		$gh_id = input('get.gh_id') ? input('get.gh_id') : null;
		$start = input('get.start') ? input('get.start') : date('Y-m-01');
		$end = input('get.end') ? input('get.end') : date('Y-m-t');

		if ($kw != null) {
			$cond['code'] = $kw;
		}

		if ($gh_id != null) {
			$cond['gh_id'] = $gh_id;
		}

		$cond['a.createtime'] = [
			'between',
			[
				strtotime($start),
				strtotime($end),
			],
		];

		$db = Db::name('opf_read');

		$count = $db->alias('a')
			->where($cond)
			->count();

		$list = $db->alias('a')
			->join('opf_app b', 'a.gh_id=b.username')
			->field('a.*,b.name')
			->where($cond)
			->order('id desc')
			->paginate(10);

		$this->assign('keyword', $kw);
		$this->assign('start', $start);
		$this->assign('end', $end);
		$this->assign('page_method', $list->render());
		$this->assign('news_count', $count);
		$this->assign('list', $list);
		$this->assign('pageindex', input('p'));

		$apps = Db::name('opf_app')->field('username,name')
			->where('status', 'active')
			->select();
		$this->assign('apps', $apps);

		return $this->fetch();
	}

	public function appt() {
		$appid = input('get.appid');

		$gzh = Db::name('opf_app')->where('appid', $appid)->find();

		if ($appid == 'wx149978cf4630bafe') {
			$serve = new \app\wechat\controller\Base();
			$app = $serve->wxapp();
			self::getUsers($gzh, $app, $gzh['nextopenid']);
			return;
		}

		$openPlatform = Factory::openPlatform(config('openflat'));

		$server = $openPlatform->server;

		$authappid = $appid;
		$authtoken = cache($this->key_refreshkey . $authappid);

		if ($authtoken == null || strlen($authtoken) < 10) {
			$authinfo = $openPlatform->getAuthorizer($appid)['authorization_info'];
			if ($authinfo['authorizer_refresh_token'] != null && $authinfo['authorizer_refresh_token'] != '') {
				$authappid = $authinfo['authorizer_appid'];
				$authtoken = $authinfo['authorizer_refresh_token'];

				Db::name('opf_app')->where('appid', $appid)->update([
					'refresh_token' => $authtoken,
				]);
			}
			cache($this->key_refreshkey . $authappid, $authtoken);
		}

		$cusapp = $openPlatform->officialAccount($authappid, $authtoken);

		self::getUsers($gzh, $cusapp, $gzh['nextopenid']);
	}

	private function getUsers($gzh, $app, $nextopenid) {
		$res = $app->user->list();
		$openids = $res['data']['openid'];

		set_time_limit(120);

		$db = Db::name('opf_appuser');

		if ($openids) {
			foreach ($openids as $openid) {
				$user = $app->user->get($openid);
				$user['gh_id'] = $gzh['username'];
				$user['data'] = json_encode($user);

				unset($user['subscribe']);
				unset($user['language']);
				unset($user['country']);
				unset($user['remark']);
				unset($user['groupid']);
				unset($user['tagid_list']);
				if (array_key_exists('subscribe_scene', $user)) {
					unset($user['subscribe_scene']);
				}
				if ($db->where('openid', $user['openid'])->count() < 1) {
					$db->insert($user);
				}

				unset($user);
			}
		}

		$nextopenid = $res['next_openid'];

		$db1 = Db::name('opf_app');
		if ($nextopenid) {

			$db1->where('appid', $gzh['appid'])->update([
				'nextopenid' => $nextopenid,
			]);

			self::getUsers($gzh['appid'], $app, $nextopenid);
		}
	}

	/*
	 * 保存阅读任务
	 */
	public function readsave() {
		$wallet = new OpenflatWalletModel();

		if (request()->isPost()) {
			$data = input('post.');

			$data['starttime'] = strtotime($data['starttime']);
			$data['endtime'] = strtotime($data['endtime']);

			if (array_key_exists('file', $data)) {
				unset($data['file']);
			}

			if ($data['id'] < 1) {
				$data['leftamount'] = 0;
				$data['createtime'] = time();
				$result = Db::name('op_read')->insert($data);

				$wallet->createwallet($data['gh_id']);
			} else {

				$appwallet = $wallet->getwallet($data['gh_id']);
				if ($appwallet == null) {
					$wallet->createwallet($data['gh_id']);
					return json([
						'code' => -1,
						'data' => '',
						'msg' => '这个公众号账号没钱，先去充值吧！',
					]);
				}
				if ($appwallet['leftamount'] < $data['leftamount']) {
					return json([
						'code' => -1,
						'data' => '',
						'msg' => '这个公众号账号余额不够，先去充值吧！',
					]);
				}

				$wallet->consumewallet(session('uid'), $data['gh_id'], $data['leftamount'], '系统充值到阅读任务');

				$result = Db::name('op_read')->where('id', $data['id'])->update($data);
			}

			if ($result) {
				return json([
					'code' => 1,
					'data' => '',
					'msg' => '保存完毕',
				]);
			}

			return json([
				'code' => -1,
				'data' => '',
				'msg' => '保存失败',
			]);
		}

		$id = input('id');
		$menu = null;

		if ($id) {
			$menu = Db::name('op_read')->where('id', $id)->find();
		}

		$this->assign('menu', $menu);
		$this->assign('wallets', $wallet->allwallet());

		return $this->fetch();
	}

	public function makevoice() {
		$client = new AipSpeech('10867310', 'RHUzrY7Gq1a1UArqySqUW1Ey', 'jvKmOZdCRS9CAquh49T7rb6yQtO852hE');

		// tex String 合成的文本，使用UTF-8编码，请注意文本长度必须小于1024字节 是
		// lang String 语言选择,填写zh 是
		// ctp String 客户端类型选择，web端填写1 是
		// cuid String 用户唯一标识，用来区分用户，填写机器 MAC 地址或 IMEI 码，长度为60以内 否
		// spd String 语速，取值0-9，默认为5中语速 否
		// pit String 音调，取值0-9，默认为5中语调 否
		// vol String 音量，取值0-15，默认为5中音量 否
		// per String 发音人选择, 0为女声，1为男声，3为情感合成-度逍遥，4为情感合成-度丫丫，默认为普通女 否

		$txt = input('get.code');

		$result = $client->synthesis($txt, 'zh', 1, array(
			'vol' => 10,
			'per' => 4,
			'spd' => 4,
			'pit' => 6,
		));

		$path = '/audio/' . time() . '.mp3';

		// 识别正确返回语音二进制 错误则返回json 参照下面错误码
		if (!is_array($result)) {
			file_put_contents('.' . $path, $result);
		}

		return redirect($path);
	}
}
