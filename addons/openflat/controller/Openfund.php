<?php
namespace addons\admin\controller;

use think\Db;
use addons\Base;

class Openfund extends Base
{

    /*
     * 铜梁视窗公众号的日志
     */
    public function index()
    {
        $cond = 'select sum(total_fee) as a,count(1) as b,%s from think_wx_order where total_fee>0';
        
        $sql = sprintf($cond, 1) . ' and paid_at>0 and type=0 '; // 获取的是 一共收了多少钱
        $sql .= ' union (' . sprintf($cond, 2) . ' and paid_at>0 and type>0) '; // 获取一共付出了多少钱
        $sql .= ' union (' . sprintf($cond, 3) . ' and (paid_at between ' . strtotime(date('Y-m-01')) . ' and ' . strtotime(date('Y-m-t')) . ' ) and type=0) '; // 获取当月的收入
        $sql .= ' union (' . sprintf($cond, 4) . ' and (paid_at between ' . strtotime(date('Y-m-01')) . ' and ' . strtotime(date('Y-m-t')) . ' ) and type>0) '; // 获取当月的收入
        
        $result = Db::query($sql);
        
        for ($i = 0; $i < 4; $i ++) {
            $result[$i]['a'] = $result[$i]['a'] ? $result[$i]['a'] : 0;
        }
        
        $this->assign('data', $result);
        
        return $this->fetch();
    }

    public function logs()
    {
        $kw = input('get.keyword') ? input('get.keyword') : null;
        $start = input('get.start') ? input('get.start') : date('Y-m-01');
        $end = input('get.end') ? input('get.end') : date('Y-m-t');
        
        if ($kw != null) {
            $cond['out_trade_no|openid'] = $kw;
        }
        $cond['paid_at'] = [
            'between',
            [
                strtotime($start),
                strtotime($end)
            ]
        ];
        
        $db = Db::name('wx_order');
        
        $count = $db->where($cond)->count();
        
        $list = $db->where($cond)
            ->order('id desc')
            ->paginate(10);
        
        $this->assign('keyword', $kw);
        $this->assign('start', $start);
        $this->assign('end', $end);
        $this->assign('page_method', $list->render());
        $this->assign('news_count', $count);
        $this->assign('list', $list);
        $this->assign('pageindex', input('p'));
        
        return $this->fetch();
    }

    public function luckm()
    {
        $kw = input('get.keyword') ? input('get.keyword') : null;
        $gh_id = input('get.gh_id') ? input('get.gh_id') : null;
        $start = input('get.start') ? input('get.start') : date('Y-m-01');
        $end = input('get.end') ? input('get.end') : date('Y-m-t');
        
        if ($kw != null) {
            $cond['code'] = $kw;
        }
        
        if ($gh_id != null && $gh_id != '') {
            $cond['gh_id'] = $gh_id;
        }
        
        $cond['a.createtime'] = [
            'between',
            [
                strtotime($start),
                strtotime($end)
            ]
        ];
        
        $db = Db::name('wx_luckcode');
        
        $count = $db->alias('a')
            ->where($cond)
            ->count();
        
        $list = $db->alias('a')
            ->join('think_op_app b', 'a.gh_id=b.username')
            ->field('a.*,b.name')
            ->where($cond)
            ->order('id desc')
            ->paginate(10);
        
        $this->assign('keyword', $kw);
        $this->assign('start', $start);
        $this->assign('end', $end);
        $this->assign('page_method', $list->render());
        $this->assign('news_count', $count);
        $this->assign('list', $list);
        $this->assign('pageindex', input('p'));
        
        $apps = Db::name('op_app')->field('username,name')
            ->where('status', 'active')
            ->select();
        $this->assign('apps', $apps);
        
        return $this->fetch();
    }

    public function lucks()
    {
        if (request()->isPost()) {
            $data = input('post.');
            
            $data['starttime'] = strtotime($data['starttime']);
            $data['endtime'] = strtotime($data['endtime']);
            
            $wallet = new \app\admin\model\OpenflatWalletModel();
            
            if ($data['id'] < 1) {
                $data['createtime'] = time();
                $result = Db::name('wx_luckcode')->insert($data);
                
                $wallet->createwallet($data['gh_id']);
            } else {
                
                $appwallet = $wallet->getwallet($data['gh_id']);
                if ($appwallet == null) {
                    $wallet->createwallet($data['gh_id']);
                    return json([
                        'code' => - 1,
                        'data' => '',
                        'msg' => '这个公众号账号没钱，先去充值吧！'
                    ]);
                }
                if ($appwallet['leftamount'] < $data['leftnum']) {
                    return json([
                        'code' => - 1,
                        'data' => '',
                        'msg' => '这个公众号账号余额不够，先去充值吧！'
                    ]);
                }
                
                $wallet->consumewallet(session('uid'), $data['gh_id'], $data['leftnum'], '系统充值到红包口令');
                
                $result = Db::name('wx_luckcode')->where('id', $data['id'])->update($data);
            }
            
            if ($result) {
                return json([
                    'code' => 1,
                    'data' => '',
                    'msg' => '保存完毕'
                ]);
            }
            
            return json([
                'code' => - 1,
                'data' => '',
                'msg' => '保存失败'
            ]);
        }
        
        $id = input('id');
        $menu = null;
        
        if ($id) {
            $menu = Db::name('wx_luckcode')->where('id', $id)->find();
        }
        $this->assign('menu', $menu);
        $apps = Db::name('op_app')->field('username,name')
            ->where('status', 'active')
            ->select();
        $this->assign('apps', $apps);
        
        return $this->fetch();
    }
}