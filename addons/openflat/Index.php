<?php
namespace addons\openflat; // 注意命名空间规范

use think\Addons;
use think\Db;

/**
 * 微信开放平台,多个公司或者多个平台数据打通、运营方式相同使用，是wechat的升级拓展
 * @author byron sampson
 */
class Index extends Addons// 需继承think\addons\Addons类

{
	// 该插件的基础信息
	public $info = [
		'name' => 'openflat', // 插件标识
		'title' => '微信开放平台', // 插件名称
		'description' => '微信开放平台拓展插件', // 插件简介
		'status' => 0, // 状态
		'author' => 'daneas',
		'version' => 'v1',
		'manage' => 'openflat://admin/index',
	];

	/**
	 * 插件安装方法
	 * @return bool
	 */
	public function install() {
		$res = Db::execute(initAddon($this->info));

		if (!$res) {
			return false;
		}

		$sql = file_get_contents(__DIR__ . "/data.sql");
		$a = explode(";\r\n", $sql); //根据";\r\n"条件对数据库中分条执行
		$total = count($a);
		for ($i = 0; $i < $total; $i++) {
			$res = Db::query($a[$i]);
		}

		return $res > 0 ? true : false;
	}

	/**
	 * 插件卸载方法
	 * @return bool
	 */
	public function uninstall() {
		$sql = "DROP TABLE IF EXISTS opf_app;
        DROP TABLE IF EXISTS opf_appuser;
        DROP TABLE IF EXISTS opf_appwallet;
        DROP TABLE IF EXISTS opf_appwallet_log;
        DROP TABLE IF EXISTS opf_luckdraw;
        DROP TABLE IF EXISTS opf_msg;
        DROP TABLE IF EXISTS opf_msgtemplate;
        DROP TABLE IF EXISTS opf_read;
        DROP TABLE IF EXISTS opf_readlog;
        DROP TABLE IF EXISTS opf_readuser;
        DROP TABLE IF EXISTS opf_signin;
        DROP TABLE IF EXISTS opf_signinlog;
        DROP TABLE IF EXISTS opf_signrank;
        DROP TABLE IF EXISTS opf_tickt;
        DROP TABLE IF EXISTS opf_ticktcommission;
        DROP TABLE IF EXISTS opf_tickts;
        delete from addons where name='openflat';";

		$a = explode(";\r\n", $sql); //根据";\r\n"条件对数据库中分条执行
		$total = count($a);
		for ($i = 0; $i < $total; $i++) {
			$res = Db::query($a[$i]);
		}
		return $res > 0 ? true : false;
	}

	/**
	 * 实现的testhook钩子方法
	 * @return mixed
	 */
	public function openflathook($param) {
		// 调用钩子时候的参数信息
		print_r($param);
		// 当前插件的配置信息，配置信息存在当前目录的config.php文件中，见下方
		print_r($this->getConfig());
		// 可以返回模板，模板文件默认读取的为插件目录中的文件。模板名不能为空！
		return $this->fetch('info');
	}

}
