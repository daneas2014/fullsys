-- ----------------------------
-- Table structure for opf_app
-- ----------------------------
DROP TABLE IF EXISTS `opf_app`;
CREATE TABLE `opf_app`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mid` int(11) NOT NULL COMMENT '管理员id',
  `name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `appid` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `username` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `type` int(11) NOT NULL DEFAULT 1 COMMENT '0是订阅号，1是服务号',
  `wximg` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '公众号二维码地址',
  `description` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `secret` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `access_token` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `refresh_token` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `authorization_info` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `jsdomain` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '安全域名',
  `options` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '接口',
  `authcode` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `status` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'active,overdate',
  `createtime` int(11) NOT NULL,
  `overdate` int(11) NOT NULL COMMENT '过期时间',
  `nextopenid` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '获取用户列表的下一个',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `appid`(`appid`) USING BTREE,
  UNIQUE INDEX `username`(`username`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of opf_app
-- ----------------------------
INSERT INTO `opf_app` VALUES (1, 21, '铜梁生活馆', 'wx17d9f462f641b50c', 'gh_07045554632a', 1, 'http://wx.tctuhuo.com/attachment/qrcode_1.jpg?time=1519631918', '铜梁生活优质内容服务平台', 'd9cc72cae64f50671443cbe141efd812', '6_e7mtnAr_H5jAWahzkruMJWwzqIaZH03-lh13RFwXrc_Qhem6gnaO8qPM0FuUdMiIalS3JbtxorgnOyuP_iWpUnB5jRz0YDGTCqxt8e9sWQ6NsFFX56JwIOXvalhqe1elyVo1B7jArb3DqwhAGIDiAFDLNI', 'refreshtoken@@@YwufWstT1w_yhRVrBnK0fITVR-o8qx7ySVKHGKI89CI', '{\"authorization_info\":{\"authorizer_appid\":\"wx17d9f462f641b50c\",\"authorizer_access_token\":\"6_e7mtnAr_H5jAWahzkruMJWwzqIaZH03-lh13RFwXrc_Qhem6gnaO8qPM0FuUdMiIalS3JbtxorgnOyuP_iWpUnB5jRz0YDGTCqxt8e9sWQ6NsFFX56JwIOXvalhqe1elyVo1B7jArb3DqwhAGIDiAFDLNI\",\"expires_in\":7200,\"authorizer_refresh_token\":\"refreshtoken@@@YwufWstT1w_yhRVrBnK0fITVR-o8qx7ySVKHGKI89CI\",\"func_info\":[{\"funcscope_category\":{\"id\":1}},{\"funcscope_category\":{\"id\":15}},{\"funcscope_category\":{\"id\":4}},{\"funcscope_category\":{\"id\":7}},{\"funcscope_category\":{\"id\":2}},{\"funcscope_category\":{\"id\":3}},{\"funcscope_category\":{\"id\":11}},{\"funcscope_category\":{\"id\":6}},{\"funcscope_category\":{\"id\":5}},{\"funcscope_category\":{\"id\":8}},{\"funcscope_category\":{\"id\":13}},{\"funcscope_category\":{\"id\":9}},{\"funcscope_category\":{\"id\":10}},{\"funcscope_category\":{\"id\":12}},{\"funcscope_category\":{\"id\":22}},{\"funcscope_category\":{\"id\":23}},{\"funcscope_category\":{\"id\":24},\"confirm_info\":{\"need_confirm\":0,\"already_confirm\":0,\"can_confirm\":0}},{\"funcscope_category\":{\"id\":26}},{\"funcscope_category\":{\"id\":33},\"confirm_info\":{\"need_confirm\":0,\"already_confirm\":0,\"can_confirm\":0}}]}}', 'v.dmake.cn', '', 'queryauthcode@@@PADkXSvu9owKuwz4I84QFqeACYzWnzjl1x', 'cancel', 1518325150, 1546272000, '');
INSERT INTO `opf_app` VALUES (2, 0, '测试公众号', 'wx570bc396a51b8ff8', 'gh_3c884a361561', 1, '', '', '', '', '', '', '', '', 'queryauthcode@@@FbMVyPQeI1C3NNjh9grPjh4Uv3NVw5behi', 'active', 1518325150, 1618325150, '');
INSERT INTO `opf_app` VALUES (3, 20, '铜城购', 'wx64943c1af286a89f', 'gh_43994364aa09', 1, '', '', 'b079c4116d5282179d4045a6122d40bb', '14_ok_BP6qj5t4GK4nNTvLDvH-vXVg9v5PZOcJg1eFSopCc1SD6JvZ5rVuQxdFacQ76FFNbD9pFJvaFBVqw9FDRWpGvel5oFl_rgqtM9Ch2MZF4pKyI3J9T9C5P8LVR-kBu4nN1Od5BgLjlrVB_LAJbAEDTIR', 'refreshtoken@@@ASbmMQPfTo5S2seBM4I2QTQIL3siLCZnqy8G9F6pnNs', '{\"authorization_info\":{\"authorizer_appid\":\"wx64943c1af286a89f\",\"authorizer_access_token\":\"14_ok_BP6qj5t4GK4nNTvLDvH-vXVg9v5PZOcJg1eFSopCc1SD6JvZ5rVuQxdFacQ76FFNbD9pFJvaFBVqw9FDRWpGvel5oFl_rgqtM9Ch2MZF4pKyI3J9T9C5P8LVR-kBu4nN1Od5BgLjlrVB_LAJbAEDTIR\",\"expires_in\":7200,\"authorizer_refresh_token\":\"refreshtoken@@@ASbmMQPfTo5S2seBM4I2QTQIL3siLCZnqy8G9F6pnNs\",\"func_info\":[{\"funcscope_category\":{\"id\":1}},{\"funcscope_category\":{\"id\":15}},{\"funcscope_category\":{\"id\":4}},{\"funcscope_category\":{\"id\":7}},{\"funcscope_category\":{\"id\":2}},{\"funcscope_category\":{\"id\":3}},{\"funcscope_category\":{\"id\":11}},{\"funcscope_category\":{\"id\":6}},{\"funcscope_category\":{\"id\":5}},{\"funcscope_category\":{\"id\":8}},{\"funcscope_category\":{\"id\":13}},{\"funcscope_category\":{\"id\":9}},{\"funcscope_category\":{\"id\":10}},{\"funcscope_category\":{\"id\":12}},{\"funcscope_category\":{\"id\":22}},{\"funcscope_category\":{\"id\":23}},{\"funcscope_category\":{\"id\":24},\"confirm_info\":{\"need_confirm\":0,\"already_confirm\":0,\"can_confirm\":0}},{\"funcscope_category\":{\"id\":26}},{\"funcscope_category\":{\"id\":33},\"confirm_info\":{\"need_confirm\":0,\"already_confirm\":0,\"can_confirm\":0}}]}}', '', '', 'queryauthcode@@@xLSZFR34Qs1rfbBw5usjLsye3uRlhpq6LB', 'cancel', 1518325150, 1618325150, '');
INSERT INTO `opf_app` VALUES (4, 1, '铜梁视窗', 'wx149978cf4630bafe', 'gh_7bf948cbfdb0', 0, '', '', '33f0d0314477c77fdd8d859646ad009f', '', '', '', 'www.5atl.com', '', '', 'active', 1518503079, 1577808000, '');
INSERT INTO `opf_app` VALUES (5, 19, '铜梁爱尔眼科', 'wxf4a78c5d23865b1f', 'gh_5cb2bd578b63', 1, '', '', 'dda8f701de310565949fec6dd3cfb580', '6_VblsG4PlUBwzZ4UYWRdYCMxQrqZg8Ji2WVuBxcsDwjWHqGs9TiT4xJdU_Xl66Yrkj5WWBJRGyNmwP90sPMYCHOmCyLWI0bcdUlRiJoVn3sBgSfe2PB0yghzI4V_8wVPz-dZyZpYwWLajhMvOVSViAEDFCF', 'refreshtoken@@@O6BXxCGDqHL0gYdaCuuOntCvVERtA0daq1AZbIclqMU', '{\"authorization_info\":{\"authorizer_appid\":\"wxf4a78c5d23865b1f\",\"authorizer_access_token\":\"6_VblsG4PlUBwzZ4UYWRdYCMxQrqZg8Ji2WVuBxcsDwjWHqGs9TiT4xJdU_Xl66Yrkj5WWBJRGyNmwP90sPMYCHOmCyLWI0bcdUlRiJoVn3sBgSfe2PB0yghzI4V_8wVPz-dZyZpYwWLajhMvOVSViAEDFCF\",\"expires_in\":7200,\"authorizer_refresh_token\":\"refreshtoken@@@O6BXxCGDqHL0gYdaCuuOntCvVERtA0daq1AZbIclqMU\",\"func_info\":[{\"funcscope_category\":{\"id\":1}},{\"funcscope_category\":{\"id\":15}},{\"funcscope_category\":{\"id\":4}},{\"funcscope_category\":{\"id\":7}},{\"funcscope_category\":{\"id\":2}},{\"funcscope_category\":{\"id\":3}},{\"funcscope_category\":{\"id\":11}},{\"funcscope_category\":{\"id\":6}},{\"funcscope_category\":{\"id\":5}},{\"funcscope_category\":{\"id\":8}},{\"funcscope_category\":{\"id\":13}},{\"funcscope_category\":{\"id\":9}},{\"funcscope_category\":{\"id\":10}},{\"funcscope_category\":{\"id\":12}},{\"funcscope_category\":{\"id\":22}},{\"funcscope_category\":{\"id\":23}},{\"funcscope_category\":{\"id\":24},\"confirm_info\":{\"need_confirm\":0,\"already_confirm\":0,\"can_confirm\":0}},{\"funcscope_category\":{\"id\":26}},{\"funcscope_category\":{\"id\":33},\"confirm_info\":{\"need_confirm\":0,\"already_confirm\":0,\"can_confirm\":0}}]}}', 'www.5atl.com', '', 'queryauthcode@@@Il1fAHlWxh9ldkjA_QDWqlhNZN610uHWoj', 'active', 1518589492, 1577808000, 'onfe60bxUMeOo1K9tmdRq1oN1Nek');
INSERT INTO `opf_app` VALUES (10, 22, '铜梁区龙康贝贝佳', 'wx9916a5a3d3eb602c', 'gh_5d91073fdbe4', 1, 'http://www.tlrbbs.com/data/attachment/forum/201801/01/163826ypzg0g074xtegp55.jpg', '', '4d2ef15a227fc9f1547e667ed5b779b7', '7_X1sFvYtClQOTvROzeLdxTADbIO11unmbkqdZM1MjOD_3LjM23k5bsEqS2IgvZ_mpnQyH5OXoNbkTlDcKxDCcUkz24z544ctC_fkc0tRkHpRuw1aUQvGup8H4n-fpEr_pP2aGUgmoJ7FPpiaIDQXeAEDWJQ', 'refreshtoken@@@UmktM9Z6_Hrg7OcNWcp0prDAseVzDzNB01zYjZ0Cdjk', '{\"authorization_info\":{\"authorizer_appid\":\"wx9916a5a3d3eb602c\",\"authorizer_access_token\":\"7_X1sFvYtClQOTvROzeLdxTADbIO11unmbkqdZM1MjOD_3LjM23k5bsEqS2IgvZ_mpnQyH5OXoNbkTlDcKxDCcUkz24z544ctC_fkc0tRkHpRuw1aUQvGup8H4n-fpEr_pP2aGUgmoJ7FPpiaIDQXeAEDWJQ\",\"expires_in\":7200,\"authorizer_refresh_token\":\"refreshtoken@@@UmktM9Z6_Hrg7OcNWcp0prDAseVzDzNB01zYjZ0Cdjk\",\"func_info\":[{\"funcscope_category\":{\"id\":1}},{\"funcscope_category\":{\"id\":15}},{\"funcscope_category\":{\"id\":4}},{\"funcscope_category\":{\"id\":7}},{\"funcscope_category\":{\"id\":2}},{\"funcscope_category\":{\"id\":3}},{\"funcscope_category\":{\"id\":11}},{\"funcscope_category\":{\"id\":6}},{\"funcscope_category\":{\"id\":5}},{\"funcscope_category\":{\"id\":8}},{\"funcscope_category\":{\"id\":13}},{\"funcscope_category\":{\"id\":9}},{\"funcscope_category\":{\"id\":10}},{\"funcscope_category\":{\"id\":12}},{\"funcscope_category\":{\"id\":22}},{\"funcscope_category\":{\"id\":23}},{\"funcscope_category\":{\"id\":24},\"confirm_info\":{\"need_confirm\":0,\"already_confirm\":0,\"can_confirm\":0}},{\"funcscope_category\":{\"id\":26}},{\"funcscope_category\":{\"id\":33},\"confirm_info\":{\"need_confirm\":0,\"already_confirm\":0,\"can_confirm\":0}}]}}', 'vote.5atl.com', '', 'queryauthcode@@@Be8JUhtCw40JC83VolhjarBhLUTek7brp9', 'overdate', 1519809885, 1577980800, '');


-- ----------------------------
-- Table structure for opf_appuser
-- ----------------------------
DROP TABLE IF EXISTS `opf_appuser`;
CREATE TABLE `opf_appuser`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `gh_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `openid` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `nickname` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `headimgurl` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `sex` int(11) NOT NULL,
  `province` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `city` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `unionid` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `data` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `subscribe_time` int(11) NOT NULL,
  `qr_scene` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `qr_scene_str` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `openid`(`openid`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '第三方公众平台上面关注的会员' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for opf_appwallet
-- ----------------------------
DROP TABLE IF EXISTS `opf_appwallet`;
CREATE TABLE `opf_appwallet`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gh_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '公众号id',
  `totalamount` double NOT NULL DEFAULT 0,
  `usedamount` double NULL DEFAULT 0,
  `leftamount` double NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `gh_id`(`gh_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of opf_appwallet
-- ----------------------------
INSERT INTO `opf_appwallet` VALUES (1, 'gh_5cb2bd578b63', 900, 0, 100);
INSERT INTO `opf_appwallet` VALUES (2, 'gh_07045554632a', 50, 0, 30);
INSERT INTO `opf_appwallet` VALUES (3, 'gh_7bf948cbfdb0', 1099, 0, 20);
INSERT INTO `opf_appwallet` VALUES (4, 'gh_5d91073fdbe4', 1000, 0, 600);

-- ----------------------------
-- Table structure for opf_appwallet_log
-- ----------------------------
DROP TABLE IF EXISTS `opf_appwallet_log`;
CREATE TABLE `opf_appwallet_log`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `admin` int(11) NOT NULL COMMENT '操作员id',
  `gh_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `amount` double NOT NULL,
  `createtime` int(11) NOT NULL,
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 36 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of opf_appwallet_log
-- ----------------------------
INSERT INTO `opf_appwallet_log` VALUES (2, 1, 'gh_5cb2bd578b63', 500, 1518594194, '爱尔眼科元宵节充值500欧文已付款');
INSERT INTO `opf_appwallet_log` VALUES (3, 1, 'gh_5cb2bd578b63', -500, 1518594679, '系统充值到红包口令');
INSERT INTO `opf_appwallet_log` VALUES (4, 1, 'gh_7bf948cbfdb0', 300, 1518595866, '铜梁视窗除夕活动3场充值100元');
INSERT INTO `opf_appwallet_log` VALUES (5, 1, 'gh_7bf948cbfdb0', -100, 1518596007, '系统充值到红包口令');
INSERT INTO `opf_appwallet_log` VALUES (6, 1, 'gh_7bf948cbfdb0', -100, 1518596344, '系统充值到红包口令');
INSERT INTO `opf_appwallet_log` VALUES (7, 1, 'gh_7bf948cbfdb0', -100, 1518596357, '系统充值到红包口令');
INSERT INTO `opf_appwallet_log` VALUES (8, 1, 'gh_7bf948cbfdb0', 50, 1519290822, '开工大吉红包');
INSERT INTO `opf_appwallet_log` VALUES (9, 1, 'gh_7bf948cbfdb0', -50, 1519290834, '系统充值到红包口令');
INSERT INTO `opf_appwallet_log` VALUES (10, 1, 'gh_7bf948cbfdb0', 50, 1519543195, '招聘红包');
INSERT INTO `opf_appwallet_log` VALUES (11, 1, 'gh_7bf948cbfdb0', -50, 1519543222, '系统充值到阅读任务');
INSERT INTO `opf_appwallet_log` VALUES (12, 1, 'gh_5cb2bd578b63', 400, 1519543623, '开学红包充值');
INSERT INTO `opf_appwallet_log` VALUES (13, 1, 'gh_07045554632a', 20, 1519647717, '招聘口令充值');
INSERT INTO `opf_appwallet_log` VALUES (14, 1, 'gh_07045554632a', -20, 1519647731, '系统充值到红包口令');
INSERT INTO `opf_appwallet_log` VALUES (15, 1, 'gh_5d91073fdbe4', 1000, 1519810347, '3月红包充值');
INSERT INTO `opf_appwallet_log` VALUES (16, 1, 'gh_5d91073fdbe4', -200, 1519810411, '系统充值到红包口令');
INSERT INTO `opf_appwallet_log` VALUES (17, 1, 'gh_7bf948cbfdb0', 20, 1519891049, '口令红包充值');
INSERT INTO `opf_appwallet_log` VALUES (18, 1, 'gh_7bf948cbfdb0', -20, 1519891059, '系统充值到红包口令');
INSERT INTO `opf_appwallet_log` VALUES (19, 1, 'gh_07045554632a', 30, 1519982384, '红包充值');
INSERT INTO `opf_appwallet_log` VALUES (20, 1, 'gh_7bf948cbfdb0', 38, 1520491905, '女王节口令');
INSERT INTO `opf_appwallet_log` VALUES (21, 1, 'gh_7bf948cbfdb0', -38, 1520491917, '系统充值到红包口令');
INSERT INTO `opf_appwallet_log` VALUES (22, 1, 'gh_5d91073fdbe4', -200, 1520644247, '系统充值到红包口令');
INSERT INTO `opf_appwallet_log` VALUES (23, 1, 'gh_5cb2bd578b63', -300, 1522054599, '系统充值到红包口令');
INSERT INTO `opf_appwallet_log` VALUES (24, 1, 'gh_7bf948cbfdb0', 30, 1522323682, '招聘红包');
INSERT INTO `opf_appwallet_log` VALUES (25, 1, 'gh_7bf948cbfdb0', -30, 1522323708, '系统充值到红包口令');
INSERT INTO `opf_appwallet_log` VALUES (26, 1, 'gh_7bf948cbfdb0', 30, 1525397859, '口令红包充值');
INSERT INTO `opf_appwallet_log` VALUES (27, 1, 'gh_7bf948cbfdb0', -30, 1525397880, '系统充值到红包口令');
INSERT INTO `opf_appwallet_log` VALUES (28, 1, 'gh_7bf948cbfdb0', 500, 1527384426, '红包充值');
INSERT INTO `opf_appwallet_log` VALUES (29, 1, 'gh_7bf948cbfdb0', -500, 1527384438, '系统充值到红包口令');
INSERT INTO `opf_appwallet_log` VALUES (30, 1, 'gh_7bf948cbfdb0', 50, 1538212167, '国庆骑兵广告');
INSERT INTO `opf_appwallet_log` VALUES (31, 1, 'gh_7bf948cbfdb0', -30, 1538212193, '系统充值到红包口令');
INSERT INTO `opf_appwallet_log` VALUES (32, 1, 'gh_7bf948cbfdb0', -1, 1538367419, '系统充值到红包口令');
INSERT INTO `opf_appwallet_log` VALUES (33, 1, 'gh_7bf948cbfdb0', 11, 1538392928, '10月4日土桥原生露营节报名啦');
INSERT INTO `opf_appwallet_log` VALUES (34, 1, 'gh_7bf948cbfdb0', -30, 1538392963, '系统充值到红包口令');
INSERT INTO `opf_appwallet_log` VALUES (35, 1, 'gh_7bf948cbfdb0', 20, 1538396597, '铜梁视窗再来一波红包');

-- ----------------------------
-- Table structure for opf_luckdraw
-- ----------------------------
DROP TABLE IF EXISTS `opf_luckdraw`;
CREATE TABLE `opf_luckdraw`  (
  `id` int(11) NOT NULL,
  `luckcode` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `luckdraw` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `phone` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `address` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `create_time` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `customerid` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for opf_msg
-- ----------------------------
DROP TABLE IF EXISTS `opf_msg`;
CREATE TABLE `opf_msg`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tid` int(11) NOT NULL COMMENT 'msgtemplate的id',
  `code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用这个代码做url链接id',
  `openid` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `data` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '键值对',
  `createtime` int(11) NOT NULL,
  `deadtime` int(11) NOT NULL,
  `sendtime` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '微信推送' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for opf_msgtemplate
-- ----------------------------
DROP TABLE IF EXISTS `opf_msgtemplate`;
CREATE TABLE `opf_msgtemplate`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `templateid` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '模板id',
  `title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `appid` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `gh_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `url` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `isurl` int(11) NOT NULL DEFAULT 1,
  `miniprogram` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `minappid` int(50) NOT NULL,
  `createtime` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '微信公众号模板消息模板存储' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of opf_msgtemplate
-- ----------------------------
INSERT INTO `opf_msgtemplate` VALUES (1, 'tickborth', 'gMHGTCjJ9wt0-TC7aaQYQIjUImA5U4Gf3ADG2wVw4A0', '您收到一张VVVIP优惠券', 'wx17d9f462f641b50c', 'gh_07045554632a', 'https://www.5atl.com/wechat-tickt-mytickts.html', 1, '', 0, 0);
INSERT INTO `opf_msgtemplate` VALUES (2, 'bbjhd', '1x3CPCxFQnPajPJtEN8u2wgMrIzBLcmVHYVLbkbiL3g', '恭喜您获得微商城第二期晒单有礼活动', 'wx9916a5a3d3eb602c', 'gh_5d91073fdbe4', 'http://www.tlrbbs.com/thread-20152-1-1.html', 1, '', 0, 1523763342);
INSERT INTO `opf_msgtemplate` VALUES (3, 'notice', 'xRFOHnnn_eKYKBXhXZ3xKvSMX2m0hZHpKRDbSxxsoNM', '恭喜你获得99元购买价值588元艺术照套餐资格', 'wx17d9f462f641b50c', 'gh_07045554632a', 'https://www.tlrbbs.com/plugin.php?id=tom_weikanjia&mod=kanjia&kanjia_id=3402&oauth_back_url=https%3A%2F%2Fwww.tlrbbs.com%2Fplugin.php&code=011AM9ri1BsWOv0IzFri1tKqri1AM9r9&state=1', 1, '', 0, 1524532277);

-- ----------------------------
-- Table structure for opf_read
-- ----------------------------
DROP TABLE IF EXISTS `opf_read`;
CREATE TABLE `opf_read`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `showpic` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `title` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `description` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `createtime` int(11) NOT NULL,
  `mpurl` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `totalamount` double NOT NULL,
  `limitnum` int(11) NOT NULL COMMENT '领取标准',
  `leftamount` double NOT NULL,
  `gh_id` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `starttime` int(11) NOT NULL,
  `endtime` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '阅读任务' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of opf_read
-- ----------------------------
INSERT INTO `opf_read` VALUES (1, 'https://timgsa.baidu.com/timg?image&quality=80&siz', '铜梁视窗元宵狂欢节，有你参加更精彩', '', '	                                <b>	                                	                                	阿斯顿发生打发大事发生大法师打发斯蒂芬斯蒂芬asdasdfasdf</b>	                                ', 1518855108, 'http://www.5atl.com', 10, 0, 0, 'gh_07045554632a', 1518696000, 1519920000);
INSERT INTO `opf_read` VALUES (2, 'http://www.reyao.cn/ruiyao/kindeditor/attached/image/20160908/20160908031339_33285.jpg', '铜梁视窗市场人员和编辑火热招募中，速度来报名啦', '', '<p>公司简介：</p><p>本公司是铜梁面向全网的网络营销企业，主要为铜梁各类企业提供深度定制营销策划和软件服务、各类主题会展、微信朋友圈广告服务、代理运营企业电商平台和营销事务部。目前公司有铜梁视窗微信公众平台和PC、小程序、APP以及一系列营销平台的自运营和销售服务。</p><p>岗位招聘：</p><p>1. 网络编辑：主要每天管理编辑公司和代运营平台推文，协助上级下发的策划活动大纲完成细案等相关工作。</p><p>2. 市场推广：主要面向铜梁区各类企业销售各类营销活动服务、招募铜梁视窗平台联盟商家等工作。</p><p>岗位要求：</p><p>1. 没有网购经验的不来，没有参加过投票助力砍价等等一系列网络活动的不来，年龄不在20-30岁的不来，性格强烈脾气火爆的不来，喜欢安安静静的做办公室的不来，不接受业绩考核的不来</p><p>2. 思想可以跳跃，但是有独立完成派发工作的能力</p><p><span style=\"white-space: normal;\">薪资待遇：</span></p><p>1. 编辑试用期1500元每月，有交通和午餐补贴，转正后2500元每月；</p><p>2. 市场推广试用期1500元每月，出上述补贴外业绩提成10%，转正后保底工资2500元每月，提成和佣金另算。</p>', 1519540904, 'http://www.5atl.com', 50, 10, 48, 'gh_07045554632a', 1514764800, 1577836800);

-- ----------------------------
-- Table structure for opf_readlog
-- ----------------------------
DROP TABLE IF EXISTS `opf_readlog`;
CREATE TABLE `opf_readlog`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `readid` bigint(20) NOT NULL,
  `readuserid` bigint(20) NOT NULL,
  `openid` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `nickname` varchar(24) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `headimgurl` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `token` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'md5',
  `createtime` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 44 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '帮助好友阅读日志' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of opf_readlog
-- ----------------------------
INSERT INTO `opf_readlog` VALUES (2, 1, 1, 'o73cGwSTPa07-i_C6ngzEcZ8kpEA', '锅烟煤', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLuYYonzUfOCZ1sq4ploqC11FYIU6eicCPlV0ia8dfXvVMrsy9OrrGIKBfFTkv47u0KaXJXapaPXxzA/132', '2d04d88eaded90fc8c1b6a913da65bbf', 1519285338);
INSERT INTO `opf_readlog` VALUES (3, 2, 2, 'o73cGwSTPa07-i_C6ngzEcZ8kpEA', '锅烟煤', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLuYYonzUfOCZ1sq4ploqC11FYIU6eicCPlV0ia8dfXvVMrsy9OrrGIKBfFTkv47u0KaXJXapaPXxzA/132', '2d04d88eaded90fc8c1b6a913da65bbf', 1519552797);
INSERT INTO `opf_readlog` VALUES (4, 2, 3, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', '大娃-何跃', 'http://thirdwx.qlogo.cn/mmopen/vi_32/eWtejun7vSn7cqfAibribL90CicXC2hLbWWAfnCqNY4N2sdVWLkNribIlhhibSQiaHGeTjl01WBSzs2euPbvmHVNFib8Q/132', '564b155d6e51db9913a70f3ddc9150dd', 1519622931);
INSERT INTO `opf_readlog` VALUES (5, 2, 2, 'o73cGwU0tdhj1TRsmjYK6P7l7RjI', '昀', 'http://thirdwx.qlogo.cn/mmopen/vi_32/1o8fDG54nogVK4XBe5za8R2Ch8opTvbYG8gT1lctxMOR9FjzJAyRZaobibNL1HM2jRDAQs6kHf3rfYQ35zBlYvg/132', 'a8d2bc61cf9322ac9047ec829a417df5', 1519625433);
INSERT INTO `opf_readlog` VALUES (6, 2, 4, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', '大娃-何跃', 'http://thirdwx.qlogo.cn/mmopen/vi_32/eWtejun7vSn7cqfAibribL90CicXC2hLbWWAfnCqNY4N2sdVWLkNribIlhhibSQiaHGeTjl01WBSzs2euPbvmHVNFib8Q/132', '564b155d6e51db9913a70f3ddc9150dd', 1519632271);
INSERT INTO `opf_readlog` VALUES (7, 2, 4, 'o73cGwZnwMesmf8vVYPJ0MMgt-1c', '小霸王', 'http://thirdwx.qlogo.cn/mmopen/vi_32/HQJpC7iaiaW1M9drR3N764sXibjc9ibJEsbQlS9sBzMMDEzQetFHyBIEWPWMCza3BH0UtATDSjMnSKkolvmVXianOBA/132', '6b0c2c77a1b40da1c6433515fca0fc87', 1519632392);
INSERT INTO `opf_readlog` VALUES (8, 2, 2, 'o73cGwVfIwzPn0mJUvEU7HjvUUpE', '123', 'http://thirdwx.qlogo.cn/mmopen/vi_32/8H9JoTQj0gnh6EJKBGXqTFvZakibuYXjrqDTAMVrJAM7Ywd0cvxrn7y5IiakADwjgaF1cgc4wVZvqY6fnyRuTqVw/132', 'e27277135637d97bfcb25dfeaf626bac', 1519633491);
INSERT INTO `opf_readlog` VALUES (9, 2, 2, 'o73cGwe1AsCVpsZyVriLij-R7gIQ', '文文', 'http://thirdwx.qlogo.cn/mmopen/vi_32/hbcP1AJQfdXowD5BItRcjib3oqBKYj8SW8bKXicwo4ic12ARnhUavbxlN8mq9JTA5FTP9RAkzaS7W6AtKGe3mIKGQ/132', 'a7fae2526419adc6a37981b940606be7', 1519634196);
INSERT INTO `opf_readlog` VALUES (10, 2, 2, 'o73cGwYeS9F1_212qvaZBZwzti90', '祖伟', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTKDYj9IQXhWOtwfNzNKu4wnYTPIKptqWg0IEEyiaCJJY0p9icsnRPnTnQ0iaHuZK3ZVicl4ibSClaicA7UQ/132', '6f46886b8f8b710c92becceca0323d9e', 1519635625);
INSERT INTO `opf_readlog` VALUES (11, 2, 2, 'o73cGwbmEPzmyY9WYIFqEhi8Sc5A', '火神之光', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLa6vzvRvAvAQniaPTbQibn0fTPaNianFfuoLmoaLzia3JJOxpJ1xocuiciaKbmTl9vR87ib6r2RHCejk2pw/132', '607431629ad7ac6dfa98dcad4dc651c8', 1519636890);
INSERT INTO `opf_readlog` VALUES (12, 2, 2, 'o73cGwd7cX7TzXZaJ4lQt0PcPuW8', '', 'http://thirdwx.qlogo.cn/mmopen/vi_32/fttwalbof7wyzRyAlCkldfY5UxjlKemBPVaOHicHv08CYktPiaesIz5NOwyqttoeViaR1YibExiaP071kUD6ibmTP4dw/132', '14590c2eebdb4c856e1e5a041aad38e3', 1519637331);
INSERT INTO `opf_readlog` VALUES (13, 2, 2, 'o73cGwXBIb8OyGHj5ftCxNpeqemI', '周莉', 'http://thirdwx.qlogo.cn/mmopen/vi_32/KybU3tziaCPNnOBicPDm5pu7ByoYCRibZAy96JE8SCvME82HStD2Np2dv7jG9UvBAsUO3MtOJb1RoD1X1D9jqnnkw/132', '71279cde6a9a278ed9b396a52aa14520', 1519637961);
INSERT INTO `opf_readlog` VALUES (14, 2, 2, 'o73cGwXFrUiZudgmE3GOXD4v1W38', '陈海冬', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLyb9zsXpTX8A2RYGbjBiaibDvicicO8FiboqzBaQc1EBfXEvruWFp4qEKyibKHkC05IsficWjwmVMNySLRg/132', '2c11058695ed5eafc021cad3ada77333', 1519642460);
INSERT INTO `opf_readlog` VALUES (15, 2, 2, 'o73cGwdCOWaK_t2ilVUEvrhY_TrY', 'Hunrry', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q7KhGI6GicW0y2bicqFjDITKZrtVTiaqUfnlbIiaqmzXks1fGz7RpthqZ8Xo1fhQ4fEic82brveiaPuun8gOYw2CXribg/132', 'fc44613c404ad63f5c691df1d67efc29', 1519648506);
INSERT INTO `opf_readlog` VALUES (16, 2, 2, 'o73cGwaFObAUuqQtbBAaif4JF8yM', 'A金赛广告', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Xftl5TSGibzVVzLJPvGwl06KicQfndo2J0r9rRZibZQFa3ZZH2eUCF3QR4zMPNq0LAzNibOxDzz1aYbG0BU9kE7KYg/132', '19fadff37ff33dabda0f524fe808ecb6', 1519652807);
INSERT INTO `opf_readlog` VALUES (17, 2, 2, 'o73cGwesvIh0aG0V0cqddixDTb2o', '曾经的曾经叫过去', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLbPelttpoZVldsrkU7r5G3vRTRmbibg7NYqe7LHtyU13L7B557EDmx8Bjhia5oyLgy0yNS6DIddozQ/132', 'f4b395fa64b2920332f5405402ab8c11', 1519870454);
INSERT INTO `opf_readlog` VALUES (18, 2, 2, 'o73cGwfqHwYzLHno4lD4P6G7O5AA', '', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83epqnFrIDeHzdODjJp8VTibrJ58lvJnHGS3TVQqD0CpQbFtjIwgkuW7XEoHqsJwrEuxiaoKCNW1Msdbg/132', '2021af6c18c52cf47e5ff758ac43671a', 1519870481);
INSERT INTO `opf_readlog` VALUES (19, 2, 2, 'o73cGwbTM963RgqukyrWJpjngqXQ', '毛毛', 'http://thirdwx.qlogo.cn/mmopen/vi_32/lSw2rTVa1AHg3WVWGyb8r46fzrsl3mrWB350wSv69RMokVUjUm8jph3D0ScbZvcKXL0etNQWt6Jr9wpyib238sA/132', 'ec89d0ab11cc54794376ac208ad62e19', 1519870483);
INSERT INTO `opf_readlog` VALUES (20, 2, 2, 'o73cGwbUGA1mIkKf69vdPI5Tyq3k', '小张（贝贝佳14店）', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83epjqraX93EPbVEqAeLO1CaricpD7VTONrG71X01BjOc5t3UbliboX40DBZEoYAcQCiaoCOoAcpzg5Lpw/132', '05da12444dedccd937d09e7b6e5d6441', 1519870625);
INSERT INTO `opf_readlog` VALUES (21, 2, 2, 'o73cGwTxw4F59WKF8iBD-ZEgCYns', '　　　　　¹⁷⁸²³⁵⁷⁷ºº⁷', 'http://thirdwx.qlogo.cn/mmopen/vi_32/UGyjPibnzwu3671pMc8q45gCFz9NePMeCGzJ5mKyjYyC8OYJWmBef2ibUOWJWPdnu2uzzjPibiadBYKk53uK21p7xQ/132', '6423e8bd35c6fc2f38d1d1ae40c66738', 1519871127);
INSERT INTO `opf_readlog` VALUES (22, 2, 2, 'o73cGwUArFcSvIqXc2hduqAmObug', '飞哥', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTKxYTiaWLibZmHUc8BRMbgk6e3s39GqJIWJfZkzb3wAnQ73r6aEibHK5cZNic2sDTQB1GcLhfsxFJrpZQ/132', 'b6f41a1ed861b0bb992e6155ccf200c8', 1519871185);
INSERT INTO `opf_readlog` VALUES (23, 2, 2, 'o73cGwXXSzxhmtTLdnOhr3ltWQlw', '雪儿', 'http://thirdwx.qlogo.cn/mmopen/vi_32/adQQjDic8WoSnIZala3PWXn8icqF196h4Y8xbmRpcUdEgkuw2tGguvINWOQPUvSZThuKMpo2yJibcRaFfKG6jETvw/132', 'd9981dca1a532cbaf24aa63872c76c0a', 1519871451);
INSERT INTO `opf_readlog` VALUES (24, 2, 2, 'o73cGwc_WQvc-pOhjKj97T5f75mQ', '烟波里成灰', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJj3DhppllicXttfBQp0x3aIsC8fBcYXfSFGpOpfp2icrF5WkkE3SkwU3Q9SMFaZMP2W7VH2ORBKPwQ/132', 'da945633e86aaafa42310e44ea88c309', 1519871494);
INSERT INTO `opf_readlog` VALUES (25, 2, 2, 'o73cGwd3HjTrHgQC0AxnfGwebF14', '周小利', 'http://thirdwx.qlogo.cn/mmopen/vi_32/0iaWEjweUtZrlmptlnWvG4jn9CU8UJ7oUuCa8Q7sDgfJAXTaics85ibYyyKRlHfcEEm0Dtia8Mkf5uh4RIqziavsN8A/132', 'e649c9672d173e3061eff6b6d38b28d2', 1519871519);
INSERT INTO `opf_readlog` VALUES (26, 2, 2, 'o73cGwWTqgmCwTYR2GO57aBDaTjU', 'meiny', 'http://thirdwx.qlogo.cn/mmopen/vi_32/3G9MQK2ZRV9pqJSI8SvLH6B5rXDaiazSShOJlO39ctLzVJb0icyLyOUKQ92F4tktH8XgdE5jLAeOYicibBnBEXstXg/132', 'e77f4fcc373c27abedf1dae0d4960023', 1519871570);
INSERT INTO `opf_readlog` VALUES (27, 2, 2, 'o73cGwfradtQjZ4OnOpDxtzXj-II', '白云蓝婧萱', 'http://thirdwx.qlogo.cn/mmopen/vi_32/PrKSgib1DcguGCXPAFCOb3syqGesMvE6ytj5TJ0FvpuCuZXBXyNkSAzDCPibcibzDxSqEZWQrCwpOCuyXmibHbXbiaA/132', '8e66a625e71dce2d41df1fa5dded2c18', 1519872289);
INSERT INTO `opf_readlog` VALUES (28, 2, 2, 'o73cGwV_FyqEiDzZ6QkgT8zmPYrw', 'wuyi', 'http://thirdwx.qlogo.cn/mmopen/vi_32/sJEYBibuAyO2v3UTC0cntCiazeF5h0pvLxkFqNuYia0fgXLWHBQQqzjz9pMYnudk1pJDNwRjksYkFf3K1uTbnsdFg/132', '745d8c8dc56a5f66f8febaf2b8f2c980', 1519873776);
INSERT INTO `opf_readlog` VALUES (29, 2, 2, 'o73cGwd4prffb3ygIMpxKG2lZjy8', 'Best lover', 'http://thirdwx.qlogo.cn/mmopen/vi_32/qEbHIAvg4F6UmX9NSJCibH3E512T71iacJUnaWwYJW9x2VCQaygibiaCtlggS9JKNwaJ2hgiaXm5GsRg4DotcrZ0vpw/132', '6cfa7eebea98c4ece834a3b3932ee61c', 1519874353);
INSERT INTO `opf_readlog` VALUES (30, 2, 2, 'o73cGwUrfmHqww8_ibpJxi98N2hQ', 'Love has Providence', 'http://thirdwx.qlogo.cn/mmopen/vi_32/gh6cKSmDqNBZtwjkno8Q0dTUFP0EmVkStKe6RtU7Yzmd7ibUw443sMsymzxQ2OslMI26FNk9GaeBnicibjaiaCo73g/132', 'f751fd56749ba6210befeb997cccd944', 1519875418);
INSERT INTO `opf_readlog` VALUES (31, 2, 2, 'o73cGwQU9MUm8brpfzZvAMUxxB8g', '欢', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLr5yvbicZQjpXO78Yw9F8AkdgSd5O8Vib1EHxvtADY7sicxdx1ibkICMGpB9uiciaXOibRicDq8tNL7SY09w/132', '27030e12522da5a6d35694943bdccbac', 1519875558);
INSERT INTO `opf_readlog` VALUES (32, 2, 2, 'o73cGwYQk22hrzzLY-1UHm5n8oDk', '张春兰', 'http://thirdwx.qlogo.cn/mmopen/vi_32/jL0T8DLC74KszicGcShVhwJvImWIPBHtmvzCyiby58pGkfPX3UzRkicUSSHIbNAVB1H04ellCJdSOfIkYxSO5e3GQ/132', 'b908acf6bdf4ae1a5aaf450fb8db995b', 1519876218);
INSERT INTO `opf_readlog` VALUES (33, 2, 2, 'o73cGweIrDmq3kdD0ndBS9eHHdAU', '所谓伊人、', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIia6JywZv309k1Apgt9CRKqEpgZ81OzbLqkzolbxfPF4GOkicCiaXARqZoqFbwIwnibG96TAIlb78wkQ/132', '2e9a158288b241d06bb47d62c73d5e2b', 1519876329);
INSERT INTO `opf_readlog` VALUES (34, 2, 2, 'o73cGwVlFajLd1RHXYWeOdpRFnf0', '荼蘼花开', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Y4R8glNgDXjwCQDeu2VhJFM0GamMc9icAzvFA7YsA17FEUKxojMcaEW9vNsicLoMR2as5T3Fib8dYlNx45wSHYyRQ/132', 'f44d41e3eb89a1302d50e9ad52e0f165', 1519877732);
INSERT INTO `opf_readlog` VALUES (35, 2, 2, 'o73cGwVHhbD48YNwfvoilhMdyOro', '', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83ep43BFD5j8h530Ysq2ZkVqc2RqfibffsnpMLkAmqMO00zHZeicbia8utYmgRmsZDbiaHEzTI4ichKia1Uvw/132', 'afdfb843981c6f2623e7ace4782a0069', 1519877988);
INSERT INTO `opf_readlog` VALUES (36, 2, 2, 'o73cGwfxxhHTtbKf7cYyhKqRSZdk', '丽', 'http://thirdwx.qlogo.cn/mmopen/vi_32/hdtG6n6ichEy2chY8f8ibO87tSE6ibcYgSXZ2TDeQsIqNeatqJKcK83icDWkHTKQBSe0bqOmiapg6AnNLpOoqs5ULKQ/132', 'b853bf8e8f6721963c27bf9b7a21dfd9', 1519878139);
INSERT INTO `opf_readlog` VALUES (37, 2, 2, 'o73cGwS6QETABLm-mY0yjvxTxYl8', '李春凤', 'http://thirdwx.qlogo.cn/mmopen/vi_32/1oDNM9AJSUhrGrQIj9I3kiatPVYT7ZWcWybrCqdRnHHXdJ0jECMbe3kjlcCkQrqf2b3Mgjgiczt2th4b5j9Or3hw/132', '5e8a5c4d81c9439c63e42884afc75f93', 1519883599);
INSERT INTO `opf_readlog` VALUES (38, 2, 2, 'o73cGwe5Gv62nsxvW39gpYYo6jSk', '贝贝佳周欣', 'http://thirdwx.qlogo.cn/mmopen/vi_32/qqu275NdMzlQibes8WMy9qJJFWRbEG5HWLm6F9qWwpuJcBOhZTDQ09qicDegNwlCqEnt1jvK9YWxXsJe4icsQm9xg/132', 'b22875b4266d98718efe6d632f80fbe3', 1519900780);
INSERT INTO `opf_readlog` VALUES (39, 2, 2, 'o73cGwfh_xBzzGd9TEdiyAhhkvq0', '王秀英', 'http://thirdwx.qlogo.cn/mmopen/vi_32/wTiaH8ccG2pXtpGdxF7K4NQLa0mraxFsUxGfJACZaBtsx7uEOCzduyVSxyznZLbaAw9F8fTJPxhJdZQBCHBVUgw/132', '062b7f1e53fd8a92ba3e96ec00a807dd', 1519902448);
INSERT INTO `opf_readlog` VALUES (40, 2, 2, 'o73cGwdme5c4eFJ10um3X7ifYmDE', '海阔天空', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIq1iajMgrBkK15xcqFwhFXTjmXXTibql98C4t1QbDGfOuUjv0IIjfZ1nrFTWibXS1f2rFGYoscck9YQ/132', '45a27a462d17fc65c7afe1a5d15a33de', 1519950367);
INSERT INTO `opf_readlog` VALUES (41, 2, 2, 'o73cGwRdCafX6IPISeTvll9cUBe0', '易通瓷砖批发@周晓君15902330330', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Vnx2INt7VaBOu7haVzxMd0WBFnY1icIQq2gwvCUkNVGdkESLjYQg3RscqARgspKAOK9kqfJ2AZnj7Os1GlAiazsw/132', 'af9d5cbbc3ce39adeae3c12e3294c487', 1520053389);
INSERT INTO `opf_readlog` VALUES (42, 2, 9, 'o73cGwYB3WuGdM3GxgEfRiemiSAg', '靖小米', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLLgTzWH8GGcUaFRWIqPWaALSKFCJlhCb9ffPbwVeU91NlB9yexkj0CtM8iccqI1vVNaSGbPok82XQ/132', '38c2646a53b68b346c04b2bd28d5d957', 1522324792);
INSERT INTO `opf_readlog` VALUES (43, 2, 10, 'o73cGwfi-eJHXh-tq5h39mNdqjRw', '王小辰', 'http://thirdwx.qlogo.cn/mmopen/vi_32/hxbqCteSU6VrrVVlibicEftmCq3FlPVU0mWFicYOxm1Iy1T7UBokX64QIjCDMUSYgyATs0J7CUanMgia9nWg9UPUcA/132', 'b2216df65d90a4f6490e7be99eb914c5', 1525403172);

-- ----------------------------
-- Table structure for opf_readuser
-- ----------------------------
DROP TABLE IF EXISTS `opf_readuser`;
CREATE TABLE `opf_readuser`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `readid` bigint(20) NOT NULL,
  `openid` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `createtime` int(11) NOT NULL,
  `status` varchar(5) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'in,end',
  `nickname` varchar(24) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `headimgurl` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `viewnum` int(11) NOT NULL,
  `gh_id` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '阅读任务' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of opf_readuser
-- ----------------------------
INSERT INTO `opf_readuser` VALUES (1, 1, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 1519223759, 'in', '大娃-何跃', 'http://thirdwx.qlogo.cn/mmopen/vi_32/eWtejun7vSn7cqfAibribL90CicXC2hLbWWAfnCqNY4N2sdVWLkNribIlhhibSQiaHGeTjl01WBSzs2euPbvmHVNFib8Q/132', 1, 'gh_07045554632a');
INSERT INTO `opf_readuser` VALUES (2, 2, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 1519552456, 'end', '大娃-何跃', 'http://thirdwx.qlogo.cn/mmopen/vi_32/eWtejun7vSn7cqfAibribL90CicXC2hLbWWAfnCqNY4N2sdVWLkNribIlhhibSQiaHGeTjl01WBSzs2euPbvmHVNFib8Q/132', 36, 'gh_07045554632a');
INSERT INTO `opf_readuser` VALUES (3, 2, 'o73cGwSTPa07-i_C6ngzEcZ8kpEA', 1519553707, 'in', '锅烟煤', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLuYYonzUfOCZ1sq4ploqC11FYIU6eicCPlV0ia8dfXvVMrsy9OrrGIKBfFTkv47u0KaXJXapaPXxzA/132', 1, 'gh_07045554632a');
INSERT INTO `opf_readuser` VALUES (4, 2, 'o73cGwflBGZyE_FYzV2Tn9lizSBM', 1519632215, 'in', '꧁༺冬༻꧂', 'http://thirdwx.qlogo.cn/mmopen/vi_32/P5XJib9wlWdPthCbua6Z7NjHbia2DbibdEzlo268Q4SxoBJpicPj1MNrtzdddEyYqa3SRuqKV7Dd3qKSRtQuQqiayQw/132', 2, 'gh_07045554632a');
INSERT INTO `opf_readuser` VALUES (5, 2, 'o73cGwY7zxBfGiOMHQxlIFE7scp8', 1519731031, 'in', '天天向上', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTKqgkh0Bib1ggeDq4PAW2AF5R4YF11Q1ia2ORs13u7oibn4WWfNALeEKSHSjDib4eXVFjtkjXOc6vkUfg/132', 0, 'gh_07045554632a');
INSERT INTO `opf_readuser` VALUES (6, 2, 'o73cGwSUvqV5OO030MheLbWJJ9rE', 1519731043, 'in', '小', 'http://thirdwx.qlogo.cn/mmopen/vi_32/a6ia2YcQ9Nw2olueLQQXXEoNJSFuOH2gnDTg8f65sSrG1V0xQia97a1qYuCVib3EiaxWiaVeQzhH3UOScLyIRAICpUQ/132', 0, 'gh_07045554632a');
INSERT INTO `opf_readuser` VALUES (7, 2, 'o73cGwfx2tI1OCaeVi9WxQ9qSMgk', 1519731059, 'in', '我不多话只爱多想', 'http://thirdwx.qlogo.cn/mmopen/vi_32/oCx7GX9P4w5ToEeRl1IiaZVhUdeWJdecx4Gs0ncHicY0WBTa8FG9HeKicAj7z0uicL06rJ59k4VtG6SG36uibqBffOQ/132', 0, 'gh_07045554632a');
INSERT INTO `opf_readuser` VALUES (8, 2, 'o73cGwUkGve7VPUbI1QgeSUWhlGU', 1519731060, 'in', '天天快乐', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q3csR3XGFcibte7RjD7xbg23flR9UVVKnKzKDMyn3xkWOwP6MWD6g3Tb4EJEBwHkXlOLibiaKuNSTb4RTPE99ibXIw/132', 0, 'gh_07045554632a');
INSERT INTO `opf_readuser` VALUES (9, 2, 'o73cGwSsq2UjZlGpFmoraCm-8mcU', 1522324715, 'in', '你不知道的事', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTKEicWfExOkiaGN4wqFxJ2IgymQX4pqL6o3XrJvHHsBC9vcpZT9tY6bcUEvgtK9TwYx70trG3H2ticzg/132', 1, 'gh_07045554632a');
INSERT INTO `opf_readuser` VALUES (10, 2, 'o73cGwUkdj3s9wgDiCZTl63Gqr9Q', 1525403109, 'in', '大熊猫猫', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIc9VqokTCzIbo3UsZCMQVdLNNRaOltiauP26Vj70o7PhSHB3XeibcvibaEzs8bqICoUibWJwK2o7CTRQ/132', 1, 'gh_07045554632a');

-- ----------------------------
-- Table structure for opf_signin
-- ----------------------------
DROP TABLE IF EXISTS `opf_signin`;
CREATE TABLE `opf_signin`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `gh_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '签到任务标题',
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '签到任务详情',
  `chargenum` int(11) NOT NULL COMMENT '充值次数',
  `totalnum` int(11) NOT NULL COMMENT '总额分',
  `leftnum` int(11) NOT NULL COMMENT '余额',
  `lastchargetime` int(11) NOT NULL COMMENT '最后充值时间',
  `limittime` int(11) NOT NULL COMMENT '连续签到几天',
  `prisemin` int(11) NOT NULL COMMENT '奖励最低金额分',
  `prisemax` int(11) NOT NULL COMMENT '奖励最高金额分',
  `signinnum` int(11) NOT NULL COMMENT '签到人次',
  `prisednum` int(11) NOT NULL COMMENT '奖励人次',
  `showpic` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '封面图',
  `description` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '简介',
  `locationlimit` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否限制签到地点',
  `jumpurl` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '签到完了跳转链接',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '公众号签到任务' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of opf_signin
-- ----------------------------
INSERT INTO `opf_signin` VALUES (1, 'gh_7bf948cbfdb0', '铜梁视窗会员签到有福利！', '<br/>\r\n铜梁视窗每日签到细则：\r\n<br/>\r\n1. 网友连续签到7天即可获得随机现金红包1个；\r\n<br/>\r\n2. 网友连续签到必须在同一月份有效；\r\n<br/>\r\n3. 网友签到中断，则会重新统计签到天数，例如在7号忘了签到，那么签到统计将从8号重新开始！\r\n<br/>\r\n4. 取消公众号关注后该会员的统计将被清零！\r\n<br/>\r\n', 200, 1000, 175, 1520217324, 7, 100, 101, 5, 425, 'http://imgs.soufun.com/news/2015_09/23/news/1442998756090_000.jpg', '连续签到7天领取红包', NULL, 'https://www.maimaitl.com/m');
INSERT INTO `opf_signin` VALUES (2, 'gh_5cb2bd578b63', '爱尔眼科签到测试', '签到测试', 100, 100, 101, 0, 7, 100, 100, 1, 0, '', '', NULL, '');

-- ----------------------------
-- Table structure for opf_signinlog
-- ----------------------------
DROP TABLE IF EXISTS `opf_signinlog`;
CREATE TABLE `opf_signinlog`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `gh_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `gh_openid` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '公众号的openid',
  `openid` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `headimg` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `nickname` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `signinid` bigint(20) NOT NULL,
  `signintime` int(11) NOT NULL COMMENT '签到时间',
  `location` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '坐标',
  `prised` int(11) NOT NULL DEFAULT 0 COMMENT '是否受到了奖励',
  `topic_img` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `topic_imgs` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `tel` varchar(12) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 12 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '个人签到登记' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of opf_signinlog
-- ----------------------------
INSERT INTO `opf_signinlog` VALUES (6, 'gh_7bf948cbfdb0', 'oN5FQuH12hi5_lc4RTRDXrF-ZeKg', 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 'http://thirdwx.qlogo.cn/mmopen/vi_32/eWtejun7vSn7cqfAibribL90CicXC2hLbWWAfnCqNY4N2sdVWLkNribIlhhibSQiaHGeTjl01WBSzs2euPbvmHVNFib8Q/132', '大娃-何跃', 1, 1520239015, '106.08821680593,29.825740544794', 1, '', '', '', '');
INSERT INTO `opf_signinlog` VALUES (7, 'gh_7bf948cbfdb0', 'oN5FQuH12hi5_lc4RTRDXrF-ZeKg', 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 'http://thirdwx.qlogo.cn/mmopen/vi_32/eWtejun7vSn7cqfAibribL90CicXC2hLbWWAfnCqNY4N2sdVWLkNribIlhhibSQiaHGeTjl01WBSzs2euPbvmHVNFib8Q/132', '大娃-何跃', 1, 1520302536, '114.02597366,22.54605355', 1, '', '', '', '');
INSERT INTO `opf_signinlog` VALUES (8, 'gh_7bf948cbfdb0', 'oN5FQuI62F-LkGYHghBO-jHbIvhw', 'o73cGwSTPa07-i_C6ngzEcZ8kpEA', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLuYYonzUfOCZ1sq4ploqC11FYIU6eicCPlV0ia8dfXvVMrsy9OrrGIKBfFTkv47u0KaXJXapaPXxzA/132', '锅烟煤', 1, 1520302989, '106.08819266268,29.825730548389', 1, '', '', '', '');
INSERT INTO `opf_signinlog` VALUES (9, 'gh_7bf948cbfdb0', 'oN5FQuEoj7Fc-guLcSqJmrk3Vyt8', 'o73cGwflBGZyE_FYzV2Tn9lizSBM', 'http://thirdwx.qlogo.cn/mmopen/vi_32/P5XJib9wlWdPthCbua6Z7NjHbia2DbibdEzlo268Q4SxoBJpicPj1MNrtzdddEyYqa3SRuqKV7Dd3qKSRtQuQqiayQw/132', '꧁༺冬༻꧂', 1, 1520313846, '106.08791726181,29.826758481252', 0, '', '', '', '');
INSERT INTO `opf_signinlog` VALUES (10, 'gh_7bf948cbfdb0', 'oN5FQuMzpVHGddAnvO-hWs9uBqRk', 'o73cGwZRBgTtBwpI02XqqjDNoXus', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83erVNHlGkjRZyx928r6x2NJJfBcQpnFNLyia0nLsicPZcE0K06a418bqMRZhDDA1UBE2Nw9Ug52tMicXQ/132', '云', 1, 1520314744, '106.08791974382,29.826987633681', 0, '', '', '', '');
INSERT INTO `opf_signinlog` VALUES (11, 'gh_5cb2bd578b63', 'onfe60XphC3_NZleQjwO41mAf0LM', 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 'http://thirdwx.qlogo.cn/mmopen/vi_32/eWtejun7vSn7cqfAibribL90CicXC2hLbWWAfnCqNY4N2sdVWLkNribIlhhibSQiaHGeTjl01WBSzs2euPbvmHVNFib8Q/132', '大娃-何跃', 2, 1520318784, '106.08645446888,29.826066032011', 0, '', '', '', '');

-- ----------------------------
-- Table structure for opf_signrank
-- ----------------------------
DROP TABLE IF EXISTS `opf_signrank`;
CREATE TABLE `opf_signrank`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `openid` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `gh_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `total` int(11) NOT NULL,
  `surplus` int(11) NOT NULL,
  `used` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of opf_signrank
-- ----------------------------
INSERT INTO `opf_signrank` VALUES (1, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 'gh_07045554632a', 1000, 960, 40);

-- ----------------------------
-- Table structure for opf_tickt
-- ----------------------------
DROP TABLE IF EXISTS `opf_tickt`;
CREATE TABLE `opf_tickt`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '消息的code',
  `checkshopid` int(11) NOT NULL COMMENT '核销店铺编号',
  `shareopenid` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'openid或者是check',
  `checkopenid` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '核销人的openid',
  `ticksid` int(11) NOT NULL COMMENT '票据的id',
  `createtime` int(11) NOT NULL COMMENT '创建时间',
  `usedtime` int(11) NOT NULL COMMENT '使用时间',
  `usedopenid` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '核销人的openid',
  `openid` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户的openid',
  `commission` int(11) NOT NULL COMMENT '本单佣金',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 39 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '这个是用户将要收到的券' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of opf_tickt
-- ----------------------------
INSERT INTO `opf_tickt` VALUES (1, 'ae5aca80ba250bac4afc8f6e2759024b', 10003, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', '0', 0, 1523280006, 0, '', '', 0);
INSERT INTO `opf_tickt` VALUES (2, '7bd91820875e6518751f98307b27874a', 10003, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', '0', 0, 1523280085, 0, '', '', 0);
INSERT INTO `opf_tickt` VALUES (3, '4114b3e1074f7ab75314b72b4c64b196', 10003, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', '0', 1, 1523280252, 0, '', '', 0);
INSERT INTO `opf_tickt` VALUES (4, '310c1b4526bf9fd6116cd1c6ac8e72ef', 10003, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', '0', 1, 1523280421, 0, '', '', 0);
INSERT INTO `opf_tickt` VALUES (5, 'cc0e32b3829a15244749accbfee061d0', 10003, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 1, 1523280460, 1523281185, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 20);
INSERT INTO `opf_tickt` VALUES (6, 'dc2a4e796eb2a1b39e0af1b16c5f796d', 10003, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 1, 1523322674, 1523322752, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 20);
INSERT INTO `opf_tickt` VALUES (7, '5dc5a3c1a351f4043a1f761eb83d5ed7', 10003, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 1, 1523323091, 1523323127, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 20);
INSERT INTO `opf_tickt` VALUES (8, '82f71ad0a749516c533e777b9df80c71', 10003, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 1, 1523323165, 1523407530, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 20);
INSERT INTO `opf_tickt` VALUES (9, '14315eb636df6fe9e04599b7015b7e81', 10003, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 1, 1523332330, 0, '', '', 20);
INSERT INTO `opf_tickt` VALUES (10, '894012cceab40876f86aee4adc68af96', 10003, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 1, 1523332485, 0, '', '', 20);
INSERT INTO `opf_tickt` VALUES (11, '9e70e29557eaabc06f5bc9d31c6d75eb', 10003, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 1, 1523332651, 0, '', 'o73cGwSTPa07-i_C6ngzEcZ8kpEA', 20);
INSERT INTO `opf_tickt` VALUES (12, 'cbd92b732426dafd8f6676e9b5a04444', 10003, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 1, 1523338345, 0, '', '', 20);
INSERT INTO `opf_tickt` VALUES (13, '95ed79aba6331e49c4dcc51070602d76', 10003, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 1, 1523338352, 1523940460, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 20);
INSERT INTO `opf_tickt` VALUES (14, '35419848a2784ce00ae48a5ee5d6f079', 10003, '铜梁视窗', 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 0, 1523367323, 0, '', '', 0);
INSERT INTO `opf_tickt` VALUES (15, 'c6bee58aaeab333e3895d3293a721160', 10003, '铜梁视窗', 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 0, 1523367358, 0, '', '', 0);
INSERT INTO `opf_tickt` VALUES (16, '00b1da189ea79a83a9e5fca454542f71', 10003, '铜梁视窗', 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 0, 1523367386, 0, '', '', 0);
INSERT INTO `opf_tickt` VALUES (17, '553cbb57129163a6a224232775ed4d59', 10003, '铜梁视窗', 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 0, 1523367399, 0, '', '', 0);
INSERT INTO `opf_tickt` VALUES (18, '0b8fc76066aaa521d37422ff8928169c', 10003, '铜梁视窗', 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 0, 1523367445, 0, '', '', 0);
INSERT INTO `opf_tickt` VALUES (19, 'e22b595eab9a6f4522e4b65316d108a7', 10003, '铜梁视窗', 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 1, 1523367582, 1523438032, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 0);
INSERT INTO `opf_tickt` VALUES (20, '1c7ff6100f9dae32487f2c8728f4ceae', 10003, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 1, 1523409932, 0, '', '', 20);
INSERT INTO `opf_tickt` VALUES (21, '80abf20f6f64d9828f9b1aa325a61d06', 10003, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 1, 1523428597, 1523428820, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 'o73cGwflBGZyE_FYzV2Tn9lizSBM', 20);
INSERT INTO `opf_tickt` VALUES (22, 'a5501484eaa326a9775cf7b7bae7900f', 10003, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 1, 1523430402, 0, '', '', 20);
INSERT INTO `opf_tickt` VALUES (23, '8a9dec7640fa2aa6460613f2621ccc35', 10003, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 1, 1523438174, 0, '', '', 20);
INSERT INTO `opf_tickt` VALUES (24, '55e25d2c320520ec9de194e4cccff9e8', 10003, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 1, 1523438210, 0, '', '', 20);
INSERT INTO `opf_tickt` VALUES (25, 'cf22795c721d333a55374b43aaa7039e', 10003, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 1, 1523438243, 0, '', '', 20);
INSERT INTO `opf_tickt` VALUES (26, '0f53342f54c55e735fa4d5499474a629', 10003, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 1, 1523438269, 0, '', '', 20);
INSERT INTO `opf_tickt` VALUES (27, 'ef732223ec6a04753b8d7611313555d4', 10003, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 1, 1523438300, 0, '', '', 20);
INSERT INTO `opf_tickt` VALUES (28, '28e3f49bc48d390f66fe23a83c2ff844', 10003, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 1, 1523438435, 0, '', '', 20);
INSERT INTO `opf_tickt` VALUES (29, 'ed189d3ae81c249a168c71c0814cd33d', 10003, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 1, 1523940471, 0, '', '', 20);
INSERT INTO `opf_tickt` VALUES (30, '80eddde1fb924d9d79d39dfd23ec1ab3', 10003, 'o73cGwY0T-68x_LigknfWX2HvTYE', 'o73cGwZLQw0XSG_7qJ9abHuH_sEI,	o73cGwY0T-68x_LigknfWX2HvTYE', 1, 1524473685, 0, '', '', 20);
INSERT INTO `opf_tickt` VALUES (31, 'fb704a7ee22c92e3ec86925be722b2f5', 10003, 'o73cGwY0T-68x_LigknfWX2HvTYE', 'o73cGwZLQw0XSG_7qJ9abHuH_sEI,	o73cGwY0T-68x_LigknfWX2HvTYE', 1, 1524474049, 0, '', '', 20);
INSERT INTO `opf_tickt` VALUES (32, 'e6c10d6feb47d9c2ef85d9a2b8786305', 10003, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 'o73cGwZLQw0XSG_7qJ9abHuH_sEI,	o73cGwY0T-68x_LigknfWX2HvTYE', 1, 1524474068, 0, '', '', 20);
INSERT INTO `opf_tickt` VALUES (33, '892ebf5a5f41bc4d95ab85f092ee6ad1', 10003, 'o73cGwY0T-68x_LigknfWX2HvTYE', 'o73cGwZLQw0XSG_7qJ9abHuH_sEI,	o73cGwY0T-68x_LigknfWX2HvTYE', 1, 1524474106, 0, '', '', 20);
INSERT INTO `opf_tickt` VALUES (34, 'eddc1845d7df4015c7b568745b916fbe', 10003, 'o73cGwY0T-68x_LigknfWX2HvTYE', 'o73cGwZLQw0XSG_7qJ9abHuH_sEI,	o73cGwY0T-68x_LigknfWX2HvTYE', 1, 1524474204, 0, '', '', 20);
INSERT INTO `opf_tickt` VALUES (35, '4188bab331b1408e992f7b056e06a838', 10003, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 'o73cGwZLQw0XSG_7qJ9abHuH_sEI,	o73cGwY0T-68x_LigknfWX2HvTYE', 1, 1524474274, 0, '', '', 20);
INSERT INTO `opf_tickt` VALUES (36, 'eb02e35689961bcaa706d7a3eb672733', 10003, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 'o73cGwZLQw0XSG_7qJ9abHuH_sEI,	o73cGwY0T-68x_LigknfWX2HvTYE', 1, 1524474303, 0, '', 'o73cGwSTPa07-i_C6ngzEcZ8kpEA', 20);
INSERT INTO `opf_tickt` VALUES (37, '7e6a3aa3d55fee852139774e160d35df', 10003, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 'o73cGwZLQw0XSG_7qJ9abHuH_sEI,	o73cGwY0T-68x_LigknfWX2HvTYE', 1, 1524474422, 0, '', '', 20);
INSERT INTO `opf_tickt` VALUES (38, 'e3662f39c7d98d320587912d4c0f7ab3', 10003, 'o73cGwY0T-68x_LigknfWX2HvTYE', 'o73cGwZLQw0XSG_7qJ9abHuH_sEI,	o73cGwY0T-68x_LigknfWX2HvTYE', 1, 1524474620, 0, '', '', 20);


-- ----------------------------
-- Table structure for opf_ticktcommission
-- ----------------------------
DROP TABLE IF EXISTS `opf_ticktcommission`;
CREATE TABLE `opf_ticktcommission`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ticktsid` bigint(20) NOT NULL,
  `ticktid` bigint(20) NOT NULL,
  `openid` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'share,use,check',
  `commission` double NOT NULL,
  `createtime` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 43 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '券佣金' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of opf_ticktcommission
-- ----------------------------
INSERT INTO `opf_ticktcommission` VALUES (1, 1, 6, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 'share', 8, 1523322743);
INSERT INTO `opf_ticktcommission` VALUES (2, 1, 6, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 'check', 6, 1523322743);
INSERT INTO `opf_ticktcommission` VALUES (3, 1, 6, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 'use', 6, 1523322743);
INSERT INTO `opf_ticktcommission` VALUES (4, 1, 6, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 'share', 8, 1523322748);
INSERT INTO `opf_ticktcommission` VALUES (5, 1, 6, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 'check', 6, 1523322748);
INSERT INTO `opf_ticktcommission` VALUES (6, 1, 6, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 'use', 6, 1523322748);
INSERT INTO `opf_ticktcommission` VALUES (7, 1, 6, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 'share', 8, 1523322752);
INSERT INTO `opf_ticktcommission` VALUES (8, 1, 6, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 'check', 6, 1523322752);
INSERT INTO `opf_ticktcommission` VALUES (9, 1, 6, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 'use', 6, 1523322752);
INSERT INTO `opf_ticktcommission` VALUES (10, 1, 7, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 'share', 8, 1523323114);
INSERT INTO `opf_ticktcommission` VALUES (11, 1, 7, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 'check', 6, 1523323114);
INSERT INTO `opf_ticktcommission` VALUES (12, 1, 7, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 'use', 6, 1523323114);
INSERT INTO `opf_ticktcommission` VALUES (13, 1, 7, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 'share', 8, 1523323119);
INSERT INTO `opf_ticktcommission` VALUES (14, 1, 7, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 'check', 6, 1523323119);
INSERT INTO `opf_ticktcommission` VALUES (15, 1, 7, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 'use', 6, 1523323119);
INSERT INTO `opf_ticktcommission` VALUES (16, 1, 7, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 'share', 8, 1523323123);
INSERT INTO `opf_ticktcommission` VALUES (17, 1, 7, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 'check', 6, 1523323123);
INSERT INTO `opf_ticktcommission` VALUES (18, 1, 7, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 'use', 6, 1523323123);
INSERT INTO `opf_ticktcommission` VALUES (19, 1, 7, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 'share', 8, 1523323127);
INSERT INTO `opf_ticktcommission` VALUES (20, 1, 7, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 'check', 6, 1523323127);
INSERT INTO `opf_ticktcommission` VALUES (21, 1, 7, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 'use', 6, 1523323127);
INSERT INTO `opf_ticktcommission` VALUES (22, 1, 8, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 'share', 8, 1523333686);
INSERT INTO `opf_ticktcommission` VALUES (23, 1, 8, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 'check', 6, 1523333686);
INSERT INTO `opf_ticktcommission` VALUES (24, 1, 8, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 'use', 6, 1523333686);
INSERT INTO `opf_ticktcommission` VALUES (25, 1, 8, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 'share', 8, 1523407530);
INSERT INTO `opf_ticktcommission` VALUES (26, 1, 8, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 'check', 6, 1523407530);
INSERT INTO `opf_ticktcommission` VALUES (27, 1, 8, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 'use', 6, 1523407530);
INSERT INTO `opf_ticktcommission` VALUES (28, 1, 21, 'o73cGwflBGZyE_FYzV2Tn9lizSBM', 'share', 8, 1523428820);
INSERT INTO `opf_ticktcommission` VALUES (29, 1, 21, 'o73cGwflBGZyE_FYzV2Tn9lizSBM', 'check', 6, 1523428820);
INSERT INTO `opf_ticktcommission` VALUES (30, 1, 21, 'o73cGwflBGZyE_FYzV2Tn9lizSBM', 'use', 6, 1523428820);
INSERT INTO `opf_ticktcommission` VALUES (31, 1, 13, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 'share', 8, 1523437961);
INSERT INTO `opf_ticktcommission` VALUES (32, 1, 13, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 'check', 6, 1523437961);
INSERT INTO `opf_ticktcommission` VALUES (33, 1, 13, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 'use', 6, 1523437961);
INSERT INTO `opf_ticktcommission` VALUES (34, 1, 19, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 'share', 0, 1523438032);
INSERT INTO `opf_ticktcommission` VALUES (35, 1, 19, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 'check', 0, 1523438032);
INSERT INTO `opf_ticktcommission` VALUES (36, 1, 19, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 'use', 0, 1523438032);
INSERT INTO `opf_ticktcommission` VALUES (37, 1, 13, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 'share', 8, 1523438333);
INSERT INTO `opf_ticktcommission` VALUES (38, 1, 13, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 'check', 6, 1523438333);
INSERT INTO `opf_ticktcommission` VALUES (39, 1, 13, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 'use', 6, 1523438333);
INSERT INTO `opf_ticktcommission` VALUES (40, 1, 13, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 'share', 8, 1523940460);
INSERT INTO `opf_ticktcommission` VALUES (41, 1, 13, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 'check', 6, 1523940460);
INSERT INTO `opf_ticktcommission` VALUES (42, 1, 13, 'o73cGwZLQw0XSG_7qJ9abHuH_sEI', 'use', 6, 1523940460);


-- ----------------------------
-- Table structure for opf_tickts
-- ----------------------------
DROP TABLE IF EXISTS `opf_tickts`;
CREATE TABLE `opf_tickts`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `shopid` int(50) NOT NULL,
  `shopname` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `summry` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `condi` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '赠送条件',
  `adimg` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `adurl` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `worth` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `surplus` int(11) NOT NULL,
  `used` int(11) NOT NULL,
  `coin` int(11) NOT NULL COMMENT '要好多积分',
  `createtime` int(11) NOT NULL,
  `deadtime` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '券包集合' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of opf_tickts
-- ----------------------------
INSERT INTO `opf_tickts` VALUES (1, 10003, '重庆众智明星科技有限公司', '铜梁视窗网络服务立省200元券', '任何个人或企业在于铜梁视窗达成合作协议后（微商城建设、小程序建设、企业公众号托管等业务），出示此券立减200元', '在铜梁视窗合伙商家处消费满100元可得', 'http://source.5atl.com/attachment/images/20180407/c35808d9e73b6846eca0ea5a3785882c.jpg', 'http://www.dmake.cn', 200, 100, 85, 0, 10, 1523083271, 1623083271);