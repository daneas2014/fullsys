<?php
namespace app\openflat\model;

use think\Model;
use think\Db;
use EasyWeChat\Kernel\Messages\News;
use EasyWeChat\Kernel\Messages\NewsItem;
use EasyWeChat\Kernel\Messages\Text;

class ServiceEvent extends Model
{

    public function processEvent($wxid, $openid, $message, $cusapp = NULL)
    {
        switch ($message['Event']) {
            case 'subscribe':
                
                // 1.登记用户
                $user = $cusapp->user->get($openid);
                $user['gh_id'] = $wxid;
                $user['data'] = json_encode($user);
                
                unset($user['subscribe']);
                unset($user['language']);
                unset($user['country']);
                unset($user['remark']);
                unset($user['groupid']);
                unset($user['tagid_list']);
                
                Db::name('op_appuser')->insert($user);
                
                if ($message['EventKey']) {
                    $key = str_replace('qrscene_', '', $message['EventKey']);
                    $tiket = $message->Ticket;
                    return $key;
                }
                
                // 2. 看看是否有回复关键词
                $twk = Db::name('wx_keywords')->where('carjump', 1)
                    ->where('gh_id', $wxid)
                    ->find();
                
                if ($twk == null) {
                    return new Text('感谢您关注咱们的公众号，更多服务请稍后体验');
                }
                
                // 3. 如果是文字就回复
                if ($twk['wordorpic'] == 0) {
                    return new Text($twk['description']);
                }
                
                // 4. 如果是图片就回复
                $nitem = [
                    new NewsItem([
                        'title' => $twk['title'],
                        'description' => $twk['description'],
                        'image' => $twk['showpic'],
                        'url' => $twk['url']
                    ])
                ];
                
                return new News($nitem);
                
                break;
            case 'unsubscribe':
                // 直接删除会员资料
                Db::name('op_appuser')->where('openid', $openid)->delete();
                return 'success';
                break;
            case 'LOCATION': // 服务号功能
                return $message['Latitude'] . ',' . $message['Longitude'] . ',' . $message['Precision'];
                break;
            case 'CLICK':
                $keywords = $message['EventKey'];
                $txt = new ServiceProcess();
                return $txt->processPutTxt($wxid, $openid, $keywords);
                break;
            case 'VIEW':
                $url = $message['EventKey'];
                //return $url;
                break;
            case 'SCAN': // 服务号功能
                $tiket = $message['Ticket'];
                return $tiket;
                break;
            default:
                return 'UNKNOWN';
                break;
        }
    }
}