<?php
namespace addons\apps\controller;

use think\addons\Controller;
use think\Db;

class Fontawesome extends controller {

	protected function _initialize() {
		parent::_initialize();

		$this->assign(['title' => 'fontwasome应用工具',
			'description' => 'fontwasome等小工具',
			'keywords' => '']);
	}

	public function fontawesome() { 
		return $this->fetch();
	}

	public function fontawesomeplus() { 

		return $this->fetch();
	}

	public function fontawesomev() { 

		$where['category'] = 'objects';
		$where['type'] = 'free';
		$list = Db::name('op_fontawesomev')->where($where)->order('category')->select();
		$nav = Db::name('op_fontawesomev')->group('category')->field('category')->select();
		$assign = ['list' => $list, 'nav' => $nav];
		$this->view->assign($assign);
		return $this->fetch();
	}
}
