<?php
namespace addons\apps;

use think\Addons;
use think\Db;

/**
 * 插件测试
 * @author byron sampson
 */
class Index extends Addons {
	public $info = [
		'name' => 'apps',
		'title' => '应用工具',
		'description' => 'fontwasome等小工具',
		'status' => 1,
		'author' => 'daneas',
		'version' => '1',
        'manage'=> 'apps://fontawesome/fontawesome'
	];

	/**
	 * 插件安装方法
	 * @return bool
	 */
	public function install() {
        $res = Db::execute(initAddon($this->info));

		if (!$res) {
			return false;
		}

		$sql = file_get_contents(__DIR__ . "/data.sql"); 

		$a = explode(";\r\n", $sql); //根据";\r\n"条件对数据库中分条执行
		$total = count($a);
		for ($i = 0; $i < $total; $i++) {
			$res = Db::query($a[$i]);
		}

		return $res > 0 ? true : false;
	}

	/**
	 * 插件卸载方法
	 * @return bool
	 */
	public function uninstall() {
		$sql = "DROP TABLE IF EXISTS op_fontawesomev;
		delete from addons where name='apps';";
		
		$a = explode(";\r\n", $sql); //根据";\r\n"条件对数据库中分条执行
		$total = count($a);
		for ($i = 0; $i < $total; $i++) {
			$res = Db::query($a[$i]);
		}
		return $res > 0 ? true : false;
	}

	/**
	 * 实现的testHook钩子方法
	 * @return mixed
	 */
	public function appshook($param) {
		echo '<p><font color="red">开始处理钩子啦</font></p>';
		echo '<p><font color="green">打印传给钩子的参数：</font></p>';
		dump($param);
		echo '<p><font color="green">打印插件配置：</font></p>';
		dump($this->getConfig());

		// 这里可以通过钩子来调用钩子模板
		return $this->fetch('info');
	}

}
