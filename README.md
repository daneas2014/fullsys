FullSys
=======================

#### 介绍
可以用于建立一切平台的系统底层，具备插件管理，并且API功能好到让新手菜鸟喜极而泣。DMAKE轻量级PHP平台建站系统（板砖博客）是基于Thinkphp5前后端分离的理念开发而来，经过门户网站、淘宝客、B2C商城、微信公众平台、资讯网站、资源网站、视频站和企业站群等多种运营模式集成并产品化，方便更多的个人站长和企业能够快速建立互联网品牌。
TIPS：于2023年11月拓展物联网管理框架，可对物联网设备点位、设备信息、资产和数据接口进行管理，并提供前端大屏、移动端数据接口。

#### 软件架构
软件架构说明


#### 安装教程

1.  搭建PHP7.X运营环境，推荐LNMP环境
2.  新建数据库fullsys，推荐Mysql8.0，如果拓展物联网应用，需要安装TDengine3.2.1版本及以上
3.  导入data文件init.sql至数据库中
4.  vender中包含诸多扩展，扩展文件更新至composer.json中，在本地安装本系统时请用composer做扩展安装
5.  本系统依赖于thinkphp5.0.24
6.  thinkphp5 获取地址 https://packagist.org/packages/topthink/framework#v5.0.24  基础知识学习地址：https://www.kancloud.cn/manual/thinkphp5/118003
7.  预设了TDengine的功能，参考依赖https://github.com/Yurunsoft/php-tdengine

#### 使用说明

1.  登录 https://www.dmake.cn 可见所有已知功能
2.  本系统仅供学习使用，商用需要和作者联系确认可用范围。
3.  如果在使用过程中发现bug或者疑问，请联系QQ 554305954 / 微信 daneas2014


ThinkPHP 5.0
===============
本系统基于TP5.0开发。

> ThinkPHP5的运行环境要求PHP5.4以上。

详细开发文档参考 [ThinkPHP5完全开发手册](http://www.kancloud.cn/manual/thinkphp5)

## 目录结构

初始的目录结构如下：

~~~
public  WEB部署目录（或者子目录）

├─addons                插件目录
│  ├─module_name        模块目录
│  │  ├─config.php      模块配置文件
│  │  ├─common.php      模块函数文件
│  │  ├─Index.php       插件管理controller
│  │  ├─controller      控制器目录
│  │  ├─model           模型目录
│  │  ├─view            视图目录
│  │  └─ ...            更多类库目录
├─application           应用目录
│  ├─common             公共模块目录（可以更改）
│  ├─module_name        模块目录
│  │  ├─config.php      模块配置文件
│  │  ├─common.php      模块函数文件
│  │  ├─controller      控制器目录
│  │  ├─model           模型目录
│  │  ├─view            视图目录
│  │  └─ ...            更多类库目录
│  │
│  ├─command.php        命令行工具配置文件
│  ├─common.php         公共函数文件
│  ├─helperfm.php       格式转化函数文件
│  ├─helperwx.php       微信公众号使用函数文件
│  ├─helpersend.php     发送短信或邮件函数文件
│  ├─tags.php           应用行为扩展定义文件
│
├─config                配置文件目录
├─minprogram            配套小程序源码目录
│
│
├─thinkphp              框架系统目录（尽量不更改）│
├─extend                扩展类库目录
├─runtime               应用的运行时目录（可写，可定制）
├─vendor                第三方类库目录（Composer依赖库）
├─build.php             自动生成定义文件（参考）
├─composer.json         composer 定义文件
├─LICENSE.txt           授权说明文件
├─README.md             README 文件
├─think                 命令行入口文件
~~~

> 上面的目录结构和名称是可以改变的，这取决于你的入口文件和配置参数。

## 命名规范

`ThinkPHP5`遵循PSR-2命名规范和PSR-4自动加载规范，并且注意如下规范：

### 目录和文件

*   目录不强制规范，驼峰和小写+下划线模式均支持；
*   类库、函数文件统一以`.php`为后缀；
*   类的文件名均以命名空间定义，并且命名空间的路径和类库文件所在路径一致；
*   类名和类文件名保持一致，统一采用驼峰法命名（首字母大写）；

### 函数和类、属性命名

*   类的命名采用驼峰法，并且首字母大写，例如 `User`、`UserType`，默认不需要添加后缀，例如`UserController`应该直接命名为`User`；
*   函数的命名使用小写字母和下划线（小写字母开头）的方式，例如 `get_client_ip`；
*   方法的命名使用驼峰法，并且首字母小写，例如 `getUserName`；
*   属性的命名使用驼峰法，并且首字母小写，例如 `tableName`、`instance`；
*   以双下划线“__”打头的函数或方法作为魔法方法，例如 `__call` 和 `__autoload`；

### 常量和配置

*   常量以大写字母和下划线命名，例如 `APP_PATH`和 `THINK_PATH`；
*   配置参数以小写字母和下划线命名，例如 `url_route_on` 和`url_convert`；

### 数据表和字段

*   数据表和字段采用小写加下划线方式命名，并注意字段名不要以下划线开头，例如 `think_user` 表和 `user_name`字段，不建议使用驼峰和中文作为数据表字段命名。


### 用户信息
* 前端的用户 session存储的 mid  memberinfo  一些字段提取：nickname headimage


## 参与开发

请参阅 [ThinkPHP5 核心框架包](https://github.com/top-think/framework)。

## 版权信息

ThinkPHP遵循Apache2开源协议发布，并提供免费使用。

本项目包含的第三方源码和二进制文件之版权信息另行标注。

版权所有Copyright © 2006-2018 by ThinkPHP (http://thinkphp.cn)

All rights reserved。

ThinkPHP® 商标和著作权所有者为上海顶想信息科技有限公司。

更多细节参阅 [LICENSE.txt](LICENSE.txt)

